import { ACCOUNT_TERMS } from 'src/terms';
import dynamic from "next/dynamic";

const UserAccountForm = dynamic(() => import('src/components/forms/UserAccountForm'));
const ProjectsTab = dynamic(() => import('src/components/widgets/ProjectsTab'));
const OrderDetails = dynamic(() => import('src/components/widgets/OrderDetails'));
const OrderTrackTab = dynamic(() => import('src/components/widgets/OrderTrackTab'));
const MyFilesTab = dynamic(() => import('src/components/widgets/MyFilesTab'));
const ShippingInfoForm = dynamic(() => import('src/components/forms/ShippingInfoForm'));
const PaymentMethodTab = dynamic(() => import('src/components/widgets/PaymentMethodTab'));

const {
  MY_ORDERS,
  MY_FILES,
  MY_ACCOUNT,
  ACCOUNT_DETAILS,
  SHIPPING_INFO,
  ORDER_DETAILS,
  TRACK_ORDER,
  PAYMENT_INFO,
} = ACCOUNT_TERMS;

const PROOFS = 'Proofs';

const MAIN_TABS = [
  {
    text: MY_ORDERS,
    id: 'orderDetails',
    TABS: [
      {
        text: ORDER_DETAILS,
        id: 'orderDetails',
        component: OrderDetails,
      },
    ],
  },
  {
    text: PROOFS,
    id: 'proofs',
    TABS: [
      {
        text: PROOFS,
        id: 'proofs',
        component: ProjectsTab,
      },
    ],
  },
  {
    text: MY_FILES,
    id: 'myFiles',
    TABS: [
      {
        text: MY_FILES,
        id: 'myFiles',
        component: MyFilesTab,
      },
    ],
  },
  {
    text: TRACK_ORDER,
    id: 'trackOrder',
    TABS: [
      {
        text: TRACK_ORDER,
        id: 'trackOrder',
        component: OrderTrackTab,
      },
    ],
  },
  {
    text: MY_ACCOUNT,
    id: 'account',
    TABS: [
      {
        text: ACCOUNT_DETAILS,
        id: 'account',
        component: UserAccountForm,
      },
      {
        text: SHIPPING_INFO,
        id: 'shipping',
        component: ShippingInfoForm,
      },
      {
        text: PAYMENT_INFO,
        id: 'payment',
        component: PaymentMethodTab,
      },
    ],
  },
];

const LOGOUT_TEXT = 'Logout';

const constants = {
  MAIN_TABS,
  LOGOUT_TEXT,
};

export default constants;
