const TERMS = {
  TITLE: 'Frequently Asked\nQuestions',
  FAQ_ITEMS: [
    {
      title: 'General',
      subtitles: [
        {
          subtitle: 'What are your hours?',
          description:
            'We are open to assist you Monday through Friday from 8am to 6pm and Saturdays from 10am to 4pm eastern standard time.',
        },
        {
          subtitle: 'Where are you located?',
          description:
            'Our headquarters is located in Boca Raton, Florida. Our exact address is 1000 Clint Moore Road Suite 208, Boca Raton, FL 33487.',
        },
        {
          subtitle: 'Can I come visit you?',
          description:
            'Absolutely! We would love for you to come visit us! Feel free to drop by anytime during our business hours.',
        },
        {
          subtitle: 'How long have you been in business?',
          description: 'We have been digitizing, enhancing and creating memories since 2005.',
        },
        {
          subtitle: 'Why should I trust you with my memories?',
          description:
            "Our team is passionate about digitizing, preserving, and creating. We treat each order with the utmost of care and can proudly say, we have never lost a single one of our client's memories.",
        },
        {
          subtitle: 'What shipping provider do you use?',
          description: 'UPS, USPS and Federal Express.',
        },
        {
          subtitle: 'Can I track my order?',
          description:
            'Yes! Please login to your ******* account and go to "My orders". Here you will be able to track the status of your order.',
        },
        {
          subtitle: 'Do you save my credit card information?',
          description: 'We will only save your credit card if you give us permission to.',
        },
        {
          subtitle: 'What does processing speed mean?',
          description:
            'The processing speed is how fast your order will be completed as soon as we receive your order.',
        },
        {
          subtitle: "What do I do if my credit card doesn't go though?",
          description:
            'You will have an opportunity to re-enter a different credit and change your billing address.',
        },
        {
          subtitle: "Help, my promo code isn't working?",
          description:
            'Please verify that your promo code is not expired and that it is available to what you are ordering. If you have any questions, please feel free to contact us for further assistance.',
        },
        {
          subtitle: 'How do I download my files from *******?',
          description:
            'If you have purchased a ******* subscription, simply login to you your ******* account and go to "My cloud". Here you can select and download your ditigized media.',
        },
        {
          subtitle: 'Does my digital download link expire?',
          description:
            'Yes, access to your photos will expire after your subscription period is up.  You will have an opportunity to extend your subscription and keep access to your photos. We offer yearly subscriptions where you can cancel at anytime.',
        },
      ],
    },
    {
      title: 'Media transfer',
      subtitles: [
        {
          subtitle: 'What can you do with my old home movies and photos?',
          description:
            'We can digitize your media into image files that can be loaded on to your choice of a data disk, USB drive, or *******.',
        },
        {
          subtitle: 'Do I need to label or order my media before sending them to you?',
          description: 'We digitize your media in the exact order you provide them to us.',
        },
        {
          subtitle: 'Do I receive my originals back after they are digitized?',
          description:
            'Yes, all your original media is returned to you at the time your order is complete.',
        },
        {
          subtitle: 'How long does the process take?',
          description:
            'All work is completed in 2 to 3 weeks. We do offer rushed services if you are too excited to wait. Simply select the rush processing option at checkout.',
        },
        {
          subtitle: 'What if some of my media are in really poor condition?',
          description:
            'Our advanced technology is the best in the industry to perform the highest quality scans possible. If you are still not satisfied you can always order a photo restoration.',
        },
        {
          subtitle: 'Do you enhance my memories?',
          description:
            'Yes! Enhancement is included free of charge for every legacy media digitization product we offer.',
        },
        {
          subtitle: 'Will all my media fit on one USB?',
          description:
            'Certainly! We will provide a ******* USB large enough to fit all your digitized media.',
        },
        {
          subtitle: 'What if I accidentally delete all my files on my USB drive?',
          description: 'We keep an archive of all your digital files for up to one year.',
        },
        {
          subtitle: 'How do I backup the digital files on my USB drive?',
          description:
            'Creating a backup of your memories is a great insurance policy in case anything happens to your original USB drive. Simply plug your USB drive into your computer and open the folder. Click the files you want to move and drag them to the desktop or the folder you would like to store them in.',
        },
        {
          subtitle: 'Does my digital download link expire?',
          description:
            'Yes, access to your media will expire after your subscription period is up. You will have an opportunity to extend your subscription and keep access to your media. We offer yearly subscriptions where you can cancel at anytime.',
        },
        {
          subtitle: 'How do I download my files from *******?',
          description:
            'If you have purchased a ******* subscription, simply login to you your ******* account and go to "My cloud". Here you can select and download your ditigized media.',
        },
        {
          subtitle: 'What can you do with my old video tapes?',
          description:
            'We digitize your media into digital files that can be loaded on to your choice of a DVD, USB drive, or *******.',
        },
        {
          subtitle: 'Do I need to label or order my media before sending them to you?',
          description:
            'We digitize and organize your media in the exact order you provide them to us.',
        },
      ],
    },
    {
      title: 'Enhance',
      subtitles: [
        {
          subtitle: 'What file format can I upload?',
          description: 'We accept jpeg, png, pdf, and bmp files.',
        },
        {
          subtitle: 'How do I scan my photo?',
          description:
            'We suggest scanning at the highest resolution possible (600dpi-1200dpi recommended). Do not add any enhancements from your software.',
        },
        {
          subtitle: 'What if I do not have a scanned copy of my photo?',
          description:
            'No problem! You can just order our easy 3-way ship kit that arrives right to your door to make mailing your photo safe and easy.',
        },
        {
          subtitle: 'Do I get my original photo back?',
          description:
            'Absolutely! After your photo is approved, we will return ship your original photo to you.',
        },
        {
          subtitle: 'Do you enhance the actual photo?',
          description:
            'No, we actually make a digital version of your photo and enhance it digitally.',
        },
        {
          subtitle: 'What is included?',
          description:
            'After you approve your enhanced photo, we will provide you a digital copy of your photo to download to your computer.',
        },
        {
          subtitle: 'What can you retouch?',
          description:
            'We can correct color and exposure, smooth wrinkles, remove blemishes, whiten teeth, sharpen eyes, remove wrinkles and marks from clothing, remove double chins, and slim and enhance features.',
        },
        {
          subtitle: 'What can you fix with your photo restoration service?',
          description:
            'We can repair discoloration, fading, sun bleaching, rips, tears, cracks, creases, marks, scratches, water damage, mold, fire damage, tape and photos stuck behind glass. ',
        },
        {
          subtitle: 'Can you fix my fuzzy photo?',
          description:
            'We work with the resolution and detail of the photo to enhance. We are very limited to what we can do regarding adding clarity or resolution and we may not achieve optimal results.',
        },
        {
          subtitle: 'Can I get new prints/ art of my enhanced photo?',
          description:
            'We specialize in high quality hand made photo art and prints. After your enhanced photo is approved, simply add your enhanced photo to our photo art page and select your favorite photo art.',
        },
        {
          subtitle: 'What is your usual turn-around time?',
          description:
            'We strive to have a proof of your retouched photo by 3 days from when we receive it.',
        },
        {
          subtitle: 'What if I need my photo enhanced in a hurry?',
          description:
            'Our standard enhancing service can take up to 3 business days to receive your digital proof. We do offer a 1 business day rush service for an additional $20. Simply select rush processing at checkout.',
        },
        {
          subtitle: 'How many revisions do I get with my ehnanced  photo?',
          description: 'We provide up to three revisions to your photo enhancement.',
        },
        {
          subtitle: 'What if I am still unhappy with the job you do?',
          description:
            'If you are still unhappy and have not yet approved your photo, we will be happy to provide a full refund.',
        },
        {
          subtitle: 'What do I get back?',
          description:
            'Our photo enhancing service includes a download link of your newly restored digital JPEG file from our *******. You can choose to order photo art and prints when you place your inital order or after you approve your digital enhancement.',
        },
        {
          subtitle: 'What is the difference between photo retouching & photo restoration?',
          description:
            'Photo retouching includes basic color correction and skin, hair and eye enhancement. A restoration repairs actual damage to your photo including severe fading, rips, tears, cracks, water damage, sun damage, and the like.',
        },
        {
          subtitle: 'What if I want to add color to my black and white photo?',
          description:
            'With our photo restoration service, we can colorize your photo if it is currently black and white or greyscale for and additional $20. Please check the "Colorize" option during ordering. Any suggestions on colors may be added in the customer notes section.',
        },
      ],
    },
    {
      title: 'Create',
      subtitles: [
        {
          subtitle: 'How long does photo art take to create?',
          description: 'Photo art takes 3-5 business days to create and ship out.',
        },
        {
          subtitle: 'Can I print photos off my cell phone?',
          description:
            'Yes! You can simply upload your photo right on our website to get started designing and printing your personalized photo art & prints.',
        },
        {
          subtitle: 'What file format can I upload?',
          description: 'We accept jpeg, png, pdf, and bmp files.',
        },
        {
          subtitle: 'How do I scan my photo?',
          description:
            'We suggest scanning at the highest resolution possible (600dpi-1200dpi recommended). ',
        },
        {
          subtitle: 'What if I do not have a scanned copy of my photo?',
          description:
            'No problem! You can just order our easy 3-way ship kit that arrives right to your door to make mailing your photo safe and easy.',
        },
        {
          subtitle: 'What if my photo is damaged?',
          description:
            'Good question! We offer photo restoration to bring your photos back to life! Please visit our photo restoraiton page for more information.',
        },
        {
          subtitle: 'Can you retouch my photo first?',
          description:
            'Yes! We offer professional photo retouching to make you look your best. Please visit our photo retouching page for more information.',
        },
        {
          subtitle: 'Can I personalize my photo art?',
          description: 'Yes! Our photo art comes with personalization options.',
        },
        {
          subtitle: "What makes *******'s print quality better than my home printer?",
          description:
            'We only use archival quality ink with museum quality paper to esure the best quality photo prints.',
        },
        {
          subtitle: 'What if I want something nicer than a paper print?',
          description:
            'Not a problem! We also print onto to multiple surfaces such as wood, canvas, metal, and even glass!',
        },
        {
          subtitle: 'Can I blow up my photo?',
          description:
            'We can, but its not something we advise. We cannot add resolution to where it doesn’t exist. The more we scale the photo, the less detail and more pixelated it will become.',
        },
        {
          subtitle: 'What if I want my photo gift wrapped?',
          description:
            'Simply select gift options during checkout. We can send to you or anyone you wish with your personalized message.',
        },
        {
          subtitle: 'What if my photo arrives damaged?',
          description:
            "Please contact us and include photos of the damage and we'll remake it for you right away.",
        },
        {
          subtitle: 'What if I am not happy with my photo art when I receive it?',
          description: 'Please contact us immedately so we can help to make it right.',
        },
      ],
    },
  ],
  SEO_TITLE: 'Frequently asked questions | ******* ',
  SEO_DESCRIPTION: 'Search for answers to all of your questions about transferring tapes, scanning photos, converting film, restoring photos and creating wall art.',
};

export default TERMS;
