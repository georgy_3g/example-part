import { HOME_TERMS, ENHANCE_TERMS, DIGITIZE_TERMS } from 'src/terms';
import ROUTES from 'src/constants/routes';
import {
  convertCassetteToCd,
  convertVhsTapesToDigitalDvd,
  photoScanningService,
  transferDigitalDeviceToDvd,
  transferFilmToDigitalDvd,
  photoartUrl,
  giftUrl
} from 'public';

import whatIncludeImageOne from 'public/img/photo-restoration/LOVE GUARANTEE/main.png';
import whatIncludeImageTwo from 'public/img/home/OUR_MISSION/DESKTOP/*******-our-mission.jpg';

import mobTransferDigitalDeviceToDvd from  'public/img/digitize/SUB_MENU/TABLET/digital-device-transfer-service.jpg';
import mobConvertCassetteToCd from  'public/img/digitize/SUB_MENU/TABLET/transfer-audio-cassettes-records-reels.jpg';
import mobTransferFilmToDigitalDvd from  'public/img/digitize/SUB_MENU/TABLET/convert-8mm-16mm-film-reels.jpg';
import mobConvertVhsTapesToDigitalDvd from  'public/img/digitize/SUB_MENU/TABLET/transfer-tapes-vhs-to-digital.jpg';
import mobPhotoScanningService from  'public/img/digitize/SUB_MENU/TABLET/photo-scanning-service.jpg';

import mobImgRetouching from  'public/img/enhance/SUB_MENU/TABLET/advanced-photo-retouching-service.jpg';
import mobImgRestoration from  'public/img/enhance/SUB_MENU/TABLET/old-damaged-photo-restoration-service.jpg';

import imgRetouching from  'public/img/enhance/SUB_MENU/DESKTOP/advanced-photo-retouching-service.jpg';
import imgRestoration from  'public/img/enhance/SUB_MENU/DESKTOP/old-damaged-photo-restoration-service.jpg';

import photoArtImg from  'public/img/home/SUB_MENU/DESKTOP/*******-create-photo-art.jpg';
import digitizing from  'public/img/home/SUB_MENU/DESKTOP/*******-transfer-my-old-media.jpg';
import retouching from  'public/img/home/SUB_MENU/DESKTOP/*******-enhance-my-photo.jpg';
import mobPhotoArtImg from  'public/img/home/SUB_MENU/TABLET/*******-create-photo-art.jpg';
import mobDigitizing from  'public/img/home/SUB_MENU/TABLET/*******-transfer-my-old-media.jpg';
import mobRetouching from  'public/img/home/SUB_MENU/TABLET/*******-enhance-my-photo.jpg';

import usa from  'public/img/icon-png/custom-usa.png';
import usaMiddle from  'public/img/icon-png/custom-usa@2x.png';
import usaFull from  'public/img/icon-png/custom-usa@3x.png';

import memorybox from  'public/img/icon-png/custom-memorybox.png';
import memoryboxMiddle from  'public/img/icon-png/custom-memorybox@2x.png';
import memoryboxFull from  'public/img/icon-png/custom-memorybox@3x.png';

const { RESTORATION_TITLE, RESTORATION_DESC, RETOUCHING_TITLE, RETOUCHING_DESC } = ENHANCE_TERMS;

const {
  TAPE_TITLE,
  TAPE_DESC,
  FILM_TITLE,
  FILM_DESC,
  AUDIO_TITLE,
  AUDIO_DESC,
  DIGITAL_TITLE,
  DIGITAL_DESC,
  PHOTO_TITLE,
  PHOTO_DESC,
} = DIGITIZE_TERMS;

const HOW_IT_WORKING = [
  {
    imgUrl: {
      full: usaFull,
      middle: usaMiddle,
      defaultImg: usa,
    },
    title: 'American made',
  },
  {
    imgUrl: photoartUrl,
    title: 'Handcrafted',
  },
  {
    imgUrl: {
      full: memoryboxFull,
      middle: memoryboxMiddle,
      defaultImg: memorybox,
    },
    title: 'Fun experience',
  },
  {
    imgUrl: giftUrl,
    title: 'Unique gifts',
  },
];

const {
  digitize,
  enhance,
  photoArt,
  photoRestoration,
  photoRetouching,
  tapeTransfer,
  filmTransfer,
  audioTransfer,
  digitalTransfer,
  photoTransfer,
  ourMission,
  giftGuide,
} = ROUTES;

const { DIGITIZE_DESC, ENHANCE_DESC, CREATE_DESC } = HOME_TERMS;

const DATA_CARD = [
  {
    title: 'Restore your photos',
    desc: ENHANCE_DESC,
    textBtn: 'Start restoring',
    imgUrl: { defaultImg: retouching },
    mobileImgUrl: { defaultImg: mobRetouching },
    imgAlt: 'Restore, Enhance and Repair Old Photos',
    url: enhance,
  },
  {
    title: 'Create photo art',
    desc: CREATE_DESC,
    textBtn: 'Create art',
    imgUrl: { defaultImg: photoArtImg },
    mobileImgUrl: { defaultImg: mobPhotoArtImg },
    imgAlt: 'Personalized Canvas and Photo Frames',
    url: photoArt,
  },
  {
    title: 'Digitize old media',
    desc: DIGITIZE_DESC,
    textBtn: 'Digitize now',
    imgUrl: { defaultImg: digitizing },
    mobileImgUrl: { defaultImg: mobDigitizing },
    imgAlt: 'Digital Media Transfer Services',
    url: digitize,
  },
];

const ENHANCE_DATA_CARDS = [
  {
    title: RESTORATION_TITLE,
    desc: RESTORATION_DESC,
    textBtn: 'Restore photos',
    imgUrl: { defaultImg: imgRestoration },
    mobileImgUrl: { defaultImg: mobImgRestoration },
    imgAlt: RESTORATION_TITLE,
    url: photoRestoration,
  },
  {
    title: RETOUCHING_TITLE,
    desc: RETOUCHING_DESC,
    textBtn: 'Retouch photos',
    imgUrl: { defaultImg: imgRetouching },
    mobileImgUrl: { defaultImg: mobImgRetouching },
    imgAlt: RETOUCHING_TITLE,
    url: photoRetouching,
  },
];

const DIGITIZE_DATA_CARDS = [
  {
    title: PHOTO_TITLE,
    desc: PHOTO_DESC,
    textBtn: 'Scan now',
    imgUrl: photoScanningService,
    mobileImgUrl: { defaultImg: mobPhotoScanningService },
    imgAlt: PHOTO_TITLE,
    url: photoTransfer,
  },
  {
    title: TAPE_TITLE,
    desc: TAPE_DESC,
    textBtn: 'Transfer now',
    imgUrl: convertVhsTapesToDigitalDvd,
    mobileImgUrl: { defaultImg: mobConvertVhsTapesToDigitalDvd },
    imgAlt: TAPE_TITLE,
    url: tapeTransfer,
  },
  {
    title: FILM_TITLE,
    desc: FILM_DESC,
    textBtn: 'Convert now',
    imgUrl: transferFilmToDigitalDvd,
    mobileImgUrl: { defaultImg: mobTransferFilmToDigitalDvd },
    imgAlt: FILM_TITLE,
    url: filmTransfer,
  },
  {
    title: AUDIO_TITLE,
    desc: AUDIO_DESC,
    textBtn: 'Digitize now',
    imgUrl: convertCassetteToCd,
    mobileImgUrl: { defaultImg: mobConvertCassetteToCd },
    imgAlt: AUDIO_TITLE,
    url: audioTransfer,
  },
  {
    title: DIGITAL_TITLE,
    desc: DIGITAL_DESC,
    textBtn: 'Organize now',
    imgUrl: transferDigitalDeviceToDvd,
    mobileImgUrl: { defaultImg: mobTransferDigitalDeviceToDvd },
    imgAlt: DIGITAL_TITLE,
    url: digitalTransfer,
  },
];

const BANNER_TITLE = 'Preserve your family memories & create extraordinary.';
const BANNER_DESC =
  '******* are experts in enhancing & restoring your damaged old photos to perfection. We make it easy\nfor you to design one-of-a-kind, premium photo art & gifts from your favorite pictures. Our team of archivists\nand trained technicians can also professionally digitize your photo albums, home videos & movie reels to a\npersonalized USB & secure ******* storage.';
const WE_DO_TITLE = 'Create unique personalized keepsakes from \nyour favorite moments.';
const WE_DO_SUB_TITLE =
  'No matter which service you choose, our passionate team will \npreserve and present your memories for you to cherish *******.';
const SEO_DESCRIPTION =
  'Enhance your favorite photo with the worlds top certified photoshop artists. Create unique personalized photo frames with our easy gift designer.';
const SEO_TITLE = 'Online Photo Enhancement and Picture Framing | *******';

const NEW_INFO_WIDGET_BUTTON_TEXT = 'Gift Ideas';

const WHAT_INCLUDED_TITLE = 'Design & personalize modern\nwall art from your favorite\nfamily photos.';
const WHAT_INCLUDED_TEXT =
  '******* is a group of passionate photo editors, designers, master artisans and craftsmen.\nWe believe that enhancing your photo is only the first step. Your cherished photos are special and should be printed & displayed in your home to enjoy every day.\n\nBrowse our exclusive collection of ******* Art and get started designing your masterpiece today.';
const WHAT_INCLUDE_BUTTON_LINK = photoArt;
const WHAT_INCLUDE_BUTTON_TEXT = 'Start creating';

const WHAT_INCLUDED_TITLE_TWO = '******* mission.';
const WHAT_INCLUDED_TEXT_TWO =
  'We are a dedicated team of archivists, designers and\ncreators passionate about helping you enhance your\nfamily memories and make unique personalized\nkeepsakes to treasure *******.\n\nThis is our mission and we are excited to share it with\nyou.';
const WHAT_INCLUDE_BUTTON_LINK_TWO = ourMission;
const WHAT_INCLUDE_BUTTON_TEXT_TWO = 'Get to know us';


const WHAT_WE_DO_TITLE_ENHANCE = 'Give new life to your favorite photo.';
const WHAT_WE_DO_SUBTITLE_ENHANCE = '******* world class artists enhance & retouch your favorite photos to perfection. All photo enhancement\nis done by hand, by real artists. Our goal is to make your precious photos look better than you ever imagined they\ncould. Every photo enhancement includes work by a senior artist, easy photo proofing with unlimited revisions &\nfree lifetime cloud storage of your restored photos to download and share with family and friends.';


const WHAT_INCLUDE_SLIDER_LIST = [
  {
    id: 'what-include-restoration-1',
    url: whatIncludeImageOne,
    title: 'Easy online ordering.',
    text: 'Simply upload your scanned digital photo from your computer, tablet or phone or if you need help scanning your photo, select our easy scan ship kit. Our 3 way secure ship kit is delivered right to your door with everything you need to let our professionals scan your photos for you.',
    alt: 'Restore or retouch photo service - easy ordering',
  },
];

const WHAT_INCLUDE_SLIDER_LIST_MOB = [
  {
    id: 'what-include-restoration-1',
    url: whatIncludeImageOne,
    title: 'Easy online ordering.',
    text: 'Simply upload your scanned digital photo from your computer, tablet or phone or if you need help scanning your photo, select our easy scan ship kit. Our 3 way secure ship kit is delivered right to your door with everything you need to let our professionals scan your photos for you.',
    alt: 'Restore or retouch photo service - easy ordering',
  },
];

const WHAT_INCLUDE_SLIDER_LIST_TWO = [
  {
    id: 'what-include-restoration-1',
    url: whatIncludeImageTwo,
    title: 'Easy online ordering.',
    text: 'Simply upload your scanned digital photo from your computer, tablet or phone or if you need help scanning your photo, select our easy scan ship kit. Our 3 way secure ship kit is delivered right to your door with everything you need to let our professionals scan your photos for you.',
    alt: 'Restore or retouch photo service - easy ordering',
  },
];

const WHAT_WE_DO_TITLE_DIGITIZE = 'Superior quality photo scanning & home movie digitization\nis only the beginning.'
const WHAT_WE_DO_DESC_DIGITIZE = '******* has helped over 1 million customer in the USA since 2007 digitize, enhance & preserve their most\nprecious family memories. Our goal from day one was to make it easy to transfer your precious family memories\ninto modernized digital keepsakes for you and future generations to enjoy *******. Explore our premium photo\nscanning, tape transfer, film digitization & audio conversion below & get started transferring today.';

const WHAT_INCLUDED_RIGHT_TITLE = 'Design & create the most\nmemorable photo art gift.';
const WHAT_INCLUDED_RIGHT_DESC = 'They have never been gifted something like this before.\n\nAdd a special photo to our online photo frame design\nsoftware & create a unique gift for someone you love.\nWe have beautiful picture frames for every style home.\nChoose between modern picture frames, nautical wall\nart, rustic frames and more.\n\nNew gift hero’s made daily.';
const WHAT_INCLUDED_RIGHT_BUTTON_LINK = giftGuide;

const constants = {
  DATA_CARD,
  BANNER_TITLE,
  BANNER_DESC,
  WE_DO_TITLE,
  HOW_IT_WORKING,
  SEO_DESCRIPTION,
  WE_DO_SUB_TITLE,
  SEO_TITLE,
  NEW_INFO_WIDGET_BUTTON_TEXT,
  ENHANCE_DATA_CARDS,
  WHAT_INCLUDED_TITLE,
  WHAT_INCLUDED_TEXT,
  WHAT_INCLUDE_BUTTON_LINK,
  WHAT_INCLUDE_BUTTON_TEXT,
  WHAT_INCLUDE_BUTTON_LINK_TWO,
  WHAT_INCLUDE_BUTTON_TEXT_TWO,
  WHAT_INCLUDED_TITLE_TWO,
  WHAT_INCLUDED_TEXT_TWO,
  DIGITIZE_DATA_CARDS,
  WHAT_WE_DO_TITLE_ENHANCE,
  WHAT_WE_DO_SUBTITLE_ENHANCE,
  WHAT_WE_DO_TITLE_DIGITIZE,
  WHAT_WE_DO_DESC_DIGITIZE,
  WHAT_INCLUDE_SLIDER_LIST,
  WHAT_INCLUDE_SLIDER_LIST_MOB,
  WHAT_INCLUDED_RIGHT_TITLE,
  WHAT_INCLUDED_RIGHT_DESC,
  WHAT_INCLUDED_RIGHT_BUTTON_LINK,
  WHAT_INCLUDE_SLIDER_LIST_TWO,
};

export default constants;
