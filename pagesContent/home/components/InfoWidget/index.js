import React from 'react';
import PropTypes from 'prop-types';
import DICTIONARY from 'src/constants/dictionary';
import bem from 'src/utils/bem';
import useMobileStyle from 'src/utils/useMobileStyle';
import { connect } from 'react-redux';
import Image from 'next/image';
import LazyLoad from 'react-lazyload';
import dynamic from 'next/dynamic';
import { isMobileDeviceSelector } from 'src/redux/auth/selectors';
import styles from './index.module.scss';

const ColorButton = dynamic(() => import('src/components/buttons/ColorButton'));

const b = bem('info-widget', styles);

const { HOW_IT_WORKS } = DICTIONARY;

const defaultProps = {
  title: '',
  desc: '',
  bgImg: {},
  tabletBgImg: {},
  mobileBgImg: {},
  learnMoreLink: '',
  isMobileDevice: true,
};

const propTypes = {
  title: PropTypes.string,
  desc: PropTypes.string,
  bgImg: PropTypes.shape({}),
  tabletBgImg: PropTypes.shape({}),
  mobileBgImg: PropTypes.shape({}),
  learnMoreLink: PropTypes.string,
  isMobileDevice: PropTypes.bool,
};

const InfoWidget = (props) => {
  const { title, desc, bgImg, learnMoreLink, mobileBgImg, tabletBgImg, isMobileDevice } = props;

  const isMobile = useMobileStyle(480, isMobileDevice);
  const isTablet = useMobileStyle(1024, isMobileDevice);

  const getIMage = () => {
    if (isMobile) {
      return mobileBgImg;
    }
    if (isTablet) {
      return tabletBgImg;
    }
    return bgImg;
  };

  return (
    <div className={b()}>
      <div className={b('img-wrapper')}>
        <div className={b('stripe', { right: true })} />
        <div className={b('img-container')}>
          <LazyLoad offset={100}>
            <Image
              className={b('image', { mix: 'lazyload' })}
              data-src={getIMage()}
              src={getIMage()}
              layout="fill"
              alt="digitize your family memories"
            />
          </LazyLoad>
          <div className={b('container')}>
            <h2 className={b('title')}>{title}</h2>
            <div className={b('desc')}>{desc}</div>
            <div className={b('btn-container')}>
              <a className={b('link')} href={learnMoreLink}>
                <ColorButton text={HOW_IT_WORKS} />
              </a>
            </div>
          </div>
        </div>
        <div className={b('stripe', { left: true })} />
      </div>
    </div>
  );
}

InfoWidget.propTypes = propTypes;
InfoWidget.defaultProps = defaultProps;

const stateProps = (state) => ({
  isMobileDevice: isMobileDeviceSelector(state),
});

export default connect(stateProps, null)(InfoWidget);
