import ROUTES from 'src/constants/routes';

const { home, payment, account } = ROUTES;

const TERMS = {
  POPUP_NAME: {
    cart: 'cart',
    login: 'login',
    signUp: 'signUp',
    forgotPass: 'forgotPass',
    menu: 'menu',
  },
  LOGIN: 'Login',
  EXISTING_USER: 'Existing user',
  REGISTER_NEW_ACCOUNT: 'Register new account',
};
const LINKS = {
  home,
  payment,
  account,
};

const constants = { TERMS, LINKS };
export default constants;
