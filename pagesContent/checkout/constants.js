const TERMS = {
  CHECKOUT: 'Secure Checkout',
  CART: 'Cart',
  YOUR_PRODUCTS: 'Your products',
  PROCESSING_SPEED: 'Processing speed',
  NEED_QUICK: 'Need your order quickly?',
  STANDARD_PROCESSING: 'Standard processing time',
  RUSH_PROCESSING: 'Rush Processing',
  RUSH_PRICE: '+ 25 %',
  FREE: 'Included',
  ORDER_SUMMARY: 'Order summary',
  STANDARD: 'standard',
  RUSH: 'rush',
  ENHANCE_PRINTS: 'enhance_prints',
  PERSONALIZE: 'personalize',
  *******_SAFE: '*******_safe',
  ENHANCE_MY_TRANSFER: 'enhance_my_transfer',
  PHOTO_ART: 'photo_art',
};

export default TERMS;
