const TITLE = 'Hear from your new best friends.';
const TITLE_DESC =
  'Stay connected with us as we explore stories, thoughts and insights from our ******* family and friends.';
const LOAD_MORE = 'Loading more';
const READ_ARTICLE = 'Read article';
const VIEW_BLOG = 'View blog';
const LOAD_SCROLLING = 0.2;
const LIMIT = 6;
const SEO = {
  title: 'Latest News, Stories and How-tos | *******',
  description:
    'Follow ******* blog to learn the latest news on best practices to help you create something truly extraordinary with your memories.',
};
const constants = {
  TITLE,
  TITLE_DESC,
  LOAD_MORE,
  READ_ARTICLE,
  LOAD_SCROLLING,
  LIMIT,
  SEO,
  VIEW_BLOG,
};
export default constants;
