import arhival from  'public/img/icon-png/custom-arhival.png';
import arhivalMiddle from  'public/img/icon-png/custom-arhival@2x.png';
import arhivalFull from  'public/img/icon-png/custom-arhival@3x.png';

import family from  'public/img/icon-png/custom-family.png';
import familyMiddle from  'public/img/icon-png/custom-family@2x.png';
import familyFull from  'public/img/icon-png/custom-family@3x.png';

import photoart from  'public/img/icon-png/custom-photoart.png';
import photoartMiddle from  'public/img/icon-png/custom-photoart@2x.png';
import photoartFull from  'public/img/icon-png/custom-photoart@3x.png';

import sizeErrorMailIcon from  'public/img/photoArt/learn-more-popup/SizeErrorMailIcon.png';
import sizeErrorResizeIcon from  'public/img/photoArt/learn-more-popup/SizeErrorResizeIcon.png';
import sizeErrorCroppingIcon from  'public/img/photoArt/learn-more-popup/SizeErrorCroppingIcon.png';

import gift from  'public/img/icon-png/custom-gift.png';
import giftMiddle from  'public/img/icon-png/custom-gift@2x.png';
import giftFull from  'public/img/icon-png/custom-gift@3x.png';

import shipping from  'public/img/icon-png/custom-shipping.png';
import shippingMiddle from  'public/img/icon-png/custom-shipping@2x.png';
import shippingFull from  'public/img/icon-png/custom-shipping@3x.png';

import tin from  'public/img/shared/photo-scanning-service-deliverable-usb-personalize-blank-530x360.jpg';

import cunstomChanges from  'public/img/enhance/custom-changes@3x.png';
import cunstomChangesMiddle from  'public/img/enhance/custom-changes@2x.png';
import cunstomChangesDefaultImg from  'public/img/enhance/custom-changes.png';

import cunstomHand from  'public/img/enhance/custom-byhand@3x.png';
import cunstomHandMiddle from  'public/img/enhance/custom-byhand@2x.png';
import cunstomHandDefaultImg from  'public/img/enhance/custom-byhand.png';

import cunstomNoobligation from  'public/img/enhance/custom-noobligation@3x.png';
import cunstomNoobligationMiddle from  'public/img/enhance/custom-noobligation@2x.png';
import cunstomNoobligationDefaultImg from  'public/img/enhance/custom-noobligation.png';

import subConvertCassetteToCd from  'public/img/digitize/SUB_MENU/DESKTOP/transfer-audio-cassettes-records-reels.jpg';
import subConvertVhsTapesToDigitalDvd from  'public/img/digitize/SUB_MENU/DESKTOP/transfer-tapes-vhs-to-digital.jpg';
import subPhotoScanningService from  'public/img/digitize/SUB_MENU/DESKTOP/photo-scanning-service.jpg';
import subTransferFilmToDigitalDvd from  'public/img/digitize/SUB_MENU/DESKTOP/convert-8mm-16mm-film-reels.jpg';
import subTransferDigitalDeviceToDvd from  'public/img/digitize/SUB_MENU/DESKTOP/digital-device-transfer-service.jpg';

import subConvertCassetteToCdMob from  'public/img/digitize/SUB_MENU/TABLET/transfer-audio-cassettes-records-reels.jpg';
import subConvertVhsTapesToDigitalDvdMob from  'public/img/digitize/SUB_MENU/TABLET/transfer-tapes-vhs-to-digital.jpg';
import subPhotoScanningServiceMob from  'public/img/digitize/SUB_MENU/TABLET/photo-scanning-service.jpg';
import subTransferFilmToDigitalDvdMob from  'public/img/digitize/SUB_MENU/TABLET/convert-8mm-16mm-film-reels.jpg';
import subTransferDigitalDeviceToDvdMob from  'public/img/digitize/SUB_MENU/TABLET/digital-device-transfer-service.jpg';

import box from  'public/img/icon-png/custom-memorybox.png';
import boxMiddle from  'public/img/icon-png/custom-memorybox@2x.png';
import boxFull from  'public/img/icon-png/custom-memorybox@3x.png';

import quality from  'public/img/icon-png/custom-quality.png';
import qualityMiddle from  'public/img/icon-png/custom-quality@2x.png';
import qualityFull from  'public/img/icon-png/custom-quality@3x.png';

import closingQuote from  'public/img/mission/closing-quote.png';
import openingQuote from  'public/img/mission/opening-quote.png';

const boxUrl = {
  full: boxFull,
  middle: boxMiddle,
  defaultImg: box,
};

const qualityUrl = {
  full: qualityFull,
  middle: qualityMiddle,
  defaultImg: quality,
};

const arhivalUrl = {
  full: arhivalFull,
  middle: arhivalMiddle,
  defaultImg: arhival,
};

const familyUrl = {
  full: familyFull,
  middle: familyMiddle,
  defaultImg: family,
};

const photoartUrl = {
  full: photoartFull,
  middle: photoartMiddle,
  defaultImg: photoart,
};

const giftUrl = {
  full: giftFull,
  middle: giftMiddle,
  defaultImg: gift,
};

const shippingUrl = {
  full: shippingFull,
  middle: shippingMiddle,
  defaultImg: shipping,
};

const tinUrl = {
  full: tin,
  middle: tin,
  defaultImg: tin,
};

const customHandUrl = {
  full: cunstomHand,
  middle: cunstomHandMiddle,
  defaultImg: cunstomHandDefaultImg,
};

const customChangesUrl = {
  full: cunstomChanges,
  middle: cunstomChangesMiddle,
  defaultImg: cunstomChangesDefaultImg,
};

const customNoobligationUrl = {
  full: cunstomNoobligation,
  middle: cunstomNoobligationMiddle,
  defaultImg: cunstomNoobligationDefaultImg,
};

const convertCassetteToCd = {
  full: subConvertCassetteToCd,
  middle: subConvertCassetteToCdMob,
  defaultImg: subConvertCassetteToCdMob,
};

const convertVhsTapesToDigitalDvd = {
  full: subConvertVhsTapesToDigitalDvd,
  middle: subConvertVhsTapesToDigitalDvdMob,
  defaultImg: subConvertVhsTapesToDigitalDvdMob,
};

const photoScanningService = {
  full: subPhotoScanningService,
  middle: subPhotoScanningServiceMob,
  defaultImg: subPhotoScanningServiceMob,
};

const transferDigitalDeviceToDvd = {
  full: subTransferDigitalDeviceToDvd,
  middle: subTransferDigitalDeviceToDvdMob,
  defaultImg: subTransferDigitalDeviceToDvdMob,
};

const transferFilmToDigitalDvd = {
  full: subTransferFilmToDigitalDvd,
  middle: subTransferFilmToDigitalDvdMob,
  defaultImg: subTransferFilmToDigitalDvdMob,
};

export {
  arhivalUrl,
  familyUrl,
  photoartUrl,
  giftUrl,
  shippingUrl,
  tinUrl,
  customHandUrl,
  customChangesUrl,
  customNoobligationUrl,
  convertCassetteToCd,
  convertVhsTapesToDigitalDvd,
  photoScanningService,
  transferDigitalDeviceToDvd,
  transferFilmToDigitalDvd,
  boxUrl,
  qualityUrl,
  sizeErrorMailIcon,
  sizeErrorResizeIcon,
  sizeErrorCroppingIcon,
  closingQuote,
  openingQuote,
};
