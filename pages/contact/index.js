import React from 'react';
import { NextSeo } from 'next-seo';
import bem from 'src/utils/bem';
import dynamic from "next/dynamic";
import styles from './index.module.scss';

const Navigation = dynamic(() => import('src/components/shared/Navigation'));
const GetInTouch = dynamic(() => import('src/components/shared/GetInTouch'));
const ComeVisitUs = dynamic(() => import('src/components/shared/ComeVisitUs'));
const Footer = dynamic(() => import('src/components/shared/Footer'));

const b = bem('contact', styles);

const SEO_TITLE = 'Contact Us';
const SEO_DESCRIPTION = 'Call us, write us or visit our headquarters in Boca Raton, Florida';

const Contact = () => (
  <>
    <NextSeo
      title={SEO_TITLE}
      description={SEO_DESCRIPTION}
      canonical="https://******/contact/"
      languageAlternates={[
        {
          hrefLang: 'en-us',
          href: 'https://******/contact/',
        },
      ]}
    />
    <Navigation />
    <main className={b()}>
      <GetInTouch isConnectPage />
      <div className={b('come-visit-us-wrap')}>
        <ComeVisitUs />
      </div>
      <div className={b('left-rect')} />
      <div className={b('right-rect')} />
      <div className={b('right-small-rect')} />
    </main>
    <Footer />
  </>
);

export default Contact;
