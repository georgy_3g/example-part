import React from 'react';
import PropTypes from 'prop-types';
import formatUrl from 'src/utils/formatUrl';
import ROUTES from 'src/constants/routes';
import Image from 'next/image';
import constants from '../../pagesContent/blogList/constants';

const { READ_ARTICLE } = constants;
const { blog } = ROUTES;

const defaultProps = {
  blogs: {},
  bem: () => {},
};

const propTypes = {
  blogs: PropTypes.shape({
    data: PropTypes.arrayOf(PropTypes.shape({})),
  }),
  bem: PropTypes.func,
};

function FeaturedArticle(props) {
  const {
    blogs: { data },
    bem,
  } = props;
  return (
    <>
      {data.map(item => (item.isFeatured && (
        <div className={bem('featured-article-wrapper')} key={item.id}>
          <div className={bem('featured-article')}>
            <Image
              className={bem('featured-image')}
              src={item.previewImageUri}
              alt={item.title}
              width='100%'
              height='100%'
            />
            <div className={bem('featured-info')}>
              <p className={bem('featured-category')}>{item.category}</p>
              <p className={bem('featured-title')}>{item.title}</p>
              <a className={bem('link')} href={`${blog}${formatUrl(item.titleSeo)}`}>
                <div className={bem('article-link')}>
                  <button className="btn" type="button">{READ_ARTICLE}</button>
                </div>
              </a>
            </div>
          </div>
        </div>
      )))}
    </>
  );
}

FeaturedArticle.propTypes = propTypes;
FeaturedArticle.defaultProps = defaultProps;

export default FeaturedArticle;
