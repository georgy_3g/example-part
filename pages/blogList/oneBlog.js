import React, { useEffect, useRef, useState } from 'react';
import PropTypes from 'prop-types';
import formatUrl from 'src/utils/formatUrl';
import Image from 'next/image';
import constants from '../../pagesContent/blogList/constants';

const { READ_ARTICLE, VIEW_BLOG } = constants;

const defaultProps = {
  item: {},
  bem: () => {},
  selectedCategory: {},
};

const propTypes = {
  item: PropTypes.shape({
    id: PropTypes.number,
    titleSeo: PropTypes.string,
    title: PropTypes.string,
    category: PropTypes.string,
    previewImageUri: PropTypes.string,
    description: PropTypes.string,
    summeryDescription: PropTypes.string,
    isCategory: PropTypes.bool,
    imageUrl: PropTypes.string,
    name: PropTypes.string,
    url: PropTypes.string,
  }),
  bem: PropTypes.func,
  selectedCategory: PropTypes.shape({
    name: PropTypes.string,
  }),
};

function OneBlog(props) {
  const [href, setHref] = useState('');
  const { item, bem } = props;
  const articleInfo = useRef();

  const {
    id,
    titleSeo,
    title,
    category,
    summeryDescription,
    previewImageUri,
    imageUrl,
    isCategory,
    name,
    url,
  } = item;

  useEffect(() => {
    setHref(window.location.origin);
  }, []);

  const cardTitle = isCategory ? name : title;
  const cardDescription = isCategory ? summeryDescription : category;
  const textButton = isCategory ? VIEW_BLOG : READ_ARTICLE;
  const link = isCategory
  ? `${href}/blog/${formatUrl(url, true)}/`
  : `${href}/blog/${formatUrl(titleSeo, true)}/`;
  const textOverflow = articleInfo.current && articleInfo.current.clientWidth === 334 ? 60 : 130;

  return (
    <a className={bem('article')} href={link}>
      <div className={bem('article-image')}>
        <Image
          className={bem('article-image')}
          src={previewImageUri || imageUrl}
          alt="article-bg"
          layout="fill"
        />
      </div>
      <div className={bem('article-info')} ref={articleInfo}>
        <div className={bem('text-wrapper')}>
          {cardTitle.length < 130
            ? <span className={bem('article-title')}>{cardTitle}</span>
            : <span className={bem('article-title')}>{`${cardTitle.slice(0, 120)} ...`}</span>}
          {cardDescription.length < textOverflow
            ? <p className={bem('article-category')}>{cardDescription}</p>
            : <p className={bem('article-category')}>{`${cardDescription.slice(0, textOverflow - 10)} ...`}</p>}
        </div>
        <div className={bem('article-link')} key={id}>
          <span className={bem('link-text')}>{textButton}</span>
        </div>
      </div>
    </a>
  );
}

OneBlog.propTypes = propTypes;
OneBlog.defaultProps = defaultProps;

export default OneBlog;
