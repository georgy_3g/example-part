import React, { Component } from 'react';
import { NextSeo } from 'next-seo';
import PropTypes from 'prop-types';
import preloadBlogs from 'src/utils/preloadBlogs';
import formatUrl from 'src/utils/formatUrl';
import { wrapper } from 'src/redux/store';
import getLinkBreadCrumbs from 'src/utils/getLinkBreadCrumbs';
import axios from 'axios';
import URL from 'src/redux/urls';
import { apiCall } from 'src/redux/utils';
import dynamic from 'next/dynamic';
import { getPromoSection } from 'src/redux/promo/actions';
import { loadArticle, loadBlogs, loadCategories } from 'src/redux/blogList/actions';
import Content from './content';
import constants from '../../pagesContent/blogList/constants';
import {getBreadCrumbBlogList, getSeoBlog} from "../../src/utils/getSeoStructureData";

const Navigation = dynamic(() => import('src/components/shared/Navigation'));
const Footer = dynamic(() => import('src/components/shared/Footer'));
const BreadCrumbs = dynamic(() => import('src/components/shared/BreadCrumbs'));
const Article = dynamic(() => import('src/components/shared/Article'));

const { SEO } = constants;

const defaultProps = {
  params: {},
  selectedCategory: {},
  isCategory: false,
  categoryList: [],
  article: {},
};

const propTypes = {
  params: PropTypes.shape({}),
  selectedCategory: PropTypes.shape({
    seoTitle: PropTypes.string,
    seoDescription: PropTypes.string,
  }),
  article: PropTypes.shape({
    titleSeo: PropTypes.string,
    descriptionSeo: PropTypes.string,
  }),
  isCategory: PropTypes.bool,
  categoryList: PropTypes.arrayOf(),
};

export default class BlogList extends Component {
  static getInitialProps = wrapper.getInitialPageProps(store => async ({ res, req }) => {
    const { dispatch } = store;
    const { params } = req || {};
    const { title } = params || {};

    await dispatch(getPromoSection({ key: 'blog' }));
    const {
      data: { data },
    } = await apiCall({ type: 'GET', url: URL.BLOG_CATEGORIES_LIST });
    await dispatch(loadCategories(data));


    if (title) {
      const regexCategory = /-/g;
      const resultCategory = title.replace(regexCategory, ' ');

      const regexArticle = /[a-z]/g;
      const resultArticle = title.toLowerCase().match(regexArticle).join('');

      const selectedCategory = data.find(({ url }) => url === resultCategory) || {}
      const isCategory = Boolean(Object.keys(selectedCategory).length);
      if (isCategory) {
        const payload = await preloadBlogs(URL.BLOG_LIST, selectedCategory.id );
        await dispatch(loadBlogs(payload));
        return {
          categoryList: data,
          params,
          selectedCategory,
          isCategory,
        }
      }
      const article = await axios.get(`${URL.BLOG_LIST}/${resultArticle}`)
      .then(({ data: { data: dataArticle } }) => dataArticle)
      .catch(() => {
        if(res) {
          res.redirect('/404');
          res.end();
          return {};
        }
        return {};
      });
      await dispatch(loadArticle(article));
      return {
        categoryList: data,
        params,
        article,
        isCategory
      }
    }
    return {}
  })

  render() {
    const {
      categoryList,
      selectedCategory,
      article,
      selectedCategory: {
        seoTitle,
        seoDescription,
      },
      isCategory,
      article: {
        titleSeo,
        descriptionSeo,
      },
      params,
    } = this.props;
    const { title } = params || {};
    const canonicalUrl = title ? `https://*******.com/blog/${formatUrl(title || '', true)}/` : `https://*******.com/blog/${formatUrl(title || '', true)}`;
    const titleSEO = isCategory || !title ? seoTitle : titleSeo;
    const descriptionSEO = isCategory || !title ? seoDescription : descriptionSeo;
    const breadCrumbList = getLinkBreadCrumbs({
      category: selectedCategory,
      article,
      categoryList,
    });
    const seoBlogList = getBreadCrumbBlogList(breadCrumbList)
    return (
      <>
        <NextSeo
          title={titleSEO || SEO.title}
          description={descriptionSEO || SEO.description}
          canonical={canonicalUrl}
          languageAlternates={[{
            hrefLang: 'en-us',
            href: canonicalUrl,
          }]}
        />
        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{ __html: JSON.stringify(seoBlogList) }}
          async
        />
        {
          !isCategory && Object.keys(article).length > 0 ? (
            <script
              type="application/ld+json"
              dangerouslySetInnerHTML={{ __html: JSON.stringify(getSeoBlog(article, canonicalUrl)) }}
              async
            />
          )
            : null
        }
        <Navigation />
        <BreadCrumbs links={breadCrumbList} />
        {
          isCategory || !title
          ? <Content isCategory={isCategory} selectedCategory={selectedCategory} />
          : <Article article={article} />
        }
        <Footer blog={false} />
      </>
    );
  }
}

BlogList.propTypes = propTypes;
BlogList.defaultProps = defaultProps;
