import React, { Component } from 'react';
import PropTypes from 'prop-types';
import bem from 'src/utils/bem';
import { connect } from 'react-redux';
import { categoriesBlog, getBlogList, isBlogsLoading } from 'src/redux/blogList/selectors';
import { promoSectionSelector } from 'src/redux/promo/selectors';
import { addBlogs } from 'src/redux/blogList/actions';
import { getPromoSection } from 'src/redux/promo/actions';
import constants from '../../pagesContent/blogList/constants';
import Articles from './articles';
import styles from './index.module.scss';

const { TITLE, TITLE_DESC } = constants;

const b = bem('blog-list-content', styles);

const defaultProps = {
  blogs: {},
  moreArticles: () => {  },
  selectedCategory: {},
  categories: [],
  promoSection: {},
  isCategory: false,
};

const propTypes = {
  blogs: PropTypes.shape({
    total: PropTypes.string,
    data: PropTypes.arrayOf(PropTypes.shape({})),
  }),
  moreArticles: PropTypes.func,
  categories: PropTypes.arrayOf(),
  selectedCategory: PropTypes.shape({
    id: PropTypes.number,
    name: PropTypes.string,
    description: PropTypes.string,
    imageUrl: PropTypes.string,
  }),
  promoSection: PropTypes.shape({
    id: PropTypes.number,
  }),
  isCategory: PropTypes.bool,
};

class Content extends Component {
  componentDidMount() {
    const {
      moreArticles,
      blogs,
      categories,
      isCategory,
      selectedCategory
    } = this.props;
    const listCard = isCategory ?  blogs : { data: categories, total: categories.length };
    const { total } = listCard;
    moreArticles({
      limit: total,
      offset: 0,
      categoryId: selectedCategory.id });
  }

  render() {
    const {
      blogs,
      categories,
      selectedCategory,
      isCategory,
    } = this.props;
    const formatCategories = categories.map((item) => ({ ...item, isCategory: true }));
    const data = isCategory ?  blogs : { data: formatCategories };
    const title = isCategory ?  selectedCategory.name : TITLE
    const description = isCategory ?  selectedCategory.description : TITLE_DESC
    return (
      <main
        className={b()}
        ref={(e) => {
          this.refBlogList = e;
        }}
      >
        <div className={b('title-container')}>
          <h1 className={b('title')}>{title}</h1>
          {isCategory && (
            <img
              className={b('category-image')}
              src={selectedCategory.imageUrl}
              alt={selectedCategory.name}
            />
          )}
          <h2 className={b('title-desc')}>{description}</h2>
        </div>
        <div className={b('desktop')}>
          <Articles blogs={data} bem={b} selectedCategory={selectedCategory} />
        </div>
      </main>
    );
  }
}

const stateProps = (state) => ({
  blogs: getBlogList(state),
  categories: categoriesBlog(state),
  isLoading: isBlogsLoading(state),
  promoSection: promoSectionSelector(state),
});

const actions = {
  moreArticles: addBlogs,
  getPromo: getPromoSection,
};

Content.propTypes = propTypes;
Content.defaultProps = defaultProps;

export default connect(stateProps, actions)(Content);
