import React from 'react';
import PropTypes from 'prop-types';
import OneBlog from './oneBlog';

const defaultProps = {
  blogs: {},
  bem: () => {},
  selectedCategory: {},
};

const propTypes = {
  blogs: PropTypes.shape({
    data: PropTypes.arrayOf(PropTypes.shape({})),
  }),
  bem: PropTypes.func,
  selectedCategory: PropTypes.shape({}),
};

const Articles = (props) => {
  const {
    blogs: { data = [] },
    bem,
    selectedCategory,
  } = props;

  return (
    <section className={bem('article-columns')}>
      <div className={bem('article-list')}>
        <div className={bem('top-stripe')} />
        <div className={bem('top-rect')} />
        {data.map(item => (
          <OneBlog 
            item={item} 
            bem={bem}
            key={item.id}
            selectedCategory={selectedCategory}
          />
))}
      </div>
    </section>
  );
};

Articles.propTypes = propTypes;
Articles.defaultProps = defaultProps;

export default Articles;
