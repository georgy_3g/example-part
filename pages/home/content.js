import React, { Component } from 'react';
import bem from 'src/utils/bem';
import PropTypes from 'prop-types';

import { HOME_TERMS } from 'src/terms';
import ROUTES from 'src/constants/routes';
import dynamic from 'next/dynamic';
import { connect } from 'react-redux';
import Colors from 'src/styles/colors.json';
import debounce from 'lodash/debounce';
import bgProductUrl from 'public/img/home/HOW_IT_WORKS/DESKTOP/*******-how-it-works.jpg';
import mobileBgProductUrl from 'public/img/home/HOW_IT_WORKS/MOBILE/*******-how-it-works.jpg';
import tabletBgImg from 'public/img/home/HOW_IT_WORKS/TABLET/*******-how-it-works.jpg';
import infoGiftImage from 'public/img/home/GIFT/DESKTOP/*******-give-the-perfect-gift.jpg';
import infoGiftImageMob from 'public/img/home/GIFT/TABLET/*******-give-the-perfect-gift.jpg';
import { isMobileDeviceSelector } from 'src/redux/auth/selectors';
import { getAllPricesAction } from 'src/redux/prices/actions';
import constants from '../../pagesContent/home/constants';
import styles from './index.module.scss';

const WhatWeDo = dynamic(() => import('src/components/elements/WhatWeDo'));
const WhatIncluded = dynamic(() => import('src/components/widgets/WhatIncluded'));
const WhatIncludedRight = dynamic(() => import('src/components/widgets/WhatIncludedRight'));
const OurPartners = dynamic(() => import('src/components/elements/OurPartners'));
const TypeProducts = dynamic(() => import('src/components/elements/TypeProducts'));
const HomeBanner = dynamic(() => import('src/components/elements/HomeBanner'));
const InfoWidget = dynamic(import('../../pagesContent/home/components/InfoWidget'));

const {
  DATA_CARD,
  DIGITIZE_DATA_CARDS,
  BANNER_TITLE,
  BANNER_DESC,
  NEW_INFO_WIDGET_BUTTON_TEXT,
  ENHANCE_DATA_CARDS,
  WHAT_INCLUDE_BUTTON_LINK,
  WHAT_INCLUDE_BUTTON_TEXT,
  WHAT_INCLUDED_TITLE,
  WHAT_INCLUDED_TEXT,
  WHAT_INCLUDE_BUTTON_LINK_TWO,
  WHAT_INCLUDE_BUTTON_TEXT_TWO,
  WHAT_INCLUDED_TITLE_TWO,
  WHAT_INCLUDED_TEXT_TWO,
  WHAT_WE_DO_TITLE_ENHANCE,
  WHAT_WE_DO_SUBTITLE_ENHANCE,
  WHAT_WE_DO_TITLE_DIGITIZE,
  WHAT_WE_DO_DESC_DIGITIZE,
  WHAT_INCLUDE_SLIDER_LIST,
  WHAT_INCLUDE_SLIDER_LIST_MOB,
  WHAT_INCLUDED_RIGHT_TITLE,
  WHAT_INCLUDED_RIGHT_DESC,
  WHAT_INCLUDED_RIGHT_BUTTON_LINK,
  WHAT_INCLUDE_SLIDER_LIST_TWO,
} = constants;

const icebergBlue = Colors['$iceberg-blue-light-color'];
const lightGray = Colors['$light-slate-gray-color'];
const yellowColor = Colors['$coconut-cream-yellow-color'];

const {
  HOW_IT_WORKS,
  HOW_IT_WORKS_DESC,
} = HOME_TERMS;

const { howItWorks } = ROUTES;

const b = bem('home', styles);

const defaultProps = {
  isMobileDevice: true,
};

const propTypes = {
  isMobileDevice: PropTypes.bool,
};

class HomeContent extends Component {
  resizeWindow = debounce(() => {
    this.getContent();
  }, 300);

  constructor(props) {
    super(props);

    const { isMobileDevice } = props;

    this.state = {
      content: {
        whatIncludeSliderList: isMobileDevice
        ? WHAT_INCLUDE_SLIDER_LIST_MOB
        : WHAT_INCLUDE_SLIDER_LIST,
      },
    };
  }

  componentDidMount() {
    window.addEventListener('resize', this.resizeWindow);
    this.windowWidth = window.innerWidth;
    this.getContent();
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.resizeWindow);
    this.windowWidth = null;
  }

  getContent = () => {
    const {
      content: { whatIncludeSliderList },
    } = this.state;
    if (window.innerWidth <= 480 && whatIncludeSliderList !== WHAT_INCLUDE_SLIDER_LIST_MOB) {
      this.setState({
        content: {
          whatIncludeSliderList: WHAT_INCLUDE_SLIDER_LIST_MOB,
        },
      });
    } else if (
      window.innerWidth <= 1024 &&
      window.innerWidth > 480 &&
      whatIncludeSliderList !== WHAT_INCLUDE_SLIDER_LIST_MOB
    ) {
      this.setState({
        content: {
          whatIncludeSliderList: WHAT_INCLUDE_SLIDER_LIST_MOB,
        },
      });
    } else if (window.innerWidth > 1024 && whatIncludeSliderList !== WHAT_INCLUDE_SLIDER_LIST) {
      this.setState({
        content: {
          whatIncludeSliderList: WHAT_INCLUDE_SLIDER_LIST,
        },
      });
    }
  };

  render() {
    const { content } = this.state;
    const { whatIncludeSliderList } = content;
    return (
      <main className={b()}>
        <HomeBanner main bannerTitle={BANNER_TITLE} bannerDesc={BANNER_DESC} />
        <TypeProducts dataCard={DATA_CARD} />
        <OurPartners />
        <WhatWeDo
          dataTitle={WHAT_WE_DO_TITLE_ENHANCE}
          subTitle={WHAT_WE_DO_SUBTITLE_ENHANCE}
          dataCard={ENHANCE_DATA_CARDS}
          withoutTitleInMobile
        />
        <WhatIncluded
          sliderList={whatIncludeSliderList}
          buttonText={WHAT_INCLUDE_BUTTON_TEXT}
          buttonLink={WHAT_INCLUDE_BUTTON_LINK}
          title={WHAT_INCLUDED_TITLE}
          text={WHAT_INCLUDED_TEXT}
          infinite={false}
          isShowRightImage={false}
          withoutControls
        />
        <WhatWeDo
          dataTitle={WHAT_WE_DO_TITLE_DIGITIZE}
          subTitle={WHAT_WE_DO_DESC_DIGITIZE}
          dataCard={DIGITIZE_DATA_CARDS}
          withoutTitleInMobile
        />
        <WhatIncludedRight
          isRight
          cardImage={infoGiftImage}
          cardImageMob={infoGiftImageMob}
          cardBackgroundStripeColor={icebergBlue}
          cardBackgroundSecondStripeColor={lightGray}
          title={WHAT_INCLUDED_RIGHT_TITLE}
          description={WHAT_INCLUDED_RIGHT_DESC}
          isLeftStripe
          leftStripeColor={yellowColor}
          isButton
          buttonText={NEW_INFO_WIDGET_BUTTON_TEXT}
          buttonLink={WHAT_INCLUDED_RIGHT_BUTTON_LINK}
        />
        <InfoWidget
          title={HOW_IT_WORKS}
          desc={HOW_IT_WORKS_DESC}
          bgImg={bgProductUrl}
          mobileBgImg={mobileBgProductUrl}
          tabletBgImg={tabletBgImg}
          learnMoreLink={howItWorks}
        />
        <WhatIncluded
          sliderList={WHAT_INCLUDE_SLIDER_LIST_TWO}
          buttonText={WHAT_INCLUDE_BUTTON_TEXT_TWO}
          buttonLink={WHAT_INCLUDE_BUTTON_LINK_TWO}
          title={WHAT_INCLUDED_TITLE_TWO}
          text={WHAT_INCLUDED_TEXT_TWO}
          infinite={false}
          isShowRightImage={false}
          withoutControls
          isMargin
        />
      </main>
    );
  }
}

HomeContent.propTypes = propTypes;
HomeContent.defaultProps = defaultProps;

const stateProps = (state) => ({
  isMobileDevice: isMobileDeviceSelector(state),
});

const actions = {
  getAllPrices: getAllPricesAction,
};

export default connect(stateProps, actions)(HomeContent);
