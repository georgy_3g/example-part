import React from 'react';
import { NextSeo } from 'next-seo';
import dynamic from "next/dynamic";
import mobPhotoArtImg from  'public/img/home/SUB_MENU/TABLET/*******-create-photo-art.jpg';
import mobDigitizing from  'public/img/home/SUB_MENU/TABLET/*******-transfer-my-old-media.jpg';
import mobRetouching from  'public/img/home/SUB_MENU/TABLET/*******-enhance-my-photo.jpg';
import HomeContent from './content';
import constants from '../../pagesContent/home/constants';
import { getBreadCrumbList } from "../../src/utils/getSeoStructureData";

const Navigation = dynamic(() => import('src/components/shared/Navigation'));
const Footer = dynamic(() => import('src/components/shared/Footer'));

const { SEO_DESCRIPTION, SEO_TITLE } = constants;

const breadCrumbList = [
  {
    name: 'Home',
    position: '1',
    itemUrl: 'https://*******.com/'
  },
]

const Home = () => (
  <>
    <NextSeo
      title={SEO_TITLE}
      description={SEO_DESCRIPTION}
      canonical="https://*******.com/"
      languageAlternates={[
        {
          hrefLang: 'en-us',
          href: 'https://*******.com/',
        },
      ]}
      facebook={{
        appId: '*******FL',
      }}
      openGraph={{
        type: 'website',
        url: "https://*******.com/",
        title: SEO_TITLE,
        description: SEO_DESCRIPTION,
        images: [
          {
            url: mobRetouching.src,
            width: 290,
            height: 290,
            alt: 'Restore, Enhance and Repair Old Photos',
          },
          {
            url: mobPhotoArtImg.src,
            width: 290,
            height: 290,
            alt: 'Personalized Canvas and Photo Frames',
          },
          {
            url: mobDigitizing.src,
            width: 290,
            height: 290,
            alt: 'Digital Media Transfer Services',
          },
        ],
      }}
    />
    <script
      type="application/ld+json"
      dangerouslySetInnerHTML={{ __html: JSON.stringify(getBreadCrumbList(breadCrumbList)) }}
      async
    />
    <Navigation />
    <HomeContent />
    <Footer />
  </>
);

export default Home;
