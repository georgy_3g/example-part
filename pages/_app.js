import React from 'react';
import PropTypes from 'prop-types';
import { envScript } from 'src/utils/env';
import App from 'next/app';
import { removeToken, setDeviceTypeAction } from 'src/redux/auth/actions';
import { wrapper } from 'src/redux/store';
import 'src/styles/global.scss';
import 'public/fonts/stylesheet.css';
import 'public/fonts/stylesheetOthers.css';
import {
  FACEBOOK_LINK,
  INSTAGRAM_LINK,
  NEXT_PUBLIC_CHAT_HOST,
  NEXT_PUBLIC_CHAT_TOKEN,
  TIKTOK_LINK,
  TWITTER_LINK,
  YOUTUBE_LINK,
} from 'src/config';
// import Rollbar from 'rollbar';
import Head from 'next/head';
import { SocialProfileJsonLd } from "next-seo";
import firstImage from 'public/img/home/our-mission.jpg';
import secondImage from 'public/img/home/digitizing-service.jpg';
import thirdImage from 'public/img/home/create-photo-art.jpg';
import Script from 'next/script';
import { Partytown } from '@builder.io/partytown/react';

const ONE_DAY = 86400000;

const defaultProps = {
  Component: () => {},
  pageProps: {},
  ctx: {},
};

const propTypes = {
  Component: PropTypes.any,
  pageProps: PropTypes.shape({}),
  ctx: PropTypes.shape({}),
};

class MyApp extends App {
  static getInitialProps = wrapper.getInitialAppProps(store => async context => {
    const { ctx } = context || {};
    const {
      req,
    } = ctx;
    const { route: { path } = {} } = req || {};

    const { dispatch = () => {}, getState = () => ({}) } = store
    const props = await getState();
    const {
      auth: { token, rememberMe, startTime } = {},
    } = props || {};

    try {
      const isMobileDevice = Boolean(
        ((req ? req.headers['user-agent'] : navigator.userAgent) || '').match(
          /Android|BlackBerry|iPhone|iPad|iPod|Opera Mini|IEMobile|WPDesktop/i,
        ),
      );

      await dispatch(setDeviceTypeAction(isMobileDevice));

    }
    catch {
      await dispatch(setDeviceTypeAction(true));
    }

    try {
      if (token && !rememberMe && startTime && startTime + ONE_DAY * 10 < Number(new Date())) {
        await dispatch(removeToken());
      }

    }
    catch {
      await dispatch(removeToken());
    }

    return {
      pageProps: {
        ...(await App.getInitialProps(context)).pageProps,
      },
      path,
    };
  })

  constructor(props) {
    super(props);
    this.state = {
      isPageLoaded: false,
      // rollbar:
      //   NEXT_PUBLIC_NODE_ENV !== 'develop' && NEXT_PUBLIC_ROLLBAR_TOKEN
      //     ? new Rollbar({
      //         accessToken: NEXT_PUBLIC_ROLLBAR_TOKEN,
      //         environment: `client ${NEXT_PUBLIC_NODE_ENV}`,
      //       })
      //     : {},
      isGTMLoaded: false,
    };
  }

  render() {
    const {
      Component,
      pageProps,
    } = this.props;
    return (
      <>
        <Head>
          <link rel="icon" href="/favicon.ico" type="image/png" />
          <link rel="icon" href="/favicon.ico" type="image/ico" />
          <meta name="application-name" content="&nbsp;" />
          <meta name="msapplication-TileColor" content="#FFFFFF" />
          <meta name="msapplication-TileImage" content="/favicon.ico" />
          <script dangerouslySetInnerHTML={{ __html: envScript }} />
          <link rel="shortcut icon" href="/favicon.ico" type="image/png" />
          <link rel="shortcut icon" href="/favicon.ico" type="image/ico" />
          <link rel='manifest' href='/manifest.json' />
          <meta charSet="utf-8" />
          <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
          <meta
            name="viewport"
            content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=5"
          />
          <meta name="description" content="Description" />
          <meta name="keywords" content="Keywords" />
          <link
            href="/icons/FS-72.png"
            rel="icon"
            type="image/png"
            sizes="16x16"
          />
          <link
            href="/icons/FS-72.png"
            rel="icon"
            type="image/png"
            sizes="32x32"
          />
          <link rel="apple-touch-icon" href="/apple-icon.png" />
          <meta name="theme-color" content="#317EFB" />
          <script type="text/partytown" src="https://mozilla.github.io/pdf.js/build/pdf.js" />
        </Head>
        <Script
          src="/~partytown/partytown.js"
          onLoad={() => {
            window.partytown = {
              forward: ["dataLayer.push", "fbq"],
              lib: "/partytown/",
              resolveUrl: (url, location) => {
                if (
                  url.hostname === 'connect.facebook.net' ||
                  url.hostname === 'wchat.freshchat.com' ||
                  url.hostname === 'mozilla.github.io' ||
                  (url.hostname === 'www.google-analytics.com' && url.href.endsWith('.js'))
                ) {
                  const proxyUrl = new URL('', location.origin);
                  proxyUrl.searchParams.append('url', url.href);
                  return proxyUrl;
                }
                return url;
              }
            }
          }}
        />
        <Script
          strategy="worker"
          type="text/partytown"
          src="https://wchat.freshchat.com/js/widget.js"
          onLoad={() => {
            window.fcWidget.init({
              token: `${NEXT_PUBLIC_CHAT_TOKEN}`,
              host: `${NEXT_PUBLIC_CHAT_HOST}`,
            });
          }}
        />
        <Script strategy="worker" type="text/partytown" src="https://mozilla.github.io/pdf.js/build/pdf.js" />
        <Component {...pageProps} />
      </>
    );
  }
}

MyApp.propTypes = propTypes;
MyApp.defaultProps = defaultProps;

export default wrapper.withRedux(MyApp);
