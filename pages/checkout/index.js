import React, { Component } from 'react';
import { NextSeo } from 'next-seo';
import axios from 'axios';
import Head from 'next/head';
import URL from 'src/redux/urls';
import dynamic from 'next/dynamic';
import { wrapper } from 'src/redux/store';
import preloadPrice from 'src/utils/preloadPrice';
import preloadCloudStatus from 'src/utils/preloadCloudStatus';
import { loadPriceList } from 'src/redux/prices/actions';
import { photoArtProductListAction } from 'src/redux/shared/actions';
import { setCloudStatus } from 'src/redux/myCloud/actions';
import Content from './content';

const Navigation = dynamic(() => import('src/components/shared/Navigation'));
const Footer = dynamic(() => import('src/components/shared/Footer'));

export default class Checkout extends Component {
  static getInitialProps = wrapper.getInitialPageProps(store => async () => {
    const { dispatch, getState } = store;
    const props = await getState();
    const {
      auth: { token },
    } = props;

    const [products, photoArtData] = await Promise.all([
      preloadPrice(URL.PRODUCT),
      axios.get(URL.PHOTO_ART_PRODUCT),
    ]);
    const {
      data: { data: photoArt },
    } = photoArtData;

    await Promise.all([
      dispatch(loadPriceList(products)),
      dispatch(photoArtProductListAction(photoArt)),
    ]);

    if (token) {
      const daysRemaining = await preloadCloudStatus(URL.MEMOYA_CLOUD_STATUS, token);
      await dispatch(setCloudStatus({ ...daysRemaining }));
    } else {
      await dispatch(setCloudStatus({ daysRemaining: 0 }));
    }

    return {};
  })

  render() {
    return (
      <>
        <Head>
          <meta name="robots" content="noindex" />
        </Head>
        <NextSeo noindex />
        <Navigation />
        <Content />
        <Footer blog={false} />
      </>
    );
  }
}
