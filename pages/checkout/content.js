import React, { Component } from 'react';
import bem from 'src/utils/bem';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import debounce from 'lodash/debounce';
import { SHIPPING_PRODUCTS } from 'src/constants';
import dynamic from 'next/dynamic';

import { setCoupon } from 'src/redux/coupon/actions';
import { orderCalculateAction } from 'src/redux/orderCreate/actions';
import ordersTypeSelect from 'src/redux/orders/selectors';
import { orderRushStatusSelector } from 'src/redux/orderRush/selectors';
import { photoArtHintsSelector } from 'src/redux/photoArt/selectors';
import { getToken, userIdSelector } from 'src/redux/auth/selectors';
import { couponSelect } from 'src/redux/coupon/selectors';
import styles from './index.module.scss';
import TERMS from '../../pagesContent/checkout/constants';

const OrderCheckoutCard = dynamic(() => import('src/components/cards/OrderCheckoutCard'));
const OrderSummary = dynamic(() => import('src/components/widgets/OrderSummary'));

const b = bem('checkout', styles);

const {
  CART,
  YOUR_PRODUCTS,
  ORDER_SUMMARY,
  ENHANCE_PRINTS,
  PERSONALIZE,
  *******_SAFE,
  ENHANCE_MY_TRANSFER,
  PHOTO_ART,
} = TERMS;

const defaultProps = {
  orders: {},
  orderRushStatus: false,
  orderCalculate: () => {},
  token: '',
  getCoupon: {},
  userId: null,
  setCouponLocal: () => { },
};

const propTypes = {
  orders: PropTypes.shape({}),
  orderRushStatus: PropTypes.bool,
  orderCalculate: PropTypes.func,
  token: PropTypes.string,
  getCoupon: PropTypes.shape({
    id: PropTypes.number,
  }),
  userId: PropTypes.number,
  setCouponLocal: PropTypes.func,
};

class Content extends Component {
  constructor(props) {
    super(props);

    this.state = {
      deletePopup: false,
      orderId: null,
      orderType: null,
      isHintOpen: false,
      hintName: '',
    };
  }

  componentDidMount() {
    const { setCouponLocal } = this.props;

    const coupon = JSON.parse(sessionStorage.getItem('promoCode') || '{}') || {};
    setCouponLocal(coupon);
  }

  componentDidUpdate(prevProps) {
    const { orders, getCoupon } = this.props;
    const { orders: prevOrders, getCoupon: prevCoupon } = prevProps;
    const ordersList = this.getProductsArr(orders);
    const prevOrdersList = this.getProductsArr(prevOrders);

    const { id: couponId } = getCoupon;
    const { id: prevCouponId } = prevCoupon;

    if (prevOrdersList.length !== ordersList.length || couponId !== prevCouponId) {
      this.getOrderPaymentInfo();
    }
  }

  getProductsArr = (orders) =>
    Object.values(orders).reduce((acc, item) => (item.length ? [...acc, ...item] : acc), []);

  getOrderCalculate = () => {
    const { getCoupon, orderRushStatus, orders, token, orderCalculate, userId } = this.props;
    const { id: promoCodeId } = getCoupon;
    const orderItems = [];

    Object.keys(orders).forEach(
      (key) =>
        orders[key].length > 0 &&
        orders[key].forEach((item) => {
          const { options, quantity: productQuantity } = item;

          const newOptions = options
            ? options
                .map((option) => {
                  const { optionId, quantity, name } = option;
                  if (!quantity) {
                    return null;
                  }

                  if (name === ENHANCE_PRINTS) {
                    return {
                      optionId,
                      quantity,
                    };
                  }

                  if (quantity === true && name === PERSONALIZE) {
                    const *******Safe = options.find((opt) => opt.name === *******_SAFE);
                    return *******Safe.quantity
                      ? {
                          optionId,
                          quantity: *******Safe.quantity,
                        }
                    : null;
                  }

                  if (quantity === true && name === ENHANCE_MY_TRANSFER) {
                    return {
                      optionId,
                      quantity: productQuantity,
                    };
                  }

                  if (quantity === true) {
                    return {
                      optionId,
                      quantity: 1,
                    };
                  }

                  if (name === PHOTO_ART) {
                    return {
                      optionId,
                      quantity: productQuantity,
                    };
                  }

                  return {
                    optionId,
                    quantity,
                  };
                })
                .filter(Boolean)
            : [];
          const newItem = {
            productId: item.productId,
            quantity: item.quantity,
            ...(item.text && { customText: `${item.text}@@@@@${item.textSecond}` }),
            ...(item.customText && { customText: item.customText }),
            isGift: item.isGift,
            ...(newOptions && { options: newOptions }),
            isMatting: Boolean(item.isMatting),
            ...(item.framePhotoMattingId && { framePhotoMattingId: item.framePhotoMattingId }),
            ...(item.framePhotoMattingColorId && {
              framePhotoMattingColorId: item.framePhotoMattingColorId,
            }),
            // personalDiscount: 0,
          };

          if (item.isGift && item.giftOptions) {
            newItem.giftOptions = { addWrapping: Boolean(item.giftOptions.addWrapping) };
          }

          orderItems.push(newItem);
        }),
    );
    orderCalculate({
      order: {
        ...(promoCodeId && { promoCodeId }),
        isQuickly: orderRushStatus,
        ...(orderItems && { orderItems }),
        ...(userId ? { userId } : {}),
      },
      token,
    });
  };

  getOrderPaymentInfo = debounce(() => {
    this.getOrderCalculate();
  }, 500);

  openPopup = (data) => {
    const { orderId, deletePopup, orderType } = data;
    this.setState({
      orderId,
      deletePopup,
      orderType,
    });
  };

  toggleHint = (name) => {
    const { isHintOpen, hintName } = this.state;
    if (hintName === name) {
      this.setState({
        isHintOpen: !isHintOpen,
        hintName: name,
      });
    } else {
      this.setState({
        isHintOpen: true,
        hintName: name,
      });
    }
  };

  checkOrder = () => {
    const { orders } = this.props;
    return Object.values(orders).reduce(
      (acc, item) => (!acc ? acc : item.every(({ isInvalidOrder }) => !isInvalidOrder)),
      true,
    );
  };

  render() {
    const { orders, orderRushStatus } = this.props;
    const ordersValues = Object.values(orders).reduce((acc, item) => {
      if (item.length > 0) {
        return [...acc, ...item];
      }
      return acc;
    }, []).filter(item => !SHIPPING_PRODUCTS.includes(item.typeProduct));
    const isValidOrders = this.checkOrder();

    const { deletePopup, orderId, orderType } = this.state;
    return (
      <main className={b()}>
        <div className={b('title')}>
          <div className={b('title-icon')}>
            <h1 className={b('title-icon-name', { mix: 'title' })}>{CART}</h1>
          </div>
        </div>
        <div className={b('orders-wrapper')}>
          <section className={b('order-details')}>
            <h2 className={b('products-title', { mix: 'title' })}>{YOUR_PRODUCTS}</h2>
            <div className={b('orders')}>
              {Boolean(ordersValues && ordersValues.length) && (
                <OrderCheckoutCard
                  data={ordersValues}
                  isDeletePopup={deletePopup}
                  orderId={orderId}
                  orderType={orderType}
                  openPopup={this.openPopup}
                  getOrderPaymentInfo={this.getOrderPaymentInfo}
                />
              )}
            </div>
          </section>
          <section className={b('orders-summary')}>
            <h2 className={b('products-title', { mix: 'title' })}>{ORDER_SUMMARY}</h2>
            <div className={b('orders-summary-wrap')}>
              <OrderSummary
                orderRushStatus={orderRushStatus}
                isCheckoutPage
                isCouponForm
                isValidOrders={isValidOrders}
                getOrderPaymentInfo={this.getOrderPaymentInfo}
              />
            </div>
          </section>
        </div>
      </main>
    );
  }
}

Content.propTypes = propTypes;
Content.defaultProps = defaultProps;

const stateProps = (state) => ({
  orders: ordersTypeSelect(state),
  orderRushStatus: orderRushStatusSelector(state),
  hints: photoArtHintsSelector(state),
  token: getToken(state),
  getCoupon: couponSelect(state),
  userId: userIdSelector(state),
});

const actions = {
  orderCalculate: orderCalculateAction,
  setCouponLocal: setCoupon,
};

export default connect(stateProps, actions)(Content);
