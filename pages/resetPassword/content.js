import React, { Component } from 'react';
import PropTypes from 'prop-types';
import bem from 'src/utils/bem';
import * as Yup from 'yup';
import { connect } from 'react-redux';
import { Formik } from 'formik';
import DICTIONARY from 'src/constants/dictionary';
import INPUTS_NAMES from 'src/constants/inputsNames';
import ERRORS from 'src/constants/errors';
import dynamic from 'next/dynamic';
import { isPasswordChange } from 'src/redux/shared/selectors';
import { setNewPassAction } from 'src/redux/shared/actions';
import styles from './index.module.scss';

const BlueButton = dynamic(() => import('src/components/buttons/BlueButton'));
const NewCustomInput = dynamic(() => import('src/components/inputs/NewCustomInput'));

const b = bem('reset-password', styles);
const form = bem('form', styles);

const { SHOULD_BE_FILLED, SHORT_PASSWORD, PASSWORDS_MATCH } = ERRORS;

const { SUBMIT, PASSWORD, NEW_PASSWORD, CONFIRM_NEW_PASSWORD, RESET_PASS_SUCCESS } = DICTIONARY;

const schema = Yup.object().shape({
  password: Yup.string().min(6, SHORT_PASSWORD).required(SHOULD_BE_FILLED),
  newPassword: Yup.string()
    .min(6, SHORT_PASSWORD)
    .required(SHOULD_BE_FILLED)
    .oneOf([Yup.ref('password'), null], PASSWORDS_MATCH),
});

const defaultProps = {
  query: '',
  setNewPass: () => {},
  isChangePassword: false,
};

const propTypes = {
  query: PropTypes.string,
  setNewPass: PropTypes.func,
  isChangePassword: PropTypes.bool,
};

class ResetPasswordContent extends Component {
  onSubmit = (values) => {
    const { password, newPassword } = values;
    const { query, setNewPass } = this.props;
    const { token } = query;
    const newPassDetails = {
      token,
      password,
    };
    if (password === newPassword) {
      setNewPass(newPassDetails);
    }
  };

  render() {
    const { isChangePassword } = this.props;
    return (
      <main className={b()}>
        <Formik
          initialValues={{ password: '', newPassword: '' }}
          validationSchema={schema}
          onSubmit={(values, actions) => {
            actions.resetForm();
            this.onSubmit(values);
          }}
          render={({ errors, handleSubmit, touched, handleChange, values }) => (
            <form onSubmit={handleSubmit} className={b('form', { mix: form() })}>
              <fieldset className={b('fieldset', { mix: form('fieldset') })}>
                <legend className={b('legend', { mix: form('legend') })}>{NEW_PASSWORD}</legend>
                <NewCustomInput
                  className={b('input')}
                  onChange={handleChange}
                  name={INPUTS_NAMES.password}
                  placeholder={PASSWORD}
                  type={PASSWORD}
                  value={values.password}
                  isTouched={touched.password}
                  error={errors.password}
                  isValid={!errors.password}
                />
                <NewCustomInput
                  className={b('input')}
                  onChange={handleChange}
                  name={INPUTS_NAMES.newPassword}
                  placeholder={CONFIRM_NEW_PASSWORD}
                  type={PASSWORD}
                  value={values.newPassword}
                  isTouched={touched.newPassword}
                  error={errors.newPassword}
                  isValid={!errors.newPassword}
                />
              </fieldset>
              {isChangePassword && <p className={b('submit-msg')}>{RESET_PASS_SUCCESS}</p>}
              <BlueButton
                className={b('btn')}
                text={SUBMIT}
                type="button"
                onClick={handleSubmit}
                disabled={Object.keys(errors).length}
              >
                {SUBMIT}
              </BlueButton>
            </form>
          )}
        />
      </main>
    );
  }
}

ResetPasswordContent.propTypes = propTypes;
ResetPasswordContent.defaultProps = defaultProps;

const stateProps = (state) => ({
  isChangePassword: isPasswordChange(state),
});

const actions = {
  setNewPass: setNewPassAction,
};

export default connect(stateProps, actions)(ResetPasswordContent);
