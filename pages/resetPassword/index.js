import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { NextSeo } from 'next-seo';
import preloadPrice from 'src/utils/preloadPrice';
import URL from 'src/redux/urls';
import dynamic from 'next/dynamic';
import { wrapper } from 'src/redux/store';
import { loadPriceList } from 'src/redux/prices/actions';
import ResetPasswordContent from './content';

const Navigation = dynamic(() => import('src/components/shared/Navigation'));
const Footer = dynamic(() => import('src/components/shared/Footer'));

const propTypes = {
  query: PropTypes.string.isRequired,
};

export default class ResetPassword extends Component {
  static getInitialProps = wrapper.getInitialPageProps(store => async ({ req }) => {
    const { dispatch } = store;

    const [
      products,
    ] = await Promise.all([
      preloadPrice(URL.PRODUCT),
    ]);

    await dispatch(loadPriceList(products));
    const { query } = req;
    return {
      query,
    };
  })

  render() {
    const { query } = this.props;
    return (
      <>
        <NextSeo
          title='*******'
          description='*******'
        />
        <Navigation />
        <ResetPasswordContent query={query} />
        <Footer />
      </>
    );
  }
}

ResetPassword.propTypes = propTypes;
