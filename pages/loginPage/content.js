import React from 'react';
import bem from 'src/utils/bem';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import ROUTES from 'src/constants/routes';
import dynamic from 'next/dynamic';
import { setPopupData } from 'src/redux/shared/actions';
import constants from '../../pagesContent/loginPage/constants';

import styles from './index.module.scss';

const LoginForm = dynamic(() => import('src/components/forms/LoginForm'));
const SignUpForm = dynamic(() => import('src/components/forms/SignUpForm'));

const { TERMS, LINKS } = constants;
const b = bem('login-page', styles);

const { POPUP_NAME } = TERMS;

const { account } = ROUTES;

const defaultProps = {
  setPopupName: () => {},
  continuе: '',
};

const propTypes = {
  setPopupName: PropTypes.func,
  continuе: PropTypes.string,
};

function Content({ setPopupName, continuе }) {
  const redirectLink = LINKS[continuе];

  return (
    <main className={b()}>
      <div className={b('forms-wrap')}>
        <div className={b('form-block')}>
          <LoginForm
            className={b('login')}
            changePopup={setPopupName}
            popupName={POPUP_NAME}
            withoutSignup
            redirectLink={redirectLink || account}
            buttonInCenter
          />
        </div>
        <div className={b('forms-block')}>
          <SignUpForm className={b('signup')} redirectLink={redirectLink || account} />
        </div>
      </div>
    </main>
  );
}

Content.propTypes = propTypes;
Content.defaultProps = defaultProps;

const actions = {
  setPopupName: setPopupData,
};

export default connect(null, actions)(Content);
