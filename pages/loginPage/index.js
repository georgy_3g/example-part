import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { NextSeo } from 'next-seo';
import ROUTES from 'src/constants/routes';
import preloadPrice from 'src/utils/preloadPrice';
import URL from 'src/redux/urls';
import dynamic from 'next/dynamic';
import { loadPriceList } from 'src/redux/prices/actions';
import { wrapper } from 'src/redux/store';
import Content from './content';

const Navigation = dynamic(() => import('src/components/shared/Navigation'));
const Footer = dynamic(() => import('src/components/shared/Footer'));

const { home } = ROUTES;

const defaultProps = {
  next: '',
};

const propTypes = {
  next: PropTypes.string,
};

class Login extends Component {
  static getQuery = () => {
    try {
      const params = {};
      const urlParams = new URLSearchParams(window.location.search);
      urlParams.forEach((value, key) => {
        params[key] = value;
      });
      return params;
    } catch (e) {
      return {};
    }
  };

  static getInitialProps = wrapper.getInitialPageProps(store => async ({ res, req }) => {
    const { dispatch, getState } = store;
    const { next } = (req ? req.query : this.getQuery()) || {};
    const props = await getState();
    const {
      auth: { token },
    } = props;

    if (token) {
      if (res) {
        res.redirect(home);
        res.end();
        return {};
      }
      document.location.href = home;
    }

    const [products] = await Promise.all([preloadPrice(URL.PRODUCT)]);

    await dispatch(loadPriceList(products));

    return { next };
  })

  render() {
    const { next } = this.props;
    return (
      <>
        <NextSeo
          title="*******"
          description="*******"
          languageAlternates={[
            {
              hrefLang: 'en-us',
              href: 'https://*******.com/login/',
            },
          ]}
        />
        <Navigation />
        <Content continuе={next} />
        <Footer blog={false} />
      </>
    );
  }
}

Login.propTypes = propTypes;
Login.defaultProps = defaultProps;

export default Login;
