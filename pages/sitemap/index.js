import axios from 'axios';
import { NEXT_PUBLIC_IMAGES_DOMAIN } from 'src/config';

const Sitemap = () => {};

export const getServerSideProps = async ({ res }) => {
	const data = await axios.get(`${NEXT_PUBLIC_IMAGES_DOMAIN}/front/sitemap.xml`);
	const { data: sitemap = '' } = data || {}

	res.setHeader("Content-Type", "text/xml");
	res.write(sitemap);
	res.end();

	return {
		props: {},
	};
};

export default Sitemap;
