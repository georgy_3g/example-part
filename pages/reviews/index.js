import React from 'react';
import { NextSeo } from 'next-seo';
import { connect } from 'react-redux';
import dynamic from 'next/dynamic';
import { reviewsCountSelect, reviewsSelect } from 'src/redux/reviewers/selectors';
import Content from './content';
import TERMS from '../../pagesContent/reviews/constants';

const Navigation = dynamic(() => import('src/components/shared/Navigation'));
const Footer = dynamic(() => import('src/components/shared/Footer'));

const { SEO_TITLE, SEO_DESCRIPTION } = TERMS;
const Reviews = () => (
  <>
    <NextSeo
      title={SEO_TITLE}
      description={SEO_DESCRIPTION}
      languageAlternates={[
        {
          hrefLang: 'en-us',
          href: 'https://*******.com/reviews/',
        },
      ]}
    />
    <Navigation />
    <Content />
    <Footer />
  </>
);

const stateProps = (state) => ({
  getReviews: reviewsSelect(state),
  getReviewsCount: reviewsCountSelect(state),
});

export default connect(stateProps)(Reviews);
