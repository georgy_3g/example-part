import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import bem from 'src/utils/bem';
import dynamic from 'next/dynamic';
import {
  reviewsCountSelect,
  reviewsLoadingSelect,
  reviewsRateSelect,
  reviewsSelect,
  reviewTotalPagesSelect,
} from 'src/redux/reviewers/selectors';
import { loadNewReviewers } from 'src/redux/reviewers/actions';
import TERMS from '../../pagesContent/reviews/constants';
import styles from './index.module.scss';

const ReviewCard = dynamic(() => import('src/components/cards/ReviewCard'));
const SortIcon = dynamic(() => import('src/components/svg/SortIcon'));
const ArrowIcon = dynamic(() => import('src/components/svg/ArrowIcon'));
const Paginate = dynamic(() => import('src/components/elements/Paginate'));

const { REVIEWS_TITLE, REVIEWS, SORTING_DESC, NEW, OLD, DESC, ASC } = TERMS;
const b = bem('reviews', styles);

const propTypes = {
  reviews: PropTypes.arrayOf(PropTypes.shape({})),
  totalReviews: PropTypes.number,
  loadReviews: PropTypes.func,
  totalPages: PropTypes.number,
};

const defaultProps = {
  reviews: [],
  totalReviews: 0,
  loadReviews: () => {},
  totalPages: 0,
};

class Content extends Component {
  constructor(props) {
    super(props);
    this.state = {
      limit: 12,
      offset: 0,
      order: 'desc',
    };
  }

  componentDidMount() {
    const { limit, offset, order } = this.state;
    const { loadReviews } = this.props;
    loadReviews({ limit, order, offset });
  }

  changeSorting = (type) => {
    const { limit, offset } = this.state;
    const { loadReviews } = this.props;
    loadReviews({ limit, order: type, offset });
    this.setState({ order: type });
  };

  handlePageClick = ({ selected }) => {
    const { limit, order } = this.state;
    const { loadReviews } = this.props;
    loadReviews({ limit, order, offset: selected });
  };

  render() {
    const { totalReviews, reviews, totalPages } = this.props;
    const { order } = this.state;
    return (
      <main className={b()}>
        <h1 className={b('title')}>{REVIEWS_TITLE}</h1>
        <div className={b('reviews-wrapper')}>
          <div className={b('review-statistic')}>
            <div className={b('total-reviews-wrapper')}>
              <div className={b('total-reviews-title')}>{REVIEWS}</div>
              <div className={b('total-reviews')}>{totalReviews}</div>
            </div>
          </div>
        </div>
        <div className={b('sorting')}>
          <SortIcon />
          <div className={b('sorting-desc')}>{SORTING_DESC}</div>
          <div
            className={b('sorting-btn', { isActive: order === DESC })}
            role="button"
            tabIndex={-1}
            onClick={() => this.changeSorting(DESC)}
          >
            {NEW}
          </div>
          <div
            className={b('sorting-btn', { isActive: order === ASC })}
            role="button"
            tabIndex={-1}
            onClick={() => this.changeSorting(ASC)}
          >
            {OLD}
          </div>
        </div>
        <div className={b('reviews-list')}>
          {reviews.map((review) => (
            <ReviewCard review={review} />
          ))}
          <div className={b('stripe')} />
          <div className={b('square')} />
        </div>
        <div className={b('pagination-wrapper')}>
          <Paginate
            previousLabel={<ArrowIcon width={9} height={16} className={b('back-arrow')} />}
            nextLabel={<ArrowIcon width={9} height={16} className={b('next-arrow')} />}
            breakLabel="..."
            breakClassName={b('pagination-break')}
            breakLinkClassName={b('pagination-break-link')}
            pageCount={totalPages}
            marginPagesDisplayed={1}
            pageRangeDisplayed={2}
            onPageChange={this.handlePageClick}
            containerClassName={b('pagination')}
            subContainerClassName={b('pagination-pages')}
            pageClassName={b('pagination-page')}
            pageLinkClassName={b('pagination-page-link')}
            activeClassName={b('active-page')}
            previousClassName={b('back-wrapper')}
            nextClassName={b('next-wrapper')}
            previousLinkClassName={b('back')}
            nextLinkClassName={b('next')}
            disabledClassName={b('disabled')}
          />
        </div>
      </main>
    );
  }
}

Content.propTypes = propTypes;
Content.defaultProps = defaultProps;

const stateProps = (state) => ({
  reviews: reviewsSelect(state),
  totalReviews: reviewsCountSelect(state),
  averageRating: reviewsRateSelect(state),
  isLoading: reviewsLoadingSelect(state),
  totalPages: reviewTotalPagesSelect(state),
});

const actions = {
  loadReviews: loadNewReviewers,
};

export default connect(stateProps, actions)(Content);
