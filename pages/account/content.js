import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import bem from 'src/utils/bem';
import updateURL from 'src/utils/updateURL';
import queryToObject from 'src/utils/queryToObject';
import ROUTES from 'src/constants/routes';
import { ACCOUNT_TERMS } from 'src/terms';
import debounce from 'lodash/debounce';

import dynamic from 'next/dynamic';

import { removeToken } from 'src/redux/auth/actions';
import { loadOrderDetails, loadOrderImages, loadOrderTrack } from 'src/redux/ordersList/actions';
import { getPromoSection } from 'src/redux/promo/actions';
import { getProjectListAction } from 'src/redux/projects/actions';
import {
  orderDetailsSelect,
  orderImagesSelect,
  ordersListSelect,
  proofsImagesSelector,
} from 'src/redux/ordersList/selectors';
import { getToken } from 'src/redux/auth/selectors';
import { promoSectionSelector } from 'src/redux/promo/selectors';
import { subscriptionSelector } from 'src/redux/myCloud/selectors';
import styles from './index.module.scss';
import constants from '../../pagesContent/account/constants';

const AccountTabs = dynamic(() => import('src/components/elements/AccountTabs'));
const NewSelectOrders = dynamic(() => import('src/components/elements/NewSelectOrders'));
const LogoutIcon = dynamic(() => import('src/components/svg/LogoutIcon'));

const { MAIN_TABS, LOGOUT_TEXT } = constants;

const b = bem('account', styles);

const { home } = ROUTES;
const { MY_ORDERS, ENHANCEMENTS, TRACK_ORDER } = ACCOUNT_TERMS;

const defaultProps = {
  logout: () => {},
  proofsImages: [],
  orderDetails: {},
  query: {},
  promoSection: {},
  getProjectList: () => {},
  token: '',
  subscribe: '',
  getPromo: () => {},
};

const propTypes = {
  logout: PropTypes.func,
  proofsImages: PropTypes.arrayOf(PropTypes.shape({})),
  orderDetails: PropTypes.shape({
    id: PropTypes.number,
  }),
  query: PropTypes.shape({
    main: PropTypes.string,
    tab: PropTypes.string,
    order: PropTypes.string,
  }),
  promoSection: PropTypes.shape({ id: PropTypes.number }),
  getProjectList: PropTypes.func,
  token: PropTypes.string,
  getPromo: PropTypes.func,
  subscribe: PropTypes.string,
};

class AccountContent extends Component {
  resizeWindow = debounce(() => {
    this.getPromoBannerMargin();
  }, 300);

  constructor(props) {
    super(props);
    this.child = React.createRef();
    const { query, proofsImages } = props;
    const { main, tab } = query || {};

    const selectedMain = main ? MAIN_TABS.find((item) => item.id === main) : MAIN_TABS[0];

    const isValidTab = (tab === 'proofs' && proofsImages.length) || tab !== 'proofs';

    const selectedTab =
      tab && isValidTab ? selectedMain.TABS.find((item) => item.id === tab) : selectedMain.TABS[0];

    this.state = {
      activeMainTab: selectedMain,
      activeScene: selectedTab.id,
      marginSize: 0,
    };
  }

  componentDidMount() {
    const { activeMainTab, activeScene } = this.state;
    const { getProjectList, token, getPromo } = this.props;
    this.setNewUrl({ main: activeMainTab.id, tab: activeScene });
    if (activeMainTab.id === 'account' || activeMainTab.id === 'my-message') {
      getPromo({ key: `account?main=${activeMainTab.id}` });
    }
    if (token) {
      getProjectList({ token, order: 'desc' });
    }
    window.addEventListener('resize', this.resizeWindow);
  }

  componentDidUpdate(prevProps, prevState) {
    const {
      orderDetails: { id },
      promoSection,
      getPromo,
    } = this.props;
    const {
      orderDetails: { id: oldId },
    } = prevProps;
    const { activeMainTab } = this.state;
    const { activeMainTab: oldTab } = prevState;
    if (id !== oldId) {
      this.setNewUrl({ order: id });
    }

    if (
      activeMainTab.id !== oldTab.id &&
      (activeMainTab.id === 'account' || activeMainTab.id === 'my-message')
    ) {
      getPromo({ key: `account?main=${activeMainTab.id}` });
    }

    if (oldId && id !== oldId) {
      this.setDefaultScene();
    }
    if (
      (!prevProps.promoSection || !prevProps.promoSection.id) &&
      promoSection &&
      promoSection.id
    ) {
      setTimeout(this.getPromoBannerMargin, 500);
    }
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.resizeWindow);
    this.windowWidth = null;
  }

  getPromoBannerMargin = () => {
    const promoBanner = document.getElementById('promo-banner');
    if (promoBanner) {
      const { height } = promoBanner.getBoundingClientRect();
      this.setState({ marginSize: Math.round(height / 2) });
    }
  };

  setDefaultScene = () => {
    this.setState({
      activeScene: MAIN_TABS[0].TABS[0].id,
    });
  };

  changeMainScreen = (id, queryParams, isTubClick) => {
    if (this.child && this.child.closeProject && isTubClick) {
      this.child.closeProject();
    }
    MAIN_TABS.forEach((item) => {
      if (item.id === id) {
        this.setNewUrl({
          main: item.id,
          tab: item.TABS[0].id,
          ...(queryParams && queryParams),
        });
        this.setState({
          activeMainTab: item,
          activeScene: item.TABS[0].id,
        });
      }
    });
  };

  changeScene = (id) => {
    const { activeMainTab } = this.state;
    const newActiveMainTab = MAIN_TABS.find(({id: mainId}) => mainId ===id);
    this.setNewUrl({ tab: id });
    this.setState({
      activeScene: id,
    });

    if(newActiveMainTab && activeMainTab.id !== newActiveMainTab.id) {
      this.setState({
        activeMainTab: newActiveMainTab,
      });

    }
  }

  setNewUrl = (newParams) => {
    const search = window ? window.location.search : '';
    const query = search ? queryToObject(search) : {};
    const newQuery = { ...query, ...newParams };
    if (newQuery.tab !== 'proofs') {
      newQuery.imageId = false;
    }
    updateURL(newQuery);
  };

  logout = () => {
    const { logout } = this.props;
    logout();
    document.location.href = home;
  };

  getQuery = () => {
    const { query } = this.props;
    try {
      const params = {};
      const urlParams = new URLSearchParams(window.location.search);
      urlParams.forEach((value, key) => {
        params[key] = value;
      });
      return params;
    } catch (e) {
      return query || {};
    }
  };

  sortedActiveTab = (sortActiveTabs) => {
    const { activeMainTab } = this.state;
    if (sortActiveTabs) return sortActiveTabs.component;
    return activeMainTab.TABS[0].component;
  };

  render() {
    const { subscribe } = this.props;
    const { activeScene, activeMainTab, marginSize } = this.state;
    const query = this.getQuery();
    const { TABS: activeTabs } = activeMainTab;
    const sortActiveTabs = activeTabs.find(({ id }) => id === activeScene);
    const Components = this.sortedActiveTab(sortActiveTabs);

    return (
      <main className={b()}>
        <div
          className={b('container')}
          id="account-container"
          style={{ marginTop: `${marginSize}px` }}
        >
          <div className={b('tabs-wrap')}>
            {(activeMainTab.text === MY_ORDERS || activeMainTab.text === TRACK_ORDER ) && (
              <div className={b('detail-orders-list')}>
                <NewSelectOrders
                  changeScene={this.changeScene}
                  query={query}
                  setNewUrl={this.setNewUrl}
                  onlyEnhance={activeMainTab.text === ENHANCEMENTS}
                  isCloud
                />
              </div>
            )}
            <AccountTabs
              tabs={MAIN_TABS}
              activeTab={activeMainTab.id}
              clickHandler={(id, imageId) => {
                this.changeMainScreen(id, { ...imageId, order: false }, true);
              }}
              withoutLine
              isMain
              statusCloud={subscribe}
            />
            {activeScene && activeMainTab.TABS.length > 1 && (
              <AccountTabs
                tabs={activeTabs}
                activeTab={activeScene}
                clickHandler={this.changeScene}
                activeSecondTab={activeTabs[0].id}
                isMobile
                withoutLine
                statusCloud={subscribe}
              />
            )}
          </div>
          <section className={b('second-tabs')}>
            {activeScene && activeMainTab.TABS.length > 1 && (
              <AccountTabs
                tabs={activeTabs}
                activeTab={activeScene}
                clickHandler={this.changeScene}
                activeSecondTab={activeTabs[0].id}
                isSubTabs
                statusCloud={subscribe}
              />
            )}
            <div className={b('scene')}>
              {activeScene && (
                <Components
                  query={query}
                  setNewUrl={this.setNewUrl}
                  changeScene={this.changeScene}
                  redirect={this.changeMainScreen}
                  onRef={(e) => {
                    this.child = e;
                  }}
                />
              )}
            </div>
          </section>
          <button className={b('logout-btn')} type="button" onClick={this.logout}>
            <LogoutIcon className={b('logout-icon')} />
            {LOGOUT_TEXT}
          </button>
        </div>
      </main>
    );
  }
}

AccountContent.propTypes = propTypes;
AccountContent.defaultProps = defaultProps;

const actions = {
  logout: removeToken,
  getOrder: loadOrderDetails,
  getOrderTrack: loadOrderTrack,
  getOrderImages: loadOrderImages,
  getPromo: getPromoSection,
  getProjectList: getProjectListAction,
};

const stateProps = (state) => ({
  ordersList: ordersListSelect(state),
  orderDetails: orderDetailsSelect(state),
  token: getToken(state),
  images: orderImagesSelect(state),
  proofsImages: proofsImagesSelector(state),
  promoSection: promoSectionSelector(state),
  subscribe: subscriptionSelector(state),
});

export default connect(stateProps, actions)(AccountContent);
