import React, { Component } from 'react';
import axios from 'axios';
import PropTypes from 'prop-types';

import URL from 'src/redux/urls';
import { connect } from 'react-redux';
import ROUTES from 'src/constants/routes';
import preloadOrdersList from 'src/utils/preloadOrdersList';
import preloadPaymentInfo from 'src/utils/preloadPaymentInfo';
import preloadOrderImages from 'src/utils/preloadOrderImages';
import preloadPlan from 'src/utils/preloadPlan';
import preloadPrice from 'src/utils/preloadPrice';
import preloadCloudStatus from 'src/utils/preloadCloudStatus';
import { wrapper } from 'src/redux/store';
import { NextSeo } from 'next-seo';
import dynamic from 'next/dynamic';
import { removeToken } from 'src/redux/auth/actions';
import { loadPriceList } from 'src/redux/prices/actions';
import { setCloudStatus, setPlanIdAction } from 'src/redux/myCloud/actions';
import { loadOrdersList, setEnhanceOrderList, setOrderImages } from 'src/redux/ordersList/actions';
import { getPaymentInfo } from 'src/redux/shared/actions';
import { getProductsAction } from 'src/redux/photoArt/actions';
import AccountContent from './content';

const Navigation = dynamic(() => import('src/components/shared/Navigation'));
const Footer = dynamic(() => import('src/components/shared/Footer'));

const { loginPage } = ROUTES;

const defaultProps = {
  query: {},
};

const propTypes = {
  query: PropTypes.shape({}),
};

class Account extends Component {
  static getInitialProps = wrapper.getInitialPageProps(store => async ({ res, req }) => {
    const { getState, dispatch } = store;
    const {
      query: {
        main,
        tab,
        step,
        image,
        order,
      } = {},
    } = req || {};
    const props = await getState();
    const { auth: { token } } = props;
    const redirectLink = `${loginPage}?next=account`;

    if (!token) {
      if (res) {
        res.redirect(redirectLink);
        res.end();
        return {};
      }
      document.location.href = redirectLink;
    }

    const [
      products,
    ] = await Promise.all([
      preloadPrice(URL.PRODUCT),
    ]);

    await dispatch(loadPriceList(products));

    if (token) {
      const daysRemaining = await preloadCloudStatus(URL.MEMOYA_CLOUD_STATUS, token);
      await dispatch(setCloudStatus({ ...daysRemaining }));
    } else {
      await dispatch(setCloudStatus({ daysRemaining: 0 }));
    }

    if (token) {
      const ordersList = await preloadOrdersList(URL.ORDERS_LIST, token);
      dispatch(loadOrdersList(ordersList));
    }

    if (token) {
      const ordersList = await preloadOrdersList(URL.ORDERS_PROOFS_LIST, token, { type: 'enhance' });
      await dispatch(setEnhanceOrderList(ordersList));
    }

    if (token) {
      const { data: [plan] = [] } = await preloadPlan(URL.SUBSCRIPTION_PLAN, token) || {};
      const { id: planId = 0, paypalPlanId = '0' } = plan || {};
      await dispatch(setPlanIdAction({ planId, paypalPlanId }));
    }

    if (order && token) {
      const { data: images } = await preloadOrderImages(`${URL.ORDER}/${order}/images`, token);
      await dispatch(setOrderImages(images));
    }

    try {
      const { data: { creditCard, id } } = await preloadPaymentInfo(URL.USER_PAYMENT_INFO, token);
      await dispatch(getPaymentInfo({ ...creditCard, id }));
    } catch (error) {
      console.log('error', error);
    }

    try {
      const { data: { data: photoArtProducts } } = await axios.get(`${URL.PHOTO_ART_PRODUCT}/?filter={"isPreview": false, "turnOff": true}`);
      await dispatch(getProductsAction({ data: photoArtProducts }));
    } catch (error) {
      console.log('error', error);
    }
    return {
      query: {
        main,
        tab,
        step,
        image,
        order,
      },
    };
  })

  render() {
    const { query } = this.props;
    return (
      <>
        <NextSeo
          noindex
        />
        <Navigation />
        <AccountContent query={query} />
        <Footer blog={false} />
      </>
    );
  }
}

const actions = {
  logout: removeToken,
};

Account.propTypes = propTypes;
Account.defaultProps = defaultProps;

export default connect(null, actions)(Account);
