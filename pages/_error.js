import React, { Component } from 'react';
import PropTypes from 'prop-types';
import bem from 'src/utils/bem';
import ERRORS from 'src/constants/errors';
import DICTIONARY from 'src/constants/dictionary';
import styles from 'src/styles/index.module.scss';
import dynamic from "next/dynamic";

const Navigation = dynamic(() => import('src/components/shared/Navigation'));
const NotFindPageIcon = dynamic(() => import('src/components/svg/NoFoundPage'));
const ColorButton = dynamic(() => import('src/components/buttons/ColorButton'));

const b = bem('error-page', styles);

const { MESSAGE_404 } = ERRORS;
const { HOME_PAGE } = DICTIONARY;

const propTypes = {
  statusCode: PropTypes.number,
};

const defaultProps = {
  statusCode: null,
};

class Error extends Component {
  static getInitialProps({ res, err }) {
    // eslint-disable-next-line no-nested-ternary
    const statusCode = res ? res.statusCode : err ? err.statusCode : null;
    return { statusCode };
  }

  render() {
    const { statusCode } = this.props;
    return (
      <>
        <Navigation />
        <main className={b()}>
          <div className={b('wrapper')}>
            <div className={b('wrapper-main')}>
              <NotFindPageIcon statusCode={statusCode} className={b('wrapper-main-icon')} />
              <p className={b('description')}>{MESSAGE_404}</p>
              <a href="/">
                <ColorButton text={HOME_PAGE} className={b('btn')} />
              </a>
            </div>
          </div>
        </main>
      </>
    );
  }
}

Error.propTypes = propTypes;
Error.defaultProps = defaultProps;

export default Error;
