import React, { Component } from 'react';
import { NextSeo } from 'next-seo';
import preloadPrice from 'src/utils/preloadPrice';
import URL from 'src/redux/urls';
import dynamic from 'next/dynamic';
import { wrapper } from 'src/redux/store';
import { loadPriceList } from 'src/redux/prices/actions';
import Content from './content';
import TERMS from '../../pagesContent/faq/constants';

const Navigation = dynamic(() => import('src/components/shared/Navigation'));
const Footer = dynamic(() => import('src/components/shared/Footer'));

const { SEO_TITLE, SEO_DESCRIPTION } = TERMS;

export default class Faq extends Component {
  static getInitialProps = wrapper.getInitialPageProps(store => async () => {
    const { dispatch } = store;

    const [products] = await Promise.all([preloadPrice(URL.PRODUCT)]);
    await dispatch(loadPriceList(products));
  })

  render() {
    return (
      <>
        <NextSeo
          title={SEO_TITLE}
          description={SEO_DESCRIPTION}
          canonical="https://*******.com/restore-retouch-photo-service/"
          languageAlternates={[
            {
              hrefLang: 'en-us',
              href: 'https://*******.com/restore-retouch-photo-service/',
            },
          ]}
        />
        <Navigation />
        <Content />
        <Footer />
      </>
    );
  }
}
