import React from 'react';
import bem from 'src/utils/bem';
import dynamic from "next/dynamic";
import TERMS from '../../pagesContent/faq/constants';
import styles from './index.module.scss';

const KnowAbout = dynamic(() => import('src/components/elements/KnowAbout'));

const {
  TITLE,
  FAQ_ITEMS,
} = TERMS;
const b = bem('faq', styles);

function Content() {
  return (
    <main className={b()}>
      <h1 className={b('faq-title')}>
        {TITLE}
      </h1>
      {FAQ_ITEMS.map(({ title, subtitles }) => (
        <div className={b('faq-btn')}>
          <KnowAbout
            title={title}
            withoutPadding
            children={subtitles.map(({ subtitle, description }) => (
              <div className={b('faq-btn')}>
                <KnowAbout
                  title={subtitle}
                  isNestedItem
                  children={description}
                />
              </div>
          ))}
          />
        </div>
    ))}
    </main>
  )
}

export default Content;
