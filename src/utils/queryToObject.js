const queryToObject = (query) => {
  const search = query.substring(1);
  const object = JSON.parse(
    `{"${decodeURI(search).replace(/"/g, '\\"').replace(/&/g, '","').replace(/=/g, '":"')}"}`,
  );
  return object;
};

export default queryToObject;
