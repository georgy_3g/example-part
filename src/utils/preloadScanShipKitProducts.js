import axios from 'axios';
import URL from 'src/redux/urls';

const preloadScanShipKitProducts = () =>
  axios
    .get(URL.GET_EASY_SCAN_SHIP_KIT)
    .then(({ data }) => data)
    .catch(({ response }) => {
      console.log('error', response);
    });

export default preloadScanShipKitProducts;
