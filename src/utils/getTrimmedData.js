const getTrimmedData = (values) =>
  Object.fromEntries(Object.entries(values).map(([key, value]) => [key, value.trim()]));

export default getTrimmedData;
