const formattedToCardNumber = (cardNumber, withText) => {
  if (withText) {
    return `Card ending in ${cardNumber.substr(cardNumber.length - 4, 4)}`;
  }
  return `XXXX XXXX XXXX ${cardNumber.substr(cardNumber.length - 4, 4)}`;
};

export default formattedToCardNumber;
