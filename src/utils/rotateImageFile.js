import { IMAGE_TYPE_JPEG } from 'src/constants';

const rotateImageFile = (img) => {
  const { naturalWidth, naturalHeight } = img;
  const canvas = document.createElement('canvas');
  canvas.width = naturalHeight;
  canvas.height = naturalWidth;

  const ctx = canvas.getContext('2d');

  ctx.translate(naturalHeight / 2, naturalWidth / 2);
  ctx.rotate(Math.PI / 2);
  ctx.drawImage(img, -naturalWidth / 2, -naturalHeight / 2);
  ctx.rotate(-Math.PI / 2);
  ctx.translate(-naturalHeight / 2, -naturalWidth / 2);
  ctx.save();

  return canvas.toDataURL(IMAGE_TYPE_JPEG);
};

export default rotateImageFile;
