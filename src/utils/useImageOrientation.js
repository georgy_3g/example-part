import { useState, useEffect } from 'react';

const useImageOrientation = (src) => {
  const [size, setOrientation] = useState(false);

  useEffect(() => {
    const photoImg = new Image();
    photoImg.onload = () => {
      const { naturalWidth, naturalHeight } = photoImg;
      setOrientation({
        isVertical: naturalHeight > naturalWidth,
        naturalHeight,
        naturalWidth,
      });
    };

    photoImg.src = src;
  }, [src]);

  return {
    ...size,
  };
};

export default useImageOrientation;
