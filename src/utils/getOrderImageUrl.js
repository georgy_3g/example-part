const getOrderImageUrl = (api, id) => `${api}/${id}/communication`;

export default getOrderImageUrl;
