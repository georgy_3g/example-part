import { DIGITIZE_PRODUCTS } from 'src/constants';
import checkCoupon from './checkCoupon';
import isSaleOption from './isSaleOption';

const digitizeOptions = ['archival_dvd_set', '*******_safe', 'personalize'];
const typesProducts = ['photo_restoration', 'photo_colorization', 'photo_retouching'];

const getSaleValue = (valueType, couponValue, sumToSale) => {
  if (valueType !== 'percentage' && sumToSale < couponValue) {
    return 100;
  }
  return valueType === 'percentage' ? (couponValue || 0) : (couponValue || 0) / sumToSale * 100;
};

const orderSummaryInfo = ({
  orders,
  formattedPrice,
  currency,
  selectPrice,
  orderRushStatus,
  TAX,
  EXPRESS_DELIVERY_PRICE,
  isActive,
  couponValue,
  entitledProductIds: entitledProduct,
  valueType,
  targetType,
  isNeedTax,
  withOutShipping,
  isGiftCard,
  isOrderIncludeAllProducts,
  clearCoupon = () => {  },
}) => {
  const entitledProductIds = entitledProduct || [];

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const { error, ...filterOrders } = orders;

  const allProducts = Object.values(filterOrders).reduce((acc, item) => {
    if (item && item.length) {
      return [...acc, ...item];
    }
    return acc;
  }, []);

  const productAndOptions = allProducts.reduce((acc, item) => {
    const { options, typeProduct } = item || {};
    const newArr = [...acc, item];
    if (options && options.length) {
      options.forEach((itm) => {
        if (itm.quantity) {
          const { customText, isMatting } = item;
          const isPhotoArt = typeProduct === 'photo_art';
          newArr.push({ ...itm, ...(isPhotoArt ? { isMatting, customText, typeProduct } : {}) });
        }
      });
    }
    return newArr;
  }, []);


  const isGoodPromo = targetType === 'cart' || checkCoupon(entitledProductIds, productAndOptions, isOrderIncludeAllProducts);

  if (!isGoodPromo && couponValue && allProducts.length) {
    clearCoupon();
  }

  const optionAcc = (
    value,
    key,
    sum,
    saleProductIds,
  ) => (
    value.reduce((accumulator, item) => {
      const {
        [sum]: optionsSum,
        saleProductsSum,
      } = formattedPrice(item, currency, selectPrice[key], saleProductIds);
      const { total, saleTotal } = accumulator;
      return { total: total + optionsSum, saleTotal: saleTotal + saleProductsSum };
    }, { total: 0, saleTotal: 0 })
  );

  const { sum: summProducts, saleSum: saleProductsSum } = Object.entries(filterOrders)
    .filter(value => value[1].length !== 0)
    .reduce((acc, [key, value]) => {
      const { sum, saleSum } = acc;
      const { total, saleTotal } = optionAcc(value, key, 'optionsSum', entitledProductIds);
      return { sum: sum + total, saleSum: saleSum + saleTotal };
    }, { sum: 0, saleSum: 0 });

  const giftSum = Object.entries(filterOrders)
    .filter(value => value[1].length !== 0)
    .reduce((acc, [key, value]) => {
      const { total } = optionAcc(value, key, 'giftOptionsSum', entitledProductIds);
      return acc + total;
    }, 0);

  const scanShipKitSum = (orders.easy_scan_ship_kit && orders.easy_scan_ship_kit.length)
    ? selectPrice.easy_scan_ship_kit.price
    : null;

  const returnShipKitSum = (orders.return_shipping && orders.return_shipping.length)
    ? selectPrice.return_shipping.price
    : null;

  const homeShipKitSum = (orders.easy_home_ship_kit && orders.easy_home_ship_kit.length)
    ? selectPrice.easy_home_ship_kit.price
    : null;

  const freeShippingSum = (orders.free_shipping && orders.free_shipping.length)
    ? selectPrice.free_shipping.price
    : null;

  const localPickupSum = (orders.local_pickup && orders.local_pickup.length)
    ? selectPrice.local_pickup.price
    : null;

  const storeDropOfSum = (
    orders.store_drop_of && orders.store_drop_of.length && selectPrice.store_drop_of.price === 0)
    ? selectPrice.store_drop_of.price
    : null;

  const inStoreSum = (
    orders.in_store && orders.in_store.length && selectPrice.in_store.price === 0)
    ? selectPrice.in_store.price
    : null;

  const digitalDownloadSum = (
    orders.digital_download
    && orders.digital_download.length
    && selectPrice.digital_download.price === 0)
    ? selectPrice.digital_download.price
    : null;

  const trialShipKitSum = (
    orders.trial_ship_kit && orders.trial_ship_kit.length && selectPrice.trial_ship_kit.price === 0)
    ? selectPrice.trial_ship_kit.price
    : null;

  const shippingSum = (scanShipKitSum || 0) + (returnShipKitSum || 0)
    + (homeShipKitSum || 0) + (freeShippingSum || 0) + (localPickupSum || 0);

  const summAll = summProducts - shippingSum;

  const saleCardSum = isGiftCard ? summProducts : summAll;

  const sumToSale = targetType === 'cart' ? saleCardSum : saleProductsSum;

  const isCartSale = targetType === 'cart';

  const saleValue = getSaleValue(valueType, couponValue, sumToSale);

  const orderPrice = summAll + giftSum + (!withOutShipping ? shippingSum : 0);

  const taxProductsSum = Object.entries(filterOrders)
    .filter(value => value[1].length !== 0)
    .reduce((acc, [key, value]) => {
      if (DIGITIZE_PRODUCTS.includes(key)) {
        const orderOptionsSum = value.reduce((sum, { options = [] }) => {
          const orderOptions = (options || []).reduce((optionList, item) => {
            const { name } = item;
            if (digitizeOptions.includes(name)) {
              return { ...optionList, [name]: item };
            }
            return optionList;
          }, {});
          const {
            archival_dvd_set: archivalDvdSet,
            *******_safe: *******Safe,
            personalize,
          } = orderOptions;
          const { price: dvdPrice = 0, quantity: dvdQuantity = 0 } = archivalDvdSet || {};
          const { price: usbPrice = 0, quantity: usbQuantity = 0 } = *******Safe || {};
          const { price: personalizePrice = 0, quantity: personalizeQt = 0 } = personalize || {};


          if (isActive && isGoodPromo) {
            const { options: itemOptions = [] } = value[0] || {};
            const isDvdSale = isCartSale
            || isSaleOption({
              arrayProducts: entitledProductIds,
              item: value[0],
              option: archivalDvdSet,
              withOption: itemOptions.length,
            });
            const isUsbSale = isCartSale
             || isSaleOption({
               arrayProducts: entitledProductIds,
               item: value[0],
               option: *******Safe,
               withOption: itemOptions.length,
             });
            const isPersonalizeSale = isCartSale
            || isSaleOption({
              arrayProducts: entitledProductIds,
              item: value[0],
              option: personalize,
              withOption: itemOptions.length,
            });

            const personalizeSum = personalizePrice * usbQuantity
              * (isPersonalizeSale ? (100 - saleValue) / 100 : 1);

            return sum + (
              dvdPrice * dvdQuantity * (isDvdSale ? (100 - saleValue) / 100 : 1)
              + usbPrice * usbQuantity * (isUsbSale ? (100 - saleValue) / 100 : 1)
            + (personalizeQt ? personalizeSum : 0));
          }
          const personalizeSum = personalizePrice * usbQuantity;
          return sum + (
            dvdPrice * dvdQuantity
            + usbPrice * usbQuantity
          + (personalizeQt ? personalizeSum : 0));
        }, 0);
        return acc + orderOptionsSum;
      }
      if (key === 'photo_art' || key === 'gift_card') {
        const { total, saleTotal } = optionAcc(value, key, 'optionsSum', entitledProductIds);
        if (isActive && isGoodPromo) {
          if (targetType === 'cart') {
            return acc + (total - total * saleValue / 100);
          }
          return acc + (saleTotal - saleTotal * saleValue / 100) + (total - saleTotal);
        }
        return acc + total;
      }
      if (typesProducts.includes(key)) {
        const sumEnchanceOption = value.reduce((sum, item) => {
          const { options = [] } = item || {};
          return sum + (options || []).reduce((accum, optionProduct) => {
            const { quantity, optionId, name } = optionProduct;
            if (name !== 'enhance_prints') {
              return accum;
            }
            const { options: selectOption = [] } = selectPrice[key] || {};
            const { price = 0 } = selectOption.find(({ id }) => id === optionId) || {};
            if (isActive && isGoodPromo) {
              const isSale = isCartSale || isSaleOption({
                arrayProducts: entitledProductIds,
                option: optionProduct,
                withOption: options.length,
                item,
              });

              return accum + price * quantity * (isSale ? (100 - saleValue) / 100 : 1);
            }
            return accum + price * quantity;
          }, 0);
        }, 0);

        return acc + sumEnchanceOption;
      }
      return acc;
    }, 0);
  const taxSum = isNeedTax ? (taxProductsSum * TAX) : 0;
  const expressOrderSum = orderRushStatus ? summAll * EXPRESS_DELIVERY_PRICE : 0;
  const totalSumm = orderPrice + expressOrderSum + taxSum;
  const checkTotal = totalSumm >= sumToSale * saleValue / 100
    ? totalSumm - sumToSale * saleValue / 100
    : 0;
  const sumWithDiscount = (isActive && isGoodPromo)
    ? checkTotal
    : totalSumm;
  const discountSum = (isActive && isGoodPromo) ? (sumToSale * saleValue / 100).toFixed(2) : 0;

  return {
    totalInfo: {
      summAll: summAll.toFixed(2),
      expressOrderSum: expressOrderSum > 0 ? expressOrderSum.toFixed(2) : null,
      giftSum: giftSum > 0 ? giftSum.toFixed(2) : null,
      taxSum: taxSum > 0 ? taxSum.toFixed(2) : null,
      discountSum,
      scanShipKitSum: scanShipKitSum ? scanShipKitSum.toFixed(2) : null,
      homeShipKitSum: homeShipKitSum ? homeShipKitSum.toFixed(2) : null,
      returnShipKitSum: Number.isInteger(returnShipKitSum) ? returnShipKitSum.toFixed(2) : null,
      freeShippingSum: orders.free_shipping && orders.free_shipping.length
        ? freeShippingSum.toFixed(2)
        : null,
      localPickupSum: localPickupSum ? localPickupSum.toFixed(2) : null,
      storeDropOfSum: storeDropOfSum !== null ? storeDropOfSum.toFixed(2) : null,
      inStoreSum: inStoreSum !== null ? inStoreSum.toFixed(2) : null,
      digitalDownloadSum: digitalDownloadSum !== null ? digitalDownloadSum.toFixed(2) : null,
      trialShipKitSum: trialShipKitSum !== null ? trialShipKitSum.toFixed(2) : null,
    },
    sumWithDiscount: sumWithDiscount.toFixed(2),
  };
};

export default orderSummaryInfo;
