const reduxCatch = (errorHandler) => {
  return (store) => {
    return (next) => {
      return (action) => {
        try {
          return next(action);
        } catch (err) {
          errorHandler(err, store.getState, action, store.dispatch);
          return err;
        }
      };
    };
  };
}

export default reduxCatch;
