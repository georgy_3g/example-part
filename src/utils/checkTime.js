import utcToZonedTime from 'date-fns-tz/utcToZonedTime';
import zonedTimeToUtc from 'date-fns-tz/zonedTimeToUtc';
import getDay from 'date-fns/getDay';
import getHours from 'date-fns/getHours';

const checkDate = ({
  weekDaysTimeStart,
  weekDaysTimeEnd,
  holidaysTimeStart,
  holidaysTimeEnd,
  workingHoliday,
}) => {
  const date = zonedTimeToUtc(new Date());
  const timeZone = 'America/New_York';
  const estNow = utcToZonedTime(date, timeZone)
  const today = getDay(estNow);
  const hours = getHours(estNow);
  if (!today || today === 7) {
    return false;
  }
  if (today === workingHoliday) {
    return hours >= holidaysTimeStart && hours < holidaysTimeEnd;
  }
  return hours >= weekDaysTimeStart && hours < weekDaysTimeEnd;
};

export default checkDate;
