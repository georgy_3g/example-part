import axios from 'axios';
import getRollbar from './getRollbar';

const rollbar = getRollbar();

const preloadCloudStatus = (api, token) =>
  axios
    .get(`${api}`, {
      headers: { Authorization: `Bearer ${token}` },
    })
    .then(({ data: { data } = {} }) => data)
    .catch((response) => {
      rollbar.error('preload cloud status', response);
      return 0;
    });

export default preloadCloudStatus;
