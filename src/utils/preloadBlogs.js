import axios from 'axios';

const limit = 6;
const offset = 0;
const preloadBlogs = (api, categoryId) =>
  axios
    .get(
      `${api}?pagination[limit]=${limit}&pagination[offset]=${offset}&filters[categoryId]=${categoryId}`,
    )
    .then(({ data }) => data)
    .catch(({ response }) => {
      console.log('error', response);
    });

export default preloadBlogs;
