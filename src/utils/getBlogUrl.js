const getBlogUrl = (api, limit, offset) =>
  `${api}?pagination[limit]=${limit}&pagination[offset]=${offset}`;

export default getBlogUrl;
