const convertToPrice = (value = 0) => {
  if (value < 1 && value > 0) {
    return `${(value * 100).toFixed(0)}¢`;
  }
  return `$${value.toFixed(value % 1 ? 2 : 0)}`;
};

export default convertToPrice;
