import axios from 'axios';

const preloadWizardCards = (api, product) => axios.get(`${api}?product=${product}`);

export default preloadWizardCards;
