const getOrderMessageUrl = (api, id) => `${api}/${id}/changes`;

export default getOrderMessageUrl;
