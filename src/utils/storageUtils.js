const setLocalStorage = (values) => {
  const valueList = Object.keys(values);
  valueList.map((item) => localStorage.setItem(item, values[item]));
};

const setSessionStorage = (values) => {
  const valueList = Object.keys(values);
  valueList.map((item) => sessionStorage.setItem(item, values[item]));
};

const removeStorageValue = (name) => {
  localStorage.removeItem(name);
  sessionStorage.removeItem(name);
};

const getStorageValue = (name) => localStorage.getItem(name) || sessionStorage.getItem(name);

export { setLocalStorage, setSessionStorage, getStorageValue, removeStorageValue };
