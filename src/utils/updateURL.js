const updateURL = (query) => {
  const queryString = Object.keys(query)
    .map((key) => (query[key] ? `${key}=${query[key]}` : false))
    .filter((item) => item)
    .join('&');
  if (window && queryString.length) {
    window.history.pushState(null, null, `${window.location.pathname}?${queryString}`);
  }
};

export default updateURL;
