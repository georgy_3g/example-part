import axios from 'axios';
import { GOOGLE_API_KEY, GOOGLE_GEOCODING_API } from 'src/config';

const getZipCode = ({ lat, lng }) =>
  axios
    .get(GOOGLE_GEOCODING_API, {
      params: {
        latlng: `${lat},${lng}`,
        key: GOOGLE_API_KEY,
      },
    })
    .then(({ data }) => {
      const {
        results: [{ address_components: addressComponents }],
      } = data;
      return addressComponents.reduce((acc, { types, long_name: code }) => {
        if (types[0] === 'postal_code') {
          return code;
        }
        return acc;
      }, '');
    })
    .catch(( response ) => {
      console.log('error', response);
    });

export default getZipCode;
