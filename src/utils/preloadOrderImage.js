import axios from 'axios';

const preloadOrderImage = (api, token, img) =>
  img
    ? axios
        .get(`${api}`, {
          headers: { Authorization: `Bearer ${token}` },
        })
        .then(({ data }) => data)
        .catch(({ response }) => {
          console.log('error', response);
        })
    : {};

export default preloadOrderImage;
