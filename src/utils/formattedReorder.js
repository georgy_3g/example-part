const formattedReorder = (oldOrder) => {
  const {
    productId,
    isGift,
    customText,
    product,
    quantity,
    giftOptions,
    giftCardOptions,
    orderImages,
    options,
    matting,
    mattingColor,
    shipKitMaterial,
    shipKitSize,
  } = oldOrder;

  if (product.name === 'photo_art') {
    const images = orderImages.length && orderImages.map((item) => item.id);

    return {
      title: product.displayName,
      typeProduct: product.name,
      id: options[0].product.id,
      price: options[0].product.price,
      quantity,
      isGift,
      productId,
      material: product.frameMaterial.name,
      size: options[0].product.frameSize.name,
      color: options[0].product.frameColor.name,
      images,
      options: [
        {
          optionId: options[0].product.id,
          quantity: 1,
        },
      ],
      customText,
      isMatting: Boolean(matting && mattingColor),
      framePhotoMattingId: matting && matting.id ? matting.id : null,
      framePhotoMattingColorId: mattingColor && mattingColor.id ? mattingColor.id : null,
      mattingName: mattingColor && mattingColor.name ? mattingColor.name : 'None',
      mattingPrice: matting && matting.price ? matting.price : 0,
    };
  }

  if (product.name === 'easy_scan_ship_kit') {
    return {
      title: product.displayName,
      typeProduct: product.name,
      price: product.price,
      quantity,
      isGift,
      productId,
      isMatting: false,
      shipKitMaterial,
      shipKitSize,
      delivery: 15,
    };
  }

  if (product.name === 'gift_card') {
    return {
      title: product.displayName,
      typeProduct: product.name,
      price: product.price,
      quantity,
      isGift: false,
      isMatting: false,
      imgId: null,
      productId,
      giftCardOptions,
    };
  }

  const newOptions = options.map(({ product: option, quantity: qty }) => ({
    optionId: option.id,
    quantity: ['enhance_my_transfer', 'cloud_download'].includes(option.name) ? !!qty : qty,
    price: option.price,
    displayName: option.display_name,
    name: option.name,
  }));
  const images = orderImages === true && orderImages.map((item) => item.id);
  return {
    productId,
    quantity,
    text: customText,
    title: product.displayName,
    typeProduct: product.name,
    isGift,
    giftOptions,
    giftCardOptions,
    options: newOptions,
    images,
    isMatting: false,
  };
};

export default formattedReorder;
