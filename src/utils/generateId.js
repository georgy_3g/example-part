const generateId = (arr) => (arr.length > 0 ? arr[arr.length - 1].id + 1 : arr.length + 1);

export default generateId;
