import axios from 'axios';

const preloadPlan = (api, token) =>
  axios
    .get(`${api}`, {
      headers: { Authorization: `Bearer ${token}` },
    })
    .then(({ data }) => data)
    .catch(({ response }) => {
      console.log('error', response);
    });

export default preloadPlan;
