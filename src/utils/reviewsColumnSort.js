const reviewsColumnSort = (data) => {
  const reviewsColumns = [[], [], []];
  let parseIndex = 0;
  if (data && data.length > 0) {
    data.forEach((item) => {
      if (parseIndex === 3) {
        parseIndex = 0;
      }
      reviewsColumns[parseIndex].push(item);
      parseIndex += 1;
    });
  }
  return reviewsColumns;
};

export default reviewsColumnSort;
