import isIncludesOption from './isIncludesOption';

const isSaleOption = ({
  arrayProducts,
  option,
  item,
  withOption,
}, withArtOption) => {
  const isFindSale = arrayProducts.find(({ options, id }) => {
    const includerPromoOptions = isIncludesOption(options, item);
    return ((withOption && includerPromoOptions)
    || ((!options || !options.length) && !item.options.length))
    && id === item.productId && item.quantity;
  });

  const { isMatting, customText } = item;

  const isSaleOptions = option
    ? arrayProducts.find(({
      options,
    }) => (options && options.length
      ? options.find(({
        id: idOption,
        isMatting: isMattingOption,
        isPersonalize: isPersonalizeOption,
      }) => (option.optionId || option.id) === idOption
        && option.quantity
        && Boolean(isMatting) === isMattingOption
        && Boolean(customText) === isPersonalizeOption)
      : false))
    : {};

  if (withArtOption) {
    const saleMatting = isSaleOptions
    && isSaleOptions.options
    && isSaleOptions.options[0].isMatting;
    const salePersonalize = isSaleOptions
    && isSaleOptions.options
    && isSaleOptions.options[0].isPersonalize;
    return isFindSale && isSaleOptions
      ? {
        isSaleProduct: true,
        isMatting: saleMatting,
        isPersonalize: salePersonalize,
      }
    : { isSaleProduct: false };
  }

  return isFindSale && isSaleOptions;
};

export default isSaleOption;
