import axios from 'axios';

const preloadUserData = (api, token) =>
  axios
    .get(`${api}`, {
      headers: { Authorization: `Bearer ${token}` },
    })
    .then(({ data }) => data)
    .catch(({ response }) => {
      console.log('error', response);
    });

export default preloadUserData;
