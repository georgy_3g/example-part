const isIncludesOption = (arrayOptions, item) => {
  const {
    options, customText, isMatting, name,
  } = item;

  return (arrayOptions && arrayOptions.length
    ? arrayOptions.every(({
      id: idElement,
      isPersonalize: isPersonalizeOption,
      isMatting: isMattingOption,
    }) => options.find(({
      id: idOption,
      optionId,
      quantity,
    }) => (name === 'photo_art'
      ? (optionId || idOption) === idElement
        && quantity
        && Boolean(isPersonalizeOption) === Boolean(customText)
        && Boolean(isMatting) === Boolean(isMattingOption)
      : (optionId || idOption) === idElement && quantity)))
  : true);
};

export default isIncludesOption;
