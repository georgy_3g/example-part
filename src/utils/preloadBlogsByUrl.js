import axios from 'axios';

const preloadBlogByUrl = (api, limit, offset) => (
  axios.get(`${api}?pagination[limit]=${limit}&pagination[offset]=${offset}`)
    .then(({ data }) => data)
    .catch(({ response }) => {
      console.log('error', response);
    })
);

export default preloadBlogByUrl;
