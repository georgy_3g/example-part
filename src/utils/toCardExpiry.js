const toCardExpiry = (value) => value.replace(/\//g, '').replace(/_/g, '');

export default toCardExpiry;
