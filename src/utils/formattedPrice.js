import {
  GIFT_CARD_TERMS,
  ENHANCE_TERMS,
  PHOTO_ART_TERMS,
  EASY_SCAN_SHIP_KIT_TERMS,
  DIGITIZE_TERMS,
} from 'src/terms';

import { SHIPPING_PRODUCTS } from 'src/constants';

const { GIFT_CARD_TYPE, GIFT_CARD_VALUE, GIFT_CARD } = GIFT_CARD_TERMS;
const { RESTORATION_PRODUCT, COLORIZATION_PRODUCT, RETOUCHING_PRODUCT } = ENHANCE_TERMS;
const { PHOTO_PRODUCT } = DIGITIZE_TERMS;

const {
  EASY_SCAN_SHIP_KIT_TYPE,
  EASY_SCAN_SHIP_KIT_SIZE,
  EASY_SCAN_SHIP_KIT_MATERIAL,
  EASY_SCAN_SHIP_KIT,
} = EASY_SCAN_SHIP_KIT_TERMS;

const {
  PHOTO_ART_PRODUCT,
  PHOTO_ART,
  PHOTO_ART_SIZE,
  PHOTO_ART_COLOR,
  PHOTO_ART_COLLECTION,
  PHOTO_ART_MAT,
  PHOTO_ART_PERSONALIZE,
  DISPLAY_OPTION,
} = PHOTO_ART_TERMS;

const formattedPrice = (item, currency, prices, entitledProductIds = [], frameMaterials = []) => {
  const {
    quantity,
    title,
    typeProduct,
    isGift,
    giftOptions,
    options,
    color,
    material,
    size,
    customText,
    isMatting,
    mattingPrice,
    mattingName,
    productName,
    productId,
    isTrial,
    minValue,
    isPersonalizeActive,
  } = item;

  const getOptionCustomPrice = (id) => {
    const { options: productOptions } = prices;
    const {
      price: optionPrice = 0,
      additionalDiscount: optionAdditionalDiscount = 0,
    } = productOptions.find(option => option.id === id) || {};
    return optionAdditionalDiscount ? optionPrice - optionAdditionalDiscount : optionPrice;
  }

  if (SHIPPING_PRODUCTS.includes(typeProduct)) {
    return {
      optionsSum: prices.price,
      giftOptionsSum: 0,
      saleProductsSum: entitledProductIds.find(({ id }) => id === productId) ? prices.price : 0,
    };
  }

  if (typeProduct === GIFT_CARD_TYPE) {
    const {
      price: giftPrice,
      additionalDiscount: giftAdditionalDiscount,
    } = item;
    const giftCustomPrice = giftAdditionalDiscount ? giftPrice - giftAdditionalDiscount : giftPrice;
    const orderCheckoutTitle = `${GIFT_CARD}`;
    const orderTitle = `${GIFT_CARD}`;
    const qtInfo = `${GIFT_CARD_VALUE} = ${currency}${item.price}`;
    const sum = quantity * giftCustomPrice;
    const checkoutOrderInfo = {
      quantity: {
        title: orderTitle,
        quantity,
        price: `${currency}${giftCustomPrice.toFixed(2)}`,
        fullPrice: `${currency}${giftPrice.toFixed(2)}`,
        sum: `${currency}${sum.toFixed(2)}`,
      },
    };
    const giftOptionsSum = (giftOptions || {}).addWrapping && isGift ? 15 : 0;

    return {
      orderCheckoutTitle,
      qtInfo,
      orderTitle,
      priceTitle: `${title}:`,
      typeProduct,
      checkoutOrderInfo,
      optionsSum: sum,
      giftOptionsSum,
      saleProductsSum: entitledProductIds.find(({ id }) => id === productId) ? sum : 0,
    };
  }

  if (typeProduct === PHOTO_ART_PRODUCT) {
    const {
      display,
      price: artPrice,
      quantity: artQuantity,
      title: artTitle,
      discount = 0,
    } = item;
    const artCustomPrice = Number(discount) ? Number(artPrice) - Number(discount) / artQuantity : Number(artPrice);
    const sum = quantity * (artCustomPrice + (isMatting ? mattingPrice : 0) + (customText ? 10 : 0));
    const [{ optionId }] = options;

    const { options: optionsPromoPhotoArt, id: idPromoPhotoArt } =
      entitledProductIds.find(({ id }) => id === productId) || {};

    const includeOptionInPromo = optionsPromoPhotoArt && optionsPromoPhotoArt.length;
    const option = includeOptionInPromo
      ? optionsPromoPhotoArt.find(
          ({ id, isMatting: isMattingOption, isPersonalize }) =>
            id === optionId &&
            Boolean(customText) === isPersonalize &&
            isMatting === isMattingOption,
        )
      : false;

    const salePersonalize = option && optionId === option.id && customText && option.isPersonalize;
    const saleMatting = option && optionId === option.id && isMatting && option.isMatting;

    const isAllSale =
      (!options || !options.length) && (!includeOptionInPromo || optionsPromoPhotoArt.length);

    const saleOptionSum =
      includeOptionInPromo && option
        ? artPrice * artQuantity +
          ((salePersonalize || isAllSale) && customText ? 10 * artQuantity : 0) +
          ((saleMatting || isAllSale) && isMatting ? mattingPrice * artQuantity : 0)
        : 0;

    const saleProductsSum =
      includeOptionInPromo && options && options.length
        ? saleOptionSum
        : artPrice * artQuantity;

    const orderCheckoutTitle = `${PHOTO_ART}`;
    const orderTitle = `${PHOTO_ART}`;
    const qtInfo = `${artTitle} x${quantity} = ${currency}${artPrice * quantity}`;
    const mating = isMatting
      ? `${mattingName} x${quantity} = ${currency}${mattingPrice * quantity}`
      : '';
    const personalize = customText ? `x${quantity} = ${currency}${10 * quantity}` : '';
    const checkoutOrderInfo = {
      quantity: {
        title: `${title}`,
        quantity,
        price: `${currency}${artCustomPrice.toFixed(2)}`,
        fullPrice: `${currency}${artPrice.toFixed(2)}`,
        sum: `${currency}${(customText ? sum - 10 : sum).toFixed(2)}`,
        discount,
      },
    };
    const giftOptionsSum = (giftOptions || {}).addWrapping && isGift ? 15 : 0;

    const { name = '' } =
      frameMaterials.find(({ id: idMaterial }) => idMaterial === material) || {};

    const photoArtInfoMat = [
      {
        id: 1,
        title: PHOTO_ART_COLLECTION,
        name,
      },
      {
        id: 2,
        title: PHOTO_ART_SIZE,
        name: size,
      },
      {
        id: 3,
        title: PHOTO_ART_COLOR,
        name: color,
      },
      {
        id: 4,
        title: PHOTO_ART_MAT,
        name: `${mattingName} (${currency}${isMatting ? mattingPrice.toFixed(2) : 0})`,
      },
      {
        id: 5,
        title: PHOTO_ART_PERSONALIZE,
        name: isPersonalizeActive ? `+${currency}${10}` : '+$0',
        summ: customText ? 10 : 0,
      },
      ...(display && display.id
        ? [
            {
              id: 6,
              title: DISPLAY_OPTION,
              name: display.name,
            },
          ]
        : []),
    ];
    const photoArtInfo = photoArtInfoMat.filter((photoArtItem) => {
      const { name: nameFilter } = photoArtItem;
      return ['None ($0)', '+$0', 'N/A'].includes(nameFilter) === false;
    });
    return {
      orderCheckoutTitle,
      qtInfo,
      orderTitle,
      priceTitle: `${title}:`,
      typeProduct,
      checkoutOrderInfo,
      optionsSum: sum,
      giftOptionsSum,
      color,
      material,
      size,
      photoArtInfo,
      mating,
      isMatting,
      personalize,
      customText,
      saleProductsSum: idPromoPhotoArt ? saleProductsSum : 0,
      display: display && display.id ? display.name : '',
    };
  }
  if (typeProduct === EASY_SCAN_SHIP_KIT_TYPE) {
    const {
      price,
      additionalDiscount,
      shipKitMaterial = {},
      shipKitSize = {},
    } = item;
    const productCustomPrice = additionalDiscount ? price - additionalDiscount : price;
    const sum = quantity * productCustomPrice;
    const orderCheckoutTitle = `${EASY_SCAN_SHIP_KIT}`;
    const orderTitle = `${EASY_SCAN_SHIP_KIT}`;
    const giftOptionsSum = isGift && giftOptions && giftOptions.addWrapping ? 15 : 0;
    const checkoutOrderInfo = {
      quantity: {
        title: `${EASY_SCAN_SHIP_KIT}`,
        quantity,
        price: `${currency}${productCustomPrice.toFixed(2)}`,
        fullPrice: `${currency}${price.toFixed(2)}`,
        sum: `${currency}${sum.toFixed(2)}`,
      },
    };
    const easyScanOptions = [
      {
        id: 1,
        title: EASY_SCAN_SHIP_KIT_MATERIAL,
        name: shipKitMaterial.name,
      },
      {
        id: 2,
        title: EASY_SCAN_SHIP_KIT_SIZE,
        name: shipKitSize.name,
      },
    ];

    return {
      checkoutOrderInfo,
      orderCheckoutTitle,
      optionsSum: sum,
      typeProduct,
      orderTitle,
      size: shipKitSize.name,
      material: shipKitMaterial.name,
      easyScanOptions,
      giftOptionsSum,
      saleProductsSum: entitledProductIds.find(({ id }) => id === productId) ? sum : 0,
    };
  }

  const { price, displayName, additionalDiscount } = prices;
  const productCustomPrice = additionalDiscount ? price - additionalDiscount : price;
  const priceList =
    options &&
    options.length > 0 &&
    options.reduce(
      (acc, product) => ({
        ...acc,
        [product.name]: { ...product },
      }),
      {},
    );

  const {
    enhance_my_transfer: enhanceValue = {},
    archival_dvd_set: archivalValue = {},
    cloud_download: cloudValue = {},
    // --- Hidden for the future ---
    // *******_box: *******BoxValue,
    *******_safe: *******SafeValue = {},
    personalize: personalizeValue = {},
  } = priceList;

  if ([RESTORATION_PRODUCT, COLORIZATION_PRODUCT, RETOUCHING_PRODUCT].includes(typeProduct)) {
    const { options: allOptions = [] } = prices || {};
    const orderCheckoutTitle = `${displayName}`;
    const allSum = quantity * productCustomPrice;
    const orderTitle = `${displayName}`;
    const qtInfo = `${displayName} x${quantity} = ${currency}${quantity * productCustomPrice}`;
    const optionPrice = options.length > 0 ? options[0].price : 0;
    const optionName = options.length > 0 ? options[0].displayName : 'Easy Scan Ship Kit';
    const sizeName = options.length > 0 ? options[0].size : '';
    const materialName = options.length > 0 ? options[0].material : '';
    const optionQuantity = options.length > 0 ? 1 : 0;

    const optionsEnchance = options.reduce((acc, option) => {
      const {
        price: getPrice = 0,
        additionalDiscount: optionAdditionalDiscount = 0,
      } = allOptions.find(({ id }) => id === option.optionId) || {};
      const optionCustomPrice = optionAdditionalDiscount
        ? getPrice - optionAdditionalDiscount : price;
      return acc + optionCustomPrice * option.quantity;
    }, 0);

    const saleOptionSum = options.reduce((acc, option) => {
      const {
        price: getPrice = 0,
        additionalDiscount: optionAdditionalDiscount = 0,
      } = allOptions.find(({ id }) => id === option.optionId) || {};
      const optionCustomPrice = optionAdditionalDiscount
        ? getPrice - optionAdditionalDiscount : price;
      if (
        entitledProductIds.some((itm) =>
          option && itm.options && itm.id === productId
            ? itm.options.find(({ id }) => id === option.optionId)
            : false,
        )
      ) {
        return acc + Number(option.quantity * optionCustomPrice);
      }
      return acc;
    }, 0);

    const checkoutOrderInfo = {
      quantity: {
        title: displayName,
        quantity,
        price: `${currency}${productCustomPrice.toFixed(2)}`,
        fullPrice: `${currency}${price.toFixed(2)}`,
        sum: `${currency}${allSum.toFixed(2)}`,
      },
      OptionInfo: {
        title:
          optionQuantity > 0 && options[0].name === 'easy_scan_ship_kit'
            ? `Easy scan ship kit`
            : '',
        description:
          optionQuantity > 0 && options[0].name === 'easy_scan_ship_kit'
            ? `Largest size: ${sizeName}\nMaterial type: ${materialName}`
            : '',
        quantity: options.length > 0 ? 1 : 0,
        price:
          options.length > 0 && options[0].name === 'easy_scan_ship_kit'
            ? `${currency}${optionPrice.toFixed(2)}`
            : `${currency}${(0).toFixed(2)}`,
        sum:
          options.length > 0 && options[0].name === 'easy_scan_ship_kit'
            ? `${currency}${optionPrice.toFixed(2)}`
            : `${currency}${(0).toFixed(2)}`,
        withoutSum: true,
      },
    };

    const enhanceInfo = `Сloud storage x${item.options ? 0 : quantity}`;
    const deliverablesInfo = `${optionName} x${optionQuantity} (${currency}${15})`;
    const easyScanShipKitInfo =
      optionQuantity > 0 && options[0].name === 'easy_scan_ship_kit'
        ? `Largest size: ${sizeName}\nMaterial type: ${materialName}`
        : '';

    const giftOptionsSum = isGift && giftOptions && giftOptions.addWrapping ? 15 : 0;
    const optionsSum = quantity * (productCustomPrice || 0) + (optionPrice || 0) + optionsEnchance;
    const allOptionsIncludeProduct = entitledProductIds.find(({ id, options: productOptions }) => {
      const isIncludeOption =
        productOptions && productOptions.length && options && options.length
          ? productOptions.every(({ id: idOption }) =>
              options.find(({ optionId }) => idOption === optionId),
            )
          : !productOptions || !productOptions.length;
      return id === productId && isIncludeOption;
    });
    const saleProductsSum = entitledProductIds.some(
      ({ id }) => id === productId && allOptionsIncludeProduct,
    )
      ? allSum + saleOptionSum
      : 0;
    return {
      orderCheckoutTitle,
      orderTitle,
      qtInfo,
      priceTitle: `${title}:`,
      enhanceInfo,
      deliverablesInfo,
      typeProduct,
      checkoutOrderInfo,
      optionsSum,
      giftOptionsSum,
      easyScanShipKitInfo,
      saleProductsSum,
    };
  }

  const saleOptionsSum = options.reduce((acc, option) => {
    const { optionId, quantity: optionQuantity } = option;
    if (
      entitledProductIds.some((itm) =>
        option && itm.options
          ? itm.options.find(({ id }) => id === optionId && Boolean(optionQuantity))
          : false,
      )
    ) {
      return acc + Number(optionQuantity * getOptionCustomPrice(optionId));
    }
    return acc;
  }, 0);

  const qtInfo = ` ${productName} x${quantity} = ${currency}${(quantity * productCustomPrice).toFixed(2)}`;

  const enhanceSum = enhanceValue.quantity ? enhanceValue.quantity * getOptionCustomPrice(enhanceValue.optionId) : 0;
  const enhanceInfo = enhanceValue.quantity
    ? `${enhanceValue.displayName} x${
        enhanceValue.quantity ? enhanceValue.quantity : 0
      } = ${currency}${enhanceSum.toFixed(2)}`
    : '';

  const cloudDownloadSum = cloudValue.quantity ? getOptionCustomPrice(cloudValue.optionId) : 0;
  const cloudDownloadInfo = `${cloudValue.displayName} x${
    cloudValue.quantity ? 1 : 0
  } = ${currency}${cloudDownloadSum}`;

  const *******Info = `${*******SafeValue.displayName} x${*******SafeValue.quantity} = ${currency}${
    *******SafeValue.quantity * getOptionCustomPrice(*******SafeValue.optionId)
  }`;
  const archivalSetInfo = `${archivalValue.displayName} x${archivalValue.quantity} = ${currency}${
    archivalValue.quantity * getOptionCustomPrice(archivalValue.optionId)
  }`;
  // --- Hidden for the future ---
  // const *******BoxInfo = `
  // ${*******BoxValue.displayName} x${*******BoxValue.quantity}
  // (${currency}${*******BoxValue.quantity * *******BoxValue.price})
  // `;

  const personalizeInfo = personalizeValue.quantity
    ? `Personalized keepsake box x${*******SafeValue.quantity} = ${currency}${
        *******SafeValue.quantity * getOptionCustomPrice(personalizeValue.optionId)
      }`
    : '';

  const personalizeSum = personalizeValue.quantity
    ? *******SafeValue.quantity * getOptionCustomPrice(personalizeValue.optionId)
    : 0;

  const deliverablesInfo =
    (*******SafeValue.quantity ? `${*******Info}\n` : '') +
    (*******SafeValue.quantity && personalizeInfo ? `${personalizeInfo}\n` : '') +
    (cloudValue.quantity ? `${cloudDownloadInfo}\n` : '') +
    (archivalValue.quantity ? archivalSetInfo : '');

  const optionsSum =
    quantity * productCustomPrice +
    enhanceSum +
    *******SafeValue.quantity * getOptionCustomPrice(*******SafeValue.optionId) +
    cloudDownloadSum +
    archivalValue.quantity * getOptionCustomPrice(archivalValue.optionId) +
    personalizeSum;
  // --- Hidden for the future ---
  // + *******BoxValue.quantity * *******BoxValue.price;

  const giftOptionsSum = isGift && giftOptions && giftOptions.addWrapping ? 15 : 0;
  const orderTitle = `${displayName}`;
  const orderCheckoutTitle = `${displayName}`;
  const checkoutOrderInfo = {
    quantity: {
      title: displayName,
      quantity,
      price: `${currency}${productCustomPrice.toFixed(2)}`,
      fullPrice: `${currency}${(price || 0).toFixed(2)}`,
      sum: `${currency}${(quantity * productCustomPrice).toFixed(2)}`,
      minValue,
      isTrial,
    },
    *******_safe: {
      title: *******SafeValue.displayName,
      quantity: *******SafeValue.quantity,
      price: `${currency}${(getOptionCustomPrice(*******SafeValue.optionId) || 0).toFixed(2)}`,
      fullPrice: `${currency}${(*******SafeValue.price || 0).toFixed(2)}`,
      sum: `${currency}${(*******SafeValue.quantity * getOptionCustomPrice(*******SafeValue.optionId) || 0).toFixed(2)}`,
      hide: *******SafeValue.hide,
      isNotEdit: *******SafeValue.isNotEdit,
      optionId: *******SafeValue.optionId,
    },
    cloud_download: {
      title: cloudValue.displayName,
      quantity: cloudValue.quantity,
      price: `${currency}${(getOptionCustomPrice(cloudValue.optionId) || 0).toFixed(2)}`,
      fullPrice: `${currency}${(cloudValue.price || 0).toFixed(2)}`,
      sum: `${currency}${(cloudValue.quantity * getOptionCustomPrice(cloudValue.optionId) || 0).toFixed(2)}`,
      hide: cloudValue.hide,
      isNotEdit: cloudValue.isNotEdit,
      optionId: cloudValue.optionId,
    },
    archival_dvd_set: {
      title: archivalValue.displayName,
      quantity: archivalValue.quantity,
      price: `${currency}${(getOptionCustomPrice(archivalValue.optionId) || 0).toFixed(2)}`,
      fullPrice: `${currency}${(archivalValue.price || 0).toFixed(2)}`,
      sum: `${currency}${(archivalValue.quantity * getOptionCustomPrice(archivalValue.optionId) || 0).toFixed(2)}`,
      hide: archivalValue.hide,
      isNotEdit: archivalValue.isNotEdit,
      optionId: archivalValue.optionId,
    },
    // --- Hidden for the future ---
    // *******_box: {
    //   title: *******BoxValue.displayName,
    //   quantity: *******BoxValue.quantity,
    //   price: `${currency}${*******BoxValue.price.toFixed(2)}`,
    //   sum: `${currency}${(*******BoxValue.quantity * *******BoxValue.price).toFixed(2)}`,
    // },
    personalize: {
      title: personalizeValue.displayName,
      quantity: personalizeValue.quantity,
      price: `${currency}${(getOptionCustomPrice(personalizeValue.optionId) || 0).toFixed(2)}`,
      fullPrice: `${currency}${(personalizeValue.price || 0).toFixed(2)}`,
      sum: `${currency}${(personalizeValue.quantity
        ? *******SafeValue.quantity * getOptionCustomPrice(personalizeValue.optionId)
        : 0
      ).toFixed(2)}`,
      hide: personalizeValue.hide,
      isNotEdit: personalizeValue.isNotEdit,
      optionId: personalizeValue.optionId,
    },
  };
  const newCheckoutOrderInfo = {
    ...checkoutOrderInfo,
    ...(typeProduct === PHOTO_PRODUCT
      ? {
          enhance_my_transfer: {
            title: enhanceValue.displayName,
            quantity: enhanceValue.quantity,
            price: `${currency}${(getOptionCustomPrice(enhanceValue.optionId) || 0).toFixed(2)}`,
            fullPrice: `${currency}${(enhanceValue.price || 0).toFixed(2)}`,
            sum: `${currency}${(enhanceSum || 0).toFixed(2)}`,
            hide: enhanceValue.hide,
            isNotEdit: enhanceValue.isNotEdit,
            optionId: enhanceValue.optionId,
          },
        }
      : {}),
  };

  const allOptionsIncludeProduct = entitledProductIds.find(({ id, options: productOptions }) => {
    const isIncludeOption =
      productOptions && productOptions.length && options && options.length
        ? productOptions.every(({ id: idOption }) =>
            options.find(
              ({ optionId, quantity: quantityOption }) =>
                idOption === optionId && Boolean(quantityOption),
            ),
          )
        : !productOptions || !productOptions.length;
    return id === productId && isIncludeOption;
  });
  const saleProductsSum = entitledProductIds.some(
    ({ id }) => id === productId && allOptionsIncludeProduct,
  )
    ? quantity * productCustomPrice + saleOptionsSum
    : 0;

  return {
    qtInfo,
    orderTitle,
    enhanceInfo,
    deliverablesInfo,
    priceTitle: `${displayName}:`,
    typeProduct,
    orderCheckoutTitle,
    quantity,
    price,
    checkoutOrderInfo: newCheckoutOrderInfo,
    optionsSum,
    giftOptionsSum,
    saleProductsSum,
  };
};

export default formattedPrice;
