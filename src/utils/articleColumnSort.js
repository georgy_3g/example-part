const articleColumnSort = (data) => {
  const articleColumns = [[], [], []];
  let j = 0;
  if (data && data.length > 0) {
    data.forEach((item) => {
      if (j === 3) {
        j = 0;
      }
      if (item.isFeatured === false) {
        articleColumns[j].push(item);
        j += 1;
      }
    });
  }
  return articleColumns;
};

export default articleColumnSort;
