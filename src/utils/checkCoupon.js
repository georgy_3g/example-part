const checkCoupon = (entitledProductIds, productAndOptions, isOrderIncludeAllProducts) => {
  if (!entitledProductIds || !entitledProductIds.length || !entitledProductIds[0]) {
    return false;
  }

  const checkedProducts = entitledProductIds.map((item) => {
    const { id, options } = item;
    return Boolean(
      productAndOptions.find(
        ({ productId, typeProduct, options: productOptions, isMatting, customText }) => {
          if (typeProduct === 'photo_art') {
            const { optionId } = productOptions ? productOptions[0] || {} : {};
            const { isMatting: isMattingOption, isPersonalize } = options
              ? options.find((option) => option.id === optionId) || {}
              : {};
            return (
              id === productId &&
              (isMattingOption ? isMatting === isMattingOption : true) &&
              (isPersonalize ? isPersonalize === Boolean(customText) : true)
            );
          }
          const includeOptions =
            options && options.length && productOptions
              ? options.every(({ id: optionId }) =>
                  productOptions.find(
                    (option) =>
                      (option.optionId === optionId || option.id === optionId) && option.quantity,
                  ),
                )
              : (!productOptions && !options) || (productOptions && (!options || !options.length));
          return id === productId && includeOptions;
        },
      ),
    );
  });

  if (isOrderIncludeAllProducts) {
    return checkedProducts.every((item) => item);
  }
  return checkedProducts.some((item) => item);
};

export default checkCoupon;
