import isSaleOption from './isSaleOption';
import formattedPrice from './formattedPrice';

const TAX_RATE = 0.07;
const RUSH_TAX = 0.25;
const GIFT_WRAPPING = 15;
const PHOTO_ART_TYPE = 'photo_art';
const SHIPPING_PRODUCTS = [
  'free_shipping',
  'easy_home_ship_kit',
  'local_pickup',
  'easy_scan_ship_kit',
  'photo_art_ship_kit',
  'return_shipping',
  'trial_ship_kit',
];

const DIGITIZE_PRODUCTS = [
  'film_transfer',
  'tape_transfer',
  'audio_transfer',
  'digital_transfer',
  'photo_scan',
];

const TYPES_PRODUCT = ['photo_restoration', 'photo_colorization', 'photo_retouching'];
const DIGITIZE_OPTIONS = ['archival_dvd_set', '*******_safe', 'personalize'];
const FLORIDA_STATE_NAMES = ['fl', 'florida'];

const getIsNeedTax = state => state && FLORIDA_STATE_NAMES.includes(state.toLowerCase());

const getSaleValue = (valueType, couponValue, sumToSale) => {
  if (valueType !== 'percentage' && sumToSale < couponValue) {
    return 100;
  }
  return valueType === 'percentage' ? (couponValue || 0) : (couponValue || 0) / sumToSale * 100;
};


const formattedOrderDetails = ({
  products,
  isQuickly,
  orderShippingAddress,
  couponValue,
  entitledProductIds: entitledProduct,
  valueType,
  taxAmount,
  discountAmount,
  currency,
  selectPrice,
  targetType,
  isGiftCard,
}) => {
  const entitledProductIds = entitledProduct || [];
  const { state } = orderShippingAddress || {};
  const isNeedTax = getIsNeedTax(state);
  let subTotal = 0;
  let giftSubTotal = 0;

  const productsAndOptions = products.reduce((acc, item) => {
    const {
      options,
      customText,
      matting,
      product: { name: productName },
      productId,
      price: priceProduct,
      quantity: quantityProduct,
    } = item;
    const optionsProducts = options && options.length
      ? options.map((option) => {
        const {
          quantity,
          product: { id, name, displayName },
          price,
        } = option;
        return {
          optionId: id,
          quantity,
          price,
          name,
          displayName,
        };
      })
      : [];
    return [...acc, {
      typeProduct: productName,
      productId,
      isMatting: Boolean(matting),
      mattingPrice: matting ? matting.price : 0,
      customText,
      options: optionsProducts,
      price: productName !== PHOTO_ART_TYPE ? priceProduct : optionsProducts[0].price,
      quantity: quantityProduct,
    }];
  }, []);

  products.forEach(({
    product,
    price: productPrice,
    quantity,
    isGift,
    giftOptions,
    options,
    matting,
    mattingColor,
    customText,
  }) => {
    subTotal += product.name === 'photo_art' ? 0 : productPrice * quantity;
    if (isGift && giftOptions.addWrapping) giftSubTotal += GIFT_WRAPPING;
    options.forEach((option) => {
      subTotal += option.price * (product.name === 'photo_art' ? quantity : option.quantity);
    });
    if (product.name === 'photo_art') {
      subTotal += (matting && matting.price && mattingColor && mattingColor.name)
        ? (matting.price * quantity) : 0;
      subTotal += customText ? 10 * quantity : 0;
    }
  });

  const shippingSubTotal = products.reduce((shippingAcc, {
    price: shippingPrice,
    quantity: shippingQuantity,
    product: shippingProduct,
  }) => {
    const { name } = shippingProduct;
    if (SHIPPING_PRODUCTS.includes(name)) {
      return shippingAcc + shippingPrice * shippingQuantity;
    }
    return shippingAcc;
  }, 0);

  const sumWithOutShipping = subTotal - shippingSubTotal;

  const saleSum = productsAndOptions.reduce((acc, product) => {
    const {
      typeProduct,
    } = product;

    const {
      saleProductsSum,
    } = formattedPrice(product, currency, selectPrice[typeProduct], entitledProductIds);

    return acc + saleProductsSum;
  }, 0);

  const saleCardSum = isGiftCard ? subTotal : sumWithOutShipping;

  const sumToSale = targetType === 'cart' ? saleCardSum : saleSum;

  const isCartSale = targetType === 'cart';

  const saleValue = getSaleValue(valueType, couponValue, sumToSale);

  const taxProductsSum = productsAndOptions.reduce((acc, item) => {
    const { typeProduct } = item;
    if (DIGITIZE_PRODUCTS.includes(typeProduct)) {
      const orderOptionsSum = productsAndOptions.reduce((sum, { options }) => {
        const orderOptions = options.reduce((optionList, option) => {
          const { name } = option;
          if (DIGITIZE_OPTIONS.includes(name)) {
            return { ...optionList, [name]: option };
          }
          return optionList;
        }, {});
        const {
          archival_dvd_set: archivalDvdSet,
          *******_safe: *******Safe,
          personalize,
        } = orderOptions;
        const { price: dvdPrice = 0, quantity: dvdQuantity = 0 } = archivalDvdSet || {};
        const { price: usbPrice = 0, quantity: usbQuantity = 0 } = *******Safe || {};
        const { price: personalizePrice = 0, quantity: personalizeQt = 0 } = personalize || {};

        const isDvdSale = isCartSale
            || isSaleOption({
              arrayProducts: entitledProductIds,
              item,
              option: archivalDvdSet,
              withOption: options.length,
            });
        const isUsbSale = isCartSale
             || isSaleOption({
               arrayProducts: entitledProductIds,
               item,
               option: *******Safe,
               withOption: options.length,
             });
        const isPersonalizeSale = isCartSale
            || isSaleOption({
              arrayProducts: entitledProductIds,
              item,
              option: personalize,
              withOption: options.length,
            });

        const personalizeSum =
          personalizePrice * usbQuantity * (isPersonalizeSale ? (100 - saleValue) / 100 : 1);
        return (
          sum +
          (dvdPrice * dvdQuantity * (isDvdSale ? (100 - saleValue) / 100 : 1) +
            usbPrice * usbQuantity * (isUsbSale ? (100 - saleValue) / 100 : 1) +
            (personalizeQt ? personalizeSum : 0))
        );
      }, 0);

      return acc + orderOptionsSum;
    }
    if (typeProduct === 'photo_art' || typeProduct === 'gift_card') {
      const {
        optionsSum,
        saleProductsSum,
      } = formattedPrice(item, currency, selectPrice[typeProduct], entitledProductIds);
      if (targetType === 'cart') {
        return acc + (optionsSum - optionsSum * saleValue / 100);
      }
      return acc + (saleProductsSum - saleProductsSum * saleValue / 100)
      + (optionsSum - saleProductsSum);
    }

    if (TYPES_PRODUCT.includes(typeProduct)) {
      const { options = [] } = item;
      return acc + options.reduce((accum, optionProduct) => {
        const {
          quantity, name, price = 0,
        } = optionProduct;
        if (name !== 'enhance_prints') {
          return accum;
        }

        const isSale = isCartSale || isSaleOption({

          arrayProducts: entitledProductIds,
          option: optionProduct,
          withOption: options.length,
          item,
        });

        return accum + price * quantity * (isSale ? (100 - saleValue) / 100 : 1);
      }, 0);
    }
    return acc;
  }, 0);

  const orderPrice = subTotal + giftSubTotal;
  const saleTax = isNeedTax ? taxProductsSum * TAX_RATE : 0;
  const rushSubTotal = isQuickly ? sumWithOutShipping * RUSH_TAX : 0;
  const discountSum = saleValue ? (sumToSale * saleValue / 100) : 0;
  const total = orderPrice + taxAmount + rushSubTotal - discountAmount;
  return ({
    giftSubTotal,
    rushSubTotal,
    saleTax,
    total,
    shippingSubTotal,
    discountSum,
    subTotal: sumWithOutShipping,
  });
};

export default formattedOrderDetails;
