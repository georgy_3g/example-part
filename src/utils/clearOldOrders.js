import { TWO_DAYS } from 'src/constants';

const clearOldOrders = (orders) => {
  const newOrders = {};
  Object.keys(orders).forEach((key) => {
    newOrders[key] = (orders[key] && orders[key].length
    ? orders[key].filter((item) => item && item.addedAt && new Date(item.addedAt).valueOf() + TWO_DAYS > new Date().valueOf())
    : []
    )
  });

  return newOrders;
};

export default clearOldOrders;
