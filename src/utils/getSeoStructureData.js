const getProductSchema = ({
  reviewsCount,
  reviewsRate,
  lowPrice,
  imageOne,
  imageTwo,
  seoDescription,
  offerUrl,
 }) => ({
  "@context": "https://schema.org/",
  "@type": "Product",
  "name": "Photo restoration",
  "image": [
    imageOne,
    imageTwo,
  ],
  "description": seoDescription,
  // "sku": "0446310786",
  // "mpn": "925872",
  // "brand": {
  //   "@type": "Brand",
  //   "name": "ACME"
  // },
  "review": {
    "@type": "Review",
    "reviewRating": {
      "@type": "Rating",
      "ratingValue": "5",
      "bestRating": "5"
    },
    "author": {
      "@type": "Person",
      "name": "Florena"
    }
  },
  "aggregateRating": {
    "@type": "AggregateRating",
    "ratingValue": reviewsRate,
    "reviewCount": reviewsCount,
    "bestRating": "5",
  },
  "offers": {
    "@type": "Offer",
    "url": offerUrl,
    "priceCurrency": "USD",
    "price": lowPrice,
    "priceValidUntil": "2020-11-20",
    "itemCondition": "https://schema.org/UsedCondition",
    "availability": "https://schema.org/InStock"
  }
})

const getBreadCrumbList = (list) => ({
  "@context": "https://schema.org",
  "@type": "BreadcrumbList",
  "itemListElement": list.map(({name, itemUrl, position}) => ({
    "@type": "ListItem",
    "position": position,
    "name": name,
    "item": itemUrl
  }))
})

const getBreadCrumbBlogList = (list) => ({
  "@context": "https://schema.org",
  "@type": "BreadcrumbList",
  "itemListElement": list.map(({title, url}, index) => ({
    "@type": "ListItem",
    "position": index + 1,
    "name": title,
    "item": `https://*******.com${url}`
  }))
})

const getSeoFaq = (list) => ({
  "@context": "https://schema.org",
  "@type": "FAQPage",
  "mainEntity": list.map(({desc, text})=> (
    {
      "@type": "Question",
      "name": desc,
      "acceptedAnswer": {
        "@type": "Answer",
        "text": text
      }
    }))
})

const getSeoBlog = ({
  imageUri,
  title,
  titleSeo,
  descriptionSeo,
  author,
  body,
  addedAt,
  updatedAt,
  category,
  previewImageUri
   }, canonicalUrl) => (
  {
    "@context":"http://schema.org",
    "@type": "BlogPosting",
    "image": imageUri,
    "url": canonicalUrl,
    "thumbnailUrl": previewImageUri,
    "headline": title,
    "alternativeHeadline": titleSeo,
    "dateCreated": addedAt,
    "datePublished": addedAt,
    "dateModified": updatedAt || addedAt,
    "inLanguage": "en-US",
    "isFamilyFriendly": "true",
    "copyrightYear": "2021",
    "contentLocation": {
      "@type": "Place",
      "name": "Boca Raton, FL"
    },
    "author": {
      "@type": "Person",
      "name": author,
    },
    "creator": {
      "@type": "Person",
      "name": author,
    },
    "publisher": {
      "@type": "Organization",
      "name": "*******",
      "url": "https://*******.com/",
      "logo": {
        "@type": "ImageObject",
        "url": "https://file.*******.com/default/*******-logo.jpg",
        "width":"341",
        "height":"121"
      }
    },
    "mainEntityOfPage": "True",
    "keywords": [
      titleSeo,
      descriptionSeo,
    ],
    "genre":["SEO","JSON-LD"],
    "articleSection": category,
    "articleBody": body,
  }
)

export {
  getBreadCrumbList,
  getSeoFaq,
  getProductSchema,
  getBreadCrumbBlogList,
  getSeoBlog,
}
