import axios from 'axios';

const preloadZipCods = (api) =>
  axios
    .get(api)
    .then(({ data }) => data)
    .catch(({ response }) => {
      console.log('error', response);
    });

export default preloadZipCods;
