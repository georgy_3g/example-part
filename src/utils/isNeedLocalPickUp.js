const isNeedLocalPickUp = (products) => {
  const mustHave = [
    'film_transfer',
    'audio_transfer',
    'digital_transfer',
    'photo_scan',
    'tape_transfer',
  ];

  return Object
    .values(products)
    .reduce((acc, item) => (
      acc || item.some(
        ({ typeProduct }) => mustHave.includes(typeProduct),
      )
  ), false);
};

export default isNeedLocalPickUp;
