const cleanCardNumber = (value) => value.replace(/\s/g, '').replace(/_/g, '');

export default cleanCardNumber;
