// import Rollbar from 'rollbar';
import { NEXT_PUBLIC_ROLLBAR_TOKEN, NEXT_PUBLIC_NODE_ENV } from 'src/config';

// let rollbar;

const getRollbar = () => {
  if (NEXT_PUBLIC_NODE_ENV === 'production' && !NEXT_PUBLIC_ROLLBAR_TOKEN) {
    return {
      error: () => {},
      warning: () => {},
      info: () => {},
    };
  }

  // if (NEXT_PUBLIC_NODE_ENV === 'develop' || !NEXT_PUBLIC_ROLLBAR_TOKEN) {
  //   return {
  //     error: (name, error) => {
  //       console.log(name, error || '');
  //     },
  //     warning: (name, error) => {
  //       console.log(name, error || '');
  //     },
  //     info: (name, error) => {
  //       console.log(name, error || '');
  //     },
  //   };
  // }

  // if (rollbar) {
  //   return rollbar;
  // }

  // rollbar = new Rollbar({
  //   accessToken: NEXT_PUBLIC_ROLLBAR_TOKEN,
  //   environment: `client ${NEXT_PUBLIC_NODE_ENV}`,
  // });
  //
  // return rollbar;
  return {
    error: (name, error) => {
      console.log(name, error || '');
    },
    warning: (name, error) => {
      console.log(name, error || '');
    },
    info: (name, error) => {
      console.log(name, error || '');
    },
  };
};

export default getRollbar;
