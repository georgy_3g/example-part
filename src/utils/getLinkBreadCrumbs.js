import ROUTES from 'src/constants/routes';
import formatUrl from './formatUrl';

const { home, blog } = ROUTES;

const getLinkBreadCrumbs = ({
  category,
  categoryList,
  article,
}) => {
  const { url, name } = category || {};
  const { titleSeo, title } = article || {};

  const categoryByArticle = article && categoryList
  ? categoryList.find(({ name: nameCategory }) => nameCategory === article.category) || {}
  : {}

  const links = [
    {
      title: 'Home',
      url: home,
    },
    {
      title: 'Blog',
      url: blog,
    },
    ...(Object.keys(article).length && article.category ? [{
      title: article.category,
      url: `${blog}${formatUrl(categoryByArticle.url, true)}/`,
    }] : []),
    ...(url ? [{
      title: name,
      url: `${blog}${formatUrl(url, true)}/`,
    }] : []),
    ...(titleSeo ? [{
      title,
      url: `${blog}${formatUrl(titleSeo, true)}/`,
    }] : [])
  ]
  return links;
};

export default getLinkBreadCrumbs;
