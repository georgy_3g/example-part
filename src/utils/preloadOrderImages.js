import axios from 'axios';

const preloadOrderImages = (api, token) =>
  axios
    .get(`${api}`, {
      headers: { Authorization: `Bearer ${token}` },
      params: { view: 'versioned' },
    })
    .then(({ data }) => data)
    .catch(({ response }) => {
      console.log('error', response);
      return [];
    });

export default preloadOrderImages;
