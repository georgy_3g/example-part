import axios from 'axios';
import getRollbar from './getRollbar';

const rollbar = getRollbar();

const preloadOrdersList = (api, token, params) =>
  axios
    .get(`${api}`, {
      headers: { Authorization: `Bearer ${token}` },
      params: params || {},
    })
    .then(({ data }) => data)
    .catch(({ response }) => {
      rollbar.error('preload orders list', response);
    });

export default preloadOrdersList;
