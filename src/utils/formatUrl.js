const formatUrl = (address, withDash) => {
  if (address) {
    const regexp = /#|\s|,/g;
    const lotLowerGaps = withDash ? /----|--/g : /____|__/g;
    return address
      .replace(regexp, withDash ? '-' : '_')
      .replace(lotLowerGaps, withDash ? '-' : '_')
    .replace(lotLowerGaps, withDash ? '-' : '_');
  }
  return '';
};

export default formatUrl;
