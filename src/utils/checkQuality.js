const checkQuality = (height, width, photoHeight, photoWidth) => {
  const centimetersInInch = 2.54;
  const goodDPI = 300;

  const isHeightOk = (height / photoHeight) * centimetersInInch >= goodDPI;
  const isWidthOk = (width / photoWidth) * centimetersInInch >= goodDPI;

  return isHeightOk && isWidthOk;
};

export default checkQuality;
