const formatReviewsText = (text) => {

  const apostrophe = /&#039;/g
  const apostropheTwo = /&#39;/g
  const oneOpenQuote = /&lsquo;/g;
  const oneClosingQuote = /&rsquo;/g;
  const twoOpenQuote = /&ldquo;/g;
  const twoCloseQuote = /&#34;/g;
  const doubleQuote = /&quot;/g;
  const twoClossingQuote = /&rdquo;/g;
  const emDash = /&mdash;/g;
  const amp = /&amp;/g;

  return text.replace(apostrophe, "'")
  .replace(apostropheTwo,"'")
  .replace(oneOpenQuote,"‘")
  .replace(oneClosingQuote, "’")
  .replace(twoOpenQuote, "“")
  .replace(twoClossingQuote, "”")
  .replace(twoCloseQuote, "”")
  .replace(emDash, "—")
  .replace(amp, "&")
  .replace(doubleQuote, '"');
}

export default formatReviewsText