const deleteStringFirstElement = (value) => (value && value.length ? value.slice(1) : value);

export default deleteStringFirstElement;
