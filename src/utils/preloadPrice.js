import axios from 'axios';
import getRollbar from './getRollbar';

const rollbar = getRollbar();

const preloadPrice = (api) =>
  axios
    .get(api)
    .then(({ data }) => data)
    .catch(({ response }) => {
      rollbar.error('preload products', response);
    });

export default preloadPrice;
