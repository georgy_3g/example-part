const downloadFile = ({ url, fileName }) => {
  if (!window.navigator.msSaveOrOpenBlob) {
    const link = document.createElement('a');
    link.href = url;
    link.setAttribute('download', fileName);
    document.body.appendChild(link);
    link.click();
  } else {
    window.navigator.msSaveOrOpenBlob(url, fileName);
  }
};

export default downloadFile;
