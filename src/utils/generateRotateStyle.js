const generateRotateStyle = (frameRotateX, frameRotateY, frameRotateZ) =>
  `rotateX(${frameRotateX}deg) rotateY(${frameRotateY}deg) rotateZ(${frameRotateZ}deg)`;

export default generateRotateStyle;
