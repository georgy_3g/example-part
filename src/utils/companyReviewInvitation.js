import axios from 'axios';

const companyReviewInvitation = async ({
  api,
  id,
  token,
  reviewersApi,
  reviewersApiSiteId,
  reviewersApiToken,
}) => {
  const {
    data: { firstName, lastName, email },
  } = await axios
    .get(`${api}`, {
      headers: { Authorization: `Bearer ${token}` },
    })
    .then(({ data }) => data)
    .catch(({ response }) => {
      console.log('error', response);
    });

  await axios
    .post(`${reviewersApi}/merchant/invitation`, {
      store: reviewersApiSiteId,
      apikey: reviewersApiToken,
      order_id: id,
      name: `${firstName} ${lastName}`,
      email,
    })
    .then(({ data }) => data)
    .catch(({ response }) => {
      console.log('error', response);
    });
};

export default companyReviewInvitation;
