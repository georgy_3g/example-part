import axios from 'axios';
import {
  REVIEWERS_API_SITE_ID,
  REVIEWERS_API_TOKEN,
  REVIEWERS_MERCHANT_API,
  REVIEWERS_ONE_PRODUCT_API,
  REVIEWERS_PRODUCT_API,
} from 'src/config';

const preloadReviewers = (pageLimit, productSku) => {
  if (productSku) {
    return axios.get(REVIEWERS_ONE_PRODUCT_API, {
      params: {
        store: REVIEWERS_API_SITE_ID,
        sku: productSku.sku,
        per_page: pageLimit,
        order: 'desc',
      },
    });
  }
  return axios
    .get(REVIEWERS_PRODUCT_API, {
      params: {
        apikey: REVIEWERS_API_TOKEN,
        store: REVIEWERS_API_SITE_ID,
        per_page: pageLimit,
        order: 'desc',
      },
    })
    .then(({ data }) => data)
    .catch(({ response }) => {
      console.log('error', response);
  });
};

const preloadCompanyReviewers = (pageLimit) =>
  axios
    .get(REVIEWERS_MERCHANT_API, {
      params: {
        apikey: REVIEWERS_API_TOKEN,
        store: REVIEWERS_API_SITE_ID,
        per_page: pageLimit,
        order: 'desc',
      },
    })
    .then(({ data }) => data)
    .catch(({ response }) => {
      console.log('error', response);
    });

const preloadReviewersStatistics = () =>
  axios
    .get(REVIEWERS_PRODUCT_API, {
      params: {
        apikey: REVIEWERS_API_TOKEN,
        store: REVIEWERS_API_SITE_ID,
      },
    })
    .then(({ data }) => data)
    .catch(({ response }) => {
      console.log('error', response);
    });

const isProductReviews = (reviewers) => {
  if (reviewers.data && reviewers.data.stats) {
    const {
      data: {
        stats: { average, count },
        reviews,
      },
    } = reviewers;
    return {
      count,
      rating: average || 0,
      productReviews: reviews.data,
    };
  }
  const { count = 0, rating = 0, reviews: productReviews = [] } = reviewers;
  return {
    count,
    rating,
    productReviews,
  };
};

export { preloadReviewers, preloadReviewersStatistics, preloadCompanyReviewers, isProductReviews };
