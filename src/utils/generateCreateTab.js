import ROUTES from 'src/constants/routes';

const { photoArt } = ROUTES;

const generateCreateTab = (data) => ({
  withImages: true,
  link: photoArt,
  name: 'Create',
  text: 'Create personalized, handcrafted photo art from your favorite photos.',
  buttonText: 'Learn more',
  links: ([...data] || [])
    .sort((a, b) => a.order - b.order)
    .map((item) => {
      const { id, name, imageUri } = item; 
      return {
        id: name.toLowerCase(),
        text: name,
        link: `${photoArt}?collection=${id}`,
        img: imageUri,
      };
    }),
});

export default generateCreateTab;
