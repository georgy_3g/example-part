const buildUrl = (params) => {
  const keys = Object.keys(params);
  return keys.reduce((acc, key) => {
    if (Array.isArray(params[key])) {
      return (
        acc +
        params[key].reduce((accum, item, index) => {
          if (typeof item !== 'object') return `${accum}${key}[${index}]=${item}&`;
          return (
            accum +
            Object.keys(item).reduce(
              (accStr, str) => `${accStr}${key}[${index}][${str}]=${item[str]}&`,
              '',
            )
          );
        }, '')
      );
    }
    if (typeof params[key] === 'object') {
      const keyElement = Object.keys(params[key]);
      return (
        acc +
        keyElement.reduce((accum, item) => `${accum}${key}[${item}]=${params[key][item]}&`, '')
      );
    }
    return `${acc}${key}=${params[key]}&`;
  }, '');
};

export default buildUrl;
