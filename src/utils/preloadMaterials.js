import axios from 'axios';
import getRollbar from './getRollbar';

const rollbar = getRollbar();

const preloadMaterials = (api) =>
  axios
    .get(api)
    .then(({ data }) => data)
    .catch(({ response }) => {
      rollbar.error('preload materials', response);
    });

export default preloadMaterials;
