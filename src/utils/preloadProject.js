import axios from 'axios';

const preloadProject = (api, token) =>
  axios
    .get(`${api}`, {
      headers: { Authorization: `Bearer ${token}` },
    })
    .then(({ data }) => data.data)
    .catch(({ response }) => {
      console.log('error', response);
    });

export default preloadProject;
