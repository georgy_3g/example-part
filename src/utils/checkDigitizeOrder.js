import { DIGITIZE_PRODUCTS } from 'src/constants';

const checkDigitizeOrder = (item, typeProduct, itemId, counter, inc) => {
  if (DIGITIZE_PRODUCTS.includes(typeProduct)) {
    if (item.id === itemId) {
      return item.options.reduce((acc, itm) => {
        const { name, quantity } = itm;
        if (inc === null) {
          return false;
        }
        if (!acc && ['*******_safe', 'archival_dvd_set'].includes(name)) {

          if(['tape_transfer', 'audio_transfer'].includes(typeProduct) &&
              typeof inc === "boolean"
          ) {
            return Boolean(counter === name ? !quantity : quantity)
          }
          return Boolean(counter === name ? quantity + inc : quantity);
        }
        if (!acc && name === 'cloud_download') {
          return Boolean(counter === name ? !quantity : quantity);
        }
        return acc;
      }, false);
    }
  }
  return true;
};

export default checkDigitizeOrder;
