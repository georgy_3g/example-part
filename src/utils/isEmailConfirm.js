import axios from 'axios';

const isEmailConfirm = (api, token) =>
  axios
    .get(api, { params: { token } })
    .then(({ data }) => data.data)
    .catch(({ response }) => response.status);

export default isEmailConfirm;
