import { NEXT_PUBLIC_IMAGES_DOMAIN, NEXT_PUBLIC_RESIZE_IMAGES_DOMAIN } from 'src/config';

const formattedImage = ({ imagePath, needResize, imageSize }) => {
  return needResize && imageSize
    ? `${NEXT_PUBLIC_RESIZE_IMAGES_DOMAIN}/${imageSize}/${imagePath}`
    : `${NEXT_PUBLIC_IMAGES_DOMAIN}/front/${imagePath}`;
};


export default formattedImage;
