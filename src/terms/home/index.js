const HOME_TERMS = {
  WHAT_WE_DO: 'The possibilities are endless',
  WHAT_WE_DO_DESC: 'Create personalized keepsakes for your analogue and digital memories',
  WHAT_WE_DO_INFO:
    'We are a dedicated team of archivists and creators passionate about helping you create unique personalized memorabilia. \n\nThis is our mission and we are excited to share it with you. ',
  ARCHIVAL_DVD: 'Archival DVD Set',
  FAMILY_SHARING: 'Family Sharing Link',
  PERSONAL_PHOTO: 'Personalized Photo Art',
  GIFT: 'The perfect gift',
  PROMO: 'Special Promotion',
  TEN_OFF: '10% off',
  PROMO_DESC: '10% off restoration - Get it now before it ends.',
  HAPPY_CLIENTS: 'Happy clients',
  REVIEWS_COUNT: '321 reviews',
  HEADLINE_ABOUT_US: 'Your ******* team.',
  HEADLINE_ABOUT_US_DESC:
    "We've spent 15 years perfecting the art of preserving memories and creating beautiful keepsakes",
  GIVE_A_GIFT: 'Give a special gift to your loved ones.',
  PRENIUM_QUALITY: 'Premium Quality',
  MADE_IN_USA: 'American made',
  MEMORY_BOX: 'Memory Box',
  BEAUTIFUL_MATERIALS: 'Beautiful materials',
  BANNER_TAPE_TITLE: 'Not your standard photo restoration.',
  TAPE_TRANSFER_TITLE: 'How it works',
  TAPE_TRANSFER_DESC: 'It’s easy, secure and reliable to transfer your memories',
  OUR_MISSION_INFO:
    'Our mission is to take both analog and digital media from customers worldwide and use a fresh and intuitive new approach to enjoy, relive and share memories through creative deliverables to treasure *******.',
  BANER_TITLE: 'Reimagine your \nmemories',
  BANER_DESC:
    'We create beautiful, high quality keepsakes from your memories so you can relieve your best moments *******.',
  HOW_IT_WORKS: "It's simple.",
  HOW_IT_WORKS_DESC:
    '******* works hard to\nmake it easy for you to digitize,\nenhance & create extraordinary.',
  DIGITIZE: 'Digitize',
  DIGITIZE_DESC: 'Transfer your old photos, tapes &\nfilm ******* to high-resolution\ndigital files.',
  ENHANCE: 'Enhance',
  ENHANCE_DESC: 'Enhance, retouch, restore & colorize\nblack and white photos to\nperfection.',
  CREATE: 'Create',
  CREATE_DESC: 'Design & personalize unique\nhigh-quality handcrafted photo\nart and gifts.',
  LEARN_MORE: 'Learn more',
  OUR_MISSION: 'Our mission',
  OUR_MISSION_DESC:
    'We are a dedicated team of archivists and creators passionate \nabout helping you create unique personalized memorabilia. \nThis is our mission and we are excited to share it with you.',
  HOME_PAGE_GIFT: 'Give the most \nunique gift.',
  HOME_PAGE_GIFT_TEXT:
    'They`ve never had something like this before. \nNew heroes made here daily.',
};

export default HOME_TERMS;
