const ACCOUNT_TERMS = {
  MY_ORDERS: 'Details',
  MY_FILES: 'Cloud',
  MY_ACCOUNT: 'Account',
  ACCOUNT_DETAILS: 'Account details',
  SHIPPING_INFO: 'Shipping info',
  PAYMENT_DETAILS: 'Payment details',
  ORDER_DETAILS: 'Details',
  TRACK_ORDER: 'Track',
  PROOFS: 'Proofs',
  MEDIA: 'Media',
  MESSAGES: 'Messages',
  PAYMENT_INFO: 'Payment info',
  ENHANCEMENTS: 'Proof',
};

export default ACCOUNT_TERMS;
