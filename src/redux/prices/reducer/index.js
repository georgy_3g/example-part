import { HYDRATE } from 'next-redux-wrapper';
import checkHydrateData from 'src/utils/checkHydrateData';
import {
  GET_PRICES,
  LOAD_PRICES,
  SET_PRODUCT_TYPE,
  LOAD_GIFTCARDS_PRICES
} from '../types';

const INITIAL_STATE = {
  currency: '$',
  error: null,
  productType: '',
  gift_card: [],
};

export default (state = INITIAL_STATE, { type, payload }) => {
  switch (type) {
    case HYDRATE: {
      const isDiff = checkHydrateData(state, payload.prices);
      return isDiff ? { ...payload.prices } : state;
    }

    case GET_PRICES:
      return {
        ...state,
        ...payload.data,
      };

    case LOAD_PRICES:
      return {
        ...state,
        ...(payload && Object.keys(payload).length
          ? payload.data.reduce((acc, item) => {
              if (!Object.keys(acc).includes(item.name)) {
                return {
                  ...acc,
                  [item.name]: { ...item },
                };
              }
              return acc;
            }, {})
          : {}),
        shippingProducts:
          payload && Object.keys(payload).length && payload.data
            ? payload.data.filter(({ ext }) => ext && ext.shippingSetting)
            : [],
      };

    case LOAD_GIFTCARDS_PRICES:
      return {
        ...state,
        gift_card: payload.data,
      };

    case SET_PRODUCT_TYPE:
      return {
        ...state,
        productType: payload,
      };

    default:
      return state;
  }
};
