import {
  PRICES_REQUEST,
  LOAD_PRICES,
  LOAD_GIFTCARDS_PRICES,
  SET_PRODUCT_TYPE,
  GET_PRICES_START,
  GET_PRICES_FAILURE,
} from '../types';
import { apiCall, getError } from '../../utils';
import URL from '../../urls';

const getPriceList = (type) => (dispatch) => {
  dispatch({
    type: PRICES_REQUEST,
    payload: { type },
  });
};
const loadPriceList = (payload) => (dispatch) => {
  dispatch({
    type: LOAD_PRICES,
    payload,
  });
};

const loadGiftCardsPriceList = (payload) => (dispatch) => {
  dispatch({
    type: LOAD_GIFTCARDS_PRICES,
    payload,
  });
};
const setType = (payload) => (dispatch) => {
  dispatch({
    type: SET_PRODUCT_TYPE,
    payload,
  });
};
const getAllPricesAction = () => async (dispatch) => {
  try {
    dispatch({ type: GET_PRICES_START });
    const { data } = await apiCall({
      type: 'GET',
      url: `${URL.PRODUCT}`,
    });
    dispatch({ type: LOAD_PRICES, payload: data });
  } catch (error) {
    dispatch({ type: GET_PRICES_FAILURE, payload: getError(error) });
  }
};

export { getPriceList, loadPriceList, setType, loadGiftCardsPriceList, getAllPricesAction };
