const priceSelect = ({ prices }) => prices[prices.productType];
const currencyPriceSelect = ({ prices }) => prices.currency;
const orderPriceSelect = ({ prices }) => prices;
const giftCardsPriceSelect = ({ prices }) => prices.gift_card;
const photoColorizationSelect = ({ prices }) => prices.photo_colorization;
const productTypeSelect = ({ prices }) => prices.productType;
const photoRestorationSelect = ({ prices }) => prices.photo_restoration;
const photoRetouchingSelect = ({ prices }) => prices.photo_retouching;
const easyScanShipKitSelect = ({ prices }) => prices.easy_scan_ship_kit;
const photoArtSelector = ({ prices }) => prices.photo_art;
const trialShipKitSelector = ({ prices }) => prices.trial_ship_kit;
const photoScanSelector = ({ prices }) => prices.photo_scan;
const tapeTransferSelector = ({ prices }) => prices.tape_transfer;
const filmTransferSelector = ({ prices }) => prices.film_transfer;
const audioTransferSelector = ({ prices }) => prices.audio_transfer;
const digitalTransferSelector = ({ prices }) => prices.digital_transfer;
const shippingProductsSelector = ({ prices }) => prices.shippingProducts;

export {
  priceSelect,
  currencyPriceSelect,
  orderPriceSelect,
  giftCardsPriceSelect,
  photoColorizationSelect,
  productTypeSelect,
  photoRestorationSelect,
  photoRetouchingSelect,
  easyScanShipKitSelect,
  photoArtSelector,
  photoScanSelector,
  trialShipKitSelector,
  tapeTransferSelector,
  filmTransferSelector,
  audioTransferSelector,
  digitalTransferSelector,
  shippingProductsSelector,
};
