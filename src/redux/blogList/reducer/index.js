import { HYDRATE } from 'next-redux-wrapper';
import checkHydrateData from 'src/utils/checkHydrateData';
import {
  GET_BLOGS_START,
  GET_BLOGS_SUCCESS,
  GET_BLOGS_FAIL,
  BLOG_LOADING,
  GET_ARTICLE_SUCCESS,
  GET_ARTICLE_START,
  GET_BLOG_CATEGORIES,
} from '../types';

const INITIAL_STATE = {
  list: {
    total: 0,
    data: [],
  },
  isLoading: false,
  isArticleLoading: true,
  article: {},
  categories: [],
};

export default (state = INITIAL_STATE, { type, payload }) => {
  switch (type) {
    case HYDRATE: {
      const isDiff = checkHydrateData(state, payload.blogList);
      return isDiff ? { ...payload.blogList } : state;
    }

    case GET_BLOGS_START: {
      const { data = [] } = payload || {};
      return { ...state, list: data };
    }

    case BLOG_LOADING:
      return { ...state, isLoading: true };

    case GET_BLOGS_SUCCESS: {
      const { data = [], total = 0 } = payload || {};
      return {
        ...state,
        list: {
          total,
          data: [...data],
        },
        isLoading: false,
      };
    }

    case GET_BLOGS_FAIL: {
      return {
        ...INITIAL_STATE,
        error: payload,
        isLoading: false,
      };
    }

    case GET_ARTICLE_START: {
      return {
        ...state,
        isArticleLoading: true,
      };
    }

    case GET_ARTICLE_SUCCESS: {
      return {
        ...state,
        article: payload,
        isArticleLoading: false,
      };
    }

    case GET_BLOG_CATEGORIES: {
      return {
        ...state,
        categories: payload,
      };
    }

    default:
      return state;
  }
};
