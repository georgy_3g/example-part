import { apiCall, getError } from 'src/redux/utils';
import URL from 'src/redux/urls';
import {
  GET_BLOGS_START,
  GET_ARTICLE_SUCCESS,
  SET_ARTICLE_SUCCESS,
  BLOG_LOADING,
  GET_BLOGS_SUCCESS,
  GET_BLOGS_FAIL,
  GET_BLOG_CATEGORIES,
} from '../types';

const loadBlogs = (payload) => (dispatch) => {
  dispatch({
    type: GET_BLOGS_START,
    payload,
  });
};

const addBlogs = (payload) => async (dispatch) => {
  const { limit, offset, categoryId } = payload;
  try {
    dispatch({ type: BLOG_LOADING });
    const paginationLimit = 'pagination[limit]';
    const paginationOffset = 'pagination[offset]';
    const filtersCategory = 'filters[categoryId]';
    const params = {
      [paginationLimit]: limit,
      [paginationOffset]: offset,
      [filtersCategory]: categoryId,
    };
    const {
      data: { data },
    } = await apiCall({ type: 'GET', url: `${URL.BLOG_LIST}`, params });
    dispatch({ type: GET_BLOGS_SUCCESS, payload: data });
  } catch (error) {
    dispatch({ type: GET_BLOGS_FAIL, payload: getError(error) });
  }
};

const loadArticle = (payload) => (dispatch) => {
  dispatch({
    type: GET_ARTICLE_SUCCESS,
    payload,
  });
};

const loadCategories = (payload) => (dispatch) => {
  dispatch({
    type: GET_BLOG_CATEGORIES,
    payload,
  });
};

const setArticle = (payload) => (dispatch) => {
  dispatch({
    type: SET_ARTICLE_SUCCESS,
    payload,
  });
};

export { loadBlogs, addBlogs, loadArticle, setArticle, loadCategories };
