const getBlogList = ({ blogList }) => blogList.list;
const isBlogsLoading = ({ blogList }) => blogList.isLoading;
const isArticleLoading = ({ blogList }) => blogList.isArticleLoading;
const getArticle = ({ blogList }) => blogList.article;
const categoriesBlog = ({ blogList }) => blogList.categories;

export { getBlogList, isBlogsLoading, getArticle, isArticleLoading, categoriesBlog };
