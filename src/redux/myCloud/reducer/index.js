import { ONE_DAY } from 'src/constants';
import { HYDRATE } from 'next-redux-wrapper';
import checkHydrateData from 'src/utils/checkHydrateData';
import differenceInDays from "date-fns/differenceInDays";
import {
  ADD_CLOUD_FILE_SUCCESS,
  DELETE_CLOUD_FILE_SUCCESS,
  ADD_ALL_FILES_SUCCESS,
  DELETE_ALL_FILES_SUCCESS,
  SET_CLOUD_STATUS,
  CHANGE_PLAYER_FLAG,
  CHANGE_CLOUD_IMAGE,
  PAYPAL_CLOUD_SUBSCRIPTION_START,
  PAYPAL_CLOUD_SUBSCRIPTION_SUCCESS,
  PAYPAL_CLOUD_SUBSCRIPTION_FAIL,
  CREDIT_CARD_CLOUD_SUBSCRIPTION_START,
  CREDIT_CARD_CLOUD_SUBSCRIPTION_SUCCESS,
  CREDIT_CARD_CLOUD_SUBSCRIPTION_FAIL,
  SAVED_CARD_CLOUD_SUBSCRIPTION_START,
  SAVED_CARD_CLOUD_SUBSCRIPTION_SUCCESS,
  SAVED_CARD_CLOUD_SUBSCRIPTION_FAIL,
  GET_CLOUD_STATUS_SUCCESS,
  GET_CLOUD_STATUS_FAIL,
  SET_PLAN_ID,
} from '../types';

const INITIAL_STATE = {
  cloudImages: [],
  cloudAudio: [],
  cloudVideo: [],
  folder: [],
  error: '',
  cloudStatus: 0,
  stopAllPlayers: false,
  spinnerFlag: false,
  paymentFail: false,
  paymentError: '',
  planId: 0,
  paypalPlanId: '0',
  isSixtyDaysHavePassed: false,
  subscription: '',
  daysAfter: 0,
};

export default (state = INITIAL_STATE, { type, payload }) => {
  switch (type) {
    case HYDRATE: {
      const isDiff = checkHydrateData(state, payload.myCloud);
      return isDiff ? { ...payload.myCloud } : state;
    }

    case ADD_CLOUD_FILE_SUCCESS: {
      const { fileType, file } = payload;
      const newFiles = [...state[fileType], file];
      return {
        ...state,
        [fileType]: newFiles,
      };
    }

    case DELETE_CLOUD_FILE_SUCCESS: {
      const { fileType, fileId } = payload;
      const filteredFiles = state[fileType].filter(({ id }) => id !== fileId);
      return {
        ...state,
        [fileType]: filteredFiles,
      };
    }

    case ADD_ALL_FILES_SUCCESS: {
      const { cloudImages, cloudAudio, cloudVideo, folder } = payload;
      return {
        ...state,
        cloudImages,
        cloudAudio,
        cloudVideo,
        folder: [...(folder && folder.length ? folder : [])],
      };
    }

    case DELETE_ALL_FILES_SUCCESS: {
      return {
        ...state,
        cloudImages: [],
        cloudAudio: [],
        cloudVideo: [],
        folder: [],
      };
    }

    case SET_CLOUD_STATUS: {
      const { daysRemaining = 0, expirationDate, subscription = '' } = payload || {};
      return {
        ...state,
        isSixtyDaysHavePassed:
          expirationDate &&
          differenceInDays(new Date(), new Date(expirationDate)) > ONE_DAY * 60 &&
          differenceInDays(new Date(), new Date(expirationDate)) < ONE_DAY * 330,
        cloudStatus: daysRemaining,
        daysAfter: expirationDate && new Date().getTime() > new Date(expirationDate).getTime()
          ? differenceInDays(new Date(), new Date(expirationDate))
          : 0,
        subscription,
      };
    }

    case CHANGE_PLAYER_FLAG: {
      return {
        ...state,
        stopAllPlayers: payload,
      };
    }

    case CHANGE_CLOUD_IMAGE: {
      return {
        ...state,
        cloudImages: [payload],
      };
    }

    case PAYPAL_CLOUD_SUBSCRIPTION_START:
    case CREDIT_CARD_CLOUD_SUBSCRIPTION_START:
    case SAVED_CARD_CLOUD_SUBSCRIPTION_START: {
      return {
        ...state,
        spinnerFlag: true,
        paymentFail: false,
        paymentError: '',
      };
    }

    case PAYPAL_CLOUD_SUBSCRIPTION_SUCCESS:
    case CREDIT_CARD_CLOUD_SUBSCRIPTION_SUCCESS:
    case SAVED_CARD_CLOUD_SUBSCRIPTION_SUCCESS: {
      return {
        ...state,
        spinnerFlag: false,
        paymentFail: false,
      };
    }

    case PAYPAL_CLOUD_SUBSCRIPTION_FAIL:
    case CREDIT_CARD_CLOUD_SUBSCRIPTION_FAIL:
    case SAVED_CARD_CLOUD_SUBSCRIPTION_FAIL: {
      return {
        ...state,
        spinnerFlag: false,
        paymentFail: true,
        paymentError: payload,
      };
    }

    case GET_CLOUD_STATUS_SUCCESS: {
      const { daysRemaining = 0, expirationDate } = payload || {};
      return {
        ...state,
        isSixtyDaysHavePassed:
          expirationDate &&
          differenceInDays(new Date(), new Date(expirationDate)) > ONE_DAY * 60 &&
          differenceInDays(new Date(), new Date(expirationDate)) < ONE_DAY * 330,
        cloudStatus: daysRemaining,
      };
    }

    case GET_CLOUD_STATUS_FAIL: {
      return {
        ...state,
        cloudStatus: 0,
      };
    }

    case SET_PLAN_ID: {
      const { planId, paypalPlanId } = payload;
      return {
        ...state,
        planId,
        paypalPlanId,
      };
    }

    default:
      return state;
  }
};
