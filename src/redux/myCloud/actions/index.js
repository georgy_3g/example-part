import getRollbar from 'src/utils/getRollbar';
import {
  ADD_CLOUD_FILE_SUCCESS,
  DELETE_CLOUD_FILE_SUCCESS,
  ADD_ALL_FILES_SUCCESS,
  DELETE_ALL_FILES_SUCCESS,
  SET_CLOUD_STATUS,
  CHANGE_PLAYER_FLAG,
  CHANGE_CLOUD_IMAGE,
  PAYPAL_CLOUD_SUBSCRIPTION,
  SET_PLAN_ID,
  PAYPAL_CLOUD_SUBSCRIPTION_START,
  PAYPAL_CLOUD_SUBSCRIPTION_SUCCESS,
  PAYPAL_CLOUD_SUBSCRIPTION_FAIL,
  CREDIT_CARD_CLOUD_SUBSCRIPTION_START,
  CREDIT_CARD_CLOUD_SUBSCRIPTION_SUCCESS,
  CREDIT_CARD_CLOUD_SUBSCRIPTION_FAIL,
  SAVED_CARD_CLOUD_SUBSCRIPTION_START,
  SAVED_CARD_CLOUD_SUBSCRIPTION_SUCCESS,
  SAVED_CARD_CLOUD_SUBSCRIPTION_FAIL,
  GET_CLOUD_STATUS_START,
  GET_CLOUD_STATUS_SUCCESS,
  GET_CLOUD_STATUS_FAIL,
} from '../types';
import { apiCall, getError } from '../../utils';
import URL from '../../urls';

const rollbar = getRollbar();

const addFileToCloud = (payload) => async (dispatch) => {
  dispatch({
    type: ADD_CLOUD_FILE_SUCCESS,
    payload,
  });
};

const deleteFileFromCloud = (payload) => async (dispatch) => {
  dispatch({
    type: DELETE_CLOUD_FILE_SUCCESS,
    payload,
  });
};

const addAllFilesToCloud = (payload) => async (dispatch) => {
  dispatch({
    type: ADD_ALL_FILES_SUCCESS,
    payload,
  });
};

const deleteAllFilesFromCloud = () => async (dispatch) => {
  dispatch({
    type: DELETE_ALL_FILES_SUCCESS,
  });
};

const setCloudStatus = (payload) => async (dispatch) => {
  dispatch({
    type: SET_CLOUD_STATUS,
    payload,
  });
};

const changePlayerFlagAction = (payload) => async (dispatch) => {
  dispatch({
    type: CHANGE_PLAYER_FLAG,
    payload,
  });
};

const changeImageToCloud = (payload) => async (dispatch) => {
  dispatch({
    type: CHANGE_CLOUD_IMAGE,
    payload,
  });
};

const paypalCloudSubscribeAction = (payload) => async (dispatch) => {
  const { token, objectData } = payload;
  try {
    dispatch({ type: PAYPAL_CLOUD_SUBSCRIPTION_START });
    const {
      data: { data },
    } = await apiCall({
      type: 'POST',
      url: URL.PAYPAL_CLOUD_SUBSCRIPTION,
      body: { ...objectData },
      headers: { Authorization: `Bearer ${token}` },
    });
    dispatch({ type: PAYPAL_CLOUD_SUBSCRIPTION_SUCCESS, payload: { data } });
  } catch (error) {
    rollbar.error('paypal cloud subscription fail', error);
    dispatch({ type: PAYPAL_CLOUD_SUBSCRIPTION_FAIL, payload: getError(error) });
  }
  dispatch({
    type: PAYPAL_CLOUD_SUBSCRIPTION,
    payload,
  });
};

const credCardCloudSubscribeAction = (payload) => async (dispatch) => {
  const { token, objectData } = payload;
  try {
    dispatch({ type: CREDIT_CARD_CLOUD_SUBSCRIPTION_START });
    const {
      data: { data },
    } = await apiCall({
      type: 'POST',
      url: URL.CREDIT_CARD_CLOUD_SUBSCRIPTION,
      body: { ...objectData },
      headers: { Authorization: `Bearer ${token}` },
    });
    dispatch({
      type: CREDIT_CARD_CLOUD_SUBSCRIPTION_SUCCESS,
      payload: { data },
    });
  } catch (error) {
    rollbar.error('card cloud subscription fail', error);
    dispatch({ type: CREDIT_CARD_CLOUD_SUBSCRIPTION_FAIL, payload: getError(error) });
  }
};

const savedCardCloudSubscribeAction = (payload) => async (dispatch) => {
  const { token } = payload;
  try {
    dispatch({ type: SAVED_CARD_CLOUD_SUBSCRIPTION_START });
    await apiCall({
      type: 'POST',
      url: URL.SAVED_CARD_CLOUD_SUBSCRIPTION,
      headers: { Authorization: `Bearer ${token}` },
    });
    dispatch({
      type: SAVED_CARD_CLOUD_SUBSCRIPTION_SUCCESS,
    });
  } catch (error) {
    rollbar.error('saved card cloud subscription fail', error);
    dispatch({ type: SAVED_CARD_CLOUD_SUBSCRIPTION_FAIL, payload: getError(error) });
  }
};

const getCloudStatusAction = (payload) => async (dispatch) => {
  const { token } = payload;
  try {
    dispatch({ type: GET_CLOUD_STATUS_START });
    const {
      data: { data },
    } = await apiCall({
      type: 'get',
      url: URL.MEMOYA_CLOUD_STATUS,
      headers: { Authorization: `Bearer ${token}` },
    });
    dispatch({
      type: GET_CLOUD_STATUS_SUCCESS,
      payload: { ...data },
    });
  } catch (error) {
    dispatch({ type: GET_CLOUD_STATUS_FAIL, payload: getError(error) });
  }
};

const setPlanIdAction = (payload) => async (dispatch) => {
  dispatch({
    type: SET_PLAN_ID,
    payload,
  });
};

export {
  addFileToCloud,
  deleteFileFromCloud,
  addAllFilesToCloud,
  deleteAllFilesFromCloud,
  setCloudStatus,
  changePlayerFlagAction,
  changeImageToCloud,
  paypalCloudSubscribeAction,
  credCardCloudSubscribeAction,
  savedCardCloudSubscribeAction,
  getCloudStatusAction,
  setPlanIdAction,
};
