const myCloudImages = ({ myCloud }) => myCloud.cloudImages;
const myCloudAudio = ({ myCloud }) => myCloud.cloudAudio;
const myCloudVideo = ({ myCloud }) => myCloud.cloudVideo;
const myCloudFolder = ({ myCloud }) => myCloud.folder;
const myCloudError = ({ myCloud }) => myCloud.error;
const cloudStatus = ({ myCloud }) => myCloud.cloudStatus;
const stopAllPlayersSelector = ({ myCloud }) => myCloud.stopAllPlayers;
const paymentSpinnerFlagSelector = ({ myCloud }) => myCloud.spinnerFlag;
const paymentSubscribeFailSelector = ({ myCloud }) => myCloud.paymentFail;
const paymentSubscribeErrorSelector = ({ myCloud }) => myCloud.paymentError;
const planIdSelector = ({ myCloud }) => myCloud.planId;
const paypalPlanIdSelector = ({ myCloud }) => myCloud.paypalPlanId;
const isSixtyDaysHavePassedSelector = ({ myCloud }) => myCloud.isSixtyDaysHavePassed;
const subscriptionSelector = ({ myCloud }) => myCloud.subscription;
const daysAfterSubscribeSelector = ({ myCloud }) => myCloud.daysAfter;

export {
  myCloudImages,
  myCloudAudio,
  myCloudVideo,
  myCloudError,
  cloudStatus,
  stopAllPlayersSelector,
  paymentSpinnerFlagSelector,
  paymentSubscribeFailSelector,
  paymentSubscribeErrorSelector,
  planIdSelector,
  paypalPlanIdSelector,
  isSixtyDaysHavePassedSelector,
  subscriptionSelector,
  myCloudFolder,
  daysAfterSubscribeSelector,
};
