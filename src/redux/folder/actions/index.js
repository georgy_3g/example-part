import buildUrl from 'src/utils/createUrl';
import {CLEAR_FOLDER, CLEAR_ROOT_FOLDER, SET_ROOT_FOLDER, SET_FOLDER, SET_ROOM_FILES, CHANGE_ROOMS} from '../types';
import { apiCall, apiDownload, getError } from '../../utils';
import URL from '../../urls';
import {
  DOWNLOAD_ORDER_IMAGES_FAIL,
  DOWNLOAD_ORDER_IMAGES_START,
  DOWNLOAD_ORDER_IMAGES_SUCCESS,
} from '../../ordersList/types';

const getFolderById = (payload) => async (dispatch) => {
  const { token, folderId } = payload || {};
  try {
    const {
      data: { data },
    } = await apiCall({
      type: 'GET',
      url: `${URL.FOLDER_SYSTEM}/folder/${folderId}`,
      isToken: true,
      token,
    });
    dispatch({ type: SET_FOLDER, payload: data });
  } catch (error) {
    console.log(error);
  }
};
const getFolderRoot = (payload) => async (dispatch) => {
  const { token, orderId } = payload || {};
  try {
    const {
      data: { data },
    } = await apiCall({
      type: 'GET',
      url: `${URL.FOLDER_SYSTEM}/folder`,
      isToken: true,
      token,
      params: { orderId },
    });
    dispatch({ type: SET_ROOT_FOLDER, payload: data });
  } catch (error) {
    console.log(error);
  }
};
const getFolderRootOrder = (payload) => async (dispatch) => {
  const { token, orderId } = payload || {};
  try {
    const {
      data: { data },
    } = await apiCall({
      type: 'GET',
      url: `${URL.FOLDER_SYSTEM}/${orderId}/files`,
      isToken: true,
      token,
    });
    dispatch({ type: SET_ROOT_FOLDER, payload: data });
  } catch (error) {
    console.log(error);
  }
};
const sortFolderAction = (payload) => async (dispatch) => {
  const { token, filters } = payload || {};
  try {
    const {
      data: { data },
    } = await apiCall({
      type: 'GET',
      url: `${URL.FOLDER_SYSTEM}/folder/filter?${buildUrl({ filters })}`,
      isToken: true,
      token,
    });
    dispatch({ type: SET_FOLDER, payload: data });
  } catch (error) {
    console.log(error);
  }
};
const cleanFolder = () => (dispatch) => {
  dispatch({
    type: CLEAR_FOLDER,
  });
};
const clearRootFolder = () => (dispatch) => {
  dispatch({
    type: CLEAR_ROOT_FOLDER,
  });
};
const clearRoomFiles = (payload) => (dispatch) => {
  dispatch({
    payload,
    type: CHANGE_ROOMS,
  });
};
const setRoomFiles = (payload) => async (dispatch) => {
  const { chatToken, formData } = payload;
  try {
    const {
      data: { data },
    } = await apiCall({
      type: 'POST',
      url: URL.POST_CHAT_FILE,
      body: formData,
      isToken: true,
      token: chatToken,
    });
    dispatch({ type: SET_ROOM_FILES, payload: data });
  } catch (error) {
    console.log(error);
  }
};

const getArchiveActions = (payload) => async (dispatch) => {
  const { token, orderId, ids } = payload || {};

  try {
    dispatch({ type: DOWNLOAD_ORDER_IMAGES_START });
    // await apiStreamDownload({
    //   url: `${URL.FOLDER_SYSTEM}/${orderId}/files/archive?${buildUrl({ ids })}`,
    //   fileName: `${orderId}.zip`,
    //   token,
    //   // dispatch,
    //   // type: SET_DONWLOADED_STREAM_CHUNKS_SUCCESS,
    // })

    const { data: { data } } = await apiDownload({
      type: 'GET',
      url: `${URL.FOLDER_SYSTEM}/${orderId}/files/archive?${buildUrl({ ids })}`,
      fileName: `${orderId}.zip`,
      token,
      isToken: true,
      isStream: true,
      responseType: ''
    });
    if (!window.navigator.msSaveOrOpenBlob) {
      const link = document.createElement('a');
      link.href = `${URL.FOLDER_SYSTEM}/files/archive/${data}`;
      link.setAttribute('download', `${orderId}.zip`);
      document.body.appendChild(link);
      link.click();
    }
    dispatch({ type: DOWNLOAD_ORDER_IMAGES_SUCCESS });
  } catch (error) {
    dispatch({ type: DOWNLOAD_ORDER_IMAGES_FAIL, payload: getError(error) });
  }
};

export {
  cleanFolder,
  getFolderById,
  getFolderRoot,
  getFolderRootOrder,
  sortFolderAction,
  getArchiveActions,
  clearRootFolder,
  setRoomFiles,
  clearRoomFiles
};
