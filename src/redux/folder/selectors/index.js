const getRootFilesSelector = ({ folders }) => folders.rootFolder;
const getFileSelector = ({ folders }) => folders.folder;
const downloadedPercentageSelector = ({ folders }) => folders.downloadedPercentage;
const downloadSpeedSelector = ({ folders }) => folders.downloadSpeed;
const getRoomFiles = ({ folders }) =>  folders.roomFiles;

export { getRootFilesSelector, getFileSelector, downloadedPercentageSelector, downloadSpeedSelector, getRoomFiles };
