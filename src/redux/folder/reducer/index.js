import { HYDRATE } from 'next-redux-wrapper';
import checkHydrateData from 'src/utils/checkHydrateData';
import {
  CLEAR_ROOT_FOLDER,
  CLEAR_FOLDER,
  SET_ROOT_FOLDER,
  SET_FOLDER,
  SET_DONWLOADED_STREAM_CHUNKS_SUCCESS,
  SET_ROOM_FILES,
  CHANGE_ROOMS
} from '../types';

const INITIAL_STATE = {
  rootFolder: [],
  folder: [],
  contentLength: 0,
  receivedLength: 0,
  downloadedPercentage: 0,
  downloadSpeed: 0,
  roomFiles: []
};

export default (state = INITIAL_STATE, { type, payload }) => {
  switch (type) {
    case HYDRATE: {
      const isDiff = checkHydrateData(state, payload.folders);
      return isDiff ? { ...payload.folders } : state;
    }
    case SET_ROOT_FOLDER:
      return { ...state, rootFolder: payload };
    case SET_FOLDER:
      return { ...state, folder: payload };
    case CLEAR_ROOT_FOLDER:
      return { ...state, rootFolder: [] };
    case CLEAR_FOLDER:
      return { ...state, folder: [] };
    case SET_DONWLOADED_STREAM_CHUNKS_SUCCESS:
      return {
        ...state,
        downloadSpeed: (payload.receivedLength - state.receivedLength) / 1e+6,
        ...payload,
      };
    case SET_ROOM_FILES:
      return { ...state, roomFiles: [...state.roomFiles, payload] };

    case CHANGE_ROOMS:
      return { ...state, roomFiles: payload };

    default:
      return state;
  }
};
