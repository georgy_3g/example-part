import {
  REVIEWERS_API_TOKEN,
  REVIEWERS_API_SITE_ID,
  REVIEWERS_MERCHANT_API,
  REVIEWERS_LOCAL_API,
} from 'src/config';
import sortBy from 'lodash/sortBy';
import {
  GET_REVIEWERS_STATISTIC,
  GET_COMPANY_REVIEWERS,
  GET_NEW_REVIEWERS_START,
  GET_NEW_REVIEWERS_SUCCESS,
  GET_NEW_REVIEWERS_FAIL,
} from '../types';
import { apiCall } from '../../utils';

const loadReviewersStatistic = (payload) => (dispatch) => {
  dispatch({
    type: GET_REVIEWERS_STATISTIC,
    payload,
  });
};

const loadNewReviewers = (payload) => async (dispatch) => {
  const { limit, order, offset, isTopBar, topBarLimit } = payload;
  try {
    dispatch({ type: GET_NEW_REVIEWERS_START });
    const { data: companyData } = await apiCall({
      type: 'GET',
      url: REVIEWERS_MERCHANT_API,
      params: {
        apikey: REVIEWERS_API_TOKEN,
        store: REVIEWERS_API_SITE_ID,
        per_page: (isTopBar ? topBarLimit : limit) / 2,
        order,
        page: offset,
      },
    });
    

    const { data: localData } = await apiCall({
      type: 'GET',
      url: REVIEWERS_LOCAL_API,
      params: {
        store: REVIEWERS_API_SITE_ID,
        limit: (isTopBar ? topBarLimit : limit) / 2,
        page: offset + 1,
      },
    });
    const { reviews, total_pages: totalPages } = companyData;

    const {
      reviews: { data },
      stats: { total_reviews: total, average_rating: average },
    } = localData;

    const localReviews = data.map((item) => ({
      reviewer: { first_name: item.author },
      rating: item.rating,
      date_created: item.date_created,
      comments: item.comments,
      images: [],
    }));
    const allReviews = sortBy([...reviews, ...localReviews], [{'date_created': 'desc'}]);
    if (isTopBar) {
      dispatch({
        type: GET_NEW_REVIEWERS_SUCCESS,
        payload: {
          topBarReviews: allReviews,
          totalPages,
          total,
          average,
        },
      });
    } else {
      dispatch({
        type: GET_NEW_REVIEWERS_SUCCESS,
        payload: {
          reviews: allReviews,
          totalPages,
          total,
          average,
        },
      });
    }
  } catch (error) {
    dispatch({ type: GET_NEW_REVIEWERS_FAIL, payload: error });
  }
};

const loadCompanyReviewers = (payload) => (dispatch) => {
  dispatch({
    type: GET_COMPANY_REVIEWERS,
    payload,
  });
};

export { loadReviewersStatistic, loadNewReviewers, loadCompanyReviewers };
