import { HYDRATE } from 'next-redux-wrapper';
import checkHydrateData from 'src/utils/checkHydrateData';
import {
  GET_REVIEWERS_STATISTIC,
  GET_NEW_REVIEWERS_SUCCESS,
  GET_NEW_REVIEWERS_START,
  GET_NEW_REVIEWERS_FAIL,
} from '../types';

const INITIAL_STATE = {
  total: 0,
  totalPages: 0,
  average: 0,
  reviewers: [],
  topBarReviewers: [],
  loading: false,
  error: '',
};

export default (state = INITIAL_STATE, { type, payload }) => {
  switch (type) {
    case HYDRATE: {
      const isDiff = checkHydrateData(state, payload.reviewers);
      return isDiff ? { ...payload.reviewers } : state;
    }
    case GET_REVIEWERS_STATISTIC:
      return {
        ...state,
        reviewsCount: payload ? payload.total_reviews : state.reviewsCount,
        sharedRate: payload ? payload.average_rating : state.sharedRate,
      };

    case GET_NEW_REVIEWERS_START:
      return {
        ...state,
        loading: true,
      };

    case GET_NEW_REVIEWERS_SUCCESS:
      return {
        ...state,
        reviewers: payload.reviews || state.reviewers,
        topBarReviewers: payload.topBarReviews || state.topBarReviewers,
        totalPages: payload.totalPages || state.totalPages,
        total: payload.total || state.total,
        average: payload.average || state.average,
        loading: false,
      };

    case GET_NEW_REVIEWERS_FAIL:
      return {
        ...state,
        error: payload,
      };

    default:
      return state;
  }
};
