const reviewsCountSelect = ({ reviewers }) => reviewers.total;
const reviewsRateSelect = ({ reviewers }) => reviewers.average;
const reviewsSelect = ({ reviewers }) => reviewers.reviewers;
const reviewsLoadingSelect = ({ reviewers }) => reviewers.loading;
const reviewTotalPagesSelect = ({ reviewers }) => reviewers.totalPages;

export {
  reviewsCountSelect,
  reviewsRateSelect,
  reviewsSelect,
  reviewsLoadingSelect,
  reviewTotalPagesSelect,
};
