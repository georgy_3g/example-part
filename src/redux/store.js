import { createStore, applyMiddleware, compose } from 'redux';
import { NEXT_PUBLIC_DEV_TOOLS_ENABLE } from 'src/config';
import thunkMiddleware from 'redux-thunk';
import { composeWithDevTools } from '@redux-devtools/extension';
import reduxCatch from 'src/utils/reduxCatch';
import {nextReduxCookieMiddleware, wrapMakeStore} from 'next-redux-cookie-wrapper';
import { createWrapper } from 'next-redux-wrapper';
import reducersList from './reducers';

const paths = [
  {subtree: 'auth', secure: false},
  {subtree: 'coupon', secure: false},
  {subtree: 'orderRush', secure: false},
  {subtree: 'orderCreate', secure: false},
  {subtree: 'location', secure: false},
];

const init = (store) => {
  return store;
};

const errorHandler = (error, getState, lastAction, dispatch) => {
  console.error('--==redux middleware error==--', error);
  console.debug('--==last action was==--', lastAction && lastAction.type ? lastAction.type : lastAction);
}

const initialStore = wrapMakeStore(() => {

  const middleware = [
    reduxCatch(errorHandler),
    thunkMiddleware,
    nextReduxCookieMiddleware({
      subtrees: paths,
      secure: false,
      compress: false,
      sameSite: false,
    }),
  ];

  if (NEXT_PUBLIC_DEV_TOOLS_ENABLE) {
    const store = createStore(
      reducersList,
      composeWithDevTools(applyMiddleware(...middleware)),
    );
    return init(store);
  }

  const store = createStore(reducersList, compose(applyMiddleware(...middleware)));
  return init(store)
});


// eslint-disable-next-line import/prefer-default-export
export const wrapper = createWrapper(initialStore, {debug: false});
