import { combineReducers } from 'redux';
import reviewers from './reviewers/reducer';
import prices from './prices/reducer';
import blogList from './blogList/reducer';
import auth from './auth/reducer';
import myCloud from './myCloud/reducer';
import folders from './folder/reducer';

const rootReducer = combineReducers({
  reviewers,
  prices,
  blogList,
  auth,
  myCloud,
  folders,
});

export default rootReducer;
