import { HYDRATE } from 'next-redux-wrapper';
import checkHydrateData from 'src/utils/checkHydrateData';
import {
  AUTH_SUCCESS,
  REMOVE_TOKEN,
  OPEN_LOGIN_POPUP,
  CLOSE_LOGIN_POPUP,
  SET_DEVICE_TYPE,
  SET_APP_VERSION,
  CLEAR_STORAGES
} from '../types';

import { NEW_PASS_SUCCESS } from '../../shared/types';

const INITIAL_STATE = {
  chatToken: null,
  token: null,
  rememberMe: false,
  isOpenLoginPopup: false,
  startTime: null,
  userId: null,
  isMobileDevice: true,
  withoutRedirect: false,
  appVersion: null,
};

const auth = (state = INITIAL_STATE, { type, payload }) => {
  switch (type) {
    case HYDRATE: {
      const isDiff = checkHydrateData(state, payload.auth);
      return isDiff ? { ...payload.auth } : state;
    }
    case AUTH_SUCCESS: {
      const { token, rememberMe, id, chatToken } = payload;
      return {
        ...state,
        token,
        rememberMe,
        userId: id,
        startTime: Number(new Date()),
        chatToken,
      };
    }
    case REMOVE_TOKEN: {
      const { isOpenLoginPopup, appVersion } = state;
      return {
        ...INITIAL_STATE,
        isOpenLoginPopup,
        appVersion,
      };
    }
    case NEW_PASS_SUCCESS: {
      const { token, id } = payload;
      return {
        ...state,
        token,
        userId: id,
        rememberMe: false,
        startTime: Number(new Date()),
      };
    }

    case OPEN_LOGIN_POPUP: {
      return {
        ...state,
        isOpenLoginPopup: true,
        withoutRedirect: Boolean(payload),
      };
    }

    case CLOSE_LOGIN_POPUP: {
      return {
        ...state,
        isOpenLoginPopup: false,
        withoutRedirect: false,
      };
    }

    case SET_DEVICE_TYPE: {
      return {
        ...state,
        isMobileDevice: payload,
      };
    }

    case SET_APP_VERSION: {
      return {
        ...INITIAL_STATE,
        appVersion: payload,
      };
    }

    case CLEAR_STORAGES: {
      localStorage.removeItem('ordersImages');
      localStorage.removeItem('editOrder');
      localStorage.removeItem('enhance/Option');
      localStorage.removeItem('promoCode');
      localStorage.removeItem('orderCreate');
      localStorage.removeItem('orders');

      sessionStorage.removeItem('projectData');
      sessionStorage.removeItem('isShippingTargetPopup');
      sessionStorage.removeItem('cloudImageData');
      sessionStorage.removeItem('promoCode');
      sessionStorage.removeItem('wizardData');

      const cookies = document.cookie.split(";");

      cookies.forEach((cookie) => {
        const eqPos = cookie.indexOf("=");
        const name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
        document.cookie = `${name}=;expires=Thu, 01 Jan 1970 00:00:00 GMT`;
      })

      return {
        ...INITIAL_STATE,
        appVersion: payload,
      };
    }

    default:
      return state;
  }
};

export default auth;
