import {
  AUTH_SUCCESS,
  REMOVE_TOKEN,
  OPEN_LOGIN_POPUP,
  CLOSE_LOGIN_POPUP,
  SET_DEVICE_TYPE,
  SET_APP_VERSION,
  CLEAR_AFTER_APP_VERSION_CHANGE,
  CLEAR_STORAGES,
} from '../types';

const authSuccess = (payload) => async (dispatch) => {
  dispatch({
    type: AUTH_SUCCESS,
    payload,
  });
};

const removeToken = () => async (dispatch) => {
  dispatch({
    type: REMOVE_TOKEN,
  });
};

const openLoginPopupAction = (payload) => async (dispatch) => {
  dispatch({
    type: OPEN_LOGIN_POPUP,
    payload,
  });
};

const closeLoginPopupAction = () => async (dispatch) => {
  dispatch({
    type: CLOSE_LOGIN_POPUP,
  });
};

const setDeviceTypeAction = (payload) => async (dispatch) => {
  dispatch({
    type: SET_DEVICE_TYPE,
    payload,
  });
};

const setAppVersionAction = (payload) => async (dispatch) => {
  dispatch({
    type: SET_APP_VERSION,
    payload,
  });
};

const clearAfterAppVersionChangeAction = () => async (dispatch) => {
  dispatch({
    type: CLEAR_AFTER_APP_VERSION_CHANGE,
  });
};

const clearStoragesAction = (payload) => async (dispatch) => {
  dispatch({
    type: CLEAR_STORAGES,
    payload,
  });
};

export {
  authSuccess,
  removeToken,
  openLoginPopupAction,
  closeLoginPopupAction,
  setDeviceTypeAction,
  setAppVersionAction,
  clearAfterAppVersionChangeAction,
  clearStoragesAction,
};
