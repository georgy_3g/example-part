const getToken = ({ auth }) => auth.token;
const isRememberAuth = ({ auth }) => auth.rememberMe;
const isOpenLoginPopupSelector = ({ auth }) => auth.isOpenLoginPopup;
const userIdSelector = ({ auth }) => auth.userId;
const chatTokenSelector = ({ auth }) => auth.chatToken;
const isMobileDeviceSelector = ({ auth }) => auth.isMobileDevice;
const withoutRedirectSelector = ({ auth }) => auth.withoutRedirect;
const appVersionSelector = ({ auth }) => auth.appVersion;

export {
  getToken,
  isRememberAuth,
  isOpenLoginPopupSelector,
  chatTokenSelector,
  userIdSelector,
  isMobileDeviceSelector,
  withoutRedirectSelector,
  appVersionSelector,
};
