import {
  CHECK_COUPON_FAIL,
  CHECK_COUPON_START,
  CHECK_COUPON_SUCCESS,
  CLEAR_COUPON,
  SET_COUPON,
} from "../types";
import { apiCall, getError } from "../../utils";
import URL from "../../urls";


const checkCoupon = payload => async dispatch => {
  const { token, couponName, userId } = payload;
  const isToken = true;
  try {
    dispatch({ type: CHECK_COUPON_START });
    const { data: { data } } = await apiCall({
      type: 'GET',
      url: URL.PROMO_CODE,
      params: { name: couponName, ...(userId ? { userId } : {}) },
      isToken,
      token,
    });
    dispatch({ type: CHECK_COUPON_SUCCESS, payload: data });
    sessionStorage.setItem('promoCode', JSON.stringify(data));
  } catch (error) {
    const err = error && error.response && error.response.data
      ? error.response.data
      : getError(error);
    dispatch({ type: CHECK_COUPON_FAIL, payload: err });
  }
}

const clearCoupon = () => dispatch => {
  dispatch({
    type: CLEAR_COUPON,
  });
}

const setCoupon = (coupon) => dispatch => {
  dispatch({
    type: SET_COUPON,
    payload: coupon,
  });
}

export {
  checkCoupon,
  clearCoupon,
  setCoupon,
};
