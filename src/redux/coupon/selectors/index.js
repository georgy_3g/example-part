const couponNameSelect = ({ coupon }) => coupon.coupon.name;
const couponTypeSelect = ({ coupon }) => coupon.coupon.type;
const couponValueSelect = ({ coupon }) => coupon.coupon.value;
const couponActiveSelect = ({ coupon }) => coupon.coupon.isActive;
const couponSelect = ({ coupon }) => coupon.coupon;
const couponError = ({ coupon }) => coupon.error;

export {
  couponNameSelect,
  couponTypeSelect,
  couponValueSelect,
  couponActiveSelect,
  couponSelect,
  couponError,
};
