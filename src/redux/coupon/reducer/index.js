import { ONE_DAY } from 'src/constants';
import { HYDRATE } from 'next-redux-wrapper';
import checkHydrateData from 'src/utils/checkHydrateData';
import differenceInDays from "date-fns/differenceInDays";
import {
  CHECK_COUPON_START,
  CHECK_COUPON_SUCCESS,
  CHECK_COUPON_FAIL,
  CLEAR_COUPON,
  SET_COUPON,
} from '../types';

import { REMOVE_TOKEN, CLEAR_AFTER_APP_VERSION_CHANGE } from '../../auth/types';

import { CLEAR_ORDER_SUCCESS, UPDATE_CART } from '../../orders/types';

const INITIAL_STATE = {
  coupon: {},
  error: null,
};

export default (state = INITIAL_STATE, { type, payload }) => {
  switch (type) {
    case HYDRATE: {
      const isDiff = checkHydrateData(state, payload.coupon);
      return isDiff ? { ...payload.coupon } : state;
    }

    case CHECK_COUPON_START: {
      return {
        ...state,
        error: null,
      };
    }

    case CHECK_COUPON_SUCCESS: {
      return {
        ...state,
        coupon: payload,
        error: null,
      };
    }

    case CHECK_COUPON_FAIL: {
      return {
        ...INITIAL_STATE,
        error: payload,
        coupon: {},
      };
    }

    case SET_COUPON: {
      return {
        ...state,
        coupon: payload,
        error: null,
      };
    }

    case CLEAR_COUPON:
    case REMOVE_TOKEN:
    case CLEAR_ORDER_SUCCESS: {
      sessionStorage.removeItem('promoCode');
      return {
        ...INITIAL_STATE,
      };
    }

    case CLEAR_AFTER_APP_VERSION_CHANGE: {
      return {
        ...INITIAL_STATE,
      };
    }

    case UPDATE_CART: {
      const orderId = JSON.parse(localStorage.getItem('orderCreate') || '{}');
      const page = document.location.pathname;

      const { date, id } = orderId || {};
      const needDeleteOrderId = Boolean((!date || differenceInDays(new Date(date), new Date()) > ONE_DAY) && id);

      if (
        (page !== '/payment/' &&
          page !== '/cart/checkout/' &&
          page !== '/payment' &&
          page !== '/cart/checkout' &&
          id) ||
        needDeleteOrderId
      ) {
        return { ...INITIAL_STATE };
      }

      return { ...state };
    }

    default:
      return state;
  }
};
