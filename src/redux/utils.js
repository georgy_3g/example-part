import axios from 'axios';

const getAuth = (isToken, token) => {
  if (isToken) {
    return { Authorization: `Bearer ${token}` };
  }
  return {};
};

const apiCall = ({ type, body, headers, url, isToken, params, token, responseType }) => {
  const auth = getAuth(isToken, token);
  // const options = {
  //     method: type,
  //     headers: {
  //         'Content-Type': 'application/json',
  //         ...headers,
  //         ...auth,
  //     },
  //     body,
  //     params,
  //     responseType,
  // };

  return axios({
    url,
    headers: {
      'Content-Type': 'application/json',
      ...(headers || {}),
      ...(auth || {}),
    },
    data: body,
    method: type,
    params,
    responseType,
  });
};

const getError = ({ response }) => ((response || {}).data || {}).message;

const getErrorCode = ({ response }) => ((response || {}).data || {}).errorCode;

const deleteEmptyFields = (values) => {
  const keys = Object.keys(values);
  return keys.reduce((acc, item) => (values[item] ? { ...acc, [item]: values[item] } : acc), {});
};

const checkSameEmail = (values, email) => {
  const { email: emailValue, ...rest } = values;
  return emailValue === email ? rest : values;
};

const apiDownload = ({
  headers,
  url,
  type,
  isToken,
  token,
  params,
  fileName,
  onDownloadProgress,
  isStream,
}) => {
  const auth = getAuth(isToken, token);
  const options = {
    method: type,
    params,
    headers: {
      ...auth,
      ...headers,
    },
    responseType: isStream ? 'application/json' : 'blob',
    url,
    onDownloadProgress,
  };

  if(isStream) {
    return axios(options)
  }
  return axios(options).then((response) => {
    if (!window.navigator.msSaveOrOpenBlob) {
      const newUrl = window.URL.createObjectURL(new Blob([response.data]));
      const link = document.createElement('a');
      link.href = newUrl;
      link.setAttribute('download', fileName || 'invoice.pdf');
      document.body.appendChild(link);
      link.click();
    } else {
      window.navigator.msSaveOrOpenBlob(new Blob([response.data]), 'invoice.pdf');
    }
  });
};

const apiStreamDownload = async ({
    url,
    token,
    fileName,
    dispatch,
    type,
  }) => {
  const response = await fetch(url,{
    method: 'GET',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`,
    }
  })
  const reader = response.body.getReader();
  const contentLength = +response.headers.get('Content-Length');
  let receivedLength = 0;
  let currentTime = new Date();
  const chunks = [];
  // eslint-disable-next-line no-constant-condition
  while(true) {
    // eslint-disable-next-line no-await-in-loop
    const {done, value} = await reader.read();
    if (done) {
      break;
    }
    chunks.push(value);
    receivedLength += value.length
    const time = new Date();
    if((time.getTime() - currentTime.getTime() >= 1000) || (receivedLength === contentLength)) {
      currentTime = time;
      dispatch({ type, payload: {
          receivedLength,
          contentLength,
          downloadedPercentage: receivedLength / 1e+6,
        }})
    }

  }
  if (!window.navigator.msSaveOrOpenBlob) {
    // const newUrl = window.URL.createObjectURL(new Blob(chunks));
    const link = document.createElement('a');
    link.href = response;
    link.setAttribute('download', fileName);
    document.body.appendChild(link);
    link.click();
  }
  else {
    window.navigator.msSaveOrOpenBlob(new Blob(response), fileName);
  }
}

export { apiCall, getError, deleteEmptyFields, checkSameEmail, apiDownload, getErrorCode, apiStreamDownload };
