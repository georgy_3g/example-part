const ERRORS = {
  SHOULD_BE_FILLED: 'This field should be filled',
  ENTER_NAME: 'Please enter a name',
  INVALID_EMAIL: 'Invalid email address. Please try again.',
  SHORT_PASSWORD: 'Password must be at least 6 characters long',
  SHORT_NAME: 'Name must at least 2 characters',
  WRONG_LOGIN: 'Wrong email or password. Please try again.',
  PASSWORDS_MATCH: 'Passwords does not match',
  SHOULD_BE_SELECTED: 'Should be selected',
  MESSAGE_404: "Oh no! The page you're looking is not available.",
  CONFIRM_PASSWORD_ERROR: 'Your password and confirmation password do not match',
  CHOOSE_DELIVERABLES: 'You need to choose deliverables.',
  ENHANCE_UPLOAD_ERROR:
    'You can upload max 25 photos at once. Max size of photo 25mb. You can upload jpeg, jpg, png, pdf.',
};

export default ERRORS;
