const ORIENTATION_TYPES = {
  horizontal: 'horizontal',
  vertical: 'vertical',
};

const PHONE_MASK = '***-***-****';
const CARD_MASK = '9999 9999 9999 9999';
const AE_CARD_MASK = '9999 999999 99999';
const CARD_EXPIRATION_DATE = '99/99';

const CHECK_WHITESPACE = /\s+/g;
const TWO_DAYS = 172800000;
const ONE_DAY = 86400000;

const IMAGE_TYPE_JPEG = 'image/jpeg';

export {
  ORIENTATION_TYPES,
  PHONE_MASK,
  CARD_MASK,
  AE_CARD_MASK,
  CARD_EXPIRATION_DATE,
  CHECK_WHITESPACE,
  TWO_DAYS,
  ONE_DAY,
  IMAGE_TYPE_JPEG,
};
