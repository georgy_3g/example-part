import React from 'react';
import PropTypes from 'prop-types';
import Colors from 'src/styles/colors.json';

const blueWoodColor = Colors['$pickled-bluewood-color'];

const propTypes = {
  fill: PropTypes.string,
  stroke: PropTypes.string,
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
  open: PropTypes.bool,
};

const defaultProps = {
  fill: 'none',
  stroke: blueWoodColor,
  className: '',
  width: '21',
  height: '20',
  open: false,
};


function ExpansionDecreaseIcon({
  className,
  width,
  height,
  fill,
  stroke,
  open,
}) {
  return (
    <>
      {open ? (
        <svg
          className={className}
          width={width}
          height={height}
          fill={fill}
          viewBox="0 0 17 16"
        >
          <path
            stroke={stroke}
            strokeLinecap="round"
            strokeWidth="1.5"
            d="M14.89 14.266l-3.324-3.269M15.137 10.129H10.77M10.723 14.496V10.13M1.445 1.773l4.418 4.024M1.445 5.793h4.367M5.863 1.426v4.366"
          />
        </svg>
    ) : (
      <svg
        className={className}
        width={width}
        height={height}
        fill={fill}
        viewBox="0 0 21 20"
      >
        <path
          stroke={stroke}
          strokeLinecap="round"
          strokeWidth="1.5"
          d="M12.785 12.172l3.325 3.268M12.54 16.309h4.365M16.953 11.941v4.367M8.047 7.629L3.629 3.605M8.047 3.605H3.68M3.629 7.973V3.606"
        />
      </svg>
    )}
    </>
  );
}

ExpansionDecreaseIcon.propTypes = propTypes;
ExpansionDecreaseIcon.defaultProps = defaultProps;

export default ExpansionDecreaseIcon;
