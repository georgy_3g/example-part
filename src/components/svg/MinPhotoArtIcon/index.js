import React from 'react';
import PropTypes from 'prop-types';
import Colors from 'src/styles/colors.json';

const greenColor = Colors['$summer-green-color'];
const blueColor = Colors['$pickled-bluewood-color'];

const propTypes = {
  stroke: PropTypes.string,
  stroke2: PropTypes.string,
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
  strokeWidth: PropTypes.string,
};

const defaultProps = {
  stroke: greenColor,
  stroke2: blueColor,
  className: '',
  width: '46',
  height: '46',
  strokeWidth: '2',
};

function MinPhotoArtIcon({
  width,
  height,
  stroke,
  stroke2,
  className,
  strokeWidth,
}) {
  return (
    <svg
      width={width}
      height={height}
      fill="none"
      viewBox="0 0 46 46"
      className={className}
    >
      <rect
        width="38"
        height="33"
        x="4"
        y="6"
        stroke={stroke2}
        strokeWidth={strokeWidth}
        rx="6"
      />
      <path
        stroke={stroke}
        strokeLinecap="round"
        strokeWidth={strokeWidth}
        d="M10.648 27.041l5.805-8.05a1.447 1.447 0 012.04-.313l2.744 2.05M19.469 25.313l3.008-4.465a1.447 1.447 0 012.112-.314l2.527 2.053"
      />
      <circle cx="33" cy="15" r="3" stroke={stroke} strokeWidth={strokeWidth} />
      <path
        stroke={stroke}
        strokeLinecap="round"
        strokeWidth={strokeWidth}
        d="M8.883 31.953h21.176"
      />
    </svg>
  );
}

MinPhotoArtIcon.propTypes = propTypes;
MinPhotoArtIcon.defaultProps = defaultProps;

export default MinPhotoArtIcon;
