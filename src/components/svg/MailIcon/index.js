import React from 'react';
import PropTypes from 'prop-types';
import Colors from 'src/styles/colors.json';

const white = Colors['$white-color'];

const propTypes = {
  fill: PropTypes.string,
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
};

const defaultProps = {
  fill: white,
  className: '',
  width: '24',
  height: '24',
};

function MailIcon({
  width,
  height,
  fill,
  className,
}) {
  return (
    <svg
      className={className}
      width={width}
      height={height}
      fill="none"
      viewBox="0 0 24 24"
    >
      <path
        fill={fill}
        fillRule="evenodd"
        d="M5 5.25h14c.548 0 1.055.177 1.468.476a.096.096 0 010 .154l-7.706 5.955a1.25 1.25 0 01-1.529 0L3.53 5.882a.096.096 0 010-.154c.413-.3.921-.478 1.471-.478zM2.714 7.148a.096.096 0 00-.153.052c-.04.177-.061.361-.061.55v8.5c0 .145.012.288.036.427.013.074.102.102.158.052l5.708-5.036a.1.1 0 00-.005-.154L2.714 7.148zm7.053 5.45a.1.1 0 00-.128.004l-6.19 5.462a.097.097 0 00.002.148A2.49 2.49 0 005 18.75h14c.585 0 1.123-.2 1.549-.538a.097.097 0 00.002-.148L14.358 12.6a.1.1 0 00-.127-.005l-.552.427a2.75 2.75 0 01-3.363 0l-.55-.424zm5.834-1.06a.1.1 0 00-.005.153l5.71 5.038c.056.05.145.022.158-.052a2.51 2.51 0 00.036-.427v-8.5c0-.19-.021-.375-.061-.553a.096.096 0 00-.153-.053L15.6 11.537zM1 7.75a4 4 0 014-4h14a4 4 0 014 4v8.5a4 4 0 01-4 4H5a4 4 0 01-4-4v-8.5z"
        clipRule="evenodd"
      />
    </svg>
  );
}

MailIcon.propTypes = propTypes;
MailIcon.defaultProps = defaultProps;

export default MailIcon;
