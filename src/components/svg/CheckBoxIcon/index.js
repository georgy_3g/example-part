import React from 'react';
import PropTypes from 'prop-types';
import Colors from 'src/styles/colors.json';

const gray = Colors['$slate-gray-color'];
const blue = Colors['$pickled-bluewood-color'];

const propTypes = {
  fill: PropTypes.string,
  stroke: PropTypes.string,
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
};

const defaultProps = {
  fill: gray,
  stroke: blue,
  className: '',
  width: '24',
  height: '24',
};


function CheckBoxIcon({
  width,
  height,
  fill,
  stroke,
  className,
}) {
  return (
    <svg
      width={width}
      height={height}
      version="1.1"
      viewBox="0 0 24 24"
      className={className}
      fill="none"
    >
      <rect
        width="22"
        height="22"
        x="1"
        y="1"
        stroke={fill}
        strokeWidth="2"
        rx="5"
      />
      <path
        stroke={stroke}
        strokeLinecap="round"
        strokeWidth="2"
        d="M6 13.343l2.817 2.872a1 1 0 001.48-.057L18 7"
      />
    </svg>
  );
}

CheckBoxIcon.propTypes = propTypes;
CheckBoxIcon.defaultProps = defaultProps;

export default CheckBoxIcon;
