import React from 'react';
import PropTypes from 'prop-types';
import Colors from 'src/styles/colors.json';

const blue = Colors['$pickled-bluewood-color'];
const white = Colors['$white-color'];

const propTypes = {
  fill: PropTypes.string,
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
  approved: PropTypes.bool,
};

const defaultProps = {
  fill: 'none',
  className: '',
  width: '24',
  height: '24',
  approved: false,
};

function ApprovingIcon({ className, approved, fill, width, height }) {
  return approved ? (
    <svg className={className} width={width} height={height} fill={fill} viewBox="0 0 23 21">
      <rect
        width="18.541"
        height="16.036"
        x="1.727"
        y="1.043"
        stroke={blue}
        strokeWidth="1.5"
        rx="3.25"
      />
      <circle
        r="5.25"
        fill={white}
        stroke={blue}
        strokeLinecap="round"
        strokeWidth="1.5"
        transform="matrix(-1 0 0 1 17 15)"
      />
      <path
        stroke={blue}
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth="1.5"
        d="M15 15.5l1.302 1 2.698-3"
      />
      <path
        stroke={blue}
        strokeLinecap="round"
        strokeWidth="1.5"
        d="M5.527 11.339L8.007 7.9a1.447 1.447 0 012.04-.313l.785.588M9.95 10.47l1.065-1.583a1.447 1.447 0 012.113-.314l.653.53"
      />
      <circle cx="15.75" cy="5.25" r="1.5" stroke={blue} strokeWidth="1.5" />
      <path stroke={blue} strokeLinecap="round" strokeWidth="1.5" d="M4.648 13.797H11.5" />
    </svg>
  ) : (
    <svg className={className} width={width} height={height} fill={fill} viewBox="0 0 24 24">
      <path
        stroke={blue}
        strokeLinecap="round"
        strokeWidth="1.5"
        d="M22.188 13.127a10.25 10.25 0 10-20.376-2.254 10.25 10.25 0 0020.376 2.254h0z"
      />
      <circle
        r="5.25"
        fill={white}
        stroke={blue}
        strokeLinecap="round"
        strokeWidth="1.5"
        transform="matrix(-1 0 0 1 18 18)"
      />
      <path
        stroke={blue}
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth="1.5"
        d="M16 18.5l1.302 1 2.698-3"
      />
      <path
        stroke={blue}
        strokeLinecap="round"
        strokeWidth="1.5"
        d="M12 6.447v5.545a.654.654 0 01-.653.654H7"
      />
    </svg>
  );
}

ApprovingIcon.propTypes = propTypes;
ApprovingIcon.defaultProps = defaultProps;

export default ApprovingIcon;
