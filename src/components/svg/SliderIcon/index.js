import React from 'react';
import PropTypes from 'prop-types';
import Colors from 'src/styles/colors.json';

const color = Colors['$darker-cadet-blue-color'];

const propTypes = {
  stroke: PropTypes.string,
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
};

const defaultProps = {
  stroke: color,
  className: '',
  width: '52',
  height: '40',
};

function SliderIcon({
  className,
  stroke,
  width,
  height,
}) {
  return (
    <svg className={className} width={width} height={height} viewBox="0 0 52 40" fill="none">
      <rect
        width="32"
        height="32"
        x="10"
        y="4"
        stroke={stroke}
        strokeWidth="2"
        rx="5"
      />
      <path
        stroke={stroke}
        strokeLinecap="round"
        strokeWidth="2"
        d="M17 28.07l5.067-7.028a1.263 1.263 0 011.78-.273l2.395 1.79M24.7 26.559l2.625-3.897a1.263 1.263 0 011.844-.275l2.205 1.793"
      />
      <circle
        cx="32.507"
        cy="13.492"
        r="2.491"
        stroke={stroke}
        strokeWidth="2"
      />
      <path
        stroke={stroke}
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth="2"
        d="M48 16l2.806 3.428c.125.157.194.36.194.572 0 .212-.07.415-.194.572L48 24M4 16l-2.806 3.428A.919.919 0 001 20c0 .212.07.415.194.572L4 24"
      />
    </svg>
  );
}

SliderIcon.propTypes = propTypes;
SliderIcon.defaultProps = defaultProps;

export default SliderIcon;
