import React from 'react';
import PropTypes from 'prop-types';
import Colors from 'src/styles/colors.json';

const whiteColor = Colors['$white-color'];

const propTypes = {
  stroke: PropTypes.string,
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
  strokeWidth: PropTypes.string,
};

const defaultProps = {
  stroke: whiteColor,
  className: '',
  width: '25',
  height: '22',
  strokeWidth: '1.5',
};

function CartIcon({
  width,
  height,
  stroke,
  className,
  strokeWidth,
}) {
  return (
    <svg className={className} width={width} height={height} fill="none" viewBox="0 0 25 22">
      <title>cart button</title>
      <path
        stroke={stroke}
        strokeWidth={strokeWidth}
        d="M7.472 4a1.2 1.2 0 00-1.066 1.751L9.38 11.5h10.323a2.4 2.4 0 002.138-1.31l2.268-4.445A1.2 1.2 0 0023.04 4H7.473z"
        clipRule="evenodd"
      />
      <circle
        cx="21.25"
        cy="19.25"
        r="1.5"
        stroke={stroke}
        strokeWidth={strokeWidth}
      />
      <circle
        cx="10.75"
        cy="19.25"
        r="1.5"
        stroke={stroke}
        strokeWidth={strokeWidth}
      />
      <path
        stroke={stroke}
        strokeLinecap="round"
        strokeWidth={strokeWidth}
        d="M9.398 11.5l1.014 2.286a1.2 1.2 0 001.097.714h9.89M1 1h1.5a1.2 1.2 0 011.07.656L4 2.5"
      />
    </svg>
  );
}

CartIcon.propTypes = propTypes;
CartIcon.defaultProps = defaultProps;

export default CartIcon;
