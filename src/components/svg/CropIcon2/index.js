import React from 'react';
import PropTypes from 'prop-types';
import Colors from 'src/styles/colors.json';

const strokeColor = Colors['$pickled-bluewood-color'];

const propTypes = {
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
  stroke: PropTypes.string,
};

const defaultProps = {
  className: '',
  width: '24',
  height: '24',
  stroke: strokeColor,
};

function CropIcon2(props) {
  const { className, width, height, stroke } = props;
  return (
    <svg className={className} width={width} height={height} fill="none" viewBox="0 0 24 24">
      <path
        stroke={stroke}
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth="2"
        d="M6 1v15a2 2 0 002 2h15"
      />
      <path
        stroke={stroke}
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth="2"
        d="M9 6h7a2 2 0 012 2v7M18 21v2M1 6h2"
      />
    </svg>
  );
}

CropIcon2.propTypes = propTypes;
CropIcon2.defaultProps = defaultProps;

export default CropIcon2;
