export interface propsInterface {
  fill?: string;
  className?: string;
  width?: string;
  height?: string;
  stroke?: string;
  stroke2?: string;
  strokeWidth?: string;
}
