import React, { FunctionComponent } from 'react';
import { propsInterface } from './interface';

const defaultProps = {
  fill: '#9EE5D8',
  className: '',
  width: '24',
  height: '24',
  stroke: '#123C55',
  stroke2: '',
  strokeWidth: '2',
};

const CheckFull: FunctionComponent<propsInterface> = ({
  width,
  height,
  fill,
  className,
  stroke,
  stroke2,
  strokeWidth,
}) => (
  <svg className={className} width={width} height={height} viewBox="0 0 24 24">
    <g fill="none" fillRule="evenodd" strokeWidth={strokeWidth} transform="translate(1)">
      <circle cx="11" cy="12" r="11" fill={fill} stroke={stroke} />
      <path
        strokeLinecap="round"
        strokeLinejoin="round"
        d="M6 12.606l3.606 3.606L16.818 9"
        stroke={stroke2 || stroke}
      />
    </g>
  </svg>
);

CheckFull.defaultProps = defaultProps;

export default CheckFull;
