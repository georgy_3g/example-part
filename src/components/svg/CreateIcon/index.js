import React from 'react';
import PropTypes from 'prop-types';
import Colors from 'src/styles/colors.json';

const blue = Colors['$pickled-bluewood-color'];
const green = Colors['$summer-green-color'];
const borderColor = Colors['$slate-gray-color'];
const gray = Colors['$silver-gray-color'];

const propTypes = {
  fill: PropTypes.string,
  fill2: PropTypes.string,
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
  disabled: PropTypes.bool,
  stroke: PropTypes.string,
};

const defaultProps = {
  fill: blue,
  fill2: green,
  className: '',
  width: '70',
  height: '70',
  disabled: false,
  stroke: borderColor,
};

function CreateIcon({
  width,
  height,
  fill2,
  fill,
  stroke,
  className,
  disabled,
}) {
  return (
    <svg
      width={width}
      height={height}
      version="1.1"
      viewBox="0 0 70 70"
      className={className}
      fill="none"
    >
      <rect
        width="68"
        height="68"
        x="1"
        y="1"
        stroke={disabled ? gray : stroke}
        strokeWidth="2"
        rx="19"
      />
      <rect
        width="38"
        height="33"
        x="16"
        y="18"
        stroke={disabled ? gray : fill}
        strokeWidth="2"
        rx="6"
      />
      <path
        stroke={disabled ? gray : fill2}
        strokeLinecap="round"
        strokeWidth="2"
        d="M22.648 39.041l5.805-8.05a1.447 1.447 0 012.04-.313l2.744 2.05M31.469 37.313l3.008-4.465a1.447 1.447 0 012.112-.314l2.527 2.053"
      />
      <circle cx="45" cy="27" r="3" stroke={disabled ? gray : fill2} strokeWidth="2" />
      <path
        stroke={disabled ? gray : fill2}
        strokeLinecap="round"
        strokeWidth="2"
        d="M20.883 43.953h21.176"
      />
    </svg>
  );
}

CreateIcon.propTypes = propTypes;
CreateIcon.defaultProps = defaultProps;

export default CreateIcon;
