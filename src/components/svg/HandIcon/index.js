import React from 'react';
import PropTypes from 'prop-types';
import Colors from 'src/styles/colors.json';

const blackColor = Colors['$black-color'];
const sidecarYellowColor = Colors['$sidecar-yellow-color'];

const propTypes = {
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
  stroke: PropTypes.string,
  color: PropTypes.string,
};

const defaultProps = {
  className: '',
  width: '58',
  height: '58',
  stroke: sidecarYellowColor,
  color: blackColor,
};

function HandIcon({
  width,
  height,
  className,
  stroke,
  color,
}) {
  return (
    <svg
      className={className}
      width={width}
      height={height}
      fill="none"
      viewBox="0 0 80 80"
    >
      <rect
        width="80"
        height="80"
        fill={stroke}
        rx="24"
        transform="matrix(-1 0 0 1 80 0)"
      />
      <path
        stroke={color}
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth="2.2"
        d="M55.039 37.071c1.38.964 3.702 2.128 5.442 2.95 1.38.651 2.287 2.03 2.287 3.556v2.328l-.005 1.103c-.007 1.453-1.099 2.662-2.53 2.908-.606.104-1.207.224-1.652.349l-.364.132c-5.081 1.258-15.955 4.022-21.201 5.363h-.083c-.22.016-.442.016-.662 0a3.462 3.462 0 01-1.109-.2l-.199-.082-15.507-11.287-.695-.497a3.428 3.428 0 01-.695-.513A2.83 2.83 0 0120.3 38.35h.397l.348.082c.143.041.282.097.413.166l.265.149 1.837 1.34 9.93 6.422c.325.043.653.043.977 0 .53-.133 3.509-.927 6.273-1.655h.148c.696-.215 1.523-.43 2.35-.63l2.384-.628a2.648 2.648 0 001.376-2.796c-.096-.551-.22-.755-.623-1.145a2.648 2.648 0 00-1.47-.721l-6.416.655a1.787 1.787 0 01-1.152-.578c-.299-.329-.464-1.042-.464-1.486a1.787 1.787 0 011.616-1.779l6.62-.958h3.31c2.398.01 4.725.813 6.62 2.284zM33.835 22.913a3.308 3.308 0 00-1.225-.215 3.492 3.492 0 00-2.962 1.555 3.559 3.559 0 00-2.93-1.522 3.643 3.643 0 00-1.224.231 3.526 3.526 0 00-1.97 4.535c.163.398.363.78.596 1.142v0l.232.364a17.575 17.575 0 003.111 3.31l2.317 2.003 2.284-2.036a17.05 17.05 0 003.211-3.608c.078-.112.15-.228.215-.347.175-.288.32-.593.43-.91a3.493 3.493 0 00-2.085-4.502v0z"
      />
    </svg>
  );
}

HandIcon.propTypes = propTypes;
HandIcon.defaultProps = defaultProps;

export default HandIcon;
