import React from 'react';
import PropTypes from 'prop-types';
import Colors from 'src/styles/colors.json';

const whiteColor = Colors['$white-color'];

const propTypes = {
  fill: PropTypes.string,
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
};

const defaultProps = {
  fill: whiteColor,
  className: '',
  width: '46',
  height: '46',
};

function YouTubeIcon({
  width,
  height,
  fill,
  className,
}) {
  return (
    <svg
      className={className}
      height={height}
      width={width}
      fill="none"
      viewBox="0 0 46 46"
    >
      <title>******* youtube page</title>
      <circle cx="23" cy="23" r="22.5" stroke={fill} />
      <path
        fill={fill}
        d="M31.624 18.187a2.579 2.579 0 00-.582-1.12 2.216 2.216 0 00-1.009-.648C28.63 16 23 16 23 16s-5.629 0-7.033.417c-.382.114-.73.337-1.01.647-.278.31-.48.697-.581 1.12C14 19.746 14 23 14 23s0 3.255.376 4.813c.207.86.817 1.538 1.59 1.768C17.372 30 23 30 23 30s5.629 0 7.033-.42c.776-.229 1.384-.906 1.591-1.767C32 26.255 32 23 32 23s0-3.255-.376-4.813zm-10.412 7.8v-5.974l4.66 2.965-4.66 3.01z"
      />
    </svg>
  );
}

YouTubeIcon.propTypes = propTypes;
YouTubeIcon.defaultProps = defaultProps;

export default YouTubeIcon;
