import React from 'react';
import PropTypes from 'prop-types';
import colors from 'src/styles/colors.json';


const summerGreenColor = colors['$summer-green-color'];
const blueColor = colors['$pickled-bluewood-color'];
const gray = colors['$silver-gray-color'];

const propTypes = {
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
  fill: PropTypes.string,
  stroke: PropTypes.string,
  strokeTwo: PropTypes.string,
  strokeWidth: PropTypes.number,
  strokeLinecap: PropTypes.string,
  disabled: PropTypes.bool,

};

const defaultProps = {
  className: '',
  width: '46',
  height: '46',
  fill: 'none',
  stroke: summerGreenColor,
  strokeTwo: blueColor,
  strokeWidth: 2,
  strokeLinecap: 'round',
  disabled: false,
};

function CreateArtIcon({
  className,
  width,
  height,
  fill,
  stroke,
  strokeTwo,
  strokeWidth,
  strokeLinecap,
  disabled,
}) {
  return (
    <svg
      className={className}
      width={width}
      height={height}
      fill="none"
      viewBox="0 0 60 52"
    >
      <path
        stroke={disabled ? gray : stroke}
        d="M23,6l5-4.3c1.1-1,2.8-1,3.9,0L37,6"
        strokeWidth={strokeWidth}
        fill={fill}
      />
      <path
        stroke={disabled ? gray : stroke}
        d="M8,6h44c3.9,0,7,3.1,7,7v31c0,3.9-3.1,7-7,7H8c-3.9,0-7-3.1-7-7V13C1,9.1,4.1,6,8,6z"
        strokeWidth={strokeWidth}
        fill={fill}
      />
      <path
        stroke={disabled ? gray : strokeTwo}
        d="M8,10h44c1.7,0,3,1.3,3,3v31c0,1.7-1.3,3-3,3H8c-1.7,0-3-1.3-3-3V13C5,11.3,6.3,10,8,10z"
        strokeWidth={strokeWidth}
        fill={fill}
      />
      <path
        stroke={disabled ? gray : stroke}
        d="M16,34l6.1-7.9c0.5-0.6,1.3-0.7,2-0.3l2.9,2.1"
        strokeWidth={strokeWidth}
        fill={fill}
      />
      <path
        stroke={disabled ? gray : stroke}
        d="M26,31l3.2-4.7c0.5-0.7,1.4-0.8,2.1-0.3l2.7,2.2"
        strokeWidth={strokeWidth}
        fill={fill}
      />
      <circle
        stroke={disabled ? gray : stroke}
        cx="42"
        cy="21"
        r="3"
        strokeWidth={strokeWidth}
        fill={fill}
      />
      <path
        stroke={disabled ? gray : stroke}
        d="M16,39.5h18"
        strokeWidth={strokeWidth}
        strokeLinecap={strokeLinecap}
        fill={fill}
      />
    </svg>
  );
}

CreateArtIcon.propTypes = propTypes;
CreateArtIcon.defaultProps = defaultProps;

export default CreateArtIcon;
