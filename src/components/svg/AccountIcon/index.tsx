import React, { FunctionComponent } from 'react';
import Colors from 'src/styles/colors.json';
import { Props } from './interface';

const summerGreenColor = Colors['$summer-green-color'];
const slateGrayColor = Colors['$slate-gray-color-2'];

const defaultProps = {
  className: '',
  width: '64',
  height: '64',
  strokeCircle: summerGreenColor,
  strokePath: slateGrayColor,
  strokeRect: slateGrayColor,
};

const AccountIcon:FunctionComponent<Props> = ({
  className,
  width,
  height,
  strokeCircle,
  strokePath,
  strokeRect,
}) => (
  <svg
    className={className}
    width={width}
    height={height}
    fill="none"
    viewBox="0 0 64 64"
  >
    <title>account button</title>
    <g strokeWidth="2" opacity="0.75">
      <circle cx="32" cy="24" r="6" stroke={strokeCircle} />
      <path
        stroke={strokePath}
        d="M20 41.428c0-4.384 4.33-7.499 8.692-6.338.69.183 1.333.34 1.882.452.53.107 1.036.186 1.426.186.39 0 .896-.079 1.426-.186a33.615 33.615 0 001.882-.452C39.67 33.93 44 37.044 44 41.428A4.572 4.572 0 0139.428 46H24.572A4.572 4.572 0 0120 41.428z"
      />
      <rect
        width="52"
        height="52"
        x="6"
        y="6"
        stroke={strokeRect}
        rx="13"
      />
    </g>
  </svg>
);

AccountIcon.defaultProps = defaultProps;

export default AccountIcon;
