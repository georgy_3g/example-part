import React from 'react';
import PropTypes from 'prop-types';
import Colors from 'src/styles/colors.json';

const grayLightColor = Colors['$gray-light-color'];
const lightSlateGrayColor = Colors['$light-slate-gray-color'];

const propTypes = {
  fill: PropTypes.string,
  crossFill: PropTypes.string,
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
};

const defaultProps = {
  fill: grayLightColor,
  crossFill: lightSlateGrayColor,
  className: '',
  width: '32',
  height: '32',
};

function CloseIcon({
  width,
  height,
  fill,
  crossFill,
  className,
}) {
  return (
    <svg
      className={className}
      height={height}
      width={width}
      fill={fill}
      viewBox="0 0 32 32"
    >
      <circle cx="16" cy="16" r="16" fill={fill} />
      <path
        fill={crossFill}
        fillRule="evenodd"
        d="M20.951 22.093a1 1 0 001.414-1.415l-4.95-4.95 4.95-4.95a1 1 0 00-1.414-1.413l-4.95 4.95-4.95-4.95a1 1 0 10-1.414 1.414l4.95 4.95-4.95 4.95a1 1 0 101.415 1.414l4.95-4.95 4.95 4.95z"
        clipRule="evenodd"
      />
    </svg>
  );
}

CloseIcon.propTypes = propTypes;
CloseIcon.defaultProps = defaultProps;

export default CloseIcon;
