import React from 'react';
import PropTypes from 'prop-types';
import Colors from 'src/styles/colors.json';

const darkBlueColor = Colors['$dark-slate-blue-color'];

const propTypes = {
  width: PropTypes.string,
  height: PropTypes.string,
  className: PropTypes.string,
  stroke: PropTypes.string,
};

const defaultProps = {
  width: '23',
  height: '23',
  className: '',
  stroke: darkBlueColor,
};

function ZoomIcon({
  width,
  height,
  className,
  stroke,
}) {
  return (
    <svg className={className} width={width} height={height} stroke={stroke} viewBox="0 0 23 23">
      <g fill="none" fillRule="evenodd" strokeLinecap="round" strokeLinejoin="round">
        <g
          transform="translate(0 1)"
          strokeWidth="2"
        >
          <g transform="translate(1)">
            <path
              d="M14.7779066,2.67811967 C18.1193164,6.01952951 18.1193164,11.4364967 14.7779066,14.7779066 C11.4364967,18.1193164 6.01952951,18.1193164 2.67811967,14.7779066 C-0.663290164,11.4364967 -0.663290164,6.01952951 2.67811967,2.67811967 C6.01952951,-0.663290164 11.4364967,-0.663290164 14.7779066,2.67811967 Z"
            />
            <path
              d="M14.7779066,14.7779066 L20.8279721,20.8279721"
            />
            <path
              d="M8.72808197,6.24732787 L8.72808197,11.4112623"
            />
            <path
              d="M11.3100492,8.82929508 L6.14611475,8.82929508"
            />
          </g>
        </g>
      </g>
    </svg>
  );
}

ZoomIcon.propTypes = propTypes;
ZoomIcon.defaultProps = defaultProps;

export default ZoomIcon;
