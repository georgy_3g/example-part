import React from 'react';
import PropTypes from 'prop-types';
import Colors from 'src/styles/colors.json';

const strokeColor = Colors['$slate-gray-color'];
const fillColor = Colors['$athens-gray-color'];

const propTypes = {
  fill: PropTypes.string,
  stroke: PropTypes.string,
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
};

const defaultProps = {
  fill: fillColor,
  stroke: strokeColor,
  className: '',
  width: '32',
  height: '32',
};

function RectangleIcon({
  width,
  height,
  fill,
  stroke,
  className,
}) {
  return (
    <svg
      className={className}
      width={width}
      height={height}
      fill="none"
      viewBox="0 0 32 32"
    >
      <circle cx="16" cy="16" r="16" fill={fill} />
      <path
        fill={stroke}
        fillRule="evenodd"
        d="M20.953 22.09a1 1 0 001.414-1.415l-4.95-4.95 4.948-4.949a1 1 0 10-1.414-1.414l-4.949 4.948-4.949-4.949a1 1 0 00-1.414 1.414l4.949 4.95-4.951 4.95a1 1 0 001.414 1.415l4.951-4.951 4.95 4.95z"
        clipRule="evenodd"
      />
    </svg>
  );
}

RectangleIcon.propTypes = propTypes;
RectangleIcon.defaultProps = defaultProps;

export default RectangleIcon;
