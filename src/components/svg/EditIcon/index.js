import React from 'react';
import PropTypes from 'prop-types';
import Colors from 'src/styles/colors.json';

const grayColor = Colors['$darker-cadet-blue-color'];

const propTypes = {
  stroke: PropTypes.string,
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
};

const defaultProps = {
  stroke: grayColor,
  className: '',
  width: '18',
  height: '18',
};

function EditIcon({
  width,
  height,
  stroke,
  className,
}) {
  return (
    <svg
      width={width}
      height={height}
      fill="none"
      viewBox="0 0 18 18"
      className={className}
    >
      <path
        stroke={stroke}
        strokeLinecap="round"
        strokeWidth="1.636"
        d="M8.138 2.455H4.09A3.273 3.273 0 00.818 5.728v8.182a3.273 3.273 0 003.273 3.272h8.093a3.273 3.273 0 003.273-3.272V9.819"
      />
      <path
        stroke={stroke}
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth="1.636"
        d="M14.236 1.327a1.72 1.72 0 012.44 0 1.74 1.74 0 010 2.454l-7.565 7.612a.818.818 0 01-.381.216l-1.71.43a.818.818 0 01-.994-.99l.43-1.73a.818.818 0 01.214-.38l7.566-7.612z"
      />
    </svg>
  );
}

EditIcon.propTypes = propTypes;
EditIcon.defaultProps = defaultProps;

export default EditIcon;
