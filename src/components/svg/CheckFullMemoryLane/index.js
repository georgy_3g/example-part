import React from 'react';
import PropTypes from 'prop-types';
import colors from 'src/styles/colors.json';

const greenColor = colors['$summer-green-color'];
const blueColor = colors['$pickled-bluewood-color'];

const propTypes = {
  fill: PropTypes.string,
  className: PropTypes.string,
  width: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  height: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  stroke: PropTypes.string,
  strokeWidth: PropTypes.string,
};

const defaultProps = {
  fill: blueColor,
  className: '',
  width: '46',
  height: '46',
  stroke: greenColor,
  strokeWidth: '2',
};

function CheckFullMemoryLane({
  width,
  height,
  fill,
  className,
  stroke,
  strokeWidth,
}) {
  return (
    <svg
      className={className}
      width={width}
      height={height}
      viewBox="0 0 46 46"
      fill="none"
    >
      <circle cx={23} cy={23} r={22} stroke={stroke} strokeWidth={strokeWidth} />
      <path
        d="M15 24.978l4.735 3.538a1 1 0 001.255-.047L33 18"
        stroke={fill}
        strokeWidth={2}
        strokeLinecap="round"
      />
    </svg>
  );
}

CheckFullMemoryLane.propTypes = propTypes;
CheckFullMemoryLane.defaultProps = defaultProps;

export default CheckFullMemoryLane;
