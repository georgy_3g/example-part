import React from 'react';
import PropTypes from 'prop-types';
import Colors from 'src/styles/colors.json';

const blue = Colors['$white-color'];

const propTypes = {
  fill: PropTypes.string,
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
};

const defaultProps = {
  className: '',
  width: '24',
  height: '24',
  fill: blue,
};

function PauseIcon(props) {
  const { fill, width, height, className } = props;
  return (
    <svg width={width} height={height} viewBox="0 0 24 24" className={className}>
      <path d="M6 19h4V5H6v14zm8-14v14h4V5h-4z" fill={fill} />
      <path fill="none" d="M0 0h24v24H0z" />
    </svg>
  );
}

PauseIcon.propTypes = propTypes;
PauseIcon.defaultProps = defaultProps;

export default PauseIcon;
