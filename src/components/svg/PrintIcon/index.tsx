import React, { FunctionComponent } from 'react';
import Colors from 'src/styles/colors.json';
import { propsInterface } from './interface';

const white = Colors['$white-color'];
const sharkGrayColor = Colors['$shark-gray-color'];
const summerGreenColor = Colors['$summer-green-color'];
const gray = Colors['$silver-gray-color'];

const defaultProps = {
  className: '',
  width: '18',
  height: '18',
  fill: white,
  strokeOne: sharkGrayColor,
  strokeTwo: summerGreenColor,
  disabled: false,
};

const PrintIcon: FunctionComponent<propsInterface> = ({
  className,
  height,
  width,
  fill,
  strokeOne,
  strokeTwo,
  disabled,
}) => (
  <svg width={width} height={height} fill="none" viewBox="0 0 18 18" className={className}>
    <path
      stroke={disabled ? gray : strokeOne}
      d="M3.875 3.375c0-.656.531-1.188 1.187-1.188h7.875c.656 0 1.188.532 1.188 1.188v1.75H3.874v-1.75z"
    />
    <rect width="15.875" height="8.563" x="1.062" y="5.281" stroke={disabled ? gray : strokeOne} rx="1.75" />
    <rect
      width="10.125"
      height="5.063"
      x="3.937"
      y="10.688"
      fill={fill}
      stroke={disabled ? gray : strokeOne}
      strokeLinejoin="round"
      rx="1.125"
    />
    <path stroke={disabled ? gray : strokeTwo} strokeLinecap="round" d="M6.187 12.375h5.625M6.75 14.063h4.5" />
  </svg>
);

PrintIcon.defaultProps = defaultProps;

export default PrintIcon;
