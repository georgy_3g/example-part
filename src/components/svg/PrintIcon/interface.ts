export interface propsInterface {
  className?: string;
  height?: string;
  width?: string;
  fill?: string;
  strokeOne?: string;
  strokeTwo?: string;
  disabled?: boolean;
}
