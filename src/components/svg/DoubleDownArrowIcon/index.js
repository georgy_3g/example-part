import React from 'react';
import PropTypes from 'prop-types';
import Colors from 'src/styles/colors.json';

const mainColor = Colors['$athens-gray-color'];

const propTypes = {
  color: PropTypes.string,
  strokeWidth: PropTypes.string,
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
};

const defaultProps = {
  className: '',
  width: '18',
  height: '18',
  strokeWidth: '1.5',
  color: mainColor,
};

function DoubleDownArrowIcon({
  width,
  height,
  strokeWidth,
  color,
  className,
}) {
  return (
    <svg
      className={className}
      width={width}
      height={height}
      fill="none"
      viewBox="0 0 18 18"
    >
      <path
        stroke={color}
        strokeLinecap="round"
        strokeWidth={strokeWidth}
        d="M17 9l-6.515 6.515a2.1 2.1 0 01-2.97 0L1 9M15 3l-4.515 4.515a2.1 2.1 0 01-2.97 0L3 3"
      />
    </svg>
  );
}

DoubleDownArrowIcon.propTypes = propTypes;
DoubleDownArrowIcon.defaultProps = defaultProps;

export default DoubleDownArrowIcon;
