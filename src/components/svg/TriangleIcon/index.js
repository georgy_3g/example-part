import React from 'react';
import PropTypes from 'prop-types';
import Colors from 'src/styles/colors.json';

const color = Colors['$mako-gray-color'];

const propTypes = {
  fill: PropTypes.string,
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
};

const defaultProps = {
  fill: color,
  className: '',
  width: '50',
  height: '25',
};

function TriangleIcon({
  className,
  fill,
  width,
  height,
}) {
  return (
    <svg
      version="1.1"
      width={width}
      height={height}
      viewBox="0 0 100 100"
      preserveAspectRatio="none"
      className={className}
    >
      <polygon fill={fill} points="0,100 50,0 100,100" />
    </svg>
  );
}

TriangleIcon.propTypes = propTypes;
TriangleIcon.defaultProps = defaultProps;

export default TriangleIcon;
