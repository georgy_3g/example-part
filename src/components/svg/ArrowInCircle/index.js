import React from 'react';
import PropTypes from 'prop-types';
import Colors from 'src/styles/colors.json';

const gray = Colors['$bombay-gray-color'];

const propTypes = {
  stroke: PropTypes.string,
  strokeWidth: PropTypes.string,
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
};

const defaultProps = {
  stroke: gray,
  strokeWidth: '2',
  className: '',
  width: '36',
  height: '36',
};

function ArrowInCircle({
  stroke,
  strokeWidth,
  className,
  width,
  height,
}) {
  return (
    <svg
      className={className}
      width={width}
      height={height}
      fill="none"
      viewBox="0 0 36 36"
    >
      <circle
        cx="18"
        cy="18"
        r="17"
        stroke={stroke}
        strokeWidth={strokeWidth}
        transform="rotate(-90 18 18)"
      />
      <path
        stroke={stroke}
        strokeLinecap="round"
        strokeWidth={strokeWidth}
        d="M17 23l3.49-4.224a1 1 0 00.02-1.25L17 13"
      />
    </svg>
  );
}

ArrowInCircle.propTypes = propTypes;
ArrowInCircle.defaultProps = defaultProps;

export default ArrowInCircle;
