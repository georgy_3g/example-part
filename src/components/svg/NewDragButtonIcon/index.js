import React from 'react';
import PropTypes from 'prop-types';
import colors from 'src/styles/colors.json';

const darkGrayColor = colors['$pickled-bluewood-color'];
const grayColor = colors['$darker-cadet-blue-color'];
const whiteColor = colors['$white-color'];

const defaultProps = {
  className: '',
  width: '50',
  height: '50',
  color1: darkGrayColor,
  color2: grayColor,
};

const propTypes = {
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
  color1: PropTypes.string,
  color2: PropTypes.string,
};

function DragButton(props) {
  const { className, width, height, color1, color2 } = props;

  return (
    <svg
      className={className}
      x="0"
      y="0"
      enableBackground="new 0 0 28.8 28.8"
      version="1.1"
      viewBox="0 0 28.8 28.8"
      xmlSpace="preserve"
      width={width}
      height={height}
    >
      <path
        d="M8.6 0h11.5c4.8 0 8.6 3.9 8.6 8.6v11.5c0 4.8-3.9 8.6-8.6 8.6H8.6c-4.8 0-8.6-3.9-8.6-8.6V8.6C0 3.9 3.9 0 8.6 0z"
        fill={whiteColor}
      />
      <path
        d="M8.2 18.1l-2.9-2.9c-.5-.5-.5-1.3 0-1.8l2.9-2.9v7.6zM20.6 18.1l2.9-2.9c.5-.5.5-1.3 0-1.8l-2.9-2.9v7.6z"
        fill={color1}
        stroke={color1}
        strokeWidth={2}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M14.4 21.5V7.2"
        opacity="0.4"
        fill="none"
        stroke={color2}
        strokeWidth={2}
        strokeLinecap="round"
        strokeLinejoin="round"
        enableBackground="new"
      />
    </svg>
  );
}

DragButton.propTypes = propTypes;
DragButton.defaultProps = defaultProps;

export default DragButton;
