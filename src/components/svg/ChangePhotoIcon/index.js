import React from 'react';
import PropTypes from 'prop-types';
import Colors from 'src/styles/colors.json';

const darkerBlue = Colors['$darker-cadet-blue-color'];
const gray = Colors['$gull-gray-color'];
const arrowColor = Colors['$pickled-bluewood-color'];

const propTypes = {
  stroke: PropTypes.string,
  stroke2: PropTypes.string,
  arrowStroke: PropTypes.string,
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
};

const defaultProps = {
  stroke: darkerBlue,
  stroke2: gray,
  arrowStroke: arrowColor,
  className: '',
  width: '94',
  height: '71',
};

function ChangePhotoIcon({
  width,
  height,
  stroke,
  stroke2,
  arrowStroke,
  className,
}) {
  return (
    <svg
      className={className}
      width={width}
      height={height}
      fill="none"
      viewBox="0 0 94 71"
    >
      <rect
        width="41.385"
        height="35.526"
        x="1"
        y="33.477"
        stroke={stroke}
        strokeWidth="2"
        rx="7"
      />
      <path
        stroke={stroke2}
        strokeLinecap="round"
        strokeWidth="2"
        d="M8.295 55.616l6.369-8.816a1.447 1.447 0 012.038-.312l3.077 2.294M17.863 53.743l3.338-4.945a1.447 1.447 0 012.11-.314l2.846 2.308"
      />
      <path
        stroke={stroke2}
        strokeWidth="2"
        d="M35.053 43.293c0 1.61-1.326 2.938-2.99 2.938-1.665 0-2.99-1.328-2.99-2.938s1.325-2.938 2.99-2.938c1.664 0 2.99 1.328 2.99 2.938z"
      />
      <path
        stroke={stroke2}
        strokeLinecap="round"
        strokeWidth="2"
        d="M6.38 60.934h22.97"
      />
      <rect
        width="41.385"
        height="35.526"
        x="51.615"
        y="1"
        stroke={stroke}
        strokeWidth="2"
        rx="7"
      />
      <path
        stroke={stroke2}
        strokeLinecap="round"
        strokeWidth="2"
        d="M58.91 23.14l6.37-8.816a1.447 1.447 0 012.037-.313l3.077 2.295M68.478 21.266l3.338-4.945a1.447 1.447 0 012.111-.314l2.846 2.308"
      />
      <path
        stroke={stroke2}
        strokeWidth="2"
        d="M85.669 10.816c0 1.61-1.327 2.938-2.99 2.938-1.665 0-2.992-1.328-2.992-2.938s1.327-2.937 2.991-2.937c1.664 0 2.99 1.328 2.99 2.937z"
      />
      <path
        stroke={stroke2}
        strokeLinecap="round"
        strokeWidth="2"
        d="M56.996 28.457h22.968"
      />
      <path
        stroke={arrowStroke}
        strokeLinecap="square"
        strokeLinejoin="round"
        strokeWidth="2"
        d="M68.693 44.744v0c0 5.895-4.754 10.688-10.65 10.734l-2.729.022"
      />
      <path
        stroke={arrowStroke}
        strokeLinecap="round"
        strokeWidth="2"
        d="M55.963 57.73l-1.257-1.698a1 1 0 01.046-1.247l1.21-1.407"
      />
      <path
        stroke={arrowStroke}
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth="2"
        d="M25.309 25.256v0c0-5.896 4.754-10.688 10.65-10.734l2.729-.022"
      />
      <path
        stroke={arrowStroke}
        strokeLinecap="round"
        strokeWidth="2"
        d="M38.04 12.27l1.256 1.698a1 1 0 01-.046 1.247l-1.21 1.407"
      />
    </svg>
  );
}

ChangePhotoIcon.propTypes = propTypes;
ChangePhotoIcon.defaultProps = defaultProps;

export default ChangePhotoIcon;
