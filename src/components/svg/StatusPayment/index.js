import React from 'react';
import PropTypes from 'prop-types';
import Colors from 'src/styles/colors.json';

const success = Colors['$summer-green-dark-color'];
const error = Colors['$sea-pink-red-color'];
const black = Colors['$black-color'];

const defaultProps = {
  payment: true,
  className: '',
  width: '76',
  height: '76',
};
const propType = {
  payment: PropTypes.bool,
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
};

function StatusPayment({ payment, height, width, className }) {
  const fill = payment ? success : error;
  return (
    <svg width={width} height={height} fill="none" viewBox="0 0 76 76" className={className}>
      <circle cx="41" cy="41" r="35" fill={fill} />
      <circle cx="35" cy="35" r="34" stroke={black} strokeWidth="2" />
      {payment ? (
        <path
          fill={black}
          d="M22.664 36.253a1 1 0 00-1.328 1.494l1.328-1.494zm27.08-10.584a1 1 0 10-1.487-1.338l1.486 1.338zm-16.75 17.115l-.743-.67.744.67zm-11.658-5.037l6.771 6.02 1.329-1.495-6.772-6.02-1.328 1.495zm12.402 5.706l16.005-17.784-1.486-1.338L32.25 42.115l1.487 1.338zm-5.63.314a4 4 0 005.63-.314l-1.487-1.338a2 2 0 01-2.815.157l-1.329 1.495z"
        />
      ) : (
        <path stroke={black} strokeLinecap="round" strokeWidth="2" d="M35 41V18M35 52v-3" />
      )}
    </svg>
  );
}

StatusPayment.propTypes = propType;
StatusPayment.defaultProps = defaultProps;

export default StatusPayment;
