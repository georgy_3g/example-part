import React from 'react';
import PropTypes from 'prop-types';
import Colors from 'src/styles/colors.json';

const mainColor = Colors['$dark-slate-blue-color'];

const propTypes = {
  fill: PropTypes.string,
  stroke: PropTypes.string,
  fillBg: PropTypes.string,
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
};

const defaultProps = {
  fill: mainColor,
  fillBg: '',
  stroke: mainColor,
  className: '',
  width: '22',
  height: '22',
};

function HintIcon({
  width,
  height,
  stroke,
  fill,
  fillBg,
  className,
}) {
  return (
    <svg
      width={width}
      height={height}
      viewBox="0 0 22 22"
      className={`svg ${className}`}
    >
      <g
        fill="none"
        fillRule="evenodd"
      >
        <circle cx="11" cy="11" r="11" fill={fillBg} />
        <path
          fill={fill}
          stroke={stroke}
          d="M11.826 6.526a.826.826 0 1 1-1.653 0 .826.826 0 0 1 1.653 0M11 10.553c.347 0 .628.28.628.628v4.491a.628.628 0 1 1-1.256 0v-4.491c0-.347.28-.628.628-.628"
        />
      </g>
    </svg>
  );
}

HintIcon.propTypes = propTypes;
HintIcon.defaultProps = defaultProps;

export default HintIcon;
