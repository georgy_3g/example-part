import React from 'react';
import PropTypes from 'prop-types';
import Colors from 'src/styles/colors.json';

const grey = Colors['$porcelain-gray-color'];
const white = Colors['$paris-white-color'];
const green = Colors['$summer-green-color'];

const propTypes = {
  outlineFirst: PropTypes.string,
  outlineSecond: PropTypes.string,
  outlineThird: PropTypes.string,
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
};

const defaultProps = {
  outlineFirst: grey,
  outlineSecond: white,
  outlineThird: green,
  className: '',
  width: '22',
  height: '22',
};

function TrackIcon({
  className,
  width,
  height,
  outlineFirst,
  outlineSecond,
  outlineThird,
}) {
  return (
    <svg
      className={className}
      width={width}
      height={height}
      fill="none"
      viewBox="0 0 22 22"
    >
      <circle cx="11" cy="11" r="10" stroke={outlineFirst} strokeWidth="2" />
      <circle cx="11" cy="11" r="6" stroke={outlineSecond} strokeWidth="2" />
      <circle cx="11" cy="11" r="3" fill={outlineThird} />
    </svg>
  );
}

TrackIcon.propTypes = propTypes;
TrackIcon.defaultProps = defaultProps;

export default TrackIcon;
