import React from 'react';
import PropTypes from 'prop-types';
import Colors from 'src/styles/colors.json';

const sharkGrayColor = Colors['$hit-gray-color'];

const propTypes = {
  stroke: PropTypes.string,
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
};

const defaultProps = {
  stroke: sharkGrayColor,
  className: '',
  width: '144',
  height: '144',
};

const HeartIcon = ({
  width,
  height,
  stroke,
  className,
}) => {
  return (
    <svg className={className} width={width} height={height} viewBox="0 0 144 144">
      <g
        fill="none"
        stroke={stroke}
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth="14"
      >
        <path d="M113.7 15.6c-4.6-1.7-9.7-2.6-14.6-2-4.9.3-9.7 1.7-14.3 4.3-5.1 2.9-9.4 6.6-12.8 11.4-3.4-4.9-7.7-8.6-12.8-11.4-5.1-2.6-10.8-4-16.6-4-4.3 0-8.3.6-12.3 2-4.3 1.7-8.6 4-12 7.4S12 30.4 10 34.7s-3.1 8.8-3.1 13.7.6 9.4 2.3 14c1.7 4 3.7 8 6 11.7l2.3 3.7c8.9 12.6 19.4 24 31.1 34l17.7 15.4c3.1 2.9 8 2.9 11.4 0l17.7-15.4c12.6-10.6 23.4-22.8 32.5-36.5.9-1.1 1.4-2.6 2.3-3.7 1.7-2.9 3.1-6 4.6-9.1 1.7-4.3 2.3-9.1 2.3-13.7-.3-4.9-1.1-9.4-3.1-13.7s-4.9-8-8.3-11.4c-3.7-4.1-7.7-6.4-12-8.1z" />
        <path d="M50 38.7c-.9-.3-2-.3-2.9 0-1.4.3-2.6.9-3.7 1.7-1.1.9-1.7 2-2.3 3.1-.9-.9-2-1.7-3.4-2-1.1-.3-2.6-.3-4 0-.9.3-2 .6-2.6 1.1-.9.6-1.7 1.4-2.3 2.3-.6.9-1.1 2-1.4 2.9-.3 1.1-.3 2.3 0 3.1.3 1.1.6 2 1.1 3.1.6.9 1.4 1.7 2 2.6l.6.6c2.6 2.6 5.4 4.6 8.6 6.3L46 67l4.3-5.7c2.3-2.9 4.3-6.3 5.7-9.7l.3-.9c.3-.9.6-1.4.6-2.3.3-1.1.3-2.3 0-3.1-.3-1.1-.6-2-1.4-2.9-.6-.9-1.4-1.7-2.3-2.3-.9-.8-2-1.1-3.2-1.4z" />
      </g>
    </svg>
  );
}

HeartIcon.propTypes = propTypes;
HeartIcon.defaultProps = defaultProps;

export default HeartIcon;
