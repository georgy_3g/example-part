import React from 'react';
import PropTypes from 'prop-types';
import colors from 'src/styles/colors.json';


const whiteColor = colors['$white-color'];
const summerGreenColor = colors['$summer-green-color'];
const blueColor = colors['$pickled-bluewood-color'];
const gray = colors['$silver-gray-color'];

const propTypes = {
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
  fillOne: PropTypes.string,
  fillTwo: PropTypes.string,
  fillFree: PropTypes.string,
  stroke: PropTypes.string,
  strokeTwo: PropTypes.string,
  strokeWidth: PropTypes.number,
  strokeLinecap: PropTypes.string,
  disabled: PropTypes.bool,
};

const defaultProps = {
  className: '',
  width: '46',
  height: '46',
  fillOne: 'none',
  fillTwo: 'none',
  fillFree: whiteColor,
  stroke: summerGreenColor,
  strokeTwo: blueColor,
  strokeWidth: 2,
  strokeLinecap: 'round',
  disabled: false,
};

function OrderPrintsIcon({
  className,
  width,
  height,
  fillOne,
  fillTwo,
  fillFree,
  stroke,
  strokeTwo,
  strokeWidth,
  strokeLinecap,
  disabled,
}) {
  return (
    <svg
      className={className}
      width={width}
      height={height}
      fill="none"
      viewBox="0 0 60 52"
    >
      <path
        fill={fillOne}
        d="M8,9.1l35.2-7.5C47,0.8,50.7,3.3,51.5,7l6,28c0.8,3.8-1.6,7.5-5.4,8.3l-35.2,7.5c-3.8,0.8-7.5-1.6-8.3-5.4l-6-28C1.8,13.6,4.3,9.9,8,9.1z"
        stroke={disabled ? gray : stroke}
        strokeWidth={strokeWidth}
      />
      <path
        strokeWidth={strokeWidth}
        strokeLinecap={strokeLinecap}
        stroke={disabled ? gray : stroke}
        fill={fillTwo}
        d="M15.8,35.4l5.5-12.5c0.3-0.7,1.2-1.1,1.9-0.7l4.5,2.1"
      />
      <path
        strokeWidth={strokeWidth}
        stroke={disabled ? gray : stroke}
        fill={fillTwo}
        strokeLinecap={strokeLinecap}
        d="M26.7,30.7l2.9-7.1c0.3-0.8,1.2-1.1,2-0.7l4.3,2.2"
      />
      <path
        strokeWidth={strokeWidth}
        stroke={disabled ? gray : stroke}
        fill={fillOne}
        d="M45.2,13.9c0.5,2.1-0.9,4.3-3.1,4.7c-2.2,0.5-4.3-0.9-4.8-3s0.9-4.3,3.1-4.7C42.6,10.4,44.7,11.8,45.2,13.9z"
      />
      <path
        strokeWidth={strokeWidth}
        stroke={disabled ? gray : stroke}
        fill={fillTwo}
        strokeLinecap={strokeLinecap}
        d="M14.8,42.2l27.4-5.8"
      />
      <path
        strokeWidth={strokeWidth}
        stroke={disabled ? gray : strokeTwo}
        fill={fillFree}
        d="M16.9,1.7l35.2,7.5c3.8,0.8,6.2,4.5,5.4,8.3l-5.9,27.9c-0.8,3.8-4.5,6.2-8.3,5.4L8,43.3c-3.8-0.8-6.2-4.5-5.4-8.3l6-27.9C9.4,3.3,13.1,0.9,16.9,1.7z"
      />
      <path
        strokeWidth={strokeWidth}
        stroke={disabled ? gray : strokeTwo}
        fill={fillTwo}
        strokeLinecap={strokeLinecap}
        d="M13.3,28.8l10.1-9.1c0.6-0.5,1.5-0.5,2.1,0.1l3.3,3.8"
      />
      <path
        strokeWidth={strokeWidth}
        stroke={disabled ? gray : strokeTwo}
        fill={fillTwo}
        strokeLinecap={strokeLinecap}
        d="M25.2,29l5.5-5.3c0.6-0.6,1.6-0.5,2.1,0.1l3,3.8"
      />
      <path
        strokeWidth={strokeWidth}
        stroke={disabled ? gray : stroke}
        fill={fillOne}
        d="M48.9,21.2c-0.5,2.1-2.6,3.5-4.8,3s-3.6-2.6-3.1-4.7s2.6-3.5,4.8-3C48,16.9,49.4,19,48.9,21.2z"
      />
      <path
        strokeWidth={strokeWidth}
        stroke={disabled ? gray : stroke}
        fill={fillTwo}
        strokeLinecap={strokeLinecap}
        d="M9.6,34.7L37,40.5"
      />
    </svg>
  );
}

OrderPrintsIcon.propTypes = propTypes;
OrderPrintsIcon.defaultProps = defaultProps;

export default OrderPrintsIcon;
