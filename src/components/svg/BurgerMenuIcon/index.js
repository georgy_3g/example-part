import React from 'react';
import PropTypes from 'prop-types';
import Colors from 'src/styles/colors.json';

const fillColor = Colors['$athens-light-gray-color'];
const strokeColor = Colors['$shark-gray-color'];

const propTypes = {
  fill: PropTypes.string,
  stroke: PropTypes.string,
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
};

const defaultProps = {
  fill: fillColor,
  stroke: strokeColor,
  className: '',
  width: '41',
  height: '41',
};

function BurgerMenuIcon({
  width,
  height,
  stroke,
  fill,
  className,
}) {
  return (
    <svg
      className={className}
      width={width}
      height={height}
      fill="none"
      viewBox="0 0 41 41"
    >
      <title>Menu button</title>
      <rect width="41" height="41" fill={fill} rx="12" />
      <path
        stroke={stroke}
        strokeWidth="2"
        d="M12 13a1 1 0 011-1h4a1 1 0 011 1v5h-5a1 1 0 01-1-1v-4zM23 13a1 1 0 011-1h4a1 1 0 011 1v4a1 1 0 01-1 1h-5v-5zM12 24a1 1 0 011-1h5v5a1 1 0 01-1 1h-4a1 1 0 01-1-1v-4zM23 23h5a1 1 0 011 1v4a1 1 0 01-1 1h-4a1 1 0 01-1-1v-5z"
      />
    </svg>
  );
}

BurgerMenuIcon.propTypes = propTypes;
BurgerMenuIcon.defaultProps = defaultProps;

export default BurgerMenuIcon;
