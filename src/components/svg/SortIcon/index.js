import React from 'react';
import PropTypes from 'prop-types';
import Colors from 'src/styles/colors.json';

const blue = Colors['$burnt-sienna-color'];

const propTypes = {
  fill: PropTypes.string,
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
};

const defaultProps = {
  fill: blue,
  className: '',
  width: '14',
  height: '14',
};


function SortIcon({
  width,
  height,
  fill,
  className,
}) {
  return (
    <svg
      width={width}
      height={height}
      viewBox="0 0 14 14"
      className={className}
    >
      <rect width="14" height="2" fill={fill} rx="1" />
      <rect width="11" height="2" y="4" fill={fill} rx="1" />
      <rect width="8" height="2" y="8" fill={fill} rx="1" />
      <rect width="5" height="2" y="12" fill={fill} rx="1" />
    </svg>
  );
}

SortIcon.propTypes = propTypes;
SortIcon.defaultProps = defaultProps;

export default SortIcon;
