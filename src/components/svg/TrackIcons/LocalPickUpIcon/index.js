import React from 'react';
import PropTypes from 'prop-types';
import Colors from 'src/styles/colors.json';

const blue = Colors['$pickled-bluewood-color'];

const propTypes = {
  fill: PropTypes.string,
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
  stroke: PropTypes.string,
};

const defaultProps = {
  fill: 'none',
  className: '',
  width: '24',
  height: '24',
  stroke: blue,
};

function LocalPickUpIcon({
  fill,
  className,
  width,
  height,
  stroke,
}) {
  return (
    <svg
      className={className}
      x="0"
      y="0"
      width={width}
      height={height}
      enableBackground="new 0 0 24 24"
      viewBox="0 0 24 24"
    >
      <g>
        <path
          d="M19.8 10.4c0 6.2-7.8 11.5-7.8 11.5s-7.8-5.3-7.8-11.5c0-2.1.8-4.1 2.3-5.6C8 3.3 9.9 2.5 12 2.5s4 .8 5.5 2.3c1.5 1.5 2.3 3.5 2.3 5.6z"
          fill={fill}
          stroke={stroke}
          strokeWidth={1.5}
          strokeLinecap="round"
          strokeLinejoin="round"
        />
        <path
          d="M12 12.2c1.1 0 1.9-.9 1.9-1.9s-.8-2-1.9-2-1.9.9-1.9 1.9.8 2 1.9 2z"
          fill={fill}
          stroke={stroke}
          strokeWidth={1.5}
          strokeLinecap="round"
          strokeLinejoin="round"
        />
        <path
          d="M12 14.5c2.4 0 4.3-1.9 4.3-4.3S14.4 6 12 6s-4.3 1.9-4.3 4.3 1.9 4.2 4.3 4.2z"
          fill={fill}
          stroke={stroke}
          strokeWidth={1.5}
          strokeLinecap="round"
          strokeLinejoin="round"
        />
      </g>
    </svg>
  );
}

LocalPickUpIcon.propTypes = propTypes;
LocalPickUpIcon.defaultProps = defaultProps;

export default LocalPickUpIcon;
