import React from 'react';
import PropTypes from 'prop-types';
import Colors from 'src/styles/colors.json';

const blue = Colors['$pickled-bluewood-color'];
const white = Colors['$white-color'];

const propTypes = {
  fill: PropTypes.string,
  fillMask: PropTypes.string,
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
  stroke: PropTypes.string,
};

const defaultProps = {
  fill: 'none',
  fillMask: white,
  className: '',
  width: '24',
  height: '24',
  stroke: blue,
};

function ChangeRequestIcon({
  fill,
  fillMask,
  className,
  width,
  height,
  stroke,
}) {
  return (
    <svg
      className={className}
      width={width}
      height={height}
      fill={fill}
      viewBox="0 0 24 24"
    >
      <mask id="path-1-inside-1_606:75945" fill={fillMask}>
        <path
          fillRule="evenodd"
          d="M12 22c5.523 0 10-4.477 10-10S17.523 2 12 2 2 6.477 2 12c0 1.423.297 2.777.833 4.003v5.164h5.164A9.967 9.967 0 0012 22z"
          clipRule="evenodd"
        />
      </mask>
      <path
        fill={stroke}
        d="M2.833 16.003h1.5v-.314l-.125-.287-1.375.6zm0 5.164h-1.5v1.5h1.5v-1.5zm5.164 0l.601-1.375-.287-.125h-.314v1.5zM20.5 12a8.5 8.5 0 01-8.5 8.5v3c6.351 0 11.5-5.149 11.5-11.5h-3zM12 3.5a8.5 8.5 0 018.5 8.5h3C23.5 5.649 18.351.5 12 .5v3zM3.5 12A8.5 8.5 0 0112 3.5v-3C5.649.5.5 5.649.5 12h3zm.708 3.402A8.467 8.467 0 013.5 12h-3c0 1.634.342 3.192.959 4.604l2.749-1.202zm.125 5.765v-5.164h-3v5.164h3zm3.664-1.5H2.833v3h5.164v-3zM12 20.5a8.466 8.466 0 01-3.402-.708l-1.202 2.749c1.412.617 2.97.959 4.604.959v-3z"
        mask="url(#path-1-inside-1_606:75945)"
      />
      <ellipse cx="7.417" cy="12" fill={stroke} rx="1.25" ry="1.25" />
      <circle cx="12" cy="12" r="1.25" fill={stroke} />
      <circle cx="16.583" cy="12" r="1.25" fill={stroke} />
    </svg>
  );
}

ChangeRequestIcon.propTypes = propTypes;
ChangeRequestIcon.defaultProps = defaultProps;

export default ChangeRequestIcon;
