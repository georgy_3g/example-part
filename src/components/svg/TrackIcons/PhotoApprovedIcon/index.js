import React from 'react';
import PropTypes from 'prop-types';
import Colors from 'src/styles/colors.json';

const blue = Colors['$pickled-bluewood-color'];
const white = Colors['$white-color'];

const propTypes = {
  fill: PropTypes.string,
  fillCircle: PropTypes.string,
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
  stroke: PropTypes.string,
};

const defaultProps = {
  fill: 'none',
  fillCircle: white,
  className: '',
  width: '24',
  height: '24',
  stroke: blue,
};

function PhotoApprovedIcon({
  fill,
  fillCircle,
  className,
  width,
  height,
  stroke,
}) {
  return (
    <svg
      className={className}
      width={width}
      height={height}
      fill={fill}
      viewBox="0 0 24 24"
    >
      <rect
        width="18.541"
        height="16.036"
        x="2.727"
        y="4.043"
        stroke={stroke}
        strokeWidth="1.5"
        rx="3.25"
      />
      <circle
        r="5.25"
        fill={fillCircle}
        stroke={stroke}
        strokeLinecap="round"
        strokeWidth="1.5"
        transform="matrix(-1 0 0 1 18 18)"
      />
      <path
        stroke={stroke}
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth="1.5"
        d="M16 18.5l1.302 1 2.698-3"
      />
      <path
        stroke={stroke}
        strokeLinecap="round"
        strokeWidth="1.5"
        d="M6.527 14.339l2.48-3.438a1.447 1.447 0 012.04-.313l.785.588M10.95 13.47l1.065-1.583a1.447 1.447 0 012.113-.314l.653.53"
      />
      <circle
        cx="16.75"
        cy="8.25"
        r="1.5"
        stroke={stroke}
        strokeWidth="1.5"
      />
      <path
        stroke={stroke}
        strokeLinecap="round"
        strokeWidth="1.5"
        d="M5.648 16.797H12.5"
      />
    </svg>
  );
}

PhotoApprovedIcon.propTypes = propTypes;
PhotoApprovedIcon.defaultProps = defaultProps;

export default PhotoApprovedIcon;
