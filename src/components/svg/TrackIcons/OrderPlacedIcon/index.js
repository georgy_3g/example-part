import React from 'react';
import PropTypes from 'prop-types';
import Colors from 'src/styles/colors.json';

const blue = Colors['$pickled-bluewood-color'];

const propTypes = {
  fill: PropTypes.string,
  stroke: PropTypes.string,
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
};

const defaultProps = {
  fill: 'none',
  stroke: blue,
  className: '',
  width: '24',
  height: '24',
};

function OrderPlacedIcon({
  className,
  stroke,
  fill,
  width,
  height,
}) {
  return (
    <svg
      className={className}
      width={width}
      height={height}
      fill={fill}
      viewBox="0 0 24 24"
    >
      <rect
        width="6.427"
        height="0.714"
        x="7.356"
        y="10.216"
        stroke={stroke}
        strokeWidth="0.714"
        rx="0.357"
      />
      <rect
        width="8.57"
        height="0.714"
        x="7.356"
        y="13.789"
        stroke={stroke}
        strokeWidth="0.714"
        rx="0.357"
      />
      <rect
        width="4.999"
        height="0.714"
        x="7.356"
        y="17.357"
        stroke={stroke}
        strokeWidth="0.714"
        rx="0.357"
      />
      <path
        stroke={stroke}
        strokeWidth="1.5"
        d="M7.007 22h9.992a2.857 2.857 0 002.857-2.857V7.97c0-.731-.28-1.434-.783-1.965L16.13 2.896a2.856 2.856 0 00-2.074-.892H7.034a2.857 2.857 0 00-2.857 2.851L4.15 19.138A2.857 2.857 0 007.007 22z"
      />
      <path
        stroke={stroke}
        strokeWidth="1.5"
        d="M14.144 2.717v2.86c0 1.183.959 2.142 2.142 2.142h2.86"
      />
    </svg>
  );
}

OrderPlacedIcon.propTypes = propTypes;
OrderPlacedIcon.defaultProps = defaultProps;

export default OrderPlacedIcon;
