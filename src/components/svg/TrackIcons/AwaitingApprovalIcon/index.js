import React from 'react';
import PropTypes from 'prop-types';
import Colors from 'src/styles/colors.json';

const blue = Colors['$pickled-bluewood-color'];
const white = Colors['$white-color'];

const propTypes = {
  fill: PropTypes.string,
  fillCircle: PropTypes.string,
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
  stroke: PropTypes.string,
};

const defaultProps = {
  fill: 'none',
  fillCircle: white,
  className: '',
  width: '24',
  height: '24',
  stroke: blue,
};

function AwaitingApprovalIcon({
  fill,
  fillCircle,
  className,
  width,
  height,
  stroke,
}) {
  return (
    <svg
      className={className}
      width={width}
      height={height}
      fill={fill}
      viewBox="0 0 24 24"
    >
      <path
        stroke={stroke}
        strokeLinecap="round"
        strokeWidth="1.5"
        d="M22.188 13.127a10.25 10.25 0 10-20.376-2.254 10.25 10.25 0 0020.376 2.254h0z"
      />
      <circle
        r="5.25"
        fill={fillCircle}
        stroke={stroke}
        strokeLinecap="round"
        strokeWidth="1.5"
        transform="matrix(-1 0 0 1 18 18)"
      />
      <path
        stroke={stroke}
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth="1.5"
        d="M16 18.5l1.302 1 2.698-3"
      />
      <path
        stroke={stroke}
        strokeLinecap="round"
        strokeWidth="1.5"
        d="M12 6.447v5.545a.654.654 0 01-.653.654H7"
      />
    </svg>
  );
}

AwaitingApprovalIcon.propTypes = propTypes;
AwaitingApprovalIcon.defaultProps = defaultProps;

export default AwaitingApprovalIcon;
