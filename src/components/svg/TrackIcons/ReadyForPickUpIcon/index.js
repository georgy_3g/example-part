import React from 'react';
import PropTypes from 'prop-types';
import Colors from 'src/styles/colors.json';

const blue = Colors['$pickled-bluewood-color'];

const propTypes = {
  fill: PropTypes.string,
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
  stroke: PropTypes.string,
};

const defaultProps = {
  fill: 'none',
  className: '',
  width: '24',
  height: '24',
  stroke: blue,
};

function ReadyForPickUpIcon({
  fill,
  className,
  width,
  height,
  stroke,
}) {
  return (
    <svg
      className={className}
      width={width}
      height={height}
      fill={fill}
      viewBox="0 0 24 24"
    >
      <path
        stroke={stroke}
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth="1.5"
        d="M12 22c5.523 0 10-4.477 10-10S17.523 2 12 2 2 6.477 2 12s4.477 10 10 10z"
      />
      <path
        stroke={stroke}
        strokeLinecap="round"
        strokeWidth="1.5"
        d="M7.834 15.19s1.755 1.25 4.167 1.25c2.411 0 4.166-1.25 4.166-1.25M9.916 9.604l-.933-.79a.833.833 0 00-1.084.008l-.9.782M17 9.604l-.933-.79a.833.833 0 00-1.084.008l-.9.782"
      />
    </svg>
  );
}

ReadyForPickUpIcon.propTypes = propTypes;
ReadyForPickUpIcon.defaultProps = defaultProps;

export default ReadyForPickUpIcon;
