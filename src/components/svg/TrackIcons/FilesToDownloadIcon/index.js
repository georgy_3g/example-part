import React from 'react';
import PropTypes from 'prop-types';
import Colors from 'src/styles/colors.json';

const blue = Colors['$pickled-bluewood-color'];

const propTypes = {
  fill: PropTypes.string,
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
  stroke: PropTypes.string,
};

const defaultProps = {
  fill: 'none',
  className: '',
  width: '24',
  height: '24',
  stroke: blue,
};

function FilesToDownloadIcon({
  fill,
  className,
  width,
  height,
  stroke,
}) {
  return (
    <svg
      className={className}
      width={width}
      height={height}
      fill={fill}
      viewBox="0 0 24 24"
    >
      <path
        fillRule="evenodd"
        stroke={stroke}
        strokeWidth="1.5"
        d="M5.211 13.892c-.981 0-1.375 1.266-.568 1.823l7.077 4.881a1 1 0 001.159-.017l6.657-4.88c.78-.572.375-1.807-.591-1.807H15.81l.214-8.844a2 2 0 00-2-2.048h-3.263a2 2 0 00-2 1.952l-.216 8.94H5.211z"
        clipRule="evenodd"
      />
    </svg>
  );
}

FilesToDownloadIcon.propTypes = propTypes;
FilesToDownloadIcon.defaultProps = defaultProps;

export default FilesToDownloadIcon;
