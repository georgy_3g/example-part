import React from 'react';
import PropTypes from 'prop-types';
import Colors from 'src/styles/colors.json';

const blue = Colors['$pickled-bluewood-color'];

const propTypes = {
  fill: PropTypes.string,
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
  stroke: PropTypes.string,
};

const defaultProps = {
  fill: 'none',
  className: '',
  width: '24',
  height: '24',
  stroke: blue,
};

function OrderDollarIcon({
  className,
  width,
  height,
  stroke,
  fill,
}) {
  return (
    <svg
      className={className}
      width={width}
      height={height}
      fill={fill}
      viewBox="0 0 24 24"
    >
      <path
        stroke={stroke}
        strokeLinecap="round"
        strokeWidth="1.8"
        d="M15.031 7.824l-1.674-1.373a2.447 2.447 0 00-3.47.374v0a2.447 2.447 0 00.418 3.452l1.87 1.45 1.745 1.313a2.605 2.605 0 01.53 3.629v0a2.605 2.605 0 01-3.65.543l-1.838-1.367M11.801 4v1.74M11.801 17.703v1.74"
      />
    </svg>
  );
}

OrderDollarIcon.propTypes = propTypes;
OrderDollarIcon.defaultProps = defaultProps;

export default OrderDollarIcon;
