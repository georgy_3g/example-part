import React from 'react';
import PropTypes from 'prop-types';
import Colors from 'src/styles/colors.json';

const grayColor = Colors['$darker-cadet-blue-color'];

const propTypes = {
  stroke: PropTypes.string,
  fill: PropTypes.string,
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
};

const defaultProps = {
  stroke: grayColor,
  fill: grayColor,
  className: '',
  width: '44',
  height: '44',
};

function RolledPhotoIcon({
  width,
  height,
  stroke,
  fill,
  className,
}) {
  return (
    <svg className={className} width={width} height={height} fill="none" viewBox="0 0 44 44">
      <path
        stroke={fill}
        strokeLinecap="round"
        strokeWidth="1.371"
        d="M25.656 15.142l1.347 4.714a1.323 1.323 0 01-1.272 1.686h-2.818M22.14 19.404l.824 3.274a1.323 1.323 0 01-1.253 1.645l-1.914.043"
      />
      <circle
        cx="19.922"
        cy="30.803"
        r="2.514"
        stroke={fill}
        strokeWidth="1.371"
        transform="rotate(134.898 19.922 30.803)"
      />
      <path
        stroke={stroke}
        strokeWidth="1.371"
        d="M11.941 28.4C19.04 21.328 29.77 10.114 29.77 10.114M11.94 28.4c.458-1.372-1.476-2.78-3.323-4.466m3.323 4.466c-2.285 2.286-5.485-.915-5.485-1.829 0-.914 2.162-2.637 2.162-2.637m0 0c-2.101-1.73-4.027-.36-4.728.54m4.728-.54c-.098-.09.106.087 0 0zm-4.728.54c-1.549 1.993-.177 4.383 2.108 6.669 2.438 2.439 7.88 6.454 11.306 8.599 1.747 1.093 3.656.589 5.042-.936l16.11-17.72 1.008-1.345a2.729 2.729 0 00-.667-3.915c-3.264-2.168-8.733-5.765-9.027-5.712M3.89 24.474c6.48-6.667 19.752-20.22 21.012-21.085 1.576-1.081 3.152-.54 4.728.54.598.411 1.174 1.206 1.614 1.928.548.898.384 2.027-.289 2.834l-1.185 1.423"
      />
    </svg>
  );
}

RolledPhotoIcon.propTypes = propTypes;
RolledPhotoIcon.defaultProps = defaultProps;

export default RolledPhotoIcon;
