import React from 'react';
import PropTypes from 'prop-types';
import Colors from 'src/styles/colors.json';

const blue = Colors['$pickled-bluewood-color'];
const green = Colors['$summer-green-color'];
const borderColor = Colors['$slate-gray-color'];
const gray = Colors['$silver-gray-color'];

const propTypes = {
  fill: PropTypes.string,
  fill2: PropTypes.string,
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
  disabled: PropTypes.bool,
  stroke: PropTypes.string,
};

const defaultProps = {
  fill: blue,
  fill2: green,
  className: '',
  width: '70',
  height: '70',
  disabled: false,
  stroke: borderColor,
};

function ShareFileIcon({
  width,
  height,
  fill2,
  fill,
  className,
  disabled,
  stroke,
}) {
  return (
    <svg
      width={width}
      height={height}
      version="1.1"
      viewBox="0 0 70 70"
      className={className}
      fill="none"
    >
      <rect
        width="68"
        height="68"
        x="1"
        y="1"
        stroke={disabled ? gray : stroke}
        strokeWidth="2"
        rx="19"
      />
      <path
        stroke={disabled ? gray : fill2}
        strokeWidth="2"
        d="M27 37.5L40 45M27.5 32.5l12-7"
      />
      <circle cx="23" cy="35" r="5" stroke={disabled ? gray : fill} strokeWidth="2" />
      <path
        stroke={disabled ? gray : fill}
        strokeWidth="2"
        d="M44 28a5 5 0 100-10 5 5 0 000 10z"
      />
      <circle cx="44" cy="47" r="5" stroke={disabled ? gray : fill} strokeWidth="2" />
    </svg>
  );
}

ShareFileIcon.propTypes = propTypes;
ShareFileIcon.defaultProps = defaultProps;

export default ShareFileIcon;
