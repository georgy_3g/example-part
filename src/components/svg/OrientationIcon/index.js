import React from 'react';
import PropTypes from 'prop-types';
import Colors from 'src/styles/colors.json';

const grayColor = Colors['$light-slate-gray-color'];

const propTypes = {
  stroke: PropTypes.string,
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
};

const defaultProps = {
  stroke: grayColor,
  className: '',
  width: '40',
  height: '30',
};

function OrientationIcon({
  width,
  height,
  stroke,
  className,
}) {
  return (
    <svg
      width={width}
      height={height}
      className={className}
      fill="none"
      viewBox="0 0 40 30"
    >
      <rect
        width="28"
        height="38"
        x="1"
        y="29"
        stroke={stroke}
        strokeWidth="2"
        rx="5"
        transform="rotate(-90 1 29)"
      />
      <rect
        width="18"
        height="28"
        x="6"
        y="24"
        stroke={stroke}
        strokeWidth="2"
        rx="2"
        transform="rotate(-90 6 24)"
      />
    </svg>
  );
}

OrientationIcon.propTypes = propTypes;
OrientationIcon.defaultProps = defaultProps;

export default OrientationIcon;
