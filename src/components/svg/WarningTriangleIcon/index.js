import React from 'react';
import PropTypes from 'prop-types';
import Colors from 'src/styles/colors.json';

const border = Colors['$sea-pink-red-color'];
const character = Colors['$burnt-sienna-color'];

const propTypes = {
  borderColor: PropTypes.string,
  characterColor: PropTypes.string,
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
};

const defaultProps = {
  borderColor: border,
  characterColor: character,
  className: '',
  width: '24',
  height: '24',
};

function WarningTriangleIcon({
  className,
  borderColor,
  characterColor,
  width,
  height,
}) {
  return (
    <svg
      className={className}
      width={width}
      height={height}
      fill="none"
      viewBox="0 0 24 24"
    >
      <path
        stroke={borderColor}
        strokeWidth="1.5"
        d="M8.744 5.698c1.44-2.52 5.072-2.52 6.512 0l5.538 9.691c1.428 2.5-.377 5.611-3.256 5.611H6.462c-2.88 0-4.685-3.11-3.256-5.61l5.538-9.692z"
      />
      <rect
        width="1.5"
        height="6"
        x="11.25"
        y="9"
        fill={characterColor}
        rx="0.75"
      />
      <rect
        width="1.5"
        height="1.5"
        x="11.25"
        y="16.5"
        fill={characterColor}
        rx="0.75"
      />
    </svg>
  );
}

WarningTriangleIcon.propTypes = propTypes;
WarningTriangleIcon.defaultProps = defaultProps;

export default WarningTriangleIcon;
