import React from 'react';
import PropTypes from 'prop-types';
import Colors from 'src/styles/colors.json';

const color = Colors['$darker-cadet-blue-color'];

const propTypes = {
  stroke: PropTypes.string,
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
};

const defaultProps = {
  stroke: color,
  className: '',
  width: '34',
  height: '34',
};

function TileIcon({
  className,
  stroke,
  width,
  height,
}) {
  return (
    <svg className={className} width={width} height={height} viewBox="0 0 34 34" fill="none">
      <rect
        width="14"
        height="14"
        x="1"
        y="1"
        stroke={stroke}
        strokeWidth="2"
        rx="4"
      />
      <rect
        width="14"
        height="14"
        x="1"
        y="19"
        stroke={stroke}
        strokeWidth="2"
        rx="4"
      />
      <rect
        width="14"
        height="14"
        x="19"
        y="1"
        stroke={stroke}
        strokeWidth="2"
        rx="4"
      />
      <rect
        width="14"
        height="14"
        x="19"
        y="19"
        stroke={stroke}
        strokeWidth="2"
        rx="4"
      />
    </svg>
  );
}

TileIcon.propTypes = propTypes;
TileIcon.defaultProps = defaultProps;

export default TileIcon;
