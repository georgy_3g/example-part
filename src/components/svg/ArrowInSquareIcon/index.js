import React from 'react';
import PropTypes from 'prop-types';
import Colors from 'src/styles/colors.json';

const gray = Colors['$shark-gray-color'];
const whiteLilac = Colors['$white-lilac-color'];

const propTypes = {
  fill: PropTypes.string,
  stroke: PropTypes.string,
  strokeWidth: PropTypes.string,
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
};

const defaultProps = {
  fill: whiteLilac,
  stroke: gray,
  strokeWidth: '2',
  className: '',
  width: '52',
  height: '52',
};

function ArrowInSquareIcon({
  fill,
  stroke,
  strokeWidth,
  className,
  width,
  height,
}) {
  return (
    <svg
      className={className}
      width={width}
      height={height}
      fill="none"
      viewBox="0 0 52 52"
    >
      <rect width={width} height={height} fill={fill} rx="15.75" />
      <path
        stroke={stroke}
        strokeLinecap="round"
        strokeWidth={strokeWidth}
        d="M23 34l6.515-6.515a2.1 2.1 0 000-2.97L23 18"
      />
    </svg>
  );
}

ArrowInSquareIcon.propTypes = propTypes;
ArrowInSquareIcon.defaultProps = defaultProps;

export default ArrowInSquareIcon;
