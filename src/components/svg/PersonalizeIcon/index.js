import React from 'react';
import PropTypes from 'prop-types';
import Colors from 'src/styles/colors.json';

const whiteColor = Colors['$white-color'];
const blueColor = Colors['$pickled-bluewood-color'];

const propTypes = {
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
  shadowColor: PropTypes.string,
  brashColor: PropTypes.string,
  plusColor: PropTypes.string,
};

const defaultProps = {
  className: '',
  width: '29',
  height: '29',
  shadowColor: whiteColor,
  brashColor: blueColor,
  plusColor: blueColor,
};

function PersonalizeIcon(props) {
  const { width, height, className, shadowColor, brashColor, plusColor } = props;
  return (
    <svg width={width} height={height} className={className} fill="none" viewBox="0 0 29 29">
      <mask id="path-2-inside-1" fill={whiteColor}>
        <path d="M6.003 20.655c.588-3.78 5.22-5.293 7.926-2.588 2.705 2.706 1.193 7.339-2.588 7.927l-4.04.628a1.688 1.688 0 01-1.927-1.927l.629-4.04z" />
      </mask>
      <path
        fill={shadowColor}
        d="M11.034 24.017l-4.04.629.615 3.952 4.04-.628-.615-3.953zm-3.684.985l.629-4.04-3.953-.614-.628 4.04 3.952.614zm-.356-.356a.312.312 0 01.356.356l-3.952-.615c-.384 2.468 1.744 4.595 4.211 4.211l-.615-3.952zm5.52-5.164c1.549 1.548.684 4.199-1.48 4.535l.615 3.953c5.398-.84 7.557-7.454 3.694-11.317l-2.828 2.829zm2.83-2.829C11.48 12.791 4.865 14.95 4.025 20.348l3.953.614c.336-2.163 2.987-3.029 4.536-1.48l2.828-2.829z"
        mask="url(#path-2-inside-1)"
      />
      <path
        stroke={shadowColor}
        strokeWidth="2"
        d="M21.325 14.466c-1.925 1.925-3.531 3.52-4.956 4.352-.697.406-1.253.569-1.706.556-.418-.012-.864-.176-1.364-.676-.5-.5-.664-.946-.676-1.364-.013-.454.15-1.01.556-1.706.831-1.425 2.427-3.032 4.352-4.957 1.925-1.924 3.531-3.52 4.956-4.351.697-.407 1.253-.57 1.706-.557.418.012.864.176 1.364.676.5.5.664.947.676 1.365.013.453-.15 1.009-.556 1.706-.831 1.424-2.427 3.031-4.352 4.956z"
      />
      <path
        fill={shadowColor}
        d="M15.643 12.928a3.879 3.879 0 013.427 3.427l-1.371 1.37a3.879 3.879 0 00-3.427-3.426l1.371-1.37z"
      />
      <path
        stroke={shadowColor}
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth="2"
        d="M6.66 3v3.66m0 3.659v-3.66m0 0h3.659H3"
      />
      <mask id="path-8-inside-2" fill={whiteColor}>
        <path d="M5.003 19.655c.588-3.78 5.22-5.293 7.926-2.588 2.705 2.706 1.193 7.339-2.588 7.927l-4.04.628a1.688 1.688 0 01-1.927-1.927l.629-4.04z" />
      </mask>
      <path
        fill={brashColor}
        d="M10.034 23.017l-4.04.629.615 3.952 4.04-.628-.615-3.953zm-3.684.985l.629-4.04-3.953-.614-.628 4.04 3.952.614zm-.356-.356a.312.312 0 01.356.356l-3.952-.615c-.384 2.468 1.744 4.595 4.211 4.211l-.615-3.952zm5.52-5.164c1.549 1.548.684 4.199-1.48 4.535l.615 3.953c5.398-.84 7.557-7.454 3.694-11.317l-2.828 2.829zm2.83-2.829C10.48 11.79 3.865 13.95 3.025 19.348l3.953.614c.336-2.163 2.987-3.029 4.536-1.48l2.828-2.829z"
        mask="url(#path-8-inside-2)"
      />
      <path
        stroke={brashColor}
        strokeWidth="2"
        d="M20.325 13.466c-1.925 1.925-3.531 3.52-4.956 4.352-.697.406-1.253.569-1.706.556-.418-.012-.864-.176-1.364-.676-.5-.5-.664-.946-.676-1.364-.013-.454.15-1.01.556-1.706.831-1.425 2.427-3.032 4.352-4.957 1.925-1.924 3.531-3.52 4.956-4.351.697-.407 1.253-.57 1.706-.557.418.012.864.176 1.364.676.5.5.664.947.676 1.365.013.453-.15 1.009-.556 1.706-.831 1.424-2.427 3.031-4.352 4.956z"
      />
      <path
        fill={brashColor}
        d="M14.643 11.928a3.879 3.879 0 013.427 3.427l-1.371 1.37a3.879 3.879 0 00-3.427-3.426l1.371-1.37z"
      />
      <path
        stroke={plusColor}
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth="2"
        d="M5.66 2v3.66m0 3.659v-3.66m0 0h3.659H2"
      />
    </svg>
  );
}

PersonalizeIcon.propTypes = propTypes;
PersonalizeIcon.defaultProps = defaultProps;

export default PersonalizeIcon;
