import React from 'react';
import PropTypes from 'prop-types';
import Colors from 'src/styles/colors.json';

const grayColor = Colors['$light-slate-gray-color'];
const greenColor = Colors['$summer-green-color'];

const propTypes = {
  stroke: PropTypes.string,
  stroke2: PropTypes.string,
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
  fill: PropTypes.string,
};

const defaultProps = {
  stroke: grayColor,
  stroke2: greenColor,
  className: '',
  width: '35',
  height: '30',
  fill: 'none',
};

function SelectPhotoMobileIcon({
  fill,
  width,
  height,
  stroke,
  stroke2,
  className,
}) {
  return (
    <svg
      className={className}
      width={width}
      height={height}
      fill={fill}
      viewBox="0 0 35 30"
    >
      <rect
        width="28.963"
        height="24.485"
        x="0.774"
        y="0.502"
        stroke={stroke}
        strokeWidth="1.3"
        rx="4.017"
        transform="matrix(.97889 -.20444 .21144 .9774 -.09 5.779)"
      />
      <path
        stroke={stroke2}
        strokeLinecap="round"
        strokeWidth="1.3"
        d="M9.217 20.409l3.184-7.188a.842.842 0 011.123-.42l2.653 1.217M15.595 17.715l1.656-4.089a.842.842 0 011.165-.43l2.504 1.274"
      />
      <path
        stroke={stroke}
        strokeWidth="1.3"
        d="M26.296 8.026c.256 1.186-.521 2.38-1.776 2.643-1.254.262-2.459-.519-2.715-1.705-.257-1.187.52-2.381 1.775-2.643 1.254-.262 2.459.518 2.716 1.705z"
      />
      <path
        stroke={stroke}
        strokeLinecap="round"
        strokeWidth="1.3"
        d="M8.662 24.354l15.988-3.357"
      />
      <rect
        width="28.963"
        height="24.485"
        x="0.499"
        y="0.768"
        fill="#fff"
        stroke={stroke2}
        strokeWidth="1.3"
        rx="4.017"
        transform="matrix(.97889 .20444 -.21144 .9774 5.626 -.624)"
      />
      <path
        stroke={stroke2}
        strokeLinecap="round"
        strokeWidth="1.3"
        d="M7.75 16.65l5.884-5.293a.846.846 0 011.2.064l1.92 2.173M14.69 16.754l3.205-3.074a.846.846 0 011.242.073l1.76 2.165"
      />
      <path
        stroke={stroke}
        strokeWidth="1.3"
        d="M28.459 12.195c-.257 1.186-1.461 1.967-2.716 1.705-1.254-.262-2.032-1.457-1.775-2.643.257-1.187 1.461-1.967 2.716-1.705 1.254.262 2.032 1.456 1.775 2.643z"
      />
      <path
        stroke={stroke}
        strokeLinecap="round"
        strokeWidth="1.3"
        d="M5.629 20.026l15.987 3.358"
      />
    </svg>
  );
}

SelectPhotoMobileIcon.propTypes = propTypes;
SelectPhotoMobileIcon.defaultProps = defaultProps;

export default SelectPhotoMobileIcon;
