import React from 'react';
import PropTypes from 'prop-types';
import Colors from 'src/styles/colors.json';

const color = Colors['$pickled-bluewood-color'];

const propTypes = {
  fill: PropTypes.string,
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
};

const defaultProps = {
  fill: color,
  className: '',
  width: '21',
  height: '23',
};

function PayPalIcon({
  width,
  height,
  fill,
  className,
}) {
  return (
    <svg
      width={width}
      height={height}
      viewBox="0 0 21 23"
      className={className}
    >
      <path
        fill={fill}
        d="M7.289 12.22h2.157c4.685 0 7.343-2.01 8.19-6.142.027-.136.052-.267.072-.398.047-.28.07-.531.081-.773.008-.166.013-.264.012-.355a3.14 3.14 0 00-.281-1.287c-.168-.366-.421-.726-.797-1.136-1.109-1.201-3.04-1.89-5.348-1.89h-7.35c-.25 0-.492.085-.682.24a.992.992 0 00-.356.605L1.661 9.219.011 19.353a.574.574 0 00.145.484.659.659 0 00.48.21h3.603l1.104-6.243c.071-.442.307-.845.664-1.136.357-.29.812-.45 1.282-.449zm11.834-5.68c-1.03 4.669-4.364 7.147-9.679 7.147H7.29a.443.443 0 00-.281.099.405.405 0 00-.146.25l-1.436 8.488h3.78c.218 0 .43-.074.597-.21a.866.866 0 00.31-.53l.039-.185.721-4.35.047-.24a.866.866 0 01.31-.53.947.947 0 01.598-.21h.572c3.705 0 6.606-1.434 7.453-5.579.349-1.708.172-3.137-.73-4.15z"
      />
    </svg>
  );
}

PayPalIcon.propTypes = propTypes;
PayPalIcon.defaultProps = defaultProps;

export default PayPalIcon;
