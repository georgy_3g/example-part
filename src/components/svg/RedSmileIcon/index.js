import React from 'react';
import PropTypes from 'prop-types';
import Colors from 'src/styles/colors.json';

const picledBluewoodColor = Colors['$pickled-bluewood-color'];
const burntSiennaColor = Colors['$burnt-sienna-color'];

const propTypes = {
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
  primaryStroke: PropTypes.string,
  secondaryStroke: PropTypes.string,
};

const defaultProps = {
  className: '',
  width: '18',
  height: '18',
  primaryStroke: picledBluewoodColor,
  secondaryStroke: burntSiennaColor,
};

function RedSmileIcon({
  width,
  height,
  className,
  primaryStroke,
  secondaryStroke,
}) {
  return (
    <svg
      className={className}
      width={width}
      height={height}
      fill="none"
      viewBox="0 0 18 18"
    >
      <circle
        cx="9"
        cy="9"
        r="8.25"
        stroke={primaryStroke}
        strokeWidth="1.5"
      />
      <path
        stroke={secondaryStroke}
        strokeLinecap="round"
        strokeWidth="1.5"
        d="M5 11.567s1.005 2.02 4 2.02c2.994 0 4-2.02 4-2.02"
      />
      <path
        stroke={primaryStroke}
        strokeLinecap="round"
        strokeWidth="1.25"
        d="M7 7.9l-.252-.243a1.8 1.8 0 00-2.52.017L4 7.901M14 7.9l-.252-.243a1.8 1.8 0 00-2.52.017L11 7.901"
      />
    </svg>
  );
}

RedSmileIcon.propTypes = propTypes;
RedSmileIcon.defaultProps = defaultProps;

export default RedSmileIcon;
