import React from 'react';
import PropTypes from 'prop-types';
import Colors from 'src/styles/colors.json';

const blue = Colors['$fiord-blue-color'];


const propTypes = {
  stroke: PropTypes.string,
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
};

const defaultProps = {
  stroke: blue,
  className: '',
  width: '28',
  height: '28',
};

function GiftCardIcon({
  className,
  width,
  height,
  stroke,
}) {
  return (
    <svg
      className={className}
      width={width}
      height={height}
      fill="none"
      viewBox="0 0 28 28"
    >
      <path
        stroke={stroke}
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth="2"
        d="M23 15v7.5a2.41 2.41 0 01-.79 1.768A2.814 2.814 0 0120.3 25H7.7a2.814 2.814 0 01-1.91-.732A2.41 2.41 0 015 22.5V15M14 8v16.625"
      />
      <path
        stroke={stroke}
        strokeWidth="2"
        d="M14 6a3 3 0 113 3h-3V6zM14 6a3 3 0 10-3 3h3V6z"
      />
      <rect
        width="20"
        height="6"
        x="4"
        y="9"
        stroke={stroke}
        strokeWidth="2"
        rx="2"
      />
    </svg>
  );
}

GiftCardIcon.propTypes = propTypes;
GiftCardIcon.defaultProps = defaultProps;

export default GiftCardIcon;
