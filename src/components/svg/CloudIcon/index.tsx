import React, { FunctionComponent } from 'react';
import Colors from 'src/styles/colors.json';
import { Props } from './interface';

const summerGreenColor = Colors['$summer-green-color'];
const slateGrayColor = Colors['$slate-gray-color-2'];
const whiteColor = Colors['$white-color'];

const defaultProps = {
  className: '',
  width: '64',
  height: '64',
  strokePath: slateGrayColor,
  strokeRect: summerGreenColor,
  fill: whiteColor,
  fillPath:slateGrayColor,
  fillRect: whiteColor,
};

const CloudIcon:FunctionComponent<Props> = ({
  className,
  width,
  height,
  strokePath,
  strokeRect,
  fill,
  fillPath,
  fillRect,
}) => (
  <svg
    className={className}
    width={width}
    height={height}
    fill="none"
    viewBox="0 0 64 64"
  >
    <title>my cloud icon</title>
    <g opacity="0.75">
      <mask id="path-1-inside-1_6471_248611" fill={fill}>
        <path
          fillRule="evenodd"
          d="M49.822 32.114a19.4 19.4 0 00.065-1.583C49.887 20.297 41.854 12 31.945 12c-8.124 0-14.986 5.577-17.195 13.226a12.731 12.731 0 00-1.731-.118C5.829 25.108 0 31.128 0 38.554S5.829 52 13.019 52c.094 0 .189 0 .283-.003h40.416a9.828 9.828 0 00.434 0h.579c.09 0 .176-.019.253-.053C60.05 51.402 64 46.979 64 41.604c0-5.741-4.507-10.395-10.065-10.395a9.76 9.76 0 00-4.113.905z"
          clipRule="evenodd"
        />
      </mask>
      <path
        fill={fillPath}
        d="M49.822 32.114l-1.993-.164-.28 3.42 3.114-1.442-.84-1.814zM14.75 25.226l-.272 1.981 1.713.235.48-1.661-1.921-.555zm-1.448 26.771v-2h-.044l.044 2zm40.416 0l.043-2h-.043v2zm.434 0v-2h-.044l.044 2zm.832-.053l-.213-1.989-.312.033-.286.128.811 1.828zM47.887 30.53c0 .478-.02.952-.058 1.42l3.987.326c.047-.576.07-1.159.07-1.746h-4zM31.945 14c8.744 0 15.942 7.34 15.942 16.53h4c0-11.277-8.868-20.53-19.942-20.53v4zM16.671 25.78C18.651 18.928 24.773 14 31.945 14v-4c-9.076 0-16.678 6.226-19.117 14.671l3.843 1.11zm-3.652 1.328c.495 0 .982.034 1.459.1l.544-3.964a14.735 14.735 0 00-2.003-.136v4zM2 38.554c0-6.383 4.994-11.446 11.019-11.446v-4C4.664 23.108-2 30.085-2 38.554h4zM13.019 50C6.994 50 2 44.937 2 38.554h-4C-2 47.024 4.664 54 13.019 54v-4zm.239-.002l-.24.002v4c.11 0 .22-.001.328-.003l-.088-4zm40.46 0H13.302v4h40.416v-4zm.217.002l-.174-.002-.087 3.999c.087.002.174.002.26.002v-4zm.173-.002a8.343 8.343 0 01-.173.002v4l.261-.003-.087-4zm.623 0h-.579v4h.579v-4zm-.558.118c.173-.077.363-.119.558-.119v4c.375 0 .737-.08 1.064-.225l-1.622-3.656zM62 41.604c0 4.394-3.22 7.922-7.229 8.351l.426 3.977C61.319 53.277 66 47.961 66 41.604h-4zm-8.065-8.395c4.393 0 8.065 3.697 8.065 8.395h4c0-6.784-5.342-12.395-12.065-12.395v4zm-3.272.72a7.759 7.759 0 013.272-.72v-4c-1.763 0-3.44.39-4.953 1.09l1.68 3.63z"
        mask="url(#path-1-inside-1_6471_248611)"
      />
      <path
        stroke={strokePath}
        strokeWidth="2"
        d="M37 28.5c0 3.114-2.312 5.5-5 5.5s-5-2.386-5-5.5 2.312-5.5 5-5.5 5 2.386 5 5.5z"
      />
      <rect
        width="16"
        height="14"
        x="24"
        y="29"
        fill={fillRect}
        stroke={strokeRect}
        strokeWidth="2"
        rx="4.5"
      />
      <path
        fill={fillPath}
        fillRule="evenodd"
        d="M32.536 37.097a1.594 1.594 0 00-.53-3.097 1.595 1.595 0 00-.53 3.097v1.157a.53.53 0 001.06 0v-1.157z"
        clipRule="evenodd"
      />
    </g>
  </svg>
);

CloudIcon.defaultProps = defaultProps;

export default CloudIcon;
