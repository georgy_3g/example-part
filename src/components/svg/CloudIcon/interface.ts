export interface Props {
  className: string,
  width?: string,
  height?: string,
  strokeCircle?: string,
  strokePath?: string,
  strokeRect?: string,
  fill?: string,
  fillPath?: string,
  fillRect?: string,
}