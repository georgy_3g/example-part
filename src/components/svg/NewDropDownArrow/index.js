import React from 'react';
import PropTypes from 'prop-types';
import Colors from 'src/styles/colors.json';

const gray = Colors['$slate-gray-color'];

const propTypes = {
  fill: PropTypes.string,
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
};

const defaultProps = {
  fill: gray,
  className: '',
  width: '16',
  height: '9',
};


function DownloadFileSmallIcon({
  width,
  height,
  fill,
  className,
}) {
  return (
    <svg
      width={width}
      height={height}
      version="1.1"
      viewBox="0 0 16 9"
      className={className}
      fill="none"
    >
      <path
        stroke={fill}
        strokeLinecap="round"
        strokeWidth="2.5"
        d="M14 2L9.485 6.515a2.1 2.1 0 01-2.97 0L2 2"
      />
    </svg>
  );
}

DownloadFileSmallIcon.propTypes = propTypes;
DownloadFileSmallIcon.defaultProps = defaultProps;

export default DownloadFileSmallIcon;
