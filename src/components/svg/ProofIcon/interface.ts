export interface Props {
  className: string,
  width?: string,
  height?: string,
  strokeCircle?: string,
  strokePath?: string,
  strokePathSecond?: string,
  strokeRect?: string,
  strokeRectSecond?: string,
  fill?: string,
  fillPath?: string,
  fillRect?: string,
}