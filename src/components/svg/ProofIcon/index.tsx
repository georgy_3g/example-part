import React, { FunctionComponent } from 'react';
import Colors from 'src/styles/colors.json';
import { Props } from './interface';

const summerGreenColor = Colors['$summer-green-color'];
const slateGrayColor = Colors['$slate-gray-color-2'];
const whiteColor = Colors['$white-color'];

const defaultProps = {
  className: '',
  width: '64',
  height: '64',
  strokeCircle: summerGreenColor,
  strokePath: slateGrayColor,
  strokePathSecond: summerGreenColor,
  strokeRect: slateGrayColor,
  strokeRectSecond: summerGreenColor,
  fillRect: whiteColor,
};

const ProofIcon:FunctionComponent<Props> = ({
  className,
  width,
  height,
  strokeCircle,
  strokePath,
  strokePathSecond,
  strokeRect,
  strokeRectSecond,
  fillRect,
}) => (
  <svg
    className={className}
    width={width}
    height={height}
    fill="none"
    viewBox="0 0 64 64"
  >
    <title>proofs page</title>
    <g strokeWidth="2" opacity="0.75">
      <rect width="52" height="45" x="6" y="9" stroke={strokeRect} rx="8" />
      <rect
        width="42"
        height="35"
        x="11"
        y="14"
        stroke={strokeRectSecond}
        rx="4"
      />
      <path
        stroke={strokePath}
        strokeLinecap="round"
        d="M18 37l6.081-7.904a1.447 1.447 0 011.977-.302L29 30.854M28 34l3.19-4.7a1.447 1.447 0 012.105-.313L36 31.169"
      />
      <circle cx="42" cy="24" r="3" stroke={strokeCircle} />
      <path stroke={strokePathSecond} strokeLinecap="round" d="M18 42.5h18" />
      <rect
        width="18"
        height="18"
        x="29"
        y="39"
        fill={fillRect}
        stroke={strokeRect}
        rx="9"
      />
      <path
        stroke={strokePath}
        strokeLinecap="round"
        strokeLinejoin="round"
        d="M40.485 50.828L37.657 48m-2.829-2.828L37.657 48m0 0l2.828-2.828-5.657 5.656"
      />
      <rect
        width="18"
        height="18"
        x="44"
        y="39"
        fill={fillRect}
        stroke={strokeRect}
        rx="9"
      />
      <path
        stroke={strokePathSecond}
        strokeLinecap="round"
        strokeLinejoin="round"
        d="M49 48.857L51.4 51l5.6-5"
      />
    </g>
  </svg>
);

ProofIcon.defaultProps = defaultProps;

export default ProofIcon;
