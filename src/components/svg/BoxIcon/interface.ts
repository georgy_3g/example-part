export interface propsInterface {
  className?: string;
  width?: string;
  height?: string;
  fillOne?: string;
  fillTwo?: string;
  fillFree?: string;
}
