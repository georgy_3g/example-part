import React, { FunctionComponent } from 'react';
import bem from 'src/utils/bem';
import { propsInterface } from './interface';
import styles from './index.module.scss';

const b = bem('box-icon', styles);


const defaultProps = {
  className: '',
};


const BoxIcon: FunctionComponent<propsInterface> = ({ className }) => {
    return (
      <svg className={className} version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 121 93" xmlSpace="preserve">
        <title>shipping target button</title>
        <g>
          <path className={b('st0')} d="M-20-34h160v160H-20V-34z" />
          <path
            className={b("st1")}
            d="M82.6,80.8h12.1v-8.1H82.6V80.8z M80.3,42.5H35.5l2,4h54.9L80.3,42.5z M84,39.5l20.1,6.7l11.6-16.5l-20.1-5
		L84,39.5z"
          />
          <g transform="translate(0 -.124)">
            <g transform="translate(0 18)">
              <defs>
                <filter id="Adobe_OpacityMaskFilter" filterUnits="userSpaceOnUse" x="0" y="0.4" width="120.9" height="74.6">
                  <feColorMatrix type="matrix" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 1 0" />
                </filter>
              </defs>
              <mask maskUnits="userSpaceOnUse" x="0" y="0.4" width="120.9" height="74.6" id="b_1_">
                <g className={b("st2")}>
                  <path id="a_1_" className={b("st3")} d="M0,0.4h120.9V75H0V0.4z" />
                </g>
              </mask>
              <path
                className={b("st4")}
                d="M104,28.3L84,21.6L95.5,6.7l20.1,5L104,28.3z M38.3,71h64.5V32.7H38.3L38.3,71L38.3,71z M35.5,24.6h44.8
				l12.1,4H37.5L35.5,24.6L35.5,24.6z M34.3,70.2l-20.2-6.7V25.4l20.2,6.7V70.2z M6,5.9l18.8,6.3l5.7,11.3l0,0l1.9,3.7L13.5,21
				L6,5.9z M119.4,8.5l-24.2-6c-0.8-0.2-1.6,0.1-2.1,0.7L79.6,20.6H33.5L28,9.6c-0.2-0.5-0.7-0.8-1.2-1L2.7,0.5
				C1.6,0.2,0.5,0.8,0.1,1.8C-0.1,2.3,0,2.9,0.2,3.3l9.9,19.7v41.8c0,0.9,0.6,1.6,1.4,1.9l24.2,8.1c0.2,0.1,0.4,0.1,0.6,0.1h68.5
				c1.1,0,2-0.9,2-2c0,0,0,0,0,0V31.3l13.7-19.6c0.4-0.5,0.5-1.2,0.2-1.9C120.6,9.2,120,8.7,119.4,8.5z"
              />
            </g>
            <defs>
              <filter id="Adobe_OpacityMaskFilter_1_" filterUnits="userSpaceOnUse" x="78.6" y="68.8" width="20.2" height="16.1">
                <feColorMatrix type="matrix" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 1 0" />
              </filter>
            </defs>
            <mask maskUnits="userSpaceOnUse" x="78.6" y="68.8" width="20.2" height="16.1" id="d_1_">
              <g className={b("st5")}>
                <path id="c_1_" className={b("st3")} d="M0,93h120.9V0.1H0V93z" />
              </g>
            </mask>
            <path
              className={b("st6")}
              d="M82.6,80.9h12.1v-8.1H82.6V80.9z M80.6,84.9h16.1c1.1,0,2-0.9,2-2c0,0,0,0,0,0V70.8c0-1.1-0.9-2-2-2
			c0,0,0,0,0,0H80.6c-1.1,0-2,0.9-2,2c0,0,0,0,0,0v12.1C78.6,84,79.5,84.9,80.6,84.9z"
            />
            <defs>
              <filter id="Adobe_OpacityMaskFilter_2_" filterUnits="userSpaceOnUse" x="19.1" y="0.1" width="73.9" height="14.5">
                <feColorMatrix type="matrix" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 1 0" />
              </filter>
            </defs>
            <mask maskUnits="userSpaceOnUse" x="19.1" y="0.1" width="73.9" height="14.5" id="d_2_">
              <g className={b("st7")}>
                <path id="c_2_" className={b("st3")} d="M0,93h120.9V0.1H0V93z" />
              </g>
            </mask>
            <path
              className={b("st8")}
              d="M90.8,12.5c1.2,0,2.2-0.9,2.2-2.1s-0.9-2.2-2.1-2.2c0,0-0.1,0-0.1,0c-1.2,0-2.1,1-2.1,2.2
			C88.8,11.6,89.7,12.5,90.8,12.5 M77.4,4.3c1.1,0,2-0.9,2-2c0-1.1-0.9-2-2-2c0,0,0,0,0,0c-1.1,0-2,0.9-2,2
			C75.4,3.4,76.3,4.3,77.4,4.3 M40.4,4.4c1.2,0,2.1-1,2.1-2.1s-1-2.1-2.1-2.1l0,0c-1.2,0-2.1,1-2.1,2.1S39.3,4.4,40.4,4.4
			 M21.3,14.6c1.2,0,2.1-1,2.1-2.1s-1-2.1-2.1-2.1s-2.1,1-2.1,2.1S20.1,14.6,21.3,14.6"
            />
            <defs>
              <filter id="Adobe_OpacityMaskFilter_3_" filterUnits="userSpaceOnUse" x="28.4" y="9.9" width="55.8" height="16.4">
                <feColorMatrix type="matrix" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 1 0" />
              </filter>
            </defs>
            <mask maskUnits="userSpaceOnUse" x="28.4" y="9.9" width="55.8" height="16.4" id="d_3_">
              <g className={b("st9")}>
                <path id="c_3_" className={b("st3")} d="M0,93h120.9V0.1H0V93z" />
              </g>
            </mask>
            <path
              className={b("st10")}
              d="M68.3,25.4c0.9,0.1,1.6-0.5,1.8-1.3c0.6-4.2,2.2-7.3,4.6-9.1c3.4-2.5,7.6-1.9,7.6-1.8
			c0.8,0.1,1.7-0.4,1.8-1.3c0,0,0,0,0,0c0.2-0.9-0.4-1.7-1.3-1.8c0,0,0,0,0,0c-0.2,0-5.5-0.9-10,2.4c-3.2,2.3-5.2,6.1-5.9,11.3
			C66.9,24.5,67.4,25.3,68.3,25.4C68.3,25.4,68.3,25.4,68.3,25.4 M30.3,15.9c3.5-0.7,6.5-0.3,8.8,1.3c3.9,2.7,4.8,7.8,4.8,7.8
			c0.1,0.9,0.9,1.4,1.8,1.3c0.9-0.1,1.4-0.9,1.3-1.8c0,0,0,0,0,0c0-0.3-1.1-6.5-6.1-9.9c-3-2.1-6.8-2.7-11.2-1.8
			c-0.8,0.2-1.4,1-1.2,1.8c0,0,0,0,0,0C28.6,15.5,29.4,16,30.3,15.9C30.3,15.9,30.3,15.9,30.3,15.9"
            />
            <defs>
              <filter id="Adobe_OpacityMaskFilter_4_" filterUnits="userSpaceOnUse" x="56.8" y="2.6" width="3.1" height="15.5">
                <feColorMatrix type="matrix" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 1 0" />
              </filter>
            </defs>
            <mask maskUnits="userSpaceOnUse" x="56.8" y="2.6" width="3.1" height="15.5" id="d_4_">
              <g className={b("st11")}>
                <path id="c_4_" className={b("st3")} d="M0,93h120.9V0.1H0V93z" />
              </g>
            </mask>
            <path
              className={b("st12")}
              d="M58.3,18.1c0.9,0,1.6-0.7,1.6-1.6V4.2c0-0.9-0.7-1.6-1.6-1.6c-0.9,0-1.6,0.7-1.6,1.6v12.3
			C56.8,17.4,57.5,18.1,58.3,18.1"
            />
            <defs>
              <filter id="Adobe_OpacityMaskFilter_5_" filterUnits="userSpaceOnUse" x="56.7" y="27.5" width="4.3" height="4.3">
                <feColorMatrix type="matrix" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 1 0" />
              </filter>
            </defs>
            <mask maskUnits="userSpaceOnUse" x="56.7" y="27.5" width="4.3" height="4.3" id="d_5_">
              <g className={b("st13")}>
                <path id="c_5_" className={b("st3")} d="M0,93h120.9V0.1H0V93z" />
              </g>
            </mask>
            <path className={b("st14")} d="M58.8,31.8c1.2,0,2.1-1,2.1-2.1s-1-2.1-2.1-2.1s-2.1,1-2.1,2.1S57.6,31.8,58.8,31.8L58.8,31.8" />
          </g>
        </g>
      </svg>
)};

BoxIcon.defaultProps = defaultProps;

export default BoxIcon;
