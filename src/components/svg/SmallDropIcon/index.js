import React from 'react';
import PropTypes from 'prop-types';
import Colors from 'src/styles/colors.json';

const sharkGray = Colors['$shark-gray-color'];
const green = Colors['$summer-green-color'];

const propTypes = {
  color1: PropTypes.string,
  color2: PropTypes.string,
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
};

const defaultProps = {
  color1: sharkGray,
  color2: green,
  className: '',
  width: '32',
  height: '32',
};

function SmallDropIcon({
  width,
  height,
  color1,
  color2,
  className,
}) {
  return (
    <svg
      className={className}
      width={width}
      height={height}
      fill="none"
      viewBox="0 0 32 32"
    >
      <path
        fill={color2}
        d="M6.113 21.629c0-9.473 9.957-18.133 9.957-18.133s5.059 3.946 7.78 9.535L7.549 26.28c-.808-1.157-1.435-2.615-1.435-4.651z"
      />
      <path
        stroke={color1}
        strokeLinecap="round"
        strokeWidth="2"
        d="M16.07 2.291s10.807 9.472 10.807 18.944c0 3.158-1.543 9.473-10.807 9.473M16.069 2.291S5.262 11.763 5.262 21.235c0 3.158 1.544 9.473 10.807 9.473"
      />
    </svg>
  );
}

SmallDropIcon.propTypes = propTypes;
SmallDropIcon.defaultProps = defaultProps;

export default SmallDropIcon;
