import React from 'react';
import PropTypes from 'prop-types';
import Colors from 'src/styles/colors.json';

const white = Colors['$white-color'];
const sharkGray = Colors['$shark-gray-color'];
const green = Colors['$summer-green-color'];

const propTypes = {
  color1: PropTypes.string,
  color2: PropTypes.string,
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
};

const defaultProps = {
  color1: sharkGray,
  color2: green,
  className: '',
  width: '32',
  height: '32',
};

function SmallBrushIcon({
  width,
  height,
  color1,
  color2,
  className,
}) {
  return (
    <svg
      className={className}
      width={width}
      height={height}
      fill="none"
      viewBox="0 0 32 32"
    >
      <g clipPath="url(#clip0)">
        <mask id="path-1-inside-1" fill={white}>
          <path d="M5.203 23.664c.643-4.133 5.707-5.786 8.664-2.828 2.957 2.957 1.304 8.021-2.828 8.664l-4.416.687a1.845 1.845 0 01-2.107-2.107l.687-4.416z" />
        </mask>
        <path
          fill={color1}
          d="M6.623 30.187l-.308-1.976.308 1.976zm4.416-.687l-.308-1.976.308 1.976zm-5.836-5.836l-1.976-.307 1.976.307zm5.528 3.86l-4.416.687.615 3.952 4.416-.687-.615-3.952zm-4.239.864l.687-4.416-3.952-.615-.687 4.416 3.952.615zm-.177-.177a.155.155 0 01.177.177l-3.952-.615c-.4 2.572 1.818 4.79 4.39 4.39l-.615-3.952zm6.138-5.96c1.8 1.8.794 4.881-1.722 5.273l.615 3.952c5.75-.894 8.05-7.94 3.935-12.054l-2.828 2.828zm2.828-2.83c-4.114-4.114-11.16-1.814-12.054 3.936l3.952.615c.391-2.516 3.474-3.522 5.274-1.722l2.828-2.828z"
          mask="url(#path-1-inside-1)"
        />
        <path
          stroke={color1}
          strokeWidth="2"
          d="M22.338 16.647c-2.1 2.1-3.867 3.856-5.437 4.772-.77.448-1.395.635-1.914.62-.485-.014-.994-.205-1.555-.766-.56-.56-.752-1.07-.766-1.554-.015-.52.172-1.145.62-1.915.917-1.57 2.672-3.336 4.772-5.436 2.101-2.101 3.867-3.856 5.437-4.772.77-.449 1.395-.635 1.915-.62.484.013.993.205 1.554.765.56.561.752 1.07.766 1.555.015.52-.172 1.145-.62 1.914-.917 1.57-2.671 3.336-4.772 5.437z"
        />
        <path
          fill={color1}
          d="M16.06 14.897a4.24 4.24 0 013.746 3.746l-1.498 1.498a4.24 4.24 0 00-3.746-3.746l1.498-1.498z"
        />
        <path
          stroke={color2}
          strokeLinecap="round"
          strokeLinejoin="round"
          strokeWidth="2"
          d="M8.125 4v4m0 4V8m0 0h4-8"
        />
      </g>
      <defs>
        <clipPath id="clip0">
          <path fill={white} d="M0 0H32V32H0z" />
        </clipPath>
      </defs>
    </svg>
  );
}

SmallBrushIcon.propTypes = propTypes;
SmallBrushIcon.defaultProps = defaultProps;

export default SmallBrushIcon;
