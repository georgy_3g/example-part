import React from 'react';
import PropTypes from 'prop-types';
import Colors from 'src/styles/colors.json';

const blue = Colors['$pickled-bluewood-color'];
const green = Colors['$summer-green-color'];
const borderColor = Colors['$slate-gray-color'];
const gray = Colors['$silver-gray-color'];

const propTypes = {
  fill: PropTypes.string,
  fill2: PropTypes.string,
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
  disabled: PropTypes.bool,
  stroke: PropTypes.string,
};

const defaultProps = {
  fill: blue,
  fill2: green,
  className: '',
  width: '70',
  height: '70',
  disabled: false,
  stroke: borderColor,
};

function SlideshowIcon({
  width,
  height,
  fill2,
  fill,
  className,
  disabled,
  stroke,
}) {
  return (
    <svg
      width={width}
      height={height}
      version="1.1"
      viewBox="0 0 70 70"
      className={className}
      fill="none"
    >
      <rect
        width="68"
        height="68"
        x="1"
        y="1"
        stroke={disabled ? gray : stroke}
        strokeWidth="2"
        rx="19"
      />
      <rect
        width="34"
        height="34"
        x="18"
        y="18"
        stroke={disabled ? gray : fill}
        strokeWidth="2"
        rx="17"
      />
      <path
        fill={disabled ? gray : fill2}
        d="M40.5 34.134a1 1 0 010 1.732l-7.5 4.33a1 1 0 01-1.5-.866v-8.66a1 1 0 011.5-.866l7.5 4.33z"
      />
    </svg>
  );
}

SlideshowIcon.propTypes = propTypes;
SlideshowIcon.defaultProps = defaultProps;

export default SlideshowIcon;
