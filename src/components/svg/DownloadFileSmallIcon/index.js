import React from 'react';
import PropTypes from 'prop-types';
import Colors from 'src/styles/colors.json';

const green = Colors['$summer-green-color'];

const propTypes = {
  fill: PropTypes.string,
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
};

const defaultProps = {
  fill: green,
  className: '',
  width: '26',
  height: '22',
};


function DownloadFileSmallIcon({
  width,
  height,
  fill,
  className,
}) {
  return (
    <svg
      width={width}
      height={height}
      version="1.1"
      viewBox="0 0 26 22"
      className={className}
      fill="none"
    >
      <path
        stroke={fill}
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth="2"
        d="M1.816 12.716v1.625a6 6 0 006 6h10.368a6 6 0 006-6v-1.625M13 1.023v12.71m0 0L7.917 8.649M13 13.733l5.084-5.084"
      />
    </svg>
  );
}

DownloadFileSmallIcon.propTypes = propTypes;
DownloadFileSmallIcon.defaultProps = defaultProps;

export default DownloadFileSmallIcon;
