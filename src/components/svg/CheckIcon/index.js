import React from 'react';
import PropTypes from 'prop-types';
import Colors from 'src/styles/colors.json';

const greenColor = Colors['$summer-green-color'];

const propTypes = {
  stroke: PropTypes.string,
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
};

const defaultProps = {
  stroke: greenColor,
  className: '',
  width: '14',
  height: '14',
};

function CheckIcon({
  width,
  height,
  stroke,
  className,
}) {
  return (
    <svg
      width={width}
      height={height}
      fill="none"
      viewBox="0 0 14 14"
      className={className}
    >
      <path
        stroke={stroke}
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth="2"
        d="M2 8.015L6.061 11 12 3"
      />
    </svg>
  );
}

CheckIcon.propTypes = propTypes;
CheckIcon.defaultProps = defaultProps;

export default CheckIcon;
