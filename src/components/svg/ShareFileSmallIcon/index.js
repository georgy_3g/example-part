import React from 'react';
import PropTypes from 'prop-types';
import Colors from 'src/styles/colors.json';

const green = Colors['$summer-green-color'];

const propTypes = {
  fill: PropTypes.string,
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
};

const defaultProps = {
  fill: green,
  className: '',
  width: '26',
  height: '28',
};


function ShareFileSmallIcon({
  width,
  height,
  fill,
  className,
}) {
  return (
    <svg
      width={width}
      height={height}
      version="1.1"
      viewBox="0 0 26 28"
      className={className}
      fill="none"
    >
      <path
        fill={fill}
        fillRule="evenodd"
        d="M25.041 5.65a4.783 4.783 0 01-8.368 3.165l-6.454 3.764c.139.448.213.924.213 1.418 0 .555-.094 1.089-.269 1.585l6.422 3.705a4.783 4.783 0 11-.932 1.771l-6.534-3.77a4.783 4.783 0 11.12-6.453l6.452-3.763a4.783 4.783 0 119.35-1.422zM20.26 8.432a2.783 2.783 0 100-5.565 2.783 2.783 0 000 5.565zM8.432 13.997a2.783 2.783 0 11-5.565 0 2.783 2.783 0 015.565 0zm14.61 8.352a2.783 2.783 0 11-5.565 0 2.783 2.783 0 015.565 0z"
        clipRule="evenodd"
      />
    </svg>
  );
}

ShareFileSmallIcon.propTypes = propTypes;
ShareFileSmallIcon.defaultProps = defaultProps;

export default ShareFileSmallIcon;
