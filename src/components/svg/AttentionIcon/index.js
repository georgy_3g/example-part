import React from 'react';
import PropTypes from 'prop-types';
import Colors from 'src/styles/colors.json';

const blue = Colors['$pickled-bluewood-color'];

const propTypes = {
  fill: PropTypes.string,
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
};

const defaultProps = {
  fill: blue,
  className: '',
  width: '32',
  height: '32',
};

function AttentionIcon({
  width,
  height,
  fill,
  className,
}) {
  return (
    <svg
      className={className}
      width={width}
      height={height}
      fill="none"
      viewBox="0 0 32 32"
    >
      <path
        stroke={fill}
        strokeWidth="2"
        d="M11.659 7.597c1.92-3.359 6.763-3.359 8.682 0l7.384 12.922C29.63 23.853 27.223 28 23.385 28H8.614c-3.838 0-6.245-4.147-4.34-7.48l7.384-12.923z"
      />
      <rect width="2" height="8" x="15" y="12" fill={fill} rx="1" />
      <rect width="2" height="2" x="15" y="22" fill={fill} rx="1" />
    </svg>
  );
}

AttentionIcon.propTypes = propTypes;
AttentionIcon.defaultProps = defaultProps;

export default AttentionIcon;
