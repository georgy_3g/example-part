import React from 'react';
import PropTypes from 'prop-types';
import Colors from 'src/styles/colors.json';

const rhinoBlueColor = Colors['$rhino-blue-color'];
const whiteColor = Colors['$white-color'];

const propTypes = {
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
  stroke: PropTypes.string,
  color: PropTypes.string,
};

const defaultProps = {
  className: '',
  width: '80',
  height: '80',
  stroke: rhinoBlueColor,
  color: whiteColor,
};

function PaintingIcon({
  width,
  height,
  className,
  stroke,
  color,
}) {
  return (
    <svg
      width={width}
      height={height}
      className={className}
      fill="none"
      viewBox="0 0 58 58"
    >
      <rect
        width="58"
        height="58"
        fill={stroke}
        rx="17.4"
        transform="matrix(-1 0 0 1 58 0)"
      />
      <g clipPath="url(#clip0_5681:218593)">
        <path
          stroke={color}
          strokeWidth="1.595"
          d="M21.91 16.519l6.43-4.525a1.093 1.093 0 011.268 0l6.487 4.525M42.58 16.519H14.634c-1.64 0-2.97 1.312-2.97 2.93v20.932c0 1.619 1.33 2.931 2.97 2.931H42.58c1.64 0 2.97-1.312 2.97-2.931V19.45c0-1.62-1.33-2.931-2.97-2.931z"
        />
        <path
          stroke={color}
          strokeLinecap="round"
          strokeWidth="1.595"
          d="M19.36 37.036h13.085M20.082 34.313l3.799-5.338a.53.53 0 01.762-.108l1.988 1.47M25.535 33.224l2.07-3.145a.544.544 0 01.79-.109l1.851 1.539"
        />
        <path
          stroke={color}
          strokeLinecap="round"
          strokeLinejoin="round"
          strokeWidth="1.595"
          d="M37.355 23.325a1.839 1.839 0 00-.558-.245 1.96 1.96 0 00-.845 0 1.81 1.81 0 00-.749.394 1.715 1.715 0 00-1.212-1.089 1.523 1.523 0 00-.612 0 1.743 1.743 0 00-1.47 1.865 4.093 4.093 0 00.217.817 8.534 8.534 0 001.062 1.988l.872 1.253 1.361-.694a8.413 8.413 0 001.934-1.362l.15-.15c.117-.12.226-.247.326-.38a1.743 1.743 0 00-.476-2.397z"
        />
        <path
          fill={color}
          d="M29.052 14.5a1.076 1.076 0 100-2.151 1.076 1.076 0 000 2.151z"
        />
      </g>
      <defs>
        <clipPath id="clip0_5681:218593">
          <path
            fill={color}
            d="M0 0H36.25V34.674H0z"
            transform="translate(10.875 10.214)"
          />
        </clipPath>
      </defs>
    </svg>
  );
}

PaintingIcon.propTypes = propTypes;
PaintingIcon.defaultProps = defaultProps;

export default PaintingIcon;
