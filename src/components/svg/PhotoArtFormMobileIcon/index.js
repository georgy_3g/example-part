import React from 'react';
import PropTypes from 'prop-types';
import Colors from 'src/styles/colors.json';

const grayColor = Colors['$light-slate-gray-color'];
const greenColor = Colors['$summer-green-color'];
const whiteColor = Colors['$white-color'];

const propTypes = {
  stroke: PropTypes.string,
  stroke2: PropTypes.string,
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
  fill: PropTypes.string,
  opacity: PropTypes.string,
};

const defaultProps = {
  stroke: grayColor,
  stroke2: greenColor,
  className: '',
  width: '35',
  height: '30',
  fill: 'none',
  opacity: '0.8',
};

function PhotoArtFormMobileIcon({
  fill,
  width,
  height,
  stroke,
  stroke2,
  className,
  opacity,
}) {
  return (
    <svg
      className={className}
      width={width}
      height={height}
      fill={fill}
      viewBox="0 0 35 30"
    >
      <g opacity={opacity}>
        <rect
          width="32.768"
          height="28.225"
          x="0.869"
          y="0.65"
          stroke={stroke}
          strokeWidth="1.3"
          rx="3.892"
        />
        <rect
          width="1.136"
          height="15.898"
          x="10.438"
          y="6.813"
          fill={stroke}
          rx="0.568"
        />
        <rect
          width="1.136"
          height="15.898"
          x="16.684"
          y="6.813"
          fill={stroke}
          rx="0.568"
        />
        <rect
          width="1.136"
          height="15.898"
          x="22.93"
          y="6.813"
          fill={stroke}
          rx="0.568"
        />
        <circle
          cx="23.498"
          cy="11.357"
          r="1.621"
          fill={whiteColor}
          stroke={stroke2}
          strokeWidth="1.3"
        />
        <circle
          cx="17.252"
          cy="14.763"
          r="1.621"
          fill={whiteColor}
          stroke={stroke2}
          strokeWidth="1.3"
        />
        <circle
          cx="11.006"
          cy="18.17"
          r="1.621"
          fill={whiteColor}
          stroke={stroke2}
          strokeWidth="1.3"
        />
      </g>
    </svg>
  );
}

PhotoArtFormMobileIcon.propTypes = propTypes;
PhotoArtFormMobileIcon.defaultProps = defaultProps;

export default PhotoArtFormMobileIcon;
