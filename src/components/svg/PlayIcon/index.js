import React from 'react';
import PropTypes from 'prop-types';
import Colors from 'src/styles/colors.json';

const white = Colors['$white-color'];

const propTypes = {
  fill: PropTypes.string,
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
};

const defaultProps = {
  className: '',
  width: '16',
  height: '18',
  fill: white,
};

function PlayIcon(props) {
  const { fill, width, height, className } = props;
  return (
    <svg width={width} height={height} viewBox="0 0 16 18" className={className}>
      <path
        fill={fill}
        d="M15 7.268c1.333.77 1.333 2.694 0 3.464L3 17.66c-1.333.77-3-.192-3-1.732V2.072C0 .532 1.667-.43 3 .34l12 6.928z"
      />
    </svg>
  );
}

PlayIcon.propTypes = propTypes;
PlayIcon.defaultProps = defaultProps;

export default PlayIcon;
