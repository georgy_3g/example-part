import React from 'react';
import PropTypes from 'prop-types';
import Colors from 'src/styles/colors.json';

const grayLightColor = Colors['$athens-gray-color'];
const lightSlateGrayColor = Colors['$slate-gray-color-2'];

const propTypes = {
  fillRect: PropTypes.string,
  fillPath: PropTypes.string,
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
};

const defaultProps = {
  fillRect: grayLightColor,
  fillPath: lightSlateGrayColor,
  className: '',
  width: '46',
  height: '46',
};

function RectangleCloseIcon({
  width,
  height,
  fillRect,
  fillPath,
  className,
}) {
  return (
    <svg
      className={className}
      width={width}
      height={height}
      fill="none"
      viewBox="0 0 46 46"
    >
      <rect width="46" height="46" fill={fillRect} rx="12" />
      <path
        fill={fillPath}
        fillRule="evenodd"
        d="M30.071 31.485a1 1 0 001.414-1.414l-7.07-7.07 7.07-7.071a1 1 0 10-1.413-1.415L23 21.586l-7.072-7.071a1 1 0 00-1.414 1.414L21.586 23l-7.07 7.07a1 1 0 101.414 1.415L23 24.415l7.071 7.07z"
        clipRule="evenodd"
      />
    </svg>
  );
}

RectangleCloseIcon.propTypes = propTypes;
RectangleCloseIcon.defaultProps = defaultProps;

export default RectangleCloseIcon;
