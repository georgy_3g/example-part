import React from 'react';
import PropTypes from 'prop-types';
import Colors from 'src/styles/colors.json';

const picledBluewoodColor = Colors['$pickled-bluewood-color'];
const burntSiennaColor = Colors['$burnt-sienna-color'];

const propTypes = {
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
  primaryStroke: PropTypes.string,
  secondaryStroke: PropTypes.string,
};

const defaultProps = {
  className: '',
  width: '17',
  height: '17',
  primaryStroke: picledBluewoodColor,
  secondaryStroke: burntSiennaColor,
};

function HandsetIcon({
  width,
  height,
  className,
  primaryStroke,
  secondaryStroke,
}) {
  return (
    <svg
      className={className}
      width={width}
      height={height}
      fill="none"
      viewBox="0 0 17 17"
    >
      <path
        stroke={primaryStroke}
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth="1.5"
        d="M15.279 12.49v2.165a1.444 1.444 0 01-1.573 1.444 14.28 14.28 0 01-6.228-2.216 14.071 14.071 0 01-4.33-4.33A14.281 14.281 0 01.933 3.297a1.443 1.443 0 011.436-1.573h2.165a1.443 1.443 0 011.443 1.24c.091.694.26 1.374.505 2.029a1.443 1.443 0 01-.325 1.522l-.916.917a11.546 11.546 0 004.33 4.33l.916-.917a1.444 1.444 0 011.523-.325 9.262 9.262 0 002.028.506 1.443 1.443 0 011.24 1.465z"
      />
      <path
        stroke={secondaryStroke}
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth="1.5"
        d="M10.264 4.889a3.608 3.608 0 012.85 2.85m-2.85-5.737A6.495 6.495 0 0116 7.732"
      />
    </svg>
  );
}

HandsetIcon.propTypes = propTypes;
HandsetIcon.defaultProps = defaultProps;

export default HandsetIcon;
