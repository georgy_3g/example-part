import React from 'react';
import PropTypes from 'prop-types';
import Colors from 'src/styles/colors.json';

const white = Colors['$white-color'];
const green = Colors['$summer-green-color'];

const propTypes = {
  fill: PropTypes.string,
  fillPath: PropTypes.string,
  stroke: PropTypes.string,
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
};

const defaultProps = {
  fill: white,
  fillPath: green,
  stroke: green,
  className: '',
  width: '28',
  height: '28',
};

const BrushWithPlusIcon = ({
  width,
  height,
  fill,
  fillPath,
  stroke,
  className,
}) => {
  return (
    <svg
      className={className}
      width={width}
      height={height}
      fill="none"
      viewBox="0 0 28 28"
    >
      <g clipPath="url(#clip0_939_834)">
        <mask id="path-1-inside-1_939_834" fill={fill}>
          <path d="M4.53 20.987c.638-4.106 5.67-5.749 8.607-2.81 2.939 2.938 1.296 7.969-2.81 8.608l-4.387.682a1.833 1.833 0 01-2.093-2.093l.682-4.387z" />
        </mask>
        <path
          fill={fillPath}
          d="M5.94 27.467l-.307-1.976.307 1.976zm-1.41-6.48l-1.977-.308 1.976.308zm5.49 3.821l-4.387.683.614 3.952 4.388-.682-.615-3.953zm-4.197.873l.683-4.387-3.953-.615-.682 4.388 3.952.614zm-.19-.19a.167.167 0 01.19.19l-3.952-.614c-.4 2.564 1.812 4.775 4.376 4.376l-.614-3.952zm6.09-5.9c1.781 1.78.786 4.83-1.703 5.217l.615 3.953c5.723-.89 8.012-7.903 3.917-11.999l-2.829 2.829zm2.829-2.829c-4.096-4.095-11.109-1.806-11.999 3.917l3.953.615c.387-2.489 3.436-3.484 5.217-1.703l2.829-2.829z"
          mask="url(#path-1-inside-1_939_834)"
        />
        <path
          stroke={stroke}
          strokeWidth="2"
          d="M21.232 14.324c-2.087 2.087-3.84 3.83-5.4 4.74-.764.445-1.384.63-1.899.615-.479-.014-.984-.203-1.54-.76-.556-.555-.745-1.06-.759-1.54-.015-.514.17-1.134.616-1.898.91-1.56 2.652-3.313 4.74-5.4 2.087-2.087 3.84-3.83 5.4-4.74.764-.445 1.384-.63 1.898-.615.48.013.984.203 1.54.759s.746 1.06.76 1.54c.014.514-.17 1.134-.616 1.898-.91 1.56-2.652 3.313-4.74 5.4z"
        />
        <path
          fill={fillPath}
          d="M15 12.594a4.213 4.213 0 013.721 3.722l-1.488 1.488a4.213 4.213 0 00-3.722-3.721L15 12.594z"
        />
        <path
          stroke={stroke}
          strokeLinecap="round"
          strokeLinejoin="round"
          strokeWidth="2"
          d="M6 2v4m0 4V6m0 0h4-8"
        />
      </g>
      <defs>
        <clipPath id="clip0_939_834">
          <path fill={fill} d="M0 0H28V28H0z" />
        </clipPath>
      </defs>
    </svg>
  );
}

BrushWithPlusIcon.propTypes = propTypes;
BrushWithPlusIcon.defaultProps = defaultProps;

export default BrushWithPlusIcon;
