import React from 'react';
import PropTypes from 'prop-types';
import Colors from 'src/styles/colors.json';

const darkerSummerGreenColor = Colors['$darker-summer-green-color'];
const whiteColor = Colors['$white-color'];


const propTypes = {
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
  stroke: PropTypes.string,
  color: PropTypes.string,
};

const defaultProps = {
  className: '',
  width: '58',
  height: '58',
  stroke: darkerSummerGreenColor,
  color: whiteColor,
};

function PaintBrushIcon({
  width,
  height,
  className,
  stroke,
  color,
}) {
  return (
    <svg
      className={className}
      width={width}
      height={height}
      fill="none"
      viewBox="0 0 81 80"
    >
      <rect
        width="80"
        height="80"
        fill={stroke}
        rx="24"
        transform="matrix(-1 0 0 1 80.16 0)"
      />
      <mask id="path-2-inside-1_5681_223978" fill={color}>
        <path d="M22.87 36.052c.92 5.908 8.158 8.271 12.386 4.043 4.227-4.227 1.864-11.466-4.043-12.385l-5.053-.786-1.26-.196a2.637 2.637 0 00-3.011 3.012l.982 6.312z" />
      </mask>
      <path
        fill={color}
        d="M26.16 26.924l.338-2.174-.338 2.174zm-4.271 2.816l-2.174.338 2.174-.338zm2.174-.338l.981 6.312-4.347.677-.982-6.313 4.348-.676zm6.811.482l-5.052-.786.676-4.348 5.053.786-.677 4.348zm-5.052-.786l-1.26-.196.676-4.348 1.26.197-.676 4.347zm-6.107.98c-.504-3.236 2.287-6.027 5.523-5.523l-.676 4.347a.437.437 0 00-.5.5l-4.347.676zM33.7 38.54c2.954-2.955 1.303-8.014-2.826-8.656l.677-4.348c7.686 1.196 10.76 10.614 5.26 16.115L33.7 38.54zm3.111 3.111c-5.5 5.5-14.919 2.426-16.114-5.26l4.347-.677c.643 4.129 5.701 5.78 8.656 2.826l3.111 3.111z"
        mask="url(#path-2-inside-1_5681_223978)"
      />
      <path
        stroke={color}
        strokeWidth="2.2"
        d="M47.597 45.851c-2.99-2.99-5.549-5.536-7.839-6.872-1.128-.658-2.084-.956-2.912-.932-.789.022-1.594.34-2.445 1.191-.852.852-1.17 1.657-1.192 2.446-.023.828.274 1.784.932 2.912 1.336 2.29 3.882 4.848 6.873 7.838 2.99 2.991 5.548 5.537 7.838 6.873 1.128.658 2.084.955 2.912.932.79-.023 1.594-.34 2.446-1.192.85-.851 1.169-1.656 1.191-2.445.024-.828-.274-1.785-.932-2.912-1.336-2.29-3.882-4.848-6.872-7.839z"
      />
      <path
        fill={color}
        d="M38.39 48.585a6.061 6.061 0 005.354-5.355l-2.142-2.142a6.061 6.061 0 01-5.354 5.355l2.141 2.142z"
      />
      <path
        stroke={color}
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth="2.2"
        d="M57.335 22.243A4.23 4.23 0 0055.903 22a4.06 4.06 0 00-1.883.471 3.785 3.785 0 00-1.463 1.278 4.049 4.049 0 00-1.47-1.268 4.156 4.156 0 00-1.908-.45 3.876 3.876 0 00-1.416.258 4.06 4.06 0 00-1.338.822 3.956 3.956 0 00-.913 1.26 3.877 3.877 0 00-.084 3.027c.191.454.42.892.685 1.308.094.122.172.258.28.395a20.028 20.028 0 003.565 3.787l2.677 2.25 2.661-2.28a19.203 19.203 0 003.72-4.091l.25-.396c.192-.333.364-.679.513-1.034a3.887 3.887 0 00-.139-3.043 3.976 3.976 0 00-.943-1.25 4.083 4.083 0 00-1.362-.8z"
      />
    </svg>
  );
}

PaintBrushIcon.propTypes = propTypes;
PaintBrushIcon.defaultProps = defaultProps;

export default PaintBrushIcon;
