import React from 'react';
import PropTypes from 'prop-types';
import Colors from 'src/styles/colors.json';

const defaultStroke = Colors['$iceberg-blue-light-color'];

const propTypes = {
  stroke: PropTypes.string,
  strokeWidth: PropTypes.string,
  className: PropTypes.string,
  width: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  height: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
};

const defaultProps = {
  stroke: defaultStroke,
  strokeWidth: '6',
  className: '',
  width: '31',
  height: '56',
};

function ArrowSlideShowIcon({
  width,
  height,
  stroke,
  strokeWidth,
  className,
}) {
  return (
    <svg
      width={width}
      height={height}
      viewBox="0 0 31 56"
      fill="none"
      className={`svg ${className}`}
    >
      <path
        stroke={stroke}
        strokeLinecap="round"
        strokeWidth={strokeWidth}
        d="M3 53l23.515-23.515a2.1 2.1 0 000-2.97L3 3"
      />
    </svg>
  );
}

ArrowSlideShowIcon.propTypes = propTypes;
ArrowSlideShowIcon.defaultProps = defaultProps;

export default ArrowSlideShowIcon;
