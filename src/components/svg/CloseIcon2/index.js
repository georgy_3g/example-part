import React from 'react';
import PropTypes from 'prop-types';
import Colors from 'src/styles/colors.json';

const whiteColor = Colors['$white-color'];
const darkSlateBlueColor = Colors['$dark-slate-blue-color'];

const propTypes = {
  fill: PropTypes.string,
  crossFill: PropTypes.string,
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
};

const defaultProps = {
  fill: darkSlateBlueColor,
  crossFill: whiteColor,
  className: '',
  width: '30',
  height: '30',
};

const CloseIcon2 = ({
  width,
  height,
  fill,
  crossFill,
  className,
}) => {
  return (
    <svg
      className={className}
      width={width}
      height={height}
      x="0"
      y="0"
      viewBox="0 0 100 100"
    >
      <title>close button</title>
      <circle
        cx="50"
        cy="50"
        r="50"
        fill={fill}
      />
      <path
        d="M33.5 66.6L67 33"
        fill="none"
        stroke={crossFill}
        strokeWidth="4"
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeMiterlimit="10"
      />
      <path
        d="M33.5 33L67 66.6"
        fill="none"
        stroke={crossFill}
        strokeWidth="4"
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeMiterlimit="10"
      />
    </svg>
  )
}

CloseIcon2.propTypes = propTypes;
CloseIcon2.defaultProps = defaultProps;

export default CloseIcon2;
