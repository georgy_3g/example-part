import React from 'react';
import PropTypes from 'prop-types';
import Colors from 'src/styles/colors.json';

const borderColor = Colors['$athens-gray-color'];
const circleColor = Colors['$cadet-blue-color'];
const symbolColor = Colors['$pickled-bluewood-color'];

const propTypes = {
  width: PropTypes.string,
  height: PropTypes.string,
  className: PropTypes.string,
  borderStroke: PropTypes.string,
  borderStrokeWidth: PropTypes.string,
  circleStroke: PropTypes.string,
  circleStrokeWidth: PropTypes.string,
  symbolFill: PropTypes.string,
};

const defaultProps = {
  width: '60',
  height: '60',
  className: '',
  borderStroke: borderColor,
  borderStrokeWidth: 1.5,
  circleStroke: circleColor,
  circleStrokeWidth: 1.5,
  symbolFill: symbolColor,

};

function HintIcon({
  width,
  height,
  borderStroke,
  borderStrokeWidth,
  className,
  circleStroke,
  circleStrokeWidth,
  symbolFill,
}) {
  return (
    <svg
      width={width}
      height={height}
      fill="none"
      viewBox="0 0 60 60"
      className={className}
    >
      <rect
        width="58.5"
        height="58.5"
        x="0.75"
        y="0.75"
        fill="none"
        stroke={borderStroke}
        strokeWidth={borderStrokeWidth}
        rx="13.25"
      />
      <circle
        cx="30.001"
        cy="30.001"
        r="15.917"
        stroke={circleStroke}
        strokeWidth={circleStrokeWidth}
      />
      <rect
        width="3.333"
        height="11.667"
        x="28.334"
        y="26.666"
        fill={symbolFill}
        rx="1.56"
      />
      <rect
        width="3.333"
        height="3.333"
        x="28.334"
        y="21.666"
        fill={symbolFill}
        rx="1.56"
      />
    </svg>
  );
}

HintIcon.propTypes = propTypes;
HintIcon.defaultProps = defaultProps;

export default HintIcon;
