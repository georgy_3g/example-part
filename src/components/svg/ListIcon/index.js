import React from 'react';
import PropTypes from 'prop-types';
import Colors from 'src/styles/colors.json';

const blue = Colors['$pickled-bluewood-color'];

const propTypes = {
  stroke: PropTypes.string,
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
  fill: PropTypes.string,
};

const defaultProps = {
  stroke: blue,
  className: '',
  width: '10',
  height: '8',
  fill: '',
};

function ListIcon({
  className,
  stroke,
  width,
  height,
  fill,
}) {
  return (
    <svg
      className={className}
      width={width}
      height={height}
      fill={fill}
      viewBox="0 0 10 8"
    >
      <path
        stroke={stroke}
        strokeLinecap="round"
        strokeWidth="1.5"
        d="M1 1h8M1 4h4M1 7h6"
      />
    </svg>
  );
}

ListIcon.propTypes = propTypes;
ListIcon.defaultProps = defaultProps;

export default ListIcon;
