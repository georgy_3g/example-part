import React from 'react';
import PropTypes from 'prop-types';
import colors from 'src/styles/colors.json';

const propTypes = {
  fill: PropTypes.string,
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
};

const gray = colors['$silver-gray-color'];

const defaultProps = {
  fill: gray,
  className: '',
  width: '40',
  height: '40',
};

function MagnifyingPlus({
  width,
  height,
  fill,
  className,
}) {
  return (
    <svg
      className={className}
      width={width}
      height={height}
      version="1.1"
      viewBox="76 76 250 250"
    >
      <g>
        <path
          fill={fill}
          fillRule="evenodd"
          stroke="none"
          d="M166.8 81.848C114.002 88.137 77 129.157 77 181.4c0 56.257 43.343 99.6 99.6 99.6 24.534 0 44.447-6.939 62.94-21.933l2.941-2.384 32.859 32.84c18.456 18.445 33.354 33.024 33.986 33.259a7.907 7.907 0 0010.256-10.256c-.235-.632-14.831-15.547-33.299-34.025-25.614-25.63-32.792-33.025-32.481-33.466.219-.311 1.453-1.945 2.743-3.631 19.226-25.124 25.101-60.093 15.227-90.637C260.223 115.044 230.266 89.174 193 82.74c-4.623-.798-22.016-1.39-26.2-.892M188 97.851c57.357 7.407 90.414 71.33 63.356 122.514-31.907 60.359-117.605 60.359-149.512 0C70.066 160.252 120.114 89.084 188 97.851m-14.372 26.05c-4.274 2.179-4.428 3.173-4.428 28.554V173.2h-22.016c-21.192 0-22.074.03-23.577.796-4.258 2.172-5.439 8.289-2.294 11.872 2.281 2.597 1.619 2.532 25.674 2.532H168.8v21.813c0 24.055-.065 23.393 2.532 25.674 3.656 3.21 9.701 1.951 12.04-2.508.804-1.533.831-2.221.944-23.679l.116-22.1h22.09c24.74 0 23.548.134 26.025-2.926 2.088-2.58 1.964-7.208-.26-9.742-2.281-2.597-1.619-2.532-25.674-2.532H184.8v-21.774c0-14.412-.144-22.155-.425-22.9-1.448-3.832-6.916-5.779-10.747-3.825"
        />
      </g>
    </svg>
  );
}

MagnifyingPlus.propTypes = propTypes;
MagnifyingPlus.defaultProps = defaultProps;

export default MagnifyingPlus;
