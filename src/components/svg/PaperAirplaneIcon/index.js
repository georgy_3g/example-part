import React from 'react';
import PropTypes from 'prop-types';
import Colors from 'src/styles/colors.json';

const whiteColor = Colors['$white-color'];

const propTypes = {
  fill: PropTypes.string,
  stroke: PropTypes.string,
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
};

const defaultProps = {
  fill: 'none',
  stroke: whiteColor,
  className: '',
  width: '20',
  height: '20',
};

function PaperAirplaneIcon({
  fill,
  width,
  height,
  stroke,
  className,
}) {
  return (
    <svg
      className={className}
      width={width}
      height={height}
      fill={fill}
      viewBox="0 0 20 20"
    >
      <path
        stroke={stroke}
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth="2"
        d="M16.87 2.469l-8.62 8.614M17.353 1.343a.5.5 0 01.637.637l-5.24 14.973c-.3.854-1.49.903-1.858.075l-2.486-5.593a1 1 0 00-.507-.508L2.305 8.441c-.828-.368-.78-1.558.076-1.858l14.972-5.24z"
      />
    </svg>
  );
}
PaperAirplaneIcon.defaultProps = defaultProps;
PaperAirplaneIcon.propTypes = propTypes;

export default PaperAirplaneIcon;
