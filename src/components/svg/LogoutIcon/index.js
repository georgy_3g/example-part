import React from 'react';
import PropTypes from 'prop-types';
import Colors from 'src/styles/colors.json';

const color = Colors['$silver-color'];

const propTypes = {
  fill: PropTypes.string,
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
};

const defaultProps = {
  fill: color,
  className: '',
  width: '26',
  height: '26',
};

function LogoutIcon({
  width,
  height,
  fill,
  className,
}) {
  return (
    <svg
      className={className}
      width={width}
      height={height}
      fill="none"
      viewBox="0 0 26 26"
    >
      <rect width="13" height="2" x="9" y="12" fill={fill} rx="1" />
      <path
        fill={fill}
        d="M21 15.636v-5.272a.45.45 0 01.708-.368l3.765 2.635a.45.45 0 010 .738l-3.765 2.635a.45.45 0 01-.708-.368z"
      />
      <path
        stroke={fill}
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth="2"
        d="M17 6.889V6.5A4.5 4.5 0 0012.5 2h-7A4.5 4.5 0 001 6.5v13A4.5 4.5 0 005.5 24h7a4.5 4.5 0 004.5-4.5v-.389"
      />
    </svg>
  );
}

LogoutIcon.propTypes = propTypes;
LogoutIcon.defaultProps = defaultProps;

export default LogoutIcon;
