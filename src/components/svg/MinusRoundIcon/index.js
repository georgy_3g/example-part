import React from 'react';
import PropTypes from 'prop-types';
import Colors from 'src/styles/colors.json';

const mainColor = Colors['$white-color'];

const propTypes = {
  fill: PropTypes.string,
  stroke: PropTypes.string,
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
};

const defaultProps = {
  className: '',
  width: '24',
  height: '24',
  stroke: mainColor,
  fill: 'none',
};

function MinusRoundIcon(props) {
  const { stroke, fill, width, height, className } = props;
  return (
    <svg className={className} height={height} width={width} viewBox="0 0 24 24">
      <path fill={fill} d="M0 0h24v24H0z" />
      <path
        fill={stroke}
        d="M7 11v2h10v-2H7zm5-9C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm0 18c-4.41 0-8-3.59-8-8s3.59-8 8-8 8 3.59 8 8-3.59 8-8 8z"
      />
    </svg>
  );
}

MinusRoundIcon.propTypes = propTypes;
MinusRoundIcon.defaultProps = defaultProps;

export default MinusRoundIcon;
