import React from 'react';
import PropTypes from 'prop-types';
import Colors from 'src/styles/colors.json';

const white = Colors['$white-color'];
const green = Colors['$summer-green-color'];

const propTypes = {
  fill: PropTypes.string,
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
};

const defaultProps = {
  fill: 'none',
  className: '',
  width: '24',
  height: '24',
};

function BellIcon({
  width,
  height,
  fill,
  className,
}) {
  return (
    <svg
      width={width}
      height={height}
      fill={fill}
      viewBox="0 0 24 24"
      className={className}
    >
      <path
        stroke={white}
        strokeWidth="1.5"
        d="M5.312 10.033a6.701 6.701 0 0113.376 0l.43 6.889a1.25 1.25 0 01-1.247 1.328H6.13a1.25 1.25 0 01-1.248-1.328l.43-6.889z"
      />
      <mask id="path-2-inside-1" fill={white}>
        <path d="M3 18.25a.75.75 0 01.75-.75h16.438a.75.75 0 010 1.5H3.75a.75.75 0 01-.75-.75z" />
      </mask>
      <path
        fill={white}
        d="M3.75 19h16.438v-3H3.75v3zm16.438-1.5H3.75v3h16.438v-3zm-16.438 0a.75.75 0 01.75.75h-3a2.25 2.25 0 002.25 2.25v-3zm15.69.69a.75.75 0 01.748-.69v3a2.25 2.25 0 002.243-2.071l-2.99-.239zm.748.81a.75.75 0 01-.748-.81l2.99.239A2.25 2.25 0 0020.189 16v3zM3.75 16a2.25 2.25 0 00-2.25 2.25h3a.75.75 0 01-.75.75v-3z"
        mask="url(#path-2-inside-1)"
      />
      <path fill={white} d="M15 20c0 1.105-1.343 2-3 2s-3-.895-3-2h6z" />
      <circle
        cx="17"
        cy="6"
        r="2.25"
        fill={green}
        stroke={white}
        strokeWidth="1.5"
      />
    </svg>
  );
}

BellIcon.propTypes = propTypes;
BellIcon.defaultProps = defaultProps;

export default BellIcon;
