import React from 'react';
import PropTypes from 'prop-types';
import Colors from 'src/styles/colors.json';

const AturquoiseColor = Colors['$turquoise-color'];

const propTypes = {
  fill: PropTypes.string,
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
};

const defaultProps = {
  fill: AturquoiseColor,
  className: '',
  width: '50',
  height: '37',
};

function Quotes({
  width,
  height,
  fill,
  className,
}) {
  return (
    <svg className={className} width={width} height={height} viewBox="0 0 50 37">
      <path
        fill={fill}
        d="M11.52 36.84c-3.68 0-6.52-1.3-8.52-3.9-2-2.6-3-5.98-3-10.14 0-4.88 1.3-9.34 3.9-13.38C6.5 5.38 10.32 2.24 15.36 0l7.56 5.76c-3.52 1.28-6.48 3.02-8.88 5.22-2.4 2.2-4 4.66-4.8 7.38l.48.36c.8-.8 1.96-1.2 3.48-1.2 2.32 0 4.36.8 6.12 2.4 1.76 1.6 2.64 3.88 2.64 6.84 0 3.04-.96 5.48-2.88 7.32-1.92 1.84-4.44 2.76-7.56 2.76zm26.88 0c-3.68 0-6.52-1.3-8.52-3.9-2-2.6-3-5.98-3-10.14 0-4.88 1.3-9.34 3.9-13.38C33.38 5.38 37.2 2.24 42.24 0l7.56 5.76c-3.52 1.28-6.48 3.02-8.88 5.22-2.4 2.2-4 4.66-4.8 7.38l.48.36c.8-.8 1.96-1.2 3.48-1.2 2.32 0 4.36.8 6.12 2.4 1.76 1.6 2.64 3.88 2.64 6.84 0 3.04-.96 5.48-2.88 7.32-1.92 1.84-4.44 2.76-7.56 2.76z"
      />
    </svg>
  );
}

Quotes.propTypes = propTypes;
Quotes.defaultProps = defaultProps;

export default Quotes;
