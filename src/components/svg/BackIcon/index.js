import React from 'react';
import PropTypes from 'prop-types';
import Colors from 'src/styles/colors.json';

const grayLightColor = Colors['$gray-light-color'];
const lightSlateGrayColor = Colors['$light-slate-gray-color'];

const propTypes = {
  fill: PropTypes.string,
  stroke: PropTypes.string,
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
};

const defaultProps = {
  fill: grayLightColor,
  stroke: lightSlateGrayColor,
  className: '',
  width: '32',
  height: '32',
};

function BackIcon({
  width,
  height,
  fill,
  stroke,
  className,
}) {
  return (
    <svg
      className={className}
      height={height}
      width={width}
      fill="none"
      viewBox="0 0 32 32"
    >
      <circle cx="16" cy="16" r="16" fill={fill} />
      <path
        stroke={stroke}
        strokeLinecap="round"
        strokeWidth="2.5"
        d="M18 10l-4.515 4.515a2.1 2.1 0 000 2.97L18 22"
      />
    </svg>
  );
}

BackIcon.propTypes = propTypes;
BackIcon.defaultProps = defaultProps;

export default BackIcon;
