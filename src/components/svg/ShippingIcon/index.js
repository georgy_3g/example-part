import React from 'react';
import PropTypes from 'prop-types';
import Colors from 'src/styles/colors.json';

const white = Colors['$white-color'];
const blue = Colors['$pickled-bluewood-color'];

const propTypes = {
  fill: PropTypes.string,
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
  type: PropTypes.string,
};

const defaultProps = {
  fill: 'none',
  className: '',
  width: '24',
  height: '24',
  type: '',
};

function ShippingIcon({ fill, className, width, height, type }) {
  const switchType = () => {
    switch (type) {
      case 'sent':
        return (
          <path
            stroke={blue}
            strokeLinecap="round"
            strokeLinejoin="round"
            strokeWidth="1.5"
            d="M16 18.5l1.302 1 2.698-3"
          />
        );
      case 'success':
        return (
          <path
            stroke={blue}
            strokeLinecap="round"
            strokeLinejoin="round"
            strokeWidth="1.5"
            d="M20.5 18h-5m5 0l-2-2m2 2l-2 2"
          />
        );
      case 'back':
        return (
          <path
            stroke={blue}
            strokeLinecap="round"
            strokeLinejoin="round"
            strokeWidth="1.5"
            d="M15.5 18h5m-5 0l2-2m-2 2l2 2"
          />
        );
      default:
        return null;
    }
  };
  return (
    <svg className={className} width={width} height={height} fill={fill} viewBox="0 0 24 24">
      <path
        stroke={blue}
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth="1.5"
        d="M3 6.649l8.382-3.493a1 1 0 01.752-.007l4.244 1.675M3 6.65v10.029a1 1 0 00.624.926L12 21M3 6.649L7.622 8.35M12 21l8.376-3.396a1 1 0 00.624-.926V6.648M12 21V10.297m9-3.648l-4.622-1.825M21 6.65l-9 3.648m4.378-5.473L7.622 8.351m0 0L12 10.297"
      />
      <circle
        r="5.25"
        fill={white}
        stroke={blue}
        strokeLinecap="round"
        strokeWidth="1.5"
        transform="matrix(-1 0 0 1 18 18)"
      />
      {switchType()}
    </svg>
  );
}

ShippingIcon.propTypes = propTypes;
ShippingIcon.defaultProps = defaultProps;

export default ShippingIcon;
