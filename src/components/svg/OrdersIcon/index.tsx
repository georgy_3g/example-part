import React, { FunctionComponent } from 'react';
import Colors from 'src/styles/colors.json';
import { Props } from './interface';

const summerGreenColor = Colors['$summer-green-color'];
const slateGrayColor = Colors['$slate-gray-color-2'];

const defaultProps = {
  className: '',
  width: '64',
  height: '64',
  strokePath: slateGrayColor,
  strokePathSecond: summerGreenColor,
  strokeRect: slateGrayColor,
  strokeRectSecond: summerGreenColor,
};

const OrdersIcon:FunctionComponent<Props> = ({
  className,
  width,
  height,
  strokePath,
  strokePathSecond,
  strokeRect,
  strokeRectSecond,
}) => (
  <svg
    className={className}
    width={width}
    height={height}
    fill="none"
    viewBox="0 0 64 64"
  >
    <title>orders page</title>
    <g strokeWidth="2" opacity="0.75">
      <rect
        width="22"
        height="10"
        x="10"
        y="16"
        stroke={strokeRectSecond}
        rx="3"
      />
      <rect
        width="22"
        height="10"
        x="10"
        y="31"
        stroke={strokeRect}
        rx="3"
      />
      <path
        stroke={strokePathSecond}
        d="M10 49a3 3 0 013-3h16a3 3 0 013 3v5H10v-5z"
      />
      <rect
        width="56"
        height="44"
        x="4"
        y="10"
        stroke={strokeRect}
        rx="7"
      />
      <path
        stroke={strokePath}
        strokeLinecap="round"
        strokeLinejoin="round"
        d="M38 15v34"
      />
      <path
        stroke={strokePathSecond}
        strokeLinecap="round"
        strokeLinejoin="round"
        d="M44 21h10M44 28h10M44 35h10M44 42h10"
      />
    </g>
  </svg>
);

OrdersIcon.defaultProps = defaultProps;

export default OrdersIcon;
