import React from 'react';
import PropTypes from 'prop-types';
import colors from 'src/styles/colors.json';

const darkGrayColor = colors['$pickled-bluewood-color'];
const grayColor = colors['$darker-cadet-blue-color'];
const whiteColor = colors['$white-color'];

const defaultProps = {
  className: '',
  width: '40',
  height: '40',
  color1: darkGrayColor,
  color2: grayColor,
};

const propTypes = {
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
  color1: PropTypes.string,
  color2: PropTypes.string,
};

function DragButton(props) {
  const { className, width, height, color1, color2 } = props;

  return (
    <svg width={width} height={height} fill="none" viewBox="15 15 40 40" className={className}>
      <g filter="url(#filter0_d)">
        <rect width="40" height="40" x="15" y="15" fill={whiteColor} rx="12" />
      </g>
      <path
        d="M25 38L22.7071 35.7071C22.3166 35.3166 22.3166 34.6834 22.7071 34.2929L25 32"
        stroke={color1}
        strokeWidth="2"
        strokeLinecap="round"
      />
      <path
        d="M45 38L47.2929 35.7071C47.6834 35.3166 47.6834 34.6834 47.2929 34.2929L45 32"
        stroke={color1}
        strokeWidth="2"
        strokeLinecap="round"
      />
      <path
        opacity="0.4"
        d="M32 29H38"
        stroke={color2}
        strokeWidth="2"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        opacity="0.4"
        d="M32 35H38"
        stroke={color2}
        strokeWidth="2"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        opacity="0.4"
        d="M32 41H38"
        stroke={color2}
        strokeWidth="2"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
}

DragButton.propTypes = propTypes;
DragButton.defaultProps = defaultProps;

export default DragButton;
