import React from 'react';
import PropTypes from 'prop-types';
import Colors from 'src/styles/colors.json';

const grayColor = Colors['$athens-gray-color'];
const blueColor = Colors['$darker-cadet-blue-color'];

const propTypes = {
  stroke: PropTypes.string,
  stroke2: PropTypes.string,
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
};

const defaultProps = {
  stroke: grayColor,
  stroke2: blueColor,
  className: '',
  width: '60',
  height: '52',
};

function PhotoIcon({
  width,
  height,
  stroke,
  stroke2,
  className,
}) {
  return (
    <svg
      className={className}
      width={width}
      height={height}
      fill="none"
      viewBox="0 0 60 52"
    >
      <rect
        width="58"
        height="50"
        x="1"
        y="1"
        stroke={stroke}
        strokeWidth="2"
        rx="11"
      />
      <path
        stroke={stroke2}
        strokeLinecap="round"
        strokeWidth="2"
        d="M11.469 32.176l8.802-12.429a2 2 0 012.843-.435l4.237 3.223M24.707 29.529l4.61-6.968a2 2 0 012.943-.437l3.918 3.242"
      />
      <circle
        cx="44.559"
        cy="14.97"
        r="4.735"
        stroke={stroke2}
        strokeWidth="2"
      />
      <path
        stroke={stroke2}
        strokeLinecap="round"
        strokeWidth="2"
        d="M9.824 39.676H41.59"
      />
    </svg>
  );
}

PhotoIcon.propTypes = propTypes;
PhotoIcon.defaultProps = defaultProps;

export default PhotoIcon;
