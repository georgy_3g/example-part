import React from 'react';
import PropTypes from 'prop-types';
import colors from 'src/styles/colors.json';

const blue = colors['$pickled-bluewood-color'];

const propTypes = {
  fill: PropTypes.string,
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
};

const defaultProps = {
  fill: blue,
  className: '',
  width: '14',
  height: '14',
};

function FilterIcon({
  width,
  height,
  fill,
  className,
}) {
  return (
    <svg
      width={width}
      height={height}
      className={className}
      fill="none"
      viewBox="0 0 14 14"
    >
      <rect width="8" height="2" x="6" fill={fill} rx="1" />
      <rect width="3" height="2" fill={fill} rx="1" />
      <rect width="8" height="2" x="6" y="6" fill={fill} rx="1" />
      <rect width="3" height="2" y="6" fill={fill} rx="1" />
      <rect width="8" height="2" x="6" y="12" fill={fill} rx="1" />
      <rect width="3" height="2" y="12" fill={fill} rx="1" />
    </svg>
  );
}

FilterIcon.propTypes = propTypes;
FilterIcon.defaultProps = defaultProps;

export default FilterIcon;
