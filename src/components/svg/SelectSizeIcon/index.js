import React from 'react';
import PropTypes from 'prop-types';
import Colors from 'src/styles/colors.json';

const grayColor = Colors['$slate-gray-color'];
const greenColor = Colors['$summer-green-color'];

const propTypes = {
  stroke: PropTypes.string,
  stroke2: PropTypes.string,
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
};

const defaultProps = {
  stroke: grayColor,
  stroke2: greenColor,
  className: '',
  width: '61',
  height: '54',
};

function SelectSizeIcon({
  width,
  height,
  stroke,
  stroke2,
  className,
}) {
  return (
    <svg
      width={width}
      height={height}
      fill="none"
      className={className}
      viewBox="0 0 61 54"
    >
      <g opacity="0.8">
        <rect
          width="52"
          height="45"
          x="1"
          y="1"
          stroke={stroke}
          strokeWidth="2"
          rx="7"
        />
        <rect width="2" height="43" x="57" y="2" fill={stroke2} rx="1" />
        <path
          stroke={stroke2}
          strokeLinecap="round"
          strokeLinejoin="round"
          strokeWidth="2"
          d="M60 3l-2-2-2 2M60 44l-2 2-2-2"
        />
        <rect
          width="2"
          height="50"
          x="2"
          y="52"
          fill={stroke2}
          rx="1"
          transform="rotate(-90 2 52)"
        />
        <path
          stroke={stroke2}
          strokeLinecap="round"
          strokeLinejoin="round"
          strokeWidth="2"
          d="M51 49l2 2-2 2M3 49l-2 2 2 2M9 15.734v-5.3a1 1 0 011-1h5.3M45 31.934v5.3a1 1 0 01-1 1h-5.3"
        />
        <path
          stroke={stroke}
          strokeLinecap="round"
          strokeLinejoin="round"
          strokeWidth="2"
          d="M34.5 21.535l-3 3.965-3-4-3 4-3-4-3 4"
        />
      </g>
    </svg>
  );
}

SelectSizeIcon.propTypes = propTypes;
SelectSizeIcon.defaultProps = defaultProps;

export default SelectSizeIcon;
