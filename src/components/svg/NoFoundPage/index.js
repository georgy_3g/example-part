import React from 'react';
import PropTypes from 'prop-types';
import Color from 'src/styles/colors.json';

const white = Color['$white-color'];
const grey = Color['$shark-gray-color'];
const black = Color['$black-color'];
const blue = Color['$powder-blue-color'];

const propTypes = {
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
  statusCode: PropTypes.number,
};

const defaultProps = {
  className: '',
  width: '180',
  height: '180',
  statusCode: 404,
};

function NoFoundPage({
  className,
  width,
  height,
  statusCode,
}) {
  return (
    <svg
      width={width}
      height={height}
      fill="none"
      viewBox="0 0 180 180"
      className={className}
    >
      <rect
        width="118.8"
        height="104.4"
        x="19.8"
        y="39.601"
        fill={white}
        stroke={grey}
        strokeWidth="3.6"
        rx="12.6"
      />
      <rect
        width="122.4"
        height="108"
        x="18"
        y="37.801"
        fill={white}
        rx="14.4"
      />
      <rect
        width="120.4"
        height="106"
        x="31.6"
        y="51.398"
        fill={blue}
        stroke={grey}
        strokeWidth="2"
        rx="13.4"
      />
      <rect
        width="120.4"
        height="106"
        x="19"
        y="38.801"
        fill={white}
        stroke={grey}
        strokeWidth="2"
        rx="13.4"
      />
      <circle
        cx="127"
        cy="50"
        r="3"
        stroke={grey}
        strokeWidth="2"
      />
      <circle cx="115" cy="50" r="3" stroke={grey} strokeWidth="2" />
      <text x="55" y="100" fill={black} fontSize="30">
        {statusCode}
      </text>
      <circle cx="103" cy="50" r="3" stroke={grey} strokeWidth="2" />
      <circle
        cx="130.5"
        cy="135.5"
        r="25.5"
        fill={white}
        stroke={grey}
        strokeWidth="2"
      />
      <ellipse cx="137.5" cy="143" fill="#F09B92" rx="26.5" ry="27" />
      <circle
        cx="130.5"
        cy="135.5"
        r="25.5"
        stroke={grey}
        strokeWidth="2"
      />
      <path
        fill={grey}
        fillRule="evenodd"
        d="M138.495 145.068a.999.999 0 101.414-1.414l-8.365-8.365 8.365-8.365a.999.999 0 10-1.414-1.414l-8.365 8.365-7.681-7.681a.999.999 0 10-1.414 1.414l7.681 7.681-7.681 7.681a.999.999 0 101.414 1.414l7.681-7.681 8.365 8.365z"
        clipRule="evenodd"
      />
    </svg>
  );
}

NoFoundPage.propTypes = propTypes;
NoFoundPage.defaultProps = defaultProps;

export default NoFoundPage;
