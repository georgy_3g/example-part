import React, { FunctionComponent} from 'react';
import Colors from 'src/styles/colors.json';
import { Props } from './interface';

const summerGreenColor = Colors['$summer-green-color'];
const slateGrayColor = Colors['$slate-gray-color-2'];
const blueColor = Colors['$pickled-bluewood-color'];
const darkerBlueColor = Colors['$darker-cadet-blue-color'];
const whiteColor = Colors['$white-color'];

const defaultProps = {
  className: '',
  width: '50',
  height: '50',
  isSelect: false,
  
};

const CoastalIcon:FunctionComponent<Props> = ({
  className,
  width,
  height,
  isSelect,
}) => (
  <svg
    width={width}
    height={height}
    fill="none"
    viewBox="0 0 50 50"
    className={className}
  >
    <g strokeWidth="1.667" clipPath="url(#clip0_6469_242334)">
      <path
        stroke={isSelect ? blueColor : darkerBlueColor}
        strokeLinecap="round"
        d="M17.504 43.287h18.33m5.808 0h4.973m-14.12-6.1h16.697m-22.468 0H18.81"
      />
      <path
        stroke={isSelect ? summerGreenColor : darkerBlueColor}
        strokeLinecap="round"
        d="M36.668.832v3.333m0 19.167v3.333M27.504 4.998l2.357 2.357m13.553 13.553l2.357 2.357M23.336 14.166h3.333m19.167 0h3.333M27.504 23.332l2.357-2.357M43.414 7.422l2.357-2.357"
      />
      <circle cx="36.253" cy="13.749" r="4.583" stroke={isSelect ? slateGrayColor : darkerBlueColor} />
      <path
        stroke={isSelect ? summerGreenColor : darkerBlueColor}
        strokeLinecap="round"
        d="M.836 34.166h4.266c1.04 0 2.078.128 3.088.382l.58.146c.328.083.647.193.955.331l.345.154a4.1 4.1 0 011.425 1.06l.147.169c.334.386.57.846.689 1.342v0c.122.509.116 1.04-.016 1.546l-.165.63-.625 1.638-.262 1.047c-.083.335-.126.68-.126 1.025v.24c0 .45.097.895.285 1.304v0c.21.456.525.855.921 1.163l.543.422c.399.31.835.568 1.299.767l1.306.562.551.163a18.34 18.34 0 003.178.641l.128.014 1.983.173 2.78.108m6.093 0h13.101"
      />
    </g>
    <defs>
      <clipPath id="clip0_6469_242334">
        <path fill={whiteColor} d="M0 0H50V50H0z" />
      </clipPath>
    </defs>
  </svg>
);

CoastalIcon.defaultProps = defaultProps;

export default CoastalIcon;