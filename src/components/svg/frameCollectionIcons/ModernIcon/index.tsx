import React, { FunctionComponent} from 'react';
import Colors from 'src/styles/colors.json';
import { Props } from './interface';

const summerGreenColor = Colors['$summer-green-color'];
const slateGrayColor = Colors['$slate-gray-color-2'];
const blueColor = Colors['$pickled-bluewood-color'];
const darkerBlueColor = Colors['$darker-cadet-blue-color'];
const whiteColor = Colors['$white-color'];

const defaultProps = {
  className: '',
  width: '50',
  height: '50',
  isSelect: false,
  
};

const RetroIcon:FunctionComponent<Props> = ({
  className,
  width,
  height,
  isSelect,
}) => (
  <svg
    className={className}
    width={width}
    height={height}
    fill="none"
    viewBox="0 0 50 50"
  >
    <g clipPath="url(#clip0_6469_242378)">
      <path
        stroke={isSelect ? summerGreenColor : darkerBlueColor}
        strokeLinecap="round"
        strokeWidth="1.667"
        d="M16.996 38.206a11.854 11.854 0 1112.71-11.29"
      />
      <path
        stroke={isSelect ? blueColor : darkerBlueColor}
        strokeLinecap="round"
        strokeWidth="1.667"
        d="M14.51 32.985a7.408 7.408 0 013.536-14.009 7.406 7.406 0 017.202 7.94"
      />
      <path
        fill={isSelect ? blueColor : darkerBlueColor}
        d="M19.565 11.533a.833.833 0 001.667 0h-1.667zm13.207 12.879a.833.833 0 000 1.666v-1.666zm-6.54-21.176h11.176V1.569H26.232v1.667zm16.176 5v11.176h1.666V8.236h-1.666zm-21.176 3.297V8.236h-1.667v3.297h1.667zm16.176 12.879h-4.636v1.666h4.636v-1.666zm5-5a5 5 0 01-5 5v1.666a6.667 6.667 0 006.666-6.666h-1.666zm-5-16.176a5 5 0 015 5h1.666a6.667 6.667 0 00-6.666-6.667v1.667zM26.232 1.569a6.667 6.667 0 00-6.667 6.667h1.667a5 5 0 015-5V1.569z"
      />
      <path
        fill={isSelect ? slateGrayColor : darkerBlueColor}
        d="M23.577 12.336a.833.833 0 001.666 0h-1.666zm8.861 8.062a.833.833 0 000 1.667v-1.667zM27.743 7.245h8.153V5.58h-8.152v1.666zm10.653 2.5v8.153h1.667V9.745h-1.667zm-13.152 2.591v-2.59h-1.667v2.59h1.666zm10.652 8.062h-3.458v1.667h3.458v-1.667zm2.5-2.5a2.5 2.5 0 01-2.5 2.5v1.667a4.167 4.167 0 004.167-4.167h-1.667zm-2.5-10.653a2.5 2.5 0 012.5 2.5h1.667a4.167 4.167 0 00-4.167-4.166v1.666zM27.744 5.58a4.167 4.167 0 00-4.167 4.166h1.666a2.5 2.5 0 012.5-2.5V5.58z"
      />
      <path
        stroke={isSelect ? slateGrayColor : darkerBlueColor}
        strokeWidth="1.667"
        d="M25.663 47.316c.962 1.666 3.368 1.666 4.33 0l7.995-13.848c.962-1.666-.24-3.75-2.165-3.75h-15.99c-1.924 0-3.127 2.084-2.165 3.75l7.995 13.848z"
      />
      <path
        stroke={isSelect ? summerGreenColor : darkerBlueColor}
        strokeWidth="1.667"
        d="M27.11 42.583a.833.833 0 001.443 0l4.617-7.995a.833.833 0 00-.722-1.25h-9.233a.833.833 0 00-.721 1.25l4.616 7.995z"
      />
    </g>
    <defs>
      <clipPath id="clip0_6469_242378">
        <path fill={whiteColor} d="M0 0H50V50H0z" />
      </clipPath>
    </defs>
  </svg>
);

RetroIcon.defaultProps = defaultProps;

export default RetroIcon;
