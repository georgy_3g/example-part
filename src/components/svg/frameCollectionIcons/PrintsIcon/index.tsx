import React, { FunctionComponent} from 'react';
import Colors from 'src/styles/colors.json';
import { Props } from './interface';

const summerGreenColor = Colors['$summer-green-color'];
const slateGrayColor = Colors['$slate-gray-color-2'];
const blueColor = Colors['$pickled-bluewood-color'];
const darkerBlueColor = Colors['$darker-cadet-blue-color'];
const whiteColor = Colors['$white-color'];

const defaultProps = {
  className: '',
  width: '50',
  height: '50',
  isSelect: false,
  
};

const PrintsIcon:FunctionComponent<Props> = ({
  className,
  width,
  height,
  isSelect,
}) => (
  <svg
    className={className}
    width={width}
    height={height}
    fill="none"
    viewBox="0 0 50 50"
  >
    <g strokeWidth="1.667" clipPath="url(#clip0_6469_242362)">
      <path
        stroke={isSelect ? slateGrayColor : darkerBlueColor}
        strokeLinecap="round"
        d="M11.184 8.49L5.867 9.763A6.667 6.667 0 00.9 17.633l4.745 22.32a6.667 6.667 0 007.778 5.162L28.598 42.2"
      />
      <path
        stroke={isSelect ? summerGreenColor : darkerBlueColor}
        d="M5.694 19.504c.162 1.42 1.473 2.46 2.955 2.291 1.482-.17 2.523-1.48 2.36-2.9-.162-1.42-1.473-2.46-2.955-2.291-1.481.17-2.523 1.48-2.36 2.9z"
      />
      <path
        stroke={isSelect ? slateGrayColor : darkerBlueColor}
        strokeLinecap="round"
        d="M15.5 37l-4.415.575"
      />
      <rect
        width="33.803"
        height="34.188"
        x="14.743"
        y="2.227"
        stroke={isSelect ? blueColor : darkerBlueColor}
        rx="5.833"
        transform="rotate(9.5 14.743 2.227)"
      />
      <path
        stroke={isSelect ? slateGrayColor : darkerBlueColor}
        strokeLinecap="round"
        d="M17.389 24.42l6.618-6.436a1.206 1.206 0 011.72.04l2.236 2.38M26.41 23.485l3.523-3.651a1.206 1.206 0 011.78.048l2.042 2.359"
      />
      <path
        stroke={isSelect ? summerGreenColor : darkerBlueColor}
        d="M41.756 16.673c-.248 1.408-1.62 2.368-3.088 2.108-1.47-.259-2.429-1.63-2.18-3.037.248-1.408 1.619-2.368 3.088-2.108 1.469.259 2.428 1.63 2.18 3.037z"
      />
      <path
        stroke={isSelect ? summerGreenColor : darkerBlueColor}
        strokeLinecap="round"
        d="M16.322 29.4l19.022 3.507"
      />
    </g>
    <defs>
      <clipPath id="clip0_6469_242362">
        <path fill={whiteColor} d="M0 0H50V50H0z" />
      </clipPath>
    </defs>
  </svg>
);

PrintsIcon.defaultProps = defaultProps;

export default PrintsIcon;
