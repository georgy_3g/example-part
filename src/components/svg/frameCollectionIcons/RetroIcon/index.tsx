import React, { FunctionComponent} from 'react';
import Colors from 'src/styles/colors.json';
import { Props } from './interface';

const summerGreenColor = Colors['$summer-green-color'];
const slateGrayColor = Colors['$slate-gray-color-2'];
const blueColor = Colors['$pickled-bluewood-color'];
const darkerBlueColor = Colors['$darker-cadet-blue-color'];
const whiteColor = Colors['$white-color'];

const defaultProps = {
  className: '',
  width: '50',
  height: '50',
  isSelect: false,
  
};

const RusticIcon:FunctionComponent<Props> = ({
  className,
  width,
  height,
  isSelect,
}) => (
  <svg
    className={className}
    width={width}
    height={height}
    fill="none"
    viewBox="0 0 50 50"
  >
    <g clipPath="url(#clip0_6469_242351)">
      <path
        stroke={isSelect ? blueColor : darkerBlueColor}
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth="1.667"
        d="M10.164 45.288s3.583-8.702 16.38-20.987"
      />
      <path
        fill={isSelect ? summerGreenColor : darkerBlueColor}
        d="M44.97 16.624l-.463-.693.462.693zm-8.702 5.119l.202.808-.202-.808zm6.142 0l.769.322a.833.833 0 00-.666-1.15l-.103.828zm-28.116 8.715a.833.833 0 001.623.38l-1.623-.38zm4.976 6.783a.833.833 0 00.292 1.64l-.292-1.64zM46.497 7.258l-.833-.013.833.013zm-.833-.013c-.026 1.615-.127 3.736-.357 5.534-.115.902-.26 1.694-.433 2.292-.087.3-.175.526-.258.684-.09.171-.137.195-.109.176l.925 1.386c.309-.205.517-.513.662-.79.151-.291.275-.631.38-.99.21-.718.367-1.609.487-2.547.24-1.882.343-4.07.37-5.718l-1.667-.027zm-1.157 8.686c-.726.484-3.345 1.856-5.63 3.096-1.116.606-2.146 1.178-2.772 1.57-.157.098-.3.193-.417.278-.09.065-.251.186-.365.338a.946.946 0 00-.128.232.866.866 0 00-.025.536c.15.541.662.617.732.626.139.018.262.003.327-.006.08-.012.162-.03.24-.05l-.404-1.617a.936.936 0 01-.078.018c-.02.002.04-.01.133.003.025.003.513.064.656.582a.8.8 0 01-.118.674c-.054.073-.087.083.012.01a5.7 5.7 0 01.32-.212c.574-.36 1.559-.907 2.682-1.517 2.194-1.191 4.95-2.635 5.76-3.175l-.925-1.386zm-8.037 6.62c.701-.175 1.798-.222 2.95-.193 1.13.03 2.228.13 2.887.212l.206-1.654a33.141 33.141 0 00-3.05-.224c-1.17-.03-2.46.008-3.398.242l.405 1.617zm-20.553 8.286c1.178-5.038 3.717-10.615 8.39-15.083 4.662-4.458 11.499-7.861 21.37-8.487l-.105-1.664c-10.23.65-17.447 4.194-22.416 8.946-4.958 4.74-7.628 10.632-8.862 15.909l1.623.38zm25.725-9.416c-1.888 4.5-8.873 13.417-22.372 15.82l.292 1.64C33.68 36.369 41.097 27.028 43.18 22.066l-1.537-.645zM47.33 7.27a1.651 1.651 0 00-1.758-1.668l.105 1.664-.003-.001a.008.008 0 01-.003-.002.032.032 0 01-.006-.014l-.001-.005 1.666.027z"
      />
      <path
        fill={isSelect ? slateGrayColor : darkerBlueColor}
        d="M46.449 27.822a.833.833 0 10-1.537-.644l1.537.644zm-19.927 12.35a.833.833 0 00.292 1.641l-.292-1.64zm18.39-12.994c-1.544 3.681-7.282 11.017-18.39 12.994l.292 1.641c11.727-2.088 17.897-9.848 19.635-13.99l-1.537-.645zM37.187 3.406a.833.833 0 00.628 1.544l-.628-1.544zm9.284.678a.833.833 0 00.28-1.643l-.28 1.643zm-8.97.094l.313.772.006-.002a2.927 2.927 0 01.159-.06c.116-.042.29-.103.517-.174a18.182 18.182 0 011.91-.485c1.611-.32 3.781-.535 6.065-.145l.28-1.643c-2.546-.434-4.931-.192-6.668.153-.872.173-1.587.372-2.088.53a14.384 14.384 0 00-.789.274l-.012.005-.004.002h-.002l.313.773z"
      />
      <path
        stroke={isSelect ? slateGrayColor : darkerBlueColor}
        strokeLinecap="round"
        strokeWidth="1.667"
        d="M14.257 43.722c1.308.396 2.215.978 2.676 1.57.423.544.45 1.052.184 1.544-.307.565-1.044 1.176-2.252 1.641-1.188.457-2.666.706-4.196.69-1.531-.017-2.984-.298-4.127-.779-1.164-.488-1.832-1.105-2.083-1.655-.216-.475-.163-.99.326-1.55.522-.599 1.49-1.164 2.833-1.533"
      />
      <path
        stroke={isSelect ? blueColor : darkerBlueColor}
        strokeLinecap="round"
        strokeWidth="1.667"
        d="M26.178 2.845l1.179 1.178m1.178 1.179l-1.178-1.179m0 0l1.178-1.178m-1.178 1.178l-1.178 1.179M25.62 48.837l-.764-1.48m-.765-1.481l.765 1.48m0 0l-1.48.765m1.48-.765l1.48-.764"
      />
      <path
        stroke={isSelect ? summerGreenColor : darkerBlueColor}
        strokeLinecap="round"
        strokeWidth="1.667"
        d="M14.726 3.06l2.227 2.227m2.226 2.227l-2.226-2.227m0 0l2.226-2.226m-2.226 2.226l-2.227 2.227"
      />
      <path
        stroke={isSelect ? slateGrayColor : darkerBlueColor}
        strokeLinecap="round"
        strokeWidth="1.667"
        d="M12.789 14.37l1.771 1.024m1.772 1.023l-1.772-1.023m0 0l1.023-1.772m-1.023 1.772l-1.023 1.772"
      />
      <path
        stroke={isSelect ? summerGreenColor : darkerBlueColor}
        strokeLinecap="round"
        strokeWidth="1.667"
        d="M43.73 42.725l-1.892.78m-1.891.78l1.89-.78m0 0l.78 1.892m-.78-1.892l-.78-1.891"
      />
    </g>
    <defs>
      <clipPath id="clip0_6469_242351">
        <path fill={whiteColor} d="M0 0H50V50H0z" />
      </clipPath>
    </defs>
  </svg>
);

RusticIcon.defaultProps = defaultProps;

export default RusticIcon;
