import React, { FunctionComponent} from 'react';
import Colors from 'src/styles/colors.json';
import { Props } from './interface';

const summerGreenColor = Colors['$summer-green-color'];
const slateGrayColor = Colors['$slate-gray-color-2'];
const blueColor = Colors['$pickled-bluewood-color'];
const darkerBlueColor = Colors['$darker-cadet-blue-color'];

const defaultProps = {
  className: '',
  width: '50',
  height: '50',
  isSelect: false,
  
};

const ClassicIcon:FunctionComponent<Props> = ({
  className,
  width,
  height,
  isSelect,
}) => (
  <svg
    className={className}
    width={width}
    height={height}
    fill="none"
    viewBox="0 0 50 50"
  >
    <path
      stroke={isSelect ? summerGreenColor : darkerBlueColor}
      strokeLinecap="round"
      strokeWidth="1.667"
      d="M38.358 31.696V24.01m0 0l1.014.523a3.9 3.9 0 001.788.434v0c.317 0 .634-.04.942-.116l.158-.04a4.646 4.646 0 001.95-1.023v0c.297-.261.56-.56.78-.888l.035-.052c.375-.556.637-1.18.772-1.836l.024-.116c.075-.362.112-.73.112-1.1v-.172c0-.744-.175-1.476-.511-2.14v0a4.73 4.73 0 00-.62-.93l-.13-.153a6.38 6.38 0 00-1.294-1.155v0a6.38 6.38 0 00-1.495-.743l-.021-.008a6.553 6.553 0 00-2.123-.353h-1.38m0 9.868l-.125-.111a3.967 3.967 0 01-.907-1.183l-.079-.158a3.221 3.221 0 01-.342-1.444v0m-25.163 3.032l-.616.245a6.543 6.543 0 01-1.052.319l-.38.081c-.47.1-.955.12-1.432.06v0a4.258 4.258 0 01-2.076-.866l-.046-.036a4.471 4.471 0 01-.873-.906l-.112-.155a5.224 5.224 0 01-.826-1.757l-.01-.038a5.03 5.03 0 01-.104-1.994v0a5.03 5.03 0 01.666-1.855l.052-.087a5.54 5.54 0 011.357-1.525l.26-.201a5.54 5.54 0 011.2-.707v0a5.539 5.539 0 011.624-.422l1.56-.16h.86m-.052 10.004v7.55m0-7.55l.344-.313c.483-.44.825-1.01.985-1.644l.075-.296c.064-.255.074-.52.029-.779v0m28.897-2.212l-.702-.39a2.91 2.91 0 00-1.413-.366v0c-.47 0-.933.114-1.35.332l-.066.035a3.052 3.052 0 00-1.183 1.103l-.053.087c-.261.424-.4.913-.4 1.411v0M7.994 18.902l.217-.166a3 3 0 011.746-.614v0a3 3 0 011.262.244l.027.012a3.17 3.17 0 011.39 1.17v0c.238.365.404.78.478 1.21l.062.356m23.73 0h-23.73m-1.382-6.972v-2.011m0 2.011h26.564m0-2.011v2.011m-11.292 6.972v17.207a2.685 2.685 0 105.37 0V21.114s-.746-.01-1.666-.01h-2.037l-1.667.01z"
    />
    <path
      stroke={isSelect ? blueColor : darkerBlueColor}
      strokeLinecap="round"
      strokeWidth="1.667"
      d="M11.805 34.393V44.92c0 .92.746 1.667 1.666 1.667h3.545M38.36 34.393V44.92c0 .92-.746 1.667-1.666 1.667H22.272m-5.256 0v-17.38c0-.866.439-1.673 1.165-2.144v0a2.557 2.557 0 012.805.012l.078.052a2.7 2.7 0 011.209 2.251v17.209m-5.257 0h5.257"
    />
    <rect
      width="33.793"
      height="5.326"
      x="8.22"
      y="4.243"
      stroke={isSelect ? slateGrayColor : darkerBlueColor}
      strokeWidth="1.667"
      rx="2.663"
    />
  </svg>
);

ClassicIcon.defaultProps = defaultProps;

export default ClassicIcon;
