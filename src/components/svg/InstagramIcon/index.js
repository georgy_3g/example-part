import React from 'react';
import PropTypes from 'prop-types';
import Colors from 'src/styles/colors.json';

const whiteColor = Colors['$white-color'];

const propTypes = {
  fill: PropTypes.string,
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
};

const defaultProps = {
  fill: whiteColor,
  className: '',
  width: '46',
  height: '46',
};

function InstagramIcon({
  width,
  height,
  fill,
  className,
}) {
  return (
    <svg
      className={className}
      width={width}
      height={height}
      fill="none"
      viewBox="0 0 46 46"
    >
      <title>******* instagram page</title>
      <circle cx="23" cy="23" r="22.5" stroke={fill} />
      <path
        fill={fill}
        d="M23 20c-1.654 0-3 1.346-3 3s1.346 3 3 3 3-1.346 3-3-1.346-3-3-3z"
      />
      <path
        fill={fill}
        fillRule="evenodd"
        d="M30.186 15.814A6.132 6.132 0 0025.824 14h-5.648a6.132 6.132 0 00-4.362 1.814A6.132 6.132 0 0014 20.176v5.648c0 1.644.644 3.193 1.814 4.362A6.132 6.132 0 0020.176 32h5.648a6.132 6.132 0 004.362-1.814A6.132 6.132 0 0032 25.824v-5.648a6.132 6.132 0 00-1.814-4.362zm-7.188 11.948a4.77 4.77 0 01-4.764-4.764 4.77 4.77 0 014.764-4.764 4.77 4.77 0 014.764 4.764 4.77 4.77 0 01-4.764 4.764zm4.908-9.137a.531.531 0 00.528-.527.53.53 0 00-.528-.528.53.53 0 00-.527.528.53.53 0 00.527.527z"
        clipRule="evenodd"
      />
    </svg>
  );
}

InstagramIcon.propTypes = propTypes;
InstagramIcon.defaultProps = defaultProps;

export default InstagramIcon;
