import React from 'react';
import PropTypes from 'prop-types';
import Colors from 'src/styles/colors.json';

const propTypes = {
  fill: PropTypes.string,
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
};

const color = Colors['$darker-cadet-blue-color'];

const defaultProps = {
  fill: color,
  className: '',
  width: '26',
  height: '26',
};

function CardPayIcon({
  fill,
  className,
  width,
  height,
}) {
  return (
    <svg width={width} height={height} fill="none" viewBox="0 0 24 18" className={className}>
      <path
        fill={fill}
        fillRule="evenodd"
        d="M4 0a4 4 0 00-4 4h24a4 4 0 00-4-4H4zm20 8H0v6a4 4 0 004 4h16a4 4 0 004-4V8zM3 12.5a.5.5 0 01.5-.5h1a.5.5 0 01.5.5v1a.5.5 0 01-.5.5h-1a.5.5 0 01-.5-.5v-1zm4-.5a.5.5 0 00-.5.5v1a.5.5 0 00.5.5h1a.5.5 0 00.5-.5v-1A.5.5 0 008 12H7zm3 .5a.5.5 0 01.5-.5h1a.5.5 0 01.5.5v1a.5.5 0 01-.5.5h-1a.5.5 0 01-.5-.5v-1zm4-.5a.5.5 0 00-.5.5v1a.5.5 0 00.5.5h1a.5.5 0 00.5-.5v-1a.5.5 0 00-.5-.5h-1z"
        clipRule="evenodd"
      />
    </svg>
  );
}

CardPayIcon.propTypes = propTypes;
CardPayIcon.defaultProps = defaultProps;

export default CardPayIcon;
