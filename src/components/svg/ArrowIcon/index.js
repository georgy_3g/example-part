import React from 'react';
import PropTypes from 'prop-types';

const propTypes = {
  fill: PropTypes.string,
  stroke: PropTypes.string,
  strokeWidth: PropTypes.string,
  className: PropTypes.string,
  width: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  height: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
};

const defaultProps = {
  fill: 'none',
  stroke: '#123C55',
  strokeWidth: '2',
  className: '',
  width: '12',
  height: '22',
};

function ArrowIcon({
  width,
  height,
  stroke,
  strokeWidth,
  fill,
  className,
}) {
  return (
    <svg
      width={width}
      height={height}
      viewBox="0 0 12 22"
      className={`svg ${className}`}
    >
      <path
        fill={fill}
        fillRule="evenodd"
        stroke={stroke}
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth={strokeWidth}
        d="M1 1l10 10L1 21"
      />
    </svg>
  );
}

ArrowIcon.propTypes = propTypes;
ArrowIcon.defaultProps = defaultProps;

export default ArrowIcon;
