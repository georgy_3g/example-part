import React from 'react';
import PropTypes from 'prop-types';
import bem from 'src/utils/bem';
import Colors from 'src/styles/colors.json';
import styles from './index.module.scss';

const darkStateColor = Colors['$dark-slate-blue-color'];

const b = bem('dropdown-icon', styles);

const propTypes = {
  fill: PropTypes.string,
  stroke: PropTypes.string,
  strokeWidth: PropTypes.string,
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
  menuIsOpen: PropTypes.bool,
  viewBox: PropTypes.string,
};

const defaultProps = {
  fill: 'none',
  stroke: darkStateColor,
  strokeWidth: '2',
  className: '',
  width: '12',
  height: '22',
  menuIsOpen: false,
  viewBox: '0 0 12 22',
};

function DropdownIcon(props) {
  const { menuIsOpen, width, height, stroke, strokeWidth, fill, className, viewBox } = props;

  return (
    <svg
      className={b('caret', {
        right: className,
        up: menuIsOpen,
        down: !menuIsOpen,
      })}
      width={width}
      height={height}
      viewBox={viewBox}
    >
      <path
        fill={fill}
        fillRule="evenodd"
        stroke={stroke}
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth={strokeWidth}
        d="M1 1l10 10L1 21"
      />
    </svg>
  );
}

DropdownIcon.propTypes = propTypes;
DropdownIcon.defaultProps = defaultProps;

export default DropdownIcon;
