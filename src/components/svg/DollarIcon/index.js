import React from 'react';
import PropTypes from 'prop-types';
import Colors from 'src/styles/colors.json';

const redColor = Colors['$burnt-sienna-red-color'];
const whiteColor = Colors['$white-color'];

const propTypes = {
  stroke: PropTypes.string,
  fill: PropTypes.string,
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
};

const defaultProps = {
  stroke: whiteColor,
  fill: redColor,
  className: '',
  width: '30',
  height: '30',
};

function DollarIcon({
  width,
  height,
  stroke,
  fill,
  className,
}) {
  return (
    <svg
      width={width}
      height={height}
      fill="none"
      className={className}
      viewBox="0 0 30 30"
    >
      <rect
        width="30"
        height="30"
        fill={fill}
        rx="10"
        transform="matrix(-1 0 0 1 30 0)"
      />
      <path
        stroke={stroke}
        strokeLinecap="round"
        strokeWidth="1.5"
        d="M18.008 11.047l-1.552-1.273a2.27 2.27 0 00-3.22.346v0a2.27 2.27 0 00.39 3.203l1.733 1.344 1.619 1.219a2.416 2.416 0 01.492 3.365v0a2.416 2.416 0 01-3.387.504l-1.704-1.268M15.006 7.5v1.613M15.006 20.21v1.614"
      />
    </svg>
  );
}

DollarIcon.propTypes = propTypes;
DollarIcon.defaultProps = defaultProps;

export default DollarIcon;
