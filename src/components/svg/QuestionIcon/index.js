import React from 'react';
import PropTypes from 'prop-types';
import Colors from 'src/styles/colors.json';

const blackColor = Colors['$shark-gray-color'];
const redColor = Colors['$sea-pink-red-color'];

const propTypes = {
  stroke: PropTypes.string,
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
  fill: PropTypes.string,
  strokeWidth: PropTypes.string,
};

const defaultProps = {
  stroke: redColor,
  fill: blackColor,
  className: '',
  width: '24',
  height: '27',
  strokeWidth: '2',
};

function QuestionIcon({
  width,
  height,
  stroke,
  className,
  fill,
  strokeWidth,
}) {
  return (
    <svg className={className} width={width} height={height} fill="none" viewBox="0 0 24 27">
      <circle cx="12" cy="13" r="11" stroke={stroke} strokeWidth={strokeWidth} />
      <path
        fill={fill}
        d="M11.36 15.832v-.976c0-.288.021-.528.064-.72.043-.192.107-.363.192-.512.085-.16.197-.31.336-.448.15-.15.33-.325.544-.528l.512-.48c.17-.17.325-.341.464-.512a2.78 2.78 0 00.336-.56 1.55 1.55 0 00.144-.672c0-.245-.048-.47-.144-.672a1.597 1.597 0 00-.352-.544c-.15-.15-.33-.267-.544-.352a1.673 1.673 0 00-.656-.128c-.523 0-.95.165-1.28.496-.32.33-.512.763-.576 1.296l-1.632-.16c.139-.928.523-1.653 1.152-2.176.64-.533 1.43-.8 2.368-.8.448 0 .864.07 1.248.208.395.128.736.32 1.024.576.288.245.512.555.672.928.17.363.256.779.256 1.248 0 .405-.059.763-.176 1.072a3.267 3.267 0 01-.496.848l-.96.992-.16.144c-.17.16-.31.299-.416.416a1.581 1.581 0 00-.24.352c-.053.117-.09.25-.112.4-.021.15-.032.336-.032.56v.704H11.36zm-.304 2.208c0-.288.101-.533.304-.736a1.03 1.03 0 01.752-.32c.288 0 .533.107.736.32.213.203.32.448.32.736s-.107.539-.32.752a1.002 1.002 0 01-.736.304c-.288 0-.539-.101-.752-.304a1.056 1.056 0 01-.304-.752z"
      />
    </svg>
  );
}

QuestionIcon.propTypes = propTypes;
QuestionIcon.defaultProps = defaultProps;

export default QuestionIcon;
