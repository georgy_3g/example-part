import React from 'react';
import PropTypes from 'prop-types';
import Colors from 'src/styles/colors.json';

const greenColor = Colors['$summer-green-color'];
const blueColor = Colors['$pickled-bluewood-color'];

const propTypes = {
  stroke: PropTypes.string,
  stroke2: PropTypes.string,
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
  strokeWidth: PropTypes.string,
};

const defaultProps = {
  stroke: greenColor,
  stroke2: blueColor,
  className: '',
  width: '46',
  height: '46',
  strokeWidth: '2',
};

function MinDownloadIcon({
  width,
  height,
  stroke,
  stroke2,
  className,
  strokeWidth,
}) {
  return (
    <svg
      width={width}
      height={height}
      fill="none"
      viewBox="0 0 46 46"
      className={className}
    >
      <path
        stroke={stroke2}
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth={strokeWidth}
        d="M6.922 25.465v3.654a7.308 7.308 0 007.308 7.307h17.538a7.308 7.308 0 007.308-7.307v-3.654"
      />
      <path
        stroke={stroke}
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth={strokeWidth}
        d="M23 8.656v18.27m0 0l-7.309-7.308M23 26.926l7.308-7.308"
      />
    </svg>
  );
}

MinDownloadIcon.propTypes = propTypes;
MinDownloadIcon.defaultProps = defaultProps;

export default MinDownloadIcon;
