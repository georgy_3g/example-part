import React from 'react';
import PropTypes from 'prop-types';
import Colors from 'src/styles/colors.json';

const blue = Colors['$dark-slate-blue-color'];
const turquoise = Colors['$turquoise-color'];

const propTypes = {
  fill: PropTypes.string,
  fill2: PropTypes.string,
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
};

const defaultProps = {
  fill: blue,
  fill2: turquoise,
  className: '',
  width: '60',
  height: '60',
};

function AudioIcon({
  width,
  height,
  fill2,
  fill,
  className,
}) {
  return (
    <svg
      width={width}
      height={height}
      version="1.1"
      viewBox="0 0 400 400"
      className={className}
    >
      <g fillRule="evenodd" stroke="none">
        <path
          fill={fill2}
          d="M218.4 115.096c0 8.732-.313 8.104 4.035 8.104 11.408 0 27.523 5.349 38.566 12.8 42.431 28.633 46.116 88.823 7.507 122.62-12.483 10.927-31.46 18.58-46.073 18.58-4.348 0-4.035-.628-4.035 8.102v7.616l5.7-.23c43.98-1.767 81.208-37.23 86.324-82.234 5.957-52.399-34.513-100.563-86.324-102.737l-5.7-.239v7.618m-.4 27.2c0 8.632-.226 8.104 3.47 8.104 27.673 0 51.242 30.701 45.365 59.093-4.546 21.962-25.07 40.107-45.365 40.107-3.696 0-3.47-.528-3.47 8.104v7.618l4.9-.253c41.715-2.146 70.466-45.456 57.095-86.005-8.093-24.543-31.75-42.829-57.095-44.133l-4.9-.253v7.618m.491 28.204l.109 7.7 2.4.266c14.638 1.625 23.118 18.095 16.097 31.266-3.216 6.034-10.934 11.068-16.97 11.068H218.4v15.6h2.374c15.479 0 30.347-12.882 33.846-29.326 4.605-21.641-12.702-44.274-33.855-44.274h-2.382l.108 7.7"
        />
        <path
          fill={fill}
          d="M40.368 44.404C28.556 47.31 19.224 56.707 16.384 68.555c-1.222 5.095-1.222 256.995 0 262.09a32.736 32.736 0 0024.171 24.171c5.1 1.223 307.39 1.223 312.49 0a32.736 32.736 0 0024.171-24.171c1.222-5.095 1.222-256.995 0-262.09a32.736 32.736 0 00-24.171-24.171c-4.982-1.195-307.819-1.175-312.677.02m306.988 14.81c7.787 1.126 13.938 7.258 15.032 14.986.543 3.84.543 246.96 0 250.8-1.094 7.728-7.245 13.86-15.032 14.986-3.78.546-297.297.548-301.156.002-7.714-1.092-13.896-7.274-14.988-14.988-.543-3.84-.543-246.96 0-250.8 1.068-7.544 7.283-13.883 14.671-14.963 3.392-.497 298.044-.518 301.473-.023m-164.637 41.958c-4.645 1.486-5.135 1.939-44.211 40.964l-38.091 38.042-12.709-12.635c-9.855-9.798-13.053-12.745-14.245-13.131-2.331-.756-6.562-.387-8.366.728l-1.497.925v86.27l1.5.928c2.215 1.372 6.542 1.534 8.937.335 1.156-.579 5.858-4.96 13.666-12.734l11.903-11.852 38.497 38.447c25.276 25.243 39.154 38.822 40.411 39.54 6.62 3.782 15.29 3.497 21.563-.708l2.523-1.691V105.4l-2.523-1.691c-4.828-3.236-11.926-4.274-17.358-2.537"
        />
      </g>
    </svg>
  );
}

AudioIcon.propTypes = propTypes;
AudioIcon.defaultProps = defaultProps;

export default AudioIcon;
