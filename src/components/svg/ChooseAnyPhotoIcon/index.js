import React from 'react';
import PropTypes from 'prop-types';
import Colors from 'src/styles/colors.json';

const white = Colors['$white-color'];
const yellow = Colors['$sidecar-yellow-color'];
const black = Colors['$black-color'];
const parisWhite = Colors['$paris-white-color'];

const propTypes = {
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
};

const defaultProps = {
  className: '',
  width: '202',
  height: '163',
};

function ChooseAnyPhotoIcon({ className, width, height }) {
  return (
    <svg
      className={className}
      width={width}
      height={height}
      fill="none"
      viewBox="0 0 202 163"
    >
      <mask id="path-1-inside-1" fill={white}>
        <path
          fillRule="evenodd"
          d="M102.516 123.72a6.099 6.099 0 00-.872-.062c-3.23 0-5.85 2.514-5.85 5.615 0 3.101 2.62 5.614 5.85 5.614 2.459 0 4.563-1.455 5.429-3.517-2.818-.404-4.978-2.737-4.978-5.553 0-.741.149-1.449.421-2.097z"
          clipRule="evenodd"
        />
      </mask>
      <path
        fill={black}
        d="M102.516 123.72l1.856.779 1.008-2.401-2.578-.37-.286 1.992zm4.557 7.65l1.855.778 1.008-2.401-2.578-.37-.285 1.993zm-5.429-5.699c.201 0 .397.014.587.041l.571-3.984a8.2 8.2 0 00-1.158-.082v4.025zm-3.837 3.602c0-1.913 1.64-3.602 3.837-3.602v-4.025c-4.263 0-7.862 3.338-7.862 7.627h4.026zm3.837 3.602c-2.197 0-3.837-1.69-3.837-3.602h-4.025c0 4.289 3.599 7.627 7.862 7.627v-4.025zm3.573-2.284c-.548 1.305-1.915 2.284-3.573 2.284v4.025c3.259 0 6.101-1.933 7.284-4.752l-3.711-1.557zm2.141-1.214c-1.903-.273-3.251-1.82-3.251-3.56h-4.025c0 3.893 2.973 7.01 6.705 7.545l.571-3.985zm-3.251-3.56c0-.468.094-.911.265-1.318l-3.712-1.558a7.436 7.436 0 00-.578 2.876h4.025z"
        mask="url(#path-1-inside-1)"
      />
      <rect
        width="199.242"
        height="137.921"
        x="1.379"
        y="2.006"
        stroke={black}
        strokeWidth="2.013"
        rx="12.075"
      />
      <rect
        width="174.953"
        height="104.233"
        x="13.524"
        y="12.371"
        stroke={black}
        strokeWidth="2.013"
        rx="9.056"
      />
      <path
        stroke={black}
        strokeWidth="2.013"
        d="M86.335 139.928l-6.153 21.594h42.506l-6.345-21.594"
      />
      <rect
        width="87.615"
        height="55.282"
        x="61.963"
        y="48.51"
        fill={parisWhite}
        rx="6.038"
      />
      <path
        fill={yellow}
        fillRule="evenodd"
        stroke={black}
        strokeWidth="2.013"
        d="M150.131 133.756c-.948 0-1.37 1.191-.633 1.788l14.941 12.096c.369.299.897.299 1.267 0l14.941-12.096c.737-.597.315-1.788-.634-1.788h-6.665v-24.222a2.013 2.013 0 00-2.012-2.013h-12.465a2.013 2.013 0 00-2.013 2.013v24.222h-6.727z"
        clipRule="evenodd"
      />
      <rect
        width="85.603"
        height="53.27"
        x="56.897"
        y="43.467"
        stroke={black}
        strokeWidth="2.013"
        rx="5.031"
      />
      <path
        stroke={black}
        strokeLinecap="round"
        strokeWidth="2.013"
        d="M72.373 83.924l12.438-18.101a2.013 2.013 0 012.9-.444l6.349 4.977M90.59 80.468l6.25-10.07a2.013 2.013 0 013.041-.448l5.456 4.813"
      />
      <path
        stroke={black}
        strokeWidth="2.013"
        d="M122.547 61.034c0 3.495-2.846 6.336-6.367 6.336-3.52 0-6.367-2.841-6.367-6.336 0-3.496 2.847-6.336 6.367-6.336 3.521 0 6.367 2.84 6.367 6.336z"
      />
      <path
        stroke={black}
        strokeLinecap="round"
        strokeWidth="2.013"
        d="M68.904 87.81h57.254"
      />
    </svg>
  );
}

ChooseAnyPhotoIcon.propTypes = propTypes;
ChooseAnyPhotoIcon.defaultProps = defaultProps;

export default ChooseAnyPhotoIcon;
