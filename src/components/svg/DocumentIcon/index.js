import React from 'react';
import PropTypes from 'prop-types';
import Colors from 'src/styles/colors.json';

const gray = Colors['$shark-gray-color'];

const propTypes = {
  stroke: PropTypes.string,
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
};

const defaultProps = {
  stroke: gray,
  className: '',
  width: '26',
  height: '32',
};

function DocumentIcon({
  width,
  height,
  stroke,
  className,
}) {
  return (
    <svg
      width={width}
      height={height}
      fill="none"
      viewBox="0 0 26 32"
      className={className}
    >
      <rect
        width="9"
        height="1"
        x="6.5"
        y="13.5"
        fill={stroke}
        stroke={stroke}
        rx="0.5"
      />
      <rect
        width="12"
        height="1"
        x="6.5"
        y="18.5"
        fill={stroke}
        stroke={stroke}
        rx="0.5"
      />
      <rect
        width="7"
        height="1"
        x="6.5"
        y="23.5"
        fill={stroke}
        stroke={stroke}
        rx="0.5"
      />
      <path
        stroke={stroke}
        strokeWidth="2"
        d="M6.008 30H20a4 4 0 004-4V10.352a4 4 0 00-1.096-2.75l-4.122-4.353A4 4 0 0015.878 2H6.046a4 4 0 00-4 3.992l-.038 20a4 4 0 004 4.008z"
      />
      <path stroke={stroke} strokeWidth="2" d="M16 3v4a3 3 0 003 3h4" />
    </svg>
  );
}

DocumentIcon.propTypes = propTypes;
DocumentIcon.defaultProps = defaultProps;

export default DocumentIcon;
