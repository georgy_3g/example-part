import React from 'react';
import PropTypes from 'prop-types';
import Colors from 'src/styles/colors.json';

const darkGreenColor = Colors['$darker-summer-green-color'];

const propTypes = {
  fill: PropTypes.string,
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
  fillStroke: PropTypes.string,
};

const defaultProps = {
  fill: 'none',
  fillStroke: darkGreenColor,
  className: '',
  width: '16',
  height: '20',
};


function ClipIcon({
  width,
  height,
  fill,
  fillStroke,
  className,
}) {
  return (
    <svg
      className={className}
      width={width}
      height={height}
      fill={fill}
      viewBox="0 0 16 20"
    >
      <path
        fill={fillStroke}
        d="M14.265 2.608a5.67 5.67 0 00-8.009 0L.14 8.721a.21.21 0 00-.06.15c0 .056.02.11.06.15l.865.864a.21.21 0 00.298 0l6.117-6.112a3.994 3.994 0 012.843-1.177c1.073 0 2.083.417 2.84 1.177.76.76 1.177 1.77 1.177 2.84a3.989 3.989 0 01-1.177 2.841l-6.234 6.232-1.01 1.01a2.423 2.423 0 01-3.424 0 2.402 2.402 0 01-.708-1.71c0-.648.25-1.255.708-1.712l6.185-6.182a.827.827 0 01.583-.242h.003c.22 0 .424.087.579.242a.817.817 0 010 1.162l-5.056 5.05a.21.21 0 00-.06.15c0 .057.02.111.06.15l.865.866a.21.21 0 00.298 0l5.053-5.053a2.448 2.448 0 00.722-1.744c0-.659-.258-1.28-.722-1.744a2.473 2.473 0 00-3.492 0l-.6.602-5.583 5.58a4.037 4.037 0 00-1.19 2.877 4.06 4.06 0 004.066 4.06c1.04 0 2.08-.397 2.873-1.19l7.247-7.241a5.635 5.635 0 001.657-4.004 5.617 5.617 0 00-1.657-4.005z"
      />
    </svg>
  );
}

ClipIcon.propTypes = propTypes;
ClipIcon.defaultProps = defaultProps;

export default ClipIcon;
