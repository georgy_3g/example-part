import React from 'react';
import PropTypes from 'prop-types';
import Colors from 'src/styles/colors.json';

const blue = Colors['$pickled-bluewood-color'];
const green = Colors['$summer-green-color'];
const borderColor = Colors['$slate-gray-color'];
const gray = Colors['$silver-gray-color'];

const propTypes = {
  fill: PropTypes.string,
  fill2: PropTypes.string,
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
  disabled: PropTypes.bool,
  stroke: PropTypes.string,
};

const defaultProps = {
  fill: blue,
  fill2: green,
  className: '',
  width: '70',
  height: '70',
  disabled: false,
  stroke: borderColor,
};

function DownloadFileIcon({
  width,
  height,
  fill2,
  fill,
  className,
  disabled,
  stroke,
}) {
  return (
    <svg
      width={width}
      height={height}
      version="1.1"
      viewBox="0 0 70 70"
      className={className}
      fill="none"
    >
      <rect
        width="68"
        height="68"
        x="1"
        y="1"
        stroke={disabled ? gray : stroke}
        strokeWidth="2"
        rx="19"
      />
      <path
        stroke={disabled ? gray : fill}
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth="2"
        d="M18.922 37.465v3.654a7.308 7.308 0 007.308 7.307h17.538a7.308 7.308 0 007.308-7.307v-3.654"
      />
      <path
        stroke={disabled ? gray : fill2}
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth="2"
        d="M35 20.656v18.27m0 0l-7.309-7.308M35 38.926l7.308-7.308"
      />
    </svg>
  );
}

DownloadFileIcon.propTypes = propTypes;
DownloadFileIcon.defaultProps = defaultProps;

export default DownloadFileIcon;
