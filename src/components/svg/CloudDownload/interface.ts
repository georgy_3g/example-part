export interface Props {
  className: string;
  width?: string;
  height?: string;
}
