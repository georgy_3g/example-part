import React from 'react';
import PropTypes from 'prop-types';
import Colors from 'src/styles/colors.json';

const lightSlateGrayColor = Colors['$light-slate-gray-color'];

const propTypes = {
  fill: PropTypes.string,
  rectStroke: PropTypes.string,
  markStroke: PropTypes.string,
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
};

const defaultProps = {
  fill: 'none',
  rectStroke: lightSlateGrayColor,
  markStroke: 'none',
  className: '',
  width: '24',
  height: '24',
};

function CheckedIcon({
  width,
  height,
  fill,
  rectStroke,
  markStroke,
  className,
}) {
  return (
    <svg
      className={className}
      height={height}
      width={width}
      fill={fill}
      viewBox="0 0 24 24"
    >
      <rect
        width="22"
        height="22"
        x="1"
        y="1"
        stroke={rectStroke}
        strokeWidth="2"
        rx="4"
      />
      <path
        stroke={markStroke}
        strokeLinecap="round"
        strokeWidth="2"
        d="M6 13.343l2.817 2.872a1 1 0 001.48-.057L18 7"
      />
    </svg>
  );
}

CheckedIcon.propTypes = propTypes;
CheckedIcon.defaultProps = defaultProps;

export default CheckedIcon;
