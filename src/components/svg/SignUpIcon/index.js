import React from 'react';
import PropTypes from 'prop-types';
import Colors from 'src/styles/colors.json';

const whiteColor = Colors['$white-color'];
const burntSiennaColor = Colors['$burnt-sienna-color'];
const sharkGrayColor = Colors['$shark-gray-color'];
const blackColor = Colors['$black-color'];

const propTypes = {
  fill: PropTypes.string,
  fillMask: PropTypes.string,
  fillInnerPlus: PropTypes.string,
  stroke: PropTypes.string,
  circleStroke: PropTypes.string,
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
};

const defaultProps = {
  fill: whiteColor,
  fillMask: blackColor,
  fillInnerPlus: burntSiennaColor,
  stroke: sharkGrayColor,
  circleStroke: burntSiennaColor,
  className: '',
  width: '51',
  height: '42',
};

function SignUpIcon({
  width,
  height,
  stroke,
  circleStroke,
  fillInnerPlus,
  fill,
  fillMask,
  className,
}) {
  return (
    <svg
      className={className}
      width={width}
      height={height}
      fill="none"
      viewBox="0 0 51 42"
    >
      <circle
        cx="34"
        cy="9.188"
        r="7"
        stroke={circleStroke}
        strokeWidth="3"
      />
      <path
        stroke={stroke}
        strokeWidth="3"
        d="M18.5 29.236c0-4.848 4.869-8.296 9.648-6.826 1.214.374 2.366.704 3.343.942.932.227 1.844.407 2.509.407.665 0 1.577-.18 2.509-.407.977-.238 2.13-.568 3.343-.942 4.78-1.47 9.648 1.978 9.648 6.826v1.907a7 7 0 01-7 7h-17a7 7 0 01-7-7v-1.907z"
      />
      <mask
        id="path-3-outside-1"
        width="23"
        height="23"
        x="0"
        y="19"
        fill={fillMask}
        maskUnits="userSpaceOnUse"
      >
        <path fill={fill} d="M0 19H23V42H0z" />
        <path
          fillRule="evenodd"
          d="M10 37.5a1.5 1.5 0 003 0V32h5.5a1.5 1.5 0 000-3H13v-5.5a1.5 1.5 0 00-3 0V29H4.5a1.5 1.5 0 000 3H10v5.5z"
          clipRule="evenodd"
        />
      </mask>
      <path
        fill={fillInnerPlus}
        fillRule="evenodd"
        d="M10 37.5a1.5 1.5 0 003 0V32h5.5a1.5 1.5 0 000-3H13v-5.5a1.5 1.5 0 00-3 0V29H4.5a1.5 1.5 0 000 3H10v5.5z"
        clipRule="evenodd"
      />
      <path
        fill={fill}
        d="M13 32v-2.5h-2.5V32H13zm0-3h-2.5v2.5H13V29zm-3 0v2.5h2.5V29H10zm0 3h2.5v-2.5H10V32zm1.5 4.5a1 1 0 011 1h-5a4 4 0 004 4v-5zm-1 1a1 1 0 011-1v5a4 4 0 004-4h-5zm0-5.5v5.5h5V32h-5zm8-2.5H13v5h5.5v-5zm-1 1a1 1 0 011-1v5a4 4 0 004-4h-5zm1 1a1 1 0 01-1-1h5a4 4 0 00-4-4v5zm-5.5 0h5.5v-5H13v5zm-2.5-8V29h5v-5.5h-5zm1 1a1 1 0 01-1-1h5a4 4 0 00-4-4v5zm1-1a1 1 0 01-1 1v-5a4 4 0 00-4 4h5zm0 5.5v-5.5h-5V29h5zm-8 2.5H10v-5H4.5v5zm1-1a1 1 0 01-1 1v-5a4 4 0 00-4 4h5zm-1-1a1 1 0 011 1h-5a4 4 0 004 4v-5zm5.5 0H4.5v5H10v-5zm2.5 8V32h-5v5.5h5z"
        mask="url(#path-3-outside-1)"
      />
    </svg>
  );
}

SignUpIcon.propTypes = propTypes;
SignUpIcon.defaultProps = defaultProps;

export default SignUpIcon;
