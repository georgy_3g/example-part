import React, { FunctionComponent } from 'react';
import Colors from 'src/styles/colors.json';
import { propsInterface } from './interface';

const greenColor = Colors['$summer-green-color'];
const blackColor = Colors['$black-color'];
const whiteColor = Colors['$white-color'];

const defaultProps = {
  stroke: blackColor,
  fill: greenColor,
  className: '',
  width: '20',
  height: '20',
};

const ImageNoteIcon: FunctionComponent<propsInterface> = ({
  width,
  height,
  stroke,
  fill,
  className,
}) => (
  <svg width={width} height={height} className={className} fill="none" viewBox="0 0 20 20">
    <mask id="prefix__a" fill={whiteColor}>
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M10 20c5.523 0 10-4.477 10-10S15.523 0 10 0 0 4.477 0 10c0 1.423.297 2.777.833 4.002v5.165h5.164A9.968 9.968 0 0010 20z"
      />
    </mask>
    <path
      d="M.833 14.002h1.2v-.25l-.1-.23-1.1.48zm0 5.165h-1.2v1.2h1.2v-1.2zm5.164 0l.48-1.1-.229-.1h-.25v1.2zM18.8 10a8.8 8.8 0 01-8.8 8.8v2.4c6.186 0 11.2-5.014 11.2-11.2h-2.4zM10 1.2a8.8 8.8 0 018.8 8.8h2.4c0-6.186-5.014-11.2-11.2-11.2v2.4zM1.2 10A8.8 8.8 0 0110 1.2v-2.4C3.814-1.2-1.2 3.814-1.2 10h2.4zm.733 3.522A8.767 8.767 0 011.2 10h-2.4c0 1.592.333 3.109.934 4.483l2.199-.961zm.1 5.645v-5.165h-2.4v5.165h2.4zm3.964-1.2H.833v2.4h5.164v-2.4zM10 18.8a8.767 8.767 0 01-3.522-.733l-.962 2.2c1.375.6 2.892.933 4.484.933v-2.4z"
      fill={stroke}
      mask="url(#prefix__a)"
    />
    <circle cx="5.416" cy="10" r="1.25" fill={fill} />
    <circle cx="10" cy="10" r="1.25" fill={fill} />
    <circle cx="14.584" cy="10" r="1.25" fill={fill} />
  </svg>
);

ImageNoteIcon.defaultProps = defaultProps;

export default ImageNoteIcon;
