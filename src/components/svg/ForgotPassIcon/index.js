import React from 'react';
import PropTypes from 'prop-types';
import Colors from 'src/styles/colors.json';

const burntSiennaColor = Colors['$burnt-sienna-color'];
const sharkGrayColor = Colors['$shark-gray-color'];

const propTypes = {
  fillAsterisks: PropTypes.string,
  stroke: PropTypes.string,
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
};

const defaultProps = {
  fillAsterisks: burntSiennaColor,
  stroke: sharkGrayColor,
  className: '',
  width: '34',
  height: '40',
};

function ForgotPassIcon({
  width,
  height,
  stroke,
  fillAsterisks,
  className,
}) {
  return (
    <svg
      className={className}
      width={width}
      height={height}
      fill="none"
      viewBox="0 0 34 40"
    >
      <path
        fill={fillAsterisks}
        d="M10.832 26.16l1.572-.516.36 1.14-1.572.504.996 1.368-.996.732-1.008-1.38-.972 1.32-.972-.732 1.008-1.308-1.596-.552.384-1.14 1.572.564v-1.656h1.224v1.656zm6.79 0l1.572-.516.36 1.14-1.572.504.995 1.368-.995.732-1.008-1.38-.973 1.32-.972-.732 1.009-1.308-1.597-.552.384-1.14 1.573.564v-1.656h1.224v1.656zm6.788 0l1.573-.516.36 1.14-1.573.504.997 1.368-.997.732-1.008-1.38-.971 1.32-.973-.732 1.008-1.308-1.596-.552.384-1.14 1.572.564v-1.656h1.224v1.656z"
      />
      <rect
        width="31"
        height="23"
        x="1.5"
        y="15.5"
        stroke={stroke}
        strokeWidth="3"
        rx="5.5"
      />
      <path
        stroke={stroke}
        strokeWidth="3"
        d="M8.5 10a8.5 8.5 0 0117 0v4.571a.929.929 0 01-.929.929H9.43a.929.929 0 01-.929-.929V10z"
      />
    </svg>
  );
}

ForgotPassIcon.propTypes = propTypes;
ForgotPassIcon.defaultProps = defaultProps;

export default ForgotPassIcon;
