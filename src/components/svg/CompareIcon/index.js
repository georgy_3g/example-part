import React from 'react';
import PropTypes from 'prop-types';
import Colors from 'src/styles/colors.json';

const whiteColor = Colors['$white-color'];
const grayColor = Colors['$darker-cadet-blue-color'];
const greenColor = Colors['$summer-green-color'];

const propTypes = {
  color1: PropTypes.string,
  color2: PropTypes.string,
  backgroundColor: PropTypes.string,
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
};

const defaultProps = {
  color1: greenColor,
  color2: grayColor,
  backgroundColor: whiteColor,
  className: '',
  width: '40',
  height: '33',
};

function CompareIcon({
  width,
  height,
  color1,
  color2,
  backgroundColor,
  className,
}) {
  return (
    <svg
      className={className}
      width={width}
      height={height}
      fill="none"
      viewBox="0 0 40 33"
    >
      <rect
        width="27.591"
        height="24.167"
        x="0.75"
        y="0.75"
        fill={backgroundColor}
        stroke={color2}
        strokeWidth="1.5"
        rx="5.25"
      />
      <path
        stroke={color2}
        strokeLinecap="round"
        strokeWidth="1.5"
        d="M5.563 16.164l3.984-5.572a1.447 1.447 0 012.048-.314l1.668 1.256M11.977 14.895l1.944-2.91a1.447 1.447 0 012.12-.315l1.497 1.226"
      />
      <path
        stroke={color2}
        strokeWidth="1.5"
        d="M23.978 7.334a2.171 2.171 0 01-2.159 2.183 2.171 2.171 0 01-2.159-2.183c0-1.212.973-2.184 2.16-2.184 1.186 0 2.158.972 2.158 2.184z"
      />
      <path
        stroke={color2}
        strokeLinecap="round"
        strokeWidth="1.5"
        d="M4.277 19.765h15.401"
      />
      <rect
        width="27.591"
        height="24.167"
        x="11.66"
        y="8.084"
        fill={backgroundColor}
        stroke={color1}
        strokeWidth="1.5"
        rx="5.25"
      />
      <path
        stroke={color1}
        strokeLinecap="round"
        strokeWidth="1.5"
        d="M16.473 23.498l3.984-5.572a1.447 1.447 0 012.048-.314l1.668 1.256M22.887 22.23l1.944-2.911a1.447 1.447 0 012.12-.315l1.497 1.226"
      />
      <path
        stroke={color1}
        strokeWidth="1.5"
        d="M34.889 14.668a2.171 2.171 0 01-2.16 2.183 2.171 2.171 0 01-2.159-2.183c0-1.212.973-2.184 2.16-2.184 1.186 0 2.159.972 2.159 2.184z"
      />
      <path
        stroke={color1}
        strokeLinecap="round"
        strokeWidth="1.5"
        d="M15.188 27.099h15.4"
      />
    </svg>
  );
}

CompareIcon.propTypes = propTypes;
CompareIcon.defaultProps = defaultProps;

export default CompareIcon;
