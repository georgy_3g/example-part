import React from 'react';
import PropTypes from 'prop-types';
import Colors from 'src/styles/colors.json';

const greenColor = Colors['$summer-green-color'];
const blueColor = Colors['$pickled-bluewood-color'];

const propTypes = {
  stroke: PropTypes.string,
  stroke2: PropTypes.string,
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
  strokeWidth: PropTypes.string,
};

const defaultProps = {
  stroke: greenColor,
  stroke2: blueColor,
  className: '',
  width: '46',
  height: '46',
  strokeWidth: '2',
};

function MinShareIcon({
  width,
  height,
  stroke,
  stroke2,
  className,
  strokeWidth,
}) {
  return (
    <svg
      width={width}
      height={height}
      fill="none"
      viewBox="0 0 46 46"
      className={className}
    >
      <path
        stroke={stroke}
        strokeWidth={strokeWidth}
        d="M15 25.5L28 33M15.5 20.5l12-7"
      />
      <circle cx="11" cy="23" r="5" stroke={stroke2} strokeWidth={strokeWidth} />
      <path
        stroke={stroke2}
        strokeWidth={strokeWidth}
        d="M32 16a5 5 0 100-10 5 5 0 000 10z"
      />
      <circle cx="32" cy="35" r="5" stroke={stroke2} strokeWidth={strokeWidth} />
    </svg>
  );
}

MinShareIcon.propTypes = propTypes;
MinShareIcon.defaultProps = defaultProps;

export default MinShareIcon;
