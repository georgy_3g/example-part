import React from 'react';
import PropTypes from 'prop-types';
import colors from 'src/styles/colors.json';

const propTypes = {
  stroke: PropTypes.string,
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
};

const white = colors['$white-color'];

const defaultProps = {
  stroke: white,
  className: '',
  width: '24',
  height: '24',
};

function NewMagnifyingMinusIcon({
  width,
  height,
  stroke,
  className,
}) {
  return (
    <svg
      width={width}
      height={height}
      fill="none"
      viewBox="0 0 24 24"
      className={className}
    >
      <circle cx="10" cy="10" r="8" stroke={stroke} strokeWidth="2" />
      <path
        stroke={stroke}
        strokeLinecap="round"
        strokeWidth="2.668"
        d="M16.298 16.8l5.805 5.805"
      />
      <path
        stroke={stroke}
        strokeLinecap="round"
        strokeWidth="2"
        d="M7.5 10h5"
      />
    </svg>
  );
}

NewMagnifyingMinusIcon.propTypes = propTypes;
NewMagnifyingMinusIcon.defaultProps = defaultProps;

export default NewMagnifyingMinusIcon;
