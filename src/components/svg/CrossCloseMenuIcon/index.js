import React from 'react';
import PropTypes from 'prop-types';
import Colors from 'src/styles/colors.json';

const fillColor = Colors['$athens-light-gray-color'];
const strokeColor = Colors['$shark-gray-color'];

const propTypes = {
  fill: PropTypes.string,
  stroke: PropTypes.string,
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
};

const defaultProps = {
  fill: fillColor,
  stroke: strokeColor,
  className: '',
  width: '41',
  height: '41',
};

function CrossCloseMenuIcon({
  width,
  height,
  stroke,
  fill,
  className,
}) {
  return (
    <svg
      className={className}
      width={width}
      height={height}
      fill="none"
      viewBox="0 0 41 41"
    >
      <title>close button</title>
      <rect width="41" height="41" fill={fill} rx="12" />
      <rect
        width="20"
        height="2"
        x="14.136"
        y="12.721"
        fill={stroke}
        rx="1"
        transform="rotate(45 14.136 12.72)"
      />
      <rect
        width="20"
        height="2"
        x="28.278"
        y="14.135"
        fill={stroke}
        rx="1"
        transform="rotate(135 28.278 14.135)"
      />
    </svg>
  );
}

CrossCloseMenuIcon.propTypes = propTypes;
CrossCloseMenuIcon.defaultProps = defaultProps;

export default CrossCloseMenuIcon;
