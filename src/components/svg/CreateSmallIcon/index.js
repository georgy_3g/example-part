import React from 'react';
import PropTypes from 'prop-types';
import Colors from 'src/styles/colors.json';

const green = Colors['$summer-green-color'];

const propTypes = {
  fill: PropTypes.string,
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
};

const defaultProps = {
  fill: green,
  className: '',
  width: '28',
  height: '28',
};


function CreateSmallIcon({
  width,
  height,
  fill,
  className,
}) {
  return (
    <svg
      width={width}
      height={height}
      version="1.1"
      viewBox="0 0 28 28"
      className={className}
      fill="none"
    >
      <rect
        width="25.826"
        height="22.348"
        x="1.086"
        y="2.477"
        stroke={fill}
        strokeWidth="2"
        rx="4"
      />
      <path
        stroke={fill}
        strokeLinecap="round"
        strokeWidth="2"
        d="M6.406 16.81l3.776-5.237a1.447 1.447 0 012.04-.313l1.55 1.158M12.543 15.61l1.823-2.707a1.447 1.447 0 012.113-.315l1.384 1.125"
      />
      <path
        stroke={fill}
        strokeWidth="2"
        d="M22.737 8.435a1.783 1.783 0 11-3.565 0 1.783 1.783 0 013.565 0z"
      />
      <path
        stroke={fill}
        strokeLinecap="round"
        strokeWidth="2"
        d="M5.18 20.228H19.91"
      />
    </svg>
  );
}

CreateSmallIcon.propTypes = propTypes;
CreateSmallIcon.defaultProps = defaultProps;

export default CreateSmallIcon;
