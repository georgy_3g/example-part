import React from 'react';
import PropTypes from 'prop-types';
import Colors from 'src/styles/colors.json';

const whiteColor = Colors['$white-color'];

const propTypes = {
  fill: PropTypes.string,
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
};

const defaultProps = {
  fill: whiteColor,
  className: '',
  width: '46',
  height: '46',
};

function FacebookIcon({
  width,
  height,
  fill,
  className,
}) {
  return (
    <svg
      className={className}
      width={width}
      height={height}
      fill="none"
      viewBox="0 0 46 46"
    >
      <title>******* facebook page</title>
      <circle cx="23" cy="23" r="22.5" stroke={fill} />
      <path
        fill={fill}
        d="M24.491 33v-8.666h2.948l.442-3.378h-3.39v-2.157c0-.978.274-1.644 1.697-1.644L28 17.154v-3.022A24.883 24.883 0 0025.36 14c-2.614 0-4.404 1.574-4.404 4.464v2.492H18v3.378h2.956V33h3.535z"
      />
    </svg>
  );
}

FacebookIcon.propTypes = propTypes;
FacebookIcon.defaultProps = defaultProps;

export default FacebookIcon;
