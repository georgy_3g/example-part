import React from 'react';
import PropTypes from 'prop-types';
import Colors from 'src/styles/colors.json';

const pickledBluewoodColor = Colors['$pickled-bluewood-color'];
const slateGrayColor = Colors['$slate-gray-color-2'];

const propTypes = {
  fill: PropTypes.string,
  stroke: PropTypes.string,
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
};

const defaultProps = {
  fill: pickledBluewoodColor,
  stroke: slateGrayColor,
  className: '',
  width: '24',
  height: '24',
};

function InfoIcon({
  width,
  height,
  fill,
  stroke,
  className,
}) {
  return (
    <svg
      className={className}
      width={width}
      height={height}
      viewBox="0 0 24 24"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <circle cx="12" cy="12" r="11.22" stroke={stroke} strokeWidth="1.56" />
      <rect opacity="0.8" x="11" y="10" width="2" height="8" rx="1" fill={fill} />
      <rect opacity="0.8" x="11" y="6" width="2" height="2" rx="1" fill={fill} />
    </svg>
  );
}

InfoIcon.propTypes = propTypes;
InfoIcon.defaultProps = defaultProps;

export default InfoIcon;