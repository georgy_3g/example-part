import React from 'react';
import PropTypes from 'prop-types';
import Colors from 'src/styles/colors.json';

const fillColor = Colors['$pickled-bluewood-color'];

const propTypes = {
  fill: PropTypes.string,
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
};

const defaultProps = {
  fill: fillColor,
  className: '',
  width: '24',
  height: '24',
};

function RotateIcon(props) {
  const { fill, className, width, height } = props;
  return (
    <svg className={className} width={width} height={height} fill="none" viewBox="0 0 24 24">
      <g clipPath="url(#clip0)">
        <path
          stroke={fill}
          strokeLinecap="round"
          strokeWidth="2"
          d="M21.286 12a9.286 9.286 0 10-6.416 8.832"
        />
        <path
          fill={fill}
          fillRule="evenodd"
          d="M20.643 13.755a.654.654 0 001.07-.043l2.048-3.201a.654.654 0 00-.55-1.006l-4.487-.01a.654.654 0 00-.522 1.05l2.44 3.21z"
          clipRule="evenodd"
        />
      </g>
      <defs>
        <clipPath id="clip0">
          <path fill="#fff" d="M0 0H24V24H0z" />
        </clipPath>
      </defs>
    </svg>
  );
}

RotateIcon.propTypes = propTypes;
RotateIcon.defaultProps = defaultProps;

export default RotateIcon;
