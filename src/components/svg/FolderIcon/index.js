import React from 'react';
import PropTypes from 'prop-types';
import Colors from 'src/styles/colors.json';

const grayColor = Colors['$athens-gray-color'];

const propTypes = {
  fill: PropTypes.string,
  stroke: PropTypes.string,
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
};

const defaultProps = {
  fill: 'none',
  stroke: grayColor,
  className: '',
  width: '70',
  height: '70',
};

function FolderIcon({
  fill,
  width,
  height,
  className,
  stroke,
}) {
  return (
    <svg
      className={className}
      width={width}
      height={height}
      fill={fill}
      viewBox="0 0 70 70"
    >
      <path
        stroke={stroke}
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth="2"
        d="M17.622 14H33m0 0h30a6 6 0 016 6v32.5a6 6 0 01-6 6H7a6 6 0 01-6-6v-41a6 6 0 016-6h14.614a6 6 0 014.12 1.638L33 14z"
      />
      <path
        stroke={stroke}
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth="2"
        d="M6 58v0a5 5 0 005 5h48a5 5 0 005-5v0"
      />
    </svg>
  );
}

FolderIcon.propTypes = propTypes;
FolderIcon.defaultProps = defaultProps;

export default FolderIcon;
