import React from 'react';
import PropTypes from 'prop-types';
import Colors from 'src/styles/colors.json';

const grayColor = Colors['$slate-gray-color'];
const greenColor = Colors['$summer-green-color'];

const propTypes = {
  stroke: PropTypes.string,
  stroke2: PropTypes.string,
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
};

const defaultProps = {
  stroke: grayColor,
  stroke2: greenColor,
  className: '',
  width: '60',
  height: '52',
};

function SelectPhotoIcon({
  width,
  height,
  stroke,
  stroke2,
  className,
}) {
  return (
    <svg
      width={width}
      height={height}
      className={className}
      fill="none"
      viewBox="0 0 60 52"
    >
      <rect
        width="58"
        height="50"
        x="1"
        y="1"
        stroke={stroke}
        strokeWidth="2"
        rx="7"
      />
      <path
        stroke={stroke}
        strokeLinecap="round"
        strokeWidth="2"
        d="M11.47 32.066l9.138-12.673a1.447 1.447 0 012.04-.313l4.705 3.516M24.705 29.467l4.954-7.353a1.447 1.447 0 012.113-.315l4.404 3.58"
      />
      <path
        stroke={stroke2}
        strokeWidth="2"
        d="M49.295 15.168c0 2.543-2.103 4.634-4.736 4.634-2.632 0-4.735-2.091-4.735-4.633 0-2.543 2.103-4.634 4.736-4.634 2.632 0 4.735 2.091 4.735 4.633z"
      />
      <path
        stroke={stroke2}
        strokeLinecap="round"
        strokeWidth="2"
        d="M8.824 39.433H40.59"
      />
    </svg>
  );
}

SelectPhotoIcon.propTypes = propTypes;
SelectPhotoIcon.defaultProps = defaultProps;

export default SelectPhotoIcon;
