import React from 'react';
import PropTypes from 'prop-types';
import colors from 'src/styles/colors.json';

const propTypes = {
  fill: PropTypes.string,
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
};

const gray = colors['$silver-gray-color'];

const defaultProps = {
  fill: gray,
  className: '',
  width: '40',
  height: '40',
};

function MagnifyingMinus({
  width,
  height,
  fill,
  className,
}) {
  return (
    <svg
      className={className}
      width={width}
      height={height}
      version="1.1"
      viewBox="76 76 250 250"
    >
      <g>
        <path
          fill={fill}
          fillRule="evenodd"
          stroke="none"
          d="M166.8 81.848C114.002 88.137 77 129.157 77 181.4c0 56.257 43.343 99.6 99.6 99.6 24.534 0 44.447-6.939 62.94-21.933l2.941-2.384 32.859 32.84c18.456 18.445 33.354 33.024 33.986 33.259a7.907 7.907 0 0010.256-10.256c-.235-.632-14.831-15.547-33.299-34.025-25.614-25.63-32.792-33.025-32.481-33.466.219-.311 1.453-1.945 2.743-3.631 19.226-25.124 25.101-60.093 15.227-90.637C260.223 115.044 230.266 89.174 193 82.74c-4.623-.798-22.016-1.39-26.2-.892M188 97.851c57.357 7.407 90.414 71.33 63.356 122.514-31.907 60.359-117.605 60.359-149.512 0C70.066 160.252 120.114 89.084 188 97.851m-64.2 76.316c-5.702 2.545-5.646 11.629.087 14.082 1.918.821 103.508.821 105.426 0 5.781-2.473 5.781-11.625 0-14.098-1.886-.807-103.704-.791-105.513.016"
        />
      </g>
    </svg>
  );
}

MagnifyingMinus.propTypes = propTypes;
MagnifyingMinus.defaultProps = defaultProps;

export default MagnifyingMinus;
