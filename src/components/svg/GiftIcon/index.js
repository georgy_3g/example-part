import React from 'react';
import PropTypes from 'prop-types';
import Colors from 'src/styles/colors.json';

const sharkGrayColor = Colors['$hit-gray-color'];

const propTypes = {
  stroke: PropTypes.string,
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
};

const defaultProps = {
  stroke: sharkGrayColor,
  className: '',
  width: '22',
  height: '24',
};

const GiftIcon = ({
  width,
  height,
  stroke,
  className,
}) => {
  return (
    <svg
      className={className}
      width={width}
      height={height}
      fill="none"
      viewBox="0 0 22 24"
    >
      <path
        stroke={stroke}
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth="2"
        d="M20 13v7.5a2.41 2.41 0 01-.79 1.768A2.814 2.814 0 0117.3 23H4.7a2.814 2.814 0 01-1.91-.732A2.41 2.41 0 012 20.5V13M11 6v16.625"
      />
      <path
        stroke={stroke}
        strokeWidth="2"
        d="M14 7h-3V4a3 3 0 113 3zM8 7h3V4a3 3 0 10-3 3z"
      />
      <rect
        width="20"
        height="6"
        x="1"
        y="7"
        stroke={stroke}
        strokeWidth="2"
        rx="2"
      />
    </svg>
  );
}

GiftIcon.propTypes = propTypes;
GiftIcon.defaultProps = defaultProps;

export default GiftIcon;
