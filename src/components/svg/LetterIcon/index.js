import React from 'react';
import PropTypes from 'prop-types';
import Colors from 'src/styles/colors.json';

const burntSiennaColor = Colors['$burnt-sienna-color'];
const sharkGrayColor = Colors['$shark-gray-color'];
const whiteColor = Colors['$white-color'];

const propTypes = {
  fill: PropTypes.string,
  rectStroke: PropTypes.string,
  circleStroke: PropTypes.string,
  stroke: PropTypes.string,
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
};

const defaultProps = {
  fill: whiteColor,
  rectStroke: sharkGrayColor,
  circleStroke: sharkGrayColor,
  stroke: burntSiennaColor,
  className: '',
  width: '45',
  height: '36',
};

function LetterIcon({
  width,
  height,
  fill,
  stroke,
  rectStroke,
  circleStroke,
  className,
}) {
  return (
    <svg
      className={className}
      width={width}
      height={height}
      fill="none"
      viewBox="0 0 45 36"
    >
      <path
        stroke={stroke}
        strokeWidth="3"
        d="M1.7 6l16.785 9.566a5 5 0 005.01-.034L39.7 6"
      />
      <rect
        width="37"
        height="29"
        x="2.5"
        y="1.5"
        stroke={rectStroke}
        strokeWidth="3"
        rx="4.5"
      />
      <circle
        cx="36"
        cy="27"
        r="8"
        fill={fill}
        stroke={circleStroke}
        strokeWidth="2"
      />
      <path
        stroke={stroke}
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth="2"
        d="M33 27.954l2.364 1.773L39.5 25"
      />
    </svg>
  );
}

LetterIcon.propTypes = propTypes;
LetterIcon.defaultProps = defaultProps;

export default LetterIcon;
