import React from 'react';
import PropTypes from 'prop-types';
import Colors from 'src/styles/colors.json';

const icebergBlueLightColor = Colors['$iceberg-blue-light-color'];
const sharkGrayColor = Colors['$shark-gray-color'];

const propTypes = {
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
  stroke: PropTypes.string,
  color: PropTypes.string,
};

const defaultProps = {
  className: '',
  width: '58',
  height: '58',
  stroke: icebergBlueLightColor,
  color: sharkGrayColor,
};

function PeopleIcon({
  width,
  height,
  className,
  stroke,
  color,
}) {
  return (
    <svg
      className={className}
      width={width}
      height={height}
      fill="none"
      viewBox="0 0 58 58"
    >
      <rect
        width="58"
        height="58"
        fill={stroke}
        rx="17.4"
        transform="matrix(-1 0 0 1 58 0)"
      />
      <path
        stroke={color}
        strokeMiterlimit="10"
        strokeWidth="1.595"
        d="M24.899 31.301a4.164 4.164 0 001.096 2.857c.399.394.872.706 1.394.919a4.354 4.354 0 003.29 0 4.293 4.293 0 001.392-.919 4.14 4.14 0 001.02-2.857 9.846 9.846 0 01-4.058.917c-1.43-.008-2.84-.32-4.134-.917v0z"
      />
      <path
        stroke={color}
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth="1.595"
        d="M31.275 27.242c0 .978-1.027 1.765-2.31 1.765-1.281 0-2.309-.787-2.309-1.765M37.951 37.652a1.671 1.671 0 00-.367-.244 1.352 1.352 0 00-.595-.09 1.143 1.143 0 00-.57.206 1.29 1.29 0 00-.261-.556 1.268 1.268 0 00-.486-.368 1.176 1.176 0 00-.43-.09c-.324 0-.635.124-.87.348a1.29 1.29 0 00-.397.858 2.6 2.6 0 000 .462.65.65 0 000 .154c.117.534.3 1.052.545 1.54l.48.988 1.026-.347a6.41 6.41 0 001.596-.782l.114-.09c.099-.07.188-.151.266-.244a1.293 1.293 0 00-.051-1.745z"
      />
      <path
        stroke={color}
        strokeMiterlimit="10"
        strokeWidth="1.595"
        d="M24.655 32.261h-.42a7.542 7.542 0 00-2.936.563 7.65 7.65 0 00-2.502 1.664 7.801 7.801 0 00-1.685 2.51 7.91 7.91 0 00-.612 2.977v.13a3.625 3.625 0 003.625 3.625H37.87a3.625 3.625 0 003.625-3.625v-.186a7.91 7.91 0 00-.612-2.976 7.8 7.8 0 00-1.685-2.511 7.646 7.646 0 00-2.502-1.664 7.542 7.542 0 00-2.936-.563h-.42l-8.685.056z"
      />
      <path
        stroke={color}
        strokeWidth="1.595"
        d="M38.35 23.037c0 5.063-4.175 9.19-9.352 9.19-5.178 0-9.353-4.127-9.353-9.19s4.175-9.19 9.353-9.19c5.177 0 9.352 4.127 9.352 9.19z"
      />
      <path
        stroke={color}
        strokeWidth="1.595"
        d="M19.574 22.73c6.888 0 9.28-1.488 10.15-3.518.87-2.03 6.042.979 8.338 2.791"
      />
    </svg>
  );
}

PeopleIcon.propTypes = propTypes;
PeopleIcon.defaultProps = defaultProps;

export default PeopleIcon;
