import React from 'react';
import PropTypes from 'prop-types';
import colors from 'src/styles/colors.json';

const propTypes = {
  stroke: PropTypes.string,
  plusStroke: PropTypes.string,
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
};

const gray = colors['$shark-gray-color'];
const green = colors['$summer-green-color'];

const defaultProps = {
  stroke: gray,
  plusStroke: green,
  className: '',
  width: '20',
  height: '20',
};

function MagnifyingPlus2({
  width,
  height,
  stroke,
  plusStroke,
  className,
}) {
  return (
    <svg
      className={className}
      width={width}
      height={height}
      fill="none"
      viewBox="0 0 20 20"
    >
      <circle
        cx="8.336"
        cy="8.332"
        r="6.75"
        stroke={stroke}
        strokeWidth="1.5"
      />
      <path
        stroke={stroke}
        strokeLinecap="round"
        strokeWidth="1.5"
        d="M13.586 14l4.837 4.838"
      />
      <path
        stroke={plusStroke}
        strokeLinecap="round"
        strokeWidth="1.5"
        d="M8.33 6.246v4.167M6.25 8.33h4.167"
      />
    </svg>
  );
}

MagnifyingPlus2.propTypes = propTypes;
MagnifyingPlus2.defaultProps = defaultProps;

export default MagnifyingPlus2;
