import React from 'react';
import PropTypes from 'prop-types';
import Colors from 'src/styles/colors.json';

const grayColor = Colors['$shark-gray-color'];
const greenColor = Colors['$summer-green-color'];

const propTypes = {
  stroke: PropTypes.string,
  stroke2: PropTypes.string,
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
};

const defaultProps = {
  stroke: grayColor,
  stroke2: greenColor,
  className: '',
  width: '24',
  height: '24',
};

function TrashIcon2({
  width,
  height,
  stroke,
  stroke2,
  className,
}) {
  return (
    <svg
      width={width}
      height={height}
      fill="none"
      viewBox="0 0 24 24"
      className={className}
    >
      <path
        fillRule="evenodd"
        stroke={stroke}
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth="1.5"
        d="M2.658 5.63l3.114 15.502a2.316 2.316 0 002.27 1.86h7.76a2.316 2.316 0 002.271-1.86L21.184 5.63H2.659z"
        clipRule="evenodd"
      />
      <path
        stroke={stroke}
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth="1.5"
        d="M8.473 2.425l.436-.55A2.316 2.316 0 0110.722 1h2.44c.681 0 1.328.3 1.768.82l.511.605"
      />
      <path
        stroke={stroke2}
        strokeLinecap="round"
        strokeWidth="1.5"
        d="M9.605 15.978l-.833-4.556M14.237 15.978l.833-4.556M1.5 4.473h20.842"
      />
    </svg>
  );
}

TrashIcon2.propTypes = propTypes;
TrashIcon2.defaultProps = defaultProps;

export default TrashIcon2;
