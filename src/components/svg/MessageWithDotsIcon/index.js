import React from 'react';
import PropTypes from 'prop-types';
import Colors from 'src/styles/colors.json';

const whiteColor = Colors['$white-color'];
const pickledBluewoodColor = Colors['$pickled-bluewood-color'];

const propTypes = {
  fill: PropTypes.string,
  fillMask: PropTypes.string,
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
};

const defaultProps = {
  fill: pickledBluewoodColor,
  fillMask: whiteColor,
  className: '',
  width: '20',
  height: '20',
};

function MessageWithDotsIcon({
  width,
  height,
  fill,
  fillMask,
  className,
}) {
  return (
    <svg
      className={className}
      width={width}
      height={height}
      fill="none"
      viewBox="0 0 20 20"
    >
      <mask id="path-1-inside-1" fill={fillMask}>
        <path
          fillRule="evenodd"
          d="M6.5 0A6.5 6.5 0 000 6.5v4A6.5 6.5 0 006.5 17h5.662a.32.32 0 01.19.062l3.209 2.354c.353.26.855.038.901-.398l.294-2.74c.01-.101.07-.19.155-.244A6.496 6.496 0 0020 10.5v-4A6.5 6.5 0 0013.5 0h-7z"
          clipRule="evenodd"
        />
      </mask>
      <path
        fill={fill}
        d="M15.56 19.416l.888-1.21-.887 1.21zm.902-.398l1.492.16-1.492-.16zm.294-2.74l-1.492-.16 1.492.16zm.155-.244l-.788-1.276.788 1.276zm-4.56 1.028l-.887 1.21.887-1.21zM1.5 6.5a5 5 0 015-5v-3a8 8 0 00-8 8h3zm0 4v-4h-3v4h3zm5 5a5 5 0 01-5-5h-3a8 8 0 008 8v-3zm5.662 0H6.5v3h5.662v-3zm4.286 2.706l-3.21-2.354-1.774 2.42 3.21 2.354 1.774-2.42zm-1.477.652a.931.931 0 011.477-.652l-1.774 2.42c1.286.943 3.11.137 3.28-1.448l-2.983-.32zm.293-2.74l-.293 2.74 2.983.32.293-2.74-2.982-.32zM18.5 10.5a4.996 4.996 0 01-2.377 4.258L17.7 17.31a7.996 7.996 0 003.8-6.81h-3zm0-4v4h3v-4h-3zm-5-5a5 5 0 015 5h3a8 8 0 00-8-8v3zm-7 0h7v-3h-7v3zm11.747 14.937c-.04.38-.257.694-.547.873l-1.577-2.552a1.829 1.829 0 00-.859 1.36l2.983.32zM12.162 18.5a1.18 1.18 0 01-.698-.229l1.774-2.419a1.82 1.82 0 00-1.076-.352v3z"
        mask="url(#path-1-inside-1)"
      />
      <rect width="2" height="2" x="13" y="7.5" fill={fill} rx="1" />
      <rect width="2" height="2" x="9" y="7.5" fill={fill} rx="1" />
      <rect width="2" height="2" x="5" y="7.5" fill={fill} rx="1" />
    </svg>
  );
}

MessageWithDotsIcon.propTypes = propTypes;
MessageWithDotsIcon.defaultProps = defaultProps;

export default MessageWithDotsIcon;
