import React from 'react';
import PropTypes from 'prop-types';
import Colors from 'src/styles/colors.json';

const sharkGray = Colors['$shark-gray-color'];
const green = Colors['$summer-green-color'];

const propTypes = {
  color1: PropTypes.string,
  color2: PropTypes.string,
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
};

const defaultProps = {
  color1: sharkGray,
  color2: green,
  className: '',
  width: '32',
  height: '32',
};

function SmallSizeIcon({
  width,
  height,
  color1,
  color2,
  className,
}) {
  return (
    <svg
      className={className}
      width={width}
      height={height}
      fill="none"
      viewBox="0 0 32 32"
    >
      <rect
        width="26"
        height="23"
        x="3"
        y="4"
        stroke={color1}
        strokeWidth="2"
        rx="4"
      />
      <path
        stroke={color2}
        strokeLinecap="round"
        strokeWidth="2"
        d="M7 11V9.447C7 8.647 7.648 8 8.447 8H10M7 20v1.553c0 .8.648 1.447 1.447 1.447H10"
      />
      <path
        stroke={color2}
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth="2"
        d="M20 15.018L18.4 17l-1.6-2-1.6 2-1.6-2-1.6 2"
      />
      <path
        stroke={color2}
        strokeLinecap="round"
        strokeWidth="2"
        d="M25 11V9.447C25 8.647 24.352 8 23.553 8H22M25 20v1.553c0 .8-.648 1.447-1.447 1.447H22"
      />
    </svg>
  );
}

SmallSizeIcon.propTypes = propTypes;
SmallSizeIcon.defaultProps = defaultProps;

export default SmallSizeIcon;
