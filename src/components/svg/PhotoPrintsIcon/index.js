import React from 'react';
import PropTypes from 'prop-types';
import Colors from 'src/styles/colors.json';

const grayColor = Colors['$darker-cadet-blue-color'];

const propTypes = {
  stroke: PropTypes.string,
  fill: PropTypes.string,
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
};

const defaultProps = {
  stroke: grayColor,
  fill: grayColor,
  className: '',
  width: '38',
  height: '33',
};

function PhotoPrintsIcon({
  width,
  height,
  stroke,
  fill,
  className,
}) {
  return (
    <svg className={className} width={width} height={height} fill="none" viewBox="0 0 38 33">
      <rect
        width={width - 3}
        height={height - 3}
        x="1.399"
        y="1.229"
        stroke={stroke}
        strokeWidth="1.371"
        rx="5.714"
      />
      <path
        stroke={fill}
        strokeLinecap="round"
        strokeWidth="1.371"
        d="M7.705 20.695l5.307-7.36a1.323 1.323 0 011.865-.286l2.509 1.874M15.77 19.115l2.75-4.083a1.323 1.323 0 011.931-.287l2.31 1.877"
      />
      <circle
        cx="28.142"
        cy="9.685"
        r="2.971"
        stroke={fill}
        strokeWidth="1.371"
      />
      <path
        stroke={fill}
        strokeLinecap="round"
        strokeWidth="1.371"
        d="M6.092 25.186h19.361"
      />
    </svg>
  );
}

PhotoPrintsIcon.propTypes = propTypes;
PhotoPrintsIcon.defaultProps = defaultProps;

export default PhotoPrintsIcon;
