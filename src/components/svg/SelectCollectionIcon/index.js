import React from 'react';
import PropTypes from 'prop-types';
import Colors from 'src/styles/colors.json';

const grayColor = Colors['$slate-gray-color'];
const greenColor = Colors['$summer-green-color'];

const propTypes = {
  stroke: PropTypes.string,
  stroke2: PropTypes.string,
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
};

const defaultProps = {
  stroke: grayColor,
  stroke2: greenColor,
  className: '',
  width: '60',
  height: '52',
};

function SelectCollectionIcon({
  width,
  height,
  stroke,
  stroke2,
  className,
}) {
  return (
    <svg
      width={width}
      height={height}
      className={className}
      fill="none"
      viewBox="0 0 60 52"
    >
      <g opacity="0.8">
        <rect
          width="58"
          height="50"
          x="1"
          y="1"
          stroke={stroke}
          strokeWidth="2"
          rx="7"
        />
        <rect
          width="46"
          height="38"
          x="7"
          y="7"
          stroke={stroke2}
          strokeWidth="2"
          rx="3"
        />
        <path
          stroke={stroke}
          strokeLinecap="round"
          strokeWidth="1.8"
          d="M16 31l6.081-7.904a1.447 1.447 0 011.977-.302L27 24.854M26 28l3.19-4.7a1.447 1.447 0 012.105-.313L34 25.169"
        />
        <circle cx="42" cy="18" r="3" stroke={stroke2} strokeWidth="2" />
        <path
          stroke={stroke2}
          strokeLinecap="round"
          strokeWidth="2"
          d="M16 36.5h18"
        />
      </g>
    </svg>
  );
}

SelectCollectionIcon.propTypes = propTypes;
SelectCollectionIcon.defaultProps = defaultProps;

export default SelectCollectionIcon;
