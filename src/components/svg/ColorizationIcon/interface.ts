export interface propsInterface {
  stroke?: string;
  fill?: string;
  className?: string;
  width?: string;
  height?: string;
}
