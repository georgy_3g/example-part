import React, { FunctionComponent } from 'react';
import Colors from 'src/styles/colors.json';
import { propsInterface } from './interface';

const whiteColor = Colors['$summer-green-color'];
const blackColor = Colors['$black-color'];

const defaultProps = {
  stroke: blackColor,
  fill: whiteColor,
  className: '',
  width: '46',
  height: '46',
};

const ColorizationIcon: FunctionComponent<propsInterface> = ({
  width,
  height,
  stroke,
  fill,
  className,
}) => (
  <svg width={width} height={height} fill="none" className={className} viewBox="0 0 38 48">
    <path
      fill={fill}
      d="M3.346 31.942c0-14.666 15.764-28.076 15.764-28.076s8.01 6.11 12.32 14.764L5.617 39.145c-1.278-1.792-2.271-4.05-2.271-7.203z"
    />
    <path
      stroke={stroke}
      strokeLinecap="round"
      strokeWidth="3"
      d="M19.111 2s17.111 14.667 17.111 29.333c0 4.89-2.444 14.667-17.11 14.667M19.111 2S2 16.667 2 31.333C2 36.223 4.444 46 19.111 46"
    />
  </svg>
);

ColorizationIcon.defaultProps = defaultProps;

export default ColorizationIcon;
