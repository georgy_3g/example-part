import React, { FunctionComponent } from 'react';
import Color from 'src/styles/colors.json';
import { propsInterface } from './interface';

const green = Color['$summer-green-color'];

const defaultProps = {
  className: '',
  width: '24',
  height: '20',
};

const SceneryIcon: FunctionComponent<propsInterface> = ({ className, width, height }) => (
  <svg className={className} width={width} height={height} fill="none" viewBox="0 0 24 20">
    <rect
      width="22.737"
      height="18.737"
      x="0.632"
      y="0.632"
      stroke={green}
      strokeWidth="1.263"
      rx="3.084"
    />
    <path
      stroke={green}
      strokeLinecap="round"
      strokeWidth="0.929"
      d="M4.59 12.333l3.453-4.605a.929.929 0 011.285-.197l1.615 1.16"
    />
    <path
      stroke={green}
      strokeLinecap="round"
      strokeWidth="1.263"
      d="M9.883 11.333l1.774-2.531a.929.929 0 011.332-.2l1.482 1.159"
    />
    <path
      stroke={green}
      strokeWidth="1.263"
      d="M19.484 5.833c0 .814-.71 1.535-1.663 1.535-.952 0-1.662-.721-1.662-1.535 0-.815.71-1.535 1.663-1.535.952 0 1.662.72 1.662 1.535z"
    />
    <path stroke={green} strokeLinecap="round" strokeWidth="1.263" d="M3.527 15.167h12.706" />
  </svg>
);

SceneryIcon.defaultProps = defaultProps;

export default SceneryIcon;
