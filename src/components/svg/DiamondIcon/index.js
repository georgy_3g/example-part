import React from 'react';
import PropTypes from 'prop-types';
import Colors from 'src/styles/colors.json';

const blue = Colors['$pickled-bluewood-color'];

const propTypes = {
  fill: PropTypes.string,
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
  stroke: PropTypes.string,
};

const defaultProps = {
  className: '',
  width: '24',
  height: '24',
  fill: 'none',
  stroke: blue,
};

function DiamondIcon({
  className,
  width,
  height,
  fill,
  stroke,
}) {
  return (
    <svg
      className={className}
      width={width}
      height={height}
      fill={fill}
      viewBox="0 0 24 24"
    >
      <path
        stroke={stroke}
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth="1.5"
        d="M3.353 12.616l7.198 7.576a2 2 0 002.9 0l7.198-7.576a3 3 0 00.167-3.94L19.252 6.72a4 4 0 00-3.124-1.501H7.873A4 4 0 004.75 6.72L3.185 8.675a3 3 0 00.168 3.94z"
      />
      <path
        stroke={stroke}
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth="1.5"
        d="M2.832 10.718v0c6.087.73 12.254.698 18.333-.093v0"
      />
      <path
        stroke={stroke}
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth="1.5"
        d="M10.623 5.219l-2.55 4.634a2 2 0 00-.077 1.77l4.002 9.086M13.377 5.219l2.55 4.634a2 2 0 01.077 1.77l-4.002 9.086"
      />
    </svg>
  );
}

DiamondIcon.propTypes = propTypes;
DiamondIcon.defaultProps = defaultProps;

export default DiamondIcon;
