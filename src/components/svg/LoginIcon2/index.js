import React from 'react';
import PropTypes from 'prop-types';
import Colors from 'src/styles/colors.json';

const whiteColor = Colors['$white-color'];

const propTypes = {
  fill: PropTypes.string,
  stroke: PropTypes.string,
  circleStroke: PropTypes.string,
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
};

const defaultProps = {
  fill: 'none',
  stroke: whiteColor,
  circleStroke: whiteColor,
  className: '',
  width: '16',
  height: '18',
};

function LoginIcon({
  width,
  height,
  stroke,
  circleStroke,
  fill,
  className,
}) {
  return (
    <svg className={className} width={width} height={height} fill={fill} viewBox="0 0 16 18">
      <title>login button</title>
      <circle cx="8" cy="4" r="3.25" stroke={circleStroke || stroke} strokeWidth="1.5" />
      <path
        stroke={stroke}
        strokeWidth="1.5"
        d="M.75 13.644c0-2.223 2.209-3.822 4.41-3.183.589.17 1.15.322 1.625.432.453.104.896.187 1.215.187.32 0 .762-.082 1.215-.187.475-.11 1.036-.261 1.625-.432 2.201-.64 4.41.96 4.41 3.183V14A3.25 3.25 0 0112 17.25H4A3.25 3.25 0 01.75 14v-.356z"
      />
    </svg>
  );
}

LoginIcon.propTypes = propTypes;
LoginIcon.defaultProps = defaultProps;

export default LoginIcon;
