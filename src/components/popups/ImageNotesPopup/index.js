import React, { useState } from 'react';
import ReactDOM from 'react-dom';
import bem from 'src/utils/bem';
import PropTypes from 'prop-types';
import dynamic from "next/dynamic";
import styles from './index.module.scss';

const ColorButton = dynamic(() => import('src/components/buttons/ColorButton'));
const ImageNoteIcon = dynamic(() => import('src/components/svg/ImageNoteIcon'));

const b = bem('image-notes-popup', styles);

const defaultProps = {
  title: '',
  description: '',
  placeholder: '',
  buttonText: '',
  submit: () => {},
  image: {},
  close: () => {},
  portalId: '',
  buttonBackgroundColor: '',
  iconColor: '',
  cancelText: '',
  cancelBackgroundColor: '',
  isFullScreen: false,
};

const propsTypes = {
  title: PropTypes.string,
  description: PropTypes.string,
  placeholder: PropTypes.string,
  buttonText: PropTypes.string,
  submit: PropTypes.func,
  image: PropTypes.shape({
    id: PropTypes.number,
    notes: PropTypes.string,
    notesId: PropTypes.number,
  }),
  close: PropTypes.func,
  portalId: PropTypes.string,
  buttonBackgroundColor: PropTypes.string,
  iconColor: PropTypes.string,
  cancelText: PropTypes.string,
  cancelBackgroundColor: PropTypes.string,
  isFullScreen: PropTypes.bool,
};

const ImageNotesPopup = (props) => {
  const veil = React.createRef();

  const {
    title,
    description,
    placeholder,
    buttonText,
    submit,
    image,
    close,
    portalId,
    buttonBackgroundColor,
    iconColor,
    cancelText,
    cancelBackgroundColor,
    isFullScreen,
  } = props;

  const [value, onChange] = useState(image.notes || '');

  const onSubmit = (isValue) => {
    const { notes, id, notesId } = image;
    const isNew = !(notes && notesId);
    const note = {
      orderImageId: id,
      notes: isValue ? value : '',
      isNew,
      deleteNote: !isValue,
    };
    if (notesId) {
      note.notesId = notesId;
    }
    submit(note);
  };

  const closeModal = (e) => {
    if (e.target === veil.current) {
      close();
    }
  };

  return ReactDOM.createPortal(
    <div
      className={b('veil', { 'full-screen': isFullScreen })}
      ref={veil}
      onClick={closeModal}
      role="button"
      tabIndex="0"
    >
      <div className={b()}>
        <div className={b('icon')}>
          <ImageNoteIcon fill={iconColor} />
        </div>
        <div className={b('title')}>{title}</div>
        <div className={b('description')}>{description}</div>
        <textarea
          className={b('input')}
          placeholder={placeholder}
          onChange={({ target }) => onChange(target.value)}
          value={value}
        />
        <div className={b('buttons')}>
          <div className={b('btn-wrapper')}>
            <ColorButton
              className={b('btn')}
              text={cancelText}
              onClick={() => onSubmit(false)}
              backGroundColor={cancelBackgroundColor}
              textClass={b('button-tex')}
            />
          </div>
          <div className={b('btn-wrapper')}>
            <ColorButton
              className={b('btn')}
              text={buttonText}
              onClick={() => onSubmit(true)}
              backGroundColor={buttonBackgroundColor}
              textClass={b('button-tex')}
            />
          </div>
        </div>
      </div>
    </div>,
    document.getElementById(portalId) || null,
  );
};

ImageNotesPopup.propTypes = propsTypes;
ImageNotesPopup.defaultProps = defaultProps;

export default ImageNotesPopup;
