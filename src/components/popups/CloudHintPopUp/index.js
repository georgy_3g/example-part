import React from 'react';
import bem from 'src/utils/bem';
import PropTypes from 'prop-types';

import cloudImage from  'public/img/shared/cloudBigImage.png';
import dynamic from "next/dynamic";
import styles from './index.module.scss';

const CloseIcon2 = dynamic(() => import('src/components/svg/CloseIcon2'));


const b = bem('cloud-hint-popup', styles);
const CLOUD_TEXT = '*******';
const PRICE = '$50 yearly';

const defaultProps = {
  close: () => {},
  className: '',
  title: '',
  list: [],
};

const propsTypes = {
  close: PropTypes.func,
  className: PropTypes.string,
  title: PropTypes.string,
  list: PropTypes.arrayOf(PropTypes.shape({})),
};

function CloudHintPopup(props) {
  const { close, className, title, list } = props;

  return (
    <div className={b({ mix: className })}>
      <div className={b('popup-block')}>
        <button className={b('close-btn')} type="button" onClick={close}>
          <CloseIcon2 />
        </button>
        <div className={b('row')}>
          <div className={b('column')}>
            <img
              className={b('cloud-image')}
              src={cloudImage.src}
              alt="*******"
            />
            <div className={b('title')}>{title}</div>
            <span className={b('cloud-text')}>{CLOUD_TEXT}</span>
            <span className={b('price')}>{PRICE}</span>
          </div>
          <div className={b('column')}>
            <div className={b('list')}>
              {list.map((item) => (
                <div className={b('item')}>
                  <img className={b('item-image')} src={item.image.src} alt="*******" />
                  <div className={b('item-title')}>{item.title}</div>
                </div>
              ))}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

CloudHintPopup.propTypes = propsTypes;
CloudHintPopup.defaultProps = defaultProps;

export default CloudHintPopup;
