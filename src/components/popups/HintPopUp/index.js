import React from 'react';
import ReactDOM from 'react-dom';
import bem from 'src/utils/bem';
import PropTypes from 'prop-types';

import Image from 'next/image';
import dynamic from "next/dynamic";
import styles from './index.module.scss';

const CloseIcon2 = dynamic(() => import('src/components/svg/CloseIcon2'));

const b = bem('hint-popup', styles);

const defaultProps = {
  hintImages: [],
  title: '',
  description: '',
  toggleHint: () => {},
  className: '',
  portalId: 'enhance-wizard',
};

const propsTypes = {
  toggleHint: PropTypes.func,
  title: PropTypes.string,
  description: PropTypes.string,
  hintImages: PropTypes.arrayOf(PropTypes.shape({})),
  className: PropTypes.string,
  portalId: PropTypes.string,
};

function HintPopup(props) {
  const { hintImages, title, description, toggleHint, className, portalId } = props;

  return (
    <>
      {ReactDOM.createPortal(
        <div className={b({ mix: className })}>
          <div className={b('hint-block')}>
            <button className={b('close-btn')} type="button" onClick={() => toggleHint(false)}>
              <CloseIcon2 />
            </button>
            <div className={b('hint-images')}>
              {hintImages && Boolean(hintImages.length)
                ? hintImages.map(({ id: hintId, imageUri, index }) => (
                  <div
                    className={b('hint-image-wrap')}
                    key={`${hintId}_${imageUri}`}
                    style={{ order: index }}
                  >
                    <Image className={b('hint-image')} src={imageUri} alt={imageUri} width='100%' height='100%' />
                  </div>
                  ))
                : null}
            </div>
            <span className={b('hint-title')}>{title || ''}</span>
            <span className={b('hint-text')}>{description || ''}</span>
          </div>
        </div>,
        document.getElementById(portalId) || null,
      )}
    </>
  );
}

HintPopup.propTypes = propsTypes;
HintPopup.defaultProps = defaultProps;

export default HintPopup;
