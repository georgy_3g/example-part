import { sizeErrorMailIcon, sizeErrorResizeIcon, sizeErrorCroppingIcon } from 'public';

const TITLE = 'Don’t worry, you have some options';

const ITEMS = [
  {
    iconUri: sizeErrorMailIcon,
    text: '1. Rescan your original photo at a higher resolution or mail us your origianl for us to scan.',
    key: 'SizeError1',
  },
  {
    iconUri: sizeErrorResizeIcon,
    text: ' 2. Select a smaller size or product. Reducing the size of your art will help ensure a better print quality.',
    key: 'SizeError2',
  },
  {
    iconUri: sizeErrorCroppingIcon,
    text: ' 3. Adust the cropping of your image. Crop out to provide more resolution.',
    key: 'SizeError3',
  },
];

export { TITLE, ITEMS };
