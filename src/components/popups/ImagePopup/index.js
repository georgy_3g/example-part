import React, { Component } from 'react';
import bem from 'src/utils/bem';
import PropTypes from 'prop-types';
import dynamic from "next/dynamic";
import styles from './index.module.scss';

const CloseIcon2 = dynamic(() => import('src/components/svg/CloseIcon2'));

const b = bem('image-popup', styles);

const defaultProps = {
  close: () => {},
  imageUri: '',
};

const propsTypes = {
  close: PropTypes.func,
  imageUri: PropTypes.func,
};

class ImagePopup extends Component {
  constructor(props) {
    super(props);
    this.veil = React.createRef();
  }

  closeModal = (e) => {
    const { close } = this.props;
    if (e.target === this.veil.current) {
      close();
    }
  };

  render() {
    const { close, imageUri } = this.props;

    return (
      <div className={b()} ref={this.veil} onClick={this.closeModal} role="button" tabIndex="0">
        <div className={b('popup-block')}>
          <div className={b('popup-content')}>
            <img className={b('preview-image')} src={imageUri} alt={imageUri} />
            <div className={b('popup-control-block')}>
              <button className={b('control-btn')} type="button" onClick={close}>
                <CloseIcon2 />
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

ImagePopup.propTypes = propsTypes;
ImagePopup.defaultProps = defaultProps;

export default ImagePopup;
