const CROP = 'Crop';
const UPLOAD = 'Upload';
const HTTPS_REGEXP = /^https:\/\//i;
const HTTP = 'http://';

export { CROP, UPLOAD, HTTPS_REGEXP, HTTP };
