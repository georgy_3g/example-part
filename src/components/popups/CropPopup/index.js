import React, { Component } from 'react';
import bem from 'src/utils/bem';
import ReactCrop from 'react-image-crop';
import PropTypes from 'prop-types';
import { IMAGE_TYPE_JPEG } from 'src/constants';
import spinner from  'public/img/shared/spinner*******.gif';
import dynamic from "next/dynamic";
import {
  CROP,
  UPLOAD,
  HTTPS_REGEXP,
  HTTP,
} from './constants';
import styles from './index.module.scss';

const CloseIcon2 = dynamic(() => import('src/components/svg/CloseIcon2'));


const b = bem('crop-popup', styles);
const defaultProps = {
  close: () => {},
  imageUri: '',
  upload: () => {},
};

const propsTypes = {
  close: PropTypes.func,
  imageUri: PropTypes.func,
  upload: PropTypes.func,
};

class CropPopup extends Component {
  constructor(props) {
    super(props);
    this.veil = React.createRef();
    this.reader = new FileReader();
    this.state = {
      src: null,
      croppedImageUrl: null,
      disableBtn: true,
      crop: {
        unit: '%',
      },
    };
  }

  componentDidMount() {
    this.getSrc();
  }

  getSrc = async () => {
    const { imageUri } = this.props;
    const src = await this.imgToBase64(imageUri.replace(HTTPS_REGEXP, HTTP));
    this.setState({ src });
  };

  closeModal = (e) => {
    const { close } = this.props;
    if (e.target === this.veil.current) {
      close();
    }
  };

  getBase64Image = (img, type = IMAGE_TYPE_JPEG) => {
    const canvas = document.createElement('canvas');
    canvas.width = img.width;
    canvas.height = img.height;
    canvas.crossOrigin = 'anonymous';
    const ctx = canvas.getContext('2d');
    ctx.drawImage(img, 0, 0, img.width, img.height);
    return canvas.toDataURL(type);
  };

  imgToBase64 = (src) =>
    new Promise((resolve, reject) => {
      const image = new Image();

      function corsError() {
        this.crossOrigin = '';
        this.src = '';
        this.removeEventListener('error', corsError, false);
      }
      image.addEventListener('error', corsError, false);
      image.setAttribute('crossOrigin', 'anonymous');
      image.onload = () => {
        const base64 = this.getBase64Image(image);
        resolve(base64);
      };
      image.onerror = (err) => reject(err);

      image.src = `${src}?v=${Math.random()}`;
    });

  onClickUpload = () => {
    const { upload } = this.props;
    upload(this.fileToUpload);
  };

  onImageLoaded = (image) => {
    this.imageRef = image;
  };

  onCropComplete = (crop) => {
    this.setState({ disableBtn: false, crop });
  };

  onCropChange = (crop) => {
    this.setState({ crop });
  };

  onClickCrop = () => {
    const { crop } = this.state;
    this.makeClientCrop(crop);
  };

  makeClientCrop = async (crop) => {
    const { width, height } = crop;
    if (this.imageRef && width && height) {
      const croppedImageUrl = await this.getCroppedImg(this.imageRef, crop, 'newFile.jpeg');
      this.setState({ croppedImageUrl });
    }
  };

  getCroppedImg = (image, crop, fileName) => {
    const { naturalWidth, naturalHeight, width, height } = image;

    const { x, y, width: cropWidth, height: cropHeight } = crop;
    const canvas = document.createElement('canvas');
    const scaleX = naturalWidth / width;
    const scaleY = naturalHeight / height;
    const cropNaturalWidth = cropWidth * scaleX;
    const cropNaturalHeight = cropHeight * scaleY;
    canvas.width = cropNaturalWidth;
    canvas.height = cropNaturalHeight;
    const ctx = canvas.getContext('2d');

    ctx.drawImage(
      image,
      x * scaleX,
      y * scaleY,
      cropNaturalWidth,
      cropNaturalHeight,
      0,
      0,
      cropNaturalWidth,
      cropNaturalHeight,
    );

    return new Promise((resolve) => {
      canvas.toBlob((blob) => {
        if (!blob) {
          return;
        }
        const data = blob;
        data.name = fileName;
        this.fileToUpload = data;
        window.URL.revokeObjectURL(this.fileUrl);
        this.fileUrl = window.URL.createObjectURL(data);
        resolve(this.fileUrl);
      }, IMAGE_TYPE_JPEG);
    });
  };

  render() {
    const { close } = this.props;

    const { crop, croppedImageUrl, disableBtn, src } = this.state;

    return (
      <div className={b()} ref={this.veil} onClick={this.closeModal} role="button" tabIndex="0">
        <div className={b('popup-block')}>
          <button className={b('close-btn')} type="button" onClick={close}>
            <CloseIcon2 />
          </button>
          {!src && !croppedImageUrl && (
            <div className={b('spinner-block')}>
              <img
                className={b('spinner', { invisible: src || croppedImageUrl })}
                src={spinner.src}
                alt="spinner"
              />
            </div>
          )}
          <div className={b('photo-block')}>
            {src && !croppedImageUrl && (
              <div className={b('crop-block')}>
                <ReactCrop
                  src={src}
                  crop={crop}
                  onImageLoaded={this.onImageLoaded}
                  onComplete={this.onCropComplete}
                  onChange={this.onCropChange}
                />
                <button
                  className={b('btn', { mix: 'btn' })}
                  type="button"
                  onClick={this.onClickCrop}
                  disabled={disableBtn}
                >
                  {CROP}
                </button>
              </div>
            )}
            {croppedImageUrl && (
              <div className={b('photo-wrap')}>
                <img className={b('cropped-image')} alt="Crop" src={croppedImageUrl} />
                <button
                  className={b('btn', { mix: 'btn' })}
                  type="button"
                  onClick={this.onClickUpload}
                >
                  {UPLOAD}
                </button>
              </div>
            )}
          </div>
        </div>
      </div>
    );
  }
}

CropPopup.propTypes = propsTypes;
CropPopup.defaultProps = defaultProps;

export default CropPopup;
