import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { FieldArray, Formik, getIn } from 'formik';
import bem from 'src/utils/bem';
import * as Yup from 'yup';
import ERRORS from 'src/constants/errors';
import dynamic from 'next/dynamic';
import { shareStatusSelector } from 'src/redux/ordersList/selectors';
import { getToken } from 'src/redux/auth/selectors';
import { clearShareStatusAction, shareOrderFilesAction } from 'src/redux/ordersList/actions';
import styles from './index.module.scss';
import {
  EMAIL_PLACEHOLDER,
  NAME_PLACEHOLDER,
  RECIPIENT,
  SEND_TO_CONTACT,
  SUBMIT_BUTTON_TEXT,
  TEXT_PLACEHOLDER,
  WHO_SHARE_TEXT,
} from './constants';

const ColorButton = dynamic(() => import('src/components/buttons/ColorButton'));
const CustomTextarea = dynamic(() => import('src/components/inputs/CustomTextarea'));
const CloseIcon2 = dynamic(() => import('src/components/svg/CloseIcon2'));
const NewCustomInput = dynamic(() => import('src/components/inputs/NewCustomInput'));

const { SHOULD_BE_FILLED, INVALID_EMAIL } = ERRORS;

const schema = Yup.object().shape({
  recipients: Yup.array().of(
    Yup.object().shape({
      name: Yup.string().required(SHOULD_BE_FILLED),
      email: Yup.string().email(INVALID_EMAIL).required(SHOULD_BE_FILLED),
    }),
  ),
});

const b = bem('share-popup', styles);

const propTypes = {
  className: PropTypes.string,
  data: PropTypes.shape({
    images: PropTypes.arrayOf(PropTypes.shape({})),
    videos: PropTypes.arrayOf(PropTypes.shape({})),
    audios: PropTypes.arrayOf(PropTypes.shape({})),
    folder: PropTypes.arrayOf(PropTypes.shape({})),
  }),
  close: PropTypes.func,
  shareOrderFiles: PropTypes.func,
  clearShareStatus: PropTypes.func,
  shareStatus: PropTypes.shape({
    isSuccess: PropTypes.bool,
  }),
  token: PropTypes.string,
};

const defaultProp = {
  className: '',
  data: {},
  close: () => {},
  shareOrderFiles: () => {},
  clearShareStatus: () => {},
  shareStatus: {},
  token: '',
};

function SharePopup(props) {
  const { className, shareOrderFiles, data, close, clearShareStatus, shareStatus, token } = props;

  useEffect(() => {
    const { isSuccess } = shareStatus;
    if (isSuccess) {
      clearShareStatus();
      close();
    }
  });

  const stopPropagation = (e) => {
    e.stopPropagation();
  };

  const onSubmit = (values) => {
    const { recipients } = values;
    const { images = [], videos = [], audios = [], folder = [] } = data || {};
    const objectData = { recipients };
    const ids = [...images, ...videos, ...audios, ...folder].map((item) =>
      item && item.id ? item.id : item,
    );
    shareOrderFiles({ data: { ...objectData, ids }, token });
  };

  return (
    <div className={b()} onClick={stopPropagation} role="button" tabIndex={0}>
      <div className={b('content', { mix: className })}>
        <button className={b('close-btn')} type="button" onClick={close}>
          <CloseIcon2 />
        </button>
        <span className={b('text')}>{WHO_SHARE_TEXT}</span>
        <Formik
          initialValues={{ recipients: [{ ...RECIPIENT }] }}
          validationSchema={schema}
          onSubmit={(values, actions) => {
            actions.resetForm();
            onSubmit(values);
          }}
          render={({ errors, handleSubmit, touched, handleChange, values }) => (
            <form onSubmit={handleSubmit} className={b('form')}>
              <FieldArray
                name="recipients"
                render={(arrayHelpers) => (
                  <>
                    {values.recipients.map((recipient, index) => {
                      const name = `recipients[${index}].name`;
                      const touchedName = getIn(touched, name);
                      const errorName = getIn(errors, name);

                      const email = `recipients[${index}].email`;
                      const touchedEmail = getIn(touched, email);
                      const errorEmail = getIn(errors, email);

                      const message = `recipients[${index}].message`;

                      return (
                        <div className={b('row')}>
                          <div className={b('column')}>
                            <NewCustomInput
                              className={b('input-block')}
                              inputClass={b('input')}
                              onChange={handleChange}
                              name={name}
                              placeholder={NAME_PLACEHOLDER}
                              value={recipient.name}
                              isTouched={touchedName}
                              error={errorName}
                              isValid={!errorName}
                            />
                            <NewCustomInput
                              className={b('input-block')}
                              inputClass={b('input')}
                              onChange={handleChange}
                              name={email}
                              placeholder={EMAIL_PLACEHOLDER}
                              value={recipient.email}
                              isTouched={touchedEmail}
                              error={errorEmail}
                              isValid={!errorEmail}
                            />
                          </div>
                          <div className={b('column')}>
                            <CustomTextarea
                              className={b('textarea')}
                              inputClass={b('text-input')}
                              name={message}
                              rows="5"
                              onChange={handleChange}
                              placeholder={TEXT_PLACEHOLDER}
                              value={recipient.text}
                            />
                          </div>
                        </div>
                      );
                    })}
                    <div className={b('control-block')}>
                      <div className={b('count-block')}>
                        <span className={b('count-text')}>{SEND_TO_CONTACT}</span>
                        <div className={b('counter')}>
                          {values.recipients.length > 1 && (
                            <button
                              className={b('count-button')}
                              type="button"
                              onClick={() =>
                                values.recipients.length > 1 &&
                                arrayHelpers.remove(values.recipients.length - 1)}
                            >
                              - Delete contact
                            </button>
                          )}
                          <button
                            className={b('count-button')}
                            type="button"
                            onClick={() => arrayHelpers.push({ ...RECIPIENT })}
                          >
                            + Add contact
                          </button>
                        </div>
                        <div className={b('submit-wrap')}>
                          <ColorButton text={SUBMIT_BUTTON_TEXT} type="submit" />
                        </div>
                      </div>
                    </div>
                  </>
                )}
              />
            </form>
          )}
        />
      </div>
    </div>
  );
}

SharePopup.defaultProps = defaultProp;
SharePopup.propTypes = propTypes;

const stateProps = (state) => ({
  shareStatus: shareStatusSelector(state),
  token: getToken(state),
});

const actions = {
  clearShareStatus: clearShareStatusAction,
  shareOrderFiles: shareOrderFilesAction,
};

export default connect(stateProps, actions)(SharePopup);
