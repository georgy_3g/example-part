const NAME_PLACEHOLDER = 'Name';
const EMAIL_PLACEHOLDER = 'Email';
const SUBMIT_BUTTON_TEXT = 'Send';
const TEXT_PLACEHOLDER = 'Message';
const WHO_SHARE_TEXT = 'Who do you want to \nshare with ?';
const SEND_TO_CONTACT = 'Send to another contact?';
const RECIPIENT = { name: '', email: '', message: '' };

export {
  NAME_PLACEHOLDER,
  EMAIL_PLACEHOLDER,
  SUBMIT_BUTTON_TEXT,
  TEXT_PLACEHOLDER,
  WHO_SHARE_TEXT,
  SEND_TO_CONTACT,
  RECIPIENT,
};
