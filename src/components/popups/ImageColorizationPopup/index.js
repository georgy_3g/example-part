import React from 'react';
import ReactDOM from 'react-dom';
import bem from 'src/utils/bem';
import PropTypes from 'prop-types';
import Image from 'next/image';
import dynamic from "next/dynamic";
import styles from './index.module.scss';

const ColorButton = dynamic(() => import('src/components/buttons/ColorButton'));
const ColorizationIcon = dynamic(() => import('src/components/svg/ColorizationIcon'));

const b = bem('image-colorization-popup', styles);

const defaultProps = {
  title: '',
  description: '',
  buttonText: '',
  submit: () => {},
  close: () => {},
  portalId: '__next',
  buttonBackgroundColor: '',
  iconColor: '',
  imageUrl: '',
  imageAlt: '',
  cancelText: '',
  cancelBackgroundColor: '',
  isFoolScreen: false,
  isMobileDisplay: true,
};

const propsTypes = {
  title: PropTypes.string,
  description: PropTypes.string,
  buttonText: PropTypes.string,
  submit: PropTypes.func,
  close: PropTypes.func,
  portalId: PropTypes.string,
  buttonBackgroundColor: PropTypes.string,
  iconColor: PropTypes.string,
  imageUrl: PropTypes.string,
  imageAlt: PropTypes.string,
  cancelText: PropTypes.string,
  cancelBackgroundColor: PropTypes.string,
  isFullScreen: PropTypes.bool,
  isMobileDisplay: PropTypes.bool,
};

const ImageColorizationPopup = (props) => {
  const veil = React.createRef();

  const {
    title,
    description,
    buttonText,
    submit,
    close,
    portalId,
    buttonBackgroundColor,
    iconColor,
    imageUrl,
    imageAlt,
    cancelText,
    cancelBackgroundColor,
    isFullScreen,
  } = props;

  const closeModal = (e) => {
    if (e.target === veil.current) {
      close();
    }
  };

  return ReactDOM.createPortal(
    <div
      className={b('veil', { 'full-screen': isFullScreen })}
      ref={veil}
      onClick={closeModal}
      role="button"
      tabIndex="0"
    >
      <div className={b()}>
        <div className={b('icon')}>
          <ColorizationIcon fill={iconColor} />
        </div>
        <div className={b('title')}>{title}</div>
        <div className={b('description')}>{description}</div>
        <div className={b('image')}>
          <Image src={imageUrl} alt={imageAlt} layout="fill" />
        </div>
        <div className={b('buttons')}>
          <div className={b('btn')}>
            <ColorButton
              text={cancelText}
              onClick={() => submit(false)}
              backGroundColor={cancelBackgroundColor}
            />
          </div>
          <div className={b('btn')}>
            <ColorButton
              text={buttonText}
              onClick={() => submit(true)}
              backGroundColor={buttonBackgroundColor}
            />
          </div>
        </div>
      </div>
    </div>,
    document.getElementById(portalId) || null,
  );
};

ImageColorizationPopup.propTypes = propsTypes;
ImageColorizationPopup.defaultProps = defaultProps;

export default ImageColorizationPopup;
