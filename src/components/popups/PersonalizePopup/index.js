import React, { Component } from 'react';
import bem from 'src/utils/bem';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Image from 'next/image';
import dynamic from 'next/dynamic';
import { optionsSelector } from 'src/redux/photoArt/selectors';
import styles from './index.module.scss';

const CloseIcon2 = dynamic(() => import('src/components/svg/CloseIcon2'));

const b = bem('personalize-popup', styles);

const defaultProps = {
  close: () => {},
  selectedOption: {},
  personalizeText: '',
};

const propsTypes = {
  close: PropTypes.func,
  selectedOption: PropTypes.shape({
    personalizeImage: PropTypes.shape({
      image_uri: PropTypes.string,
      top: PropTypes.string,
      left: PropTypes.string,
      width: PropTypes.string,
      height: PropTypes.string,
      font_color: PropTypes.string,
      font_family: PropTypes.string,
    }),
  }),
  personalizeText: PropTypes.string,
};

class PhotoArtPreviewPopup extends Component {
  constructor(props) {
    super(props);
    this.veil = React.createRef();
  }

  closeModal = (e) => {
    const { close } = this.props;
    if (e.target === this.veil.current) {
      close();
    }
  };

  render() {
    const { close, selectedOption, personalizeText } = this.props;
    const { personalizeImage } = selectedOption;
    const {
      image_uri: imageUri,
      top,
      left,
      width,
      height,
      font_color: fontColor,
      font_family: fontFamily,
    } = personalizeImage;

    return (
      <div className={b()} ref={this.veil} onClick={this.closeModal} role="button" tabIndex="0">
        <div className={b('image-block')}>
          <Image className={b('frame')} src={imageUri} alt="frame" width='100%' height='100%' />
          <div
            className={b('nameplate-block')}
            style={{
              height: `${height}%`,
              width: `${width}%`,
              left: `${left}%`,
              top: `${top}%`,
            }}
          >
            <div className={b('text')} style={{ color: fontColor, fontFamily }}>
              {personalizeText}
            </div>
          </div>
        </div>
        <button className={b('close-btn')} type="button" onClick={close}>
          <CloseIcon2 />
        </button>
      </div>
    );
  }
}

PhotoArtPreviewPopup.propTypes = propsTypes;
PhotoArtPreviewPopup.defaultProps = defaultProps;

const stateProps = (state) => ({
  options: optionsSelector(state),
});

export default connect(stateProps)(PhotoArtPreviewPopup);
