const TITLE = 'We are preparing your \nfile(s) for download.';
const TEXT =
  'Please check your computer’s \ndesignated download folder \nonce the download is complete.';
const SECOND_TEXT = 'Please wait a few moments and \nthe download will begin.';
const SECOND_TITLE = 'Files are now downloading';

export { TITLE, TEXT, SECOND_TEXT, SECOND_TITLE };
