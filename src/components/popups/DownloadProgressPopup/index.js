import React, { useEffect, useRef } from 'react';
import bem from 'src/utils/bem';
import PropTypes from 'prop-types';
import colors from 'src/styles/colors.json';
import { connect } from 'react-redux';
import dynamic from 'next/dynamic';
import {
  downloadedPercentageSelector,
  downloadSpeedSelector,
} from 'src/redux/folder/selectors';
import { SECOND_TEXT, SECOND_TITLE, TEXT, TITLE } from './constants';
import styles from './index.module.scss';

const CloseIcon2 = dynamic(() => import('src/components/svg/CloseIcon2'));
const DownloadFileIcon = dynamic(() => import('src/components/svg/DownloadFileIcon'));

const darkBlue = colors['$dark-slate-blue-color'];

const b = bem('download-progress-popup', styles);

const defaultProps = {
  className: '',
  close: () => {},
  isLoaded: false,
  downloadedPercentage: 0,
  downloadSpeed: 0,
};

const propsTypes = {
  className: PropTypes.string,
  close: PropTypes.func,
  isLoaded: PropTypes.bool,
  downloadedPercentage: PropTypes.number,
  downloadSpeed: PropTypes.number,
};

function DownloadProgressPopup(props) {
  const ref = useRef();
  const { className, close, isLoaded, downloadedPercentage, downloadSpeed } = props;

  useEffect(() => {
    if (isLoaded) {
      ref.current = setTimeout(close, 3000);
    }
    return () => clearTimeout(ref.current);
  });

  return (
    <div className={b({ mix: className })}>
      <div className={b('content-block')}>
        <button className={b('close-btn')} type="button" onClick={close}>
          <CloseIcon2 />
        </button>
        <span className={b('title')}>{isLoaded ? SECOND_TITLE : TITLE}</span>
        {isLoaded ? (
          <div className={b('success-wrap')}>
            <DownloadFileIcon className={b('select-icon')} stroke="none" fill={darkBlue} />
          </div>
        ) : (
          <>
            <div className={b('progress-wrap')}>
              <div className={b('progress-block', { loaded: isLoaded })} style={{width: `${downloadedPercentage.toFixed(2)}%`}} />
            </div>
            <div className={b('download-speed')}>{`Speed ${downloadSpeed.toFixed(2)} Mb/s Downloaded (${downloadedPercentage.toFixed(2)}%)` }</div>
          </>
          )}
        <span className={b('text')}>{isLoaded ? TEXT : SECOND_TEXT}</span>
      </div>
    </div>
  );
}

DownloadProgressPopup.propTypes = propsTypes;
DownloadProgressPopup.defaultProps = defaultProps;

const stateProps = (state) => ({
  downloadedPercentage: downloadedPercentageSelector(state),
  downloadSpeed: downloadSpeedSelector(state),
});

export default connect(stateProps, null)(DownloadProgressPopup);
