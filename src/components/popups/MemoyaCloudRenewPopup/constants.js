const TITLE = 'Your memories have missed you! \nWhere have you been?';
const SUBTITLE = 'We’ve archived your files, but don’t worry! \nYou can still get them back.';
const TITLE_2 = 'Great idea! Let’s renew your ******* subscription.';
const SUBTITLE_2 = 'Renew now to securely preserve, access and share your files.';
const TITLE_3 = 'Upgrade your ******* membership\nnow at our yearly discounted rate.';
const SUBTITLE_3 =
  'Always keep your memories a click away.\nAccess, preserve, share and create\nhandcrafted photo art anytime.';
const BOTTOM_TEXT =
  'You are agreeing to a one year subscription to ******* Cloud service. \nYour subscription will automatically renew yearly & can be cancelled anytime.';
const PRICE = '$50 yearly';

const PAYMENT_METHOD = {
  savedCard: 'savedCard',
  card: 'card',
  paypal: 'paypal',
};
const SHOULD_BE_NUMBER = 'should be number';
const WRONG_CARD_NUMBER = 'Wrong card number';
const MAX_NUMBER_4 = 'Should be up to 4 symbols';
const MAX_NUMBER_3 = 'Should be up to 3 symbols';
const WRONG_CARD_DATE = 'Wrong card date';
const SHOULD_BE_FILLED = 'Should be filled';
const CHOOSE_PAYMENT = 'Choose your payment method';
const SAVED_BANK_CARD = 'Saved credit card';
const BANK_CARD = 'New credit card';
const BANK_CARD_TEXT = 'Visa, Master Card, American Express or Discover';
const PAYPAL = 'Paypal';
const PAYPAL_TEXT = 'Simple and fast';
const CARD_INFO = 'Credit card information';
const CARD_NAME = 'Name on card';
const CARD_NUMBER = 'Card number';
const CARD_EXPIRY = 'Expiry date';
const CARD_CVC = 'CVC';
const SUBSCRIBE = 'Subscribe';
const CLOUD_TEXT = '*******';
const SAVE_CARD = 'Save card to account';
const SUCCESS_SUBSCRIBE = 'You have successfully renewed your \n******* subscription.';
const SUCCESS_TEXT = 'Your memories are always \na click away';
const GREAT_NEWS = 'Great news';
const SUBSCRIPTION_NAME = 'memoya_cloud_1_year';
const AE_CARD_TYPE = 'american-express';

export {
  TITLE,
  SUBTITLE,
  TITLE_2,
  SUBTITLE_2,
  TITLE_3,
  SUBTITLE_3,
  BOTTOM_TEXT,
  PRICE,
  PAYMENT_METHOD,
  SHOULD_BE_NUMBER,
  WRONG_CARD_NUMBER,
  MAX_NUMBER_4,
  MAX_NUMBER_3,
  WRONG_CARD_DATE,
  SHOULD_BE_FILLED,
  CHOOSE_PAYMENT,
  SAVED_BANK_CARD,
  BANK_CARD,
  BANK_CARD_TEXT,
  PAYPAL,
  PAYPAL_TEXT,
  CARD_INFO,
  CARD_NAME,
  CARD_NUMBER,
  CARD_EXPIRY,
  CARD_CVC,
  SUBSCRIBE,
  CLOUD_TEXT,
  SAVE_CARD,
  SUCCESS_SUBSCRIBE,
  SUCCESS_TEXT,
  GREAT_NEWS,
  SUBSCRIPTION_NAME,
  AE_CARD_TYPE,
};
