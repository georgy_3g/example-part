import React, { Component } from 'react';
import bem from 'src/utils/bem';
import cleanCardNumber from 'src/utils/cleanCardNumber';
import formattedToCardNumber from 'src/utils/formattedToCardNumber';
import toCardExpiry from 'src/utils/toCardExpiry';
import { connect } from 'react-redux';
import cardValidator from 'card-validator';
import { Formik } from 'formik';
import * as Yup from 'yup';
import PropTypes from 'prop-types';
import { AE_CARD_MASK, CARD_EXPIRATION_DATE, CARD_MASK } from 'src/constants';
import colors from 'src/styles/colors.json';

import Image from 'next/image';
import cloudImage from 'public/img/shared/cloudBigImage.png';
import dynamic from 'next/dynamic';
import {
  cloudStatus,
  paymentSpinnerFlagSelector,
  paymentSubscribeErrorSelector,
  paymentSubscribeFailSelector,
  paypalPlanIdSelector,
  planIdSelector,
  subscriptionSelector,
} from 'src/redux/myCloud/selectors';
import { getToken } from 'src/redux/auth/selectors';
import { userPaymentInfoSelector } from 'src/redux/shared/selectors';
import {
  credCardCloudSubscribeAction,
  getCloudStatusAction,
  paypalCloudSubscribeAction,
  savedCardCloudSubscribeAction,
} from 'src/redux/myCloud/actions';
import { addPaymentInfo } from 'src/redux/shared/actions';
import styles from './index.module.scss';
import {
  AE_CARD_TYPE,
  BANK_CARD,
  BOTTOM_TEXT,
  CARD_CVC,
  CARD_EXPIRY,
  CARD_INFO,
  CARD_NUMBER,
  CHOOSE_PAYMENT,
  CLOUD_TEXT,
  GREAT_NEWS,
  MAX_NUMBER_3,
  MAX_NUMBER_4,
  PAYMENT_METHOD,
  PAYPAL,
  PRICE,
  SAVE_CARD,
  SHOULD_BE_FILLED,
  SHOULD_BE_NUMBER,
  SUBSCRIBE,
  SUBSCRIPTION_NAME,
  SUBTITLE,
  SUBTITLE_3,
  SUCCESS_SUBSCRIBE,
  SUCCESS_TEXT,
  TITLE,
  TITLE_3,
  WRONG_CARD_DATE,
  WRONG_CARD_NUMBER,
} from './constants';

const CheckBoxIcon = dynamic(() => import('src/components/svg/CheckBoxIcon'));
const ColorButton = dynamic(() => import('src/components/buttons/ColorButton'));
const CreditCardIcon = dynamic(() => import('src/components/svg/CreditCardIcon'));
const NewCustomInput = dynamic(() => import('src/components/inputs/NewCustomInput'));
const PayPalIcon = dynamic(() => import('src/components/svg/PayPalIcon'));
const PaypalSubscriptionButton = dynamic(() => import('src/components/elements/PaypalSubscriptionButton'));
const CloseIcon2 = dynamic(() => import('src/components/svg/CloseIcon2'));


const b = bem('memoya-cloud-renew-popup', styles);
const grayColor = colors['$gray-color'];
const darkBlue = colors['$pickled-bluewood-color'];
const orangeColor = colors['$burnt-sienna-color'];

const defaultProps = {
  close: () => {},
  creditCard: {},
  token: '',
  postSubscriptionId: () => {},
  paymentFail: false,
  paymentError: '',
  credCardCloudSubscribe: () => {},
  savedCardCloudSubscribe: () => {},
  isLoading: false,
  getCloudStatus: () => {},
  planId: 0,
  paypalPlanId: '0',
  addCard: () => {},
  subscription: '',
  value: 0,
};

const propsTypes = {
  close: PropTypes.func,
  creditCard: PropTypes.shape({
    cardNumber: PropTypes.string,
    cardType: PropTypes.string,
    id: PropTypes.number,
  }),
  token: PropTypes.string,
  postSubscriptionId: PropTypes.func,
  paymentFail: PropTypes.bool,
  paymentError: PropTypes.string,
  credCardCloudSubscribe: PropTypes.func,
  savedCardCloudSubscribe: PropTypes.func,
  isLoading: PropTypes.bool,
  getCloudStatus: PropTypes.func,
  planId: PropTypes.number,
  paypalPlanId: PropTypes.string,
  addCard: PropTypes.func,
  subscription: PropTypes.string,
  value: PropTypes.number,
};

class MemoyaCloudRenewPopup extends Component {
  constructor(props) {
    super(props);
    this.veil = React.createRef();

    const { creditCard } = props;
    const { cardNumber } = creditCard || {};

    this.state = {
      paymentMethod: cardNumber ? PAYMENT_METHOD.savedCard : PAYMENT_METHOD.card,
      validationSchema: Yup.object().shape({}),
      cvvMaxLength: 3,
      saveNewCard: false,
      typeCard: '',
    };
  }

  componentDidUpdate(prevProps) {
    const { isLoading, getCloudStatus, close, token } = this.props;
    const { isLoading: oldFlag } = prevProps;
    if (!isLoading && oldFlag) {
      getCloudStatus({ token });
      close();
    }
  }

  closeModal = (e) => {
    const { close } = this.props;
    if (e.target === this.veil.current) {
      close();
    }
  };

  cardOnChange = ({ setFieldValue, type, value, values, paymentMethod }) => {
    const { cardNumber } = values;

    if (type && (value || value === '')) {
      setFieldValue(type, value);
    }

    const number = cleanCardNumber(type === 'cardNumber' ? value : cardNumber);
    const cardInfo = cardValidator.number(number);
    const cvvMaxLength = cardInfo.card ? cardInfo.card.code.size : 3;

    const cardSchema =
      paymentMethod === PAYMENT_METHOD.card
        ? {
            cardNumber: Yup.string()
              .typeError(SHOULD_BE_NUMBER)
              .test('required', WRONG_CARD_NUMBER, (val) => {
                if (val) {
                  const isCardValidInfo = cardValidator.number(cleanCardNumber(val));
                  return val && Boolean(cleanCardNumber(val).length) && isCardValidInfo.isValid;
                }
                return false;
              }),
            cardExpirationDate: Yup.string()
              .test('required', SHOULD_BE_FILLED, (val) => val && toCardExpiry(val).length)
              .test('count', MAX_NUMBER_4, (val) => val && toCardExpiry(val).length === 4)
              .test('count', WRONG_CARD_DATE, (val) => {
                const cardDateInfo = cardValidator.expirationDate(val);
                return val && toCardExpiry(val).length && cardDateInfo.isValid;
              }),
            cardCode: Yup.number()
              .test(
                'count',
                Number(cvvMaxLength) === 3 ? MAX_NUMBER_3 : MAX_NUMBER_4,
                (_, schemaValue) => {
                  const { from } = schemaValue;
                  const {
                    value: { cardNumber: formCardNumber = '', cardCode },
                  } = from[0];
                  if (formCardNumber && cardCode) {
                    const newNumber = cleanCardNumber(formCardNumber);
                    const newCardInfo = cardValidator.number(newNumber);
                    const newCvvMaxLength = newCardInfo.card ? newCardInfo.card.code.size : 3;
                    return (
                      cardCode && String(cardCode).length === newCvvMaxLength && newCardInfo.isValid
                    );
                  }
                  return false;
                },
              )
              .integer()
              .typeError(SHOULD_BE_NUMBER)
              .required(SHOULD_BE_FILLED),
          }
        : {};

    this.setState({
      validationSchema: Yup.object().shape({
        ...cardSchema,
      }),
      cvvMaxLength,
      typeCard: cardInfo.card ? cardInfo.card.type : '',
    });
  };

  handlePayment = (paymentMethod, values) => () => {
    this.cardOnChange({ values, paymentMethod });
    this.setState({ paymentMethod });
  };

  onSubmit = (values) => {
    const { cardExpirationDate, cardNumber, cardCode } = values;
    const { credCardCloudSubscribe, savedCardCloudSubscribe, token, addCard } = this.props;
    const { paymentMethod, saveNewCard } = this.state;
    if (paymentMethod === PAYMENT_METHOD.card) {
      credCardCloudSubscribe({
        token,
        objectData: {
          cardExpirationDate: toCardExpiry(cardExpirationDate),
          cardNumber: cleanCardNumber(cardNumber),
          cardCode,
        },
      });
      if (saveNewCard) {
        addCard({
          cardExpirationDate: toCardExpiry(cardExpirationDate),
          cardNumber: cleanCardNumber(cardNumber),
          cardCode,
        });
      }
    } else if (paymentMethod === PAYMENT_METHOD.savedCard) {
      savedCardCloudSubscribe({ token });
    }
  };

  setSaveNewCard = () => {
    const { saveNewCard } = this.state;
    this.setState({ saveNewCard: !saveNewCard });
  };

  render() {
    const {
      close,
      creditCard,
      token,
      postSubscriptionId,
      paymentFail,
      paymentError,
      planId,
      paypalPlanId,
      subscription,
      value: valueDay,
    } = this.props;

    const { cardNumber } = creditCard;
    const { validationSchema, cvvMaxLength, paymentMethod, saveNewCard, typeCard } = this.state;

    return (
      <div className={b()} ref={this.veil} onClick={this.closeModal} role="button" tabIndex="0">
        {subscription && subscription === SUBSCRIPTION_NAME ? (
          <div className={b('popup-block', { success: true })}>
            <button className={b('close-btn')} type="button" onClick={close}>
              <CloseIcon2 />
            </button>
            <div className={b('success-subscribe-wrap')}>
              <Image
                className={b('cloud-image', { success: true })}
                src={cloudImage}
                width="100%"
                height="100%"
                alt="*******"
              />
              <div className={b('subscription-success-title')}>{SUCCESS_SUBSCRIBE}</div>
              <div className={b('subscription-success-text')}>{SUCCESS_TEXT}</div>
              <ColorButton text={GREAT_NEWS} backGroundColor={orangeColor} onClick={close} />
            </div>
            <div className={b('success-stripe-wrapper')}>
              <div className={b('success-stripe-one')} />
              <div className={b('success-stripe-two')} />
              <div className={b('success-square-one')} />
              <div className={b('success-square-two')} />
            </div>
          </div>
        ) : (
          <div className={b('popup-block')}>
            <button className={b('close-btn')} type="button" onClick={close}>
              <CloseIcon2 />
            </button>
            <div className={b('top-block')}>
              <span className={b('title')}>
                {valueDay > 0 && valueDay <= 30 && subscription === 'memoya_cloud_1_month'
                  ? TITLE_3
                  : TITLE}
              </span>
              <span className={b('subtitle')}>
                {valueDay > 0 && valueDay <= 30 && subscription === 'memoya_cloud_1_month'
                  ? SUBTITLE_3
                  : SUBTITLE}
              </span>
            </div>
            <div className={b('content-block')}>
              <div className={b('price-block-wrap')}>
                <div className={b('price-block')}>
                  <img
                    className={b('cloud-image')}
                    src={cloudImage.src}
                    alt="*******"
                  />
                  <span className={b('cloud-text')}>{CLOUD_TEXT}</span>
                  <span className={b('price')}>{PRICE}</span>
                  <div className={b('stripe-one')} />
                  <div className={b('stripe-two')} />
                  <div className={b('stripe-three')} />
                </div>
                <span className={b('bottom-text')}>{BOTTOM_TEXT}</span>
              </div>
              <div className={b('form-wrap')}>
                <Formik
                  initialValues={{
                    cardExpirationDate: '',
                    cardNumber: '',
                    cardCode: '',
                  }}
                  validationSchema={validationSchema}
                  onSubmit={(values, actions) => {
                    this.onSubmit(values);
                    actions.resetForm();
                  }}
                  render={({ errors, handleSubmit, touched, setFieldValue, values }) => (
                    <form onSubmit={handleSubmit} className={b('payment')}>
                      <div className={b('payment-method')}>
                        <p className={b('payment-method-title')}>{CHOOSE_PAYMENT}</p>
                        {Boolean(cardNumber) && (
                          <>
                            <div className={b('method-text-wrapper')}>
                              <div className={b('method-icon')}>
                                <CreditCardIcon
                                  fill={
                                    paymentMethod === PAYMENT_METHOD.savedCard
                                      ? darkBlue
                                      : grayColor
                                  }
                                />
                              </div>
                              <label htmlFor className={b('method-checkbox')}>
                                <p className={b('method-description')}>
                                  {formattedToCardNumber(cardNumber, true)}
                                </p>
                                <input
                                  className={b('input')}
                                  type="checkbox"
                                  onClick={this.handlePayment(PAYMENT_METHOD.savedCard, values)}
                                  checked={paymentMethod === PAYMENT_METHOD.savedCard}
                                />
                                <span className={b('custom')} />
                              </label>
                            </div>
                            <div className={b('separator')} />
                          </>
                        )}
                        <div className={b('method-text-wrapper')}>
                          <div className={b('method-icon')}>
                            <CreditCardIcon
                              fill={paymentMethod === PAYMENT_METHOD.card ? darkBlue : grayColor}
                            />
                          </div>
                          <label htmlFor className={b('method-checkbox')}>
                            <p className={b('method-description')}>{BANK_CARD}</p>
                            <input
                              className={b('input')}
                              type="checkbox"
                              onClick={this.handlePayment(PAYMENT_METHOD.card, values)}
                              checked={paymentMethod === PAYMENT_METHOD.card}
                            />
                            <span className={b('custom')} />
                          </label>
                        </div>
                        <div className={b('separator')} />
                        <div className={b('method-text-wrapper')}>
                          <div className={b('method-icon')}>
                            <PayPalIcon
                              fill={paymentMethod === PAYMENT_METHOD.paypal ? darkBlue : grayColor}
                            />
                          </div>
                          <label htmlFor className={b('method-checkbox')}>
                            <p className={b('method-description')}>{PAYPAL}</p>
                            <input
                              className={b('input')}
                              type="checkbox"
                              onClick={this.handlePayment(PAYMENT_METHOD.paypal, values)}
                              checked={paymentMethod === PAYMENT_METHOD.paypal}
                            />
                            <span className={b('custom')} />
                          </label>
                        </div>
                        <div className={b('card-info')}>
                          {paymentMethod === PAYMENT_METHOD.card && (
                            <>
                              <div className={b('info-form-row')}>
                                <span className={b('card-info-title')}>{CARD_INFO}</span>
                              </div>
                              <div className={b('info-form-row')}>
                                <div className={b('column-input', { 'full-width': true })}>
                                  <NewCustomInput
                                    className={b('column-input-field')}
                                    onChange={({ target: { value } }) =>
                                      this.cardOnChange({
                                        setFieldValue,
                                        type: 'cardNumber',
                                        value,
                                        values,
                                        paymentMethod,
                                      })}
                                    name="cardNumber"
                                    placeholder={CARD_NUMBER}
                                    value={values.cardNumber}
                                    isTouched={touched.cardNumber}
                                    error={errors.cardNumber}
                                    isValid={!errors.cardNumber}
                                    withMask
                                    mask={typeCard === AE_CARD_TYPE ? AE_CARD_MASK : CARD_MASK}
                                  />
                                </div>
                              </div>
                              <div className={b('info-form-row')}>
                                <div className={b('column-input')}>
                                  <NewCustomInput
                                    className={b('column-input-field')}
                                    onChange={({ target: { value } }) =>
                                      this.cardOnChange({
                                        setFieldValue,
                                        type: 'cardExpirationDate',
                                        value,
                                        values,
                                        paymentMethod,
                                      })}
                                    name="cardExpirationDate"
                                    placeholder={CARD_EXPIRY}
                                    value={values.cardExpirationDate}
                                    isTouched={touched.cardExpirationDate}
                                    error={errors.cardExpirationDate}
                                    isValid={!errors.cardExpirationDate}
                                    withMask
                                    mask={CARD_EXPIRATION_DATE}
                                  />
                                </div>
                                <div className={b('column-input')}>
                                  <NewCustomInput
                                    className={b('column-input-field')}
                                    onChange={({ target: { value } }) =>
                                      this.cardOnChange({
                                        setFieldValue,
                                        type: 'cardCode',
                                        value,
                                        values,
                                        paymentMethod,
                                      })}
                                    name="cardCode"
                                    placeholder={CARD_CVC}
                                    value={values.cardCode}
                                    isTouched={touched.cardCode}
                                    error={errors.cardCode}
                                    isValid={!errors.cardCode}
                                    maxLength={cvvMaxLength}
                                  />
                                </div>
                              </div>
                            </>
                          )}
                          {paymentError && paymentFail && (
                            <span className={b('error')}>{paymentError}</span>
                          )}
                          {paymentMethod !== PAYMENT_METHOD.paypal ? (
                            <div className={b('submit-btn')}>
                              <ColorButton
                                className={b('submit-btn')}
                                backGroundColor={orangeColor}
                                text={SUBSCRIBE}
                                type="submit"
                              />
                              {paymentMethod === PAYMENT_METHOD.card && (
                                <div
                                  className={b('save-new-card')}
                                  role="button"
                                  tabIndex={-1}
                                  onClick={this.setSaveNewCard}
                                >
                                  <div className={b('save-new-card-icon')}>
                                    <CheckBoxIcon stroke={saveNewCard ? darkBlue : 'none'} />
                                  </div>
                                  <div className={b('save-new-card-text')}>{SAVE_CARD}</div>
                                </div>
                              )}
                            </div>
                          ) : (
                            <PaypalSubscriptionButton
                              token={token}
                              postSubscriptionId={postSubscriptionId}
                              planId={planId}
                              paypalPlanId={paypalPlanId}
                            />
                          )}
                        </div>
                      </div>
                    </form>
                  )}
                />
              </div>
            </div>
          </div>
        )}
      </div>
    );
  }
}

MemoyaCloudRenewPopup.propTypes = propsTypes;
MemoyaCloudRenewPopup.defaultProps = defaultProps;

const stateProps = (state) => ({
  value: cloudStatus(state),
  token: getToken(state),
  creditCard: userPaymentInfoSelector(state),
  paymentFail: paymentSubscribeFailSelector(state),
  paymentError: paymentSubscribeErrorSelector(state),
  isLoading: paymentSpinnerFlagSelector(state),
  planId: planIdSelector(state),
  paypalPlanId: paypalPlanIdSelector(state),
  subscription: subscriptionSelector(state),
});

const actions = {
  postSubscriptionId: paypalCloudSubscribeAction,
  credCardCloudSubscribe: credCardCloudSubscribeAction,
  savedCardCloudSubscribe: savedCardCloudSubscribeAction,
  getCloudStatus: getCloudStatusAction,
  addCard: addPaymentInfo,
};

export default connect(stateProps, actions)(MemoyaCloudRenewPopup);
