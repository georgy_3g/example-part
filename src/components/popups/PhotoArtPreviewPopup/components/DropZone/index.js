import React, { useCallback } from 'react';
import bem from 'src/utils/bem';
import PropTypes from 'prop-types';
import { useDropzone } from 'react-dropzone';
import dynamic from "next/dynamic";
import styles from './index.module.scss';

const ColorButton = dynamic(() => import('src/components/buttons/ColorButton'));
const AttentionIcon = dynamic(() => import('src/components/svg/AttentionIcon'));

const b = bem('drop-zone', styles);

const UPLOAD_PHOTOS = 'Upload background';
const DRAG_TO_UPLOAD = 'or drag your photos here';
const DROP_HERE = 'Drag your photos here';
const TOP_TEXT = 'Upload a horizontal photo of your wall';

const propTypes = {
  className: PropTypes.string,
  selectFile: PropTypes.func,
};

const defaultProps = {
  selectFile: () => {},
  className: '',
};

function DropZone(props) {
  const { className, selectFile } = props;

  const onDrop = useCallback((files) => {
    selectFile(files);
  }, []);

  const { getRootProps, getInputProps, isDragActive } = useDropzone({ onDrop });

  return (
    <div className={b({ wrapper: true, mix: className })} {...getRootProps()}>
      <input {...getInputProps()} multiple={false} accept=".jpeg,.jpg,.png" />
      {isDragActive ? (
        <p className={b('drop-text')}>{DROP_HERE}</p>
      ) : (
        <div className={b('select-block')}>
          <div className={b('top-block')}>
            <AttentionIcon className={b('top-icon')} />
            <span className={b('top-text')}>{TOP_TEXT}</span>
          </div>
          <ColorButton className={b('select-btn')} text={UPLOAD_PHOTOS} />
          <p className={b('select-text')}>{DRAG_TO_UPLOAD}</p>
        </div>
      )}
    </div>
  );
}

DropZone.propTypes = propTypes;
DropZone.defaultProps = defaultProps;

export default DropZone;
