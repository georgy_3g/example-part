import React, { Component, Fragment } from 'react';
import bem from 'src/utils/bem';
import generateRotateStyle from 'src/utils/generateRotateStyle';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import dynamic from "next/dynamic";
import DropZone from './components/DropZone';
import styles from './index.module.scss';

const CustomRangeInput = dynamic(() => import('src/components/inputs/CustomRangeInput'));
const ChangePhotoIcon = dynamic(() => import('src/components/svg/ChangePhotoIcon'));

const b = bem('photo-art-preview-popup', styles);

const keys = {
  37: 1,
  38: 1,
  39: 1,
  40: 1,
};

const defaultProps = {
  matValue: false,
  mat: {},
  selectedOption: {},
  children: null,
  portalBlock: null,
  selectedPersonalize: null,
  personalizeText: '',
  isShowPersonalizeBlock: false,
};

const propsTypes = {
  photo: PropTypes.string.isRequired,
  display: PropTypes.shape({
    fullDisplay: PropTypes.shape({
      imageUri: PropTypes.string,
      frameScale: PropTypes.number,
      frameScaleX: PropTypes.number,
      frameLeft: PropTypes.number,
      frameTop: PropTypes.number,
      framePerspective: PropTypes.number,
      framePerspectiveOrigin: PropTypes.string,
      frameRotateX: PropTypes.number,
      frameRotateY: PropTypes.number,
      frameRotateZ: PropTypes.number,
    }),
    frameImageUri: PropTypes.string,
    frameImageOptions: PropTypes.shape({}),
    isCustomBackground: PropTypes.bool,
  }).isRequired,
  matValue: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
  mat: PropTypes.shape({
    paddingHorizontal: PropTypes.number,
    paddingVertical: PropTypes.number,
    innerType: PropTypes.string,
  }),
  selectedOption: PropTypes.shape({
    backgroundImageOptions: PropTypes.shape({}),
    backgroundImageUrl: PropTypes.string,
  }),
  children: PropTypes.node,
  portalBlock: PropTypes.node,
  selectedPersonalize: PropTypes.shape({}),
  personalizeText: PropTypes.string,
  isShowPersonalizeBlock: PropTypes.bool,
};

const UPLOAD_BUTTON_TEXT = 'Upload your own background';
const UPLOAD_ANOTHER_BACKGROUND = 'Upload another background';
const RESET_BACKGROUND = 'Reset to original background';

class PhotoArtPreviewPopup extends Component {
  constructor(props) {
    super(props);

    this.wrap = null;
    this.coordinates = null;
    this.idMouseDown = false;
    this.coordinates = null;
    this.styles = {};
    this.customBackground = null;

    this.state = {
      isShowPopup: false,
      isShowUpload: true,
      uploadedBackground: null,
      imageStyle: {},
      zoomValue: 100,
    };
  }

  componentDidMount() {
    document.addEventListener('mouseup', this.onMouseMove(true));
    document.addEventListener('touchend', this.onMouseMove(true));
  }

  componentWillUnmount() {
    document.removeEventListener('mouseup', this.onMouseMove(true));
    document.removeEventListener('touchend', this.onMouseMove(true));
    this.wrap = null;
    this.coordinates = null;
    this.coordinates = null;
    this.customBackground = null;
    this.coordinates = null;
  }

  preventDefault = (e) => {
    e.preventDefault();
  };

  // eslint-disable-next-line consistent-return
  preventDefaultForScrollKeys = (e) => {
    if (keys[e.keyCode]) {
      this.preventDefault(e);
      return false;
    }
  };

  generateImageBlock = (style) => {
    const { photo, display, matValue, mat } = this.props;

    const { paddingVertical, paddingHorizontal, innerType } = mat || {};

    const {
      fullDisplay: { frameRotateX, frameRotateY, frameRotateZ },
      frameImageOptions = {},
    } = display;

    const top = matValue ? paddingVertical : 0;
    const left = matValue ? paddingHorizontal : 0;

    const { x, y, height, width } = frameImageOptions || {};

    const blockStyle = {
      backgroundColor: `#${matValue}`,
      height: `${height}%`,
      width: `${width}%`,
      left: `${x}%`,
      top: `${y}%`,
      transform: generateRotateStyle(frameRotateX, frameRotateY, frameRotateZ),
    };

    const imageStyle = {
      height: `${100 - top * 2}%`,
      borderRadius: `${innerType === 'circle' ? 50 : 0}%`,
      width: `${100 - left * 2}%`,
      top: `${top}%`,
      left: `${left}%`,
    };

    return (
      <div
        className={b('art-wrap')}
        style={{
          ...style,
        }}
      >
        <div className={b('image-wrap')} style={blockStyle}>
          <img className={b('art-photo')} style={imageStyle} src={photo} alt="art" />
          {Boolean(matValue) && <div className={b('shadow-block')} style={imageStyle} />}
        </div>
      </div>
    );
  };

  openPopup = () => {
    this.setState({ isShowPopup: true });
  };

  closePopup = () => {
    this.setState({ isShowPopup: false });
  };

  toPixels = (data) => {
    const newData = {};
    Object.keys(data).forEach((key) => {
      const item = String(data[key]);
      if (item && item.indexOf('px') === -1) {
        newData[key] = `${item}px`;
      } else {
        newData[key] = item;
      }
    });
    return newData;
  };

  selectFile = (files) => {
    const file = files[0];
    const reader = new FileReader();

    reader.onload = async () => {
      const img = new Image();
      img.onload = () => {
        const { clientWidth, clientHeight } = this.wrap;
        const { width, height } = img;

        if (width / height > clientWidth / clientHeight) {
          const newHeight = clientHeight;
          const newWidth = (width * clientHeight) / height;
          const newLeft = (clientWidth - newWidth) / 2;
          const newTop = 0;
          const imageStyle = {
            height: newHeight,
            width: newWidth,
            left: newLeft,
            top: newTop,
          };

          this.setState({
            uploadedBackground: reader.result,
            isShowPopup: false,
            imageStyle,
            startStyle: imageStyle,
            isShowUpload: false,
          });
        } else {
          const newWidth = clientWidth;
          const newHeight = (height * clientWidth) / width;
          const newLeft = 0;
          const newTop = (clientHeight - newHeight) / 2;
          const imageStyle = {
            height: newHeight,
            width: newWidth,
            left: newLeft,
            top: newTop,
          };

          this.setState({
            uploadedBackground: reader.result,
            isShowPopup: false,
            imageStyle,
            startStyle: imageStyle,
            isShowUpload: false,
          });
        }
      };
      img.src = reader.result;
    };
    if (file) {
      reader.readAsDataURL(file);
    }
  };

  onResize = (value) => {
    const {
      startStyle: { width, height },
      imageStyle: { width: oldWidth, height: oldHeight, left, top },
    } = this.state;
    const newWidth = (width * value) / 100;
    const nawHeight = (height * value) / 100;
    const newTop = top - nawHeight / 2 + oldHeight / 2;
    const newLeft = left - newWidth / 2 + oldWidth / 2;

    const imageStyle = this.getValidPosition({
      width: newWidth,
      height: nawHeight,
      top: newTop,
      left: newLeft,
    });
    this.setState({ imageStyle, zoomValue: value });
  };

  getValidPosition = (photoStyles) => {
    const { width, height, top, left } = photoStyles;
    const { clientHeight, clientWidth } = this.wrap;

    const isLeftValid = left <= 0;
    const isTopValid = top <= 0;
    const isRightValid = left + width >= Number(clientWidth);
    const isBottomValid = top + height >= Number(clientHeight);

    const newPhotoStyle = {
      width,
      height,
      top,
      left,
    };

    if (!isLeftValid) {
      newPhotoStyle.left = 0;
    }

    if (!isTopValid) {
      newPhotoStyle.top = 0;
    }

    if (!isRightValid) {
      newPhotoStyle.left = Number(clientWidth) - width;
    }

    if (!isBottomValid) {
      newPhotoStyle.top = Number(clientHeight) - height;
    }

    return newPhotoStyle;
  };

  disableScroll = () => {
    if (document.body.clientWidth <= 1024) {
      window.addEventListener('DOMMouseScroll', this.preventDefault, false);
      window.addEventListener(this.wheelEvent, this.preventDefault, this.wheelOpt);
      window.addEventListener('touchmove', this.preventDefault, this.wheelOpt);
      window.addEventListener('keydown', this.preventDefaultForScrollKeys, false);
    }
  };

  enableScroll = () => {
    if (document.body.clientWidth <= 1024) {
      window.removeEventListener('DOMMouseScroll', this.preventDefault, false);
      window.removeEventListener(this.wheelEvent, this.preventDefault, this.wheelOpt);
      window.removeEventListener('touchmove', this.preventDefault, this.wheelOpt);
      window.removeEventListener('keydown', this.preventDefaultForScrollKeys, false);
    }
  };

  updateBackgroundStyle = (style) => {
    const styleData = this.toPixels(style);

    const styleText = Object.entries(styleData).reduce(
      (acc, [key, value], index) => `${acc}${index > 0 ? ' ' : ''}${key}: ${value};`,
      '',
    );

    this.customBackground.setAttribute('style', styleText);
  };

  onMouseDown = (e) => {
    const { imageStyle } = this.state;
    this.disableScroll();
    this.idMouseDown = true;
    this.styles = { ...imageStyle };
    const { screenX, screenY, touches } = e;
    const { screenX: mobScreenX, clientY: mobScreenY } =
      touches && touches.length ? touches[0] : { screenX: 0, screenY: 0 };
    this.coordinates = { x: screenX || mobScreenX, y: screenY || mobScreenY };
  };

  onMouseMove = (isEnd) => (e) => {
    if (this.idMouseDown) {
      const { screenX = 0, screenY = 0, touches, changedTouches } = e;
      const touchesArr = isEnd ? changedTouches : touches;
      const { screenX: mobScreenX, clientY: mobScreenY } =
        touchesArr && touchesArr.length ? touchesArr[0] : { screenX: 0, screenY: 0 };
      const { x, y } = this.coordinates;
      const { top, left } = this.styles;
      const newTop = Number(top) - Number(y) + (screenY || Math.round(mobScreenY));
      const newLeft = Number(left) - Number(x) + (screenX || Math.round(mobScreenX));

      const newImageStyles = this.getValidPosition({
        ...this.styles,
        top: newTop,
        left: newLeft,
      });

      if (isEnd) {
        this.idMouseDown = false;
        this.enableScroll();
        this.coordinates = null;
        this.styles = {};
        this.setState({ imageStyle: { ...newImageStyles } });
      } else {
        this.style = { ...newImageStyles };
        this.updateBackgroundStyle(newImageStyles);
      }
    }
  };

  changeToUploadPopup = () => {
    this.setState({ isShowUpload: true });
  };

  removeBackground = () => {
    this.setState({ isShowUpload: true, uploadedBackground: null, isShowPopup: false });
  };

  getPersonalizeBlockStyle = () => {
    const {
      selectedPersonalize: {
        personalizeDisplayDate: {
          rotateX,
          rotateY,
          rotateZ,
          scale,
          scaleX,
          top,
          left,
        } = {},
        framePersonalizeUri,
      } = {},
    } = this.props;

    return {
      height: `${scale}%`,
      width: `${scaleX}%`,
      left: `${left}%`,
      top: `${top}%`,
      transform: generateRotateStyle(rotateX, rotateY, rotateZ),
      ...(framePersonalizeUri ? { backgroundImage: `url(${framePersonalizeUri})` } : {}),
    };
  };

  getPersonalizeTextStyle = () => {
    const { selectedPersonalize = {}, display } = this.props;

    const { fullDisplay } = display || {};

    const { frameScale } = fullDisplay || {};

    const {
      fontColor,
      fontSize,
      fontWeight,
      fontFamily,
      framePersonalizeOptions: {
        height,
        width,
        x,
        y,
      } = {},
      frameDisplayData: {
        frameScale: personalizeScale,
      } = {},
    } = selectedPersonalize || {};

    if (!selectedPersonalize || !Object.keys(selectedPersonalize).length) {
      return {};
    }

    const displayWidth = document.body.clientWidth;

    const calcSize = fontSize * (frameScale / personalizeScale);

    let newSize;

    if (displayWidth >= 1920) {
      newSize = `${calcSize}px`;
    } else if (displayWidth < 1920 && displayWidth >= 1024) {
      newSize = `${calcSize / 19.2}vw`;
    } else {
      newSize = `${calcSize / 10.24}vw`;
    }

    const style = {
      ...(fontColor ? { color: fontColor } : {}),
      ...(newSize ? { fontSize: newSize } : {}),
      ...(fontWeight ? { fontWeight } : {}),
      ...(fontFamily ? { fontFamily } : {}),
      height: `${height || 100}%`,
      width: `${width || 100}%`,
      top: `${y || 0}%`,
      left: `${x || 0}%`,
    };
    return style;
  };

  render() {
    const { display, children, portalBlock, personalizeText, isShowPersonalizeBlock } = this.props;
    const { frameImageUri = '', isCustomBackground = false, fullDisplay } = display;
    const { isShowPopup, uploadedBackground, imageStyle, zoomValue, isShowUpload } = this.state;

    const {
      imageUri: displaysUri,
      frameScale,
      frameScaleX,
      frameTop,
      frameLeft,
      framePerspective,
      framePerspectiveOrigin,
      backgroundLeft,
      backgroundScale,
      backgroundTop,
    } = fullDisplay || {};

    const style = {
      height: `${frameScale}%`,
      width: `${frameScaleX}%`,
      left: `${frameLeft}%`,
      top: `${frameTop}%`,
      perspective: `${framePerspective}px`,
      perspectiveOrigin: framePerspectiveOrigin,
    };

    return (
      <div className={b()} role="button" tabIndex="0">
        <div
          className={b('photo-block')}
          ref={(e) => {
            this.wrap = e;
          }}
        >
          {children}
          <img
            className={b('background-image')}
            alt="background"
            src={displaysUri}
            style={{
              width: `${backgroundScale}%`,
              left: `-${backgroundLeft}%`,
              top: `-${backgroundTop}%`,
            }}
          />
          {isCustomBackground && (
            <>
              {uploadedBackground && (
                <>
                  <img
                    className={b('custom-background-image')}
                    ref={(e) => {
                      this.customBackground = e;
                    }}
                    alt="background"
                    src={uploadedBackground}
                    style={this.toPixels(imageStyle)}
                  />
                  <div className={b('range-wrap')}>
                    <CustomRangeInput
                      className={b('range')}
                      value={zoomValue}
                      onChange={this.onResize}
                    />
                  </div>
                  <div
                    className={b('move-zone')}
                    onMouseDown={this.onMouseDown}
                    onMouseMove={this.onMouseMove(false)}
                    onTouchStart={this.onMouseDown}
                    onTouchMove={this.onMouseMove(false)}
                    role="button"
                    tabIndex="0"
                  />
                </>
              )}
              <button className={b('upload-button')} type="button" onClick={this.openPopup}>
                {UPLOAD_BUTTON_TEXT}
              </button>
            </>
          )}
          {this.generateImageBlock(style)}
          <img className={b('background')} src={frameImageUri} alt="display" style={style} />
          {isShowPersonalizeBlock && (
            <div className={b('personalize-wrap')} style={style}>
              <div
                className={b('personalize-data-block')}
                style={this.getPersonalizeBlockStyle() || {}}
              >
                <div
                  className={b('personalize-text-data')}
                  style={this.getPersonalizeTextStyle() || {}}
                >
                  {personalizeText}
                </div>
              </div>
            </div>
          )}
        </div>
        {isShowPopup && (
          <div className={b('popup-wrap')}>
            <div className={b('upload-popup')}>
              <button className={b('close-btn')} type="button" onClick={this.closePopup} />
              {isShowUpload ? (
                <DropZone className={b('drop')} selectFile={this.selectFile} />
              ) : (
                <>
                  <ChangePhotoIcon className={b('popup-icon')} />
                  <div className={b('button-block')}>
                    <button
                      className={b('select-background-btn')}
                      type="button"
                      onClick={this.changeToUploadPopup}
                    >
                      {UPLOAD_ANOTHER_BACKGROUND}
                    </button>
                    <button
                      className={b('select-background-btn')}
                      type="button"
                      onClick={this.removeBackground}
                    >
                      {RESET_BACKGROUND}
                    </button>
                  </div>
                </>
              )}
            </div>
          </div>
        )}
        <div className={b('portal-wrap')}>
          {uploadedBackground &&
            ReactDOM.createPortal(
              <div className={b('portal-range-wrap')}>
                <CustomRangeInput
                  className={b('portal-range')}
                  inputClass={b('mobile-range-input')}
                  plusBtnClass={b('mobile-range-plus')}
                  minusBtnClass={b('mobile-range-minus')}
                  value={zoomValue}
                  onChange={this.onResize}
                />
              </div>,
              portalBlock,
            )}
        </div>
      </div>
    );
  }
}

PhotoArtPreviewPopup.propTypes = propsTypes;
PhotoArtPreviewPopup.defaultProps = defaultProps;

export default PhotoArtPreviewPopup;
