import React, { Component, createRef } from 'react';
import PropTypes from 'prop-types';
import debounce from 'lodash/debounce';
import bem from 'src/utils/bem';
import parseJwt from 'src/utils/parseJWT';
import format from 'date-fns/format';
import { connect } from 'react-redux';
import { getToken } from 'src/redux/auth/selectors';
import Colors from 'src/styles/colors.json';

import dynamic from "next/dynamic";
import { getRoomFiles } from "src/redux/folder/selectors";
import { clearRoomFiles } from "src/redux/folder/actions";
import {
  TITLE,
  SORT_ARR,
  MEMOYA_TEAM,
  YOU,
  SUB_TITLE,
  ERROR_MESSAGE,
  ADD_MESSAGES,
} from './constants';
import styles from './index.module.scss';

const DragAndDropForm = dynamic(() => import('src/components/forms/DragAndDropForm'));
const CustomSelect = dynamic(() => import('src/components/inputs/CustomSelect'));
const ClipIcon = dynamic(() => import('src/components/svg/ClipIcon'));
const CloseIcon2 = dynamic(() => import('src/components/svg/CloseIcon2'));
const PaperAirplaneIcon = dynamic(() => import('src/components/svg/PaperAirplaneIcon'));
const ExpansionDecreaseIcon = dynamic(() => import('src/components/svg/ExpansionDecreaseIcon'));

const b = bem('chat-room', styles);
const greenColor = Colors['$summer-green-color'];

const defaultProps = {
  className: '',
  headerHidden: false,
  subTitle: SUB_TITLE,
  title: TITLE,
  isModal: true,
  iconExpansionDecrease: false,
  roomId: null,
  socket: { emit: () => {}},
  openIcon: false,
  setChatImage: () => {  },
  changeSizeWindow: () => {  },
  allMessages: {},
  token: '',
  files: [],
};

const propTypes = {
  className: PropTypes.string,
  headerHidden: PropTypes.bool,
  subTitle: PropTypes.string,
  title: PropTypes.string,
  isModal: PropTypes.bool,
  iconExpansionDecrease: PropTypes.bool,
  roomId: PropTypes.number,
  socket: PropTypes.shape({
    emit: PropTypes.func,
    on: PropTypes.func
  }),
  openIcon: PropTypes.bool,
  changeSizeWindow: PropTypes.func,
  setChatImage: PropTypes.func,
  allMessages: PropTypes.shape({}),
  token: PropTypes.string,
  files: PropTypes.arrayOf({}),
};

class ChatRoom extends Component {
  resizeWindow = debounce(() => { this.isMobile(); }, 300);

  constructor(props) {
    super(props);
    const { token } = this.props;
    const { id = null } = parseJwt(token) || {};
    this.ref = createRef();
    this.state = {
      userId: id,
      textMessage: '',
      myMessages: {},
      sortBy: 'asc',
      tittleSelect: '',
      error: false,
      fontSize: this.getFontSize(),
    };
  }

  componentDidMount() {
    const { sortBy } = this.state;
    const { socket } = this.props;
    socket.on('message', ({ data, type }) => {
      if (type === 'All') {
        this.setState({ myMessages: data });
      } else {
        this.getAllMessages(sortBy);
      }
    });
    this.getAllMessages(sortBy);
    window.addEventListener('resize', this.resizeWindow);
  }

  componentDidUpdate(prevProps) {
    const { sortBy } = this.state;
    const { roomId } = this.props;
    const { roomId: prevRoomId } = prevProps;
    if (roomId !== prevRoomId){
      this.getAllMessages(sortBy);
    }
  }

  componentWillUnmount() {
    this.ref = null;
    this.clearImage();
    window.removeEventListener('resize', this.resizeWindow);
  }

  isMobile = () => {
    const { fontSize } = this.state;

    const newSize = this.getFontSize();

    if (fontSize !== newSize) {
      this.setState({ fontSize: newSize });
    }
  }

  getFontSize = () => {
    const width = window.innerWidth;
    if (width < 1920 && width > 1024) {
      return '0.729vw';
    }
    return '14px';
  }

  scrollMessage = () => {
    this.ref.current.scrollTop = this.ref.current.scrollHeight - this.ref.current.clientHeight;
  }

  getAllMessages = (sort) => {
    const { roomId, socket } = this.props;
    socket.emit('getMessages', {
      pagination: {
        limit: 100000,
        offset: 0,
      },
      sorting: [{
        column: 'date',
        order: sort,
      }],
      filter: [{
        origin: 'room',
        operator: 'in',
        target: [roomId],
      }],
    });
  };

  changeValue = (event) => {
    const { target: { value = '' } = {} } = event || { };
    this.setState({ textMessage: value });
  };

  submit = () => {
    const { textMessage } = this.state;
    const { socket, roomId, files } = this.props;
    if (!textMessage) {
      this.setState({ error: true });
      return;
    }
    const data = { roomId, message: textMessage };
    const attachmentFiles = files.map(({ key }) => key);
    const send = files.length ? { ...data, attachmentFiles } : data;
    socket.emit('sendMessage', send);
    this.clearImage();
    this.setState({ textMessage: '', error: false });
  };

  onKeyPress = (event) => {
    const { textMessage } = this.state;
    const { key = '' } = event || {};
    if (key === 'Enter' && textMessage) {
      this.submit();
    }
  };

  clearImage = (id) => {
    const { files, setChatImage } = this.props;
    if (id) {
      const newFiles = files.filter(file => file.id !== id);
      setChatImage(newFiles);
    } else {
      setChatImage([]);
    }
  }

  getReplyText = (id) => {
    const { myMessages } = this.state;
    const { messages = [] } = myMessages || {};
    const { message = '' } = messages.find(item => item.id === id) || {};
    return message;
  };

  mapMessages = () => {
    const { userId, myMessages } = this.state;
    const { isModal } = this.props;
    const { messages = [], users = [] } = myMessages || {};
    const date = new Map();

    const getDate = (messageDate) => {
      const checkDate = format(new Date(messageDate), 'dd-MM-yyyy');
      if (!date.has(checkDate)) {
        date.set(checkDate, '');
        return (
          <div className={b('date-message')}>
            {format(new Date(messageDate), 'dd-MM-yyyy')}
          </div>
        );
      }
      return null;
    };

    return (
      <div className={b('container-messages', { container: !isModal })} ref={this.ref}>
        {messages.map((item) => {
          const { id = null } = users.find(user => user.userId === userId) || {};
          const isMyMessage = item.fromChatUser === id;
          return (
            <React.Fragment key={item.id}>
              {getDate(item.date)}
              <div className={b('whose-message', { 'its-my': isMyMessage })}>
                {isMyMessage ? YOU : MEMOYA_TEAM}
              </div>
              <div className={b('text-message', { 'its-my': isMyMessage })}>
                {item.replyMessageId && (
                <div className={b('reply-message-text', { 'its-my': isMyMessage })}>
                  {this.getReplyText(item.replyMessageId)}
                </div>
                )}
                {item.message}
                <div className={b('container-images')}>
                  {Boolean(item.files.length) && item.files.map(file => (
                    <a key={file.id} href={file.url} download className={b('file-message')}>
                      <img className={b('image-message')} src={file.url} alt={file.url} />
                    </a>
                  ))}
                </div>
                <div className={b('time-message')}>
                  {format(new Date((item.date)), "h:mm aaaaa'm'")}
                </div>
              </div>
            </React.Fragment>
          );
        })}
      </div>
    );
  };

  render() {
    const {
      isModal,
      iconExpansionDecrease,
      subTitle,
      title,
      openIcon,
      changeSizeWindow,
      files,
      className,
      headerHidden,
    } = this.props;

    const {
      textMessage,
      tittleSelect,
      error,
      fontSize,
    } = this.state;

    return (
      <div className={b({ mix: className, position: !isModal })}>
        <div className={b('content-block')}>
          <div className={b('header-container', { hidden: headerHidden })}>
            <div className={b('title')}>{title}</div>
            <div className={b('sub-title')}>{subTitle}</div>
            <div role="button" tabIndex={0} onClick={changeSizeWindow}>
              {iconExpansionDecrease
              && <ExpansionDecreaseIcon open={openIcon} className={b('expansion-decrease-icon')} />}
            </div>
          </div>
          <div className={b('sort', { border: headerHidden })}>
            <CustomSelect
              className={b('sort-select')}
              selectClass={b('sort-select-input')}
              placeholder="Sort"
              value={tittleSelect}
              options={SORT_ARR}
              onChange={({ label, value }) => {
                this.setState({ tittleSelect: label, sortBy: value });
                this.getAllMessages(value);
              }}
              customStyles={{
                control: provided => ({
                  ...provided,
                  minHeight: '100% !important',
                  minWidth: '100%',
                  backgroundColor: 'transparent !important',
                  width: '100%',
                  border: 'none !important',
                  outline: 'none',
                  boxShadow: 'none',
                }),
                container: provided => ({
                  ...provided,
                  outline: 'none',
                }),
                valueContainer: provided => ({
                  ...provided,
                  height: '100%',
                  padding: '0px',
                }),
                placeholder: provided => ({
                  ...provided,
                  minHeight: '100%',
                  display: 'flex',
                  alignItems: 'center',
                  fontSize,
                }),
                indicatorsContainer: provided => ({
                  ...provided,
                  height: '100%',
                }),
              }}
            />
          </div>
          {this.mapMessages()}
          <div className={b('input-container')}>
            <input
              className={b('input-message')}
              value={textMessage}
              onChange={this.changeValue}
              onKeyDown={this.onKeyPress}
              placeholder={ADD_MESSAGES}
            />
            {error && <p className={b('error')}>{ERROR_MESSAGE}</p>}
            <DragAndDropForm
              url
              multiple={false}
              bemBlock={b}
              notPopup
              isSelectFiles
              showText={false}
              buttonClass={b('file-upload')}
              children={<ClipIcon fillStroke={greenColor} className={b('file-message-icon')} />}
            />
            <div
              className={b('send-message')}
              role="button"
              tabIndex={0}
              onClick={this.submit}
            >
              <PaperAirplaneIcon className={b('send-icon')} />
            </div>
          </div>
          {Boolean(files.length) && (
            <div className={b('container-images', { 'my-files': true })}>
              {files.map(({ url, id }) => (
                <div key={id} className={b('file-message', { 'my-files': true })}>
                  <img className={b('image-message')} src={url} alt={url} />
                  <div
                    className={b('delete-image-btn')}
                    tabIndex={0}
                    role="button"
                    onClick={() => this.clearImage(id)}
                  >
                    <CloseIcon2 className={b('delete-btn-icon')} />
                  </div>
                </div>
              ))}
            </div>
          )}
        </div>
      </div>
    );
  }
}

ChatRoom.propTypes = propTypes;
ChatRoom.defaultProps = defaultProps;

const stateProps = state => ({
  files: getRoomFiles(state),
  token: getToken(state)
});

const actions = {
  setChatImage: clearRoomFiles,
}

export default connect(stateProps, actions)(ChatRoom);
