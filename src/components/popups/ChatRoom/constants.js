const TITLE = 'Your message history';
const SUB_TITLE = 'Question about order details';
const MEMOYA_TEAM = '******* team';
const YOU = 'You';
const REPLY = 'Reply';
const ERROR_MESSAGE = 'Enter the text to send the message';
const ADD_MESSAGES = 'Message us about your order';
const SORT_ARR = [
  {
    label: 'New',
    value: 'asc',
  },
  {
    label: 'Old',
    value: 'desc',
  },
];

export { ERROR_MESSAGE, SORT_ARR, TITLE, SUB_TITLE, MEMOYA_TEAM, REPLY, YOU, ADD_MESSAGES };
