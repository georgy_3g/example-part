import React, { Component } from 'react';
import bem from 'src/utils/bem';
import PropTypes from 'prop-types';

import Image from 'next/image';
import dynamic from "next/dynamic";
import { TITLE, ITEMS } from './constants';
import styles from './index.module.scss';

const CloseIcon2 = dynamic(() => import('src/components/svg/CloseIcon2'));

const b = bem('learn-more-popup', styles);

const defaultProps = {
  close: () => {},
};

const propsTypes = {
  close: PropTypes.func,
};

class LearnMorePopup extends Component {
  constructor(props) {
    super(props);
    this.veil = React.createRef();
  }

  closeModal = (e) => {
    const { close } = this.props;
    if (e.target === this.veil.current) {
      close();
    }
  };

  render() {
    const { close } = this.props;

    return (
      <div className={b()} ref={this.veil} onClick={this.closeModal} role="button" tabIndex="0">
        <div className={b('content-block')}>
          <button className={b('close-btn')} type="button" onClick={close}>
            <CloseIcon2 />
          </button>
          <span className={b('title')}>{TITLE}</span>
          <div className={b('instruction-block')}>
            {ITEMS.map(({ iconUrl, text, key }) => (
              <div className={b('info-block')} key={key}>
                <Image src={iconUrl} className={b('info-icon')} alt="Lorem" width='100%' height='100%' />
                <span className={b('info-text')}>{text}</span>
              </div>
            ))}
          </div>
        </div>
      </div>
    );
  }
}

LearnMorePopup.propTypes = propsTypes;
LearnMorePopup.defaultProps = defaultProps;

export default LearnMorePopup;
