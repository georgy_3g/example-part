import { sizeErrorMailIcon, sizeErrorResizeIcon, sizeErrorCroppingIcon } from 'public';

const TITLE = 'Don’t worry, you have\nsome options';

const ITEMS = [
  {
    iconUrl: sizeErrorMailIcon,
    text: '1. Rescan your original photo at a\nhigher resolution or mail us your\noriginal and we can do the scanning.',
    key: 'SizeError1',
  },
  {
    iconUrl: sizeErrorResizeIcon,
    text: '2. Select a smaller size. Reducing the\nsize of your art will help to ensure a\nbetter print quality.',
    key: 'SizeError2',
  },
  {
    iconUrl: sizeErrorCroppingIcon,
    text: '3. Adjust the cropping of your image.\nCropping out will provide more\nresolution to your photo.',
    key: 'SizeError3',
  },
];

export { TITLE, ITEMS };
