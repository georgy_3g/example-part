import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import DICTIONARY from 'src/constants/dictionary';
import INPUTS_NAMES from 'src/constants/inputsNames';
import ERRORS from 'src/constants/errors';
import { Formik } from 'formik';
import bem from 'src/utils/bem';
import * as Yup from 'yup';
import dynamic from 'next/dynamic';
import { error } from 'src/redux/shared/selectors';
import { clearError, resetPassAction } from 'src/redux/shared/actions';
import styles from './index.module.scss';

const NewCustomInput = dynamic(() => import('src/components/inputs/NewCustomInput'));
const ForgotPassIcon = dynamic(() => import('src/components/svg/ForgotPassIcon'));

const b = bem('forgot-pass', styles);

const { SUBMIT, EMAIL, RESET_PASS, FORGOT_PASS_TITLE } = DICTIONARY;

const { SHOULD_BE_FILLED, INVALID_EMAIL } = ERRORS;

const defaultProps = {
  passReset: () => {},
  close: () => {},
  changePopup: () => {},
  sharedError: {},
  clear: () => {},
};

const propTypes = {
  passReset: PropTypes.func,
  close: PropTypes.func,
  changePopup: PropTypes.func,
  sharedError: PropTypes.shape({
    error: PropTypes.string,
  }),
  clear: PropTypes.func,
};

const schema = Yup.object().shape({
  email: Yup.string().email(INVALID_EMAIL).required(SHOULD_BE_FILLED),
});

function ForgotPassForm(props) {
  const { clear, sharedError } = props;
  const onSubmit = (values) => {
    const { close, changePopup, passReset } = props;
    passReset({
      ...values,
      close,
      changePopup,
      clear,
    });
  };

  useEffect(() => {
    clear();
    return () => {
      clear();
    };
  }, []);

  return (
    <div className={b()}>
      <Formik
        initialValues={{ email: '' }}
        validationSchema={schema}
        onSubmit={(values, actions) => {
          const trimmedValues = {
            email: values.email.trim().toLowerCase(),
          };
          onSubmit(trimmedValues);
          actions.resetForm();
        }}
        render={({ errors, handleSubmit, touched, handleChange, values }) => {
          const forgotPassInactive = Object.keys(errors).length && Object.keys(touched).length;

          return (
            <form onSubmit={handleSubmit} className={b('form')}>
              <fieldset className={b('fieldset')}>
                <div className={b('forgot-pass-logo')}>
                  <ForgotPassIcon className="forgot-pass__forgot-pass-logo-svg" />
                  <legend className={b('legend')}>{FORGOT_PASS_TITLE}</legend>
                </div>
                <p className={b('title')}>{RESET_PASS}</p>
                <NewCustomInput
                  className={b('input')}
                  onChange={handleChange}
                  name={INPUTS_NAMES.email}
                  placeholder={EMAIL}
                  value={values.email}
                  isTouched={touched.email}
                  error={errors.email}
                  isValid={!errors.email}
                />
                {Boolean(sharedError && sharedError.error) && (
                  <span className={b('wrong-email')}>{sharedError.error}</span>
                )}
              </fieldset>
              <button className={b('btn')} type="submit" disabled={forgotPassInactive}>
                {SUBMIT}
              </button>
            </form>
          );
        }}
      />
    </div>
  );
}

ForgotPassForm.propTypes = propTypes;
ForgotPassForm.defaultProps = defaultProps;

const mapStateToProps = (state) => ({
  sharedError: error(state),
});

const actions = {
  passReset: resetPassAction,
  clear: clearError,
};

export default connect(mapStateToProps, actions)(ForgotPassForm);
