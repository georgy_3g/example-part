import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Formik } from 'formik';
import bem from 'src/utils/bem';
import * as Yup from 'yup';
import ROUTES from 'src/constants/routes';
import ERRORS from 'src/constants/errors';
import debounce from 'lodash/debounce';
import colors from 'src/styles/colors.json';
import CustomSelect from 'src/components/inputs/CustomSelect';
import dynamic from 'next/dynamic';
import giftCardsSelect from 'src/redux/giftCards/selectors';
import { currencyPriceSelect, giftCardsPriceSelect } from 'src/redux/prices/selectors';
import ordersTypeSelect from 'src/redux/orders/selectors';
import { addToCart } from 'src/redux/orders/actions';
import styles from './index.module.scss';
import {
  FROM_PLACEHOLDER,
  MAX_FROM_TO_LENGTH,
  MAX_FROM_TO_LENGTH_FROM,
  MAX_LENGTH,
  MESSAGE,
  SUB_TOTAL,
  SUB_TOTAL_INFO,
  SUBJECT_PLACEHOLDER,
  SUBMIT_BUTTON_TEXT,
  TITLE,
  TO_PLACEHOLDER,
  VALUE_PLACEHOLDER,
} from './constants';

const CustomTextarea = dynamic(() => import('src/components/inputs/CustomTextarea'));
const NewCustomInput = dynamic(() => import('src/components/inputs/NewCustomInput'));
const RedButton = dynamic(() => import('src/components/buttons/RedButton'));
const DollarIcon = dynamic(() => import('src/components/svg/DollarIcon'));
const CheckFull = dynamic(() => import('src/components/svg/CheckFull'));

const orangeColor = colors['$burnt-sienna-red-color'];
const whiteColor = colors['$white-color'];
const greenColor = colors['$summer-green-color'];

const { SHOULD_BE_FILLED, SHOULD_BE_SELECTED } = ERRORS;

const schema = Yup.object().shape({
  to: Yup.string().required(SHOULD_BE_FILLED),
  from: Yup.string().required(SHOULD_BE_FILLED),
  subject: Yup.string().required(SHOULD_BE_FILLED),
});

const b = bem('gift-card-form', styles);

const propTypes = {
  className: PropTypes.string,
  cardsImages: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number,
    imageUri: PropTypes.string,
    name: PropTypes.string,
  })),
  cardsPrice: PropTypes.arrayOf(PropTypes.shape({})),
  typeOrders: PropTypes.shape({}),
  addOrder: PropTypes.func,
  typeProduct: PropTypes.string,
};

const defaultProp = {
  className: '',
  cardsImages: [],
  cardsPrice: [],
  addOrder: () => {},
  typeOrders: {},
  typeProduct: '',
};

class GiftCardForm extends Component {
  resizeWindow = debounce(() => { this.isMobile(); }, 300);

  constructor(props) {
    super(props);
    this.state = {
      isSelectTouched: false,
      isSelectValid: false,
      imageIndex: 0,
      cardValue: null,
      isScaleMode: false,
    };
  }

  componentDidMount() {
    window.addEventListener('resize', this.resizeWindow);
    this.isMobile();
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.resizeWindow);
  }

  onSubmit = (values) => {
    const { cardsImages, typeProduct } = this.props;
    const { cardValue, imageIndex } = this.state;
    const { displayName, value, productId } = cardValue;
    const { addOrder } = this.props;
    const order = {
      title: displayName,
      typeProduct,
      price: value,
      quantity: 1,
      isGift: false,
      imgId: cardsImages[imageIndex].id,
      productId,
      options: [],
      giftCardOptions: {
        from: values.from,
        to: values.to,
        message: values.subject,
        image: cardsImages[imageIndex].imageUri,
      },
    };
    addOrder(order);
    window.location.href = ROUTES.checkout;
  };

  onChangeSelect = (value) => {
    this.setState({ cardValue: value, isSelectValid: true });
  };

  selectValidatin = () => {
    const { cardValue } = this.state;
    this.setState({ isSelectTouched: true, isSelectValid: !!cardValue });
  };

  changeImage = (id) => () => {
    this.setState({ imageIndex: id });
  };

  isMobile = () => {
    const { isScaleMode } = this.state;

    const isScaleModeNew = window.innerWidth >= 1025 && window.innerWidth <= 1920 ;
    if (isScaleMode !== isScaleModeNew) {
      this.setState({ isScaleMode: isScaleModeNew });
    }
  }

  render() {
    const { className, cardsImages, cardsPrice } = this.props;
    const { imageIndex, isScaleMode } = this.state;
    const { cardValue, isSelectTouched, isSelectValid } = this.state;
    const sum = `$${cardValue && cardValue.value ? cardValue.value.toFixed(2) : '0.00'}`;
    const valuesOptions = cardsPrice.map(({ price, displayName, name, id }) => ({
      value: price,
      label: `$ ${price}`,
      name,
      displayName,
      productId: id,
    }));

    const customStyle = {
      singleValue: (provided) => ({
        ...provided,
        color: `${colors['$dark-slate-blue-color']}`,
        fontWeight: '500',
      }),
      control: (provided) => ({
        ...provided,
        minWidth: '100%',
        backgroundColor: 'transparent !important',
        width: '100%',
        border: 'none !important',
        boxShadow: 'none',
      }),
      placeholder: (provided) => ({
        ...provided,
        ...(isScaleMode ? {
          fontSize: '0.88vw',
          lineHeight: '1.66vw',
        } : {}),
      }),
    };
    return (
      <div className={b({ mix: className })}>
        <Formik
          initialValues={{ to: '', from: '', subject: '' }}
          validationSchema={schema}
          onSubmit={(values, actions) => {
            this.onSubmit(values);
            actions.resetForm();
          }}
          render={({
            errors,
            handleSubmit,
            touched,
            handleChange,
            values: { to, from, subject },
          }) => (
            <form onSubmit={handleSubmit} className={b('form')}>
              <div className={b('row')}>
                <div className={b('cards')}>
                  <div className={b('background-little-square')} />
                  <div className={b('card-image-container')}>
                    <img
                      className={b('card-image-full')}
                      src={cardsImages[imageIndex].imageUri}
                      alt={`Gift cards ${cardsImages[imageIndex].name}`}
                      layout="fill"
                    />
                    <div className={b('card-image-description')}>
                      <span className={b('card-text', { value: true })}>{sum}</span>
                      <span className={b('card-text', { title: true })}>{to}</span>
                      <span className={b('card-text', { message: true })}>
                        {subject || MESSAGE}
                      </span>
                      <span className={b('card-text', { bottom: true })}>{from}</span>
                    </div>
                  </div>
                  <div className={b('images')}>
                    {cardsImages.map(
                      (item, index) =>
                        cardsImages.length > 0 && (
                          <div
                            className={b('img-btn', { checked: imageIndex === index })}
                            role="button"
                            tabIndex="0"
                            onClick={this.changeImage(index)}
                          >
                            <div className={b('mini-image-wrapper')}>
                              <img
                                className={b('mini-image')}
                                src={item.imageUri}
                                alt={`Gift cards ${item.name}`}
                                key={item.id}
                                layout="fill"
                              />
                              {imageIndex === index && (
                                <div className={b('checked-wrapper')}>
                                  <CheckFull
                                    stroke={whiteColor}
                                    stroke2={greenColor}
                                    fill={whiteColor}
                                  />
                                </div>
                              )}
                            </div>
                          </div>
                        ),
                    )}
                  </div>
                </div>
                <div className={b('fields')}>
                  <div className={b('background-square')} />
                  <div className={b('column')}>
                    <div className={b('row')}>
                      <h2 className={b('title', { mix: 'title' })}>{TITLE}</h2>
                    </div>
                    <div className={b('row', { input: true })}>
                      <NewCustomInput
                        className={b('input')}
                        onChange={handleChange}
                        name="to"
                        placeholder={TO_PLACEHOLDER}
                        value={to}
                        isTouched={touched.to}
                        error={errors.to}
                        isValid={!errors.to}
                        maxLength={MAX_FROM_TO_LENGTH}
                      />
                      <NewCustomInput
                        className={b('input')}
                        onChange={handleChange}
                        name="from"
                        placeholder={FROM_PLACEHOLDER}
                        value={from}
                        isTouched={touched.from}
                        error={errors.from}
                        isValid={!errors.from}
                        maxLength={MAX_FROM_TO_LENGTH_FROM}
                      />
                    </div>
                  </div>
                  <div className={b('row')}>
                    <CustomTextarea
                      className={b('area')}
                      name="subject"
                      onChange={handleChange}
                      placeholder={SUBJECT_PLACEHOLDER}
                      value={subject}
                      isTouched={touched.subject}
                      error={errors.subject}
                      isValid={!errors.subject}
                      maxLength={MAX_LENGTH}
                    />
                  </div>
                  <div className={b('row', { select: true })}>
                    <div className={b('select-wrapper')}>
                      <CustomSelect
                        instanceId="reason"
                        className={b('select')}
                        options={valuesOptions}
                        placeholder={VALUE_PLACEHOLDER}
                        value={cardValue}
                        onChange={this.onChangeSelect}
                        isTouched={isSelectTouched}
                        isValid={isSelectValid}
                        error={SHOULD_BE_SELECTED}
                        customStyles={customStyle}
                        isDropDownIndicator={false}
                      />
                    </div>
                    <div className={b('submit-info')}>{SUB_TOTAL_INFO}</div>
                  </div>
                  <div className={b('submit-wrap')}>
                    <div className={b('submit-container')}>
                      <div className={b('summary')}>
                        <span className={b('summary-title-wrapper')}>
                          <div className={b('summary-title-icon')}>
                            <DollarIcon />
                          </div>
                          <div className={b('summary-title')}>{SUB_TOTAL}</div>
                        </span>
                        <span className={b('summary-sum')}>{sum}</span>
                      </div>
                      <RedButton
                        text={SUBMIT_BUTTON_TEXT}
                        type="submit"
                        onClick={this.selectValidatin}
                        backGroundColor={orangeColor}
                        className={b('submit-button')}
                      />
                    </div>
                  </div>
                </div>
              </div>
            </form>
          )}
        />
      </div>
    );
  }
}

GiftCardForm.defaultProps = defaultProp;
GiftCardForm.propTypes = propTypes;

const stateProps = (state) => ({
  cardsImages: giftCardsSelect(state),
  cardsPrice: giftCardsPriceSelect(state),
  currency: currencyPriceSelect(state),
  typeOrders: ordersTypeSelect(state),
});

const actions = {
  addOrder: addToCart,
};

export default connect(stateProps, actions)(GiftCardForm);
