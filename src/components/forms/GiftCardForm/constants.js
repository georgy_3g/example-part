const TO_PLACEHOLDER = 'To';
const FROM_PLACEHOLDER = 'From';
const VALUE_PLACEHOLDER = 'Value $';
const SUBJECT_PLACEHOLDER = 'Your message';
const SUBMIT_BUTTON_TEXT = 'Add to cart';
const SUB_TOTAL_INFO = 'View delivery options in cart';
const SUB_TOTAL = 'Subtotal:';
const TITLE = 'Personalize your gift card';
const TO = 'To';
const MESSAGE = 'Message';
const VALUE_OF = 'Value of ';
const FROM = 'From ';

const VALUE_OPTIONS = [
  { value: 100, label: '100' },
  { value: 200, label: '200' },
  { value: 300, label: '300' },
  { value: 400, label: '400' },
];

const MAX_LENGTH = 95;
const MAX_FROM_TO_LENGTH = 20;
const MAX_FROM_TO_LENGTH_FROM = 20;

export {
  TO_PLACEHOLDER,
  FROM_PLACEHOLDER,
  VALUE_PLACEHOLDER,
  SUBJECT_PLACEHOLDER,
  SUBMIT_BUTTON_TEXT,
  VALUE_OPTIONS,
  SUB_TOTAL,
  SUB_TOTAL_INFO,
  TITLE,
  MAX_LENGTH,
  TO,
  MESSAGE,
  VALUE_OF,
  FROM,
  MAX_FROM_TO_LENGTH,
  MAX_FROM_TO_LENGTH_FROM,
};
