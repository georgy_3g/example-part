const PRODUCT_TITLE = {
  photo_scan: 'Your photo scan details',
  tape_transfer: 'Your tape transfer deliverables',
  film_transfer: 'Your film transfer details',
  audio_transfer: 'Your audio transfer details',
  digital_transfer: 'Your digital transfer details',
};

const QUANTITY_TITLE = {
  photo_scan: 'Scan quantity',
  tape_transfer: 'Tape quantity',
  film_transfer: 'Film quantity',
  audio_transfer: 'Audio quantity',
  digital_transfer: 'Digital quantity',
};

const PER = 'per';
const REEL = 'reel';
const PRICE_TITLE = 'Sub-total:';
const minQt = 0;

const CONTROLS_NAMES = {
  quantity: 'quantity',
  safe: 'safe',
  // --- Hidden for the future ---
  // box: 'box',
  archival: 'archival',
  enhance: 'enhance',
  download: 'download',
  personalize: 'personalize',
};

const ENHANCEMENT_TITLE = 'Add enhancement';
const *******_SAFE = 'USB drive';
const ADD_PERSONALIZATION = 'Add personalization';
const CLOUD_DOWNLOAD = '*******';
const PERSONALIZE = 'Personalized keepsake box';
const TOTALS = 'Totals';
const MAX_LENGTH = 15;
const maxInputNumber = 99999;

const PRICE_POSTFIX = {
  photo_scan: 'per photo',
  tape_transfer: 'per tape',
  film_transfer: 'per film reel',
  audio_transfer: 'per audio media',
  digital_transfer: 'per device',
};

const CLOUD_DOWNLOAD_POSTFIX = 'for 30 day access';
const DVD_DISC_POSTFIX = 'per disc';
const USB_DRIVE_POSTFIX = 'per USB drive';

export {
  PRODUCT_TITLE,
  PER,
  REEL,
  PRICE_TITLE,
  minQt,
  CONTROLS_NAMES,
  QUANTITY_TITLE,
  ENHANCEMENT_TITLE,
  *******_SAFE,
  ADD_PERSONALIZATION,
  CLOUD_DOWNLOAD,
  TOTALS,
  MAX_LENGTH,
  PERSONALIZE,
  PRICE_POSTFIX,
  CLOUD_DOWNLOAD_POSTFIX,
  DVD_DISC_POSTFIX,
  USB_DRIVE_POSTFIX,
  maxInputNumber,
};
