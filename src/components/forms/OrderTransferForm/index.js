import React, { Component } from 'react';
import bem from 'src/utils/bem';
import convertToPrice from 'src/utils/convertToPrice';
import { tinUrl } from 'public';
import DICTIONARY from 'src/constants/dictionary';
import ROUTES from 'src/constants/routes';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import dynamic from 'next/dynamic';
import { currencyPriceSelect, priceSelect } from 'src/redux/prices/selectors';
import ordersTypeSelect from 'src/redux/orders/selectors';
import { addToCart } from 'src/redux/orders/actions';
import { getPriceList } from 'src/redux/prices/actions';
import styles from './index.module.scss';
import {
  CLOUD_DOWNLOAD,
  CLOUD_DOWNLOAD_POSTFIX,
  CONTROLS_NAMES,
  DVD_DISC_POSTFIX,
  ENHANCEMENT_TITLE,
  *******_SAFE,
  MAX_LENGTH,
  maxInputNumber,
  minQt,
  PERSONALIZE,
  PRICE_POSTFIX,
  PRICE_TITLE,
  PRODUCT_TITLE,
  QUANTITY_TITLE,
  TOTALS,
  USB_DRIVE_POSTFIX,
} from './constants';

const CounterInput = dynamic(() => import('src/components/inputs/CounterInput'));
const CloseIcon2 = dynamic(() => import('src/components/svg/CloseIcon2'));
const Picture = dynamic(() => import('src/components/elements/Picture'));
const TogglerInput = dynamic(() => import('src/components/inputs/TogglerInput'));
const TransferText = dynamic(() => import('src/components/inputs/TransferText'));
const ZoomIcon = dynamic(() => import('src/components/svg/ZoomIcon'));

const b = bem('order', styles);
const form = bem('form', styles);
const btn = bem('btn', styles);

const { ORDER_DETAILS, DELIVERABLES, YOUR_PACKAGE, ADD_TO_CART } = DICTIONARY;

const placeholderPersonal = 'Enter title here';

const defaultProps = {
  price: {},
  typeOrders: {},
  currency: '$',
  addOrder: () => {},
  getPrices: () => {},
  transferTitle: '',
  typeProduct: '',
};

const propTypes = {
  price: PropTypes.shape({
    options: PropTypes.arrayOf(PropTypes.shape({})),
    price: PropTypes.number,
    additionalDiscount: PropTypes.number,
    reduce: PropTypes.func,
    displayName: PropTypes.string,
    id: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    name: PropTypes.string,
  }),
  typeOrders: PropTypes.shape({}),
  transferTitle: PropTypes.string,
  currency: PropTypes.string,
  addOrder: PropTypes.func,
  getPrices: PropTypes.func,
  typeProduct: PropTypes.string,
};

class OrderTransferForm extends Component {
  constructor(props) {
    super(props);
    this.form = null;
    this.state = {
      quantity: 0,
      safe: 0,
      // --- Hidden for the future ---
      // box: 1,
      archival: 0,
      enhance: false,
      download: false,
      personalize: false,
      text: '',
      zoom: true,
      isGift: false,
    };
  }

  componentWillUnmount() {
    this.form = null;
  }

  onSubmit = (e) => {
    e.preventDefault();
    const { addOrder, transferTitle, getPrices, typeProduct, price } = this.props;
    const {
      quantity,
      safe,
      // --- Hidden for the future ---
      // box,
      archival,
      enhance,
      download,
      text,
      isGift,
      personalize,
    } = this.state;
    const { options, displayName } = price;
    const values = options.reduce(
      (acc, item) => ({
        ...acc,
        [item.name]: { ...item },
      }),
      {},
    );
    const {
      enhance_my_transfer: enhanceValue,
      archival_dvd_set: archivalValue,
      cloud_download: cloudValue,
      // --- Hidden for the future ---
      // *******_box: *******BoxValue,
      *******_safe: *******SafeValue,
      personalize: personalizeValue,
    } = values;
    const order = {
      productId: price.id,
      quantity,
      text,
      title: transferTitle,
      productName: displayName,
      typeProduct,
      isGift,
      options: [
        {
          optionId: enhanceValue.id,
          quantity: enhance,
          price: enhanceValue.customPrice,
          displayName: enhanceValue.displayName,
          name: enhanceValue.name,
        },
        {
          optionId: archivalValue.id,
          quantity: archival || 0,
          price: archivalValue.customPrice,
          displayName: archivalValue.displayName,
          name: archivalValue.name,
        },
        {
          optionId: cloudValue.id,
          quantity: download,
          price: cloudValue.customPrice,
          displayName: cloudValue.displayName,
          name: cloudValue.name,
        },
        // --- Hidden for the future ---
        // {
        //   optionId: *******BoxValue.id,
        //   quantity: box,
        //   price: *******BoxValue.price,
        //   displayName: *******BoxValue.displayName,
        //   name: *******BoxValue.name,
        // },
        {
          optionId: *******SafeValue.id,
          quantity: safe || 0,
          price: *******SafeValue.customPrice,
          displayName: *******SafeValue.displayName,
          name: *******SafeValue.name,
        },
        {
          optionId: personalizeValue.id,
          quantity: personalize,
          price: personalizeValue.customPrice,
          displayName: personalizeValue.displayName,
          name: personalizeValue.name,
        },
      ],
    };
    addOrder(order);
    window.location.href = ROUTES.checkout;
    const typeTransfer = 'digitize';
    getPrices(typeTransfer);
  };

  validateInputChangeCount = (qt, inc) => {
    if ((qt === minQt && inc < 0) || (qt >= maxInputNumber && inc > 0)) {
      return qt;
    }

    return qt + inc;
  };

  validateInputEnterCount = (value) => {
    if (value >= minQt && value <= maxInputNumber) {
      return value;
    }
    return value > maxInputNumber ? maxInputNumber : minQt;
  };

  validateInputChangeQuantity = (qt, inc) => {
    if (qt >= maxInputNumber && inc > 0) {
      return qt;
    }
    if (qt <= 1 && inc < 1) {
      return minQt + 1;
    }
    return qt + inc;
  };

  validateInputEnterQuantity = (value) => {
    if (value > maxInputNumber) {
      return maxInputNumber;
    }
    if (value >= 0) {
      return value;
    }
    return minQt;
  };

  changeCount = (countName) => (isAdd) => () => {
    const { [countName]: qt } = this.state;
    const inc = isAdd ? 1 : -1;
    const valid = this.validateInputChangeCount(qt, inc);
    this.setState({ [countName]: valid });
  };

  enterCount = (countName) => (value) => {
    const valid = this.validateInputEnterCount(value);
    this.setState({ [countName]: valid });
  };

  changeQuantity = (isAdd) => () => {
    const { quantity: qt } = this.state;
    const inc = isAdd ? 1 : -1;
    const valid = this.validateInputChangeQuantity(qt, inc);
    this.setState({ quantity: valid });
  };

  enterQuantity = (value) => {
    const valid = this.validateInputEnterQuantity(value);
    this.setState({ quantity: valid });
  };

  changeToggler = (togglerName) => (value) => {
    this.setState({ [togglerName]: value });
  };

  changeText = (e) => {
    const { value } = e.target;
    this.setState({ text: value });
  };

  roundPrice = (price) => Math.ceil(price * 100) / 100;

  getPrices = () => {
    const { quantity, safe, archival, enhance, download, personalize } = this.state;
    const { price, currency } = this.props;
    const {
      options,
      name: productName,
      price: productPrice,
      additionalDiscount,
    } = price;
    const productCustomPrice = additionalDiscount ? productPrice - additionalDiscount : productPrice;
    const values = options.reduce(
      (acc, option) => {
        const {
          name: optionName,
          price: optionPrice,
          additionalDiscount: optionAdditionalDiscount,
        } = option;
        const customPrice = optionAdditionalDiscount
          ? optionPrice - optionAdditionalDiscount : optionPrice;
          return {
        ...acc,
        [optionName]: { ...option, customPrice },
      }}, {},
    );
    const {
      enhance_my_transfer: enhanceValue,
      archival_dvd_set: archivalValue,
      cloud_download: cloudValue,
      *******_safe: *******SafeValue,
      personalize: personalizeValue,
    } = values;
    const pricePostfix = PRICE_POSTFIX[productName];
    const qtInfo = ` ${quantity || 0} x ${convertToPrice(productCustomPrice)} = ${convertToPrice(
      (quantity || 0) * productCustomPrice,
    )}`;

    const enhanceSum = enhance ? (quantity || 0) * enhanceValue.customPrice : 0;
    const enhanceInfo = ` ${quantity || 0} x ${convertToPrice(
      enhanceValue.customPrice,
    )} = ${convertToPrice(this.roundPrice(enhanceSum))}`;

    const cloudDownloadSum = download ? cloudValue.customPrice : 0;
    const cloudDownloadInfo = ` ${convertToPrice(
      download ? this.roundPrice(cloudDownloadSum) : 0,
    )}`;

    const *******Info = ` ${safe} x ${convertToPrice(*******SafeValue.customPrice)} = ${convertToPrice(
      safe * *******SafeValue.customPrice,
    )}`;
    const archivalSetInfo = ` ${archival || 0} x ${convertToPrice(
      archivalValue.customPrice,
    )} = ${convertToPrice((archival || 0) * archivalValue.customPrice)}`;

    const personalizeInfo = ` ${personalize ? safe || 0 : 0} x ${convertToPrice(
      personalizeValue.customPrice,
    )}
     = ${convertToPrice((personalize ? safe || 0 : 0) * personalizeValue.customPrice)}`;
    const personalizeSum = personalize ? (safe || 0) * personalizeValue.customPrice : 0;

    const allSum = `$ ${(
      quantity * productCustomPrice +
      enhanceSum +
      (safe || 0) * *******SafeValue.customPrice +
      cloudDownloadSum +
      archival * archivalValue.customPrice +
      personalizeSum
    ).toFixed(2)}`;

    const summaryTitle = PRODUCT_TITLE[productName];
    const quantitySubTitle = QUANTITY_TITLE[productName];

    return {
      qtInfo,
      enhanceInfo,
      pricePostfix,
      allSum,
      currency,
      enhanceValue,
      archivalValue,
      cloudValue,
      *******SafeValue,
      personalizeValue,
      price,
      productCustomPrice,
      values,
      summaryTitle,
      quantitySubTitle,
      *******Info,
      archivalTitle: archivalValue.displayName,
      archivalSetInfo,
      cloudDownloadInfo,
      personalizeInfo,
    };
  };

  zoomHandler = () => {
    const { zoom } = this.state;
    if (zoom) {
      window.scrollTo({
        left: 0,
        top: this.form.offsetTop - 260,
        behavior: 'smooth',
      });
    }
    this.setState({ zoom: !zoom });
  };

  render() {
    const {
      text,
      quantity,
      safe,
      // --- Hidden for the future ---
      // box,
      archival,
      enhance,
      download,
      zoom,
      personalize,
    } = this.state;

    const {
      qtInfo,
      enhanceInfo,
      pricePostfix,
      *******Info,
      allSum,
      enhanceValue,
      archivalValue,
      cloudValue,
      // --- Hidden for the future ---
      // *******BoxValue,
      *******SafeValue,
      personalizeValue,
      price,
      productCustomPrice,
      summaryTitle,
      quantitySubTitle,
      archivalTitle,
      archivalSetInfo,
      cloudDownloadInfo,
      personalizeInfo,
    } = this.getPrices();
    const { hint, hintImages } = price;
    return (
      <form
        className={b({ mix: form() })}
        ref={(e) => {
          this.form = e;
        }}
        onSubmit={this.onSubmit}
      >
        {zoom ? (
          <div className={b('controls-container')}>
            <fieldset className={b('fieldset', { mix: form('fieldset') })}>
              <legend className={b('legend', { mix: form('legend') })}>{ORDER_DETAILS}</legend>
              <CounterInput
                title="Quantity"
                price={`${convertToPrice(productCustomPrice)} ${pricePostfix}`}
                count={quantity}
                changeCount={this.changeQuantity}
                bemCount={b}
                mainCounter
                isHint
                hintText={hint}
                hintImages={hintImages}
                withInput
                withReplacement
                onChange={this.enterQuantity}
                min={0}
              />
              <TogglerInput
                title={enhanceValue.displayName}
                price={`${convertToPrice(enhanceValue.customPrice)} ${pricePostfix}`}
                isHint
                hintText={enhanceValue.hint}
                hintImages={enhanceValue.hintImages}
                bemToggler={b}
                checked={enhance}
                togglerChange={this.changeToggler(CONTROLS_NAMES.enhance)}
              />
            </fieldset>
            <fieldset className={b('fieldset', { mix: form('fieldset') })}>
              <legend className={b('legend', { mix: form('legend') })}>{DELIVERABLES}</legend>
              <CounterInput
                title={*******SafeValue.displayName}
                price={`${convertToPrice(*******SafeValue.customPrice)} ${USB_DRIVE_POSTFIX}`}
                count={safe}
                changeCount={this.changeCount(CONTROLS_NAMES.safe)}
                isHint
                hintText={*******SafeValue.hint}
                hintImages={*******SafeValue.hintImages}
                bemCount={b}
                mainCounter
                withInput
                withReplacement
                onChange={this.enterCount(CONTROLS_NAMES.safe)}
                min={0}
              />
              <TogglerInput
                title={cloudValue.displayName}
                price={`${convertToPrice(cloudValue.customPrice)} ${CLOUD_DOWNLOAD_POSTFIX}`}
                isHint
                hintText={cloudValue.hint}
                hintImages={cloudValue.hintImages}
                bemToggler={b}
                checked={download}
                togglerChange={this.changeToggler(CONTROLS_NAMES.download)}
              />
              <CounterInput
                title={archivalValue.displayName}
                price={`${convertToPrice(archivalValue.customPrice)} ${DVD_DISC_POSTFIX}`}
                count={archival}
                changeCount={this.changeCount(CONTROLS_NAMES.archival)}
                isHint
                hintText={archivalValue.hint}
                hintImages={archivalValue.hintImages}
                bemCount={b}
                mainCounter
                withInput
                withReplacement
                onChange={this.enterCount(CONTROLS_NAMES.archival)}
                min={0}
              />
              {/* --- Hidden for the future --- */}
              {/* <CounterInput
							 title={*******BoxValue.displayName}
							 price={`${currency}${*******BoxValue.price}`}
							 count={box}
							 changeCount={this.changeCount(CONTROLS_NAMES.box)}
							 isHint
							 hintText={*******BoxValue.hint}
							 hintImages={*******BoxValue.hintImages}
							 bemCount={b}
							 /> */}
            </fieldset>
            {Boolean(safe) && (
              <fieldset className={b('fieldset', { mix: form('fieldset') })}>
                <legend className={b('legend', { mix: form('legend') })}>{YOUR_PACKAGE}</legend>
                <TransferText
                  title={personalizeValue.displayName}
                  placeholder={placeholderPersonal}
                  handleChange={this.changeText}
                  value={text}
                  isHint
                  hintText={personalizeValue.hint}
                  hintImages={personalizeValue.hintImages}
                  maxLength={MAX_LENGTH}
                  isPersonalize
                  checked={personalize}
                  price={`${convertToPrice(personalizeValue.customPrice)} ${USB_DRIVE_POSTFIX}`}
                  personalize={CONTROLS_NAMES.personalize}
                  togglerChange={this.changeToggler}
                />
                {personalize && (
                  <div className={b('field', { img: true })}>
                    <Picture bem={b} imgUrl={tinUrl} imgAlt="img" imgText={text} />
                    <div
                      className={b('box-icon')}
                      role="button"
                      tabIndex="0"
                      onClick={this.zoomHandler}
                    >
                      <ZoomIcon className={b('box-zoom-icon')} />
                    </div>
                  </div>
                )}
              </fieldset>
            )}
          </div>
        ) : (
          <div className={b('popup')}>
            <Picture bem={b} imgUrl={tinUrl} imgAlt="" imgText={text} />
            <div className={b('popup-close-bar')}>
              <div
                className={b('close-icon')}
                role="button"
                tabIndex="0"
                onClick={this.zoomHandler}
              >
                <CloseIcon2 />
              </div>
            </div>
          </div>
        )}
        <div className={b('submit-container')}>
          <div className={b('summary-container')}>
            <h4 className={b('summary-title')}>{summaryTitle}</h4>
            <p className={b('summary-text')}>
              <span className={b('summary-text-title')}>
                {quantitySubTitle}
                :
              </span>
              {qtInfo}
            </p>
            {Boolean(enhance) && (
              <p className={b('summary-text')}>
                <span className={b('summary-text-title')}>
                  {ENHANCEMENT_TITLE}
                  :
                </span>
                {enhanceInfo}
              </p>
            )}
          </div>
          <div className={b('summary-container')}>
            <h4 className={b('summary-title')}>{summaryTitle}</h4>
            {Boolean(safe) && (
              <p className={b('summary-text')}>
                <span className={b('summary-text-title')}>
                  {*******_SAFE}
                  :
                </span>
                {*******Info}
              </p>
            )}
            {Boolean(safe && personalize) && (
              <p className={b('summary-text')}>
                <span className={b('summary-text-title')}>
                  {PERSONALIZE}
                  :
                </span>
                {personalizeInfo}
              </p>
            )}
            {Boolean(download) && (
              <p className={b('summary-text')}>
                <span className={b('summary-text-title')}>
                  {CLOUD_DOWNLOAD}
                  :
                </span>
                {cloudDownloadInfo}
              </p>
            )}
            {Boolean(archival) && (
              <p className={b('summary-text')}>
                <span className={b('summary-text-title')}>
                  {archivalTitle}
                  :
                </span>
                {archivalSetInfo}
              </p>
            )}
          </div>
          <div className={b('submit')}>
            <div className={b('price-container')}>
              <h4 className={b('summary-title')}>{TOTALS}</h4>
              <h5 className={b('price-title')}>{PRICE_TITLE}</h5>
              <p className={b('price-value')}>{allSum}</p>
            </div>
            <button
              className={b('btn', { mix: btn({ light: true, big: true }) })}
              type="submit"
              disabled={!(quantity >= 1 && (safe > 0 || archival > 0 || download))}
            >
              {ADD_TO_CART}
            </button>
          </div>
        </div>
      </form>
    );
  }
}

OrderTransferForm.propTypes = propTypes;
OrderTransferForm.defaultProps = defaultProps;

const stateProps = (state) => ({
  price: priceSelect(state),
  currency: currencyPriceSelect(state),
  typeOrders: ordersTypeSelect(state),
});

const actions = {
  addOrder: addToCart,
  getPrices: getPriceList,
};

export default connect(stateProps, actions)(OrderTransferForm);
