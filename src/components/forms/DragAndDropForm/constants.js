const DRAG_AND_DROP_TERMS = {
  DROP_HERE: 'Drop the photos here ...',
  SELECT_IMAGE: 'Select image',
  UPLOAD_PHOTOS: 'Upload your photos',
  SELECT_IMAGES: 'Select photos',
  DRAG_TO_UPLOAD: 'or drag your photos here',
};

export default DRAG_AND_DROP_TERMS;
