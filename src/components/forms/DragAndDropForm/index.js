import React, { useCallback } from 'react';
import useMobileStyle from 'src/utils/useMobileStyle';
import { useDropzone } from 'react-dropzone';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { IMAGE_TYPE_JPEG } from 'src/constants';
import dynamic from 'next/dynamic';
import { loadImages, loadImagesOrderEnhance } from 'src/redux/orderImages/actions';
import { loadImagesContact } from 'src/redux/enhance/actions';
import { chatTokenSelector, getToken, isMobileDeviceSelector } from 'src/redux/auth/selectors';
import {setRoomFiles} from "src/redux/folder/actions";
import DRAG_AND_DROP_TERMS from './constants';

const BlueButton = dynamic(() => import('src/components/buttons/BlueButton'));

const defaultProps = {
  uploadImages: () => {},
  uploadImagesEnhance: () => {},
  token: '',
  bemBlock: () => {},
  isSelectFiles: false,
  openPopup: () => {},
  isOrderEnhance: false,
  notPopup: false,
  close: () => {},
  isShowError: false,
  buttonBackgroundColor: '',
  withoutBottomText: false,
  buttonClass: '',
  children: null,
  uploadUrl: () => {},
  url: false,
  chatToken: '',
  multiple: true,
  isMobileDevice: true,
  showText: true,
  buttonRefFunc: () => {},
  forPhotoArtConstructor: false,
  buttonText: '',
  className: '',
  isContactImages: false,
  uploadImagesContact: () => {},
};

const { DROP_HERE, DRAG_TO_UPLOAD, UPLOAD_PHOTOS, SELECT_IMAGES } = DRAG_AND_DROP_TERMS;

const propTypes = {
  uploadImages: PropTypes.func,
  uploadImagesEnhance: PropTypes.func,
  token: PropTypes.string,
  bemBlock: PropTypes.func,
  isSelectFiles: PropTypes.bool,
  openPopup: PropTypes.func,
  isOrderEnhance: PropTypes.bool,
  notPopup: PropTypes.bool,
  close: PropTypes.func,
  isShowError: PropTypes.bool,
  buttonBackgroundColor: PropTypes.string,
  withoutBottomText: PropTypes.bool,
  buttonClass: PropTypes.string,
  children: PropTypes.node,
  uploadUrl: PropTypes.func,
  url: PropTypes.bool,
  chatToken: PropTypes.string,
  multiple: PropTypes.bool,
  isMobileDevice: PropTypes.bool,
  showText: PropTypes.bool,
  buttonRefFunc: PropTypes.func,
  forPhotoArtConstructor: PropTypes.bool,
  buttonText: PropTypes.string,
  className: PropTypes.string,
  isContactImages: PropTypes.bool,
  uploadImagesContact: PropTypes.func,
};

function DragAndDropForm(props) {
  const {
    uploadImages,
    uploadImagesEnhance,
    token,
    bemBlock,
    isSelectFiles,
    openPopup,
    isOrderEnhance,
    notPopup,
    close,
    isShowError,
    withoutBottomText,
    buttonClass,
    children,
    multiple,
    url,
    isMobileDevice,
    showText,
    buttonRefFunc,
    forPhotoArtConstructor,
    buttonText,
    className,
    isContactImages,
    uploadImagesContact,
    chatToken,
  } = props;

  const isMobile = useMobileStyle(1024, isMobileDevice);

  const upload = (data) => {
    const { uploadUrl } = props;
    if (isOrderEnhance) {
      uploadImagesEnhance(data);
    } else if (url) {
      uploadUrl(data);
    } else {
      uploadImages(data);
    }
    close();
  };

  const dataURLtoFile = (dataURL) => {
    const blobBin = atob(dataURL.split(',')[1]);
    const array = [];
    for (let i = 0; i < blobBin.length; i += 1) {
      array.push(blobBin.charCodeAt(i));
    }
    const data = new Blob([new Uint8Array(array)], { type: IMAGE_TYPE_JPEG });
    return data;
  };

  const convertPdf = (file) => {
    const pages = [];
    const heights = [];
    let width = 0;
    let currentPage = 1;
    const scale = 1.5;

    const uploadPdfImages = () => {
      const formData = new FormData();
      pages.forEach((page, i) => {
        const canvas = document.createElement('canvas');
        const ctx = canvas.getContext('2d');
        canvas.width = width;
        canvas.height = heights[i];
        ctx.putImageData(page, 0, 0);

        if(isContactImages) {
          const base64 = canvas.toDataURL(IMAGE_TYPE_JPEG)
          uploadImagesContact([{ base64 }]);
        }

        if (url && !isContactImages) {
          formData.append(
            'file',
            dataURLtoFile(canvas.toDataURL(IMAGE_TYPE_JPEG)),
            `${currentPage}_${file.name}.jpg`,
          );
        } else if(!isContactImages){
          formData.append(
            'files[]',
            dataURLtoFile(canvas.toDataURL(IMAGE_TYPE_JPEG)),
            `${currentPage}_${file.name}.jpg`,
          );
        }
      });
      if (url && !isContactImages) {
        upload({ formData, chatToken });
      } else if(!isContactImages){
        formData.append('isCropped', false);
        upload({ formData, token, countOfImages: pages.length });

      }
    };

    const fileReader = new FileReader();
    fileReader.onload = () => {
      const pdfjsLib = window['pdfjs-dist/build/pdf'];
      pdfjsLib.GlobalWorkerOptions.workerSrc =
        'https://mozilla.github.io/pdf.js/build/pdf.worker.js';
      pdfjsLib.getDocument(fileReader.result).promise.then((pdf) => {
        const getPage = () => {
          pdf.getPage(currentPage).then((page) => {
            const viewport = page.getViewport({ scale });
            const canvas = document.createElement('canvas');
            const ctx = canvas.getContext('2d');
            const renderContext = { canvasContext: ctx, viewport };

            canvas.height = viewport.height;
            canvas.width = viewport.width;

            page.render(renderContext).promise.then(() => {
              pages.push(ctx.getImageData(0, 0, canvas.width, canvas.height));

              heights.push(page.view[3] * scale);
              if (width < canvas.width) {
                const { width: canvasWidth } = canvas;
                width = canvasWidth;
              }

              if (currentPage < pdf.numPages) {
                currentPage += 1;
                getPage();
              } else {
                uploadPdfImages();
              }
            });
          });
        };

        getPage();
      });
    };

    fileReader.readAsArrayBuffer(file);
  };

  const toBase64 = file => new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = error => reject(error);
});

  const getBase64FileList = async (fileList) => {
    const newFileList = await Promise.all(fileList.map((file) => {
      if (!file.url) {
        return toBase64(file);
      }
      return file.url;
    }));
    return newFileList;
  }


  const onDrop = useCallback(async (files) => {
    const formData = new FormData();
    let imagesToupload = 0;

    const filesNoPdf = files.filter(file => file.type !== 'application/pdf');
    const filesBaseList = await getBase64FileList(filesNoPdf);

    files.forEach((file) => {
      if (file.type === 'application/pdf') {
        convertPdf(file);
      } else {
        if (url) {
          formData.append('file', file, file.name);
        } else {
          formData.append('files[]', file, file.name);
        }
        imagesToupload += 1;
      }
    });
    if (url) {
      upload({ formData, chatToken });
    } else if(isContactImages){
      const imagesContact = filesBaseList.map((file) => ({ base64: file }));
      uploadImagesContact(imagesContact)
    } else {
      formData.append('isCropped', false);
      upload({ formData, token, countOfImages: imagesToupload });
    }
  }, []);

  const { getRootProps, getInputProps, isDragActive } = useDropzone({ onDrop });
  const inputProps = getInputProps();
  const text = notPopup ? SELECT_IMAGES : UPLOAD_PHOTOS;
  const btnText = buttonText || text;

  return (
    <div
      className={bemBlock('drag-and-drop', {
        wrapper: true,
        'with-error': isShowError,
        constructor: forPhotoArtConstructor,
        enhance: isOrderEnhance,
        mix: className,
      })}
      {...getRootProps()}
    >
      {isSelectFiles && (
        <input
          {...inputProps}
          multiple={multiple || isMobile}
          accept={`.jpeg,.jpg,.png,.bmp${isMobile ? '' : ',.pdf'}`}
          id="upload-input"
        />
      )}
      {isDragActive ? (
        <p className={bemBlock('drop-text')}>{DROP_HERE}</p>
      ) : (
        <div className={bemBlock('select-block')} id="upload-button" ref={buttonRefFunc}>
          <BlueButton
            className={bemBlock('select-btn', { mix: buttonClass })}
            text={showText ? btnText : ''}
            onClick={openPopup()}
          >
            {children}
            {showText ? btnText : ''}
          </BlueButton>
          {!withoutBottomText && (
            <p className={bemBlock('select-text')}>{notPopup ? null : `${DRAG_TO_UPLOAD}`}</p>
          )}
        </div>
      )}
    </div>
  );
}

DragAndDropForm.propTypes = propTypes;
DragAndDropForm.defaultProps = defaultProps;

const actions = {
  uploadImages: loadImages,
  uploadUrl: setRoomFiles,
  uploadImagesEnhance: loadImagesOrderEnhance,
  uploadImagesContact: loadImagesContact,
};

const stateProps = (state) => ({
  token: getToken(state),
  chatToken: chatTokenSelector(state),
  isMobileDevice: isMobileDeviceSelector(state),
});

export default connect(stateProps, actions)(DragAndDropForm);
