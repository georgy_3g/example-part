import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import DICTIONARY from 'src/constants/dictionary';
import INPUTS_NAMES from 'src/constants/inputsNames';
import ERRORS from 'src/constants/errors';
import { Formik } from 'formik';
import bem from 'src/utils/bem';
import * as Yup from 'yup';
import dynamic from 'next/dynamic';
import { errorCodeSelector, isSignUpFail, isSignUpSuccess } from 'src/redux/shared/selectors';
import { signupAction } from 'src/redux/shared/actions';
import styles from './index.module.scss';

const SignUpIcon = dynamic(() => import('src/components/svg/SignUpIcon'));
const NewCustomInput = dynamic(() => import('src/components/inputs/NewCustomInput'));

const b = bem('signup', styles);
const form = bem('form', styles);

const {
  SIGNUP,
  EMAIL,
  PASSWORD,
  FIRST_NAME,
  LAST_NAME,
  SIGNUP_SUCCESS,
  SIGNUP_EMAIL_FAIL,
  CONFIRM_PASSWORD,
} = DICTIONARY;

const { SHOULD_BE_FILLED, ENTER_NAME, INVALID_EMAIL, SHORT_PASSWORD, CONFIRM_PASSWORD_ERROR } =
  ERRORS;

const SIGN_UP_DELAY = 3000;

const defaultProps = {
  signUp: () => {},
  close: () => {},
  isSignUp: false,
  SignUpFail: false,
  className: '',
  redirectLink: null,
  errorCode: null,
};

const propTypes = {
  signUp: PropTypes.func,
  close: PropTypes.func,
  isSignUp: PropTypes.bool,
  SignUpFail: PropTypes.bool,
  className: PropTypes.string,
  redirectLink: PropTypes.string,
  errorCode: PropTypes.string,
};

const schema = Yup.object().shape({
  email: Yup.string().email(INVALID_EMAIL).trim().required(SHOULD_BE_FILLED),
  password: Yup.string().min(6, SHORT_PASSWORD).required(SHOULD_BE_FILLED),
  confirmPassword: Yup.string()
    .oneOf([Yup.ref('password'), null], CONFIRM_PASSWORD_ERROR)
    .required(SHOULD_BE_FILLED),
  firstName: Yup.string().required(ENTER_NAME),
  lastName: Yup.string().required(ENTER_NAME),
});

class SignUpForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      signUpSuccess: false,
    };
  }

  // eslint-disable-next-line react/no-deprecated
  componentWillReceiveProps(nextProps) {
    const { close, isSignUp } = this.props;
    if (nextProps.isSignUp !== isSignUp) {
      setTimeout(() => {
        this.setState({ signUpSuccess: false });
        close();
      }, SIGN_UP_DELAY);
    }
  }

  componentDidUpdate(prevProps) {
    const { SignUpFail, errorCode } = this.props;
    if (!prevProps.SignUpFail && SignUpFail && errorCode === 'ERR_EMAIL_TAKEN') {
      this.setError('email', SIGNUP_EMAIL_FAIL);
    }
  }

  onSubmit = (values, { setFieldError }) => {
    const { signUp, redirectLink } = this.props;
    this.setError = setFieldError;
    signUp({ ...values, redirectLink });
    this.setState({ signUpSuccess: true });
  };

  render() {
    const { signUpSuccess } = this.state;
    const { isSignUp, className } = this.props;
    return (
      <div className={b({ mix: className })}>
        <Formik
          initialValues={{
            email: '',
            password: '',
            confirmPassword: '',
            firstName: '',
            lastName: '',
          }}
          validationSchema={schema}
          onSubmit={(values, actions) => {
            const trimmedValues = {
              email: values.email.trim().toLowerCase(),
              password: values.password,
              confirmPassword: values.confirmPassword,
              firstName: values.firstName.trim(),
              lastName: values.lastName.trim(),
            };
            this.onSubmit(trimmedValues, actions);
          }}
          render={({ errors, handleSubmit, touched, handleChange, values }) => {
            const signUpInactive = Object.keys(errors).length && Object.keys(touched).length;
            return (
              <form onSubmit={handleSubmit} className={b('form', { mix: form() })}>
                <fieldset className={b('fieldset', { mix: form('fieldset') })}>
                  <div className={b('sign-up-logo')}>
                    <SignUpIcon className="signup__sign-up-svg" />
                    <legend className={b('legend', { mix: form('legend') })}>{SIGNUP}</legend>
                  </div>
                  <div className={b('names')}>
                    <NewCustomInput
                      className={b('input-name')}
                      onChange={handleChange}
                      name={INPUTS_NAMES.firstName}
                      placeholder={FIRST_NAME}
                      value={values.firstName}
                      isTouched={touched.firstName}
                      error={errors.firstName}
                      isValid={!errors.firstName}
                    />
                    <NewCustomInput
                      className={b('input-name')}
                      onChange={handleChange}
                      name={INPUTS_NAMES.lastName}
                      placeholder={LAST_NAME}
                      value={values.lastName}
                      isTouched={touched.lastName}
                      error={errors.lastName}
                      isValid={!errors.lastName}
                    />
                  </div>
                  <NewCustomInput
                    className={b('input')}
                    onChange={handleChange}
                    name={INPUTS_NAMES.email}
                    placeholder={EMAIL}
                    value={values.email}
                    isTouched={touched.email}
                    error={errors.email}
                    isValid={!errors.email}
                  />
                  <NewCustomInput
                    className={b('input')}
                    onChange={handleChange}
                    name={INPUTS_NAMES.password}
                    placeholder={PASSWORD}
                    type={PASSWORD}
                    value={values.password}
                    isTouched={touched.password}
                    error={errors.password}
                    isValid={!errors.password}
                  />
                  <NewCustomInput
                    className={b('input')}
                    onChange={handleChange}
                    name={INPUTS_NAMES.confirmPassword}
                    placeholder={CONFIRM_PASSWORD}
                    type={PASSWORD}
                    value={values.confirmPassword}
                    isTouched={touched.confirmPassword}
                    error={errors.confirmPassword}
                    isValid={!errors.confirmPassword}
                  />
                </fieldset>
                {signUpSuccess && isSignUp ? (
                  <p className={b('submit-msg')}>{SIGNUP_SUCCESS}</p>
                ) : null}
                <button className={b('btn')} type="submit" disabled={signUpInactive}>
                  {SIGNUP}
                </button>
              </form>
            );
          }}
        />
      </div>
    );
  }
}

SignUpForm.propTypes = propTypes;
SignUpForm.defaultProps = defaultProps;

const stateProps = (state) => ({
  isSignUp: isSignUpSuccess(state),
  SignUpFail: isSignUpFail(state),
  errorCode: errorCodeSelector(state),
});

const actions = {
  signUp: signupAction,
};

export default connect(stateProps, actions)(SignUpForm);
