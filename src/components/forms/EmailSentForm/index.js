import React from 'react';
import PropTypes from 'prop-types';
import DICTIONARY from 'src/constants/dictionary';
import bem from 'src/utils/bem';
import dynamic from "next/dynamic";
import styles from './index.module.scss';

const LetterIcon = dynamic(() => import('src/components/svg/LetterIcon'));

const b = bem('email-sent', styles);

const { RESET_PASS_SENT, EMAIL_SENT_TITLE, BACK_TO_LOGIN } = DICTIONARY;

const defaultProps = {
  openLoginPopup: () => {},
  close: () => {},
};

const propTypes = {
  openLoginPopup: PropTypes.func,
  close: PropTypes.func,
};

function EmailSentForm(props) {
  const backToLogin = () => {
    const { openLoginPopup, close } = props;
    openLoginPopup();
    close();
  };

  return (
    <div className={b()}>
      <div className={b('form')}>
        <fieldset className={b('fieldset')}>
          <div className={b('email-sent-logo')}>
            <LetterIcon />
            <legend className={b('legend')}>{EMAIL_SENT_TITLE}</legend>
          </div>
          <p className={b('title')}>{RESET_PASS_SENT}</p>
        </fieldset>
        <button className={b('btn', { mix: 'btn-active' })} type="button" onClick={backToLogin}>
          {BACK_TO_LOGIN}
        </button>
      </div>
    </div>
  );
}

EmailSentForm.propTypes = propTypes;
EmailSentForm.defaultProps = defaultProps;

export default EmailSentForm;
