const SELECT_IMAGES_FORM_TERMS = {
  TITLE: 'Select photos to upload from your computer, tablet or phone',
  ADD_PHOTOS: 'Add photos',
  LIST: [
    {
      id: 1,
      text: 'Scan at 300dpi or larger.',
    },
    {
      id: 2,
      text: 'Do not take a photo of the photo.',
    },
    {
      id: 3,
      text: 'Save your image as JPEG, PNG, TIFF, or BMP.',
    },
    {
      id: 4,
      text: 'Scan just the image, not the enitre scanner space.',
    },
  ],
  UPLOAD: 'Upload',
  MY_PHOTOS: 'My Photos',
  SCANNING: 'Scanning Help',
  SCANNING_TITLE: 'Tips to ensure the best scan.',
  SCANNING_DESCRIPTION:
    'If you choose to upload your photo to ******* its important to get the best scan possible. ' +
    'Please follow our tips below or find a local professional to scan your photos for you.',
  SCANNING_INFO_LIST: [
    {
      id: 'scan1',
      text: 'Clean the scanner glass',
    },
    {
      id: 'scan2',
      text: 'Scan to jpeg, tiff or png format',
    },
    {
      id: 'scan3',
      text: 'Scan at 300dpi or higher',
    },
    {
      id: 'scan4',
      text: 'Scan just the image if possible',
    },
    {
      id: 'scan5',
      text: 'Do not add optional settings like color correction',
    },
  ],
  EASY_SHIP_KIK: 'Easy Scan Ship Kit',
  SHIP_KIT_TITLE: 'Let ******* scan your photos professionally',
  SHIP_KIT_TEXT:
    'Includes secure 3-way shipping to your door and back and back for your photo prints. Once your photos are scanned you will have the opportunity to use them to design your own photo art.',
  SHIP_KIT_NAME: 'Easy Ship Kit',
  SHIP_KIT_PRICE: 15,
  SHIP_KIT_PRICE_TEXT: 'for 3-way scan ship kit',
  TITLE_SHIP_KIT: 'Scan your photos. Create photo art.',
  TEXT_ONE:
    "We'll send you our easy 3-way scan ship kit right to your door. Fill it with up to 25 loose photos (8x12 or smaller), 35mm slides or negatives & ship it back to us with the included shipping label.",
  TEXT_TWO:
    'We will digitize & enhance your photos to your personal ******* & provide a 30 day trial so you can create extraordinary photo art.',
  TEXT_THREE: 'Just pay $10 for a 3-way ship kit.',
  TEXT_BUTTON: 'Add to cart',
};

export default SELECT_IMAGES_FORM_TERMS;
