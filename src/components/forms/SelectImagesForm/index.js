import React, { Component } from 'react';
import bem from 'src/utils/bem';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import colors from 'src/styles/colors.json';
import ROUTES from 'src/constants/routes';
import spinner from 'public/img/shared/spinner*******.gif';
import dynamic from 'next/dynamic';
import { orderImagesSelector } from 'src/redux/orderImages/selectors';
import { isImageUploadSelector, scanShipKipProductsSelector } from 'src/redux/shared/selectors';
import { photoScanSelector, trialShipKitSelector } from 'src/redux/prices/selectors';
import { getToken, userIdSelector } from 'src/redux/auth/selectors';
import { couponError, couponNameSelect } from 'src/redux/coupon/selectors';
import ordersTypeSelect from 'src/redux/orders/selectors';
import { addToCart } from 'src/redux/orders/actions';
import { checkCoupon } from 'src/redux/coupon/actions';
import styles from './index.module.scss';
import SELECT_IMAGES_FORM_TERMS from './constants';
import DragAndDropForm from '../DragAndDropForm';

const Tabs = dynamic(() => import('src/components/elements/Tabs'));
const FreePhotoScanContent = dynamic(() => import('src/components/widgets/FreePhotoScanContent'));
const CloseIcon2 = dynamic(() => import('src/components/svg/CloseIcon2'));
const CheckFull = dynamic(() => import('src/components/svg/CheckFull'));

const b = bem('select-images-form', styles);
const blueLightColor = colors['$iceberg-blue-light-color'];

const defaultProps = {
  isOrderEnhance: false,
  className: '',
  close: () => {},
  isImageUpload: false,
  top: null,
  price: {},
  orders: {},
  trialShipKit: {},
  addOrder: () => {},
  error: '',
};

const propTypes = {
  isOrderEnhance: PropTypes.bool,
  className: PropTypes.string,
  close: PropTypes.func,
  isImageUpload: PropTypes.bool,
  top: PropTypes.number,
  addOrder: PropTypes.func,
  orders: PropTypes.shape({
    photo_scan: PropTypes.arrayOf(),
  }),
  price: PropTypes.shape({
    options: PropTypes.arrayOf(PropTypes.shape({})),
    price: PropTypes.number,
    reduce: PropTypes.func,
    displayName: PropTypes.string,
    id: PropTypes.number,
    name: PropTypes.string,
  }),
  trialShipKit: PropTypes.shape({
    displayName: PropTypes.string,
  }),
  error: PropTypes.string,
};

const {
  UPLOAD,
  SCANNING,
  SCANNING_TITLE,
  SCANNING_DESCRIPTION,
  SCANNING_INFO_LIST,
  TITLE_SHIP_KIT,
  TEXT_ONE,
  TEXT_BUTTON,
  TEXT_THREE,
  TEXT_TWO,
  EASY_SHIP_KIK,
} = SELECT_IMAGES_FORM_TERMS;
class SelectImagesForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeScene: this.ACCOUNT_TABS({})[0].id,
      isClick: false,
    };
  }

  componentDidMount() {
    const element = document.getElementById('select-images-form');
    const promoBar = document.getElementById('promo-banner');
    const { offsetHeight = 0 } = promoBar || { offsetHeight: 0 };
    const { top } = element.getBoundingClientRect() || {};
    window.scrollTo({
      top: document.documentElement.scrollTop + top - 140 - offsetHeight,
      behavior: 'smooth',
    });
  }

  componentDidUpdate(prevProps) {
    const { isClick } = this.state;
    const { error: oldError } = prevProps;
    const { orders, error } = this.props;

    if (isClick) {
      const { photo_scan: photoScanOrders } = orders;
      const isIncludeTrial = photoScanOrders.find((order) => order.isTrial);
      if (!isIncludeTrial) this.addProducts();
    }

    if (isClick && !oldError && error) {
      this.clearStatus();
    }
  }

  ACCOUNT_TABS = ({ isOrderEnhance, close, isImageUpload, onClick }) => {
    const { trialShipKit } = this.props;
    const { displayName } = trialShipKit;
    return [
      {
        text: UPLOAD,
        id: UPLOAD,
        component: () => (
          <section className={b('upload')}>
            {isImageUpload && (
              <div className={b('spinner-block')}>
                <img
                  className={b('spinner', { invisible: !isImageUpload })}
                  src={spinner.src}
                  alt="spinner"
                />
              </div>
            )}
            <DragAndDropForm
              bemBlock={b}
              isSelectFiles
              isOrderEnhance={isOrderEnhance}
              close={close}
            />
          </section>
        ),
      },
      {
        text: displayName,
        id: displayName,
        component: () => (
          <div className={b('scan-block')}>
            <FreePhotoScanContent
              onClick={onClick}
              title={TITLE_SHIP_KIT}
              textOne={TEXT_ONE}
              textTwo={TEXT_TWO}
              textThree={TEXT_THREE}
              textButton={TEXT_BUTTON}
              b={b}
            />
          </div>
        ),
      },
      {
        text: SCANNING,
        id: SCANNING,
        component: () => (
          <div className={b('scan-block')}>
            <div className={b('col')}>
              <h2 className={b('scan-title')}>{SCANNING_TITLE}</h2>
              <span className={b('scan-text', { scan: true })}>{SCANNING_DESCRIPTION}</span>
              <ul className={b('scan-list')}>
                {SCANNING_INFO_LIST.map(({ id, text }) => (
                  <li className={b('scan-list-item')} key={id}>
                    <CheckFull
                      className={b('scan-list-item-icon')}
                      stroke={blueLightColor}
                      fill="none"
                    />
                    <span className={b('scan-list-item-text')}>{text}</span>
                  </li>
                ))}
              </ul>
            </div>
          </div>
        ),
      },
    ];
  };

  addProducts = () => {
    const { addOrder, price, close } = this.props;
    const { options, displayName } = price;
    const values = options.reduce(
      (acc, item) => ({
        ...acc,
        [item.name]: { ...item },
      }),
      {},
    );
    const {
      enhance_my_transfer: enhanceValue,
      archival_dvd_set: archivalValue,
      cloud_download: cloudValue,
      *******_safe: *******SafeValue,
      personalize: personalizeValue,
    } = values;
    const order = {
      productId: price.id,
      quantity: 25,
      text: '',
      textSecond: '',
      title: '',
      productName: displayName,
      typeProduct: 'photo_scan',
      isGift: false,
      isTrial: true,
      minValue: 25,
      options: [
        {
          optionId: enhanceValue.id,
          quantity: 0,
          price: enhanceValue.price,
          displayName: enhanceValue.displayName,
          name: enhanceValue.name,
          hide: true,
        },
        {
          optionId: archivalValue.id,
          quantity: 0,
          price: archivalValue.price,
          displayName: archivalValue.displayName,
          name: archivalValue.name,
        },
        {
          optionId: cloudValue.id,
          quantity: true,
          price: cloudValue.price,
          displayName: cloudValue.displayName,
          name: cloudValue.name,
          isNotEdit: true,
        },
        {
          optionId: *******SafeValue.id,
          quantity: 0,
          price: *******SafeValue.price,
          displayName: *******SafeValue.displayName,
          name: *******SafeValue.name,
        },
        {
          optionId: personalizeValue.id,
          quantity: false,
          price: personalizeValue.price,
          displayName: personalizeValue.displayName,
          name: personalizeValue.name,
        },
      ],
    };
    addOrder(order);
    window.location.href = ROUTES.checkout;
    close();
  };

  onClick = () => {
    this.setState({ isClick: true });
  };

  clearStatus = () => {
    this.setState({ isClick: false });
  };

  changeScene = (id) => {
    this.setState({
      activeScene: id,
    });
  };

  render() {
    const { activeScene } = this.state;

    const { isOrderEnhance, className, close, isImageUpload, top } = this.props;

    const Scene =
      activeScene &&
      this.ACCOUNT_TABS({
        isOrderEnhance,
        close,
        isImageUpload,
        onClick: this.onClick,
      }).find((item) => item.id === activeScene).component;

    const tabs = isOrderEnhance
      ? this.ACCOUNT_TABS({ isOrderEnhance }).filter(({ id: tabId }) => tabId !== EASY_SHIP_KIK)
      : this.ACCOUNT_TABS({ isOrderEnhance });
    return (
      <section
        className={b({ mix: className })}
        style={top ? { top: `${top}px` } : {}}
        id="select-images-form"
      >
        <div role="button" tabIndex={0} className={b('btn')} onClick={close}>
          <CloseIcon2 />
        </div>
        <Tabs
          tabs={tabs}
          activeTab={this.ACCOUNT_TABS({})[0].id}
          clickHandler={this.changeScene}
          className={b('tabs')}
          buttonClass={b('tabs-btn')}
          withoutSelector
          columnInMobile
        />
        <div className={b('scene')}>{activeScene && <Scene />}</div>
      </section>
    );
  }
}

SelectImagesForm.propTypes = propTypes;
SelectImagesForm.defaultProps = defaultProps;

const stateProps = (state) => ({
  loadedImages: orderImagesSelector(state),
  products: scanShipKipProductsSelector(state),
  isImageUpload: isImageUploadSelector(state),
  price: photoScanSelector(state),
  trialShipKit: trialShipKitSelector(state),
  token: getToken(state),
  userId: userIdSelector(state),
  couponName: couponNameSelect(state),
  error: couponError(state),
  orders: ordersTypeSelect(state),
});

const actions = {
  addOrder: addToCart,
  checkingCoupon: checkCoupon,
};

export default connect(stateProps, actions)(SelectImagesForm);
