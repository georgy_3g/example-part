const TERMS = {
  CART: 'Cart',
  PAYMENT_DETAILS: 'Payment details',
  BILLING_ADDRESS: 'Billing address',
  DELIVERY_ADDRESS: 'Delivery Address',
  SHIPPING_ADDRESS: 'Shipping address',
  COUNTRY: 'Country',
  USA: 'United states',

  STATES: [
    {
      label: 'Alabama',
      value: 'AL',
    },
    {
      label: 'Alaska',
      value: 'AK',
    },
    {
      label: 'Arizona',
      value: 'AZ',
    },
    {
      label: 'Arkansas',
      value: 'AR',
    },
    {
      label: 'California',
      value: 'CA',
    },
    {
      label: 'Colorado',
      value: 'CO',
    },
    {
      label: 'Connecticut',
      value: 'CT',
    },
    {
      label: 'District of Columbia',
      value: 'DC',
    },
    {
      label: 'Delaware',
      value: 'DE',
    },
    {
      label: 'Florida',
      value: 'FL',
    },
    {
      label: 'Georgia',
      value: 'GA',
    },
    {
      label: 'Hawaii',
      value: 'HI',
    },
    {
      label: 'Idaho',
      value: 'ID',
    },
    {
      label: 'Illinois',
      value: 'IL',
    },
    {
      label: 'Indiana',
      value: 'IN',
    },
    {
      label: 'Iowa',
      value: 'IA',
    },
    {
      label: 'Kansas',
      value: 'KS',
    },
    {
      label: 'Kentucky',
      value: 'KY',
    },
    {
      label: 'Louisiana',
      value: 'LA',
    },
    {
      label: 'Maine',
      value: 'ME',
    },
    {
      label: 'Maryland',
      value: 'MD',
    },
    {
      label: 'Massachusetts',
      value: 'MA',
    },
    {
      label: 'Michigan',
      value: 'MI',
    },
    {
      label: 'Minnesota',
      value: 'MN',
    },
    {
      label: 'Mississippi',
      value: 'MS',
    },
    {
      label: 'Missouri',
      value: 'MO',
    },
    {
      label: 'Montana',
      value: 'MT',
    },
    {
      label: 'Nebraska',
      value: 'NE',
    },
    {
      label: 'Nevada',
      value: 'NV',
    },
    {
      label: 'New Hampshire',
      value: 'NH',
    },
    {
      label: 'New Jersey',
      value: 'NJ',
    },
    {
      label: 'New Mexico',
      value: 'NM',
    },
    {
      label: 'New York',
      value: 'NY',
    },
    {
      label: 'North Carolina',
      value: 'NC',
    },
    {
      label: 'North Dakota',
      value: 'ND',
    },
    {
      label: 'Ohio',
      value: 'OH',
    },
    {
      label: 'Oklahoma',
      value: 'OK',
    },
    {
      label: 'Oregon',
      value: 'OR',
    },
    {
      label: 'Pennsylvania',
      value: 'PA',
    },
    {
      label: 'Rhode Island',
      value: 'RI',
    },
    {
      label: 'South Carolina',
      value: 'SC',
    },
    {
      label: 'South Dakota',
      value: 'SD',
    },
    {
      label: 'Tennessee',
      value: 'TN',
    },
    {
      label: 'Texas',
      value: 'TX',
    },
    {
      label: 'Utah',
      value: 'UT',
    },
    {
      label: 'Vermont',
      value: 'VT',
    },
    {
      label: 'Virginia',
      value: 'VA',
    },
    {
      label: 'Washington',
      value: 'WA',
    },
    {
      label: 'West Virginia',
      value: 'WV',
    },
    {
      label: 'Wisconsin',
      value: 'WI',
    },
    {
      label: 'Wyoming',
      value: 'WY',
    },
  ],
  STATES_PLACEHOLDER: 'State',
  FIRST_NAME: 'First Name',
  LAST_NAME: 'Last Name',
  EMAIL: 'Email',
  COMPANY_NAME: 'Company name',
  STREET_ADDRESS: 'Street address',
  UNIT_SUITE: 'Apartment, suite, unit etc. (optional)',
  CITY: 'City',
  POST_CODE: 'Zip code',
  PHONE: 'Phone',
  TEXT_AREA: 'Notes about you order, e.g. special notes for delivery',
  PAYPAL: 'Paypal',
  PAYPAL_TEXT: 'Simple, more secure way to pay',
  BANK_CARD: 'Bak card',
  BANK_CARD_TEXT: 'Pay with Visa, MasterCard, Maestro',
  CARD_INFO: 'Credit card details',
  CARD_NAME: 'Name on card',
  CARD_NUMBER: 'Card number',
  CARD_EXPIRY: 'Expiry date',
  CARD_CVC: 'CVC',
  CHOOSE_PAYMENT: 'Choose your payment method',
  PAY_NOW: 'Pay now',
  SAVE_ADDRESS: 'Save address',
  SAME_ADDRESS: 'Shipping Address same as Billing address',
  MAX_NUMBER_10: 'Should be up to 10 symbols',
};

export default TERMS;
