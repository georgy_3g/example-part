import React, { Component } from 'react';
import bem from 'src/utils/bem';
import { connect } from 'react-redux';
import { Formik } from 'formik';
import * as Yup from 'yup';
import PropTypes from 'prop-types';
import { PHONE_MASK } from 'src/constants';
import colors from 'src/styles/colors.json';
import CustomSelect from 'src/components/inputs/CustomSelect';
import dynamic from 'next/dynamic';
import { getToken } from 'src/redux/auth/selectors';
import { billingInfoSelect, shippingInfoSelect } from 'src/redux/shared/selectors';
import {
  addBillingInfo,
  addShippingInfo,
  getUserBillingInfo,
  getUserShippingInfo,
} from 'src/redux/shared/actions';
import styles from './index.module.scss';
import TERMS from './constants';

const NewCustomInput = dynamic(() => import('src/components/inputs/NewCustomInput'));
const CustomTextarea = dynamic(() => import('src/components/inputs/CustomTextarea'));
const ColorButton = dynamic(() => import('src/components/buttons/ColorButton'));

const b = bem('shipping-info-form', styles);

const {
  BILLING_ADDRESS,
  SHIPPING_ADDRESS,
  COUNTRY,
  USA,
  STATES,
  STATES_PLACEHOLDER,
  FIRST_NAME,
  LAST_NAME,
  EMAIL,
  COMPANY_NAME,
  STREET_ADDRESS,
  UNIT_SUITE,
  CITY,
  POST_CODE,
  PHONE,
  TEXT_AREA,
  SAVE_ADDRESS,
  SAME_ADDRESS,
  MAX_NUMBER_10,
} = TERMS;

const schema = Yup.object().shape({
  billingFirstName: Yup.string().required('SHOULD_BE_FILLED'),
  billingLastName: Yup.string().required('SHOULD_BE_FILLED'),
  billingEmail: Yup.string().required('SHOULD_BE_FILLED'),
  billingStreetAddress: Yup.string().required('SHOULD_BE_FILLED'),
  billingCity: Yup.string().required('SHOULD_BE_FILLED'),
  billingPostCode: Yup.string().required('SHOULD_BE_FILLED'),
  billingPhone: Yup.string()
    .test(
      'count',
      MAX_NUMBER_10,
      (value) => value && String(value.replace(/[_|-]/g, '')).length === 10,
    )
    .required('SHOULD_BE_FILLED'),
  deliveryFirstName: Yup.string().required('SHOULD_BE_FILLED'),
  deliveryLastName: Yup.string().required('SHOULD_BE_FILLED'),
  deliveryStreetAddress: Yup.string().required('SHOULD_BE_FILLED'),
  deliveryPhone: Yup.string()
    .test(
      'count',
      MAX_NUMBER_10,
      (value) => value && String(value.replace(/[_|-]/g, '')).length === 10,
    )
    .required('SHOULD_BE_FILLED'),
});

const defaultProps = {
  getCoupon: {},
  getCouponError: {},
  shippingInfo: {
    apartment: '',
    city: '',
    companyName: '',
    firstName: '',
    lastName: '',
    phone: '',
    state: '',
    streetAddress: '',
    zipCode: '',
    notes: '',
  },
  billingInfo: {
    apartment: '',
    city: '',
    companyName: '',
    email: '',
    firstName: '',
    lastName: '',
    phone: '',
    state: '',
    streetAddress: '',
    zipCode: '',
  },
  getShippingInfo: () => {},
  getBillingInfo: () => {},
  changeOrAddShippingInfo: () => {},
  changeOrAddBillingInfo: () => {},
};

const propTypes = {
  getCoupon: PropTypes.shape({
    isActive: PropTypes.bool,
    value: PropTypes.number,
    id: PropTypes.number,
  }),
  shippingInfo: PropTypes.shape({
    apartment: PropTypes.string,
    city: PropTypes.string,
    companyName: PropTypes.string,
    firstName: PropTypes.string,
    lastName: PropTypes.string,
    phone: PropTypes.string,
    state: PropTypes.string,
    streetAddress: PropTypes.string,
    zipCode: PropTypes.string,
    notes: PropTypes.string,
  }),
  billingInfo: PropTypes.shape({
    apartment: PropTypes.string,
    city: PropTypes.string,
    companyName: PropTypes.string,
    email: PropTypes.string,
    firstName: PropTypes.string,
    lastName: PropTypes.string,
    phone: PropTypes.string,
    state: PropTypes.string,
    streetAddress: PropTypes.string,
    zipCode: PropTypes.string,
  }),
  getCouponError: PropTypes.shape({
    message: PropTypes.string,
    statusCode: PropTypes.number,
  }),
  getShippingInfo: PropTypes.func,
  getBillingInfo: PropTypes.func,
  changeOrAddShippingInfo: PropTypes.func,
  changeOrAddBillingInfo: PropTypes.func,
};

class ShippingInfoForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isQuicklyOrder: false,
      paymentMethod: true,
      isShippingMethod: false,
      sameAddress: false,
    };
  }

  componentDidMount() {
    const { getShippingInfo, getBillingInfo } = this.props;
    getShippingInfo();
    getBillingInfo();
  }

  handleQuickly = () => () => {
    const { isQuicklyOrder } = this.state;
    this.setState({
      isQuicklyOrder: !isQuicklyOrder,
    });
  };

  handlePayment = () => () => {
    const { paymentMethod } = this.state;
    this.setState({
      paymentMethod: !paymentMethod,
    });
  };

  handleShippingMethod = () => () => {
    const { isShippingMethod } = this.state;
    this.setState({
      isShippingMethod: !isShippingMethod,
    });
  };

  onSubmit = ({
    billingFirstName,
    billingLastName,
    billingEmail,
    billingCompanyName,
    billingStreetAddress,
    billingState,
    billingUnit,
    billingCity,
    billingPostCode,
    billingPhone,
    deliveryFirstName,
    deliveryLastName,
    deliveryCompanyName,
    deliveryStreetAddress,
    deliveryState,
    deliveryUnit,
    deliveryCity,
    deliveryPostCode,
    deliveryPhone,
    textArea,
  }) => {
    const { changeOrAddShippingInfo, changeOrAddBillingInfo } = this.props;

    const order = {
      billingAddress: {
        firstName: billingFirstName,
        lastName: billingLastName,
        email: billingEmail,
        companyName: billingCompanyName,
        country: 'USA',
        streetAddress: billingStreetAddress,
        city: billingCity,
        zipCode: billingPostCode,
        apartment: billingUnit,
        state: billingState.value,
        phone: billingPhone.replace(/[_|-]/g, ''),
      },
      shippingAddress: {
        firstName: deliveryFirstName,
        lastName: deliveryLastName,
        companyName: deliveryCompanyName,
        country: 'USA',
        streetAddress: deliveryStreetAddress,
        city: deliveryCity,
        zipCode: deliveryPostCode,
        apartment: deliveryUnit,
        state: deliveryState.value,
        phone: deliveryPhone.replace(/[_|-]/g, ''),
        notes: textArea,
      },
    };

    changeOrAddBillingInfo({ order });
    changeOrAddShippingInfo({ order });
  };

  handleSameAddress =
    ({ values, setValues }) =>
    () => {
      const { sameAddress } = this.state;

      const {
        billingFirstName,
        billingLastName,
        billingCompanyName,
        billingStreetAddress,
        billingUnit,
        billingCity,
        billingState,
        billingPostCode,
        billingPhone,
      } = values;

      if (sameAddress === false) {
        this.setState({ sameAddress: true });
        setValues({
          ...values,
          deliveryFirstName: billingFirstName,
          deliveryLastName: billingLastName,
          deliveryCompanyName: billingCompanyName,
          deliveryStreetAddress: billingStreetAddress,
          deliveryUnit: billingUnit,
          deliveryCity: billingCity,
          deliveryState: billingState,
          deliveryPostCode: billingPostCode,
          deliveryPhone: billingPhone,
        });
      }
      if (sameAddress === true) {
        this.setState({ sameAddress: false });
        setValues({
          ...values,
          deliveryFirstName: '',
          deliveryLastName: '',
          deliveryCompanyName: '',
          deliveryStreetAddress: '',
          deliveryUnit: '',
          deliveryCity: '',
          deliveryPostCode: '',
          deliveryPhone: '',
          deliveryState: null,
        });
      }
    };

  render() {
    const { sameAddress } = this.state;

    const { billingInfo, shippingInfo } = this.props;

    const {
      apartment,
      city,
      companyName,
      email,
      firstName,
      lastName,
      phone,
      state,
      streetAddress,
      zipCode,
    } = billingInfo;

    const {
      apartment: apartmentSh,
      city: citySh,
      companyName: companyNameSh,
      firstName: firstNameSh,
      lastName: lastNameSh,
      phone: phoneSh,
      state: stateSh,
      streetAddress: streetAddressSh,
      zipCode: zipCodeSh,
      notes,
    } = shippingInfo;

    const customStyle = {
      singleValue: (provided) => ({
        ...provided,
        color: `${colors['$dark-slate-blue-color']}`,
        fontWeight: '500',
      }),
      control: (provided) => ({
        ...provided,
        minWidth: '100%',
        backgroundColor: 'transparent !important',
        width: '100%',
        border: 'none !important',
      }),
      placeholder: (provided) => ({
        ...provided,
      }),
    };

    return (
      <main className={b()}>
        <div className={b('orders-wrapper')}>
          <section className={b('info-wrapper')}>
            <Formik
              enableReinitialize
              initialValues={{
                billingFirstName: firstName || '',
                billingLastName: lastName || '',
                billingEmail: email || '',
                billingCompanyName: companyName || '',
                billingStreetAddress: streetAddress || '',
                billingUnit: apartment || '',
                billingCity: city || '',
                billingPostCode: zipCode || '',
                billingPhone: phone || '',
                billingState: state ? { label: state, value: state } : null,
                deliveryFirstName: firstNameSh || '',
                deliveryLastName: lastNameSh || '',
                deliveryCompanyName: companyNameSh || '',
                deliveryStreetAddress: streetAddressSh || '',
                deliveryState: stateSh ? { label: stateSh, value: stateSh } : null,
                deliveryUnit: apartmentSh || '',
                deliveryCity: citySh || '',
                deliveryPostCode: zipCodeSh || '',
                deliveryPhone: phoneSh || '',
                textArea: notes || '',
                cardExpirationDate: '',
                cardNumber: '',
                cardCode: '',
              }}
              validationSchema={schema}
              onSubmit={(values) => {
                this.onSubmit(values);
              }}
              render={({
                errors,
                handleSubmit,
                touched,
                handleChange,
                values,
                setValues,
                setFieldValue,
              }) => (
                <form onSubmit={handleSubmit} className={b('info')}>
                  <div className={b('billing')}>
                    <div className={b('billing-wrapper')}>
                      <div className={b('info-form-row', { 'with-margin': true })}>
                        <span className={b('form-title')}>{BILLING_ADDRESS}</span>
                      </div>
                      <div className={b('info-form-row')}>
                        <NewCustomInput
                          className={b('column-input-field')}
                          onChange={handleChange}
                          name="billingFirstName"
                          placeholder={FIRST_NAME}
                          value={values.billingFirstName}
                          isTouched={touched.billingFirstName}
                          error={errors.billingFirstName}
                          isValid={!errors.billingFirstName}
                        />
                        <NewCustomInput
                          className={b('column-input-field')}
                          onChange={handleChange}
                          name="billingLastName"
                          placeholder={LAST_NAME}
                          value={values.billingLastName}
                          isTouched={touched.billingLastName}
                          error={errors.billingLastName}
                          isValid={!errors.billingLastName}
                        />
                        <NewCustomInput
                          className={b('column-input-field')}
                          onChange={handleChange}
                          name="billingEmail"
                          placeholder={EMAIL}
                          value={values.billingEmail}
                          isTouched={touched.billingEmail}
                          error={errors.billingEmail}
                          isValid={!errors.billingEmail}
                        />
                        <NewCustomInput
                          className={b('column-input-field')}
                          onChange={handleChange}
                          name="billingCompanyName"
                          placeholder={COMPANY_NAME}
                          value={values.billingCompanyName}
                          isTouched={touched.billingCompanyName}
                          error={errors.billingCompanyName}
                          isValid={!errors.billingCompanyName}
                        />
                      </div>
                    </div>
                    <div className={b('billing-wrapper')}>
                      <div className={b('info-form-row', { 'with-margin': true })}>
                        <span className={b('form-title')}>{`${COUNTRY} ${USA}`}</span>
                      </div>
                      <div className={b('info-form-row', { 'with-margin': true })}>
                        <NewCustomInput
                          className={b('column-input-field', { full: true })}
                          onChange={handleChange}
                          name="billingStreetAddress"
                          placeholder={STREET_ADDRESS}
                          value={values.billingStreetAddress}
                          isTouched={touched.billingStreetAddress}
                          error={errors.billingStreetAddress}
                          isValid={!errors.billingStreetAddress}
                        />
                        <NewCustomInput
                          className={b('column-input-field', { full: true })}
                          onChange={handleChange}
                          name="billingCity"
                          placeholder={CITY}
                          value={values.billingCity}
                          isTouched={touched.billingCity}
                          error={errors.billingCity}
                          isValid={!errors.billingCity}
                        />
                      </div>
                      <div className={b('info-form-row')}>
                        <NewCustomInput
                          className={b('column-input-field')}
                          onChange={handleChange}
                          name="billingPostCode"
                          placeholder={POST_CODE}
                          value={values.billingPostCode}
                          isTouched={touched.billingPostCode}
                          error={errors.billingPostCode}
                          isValid={!errors.billingPostCode}
                        />
                        <NewCustomInput
                          className={b('column-input-field')}
                          onChange={handleChange}
                          name="billingUnit"
                          placeholder={UNIT_SUITE}
                          value={values.billingUnit}
                          isTouched={touched.billingUnit}
                          error={errors.billingUnit}
                          isValid={!errors.billingUnit}
                        />
                        <div className={b('select-wrapper')}>
                          <CustomSelect
                            instanceId="reason"
                            className={b('column-select-field')}
                            options={STATES}
                            placeholder={STATES_PLACEHOLDER}
                            value={values.billingState}
                            onChange={(value) => setFieldValue('billingState', value)}
                            error={errors.address2}
                            isValid={!errors.address2}
                            customStyles={customStyle}
                          />
                        </div>
                        <NewCustomInput
                          className={b('column-input-field')}
                          onChange={handleChange}
                          name="billingPhone"
                          placeholder={PHONE}
                          value={values.billingPhone}
                          isTouched={touched.billingPhone}
                          error={errors.billingPhone}
                          isValid={!errors.billingPhone}
                          withMask
                          mask={PHONE_MASK}
                        />
                      </div>
                    </div>
                  </div>
                  <div className={b('delivery', { wrap: sameAddress })}>
                    <div className={b('same-address-box', { 'not-same': !sameAddress })}>
                      <label htmlFor className={b('method-checkbox')}>
                        <input
                          className={b('input')}
                          type="checkbox"
                          onClick={this.handleSameAddress({ values, setValues })}
                          checked={sameAddress}
                        />
                        <span className={b('custom')} />
                        <p className={b('same-address-text')}>{SAME_ADDRESS}</p>
                      </label>
                    </div>
                    {sameAddress && (
                      <div className={b('button-wrapper', { half: true })}>
                        <ColorButton text={SAVE_ADDRESS} type="submit" />
                      </div>
                    )}
                    {!sameAddress && (
                      <>
                        <div className={b('shipping-wrapper')}>
                          <div className={b('info-form-row', { 'with-margin': true })}>
                            <span className={b('form-title')}>{SHIPPING_ADDRESS}</span>
                          </div>
                          <div className={b('info-form-row')}>
                            <NewCustomInput
                              className={b('column-input-field')}
                              onChange={handleChange}
                              name="deliveryFirstName"
                              placeholder={FIRST_NAME}
                              value={values.deliveryFirstName}
                              isTouched={touched.deliveryFirstName}
                              error={errors.deliveryFirstName}
                              isValid={!errors.deliveryFirstName}
                            />
                            <NewCustomInput
                              className={b('column-input-field')}
                              onChange={handleChange}
                              name="deliveryLastName"
                              placeholder={LAST_NAME}
                              value={values.deliveryLastName}
                              isTouched={touched.deliveryLastName}
                              error={errors.deliveryLastName}
                              isValid={!errors.deliveryLastName}
                            />
                            <NewCustomInput
                              className={b('column-input-field', { full: true, desktop: true })}
                              onChange={handleChange}
                              name="deliveryCompanyName"
                              placeholder={COMPANY_NAME}
                              value={values.deliveryCompanyName}
                              isTouched={touched.deliveryCompanyName}
                              error={errors.deliveryCompanyName}
                              isValid={!errors.deliveryCompanyName}
                            />
                          </div>
                        </div>
                        <div className={b('shipping-wrapper')}>
                          <div className={b('info-form-row', { 'with-margin': true })}>
                            <span className={b('form-title')}>{`${COUNTRY} ${USA}`}</span>
                          </div>
                          <div className={b('info-form-row', { 'with-margin': true })}>
                            <NewCustomInput
                              className={b('column-input-field', { full: true })}
                              onChange={handleChange}
                              name="deliveryStreetAddress"
                              placeholder={STREET_ADDRESS}
                              value={values.deliveryStreetAddress}
                              isTouched={touched.deliveryStreetAddress}
                              error={errors.deliveryStreetAddress}
                              isValid={!errors.deliveryStreetAddress}
                            />
                            <NewCustomInput
                              className={b('column-input-field', { full: true })}
                              onChange={handleChange}
                              name="deliveryUnit"
                              placeholder={UNIT_SUITE}
                              value={values.deliveryUnit}
                              isTouched={touched.deliveryUnit}
                              error={errors.deliveryUnit}
                              isValid={!errors.deliveryUnit}
                            />
                          </div>
                          <div className={b('info-form-row', { 'with-margin': true })}>
                            <NewCustomInput
                              className={b('column-input-field')}
                              onChange={handleChange}
                              name="deliveryCity"
                              placeholder={CITY}
                              value={values.deliveryCity}
                              isTouched={touched.deliveryCity}
                              error={errors.deliveryCity}
                              isValid={!errors.deliveryCity}
                            />
                            <NewCustomInput
                              className={b('column-input-field')}
                              onChange={handleChange}
                              name="deliveryPostCode"
                              placeholder={POST_CODE}
                              value={values.deliveryPostCode}
                              isTouched={touched.deliveryPostCode}
                              error={errors.deliveryPostCode}
                              isValid={!errors.deliveryPostCode}
                            />

                            <div className={b('select-wrapper')}>
                              <CustomSelect
                                instanceId="reason"
                                className={b('column-select-field')}
                                options={STATES}
                                placeholder={STATES_PLACEHOLDER}
                                value={values.deliveryState}
                                onChange={(value) => setFieldValue('deliveryState', value)}
                                error={errors.address2}
                                isValid={!errors.address2}
                                customStyles={customStyle}
                              />
                            </div>
                            <NewCustomInput
                              className={b('column-input-field')}
                              onChange={handleChange}
                              name="deliveryPhone"
                              placeholder={PHONE}
                              value={values.deliveryPhone}
                              isTouched={touched.deliveryPhone}
                              error={errors.deliveryPhone}
                              isValid={!errors.deliveryPhone}
                              withMask
                              mask={PHONE_MASK}
                            />
                          </div>
                          <div className={b('info-form-row')}>
                            <div className={b('column-input', { 'full-width': true })}>
                              <CustomTextarea
                                className={b('textarea')}
                                name="textArea"
                                rows="6"
                                onChange={handleChange}
                                placeholder={TEXT_AREA}
                                value={values.textArea}
                                isTouched={touched.textArea}
                                error={errors.textArea}
                                isValid={!errors.textArea}
                              />
                            </div>
                          </div>
                        </div>
                      </>
                    )}
                    {!sameAddress && (
                      <div className={b('button-wrapper')}>
                        <ColorButton text={SAVE_ADDRESS} type="submit" />
                      </div>
                    )}
                  </div>
                </form>
              )}
            />
          </section>
        </div>
      </main>
    );
  }
}

ShippingInfoForm.propTypes = propTypes;
ShippingInfoForm.defaultProps = defaultProps;

const stateProps = (state) => ({
  token: getToken(state),
  shippingInfo: shippingInfoSelect(state),
  billingInfo: billingInfoSelect(state),
});

const actions = {
  getShippingInfo: getUserShippingInfo,
  getBillingInfo: getUserBillingInfo,
  changeOrAddShippingInfo: addShippingInfo,
  changeOrAddBillingInfo: addBillingInfo,
};

export default connect(stateProps, actions)(ShippingInfoForm);
