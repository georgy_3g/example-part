import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import colors from 'src/styles/colors.json';
import bem from 'src/utils/bem';
import dynamic from 'next/dynamic';
import { isMessageLoadedSelect, orderImageMessagesSelect } from 'src/redux/ordersList/selectors';
import { chatTokenSelector, getToken } from 'src/redux/auth/selectors';
import { createOrderImageMessage } from 'src/redux/ordersList/actions';
import SocketIo from 'socket.io-client';
import { base } from 'src/config';
import styles from './index.module.scss';
import TERMS from './constants';

const CustomTextarea = dynamic(() => import('src/components/inputs/CustomTextarea'));
const ArrowIcon = dynamic(() => import('src/components/svg/ArrowIcon'));
const RedButton = dynamic(() => import('src/components/buttons/RedButton'));
const ChatRoom = dynamic(() => import('src/components/popups/ChatRoom'));

const gray = colors['$light-slate-gray-color'];
const port = base || 'http://34.207.168.203:5050';

const b = bem('request-changes-form', styles);
const form = bem('form', styles);

const {
  REQUEST_PLACEHOLDER,
  PROOF_MESSAGES,
  CLOSE_HISTORY,
  BUTTONS_TERMS,
  PRESET_TITLE,
  GO_BACK,
  SUBMIT,
  TEXT,
} = TERMS;

const defaultProps = {
  close: () => {},
  token: '',
  createMessage: () => {},
  isMessageLoaded: false,
  parentId: '',
  className: '',
  withOutChat: false,
  withOutForm: false,
  title: PROOF_MESSAGES,
  showMassageHistory: () => {},
  subtitle: '',
  openIcon: false,
  selectedImageVersionId: null,
  selectedVersionId: null,
  changeIcon: () => {},
  updateProjectList: () => {},
  orderId: null,
  isNewVersion: false,
  isShowPresets: true,
  chatToken: ''
};

const propTypes = {
  close: PropTypes.func,
  token: PropTypes.string,
  isMessageLoaded: PropTypes.bool,
  parentId: PropTypes.string,
  className: PropTypes.string,
  withOutChat: PropTypes.bool,
  withOutForm: PropTypes.bool,
  title: PropTypes.string,
  showMassageHistory: PropTypes.func,
  createMessage: PropTypes.func,
  subtitle: PropTypes.string,
  openIcon: PropTypes.bool,
  changeIcon: PropTypes.func,
  selectedImageVersionId: PropTypes.number,
  selectedVersionId: PropTypes.number,
  updateProjectList: PropTypes.func,
  orderId: PropTypes.number,
  isNewVersion: PropTypes.bool,
  isShowPresets: PropTypes.bool,
  chatToken: PropTypes.string,
};

class RequestChangesForm extends Component {
  constructor(props) {
    super(props);
    const { isNewVersion, isShowPresets, chatToken } = props;
    this.socket = SocketIo(port, { path: '/socket.io', query: { auth: chatToken }});
    this.state = {
      text: '',
      textPlaceholder: REQUEST_PLACEHOLDER,
      isMassageSubmit: false,
      isNewVersion,
      isShowPresets,
      rooms: []
    };
  }

  componentDidMount() {
    const { orderId } = this.props;
    this.socket.on('room', ({ data }) => {
      this.setState({ rooms: data })
    })
    this.socket.emit('getRooms', { orderId });
  }

  // eslint-disable-next-line react/no-deprecated
  componentWillReceiveProps(nextProps) {
    const { isMessageLoaded, showMassageHistory } = this.props;
    const { isMassageSubmit } = this.state;
    if (isMassageSubmit && nextProps.isMessageLoaded !== isMessageLoaded) {
      this.setState({ isMassageSubmit: false }, showMassageHistory);
    }
  }

  onSubmit = (e) => {
    e.preventDefault();
    const {
      token,
      parentId,
      createMessage,
      close,
      updateProjectList,
      selectedVersionId,
    } = this.props;

    const { text, isNewVersion } = this.state;
    if (isNewVersion) {
      createMessage({
        id: parentId,
        token,
        messageText: text,
        isNewVersion,
        updateProjectList,
        selectedVersionId,
      });
    }

    const roomId = this.getRoomId();
    this.socket.emit('sendMessage', {
      roomId,
      message: text,
      important: isNewVersion,
    });

    this.setState({
      text: '',
      isMassageSubmit: true,
      isNewVersion: false,
      isShowPresets: true,
    });
    close(true);
  };

  onChange = () => (e) => {
    const { value } = e.target;
    this.setState({ text: value });
  };

  onClickPreset = (text, isNewVersion) => {
    this.setState({ textPlaceholder: text, isNewVersion, isShowPresets: false });
  };

  getRoomId = () => {
    const { selectedImageVersionId } = this.props;
    const { rooms } = this.state;
    const { id = null } = rooms.find(({ version }) => version && version.orderImageId === selectedImageVersionId) || {};
    return id;
  };

  render() {
    const {
      close,
      className,
      withOutChat,
      withOutForm,
      title,
      subtitle,
      openIcon,
      changeIcon
    } = this.props;
    const { text, isShowPresets, textPlaceholder } = this.state;

    const roomId = this.getRoomId();

    return (
      <div className={b({ mix: className })}>
        {!withOutChat && (
          <>
            <ChatRoom
              isModal={false}
              iconExpansionDecrease
              roomId={roomId}
              title={title}
              subTitle={subtitle}
              openIcon={openIcon}
              changeIcon={changeIcon}
              socket={this.socket}
            />
            <button
              className={b('close-btn', { chatBack: !withOutChat })}
              type="button"
              onClick={() => close(false)}
            >
              <span className={b('popup-back-icon')}>
                <ArrowIcon
                  className={b('back-icon')}
                  width="5"
                  height="11"
                  strokeWidth="3"
                  stroke={gray}
                />
              </span>
              <span className={b('popup-back-text')}>{CLOSE_HISTORY}</span>
            </button>
          </>
        )}
        {!withOutForm && !isShowPresets && (
          <>
            <div className={b('form-border-wrap')}>
              <p className={b('form-title')}>{TEXT}</p>
              <span className={b('form-text')}>{subtitle}</span>
              <form onSubmit={this.onSubmit} className={b('form')}>
                <fieldset className={b('fieldset', { mix: form('fieldset') })}>
                  <CustomTextarea
                    inputClass={b('input')}
                    onChange={this.onChange()}
                    placeholder={textPlaceholder}
                    value={text}
                  />
                </fieldset>
                <RedButton
                  className={b('btn')}
                  type="submit"
                  text={SUBMIT}
                  disabled={!text}
                />
              </form>
            </div>
            <button className={b('close-btn')} type="button" onClick={() => close(false)}>
              <span className={b('popup-back-icon')}>
                <ArrowIcon
                  className={b('back-icon')}
                  width="5"
                  height="11"
                  strokeWidth="3"
                  stroke={gray}
                />
              </span>
              <span className={b('popup-back-text')}>{GO_BACK}</span>
            </button>
          </>
        )}
        {!withOutForm && isShowPresets && (
          <>
            <div className={b('preset-text-block')}>
              <div className={b('preset-border-wrap')}>
                <span className={b('preset-title')}>{PRESET_TITLE}</span>
                <span className={b('preset-subtitle')}>{subtitle}</span>
                <div className={b('preset-buttons-block')}>
                  {BUTTONS_TERMS.map(({ component: Icon, buttonText, isNewVersion }) => (
                    <button
                      className={b('preset-button')}
                      key={buttonText}
                      type="button"
                      onClick={() => {
                        this.onClickPreset(buttonText, isNewVersion);
                      }}
                    >
                      <Icon className={b('preset-button-icon')} />
                      {buttonText}
                    </button>
                  ))}
                </div>
              </div>
            </div>
            <button className={b('close-btn')} type="button" onClick={() => close(false)}>
              <span className={b('popup-back-icon')}>
                <ArrowIcon
                  className={b('back-icon')}
                  width="5"
                  height="11"
                  strokeWidth="3"
                  stroke={gray}
                />
              </span>
              <span className={b('popup-back-text')}>{GO_BACK}</span>
            </button>
          </>
        )}
      </div>
    );
  }
}

RequestChangesForm.propTypes = propTypes;
RequestChangesForm.defaultProps = defaultProps;

const stateProps = (state) => ({
  messages: orderImageMessagesSelect(state),
  token: getToken(state),
  isMessageLoaded: isMessageLoadedSelect(state),
  chatToken: chatTokenSelector(state),
});

const actions = {
  createMessage: createOrderImageMessage,
};

export default connect(stateProps, actions)(RequestChangesForm);
