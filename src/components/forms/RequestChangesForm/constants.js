import dynamic from "next/dynamic";

const SmallBrushIcon = dynamic(() => import('src/components/svg/SmallBrushIcon'));
const SmallDropIcon = dynamic(() => import('src/components/svg/SmallDropIcon'));

const TERMS = {
  PROOF_MESSAGES: 'Proof Messages',
  ADD_MESSAGE: 'Add message',
  MEMOYA: '******* team',
  YOU: 'You',
  TEXT: 'Please provide us with\nyour detailed changes.',
  SUBMIT: 'Submit',
  CLOSE_HISTORY: 'Close history',
  GO_BACK: 'Go back',
  BUTTONS_TERMS: [
    {
      component: SmallBrushIcon,
      buttonText: 'Request changes to your photo',
      isNewVersion: true,
    },
    {
      component: SmallDropIcon,
      buttonText: 'Message us for help',
      isNewVersion: false,
    },
  ],
  PRESET_TITLE: 'What is your message?',
  ERROR_TEXT: 'You`ve reached your included 3 revisions.',
  REQUEST_PLACEHOLDER: 'Type your request here...',
};

export default TERMS;
