import React from 'react';
import { connect } from 'formik';
import PropTypes from 'prop-types';

const defaultProps = {
  formik: PropTypes.shape(PropTypes.any),
};

const propTypes = {
  formik: {},
};

class ErrorFocus extends React.Component {
  componentDidUpdate(prevProps) {
    const { isSubmitting, isValidating, errors } = prevProps.formik;
    const keys = Object.keys(errors);
    if (keys.length > 0 && isSubmitting && !isValidating) {
      const errorElement = document.getElementById(keys[0]);
      if (errorElement) {
        const { top } = errorElement.getBoundingClientRect();
        window.scrollTo({
          top: document.documentElement.scrollTop + top - 100,
          behavior: 'smooth',
        });
      }
    }
  }

  render() {
    return null;
  }
}

ErrorFocus.propTypes = propTypes;
ErrorFocus.defaultProps = defaultProps;

export default connect(ErrorFocus);
