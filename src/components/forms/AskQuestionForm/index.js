import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Formik } from 'formik';
import bem from 'src/utils/bem';
import * as Yup from 'yup';
import DICTIONARY from 'src/constants/dictionary';
import ERRORS from 'src/constants/errors';
import dynamic from 'next/dynamic';
import { sendContactUs } from 'src/redux/shared/actions';
import { FORMIK_INITIAL_VALUES, SHOULD_BE_SELECTED, SUBMIT_BUTTON_TEXT } from './constants';
import styles from './index.module.scss';

const NewCustomInput = dynamic(() => import('src/components/inputs/NewCustomInput'));
const PhotoArtSelector = dynamic(() => import('src/components/inputs/PhotoArtSelector'));
const CustomTextarea = dynamic(() => import('src/components/inputs/CustomTextarea'));

const { SHOULD_BE_FILLED, INVALID_EMAIL } = ERRORS;
const { SUBMIT_SUCCESS } = DICTIONARY;
const ON_SUBMIT_DELAY = 5000;

const schema = Yup.object().shape({
  name: Yup.string().required(SHOULD_BE_FILLED),
  email: Yup.string().email(INVALID_EMAIL).trim().required(SHOULD_BE_FILLED),
  reason: Yup.object({
    value: Yup.string(),
  })
    .nullable()
    .required(SHOULD_BE_SELECTED),
  subject: Yup.string().required(SHOULD_BE_FILLED),
});

const b = bem('ask-question-form', styles);

const defaultProp = {
  className: '',
  sendQuestion: () => {},
  placeholders: {},
  reasonOptions: [],
};

const propTypes = {
  className: PropTypes.string,
  sendQuestion: PropTypes.func,
  placeholders: PropTypes.shape({
    name: PropTypes.string,
    email: PropTypes.string,
    reason: PropTypes.string,
    subject: PropTypes.string,
    phone: PropTypes.string,
  }),
  reasonOptions: PropTypes.arrayOf(PropTypes.shape({})),
};

class AskQuestionForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isMessageSent: false,
    };
  }

  onSubmit = (values) => {
    this.setState({ isMessageSent: true });
    const { email, name, reason, subject, phone } = values;
    const { sendQuestion } = this.props;
    sendQuestion({
      email,
      name,
      reason: reason.value,
      subject,
      phone,
    });
    setTimeout(() => {
      this.setState({ isMessageSent: false });
    }, ON_SUBMIT_DELAY);
  };

  render() {
    const { className, placeholders, reasonOptions } = this.props;
    const { isMessageSent } = this.state;
    const { name, email, reason, subject, phone } = placeholders;

    return (
      <div className={b({ mix: className })}>
        <Formik
          initialValues={{ ...FORMIK_INITIAL_VALUES }}
          validationSchema={schema}
          onSubmit={(values, actions) => {
            this.onSubmit(values);
            actions.resetForm();
          }}
          render={({ errors, handleSubmit, touched, handleChange, setFieldValue, values }) => (
            <form onSubmit={handleSubmit} className={b('form')}>
              <div className={b('main-wrapper')}>
                <div className={b('inputs-wrapper')}>
                  <div className={b('row')}>
                    <NewCustomInput
                      className={b('input')}
                      onChange={handleChange}
                      name="name"
                      placeholder={name}
                      value={values.name}
                      isTouched={Boolean(touched.name)}
                      error={errors.name}
                      isValid={Boolean(!errors.name)}
                    />
                  </div>
                  <div className={b('row')}>
                    <NewCustomInput
                      className={b('input')}
                      onChange={handleChange}
                      name="phone"
                      placeholder={phone}
                      value={values.phone}
                      isTouched={Boolean(touched.phone)}
                      error={errors.phone}
                      isValid={Boolean(!errors.phone)}
                    />
                  </div>
                  <div className={b('row')}>
                    <NewCustomInput
                      className={b('input')}
                      onChange={handleChange}
                      name="email"
                      placeholder={email}
                      value={values.email}
                      isTouched={Boolean(touched.email)}
                      error={errors.email}
                      isValid={Boolean(!errors.email)}
                    />
                  </div>
                  <div className={b('row')}>
                    <PhotoArtSelector
                      instanceId="reason"
                      className={b('select')}
                      options={reasonOptions}
                      placeholder={reason}
                      value={values.reason}
                      onChange={(value) => setFieldValue('reason', value)}
                      isTouched={Boolean(touched.reason)}
                      isValid={Boolean(!errors.reason)}
                      error={errors.reason}
                    />
                  </div>
                </div>
                <div className={b('text-area-btn-wrapper')}>
                  <div className={b('row')}>
                    <CustomTextarea
                      className={b('textarea')}
                      name="subject"
                      rows="6"
                      onChange={handleChange}
                      placeholder={subject}
                      value={values.subject}
                      isTouched={Boolean(touched.subject)}
                      error={errors.subject}
                      isValid={Boolean(!errors.subject)}
                    />
                  </div>
                  <div className={b('submit-wrap')}>
                    <button className={b('btn')} type="submit">
                      {SUBMIT_BUTTON_TEXT}
                    </button>
                    {isMessageSent && <p className={b('submit-msg')}>{SUBMIT_SUCCESS}</p>}
                  </div>
                </div>
              </div>
            </form>
          )}
        />
      </div>
    );
  }
}

AskQuestionForm.defaultProps = defaultProp;
AskQuestionForm.propTypes = propTypes;

const actions = {
  sendQuestion: sendContactUs,
};

export default connect(null, actions)(AskQuestionForm);
