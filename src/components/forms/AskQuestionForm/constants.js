const SHOULD_BE_SELECTED = 'Should be selected';
const SUBMIT_BUTTON_TEXT = 'Submit';

const FORMIK_INITIAL_VALUES = {
  name: '',
  email: '',
  subject: '',
  reason: null,
  phone: '',
};

export { SHOULD_BE_SELECTED, SUBMIT_BUTTON_TEXT, FORMIK_INITIAL_VALUES };
