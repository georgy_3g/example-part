import * as Yup from 'yup';
import toCardExpiry from 'src/utils/toCardExpiry';
import cleanCardNumber from 'src/utils/cleanCardNumber';

const TERMS = {
  CART: 'Cart',
  PAYMENT_INFO: 'Payment info',
  TEXT_AREA: 'Notes about you order, e.g. special notes for delivery',
  CARD_INFO: 'Credit card details',
  CARD_NAME: 'Name on card',
  CARD_NUMBER: 'Card number',
  CARD_EXPIRY: 'Expiry date',
  CARD_CVC: 'CVC',
  CHOOSE_PAYMENT: 'Choose your payment method',
  SAVE_CARD: 'Save card',
  SHOULD_BE_FILLED: 'Should be filled',
  SHOULD_BE_NUMBER: 'Should be number',
  MAX_NUMBER_3: 'Should be up to 3 symbols',
  MAX_NUMBER_4: 'Should be up to 4 symbols',
};

const SCHEMA = Yup.object().shape({
  cardExpirationDate: Yup.string()
    .test('required', TERMS.SHOULD_BE_FILLED, (value) => value && toCardExpiry(value).length)
    .test('count', TERMS.MAX_NUMBER_4, (value) => value && toCardExpiry(value).length === 4),
  cardNumber: Yup.string()
    .typeError(TERMS.SHOULD_BE_NUMBER)
    .test('required', TERMS.SHOULD_BE_FILLED, (value) => value && cleanCardNumber(value).length),
  cardCode: Yup.number()
    .test('count', TERMS.MAX_NUMBER_3, (value) => value && String(value).length === 3)
    .integer()
    .typeError(TERMS.SHOULD_BE_NUMBER)
    .required(TERMS.SHOULD_BE_FILLED),
  });

export { TERMS, SCHEMA };
