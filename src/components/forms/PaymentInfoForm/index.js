import React from 'react';
import bem from 'src/utils/bem';
import toCardExpiry from 'src/utils/toCardExpiry';
import cleanCardNumber from 'src/utils/cleanCardNumber';
import { Formik } from 'formik';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { CARD_EXPIRATION_DATE, CARD_MASK } from 'src/constants';
import inputCardIcon from 'public/img/shared/inputCardIcon.png';
import dynamic from 'next/dynamic';
import { addPaymentInfo } from 'src/redux/shared/actions';
import { SCHEMA, TERMS } from './constants';
import styles from './index.module.scss';

const NewCustomInput = dynamic(() => import('src/components/inputs/NewCustomInput'));
const ColorButton = dynamic(() => import('src/components/buttons/ColorButton'));


const b = bem('payment-info-form', styles);
const {
  CARD_NUMBER,
  CARD_EXPIRY,
  CARD_CVC,
  SAVE_CARD,
} = TERMS;

const defaultProps = {
  addCard: () => {},
};

const propTypes = {
  addCard: PropTypes.func,
};

const PaymentInfoForm = ({ addCard }) => {
  const addMewCard = ({ cardExpirationDate, cardNumber, cardCode }) => {
    addCard({
      cardExpirationDate: toCardExpiry(cardExpirationDate),
      cardNumber: cleanCardNumber(cardNumber),
      cardCode,
    });
  };

  return (
    <div className={b()}>
      <div className={b('orders-wrapper')}>
        <section className={b('info-wrapper')}>
          <Formik
            enableReinitialize
            initialValues={{
              cardExpirationDate: '',
              cardNumber: '',
              cardCode: '',
            }}
            validationSchema={SCHEMA}
            onSubmit={(values, actions) => {
              addMewCard(values);
              actions.resetForm();
            }}
            render={({ errors, handleSubmit, touched, handleChange, values }) => (
              <form onSubmit={handleSubmit} className={b('info')}>
                <div className={b('payment-method')}>
                  <div className={b('card-info')}>
                    <div className={b('info-form-row')}>
                      <div className={b('column-input', { 'full-width': true })}>
                        <NewCustomInput
                          className={b('column-input-field')}
                          onChange={handleChange}
                          name="cardNumber"
                          placeholder={CARD_NUMBER}
                          value={values.cardNumber}
                          isTouched={touched.cardNumber}
                          error={errors.cardNumber}
                          isValid={!errors.cardNumber}
                          withMask
                          mask={CARD_MASK}
                          withIcon
                          backGroundImage={inputCardIcon}
                        />
                      </div>
                    </div>
                    <div className={b('info-form-row')}>
                      <div className={b('column-input', { 'first-in-row': true })}>
                        <NewCustomInput
                          className={b('column-input-field')}
                          onChange={handleChange}
                          name="cardExpirationDate"
                          placeholder={CARD_EXPIRY}
                          value={values.cardExpirationDate}
                          isTouched={touched.cardExpirationDate}
                          error={errors.cardExpirationDate}
                          isValid={!errors.cardExpirationDate}
                          withMask
                          mask={CARD_EXPIRATION_DATE}
                        />
                      </div>
                      <div className={b('column-input')}>
                        <NewCustomInput
                          className={b('column-input-field')}
                          onChange={handleChange}
                          name="cardCode"
                          placeholder={CARD_CVC}
                          value={values.cardCode}
                          isTouched={touched.cardCode}
                          error={errors.cardCode}
                          isValid={!errors.cardCode}
                          maxLength="3"
                        />
                      </div>
                    </div>
                    <div className={b('btn')}>
                      <ColorButton className={b('btn')} type="submit" text={SAVE_CARD} />
                    </div>
                  </div>
                </div>
              </form>
            )}
          />
        </section>
      </div>
    </div>
  );
}

PaymentInfoForm.propTypes = propTypes;
PaymentInfoForm.defaultProps = defaultProps;

const actions = {
  addCard: addPaymentInfo,
};

export default connect(null, actions)(PaymentInfoForm);
