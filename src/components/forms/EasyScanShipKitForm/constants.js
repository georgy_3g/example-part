const QUANTITY = 'Quantity';
const LARGEST_SIZE = 'Largest Size';
const MEDIA_TYPE = 'Media Type';
const ADD_TO_CART = 'Add to cart';
const PER_IMAGE = 'per image';
const DELIVERY_PRICE = 15;
const TITLE = 'What size ship kit are we sending you?';
const DESC = 'Select the size and type of the largest\n photo you are sending us';

export { QUANTITY, LARGEST_SIZE, MEDIA_TYPE, ADD_TO_CART, PER_IMAGE, DELIVERY_PRICE, TITLE, DESC };
