import React, { Component } from 'react';
import bem from 'src/utils/bem';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import CustomSelect from 'src/components/inputs/CustomSelect';

import ROUTES from 'src/constants/routes';
import dynamic from 'next/dynamic';
import ordersTypeSelect from 'src/redux/orders/selectors';
import { orderPriceSelect } from 'src/redux/prices/selectors';
import { addToCart } from 'src/redux/orders/actions';
import styles from './index.module.scss';
import { ADD_TO_CART, LARGEST_SIZE, MEDIA_TYPE, TITLE } from './constants';

const RedButton = dynamic(() => import('src/components/buttons/RedButton'));

const b = bem('easy-scan-ship-kit-form', styles);

const defaultProps = {
  className: '',
  typeOrders: {},
  prices: {},
  addOrder: () => {  },
  close: () => {  },
};

const propTypes = {
  className: PropTypes.string,
  typeOrders: PropTypes.shape({}),
  prices: PropTypes.shape({
    photo_art_ship_kit: PropTypes.shape({
      price: PropTypes.oneOf([PropTypes.string, PropTypes.number]),
      name: PropTypes.string,
      displayName: PropTypes.string,
      id: PropTypes.number,
    }),
  }),
  addOrder: PropTypes.func,
  close: PropTypes.func,
};

class EasyScanShipKitForm extends Component {
  constructor(props) {
    super(props);
    const { prices } = props;
    const { options: easyScanOptions } = prices.photo_art_ship_kit || { options: [] };
    const sizes = [];
    const materials = {};
    easyScanOptions.forEach(({ shipKitMaterial, shipKitSize }) => {
      if (!sizes.some(({ id }) => id === shipKitSize.id)) {
        sizes.push({ ...shipKitSize, label: shipKitSize.name });
      }
      if (
        materials[shipKitSize.id]
        && !materials[shipKitSize.id].some(({ id }) => id === shipKitMaterial.id)
      ) {
        materials[shipKitSize.id].push({ ...shipKitMaterial, label: shipKitMaterial.name });
      }
      if (!materials[shipKitSize.id]) {
        materials[shipKitSize.id] = [{ ...shipKitMaterial, label: shipKitMaterial.name }];
      }
    });

    const newSizes = sizes.map((item) => {
      const { name } = item;
      const [width, height] = name.split('x');
      return { ...item, width, height };
    }).sort(({ width: firstWidth }, { width: secondWidth }) => firstWidth - secondWidth);

    const selectMaterial = easyScanOptions.length > 0 ? materials[newSizes[0].id][0] : null;

    this.state = {
      count: 1,
      sizes: newSizes,
      materials,
      selectSize: newSizes[0] || null,
      selectMaterial,
    };
  }

  changeCount = value => () => {
    const { count } = this.state;
    if (count + value >= 1) {
      this.setState({ count: count + value });
    }
  }

  changeSize = (value) => {
    const { materials } = this.state;
    const selectMaterial = materials[value.id][0];

    this.setState({
      selectSize: value,
      selectMaterial,
    });
  }

  changeMaterial = (value) => {
    this.setState({ selectMaterial: value });
  }

  addProduct = () => {
    const { count, selectSize, selectMaterial } = this.state;
    const {
      addOrder,
      prices,
      close,
    } = this.props;
    const {
      price,
      name,
      displayName,
      id: productId,
    } = prices.photo_art_ship_kit;
    const selectedProduct = this.getSelectedOption(selectSize.id, selectMaterial.id);
    const {
      shipKitMaterial,
      shipKitSize,
      price: optionPrice,
      id: optionId,
      displayName: optionDisplayName,
      name: optionName,
    } = selectedProduct || {};

    const order = {
      title: displayName,
      typeProduct: name,
      price,
      quantity: count,
      isGift: false,
      productId,
      isMatting: false,
      shipKitMaterial,
      shipKitSize,
      options: [{
        optionId,
        quantity: 1,
        price: optionPrice,
        displayName: optionDisplayName,
        name: optionName,
      }],
    };

    addOrder(order);
    window.location.href = ROUTES.checkout;
    close();
  }

  getSelectedOption = (sizeId, materialId) => {
    const { prices } = this.props;
    const { options: easyScanOptions = [] } = prices.photo_art_ship_kit || {};
    return easyScanOptions.find(({ shipKitMaterial, shipKitSize }) => (
      shipKitMaterial.id === materialId && shipKitSize.id === sizeId
    ));
  }

  render() {
    const {
      sizes,
      materials,
      selectSize,
      selectMaterial,
    } = this.state;
    const { className } = this.props;
    const materialsOptions = materials[selectSize.id];

    return (
      <div className={b({ mix: className })}>
        <span className={b('title')}>{TITLE}</span>
        <div className={b('inputs-block')}>
          <div className={b('column')}>
            <span className={b('name')}>{LARGEST_SIZE}</span>
            <CustomSelect
              className={b('select-block')}
              selectClass={b('select')}
              options={sizes}
              value={selectSize}
              onChange={this.changeSize}
            />
          </div>
          <div className={b('column')}>
            <span className={b('name')}>{MEDIA_TYPE}</span>
            <CustomSelect
              className={b('select-block')}
              selectClass={b('select')}
              options={materialsOptions}
              value={selectMaterial}
              onChange={this.changeMaterial}
            />
          </div>
        </div>
        <RedButton
          onClick={this.addProduct}
          className={b('btn')}
          text={ADD_TO_CART}
        />
      </div>
    );
  }
}

EasyScanShipKitForm.propTypes = propTypes;
EasyScanShipKitForm.defaultProps = defaultProps;

const stateProps = state => ({
  typeOrders: ordersTypeSelect(state),
  prices: orderPriceSelect(state),
});

const actions = {
  addOrder: addToCart,
};

export default connect(stateProps, actions)(EasyScanShipKitForm);
