import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import DICTIONARY from 'src/constants/dictionary';
import INPUTS_NAMES from 'src/constants/inputsNames';
import ERRORS from 'src/constants/errors';
import { Formik } from 'formik';
import bem from 'src/utils/bem';
import dynamic from 'next/dynamic';
import * as Yup from 'yup';
import { error , isLoadingShared, userProfile, userUpdated } from 'src/redux/shared/selectors';
import { getUser, updateUser } from 'src/redux/shared/actions';
import styles from './index.module.scss';

const ColorButton = dynamic(() => import('src/components/buttons/ColorButton'));
const NewCustomInput = dynamic(() => import('src/components/inputs/NewCustomInput'));

const b = bem('user-account', styles);
const form = bem('form', styles);

const { EMAIL, PASSWORD, FIRST_NAME, LAST_NAME, UPDATE } = DICTIONARY;
const { SHORT_NAME, INVALID_EMAIL, SHORT_PASSWORD } = ERRORS;

const SCHEMA_USER = Yup.object().shape({
  email: Yup.string().email(INVALID_EMAIL),
  password: Yup.string().min(6, SHORT_PASSWORD),
  name: Yup.string().min(2, SHORT_NAME),
});

const defaultProps = {
  updateUser: () => {},
  getUser: () => {},
  user: {},
  isUserUpdate: false,
  hasError: '',
};

const propTypes = {
  updateUser: PropTypes.func,
  getUser: PropTypes.func,
  user: PropTypes.shape({
    email: PropTypes.string,
    firstName: PropTypes.string,
    lastName: PropTypes.string,
    message: PropTypes.string,
  }),
  isUserUpdate: PropTypes.bool,
  hasError: PropTypes.shape({
    error: PropTypes.string,
  }),
};

class UserAccountForm extends Component {
  componentDidMount() {
    const { getUser: getUserProfile } = this.props;
    getUserProfile();
  }

  onSubmit = (values) => {
    const { updateUser: editUser } = this.props;
    editUser(values);
  };

  render() {
    const {
      user: { email, firstName, lastName, message },
      isUserUpdate,
      hasError,
    } = this.props;
    const emailValue = email || '';
    const firstNameValue = firstName || '';
    const lastNameValue = lastName || '';
    return (
      <div className={b()}>
        {hasError ? (
          <p className={b('error')}>
            You logged in as administrator. Please login with the user credentials.
          </p>
        ) : (
          <Formik
            initialValues={{
              email: emailValue,
              password: '',
              firstName: firstNameValue,
              lastName: lastNameValue,
            }}
            enableReinitialize
            validationSchema={SCHEMA_USER}
            onSubmit={(values) => {
              const updateValues = emailValue === values.email ? {
                firstName: values.firstName,
                lastName: values.firstName,
                password: values.password,
              } : {
                ...values,
              }
              this.onSubmit(updateValues);
            }}
            render={({ errors, handleSubmit, touched, handleChange, values }) => (
              <form onSubmit={handleSubmit} className={b('form', { mix: form() })}>
                <NewCustomInput
                  className={b('input')}
                  onChange={handleChange}
                  name={INPUTS_NAMES.firstName}
                  placeholder={FIRST_NAME}
                  value={values.firstName}
                  isTouched={touched.firstName}
                  error={errors.firstName}
                  isValid={!errors.firstName}
                />
                <NewCustomInput
                  className={b('input')}
                  onChange={handleChange}
                  name={INPUTS_NAMES.lastName}
                  placeholder={LAST_NAME}
                  value={values.lastName}
                  isTouched={touched.lastName}
                  error={errors.lastName}
                  isValid={!errors.lastName}
                />
                <NewCustomInput
                  className={b('input')}
                  onChange={handleChange}
                  name={INPUTS_NAMES.email}
                  placeholder={EMAIL}
                  value={values.email}
                  isTouched={touched.email}
                  error={errors.email}
                  isValid={!errors.email}
                />
                <NewCustomInput
                  className={b('input')}
                  onChange={handleChange}
                  name={INPUTS_NAMES.password}
                  placeholder={PASSWORD}
                  type={PASSWORD}
                  value={values.password}
                  isTouched={touched.password}
                  error={errors.password}
                  isValid={!errors.password}
                />
                <div className={b('btn-container')}>
                  <ColorButton className={b('btn-container')} text={UPDATE} type="submit" />
                </div>
                {isUserUpdate && <p className={b('success')}>{message}</p>}
              </form>
            )}
          />
        )}
      </div>
    );
  }
}

UserAccountForm.propTypes = propTypes;
UserAccountForm.defaultProps = defaultProps;

const stateProps = (state) => ({
  loading: isLoadingShared(state),
  isUserUpdate: userUpdated(state),
  user: userProfile(state),
  hasError: error(state),
});

const actions = {
  updateUser,
  getUser,
};

export default connect(stateProps, actions)(UserAccountForm);
