interface Props {
  links: {
    title: string;
    url: string;
  }[];
  promoSection: {
    id: number;
  };
}

export default Props;
