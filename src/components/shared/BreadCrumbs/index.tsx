import React, { FunctionComponent, useEffect, useState } from 'react';
import bem from 'src/utils/bem';
import debounce from 'lodash/debounce';
import { connect } from 'react-redux';
import { promoSectionSelector } from 'src/redux/promo/selectors';
import Props from './interface';
import styles from './index.module.scss';

const b = bem('breadcrumbs', styles);

const defaultProps = {
  links: [],
  promoSection: {
    id: 0,
  },
};

const BreadCrumbs: FunctionComponent<Props> = (props) => {
  const { links, promoSection } = props;
  const [windowWidth, setWindowWidth] = useState(1920);
  const [heightNavigation, setHeightNavigation] = useState(0);

  const setResize = () => {
    const { body } = document;
    const element = document.documentElement;
    const result = element.clientHeight ? element : body;
    setWindowWidth(result.offsetWidth);
  };

  const resizeWindow = debounce(() => {
    setResize();
  }, 300);

  const calculateMarginBreadCrumbs = () => {
    const navigation = document.getElementById('navigation');
    const promobanner = document.getElementById('promo-banner');
    const result =
    (navigation ? navigation.offsetHeight : 0) +
    (promobanner ? promobanner.offsetHeight : 10) +
    (windowWidth < 1024 ? 60 : 0) +
    (windowWidth > 1024 ? 10 : 0) +
    (windowWidth < 480 ? -10 : 0) +
    52;
    setHeightNavigation(result);
  }

  useEffect(() => {
    window.addEventListener('resize', resizeWindow);
    setResize();
    calculateMarginBreadCrumbs();
  }, []);

  useEffect(() => {
    calculateMarginBreadCrumbs();
  }, [windowWidth, promoSection]);


  return (
    <div className={b('')} style={{ marginTop: heightNavigation }}>
      <div className={b('row')}>
        {links.map((item, index) => {
          const { title, url } = item;

          return (
            <div className={b('link')}>
              <a className={b('text')} href={url}>
                {title}
              </a>
              {index < links.length - 1 && <div className={b('arrow')} />}
            </div>
          );
        })}
      </div>
    </div>
  );
};

BreadCrumbs.defaultProps = defaultProps;

const mapStateToProps = (state: any) => ({
  promoSection: promoSectionSelector(state),
});

export default connect(mapStateToProps, null)(BreadCrumbs);
