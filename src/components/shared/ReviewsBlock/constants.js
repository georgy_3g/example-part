const TERMS = {
  TITLE: 'Our customers love us.',
  REVIEWS: 'Reviews: ',
  SEO_TITLE: 'Photo Restoration and Media Digitizing Reviews',
  SEO_DESCRIPTION:
    'My photos came out fantastic! We thought they were unsalvageable! So glad I found this service!!!!',
  SORTING_DESC: 'Sorting by:',
  NEW: 'New',
  OLD: 'Old',
  DESC: 'desc',
  ASC: 'asc',
};

export default TERMS;
