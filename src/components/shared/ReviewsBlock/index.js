import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import bem from 'src/utils/bem';
import debounce from 'lodash/debounce';
import dynamic from 'next/dynamic';
import {
  reviewsCountSelect,
  reviewsLoadingSelect,
  reviewsRateSelect,
  reviewsSelect,
  reviewTotalPagesSelect,
} from 'src/redux/reviewers/selectors';
import { loadNewReviewers } from 'src/redux/reviewers/actions';
import LazyLoad from 'react-lazyload';
import TERMS from './constants';
import styles from './index.module.scss';

const Paginate = dynamic(() => import('src/components/elements/Paginate'));
const ReviewCard = dynamic(() => import('src/components/cards/ReviewCard'));
const ArrowIcon = dynamic(() => import('src/components/svg/ArrowIcon'));
const Rate = dynamic(() => import('src/components/elements/Rate'));

const { TITLE, REVIEWS } = TERMS;
const b = bem('reviews-block', styles);

const propTypes = {
  reviews: PropTypes.arrayOf(PropTypes.shape({})),
  totalReviews: PropTypes.number,
  loadReviews: PropTypes.func,
  totalPages: PropTypes.number,
  averageRating: PropTypes.number,
  title: PropTypes.string,
  description: PropTypes.string,
  customClass: PropTypes.string,
  isEnhance: PropTypes.bool,
  isDigitize: PropTypes.bool,
};

const defaultProps = {
  reviews: [],
  totalReviews: 0,
  loadReviews: () => {},
  totalPages: 0,
  averageRating: 5,
  title: TITLE,
  description: '',
  customClass: '',
  isEnhance: false,
  isDigitize: false,
};

class ReviewsBlock extends Component {
  resizeWindow = debounce(() => {
    this.getContent();
  }, 300);

  constructor(props) {
    super(props);
    this.state = {
      limit: 6,
      offset: 0,
      order: 'desc',
    };
  }

  componentDidMount() {
    const { limit, offset, order } = this.state;
    const { loadReviews } = this.props;
    window.addEventListener('resize', this.resizeWindow);
    this.windowWidth = window.innerWidth;
    loadReviews({ limit: window.innerWidth <= 480 ? 4 : limit, order, offset });
  }

  componentDidUpdate() {
    const { limit, offset, order } = this.state;
    const { loadReviews, reviews } = this.props;
    window.addEventListener('resize', this.resizeWindow);
    this.windowWidth = window.innerWidth;

    const reviewsOnlyGood = reviews.filter(item => item.rating >= 4);

    if ((reviews.length - reviewsOnlyGood.length) > 0) {
      loadReviews({ limit: window.innerWidth <= 480 ? 4 + (reviews.length - reviewsOnlyGood.length) : limit + (reviews.length - reviewsOnlyGood.length), order, offset });
    }
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.resizeWindow);
    this.windowWidth = null;
  }

  handlePageClick = ({ selected }) => {
    const { limit, order } = this.state;
    const { loadReviews } = this.props;
    this.setState({ offset: selected })

    loadReviews({ limit, order, offset: selected });
  };

  getContent = () => {
    const { order, offset } = this.state;
    const { loadReviews } = this.props;

    if (window.innerWidth <= 480) {
      loadReviews({ limit: 4, order, offset });
    } else if (window.innerWidth <= 1024) {
      loadReviews({ limit: 6, order, offset });
    } else if (window.innerWidth > 1024) {
      loadReviews({ limit: 6, order, offset });
    }
  };

  render() {
    const {
      totalReviews,
      reviews,
      totalPages,
      averageRating,
      title,
      description,
      customClass,
      isEnhance,
      isDigitize,
    } = this.props;

    const reviewsOnlyGood = reviews.filter(item => item.rating >= 4);

    return (
      <main className={b({ mix: customClass, enhance: isEnhance })}>
        <h2 className={b('title')}>{title}</h2>
        {description && <div className={b('description', { enhance: isEnhance, digitize: isDigitize })}>{description}</div>}
        <LazyLoad offset={100}>
          <div className={b('wrapper')}>
            <div className={b('reviews-wrapper')}>
              <div className={b('review-statistic')}>
                <div className={b('total-reviews-wrapper')}>
                  <div className={b('total-reviews-title')}>{REVIEWS}</div>
                  <div className={b('total-reviews')}>{totalReviews}</div>
                </div>
                <div className={b('rating')}>
                  <Rate rate={averageRating} />
                </div>
              </div>
            </div>
            <div className={b('reviews-list')}>
              {reviewsOnlyGood.map((review) => {
              const { invitation_id: key } = review;
              return <ReviewCard review={review} key={key} />;
            })}
              <div className={b('stripe')} />
            </div>
          </div>
          <div className={b('pagination-wrapper')}>
            <Paginate
              previousLabel={<ArrowIcon width={9} height={16} className={b('back-arrow')} />}
              nextLabel={<ArrowIcon width={9} height={16} className={b('next-arrow')} />}
              breakLabel="..."
              breakClassName={b('pagination-break')}
              breakLinkClassName={b('pagination-break-link')}
              pageCount={totalPages}
              marginPagesDisplayed={1}
              pageRangeDisplayed={2}
              onPageChange={this.handlePageClick}
              containerClassName={b('pagination')}
              subContainerClassName={b('pagination-pages')}
              pageClassName={b('pagination-page')}
              pageLinkClassName={b('pagination-page-link')}
              activeClassName={b('active-page')}
              previousClassName={b('back-wrapper')}
              nextClassName={b('next-wrapper')}
              previousLinkClassName={b('back')}
              nextLinkClassName={b('next')}
              disabledClassName={b('disabled')}
            />
          </div>
        </LazyLoad>
      </main>
    );

  }
}

ReviewsBlock.propTypes = propTypes;
ReviewsBlock.defaultProps = defaultProps;

const stateProps = (state) => ({
  reviews: reviewsSelect(state),
  totalReviews: reviewsCountSelect(state),
  averageRating: reviewsRateSelect(state),
  isLoading: reviewsLoadingSelect(state),
  totalPages: reviewTotalPagesSelect(state),
});

const actions = {
  loadReviews: loadNewReviewers,
};

export default connect(stateProps, actions)(ReviewsBlock);
