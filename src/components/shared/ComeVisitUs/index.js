import React, { useEffect, useRef } from 'react';
import bem from 'src/utils/bem';
import PropTypes from 'prop-types';

import map from 'public/img/contact/contact-map.jpg';
import tabMap from 'public/img/contact/contact-map-tab.jpg';
import mobMap from 'public/img/contact/contact-map-mob.jpg';
import dynamic from 'next/dynamic';
import styles from './index.module.scss';

const ComeVisitUsInfo = dynamic(() => import('src/components/widgets/ComeVisitUsInfo'));

const b = bem('come-visit-us', styles);
const propTypes = {
  className: PropTypes.string,
};

const defaultProps = {
  className: '',
};

const ComeVisitUs = ({ className }) => {
  const ref = useRef();

  const scroll = () => {
    ref.current.scrollIntoView({ block: 'center', behavior: 'smooth' });
  };

  useEffect(() => {
    if (window.location.search === '?map') {
      scroll();
    }
  }, []);

  return (
    <section ref={ref} className={`${className} ${b()}`}>
      <ComeVisitUsInfo className={b('info-block')} />
      <div className={b('map-wrapper')}>
        <img
          className={b('map', { des: true })}
          src={map.src}
          alt="Map"
        />
        <img
          className={b('map', { tab: true })}
          src={tabMap.src}
          alt="Map"
        />
        <img
          className={b('map', { mob: true })}
          src={mobMap.src}
          alt="Map"
        />
      </div>
    </section>
  );
}

ComeVisitUs.propTypes = propTypes;
ComeVisitUs.defaultProps = defaultProps;

export default ComeVisitUs;
