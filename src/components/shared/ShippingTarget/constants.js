import * as Yup from 'yup';

const TOP_TEXT = 'Contactless local pickup service or 3-way home ship kits offered nationwide.';
const YOUR_ZIP_CODE = 'Your zip code:';
const GREAT_NEWS = 'Great news! \nWe have easy pickup &\n delivery options \nin your area';
const HOME_SHIP_KIT_AVAILABLE = 'The home ship kit\noptions is available at\nyour location.';
const SELECT_HOME_SHIP_KIT = 'Select the home ship kit\noption at checkout.';
const BOTH_OPTIONS_AVAILABLE = 'Both options are\navailable at your\nlocation.';
const SELECT_PREFERED_OPTION = 'Select your prefered option\nat checkout.';
const HOME_SHIP_KIT = 'Home ship kit';
const AVAILABLE = 'Available!';
const NOT_AVAILABLE = 'Not available.';
const ENTER_ZIP_CODE =
  'Enter your zip code\nto see which options\nare available at your\nlocation.';
const ONE_DAY_MILLISECONDS = 86400000;
const BUTTON_TITLE = 'Are you local to South Florida?';
const BUTTON_TEXT = 'Visit us or schedule a pickup.';
const BUTTON_LINK = 'See your options';
const TITLE = 'Are you local to us\nin South Florida?';
const TEXT = 'Let’s see what pickup & delivery options are available in your area.';
const CHECK_BOX_TEXT = 'Use my current location';
const BOTTOM_TEXT = 'Select your preferred\noption at checkout';
const INPUT_PLACEHOLDER = 'Enter your zip code';
const NATIONAL_TITLE = 'Simple & secure nationwide\ndelivery options.';
const NATIONAL_TEXT = 'Check the delivery options in your area.';
const SUBMIT = 'Submit';
const YOUR_NAME = 'Your name';
const YOUR_EMAIL = 'Your email';
const YOUR_PHONE = 'Your phone';
const MESSAGE_ARTIST = 'Message to the artist';
const SHOULD_BE_FILLED = 'Should be filled';
const UPLOAD_PHOTOS = 'Upload photos';
const SELECT_CHECKOUT = 'Select your preferred option at checkout.';
const PLACE_ORDER_NOW = 'Place your order now';
const ICON_TITLE_NO_LOCAL = 'It’s easy to get started.';
const ICON_DESCRIPTION_NO_LOCAL = 'Ship kit or local pickup.';
const OR_CALL_ASSISCTENCE = 'Or call us for assistance';
const PHONE = '561-235-7808';
const PHONE_NO_LOCAL = '800-916-6076';
const CHECK_AVAILABILITY_AREA = 'Check availability in your area';
const PICKUP_AVAILABLE_AREA = 'Pickup available in this area!';
const ORDER_NOW = 'Order now';
const NOT_AVAILABLE_LOCATION = 'Not available at this location.';

const SCHEMA = Yup.object().shape({
  name: Yup.string()
  .required(SHOULD_BE_FILLED),
  email: Yup.string()
  .required(SHOULD_BE_FILLED),
  phone: Yup.string()
  .required(SHOULD_BE_FILLED),
  message: Yup.string()
  .required(SHOULD_BE_FILLED),
});

export {
  SCHEMA,
  TOP_TEXT,
  BUTTON_LINK,
  YOUR_ZIP_CODE,
  GREAT_NEWS,
  HOME_SHIP_KIT_AVAILABLE,
  SELECT_HOME_SHIP_KIT,
  BOTH_OPTIONS_AVAILABLE,
  SELECT_PREFERED_OPTION,
  HOME_SHIP_KIT,
  AVAILABLE,
  NOT_AVAILABLE,
  ENTER_ZIP_CODE,
  ONE_DAY_MILLISECONDS,
  BUTTON_TITLE,
  BUTTON_TEXT,
  TITLE,
  TEXT,
  CHECK_BOX_TEXT,
  BOTTOM_TEXT,
  INPUT_PLACEHOLDER,
  NATIONAL_TITLE,
  NATIONAL_TEXT,
  SUBMIT,
  YOUR_NAME,
  YOUR_EMAIL,
  YOUR_PHONE,
  MESSAGE_ARTIST,
  UPLOAD_PHOTOS,
  SELECT_CHECKOUT,
  PLACE_ORDER_NOW,
  ICON_TITLE_NO_LOCAL,
  ICON_DESCRIPTION_NO_LOCAL,
  OR_CALL_ASSISCTENCE,
  PHONE,
  PHONE_NO_LOCAL,
  CHECK_AVAILABILITY_AREA,
  PICKUP_AVAILABLE_AREA,
  ORDER_NOW,
  NOT_AVAILABLE_LOCATION,
};
