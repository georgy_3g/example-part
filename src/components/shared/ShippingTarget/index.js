import React, { Component } from 'react';
import PropTypes from 'prop-types';
import bem from 'src/utils/bem';
import getZipCode from 'src/utils/getZipCode';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';
import Image from 'next/image';
import dynamic from 'next/dynamic';
import {
  localPickupIsAvailableSelector,
  storeUtmContentSelector,
  zipCodeCreationDateSelector,
  zipCodeIsCheckedSelector,
  zipCodeSelector,
} from 'src/redux/location/selectors';
import { billingInfoSelect, zipCodesSelector } from 'src/redux/shared/selectors';
import { getToken } from 'src/redux/auth/selectors';
import { changeUtmContentActions, changeZipCode } from 'src/redux/location/actions';
import {
  changeTemporaryUtmContentActions,
  getUserBillingInfo,
} from 'src/redux/shared/actions';
import styles from './index.module.scss';
import {
  AVAILABLE,
  BUTTON_LINK,
  CHECK_AVAILABILITY_AREA,
  CHECK_BOX_TEXT,
  ICON_DESCRIPTION_NO_LOCAL,
  ICON_TITLE_NO_LOCAL,
  INPUT_PLACEHOLDER,
  NOT_AVAILABLE,
  NOT_AVAILABLE_LOCATION,
  ONE_DAY_MILLISECONDS,
  OR_CALL_ASSISCTENCE,
  ORDER_NOW,
  PHONE,
  PHONE_NO_LOCAL,
  PICKUP_AVAILABLE_AREA,
  PLACE_ORDER_NOW,
  SELECT_CHECKOUT,
} from './constants';

const GeoTagIcon = dynamic(() => import('src/components/svg/GeoTagIcon'));
const CloseIcon2 = dynamic(() => import('src/components/svg/CloseIcon2'));
const BoxIcon = dynamic(() => import('src/components/svg/BoxIcon'));

const b = bem('shipping-target', styles);

const defaultProps = {
  className: '',
  zipCode: '',
  zipCodeCreationDate: '',
  changeZip: () => {},
  zipCodes: [],
  iconDescription: '',
  iconTitle: '',
  description: '',
  descriptionNoLocal: '',
  titleFull: '',
  titleNoFull: '',
  firstItem: {
    image: {},
    title: '',
    description: '',
    buttonText: '',
  },
  secondItem: {
    image: {},
    title: '',
    description: '',
    buttonText: '',
  },
  thirdItem: {
    image: {},
    title: '',
    description: '',
    buttonText: '',
  },
  utmContent: '',
  getBillingInfo: () => {},
  token: '',
  storeUtmContent: '',
  billingInfo: {},
  changeUtmContent: () => {},
  changeTemporaryUtmContent: () => {},
};

const propTypes = {
  className: PropTypes.string,
  zipCode: PropTypes.string,
  zipCodeCreationDate: PropTypes.instanceOf(Date),
  changeZip: PropTypes.func,
  zipCodes: PropTypes.arrayOf(PropTypes.shape({})),
  iconDescription: PropTypes.string,
  iconTitle: PropTypes.string,
  description: PropTypes.string,
  descriptionNoLocal: PropTypes.string,
  titleFull: PropTypes.string,
  titleNoFull: PropTypes.string,
  firstItem: PropTypes.shape({
    image: PropTypes.shape({}),
    imageNoLocal: PropTypes.shape({}),
    title: PropTypes.string,
    titleMobile: PropTypes.string,
    titleNoLocalMobile: PropTypes.string,
    description: PropTypes.string,
    buttonText: PropTypes.string,
    descriptionNoLocal: PropTypes.string,
    descriptionMobile: PropTypes.string,
    descriptionNoLocalMobile: PropTypes.string,
    titleNoLocal: PropTypes.string,
    buttonTextNoLocal: PropTypes.string,
    buttonTextNoLocalMobile: PropTypes.string,
    buttonLink: PropTypes.string,
  }),
  secondItem: PropTypes.shape({
    image: PropTypes.shape({}),
    imageNoLocal: PropTypes.shape({}),
    imageMobile: PropTypes.shape({}),
    imageNoLocalMobile: PropTypes.shape({}),
    title: PropTypes.string,
    titleNoLocalMobile: PropTypes.string,
    description: PropTypes.string,
    buttonText: PropTypes.string,
    descriptionNoLocal: PropTypes.string,
    descriptionMobile: PropTypes.string,
    descriptionNoLocalMobile: PropTypes.string,
    titleNoLocal: PropTypes.string,
    buttonTextNoLocal: PropTypes.string,
  }),
  thirdItem: PropTypes.shape({
    image: PropTypes.shape({}),
    imageNoLocal: PropTypes.shape({}),
    title: PropTypes.string,
    titleMobile: PropTypes.string,
    titleNoLocal: PropTypes.string,
    titleNoLocalMobile: PropTypes.string,
    description: PropTypes.string,
    descriptionNoLocal: PropTypes.string,
    descriptionMobile: PropTypes.string,
    descriptionNoLocalMobile: PropTypes.string,
    buttonText: PropTypes.string,
    buttonTextNoLocal: PropTypes.string,
    buttonLink: PropTypes.string,
  }),
  utmContent: PropTypes.string,
  getBillingInfo: PropTypes.func,
  token: PropTypes.string,
  storeUtmContent: PropTypes.string,
  billingInfo: PropTypes.shape({
    zipCode: PropTypes.string,
    state: PropTypes.string,
  }),
  changeUtmContent: PropTypes.func,
  changeTemporaryUtmContent: PropTypes.func,
};

class ShippingTarget extends Component {
  constructor(props) {
    super(props);
    const { utmContent, zipCode, storeUtmContent } = props;
    this.Geolocation = null;
    this.state = {
      isOpen: false,
      coordinate: null,
      isChecked: false,
      utmContent: storeUtmContent || utmContent,
      zip: zipCode || '',
      showMobileZipCode: false,
      isShowButton: true,
    };
  }

  componentDidMount() {
    const { zipCode, token, getBillingInfo, storeUtmContent, utmContent: utmContentProps, changeUtmContent } = this.props;
    const { utmContent } = this.state;
    this.Geolocation = navigator && navigator.geolocation;
    if (token && (!zipCode || !storeUtmContent)) {
      getBillingInfo();
    }
    if (this.Geolocation && !zipCode && !utmContent) {
      this.Geolocation.getCurrentPosition(
        (e) => { this.getCoordinates(e, !token); },
        this.onErrorGetPosition,
      );
    }
    if (utmContentProps) {
      changeUtmContent(storeUtmContent || utmContentProps);
    }
    if (zipCode) {
      changeZipCode(zipCode);
    }
  }

  componentDidUpdate(prevProps) {
    const { billingInfo, utmContent } = this.props;
    const { billingInfo: prevBillingInfo } = prevProps;
    if (
      billingInfo &&
      Object.keys(billingInfo).length &&
      (!prevBillingInfo || !Object.keys(prevBillingInfo).length) &&
      utmContent.toLowerCase() !== 'l'
    ) {
      this.checkBillingData();
    }
  }

  componentWillUnmount() {
    this.Geolocation = null;
  }

  checkBillingData = () => {
    const {
      billingInfo: { zipCode, state } = {},
    } = this.props;
    if (!zipCode && !state) {
      this.setLocationGeo();
    } else {
      this.setLocationBilling();
    }
  };

  setLocationGeo = () => {
    if (this.Geolocation && Object.keys(this.Geolocation).length) {
      this.Geolocation.getCurrentPosition((e) => {
        this.getCoordinates(e, true);
      });
    } else if(window.geoip2) {
     try {
        window.geoip2.city(this.getIpZipCode, (e) => console.log('error geoip2', e));
      }
      catch(e) {
        console.log('error geoip2', e);
      }
    }
  }

  setLocationBilling = () => {
    const {
      billingInfo: { zipCode, state } = {},
      storeUtmContent,
      zipCode: storeZipCode,
      zipCodes,
      changeZip,
      changeUtmContent,
    } = this.props;
    const { zip } = this.state;

    const zipValue = zipCode || zip || '';
    const isLocal = zipCodes.some(({ zip: itmZip }) => itmZip === zipValue);
    this.setState(
      {
        ...(!storeZipCode ? { zip: zipValue } : {}),
        ...(!storeUtmContent && zipValue
          ? { utmContent: isLocal || state.toLowerCase() === 'fl' ? 'l' : 'n' }
          : {}),
      },
      () => {
        if (!storeZipCode && zipValue) {
          changeZip(zipValue);
        }
        if (!storeUtmContent && (zipValue || state)) {
          changeUtmContent((zipValue && isLocal) || state.toLowerCase() === 'fl' ? 'l' : 'n');
        }
      },
    );
  }

  getNewZipCode = async () => {
    const { coordinate } = this.state;
    const { zipCodes, changeTemporaryUtmContent } = this.props;
    const code = await getZipCode(coordinate);
    if (code) {
      const newValue = zipCodes.some(({ zip }) => zip === code) ? 'l' : 'n';
      this.setState({ utmContent: newValue, zip: code }, () => {
        changeTemporaryUtmContent(newValue);
      });
    } else {
      this.setState({ zip: '' }, this.onErrorGetPosition);
    }
  };

  checkDate = (zipCodeDate) =>
    zipCodeDate
      ? new Date(zipCodeDate).valueOf() + ONE_DAY_MILLISECONDS < new Date().valueOf()
      : false;

  getCoordinates = ({ coords }, isAutoZip) => {
    const { latitude: lat, longitude: lng } = coords;
    this.setState({ coordinate: { lat, lng } }, () => {
      if (isAutoZip) {
        this.getNewZipCode();
      }
    });
  };

  onErrorGetPosition = () => {
    const { zipCode, token } = this.props;
    const { utmContent } = this.state;
    if (!zipCode && !utmContent && !token && window.geoip2) {
      try {
          window.geoip2.city(this.getIpZipCode, (e) => console.log('error geoip2', e));
        }
        catch(e) {
          console.log('error geoip2', e);
        }
    }
  }

  getIpZipCode = (data) => {
    const { zipCodes, changeTemporaryUtmContent } = this.props;
    const { zip: stateZip } = this.state;
    const { postal: { code } = {}, subdivisions: [{ names: { en: enName } = {} }] = [{}] } =
      data || {};
    if (code) {
      const newValue = zipCodes.some(({ zip }) => zip === code) ? 'l' : 'n';
      this.setState(
        {
          utmContent: newValue,
          ...(stateZip ? {} : {zip: code}),
        },
        () => {
          changeTemporaryUtmContent(newValue);
        },
      );
    }
    if (!code && enName) {
      this.setState(
        {
          utmContent: enName.toLowerCase() === 'florida' ? 'l' : 'n',
        },
        () => {
          changeTemporaryUtmContent(enName.toLowerCase() === 'florida');
        },
      );
    }
  };

  openPopup = () => {
    const { isOpen } = this.state;
    this.setState({ isOpen: !isOpen });
  };

  checkZipCode = () => {
    const { zipCodes } = this.props;
    const { zip: zipCode } = this.state;
    return zipCodes.some(({ zip }) => zip === zipCode);
  };

  changeZipCode = (e) => {
    const { value } = e.target;
    const { changeZip, changeUtmContent } = this.props;

    changeZip(value);

    const isLocalZipCode = this.checkZipCode();
    const utm = isLocalZipCode ? 'l' : 'n';

    this.setState(
      { zip: value, utmContent: value ? utm : '' },
      () => { changeUtmContent(value ? utm : ''); }
    );
  };

  onClickCheckBox = () => {
    const { zipCode, zipCodeCreationDate, changeZip } = this.props;
    const { isChecked, coordinate } = this.state;
    this.setState({ isChecked: !isChecked }, () => {
      if (coordinate && (!zipCode || this.checkDate(zipCodeCreationDate)) && !isChecked) {
        this.getNewZipCode();
      } else if (isChecked) {
        changeZip('');
      }
    });
  };

  closeButton = () => {
    this.setState({ isShowButton: false });
  };

  clickInputButton = () => {
    const digitizeWizard = document.getElementById('digitize-wizard');
    const whatWeDo = document.getElementById('what-we-do-id');

    if (digitizeWizard || whatWeDo) {
      const top = (digitizeWizard && digitizeWizard.offsetTop - 90) || (whatWeDo && whatWeDo.offsetTop - 90);
      this.openPopup();
      window.scrollTo({
        left: 0,
        top,
        behavior: 'smooth',
      });
    }
  }

  render() {
    const {
      className,
      firstItem,
      secondItem,
      thirdItem,
      iconTitle,
      iconDescription,
      titleFull,
      titleNoFull,
      description,
      descriptionNoLocal,
    } = this.props;

    const { isOpen, isChecked, coordinate, utmContent, zip, showMobileZipCode, isShowButton } = this.state;

    const isLocal = (utmContent || '').toLowerCase() === 'l';

    const {
      image: firstImage,
      imageNoLocal: firstImageNoLocal,
      title: firstTitle,
      titleNoLocal: firstTitleNoLocal,
      titleNoLocalMobile: firstTitleNoLocalMobile,
      titleMobile: firstTitleMobile,
      description: firstDescription,
      descriptionNoLocal: firstDescriptionNoLocal,
      descriptionMobile: firstDescriptionMobile,
      descriptionNoLocalMobile: firstDescriptionNoLocalMobile,
      buttonText: firstButtonText,
      buttonTextNoLocal: firstButtonTextNoLocal,
      buttonTextNoLocalMobile: firstButtonTextNoLocalMobile,
      buttonLink: firstButtonLink,
    } = firstItem;


    const {
      image: secondImage,
      imageNoLocal: secondImageNoLocal,
      imageMobile: secondImageMobile,
      imageNoLocalMobile: secondImageNoLocalMobile,
      title: secondTitle,
      titleNoLocal: secondTitleNoLocal,
      titleNoLocalMobile: secondTitleNoLocalMobile,
      description: secondDescription,
      descriptionNoLocal: secondDescriptionNoLocal,
      descriptionMobile: secondDescriptionMobile,
      descriptionNoLocalMobile: secondDescriptionNoLocalMobile,
      buttonText: secondButtonText,
      buttonTextNoLocal: secondButtonTextNoLocal,
    } = secondItem;

    const {
      image: thirdImage,
      imageNoLocal: thirdImageNoLocal,
      title: thirdTitle,
      titleMobile: thirdTitleMobile,
      titleNoLocal: thirdTitleNoLocal,
      titleNoLocalMobile: thirdTitleNoLocalMobile,
      description: thirdDescription,
      descriptionNoLocal: thirdDescriptionNoLocal,
      descriptionMobile: thirdDescriptionMobile,
      descriptionNoLocalMobile: thirdDescriptionMobileNoLocal,
      buttonText: thirdButtonText,
      buttonTextNoLocal: thirdButtonTextNoLocal,
    } = thirdItem;

    const isZipCode = zip && zip.length >= 5;
    const isLocalPickup = zip && this.checkZipCode();

    return (
      <div className={b({ mix: className })}>
        { isShowButton && (
        <div className={b('open-block-wrap')}>
          <button className={b('close-btn')} type="button" onClick={this.closeButton}>
            <CloseIcon2 />
          </button>
          <button
            className={b('open-block')}
            id="shipping-target-open-button"
            type="button"
            onClick={this.openPopup}
          >
            <div className={b('icon-block-wrap')}>
              <div className={b('icon-block')}>
                <div className={b('back-blue-element')} />
                <div className={b('back-green-element')} />
                { isLocal ? <GeoTagIcon className={b('icon-element')} /> : <BoxIcon className={b('icon-element')} />}
              </div>
            </div>
            <div className={b('text-block')}>
              <span className={b('button-title')}>{isLocal ? iconTitle : ICON_TITLE_NO_LOCAL}</span>
              <span className={b('button-text')}>{isLocal ? iconDescription : ICON_DESCRIPTION_NO_LOCAL}</span>
              <span className={b('button-link')}>{BUTTON_LINK}</span>
            </div>
          </button>
        </div>
        )}
        {isOpen &&
          ReactDOM.createPortal(
            <div className={b('content-wrap')}>
              <div className={b('veil')} role="button" tabIndex={-1} onClick={this.openPopup} />
              <div className={b('content')}>
                <button
                  className={b('close-btn', { popup: true })}
                  type="button"
                  onClick={this.openPopup}
                >
                  <CloseIcon2 />
                </button>
                <div className={b('content-block')}>
                  <div className={b('stripe-one')} />
                  <div className={b('stripe-two')} />
                  <div className={b('input-block')}>
                    <span className={b('input-title')}>
                      {isLocal ? titleFull : titleNoFull}
                    </span>
                    <span className={b('input-text')}>{isLocal ? description : descriptionNoLocal}</span>
                    <div className={b('input-block-btn')}>
                      <button
                        className={b('input-btn')}
                        type="button"
                        onClick={this.clickInputButton}
                      >
                        {PLACE_ORDER_NOW}
                      </button>
                      <span className={b('button-bottom-text')}>{SELECT_CHECKOUT}</span>
                      <span className={b('input-contact')}>{OR_CALL_ASSISCTENCE}</span>
                      <span className={b('input-contact-phone')}>{isLocal ? PHONE : PHONE_NO_LOCAL}</span>
                    </div>
                  </div>
                  <div className={b('card')}>
                    <div className={b('card-list')}>
                      <div
                        className={b('shipping-card-wrapper')}
                        style={{ order: isLocal ? 1 : 3 }}
                      >
                        <div className={b('shipping-card')}>
                          <span
                            className={b('card-title', { mobile: true })}
                          >
                            {isLocal ? firstTitleMobile : firstTitleNoLocalMobile}
                          </span>
                          <div className={b('card-content-block')}>
                            <div className={b('card-image')}>
                              <Image
                                className={b('card-image')}
                                src={isLocal ? firstImage : firstImageNoLocal}
                                alt="ship kit"
                                layout='fill'
                              />
                            </div>
                            <div className={b('card-image', {mobile: true})}>
                              <Image
                                className={b('card-image')}
                                src={isLocal ? firstImage : firstImageNoLocal}
                                alt="ship kit"
                                layout='fill'
                              />
                            </div>
                            <span
                              className={b('card-title')}
                            >
                              {isLocal ? firstTitle : firstTitleNoLocal}
                            </span>
                            <span
                              className={b('card-desc')}
                            >
                              {isLocal ? firstDescription : firstDescriptionNoLocal}
                            </span>
                            <div className={b('card-mobile')}>
                              <span
                                className={b('card-desc', {mobile: true})}
                              >
                                {isLocal ? firstDescriptionMobile : firstDescriptionNoLocalMobile}
                              </span>
                              <a className={b('card-desc', {link: true, mobile: true})} href={firstButtonLink}>{isLocal ? firstButtonText : firstButtonTextNoLocalMobile}</a>
                            </div>
                            <a className={b('card-desc', {link: true})} href={firstButtonLink}>{isLocal ? firstButtonText : firstButtonTextNoLocal}</a>
                            <div className={b('status-shipping')}>{AVAILABLE}</div>
                          </div>
                        </div>
                      </div>
                      <div
                        className={b('shipping-card-wrapper', {'no-local': !isLocal})}
                        style={{ order: 2 }}
                      >
                        <div className={b('shipping-card')}>
                          <span
                            className={b('card-title', { mobile: true })}
                          >
                            {isLocal ? secondTitle : secondTitleNoLocalMobile}
                          </span>
                          <div className={b('card-content-block')}>
                            <div className={b('card-image')}>
                              <Image
                                className={b('card-image', { 'no-available': isZipCode && !isLocalPickup})}
                                src={isLocal ? secondImage : secondImageNoLocal}
                                alt="ship kit"
                                layout='fill'
                              />
                            </div>
                            <div className={b('card-image', {mobile: true})}>
                              <Image
                                className={b('card-image')}
                                src={isLocal ? secondImageMobile : secondImageNoLocalMobile}
                                alt="ship kit"
                                layout='fill'
                              />
                            </div>
                            <span
                              className={b('card-title')}
                            >
                              {isLocal ? secondTitle : secondTitleNoLocal}
                            </span>
                            <span
                              className={b('card-desc')}
                            >
                              {isLocal ? secondDescription : secondDescriptionNoLocal}
                            </span>
                            <div className={b('card-mobile')}>
                              <span
                                className={b('card-desc', {mobile: true})}
                              >
                                {isLocal ? secondDescriptionMobile : secondDescriptionNoLocalMobile}
                              </span>
                              <button className={b('card-desc', {link: true, mobile: true})} onClick={() => {this.setState({showMobileZipCode: !showMobileZipCode})}} type="button">{isLocal ? secondButtonText : secondButtonTextNoLocal}</button>
                            </div>
                            <div className={b('zip-code-area')}>
                              <span className={b('zip-code-text')}>{CHECK_AVAILABILITY_AREA}</span>
                              <input
                                className={b('zip-code-input')}
                                type="text"
                                value={zip}
                                onChange={this.changeZipCode}
                                placeholder={INPUT_PLACEHOLDER}
                              />
                              {coordinate && !isZipCode && (
                                <div className={b('check-block')}>
                                  <button
                                    className={b('check-box', { checked: isChecked })}
                                    type="button"
                                    onClick={this.onClickCheckBox}
                                  />
                                  <span className={b('check-box-text')}>{CHECK_BOX_TEXT}</span>
                                </div>
                              )}
                              { isZipCode && <div className={b('status-shipping', { 'no-available':  !isLocalPickup})}>{isLocalPickup ? AVAILABLE : NOT_AVAILABLE}</div>}
                            </div>
                          </div>
                          <div className={b('zip-code-area', { mobile: true, show: !showMobileZipCode })}>
                            <span className={b('zip-code-text')}>{CHECK_AVAILABILITY_AREA}</span>
                            <input
                              className={b('zip-code-input')}
                              type="text"
                              value={zip}
                              onChange={this.changeZipCode}
                              placeholder={INPUT_PLACEHOLDER}
                            />
                            {coordinate && !isZipCode && (
                            <div className={b('check-block')}>
                              <button
                                className={b('check-box', { checked: isChecked })}
                                type="button"
                                onClick={this.onClickCheckBox}
                              />
                              <span className={b('check-box-text')}>{CHECK_BOX_TEXT}</span>
                            </div>
                            )}
                            { !isLocalPickup && isZipCode && <span className={b('zip-code-text-aviable')}>{NOT_AVAILABLE_LOCATION}</span>}
                            { isLocalPickup && <span className={b('zip-code-text-aviable')}>{PICKUP_AVAILABLE_AREA}</span>}
                            { isLocalPickup && <button className={b('zip-code-button-aviable')} type="button" onClick={this.clickInputButton}>{ORDER_NOW}</button>}
                          </div>
                        </div>
                      </div>
                      <div
                        className={b('shipping-card-wrapper')}
                        style={{ order: isLocal ? 3 : 1}}
                      >
                        <div className={b('shipping-card')}>
                          <span
                            className={b('card-title', { mobile: true })}
                          >
                            {isLocal ? thirdTitleMobile : thirdTitleNoLocalMobile}
                          </span>
                          <div className={b('card-content-block')}>
                            <div className={b('card-image')}>
                              <Image
                                className={b('card-image')}
                                src={isLocal ? thirdImage : thirdImageNoLocal}
                                alt="ship kit"
                                layout='fill'
                              />
                            </div>
                            <div className={b('card-image', {mobile: true})}>
                              <Image
                                className={b('card-image')}
                                src={isLocal ? thirdImage : thirdImageNoLocal}
                                alt="ship kit"
                                layout='fill'
                              />
                            </div>
                            <span className={b('card-title')}>{isLocal ? thirdTitle : thirdTitleNoLocal}</span>
                            <span className={b('card-desc')}>{isLocal ? thirdDescription : thirdDescriptionNoLocal}</span>

                            <div className={b('card-mobile')}>
                              <span className={b('card-desc', {mobile: true})}>{isLocal ? thirdDescriptionMobile : thirdDescriptionMobileNoLocal}</span>
                              <button className={b('card-desc', {link: true, mobile: true})} onClick={this.clickInputButton} type="button">{thirdButtonTextNoLocal}</button>
                            </div>
                            <button className={b('card-desc', {link: true})} onClick={this.clickInputButton} type="button">{thirdButtonText}</button>
                            <div className={b('status-shipping')}>{AVAILABLE}</div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className={b('input-block-btn', { mobile: true })}>
                      <span className={b('input-contact')}>{OR_CALL_ASSISCTENCE}</span>
                      <span className={b('input-contact-phone')}>{isLocal ? PHONE : PHONE_NO_LOCAL}</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>,
            document.getElementById('__next') || null,
          )}
      </div>
    );
  }
}

ShippingTarget.propTypes = propTypes;
ShippingTarget.defaultProps = defaultProps;

const stateProps = (state) => ({
  zipCode: zipCodeSelector(state),
  zipCodeCreationDate: zipCodeCreationDateSelector(state),
  zipCodeIsChecked: zipCodeIsCheckedSelector(state),
  localPickupIsAvailable: localPickupIsAvailableSelector(state),
  zipCodes: zipCodesSelector(state),
  billingInfo: billingInfoSelect(state),
  token: getToken(state),
  storeUtmContent: storeUtmContentSelector(state),
});

const actions = {
  changeZip: changeZipCode,
  getBillingInfo: getUserBillingInfo,
  changeUtmContent: changeUtmContentActions,
  changeTemporaryUtmContent: changeTemporaryUtmContentActions,
};

export default connect(stateProps, actions)(ShippingTarget);
