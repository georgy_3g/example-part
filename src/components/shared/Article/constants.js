const TITLE = 'Stories & thoughts';
const TITLE_DESC = 'Lorem ipsum dolor sit amet consectetur';
const LOAD_MORE = 'Loading more';
const READ_ARTICLE = 'Read article';
const LOAD_SCROLLING = 0.3;
const LIMIT = 3;
const BACK_TO_ALL = 'Back to all articles';
const BY = 'by:';
const CONTINUE = 'Continue reading';
const SEE_ALL = 'See all';
const LOADING = 'LOADING';
const FOLLOW_US = 'Follow Us';

const constants = {
  TITLE,
  TITLE_DESC,
  LOAD_MORE,
  READ_ARTICLE,
  LOAD_SCROLLING,
  LIMIT,
  BACK_TO_ALL,
  BY,
  CONTINUE,
  SEE_ALL,
  LOADING,
  FOLLOW_US,
};

export default constants;
