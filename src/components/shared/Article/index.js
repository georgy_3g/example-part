import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import formatUrl from 'src/utils/formatUrl';
import bem from 'src/utils/bem';
import { connect } from 'react-redux';
import ROUTES from 'src/constants/routes';
import { FACEBOOK_LINK, INSTAGRAM_LINK, YOUTUBE_LINK } from 'src/config';
import format from 'date-fns/format';
import Colors from 'src/styles/colors.json';
import Image from 'next/image';
import loadIcon from 'public/img/icon-png/loading-circle.png';
import dynamic from 'next/dynamic';
import { getArticle, getBlogList, isArticleLoading } from 'src/redux/blogList/selectors';
import { loadArticle } from 'src/redux/blogList/actions';
import constants from './constants';
import styles from './index.module.scss';

const ArrowIcon = dynamic(() => import('src/components/svg/ArrowIcon'));
const InstagramIcon = dynamic(() => import('src/components/svg/InstagramIcon'));
const FacebookIcon = dynamic(() => import('src/components/svg/FacebookIcon'));
const YouTubeIcon = dynamic(() => import('src/components/svg/YouTubeIcon'));
const {
  BY,
  CONTINUE,
  SEE_ALL,
  LOADING,
  FOLLOW_US,
  READ_ARTICLE,
} = constants;

const b = bem('article-content', styles);

const lightSlateGrayColor = Colors['$light-slate-gray-color'];

const { blog } = ROUTES;

const defaultProps = {
  article: {
    body: '',
    imageUri: '',
    author: '',
    title: '',
    category: '',
    id: 0,
    addedAt: '',
  },
  blogs: {
    data: [],
  },
  isLoading: true,
};

const propTypes = {
  article: PropTypes.shape({
    body: PropTypes.string,
    imageUri: PropTypes.string,
    author: PropTypes.string,
    title: PropTypes.string,
    category: PropTypes.string,
    id: PropTypes.number,
    addedAt: PropTypes.string,
  }),
  blogs: PropTypes.shape({
    data: PropTypes.arrayOf(PropTypes.any),
  }),
  isLoading: PropTypes.bool,
};


const Content = (props) => {
  const [href, setHref] = useState('');
  const {
    article: {
      imageUri,
      author,
      title,
      body,
      id,
      addedAt,
    },
    blogs,
    isLoading,
  } = props;

  useEffect(() => {
    setHref(window.location.origin);
  }, []);

  const sortedArticles = blogs.data.filter(item => (item.id !== id));

  return (
    <main className={b()}>
      <biv className={b('top-stripe')} />
      <biv className={b('top-rect')} />
      {
        isLoading ? (
          <div className={b('loading-wrapper')}>
            <Image
              className={b('load-icon')}
              src={loadIcon}
              width='100%'
              height='100%'
              alt="icon"
            />
            <span>{LOADING}</span>
          </div>
        ) : (
          <div className={b('body-wrapper')}>
            <p className={b('title')}>{title}</p>
            <img
              className={b('image')}
              src={imageUri}
              alt="article-bg"
            />
            <div className={b('body-container')}>
              <div
                className={b('body')}
                dangerouslySetInnerHTML={{ __html: `${body}` }}
              />
              <div className={b('author-wrapper')}>
                <span className={b('author')}>{`${BY} ${author}`}</span>
                <span className={b('article-date')}>{format(new Date(addedAt), 'MM.dd.yyyy')}</span>
              </div>
              <div className={b('social')}>
                <span className={b('follow-us')}>{FOLLOW_US}</span>
                <div className={b('social-icons')}>
                  <a className={b('icon-link')} href={FACEBOOK_LINK} target="_blank" rel="noreferrer">
                    <FacebookIcon className={b('social-svg')} fill={lightSlateGrayColor} />
                  </a>
                  <a className={b('icon-link')} href={INSTAGRAM_LINK} target="_blank" rel="noreferrer">
                    <InstagramIcon className={b('social-svg')} fill={lightSlateGrayColor} />
                  </a>
                  <a className={b('icon-link')} href={YOUTUBE_LINK} target="_blank" rel="noreferrer">
                    <YouTubeIcon className={b('social-svg')} fill={lightSlateGrayColor} />
                  </a>
                </div>
              </div>
            </div>
            <section className={b('articles')}>
              <biv className={b('bottom-stripe')} />
              <div className={b('articles-wrapper')}>
                <div className={b('articles-title-block')}>
                  <p className={b('articles-title')}>{CONTINUE}</p>
                  <a className={b('article-link')} href={blog}>
                    {SEE_ALL}
                    <ArrowIcon
                      width={9}
                      height={16}
                      className={b('next-arrow')}
                    />
                  </a>
                </div>
                <div className={b('continue-reading')}>
                  {
                    (sortedArticles && sortedArticles.length > 0)
                    && sortedArticles.slice(0, 3).map(({
                      id: itemId,
                      titleSeo,
                      previewImageUri,
                      category: itemCategory,
                      title: itemTitle,
                    }) => (
                      <div className={b('article')}>
                        <div className={b('article-image')}>
                          <Image
                            className={b('article-image')}
                            src={previewImageUri}
                            alt="article-bg"
                            layout="fill"
                          />
                        </div>
                        <div className={b('article-info')}>
                          <div className={b('text-wrapper')}>
                            <span className={b('article-category')}>{itemCategory}</span>
                            {title.length < 130
                              ? <span className={b('article-title')}>{itemTitle}</span>
                              : <span className={b('article-title')}>{`${itemTitle.slice(0, 120)} ...`}</span>}
                          </div>
                          <a
                            className={b('article-link')}
                            href={`${href}/blog/${formatUrl(titleSeo, true)}/`}
                            key={itemId}
                          >
                            <div className={b('btn')}>{READ_ARTICLE}</div>
                          </a>
                        </div>
                      </div>
                    ))
                  }
                </div>
              </div>
            </section>
          </div>
        )
      }
    </main>
  );
};

const stateProps = state => ({
  article: getArticle(state),
  blogs: getBlogList(state),
  isLoading: isArticleLoading(state),
});

const actions = {
  loadCurrentArticle: loadArticle,
};


Content.propTypes = propTypes;
Content.defaultProps = defaultProps;

export default connect(stateProps, actions)(Content);
