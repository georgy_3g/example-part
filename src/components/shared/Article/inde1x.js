import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { NextSeo } from 'next-seo';
import formatUrl from 'src/utils/formatUrl';
import axios from 'axios';
import URL from 'src/redux/urls';
import dynamic from 'next/dynamic';
import { loadArticle } from 'src/redux/blogList/actions';
import Content from './content';

const Navigation = dynamic(() => import('src/components/shared/Navigation'));
const Footer = dynamic(() => import('src/components/shared/Footer'));

const defaultProps = {
  article: {},
};

const propTypes = {
  article: PropTypes.shape({
    titleSeo: PropTypes.string,
    descriptionSeo: PropTypes.string,
    id: PropTypes.number,
    previewImageUri: PropTypes.string,
  }),
};

export default class Article extends Component {
  static async getInitialProps({ store, req, res }) {
    const { dispatch } = store;
    const { params: { title } } = req;

    if (title && title !== '404' && title !== '500') {
      const regexTitle = /[a-z]/g;
      const resultTitle = title.toLowerCase().match(regexTitle).join('');

      const article = await axios.get(`${URL.BLOG_LIST}/${resultTitle}`)
        .then(({ data: { data } }) => data)
        .catch(() => {
          if(res) {
            res.redirect('/404');
            res.end();
            return {};
          }
          return {};
        });
      dispatch(loadArticle(article));
      return {
        article,
      };
    }
    if (res) {
      res.redirect('/404');
      res.end();
      return {};
    }
    document.location.href = '/404';
    return {};
  }

  render() {
    const {
      article: {
        titleSeo,
        descriptionSeo,
      },
    } = this.props;

    const canonicalUrl = `https://*******.com/blog/${formatUrl(titleSeo, true)}/`;
    return (
      <>
        <NextSeo
          title={titleSeo || '*******'}
          description={descriptionSeo || '*******'}
          canonical={canonicalUrl}
          languageAlternates={[{
            hrefLang: 'en-us',
            href: canonicalUrl,
          }]}
        />
        <Navigation />
        <Content />
        <Footer blog={false} />
      </>
    );
  }
}

Article.propTypes = propTypes;
Article.defaultProps = defaultProps;
