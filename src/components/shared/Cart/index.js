import React, { Component } from 'react';
import bem from 'src/utils/bem';
import { SHIPPING_PRODUCTS } from 'src/constants';
import DICTIONARY from 'src/constants/dictionary';
import ROUTES from 'src/constants/routes';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Colors from 'src/styles/colors.json';
import dynamic from 'next/dynamic';
import ordersTypeSelect from 'src/redux/orders/selectors';
import { closeCartAction } from 'src/redux/shared/actions';
import styles from './index.module.scss';

const CartIcon2 = dynamic(() => import('src/components/svg/CartIcon2'));
const CloseIcon2 = dynamic(() => import('src/components/svg/CloseIcon2'));
const OrderTransferCard = dynamic(() => import('src/components/cards/OrderTransferCard'));

const cartColor = Colors['$summer-green-color'];
const { CONTINUE_SHOPPING, CHECKOUT_NOW } = DICTIONARY;
const { checkout } = ROUTES;

const titleText = 'Your Cart';

const b = bem('cart', styles);

const defaultProps = {
  close: () => false,
  orders: {},
};

const propTypes = {
  close: PropTypes.func,
  orders: PropTypes.objectOf(PropTypes.shape({})),
};

class Cart extends Component {
  constructor(props) {
    super(props);
    this.veil = React.createRef();
    this.state = {
      selectedItem: {},
    };
  }

  closeHandler = (e) => {
    const { close } = this.props;
    if (e.target === this.veil.current) {
      close();
    }
  };

  clickDelete = (item) => {
    this.setState({ selectedItem: item });
  };

  render() {
    const { close, orders } = this.props;
    const { selectedItem } = this.state;

    return (
      <div
        className={b('popup')}
        onClick={this.closeHandler}
        role="button"
        tabIndex="0"
        ref={this.veil}
      >
        <section className={b()}>
          <div className={b('container')}>
            <div className={b('head-orders-wrapper')}>
              <div className={b('head')}>
                <button className={b('close-btn')} type="button" onClick={close}>
                  <CloseIcon2 />
                </button>
                <div className={b('title')}>
                  <CartIcon2 stroke={cartColor} width="42px" height="36px" />
                  <p className={b('title-text')}>{titleText}</p>
                </div>
              </div>
              <div className={b('orders')}>
                {Object.entries(orders).map(
                  ([key, value]) =>
                    value.length > 0 &&
                    !SHIPPING_PRODUCTS.includes(key) && (
                      <OrderTransferCard
                        data={value}
                        key={key}
                        selectedItem={selectedItem}
                        clickDelete={this.clickDelete}
                      />
                    ),
                )}
              </div>
            </div>
            <div className={b('checkout')}>
              {Boolean(Object.values(orders).filter((item) => item.length).length) && (
                <a className={b('checkout-link')} href={checkout}>
                  <div className={b('btn')}>{CHECKOUT_NOW}</div>
                </a>
              )}
              <button className={b('cont-shopping')} type="button" onClick={close}>
                {CONTINUE_SHOPPING}
              </button>
            </div>
          </div>
        </section>
      </div>
    );
  }
}

Cart.propTypes = propTypes;
Cart.defaultProps = defaultProps;

const stateProps = (state) => ({
  orders: ordersTypeSelect(state),
});

const actions = {
  close: closeCartAction,
};

export default connect(stateProps, actions)(Cart);
