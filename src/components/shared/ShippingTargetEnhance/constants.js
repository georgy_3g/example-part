import * as Yup from 'yup';

const TOP_TEXT = 'Contactless local pickup service or 3-way home ship kits offered nationwide.';
const YOUR_ZIP_CODE = 'Your zip code:';
const GREAT_NEWS = 'Great news! \nWe have easy pickup &\n delivery options \nin your area';
const HOME_SHIP_KIT_AVAILABLE = 'The home ship kit\noptions is available at\nyour location.';
const SELECT_HOME_SHIP_KIT = 'Select the home ship kit\noption at checkout.';
const BOTH_OPTIONS_AVAILABLE = 'Both options are\navailable at your\nlocation.';
const SELECT_PREFERED_OPTION = 'Select your prefered option\nat checkout.';
const HOME_SHIP_KIT = 'Home ship kit';
const AVAILABLE = 'Available!';
const NOT_AVAILABLE = 'Not yet available.';
const ENTER_ZIP_CODE =
  'Enter your zip code\nto see which options\nare available at your\nlocation.';
const ONE_DAY_MILLISECONDS = 86400000;
const BUTTON_TITLE = 'Are you local to South Florida?';
const BUTTON_TEXT = 'Visit us or schedule a pickup.';
const BUTTON_LINK = 'Upload your photo';
const TITLE = 'Are you local to us\nin South Florida?';
const TEXT = 'Let’s see what pickup & delivery options are available in your area.';
const CHECK_BOX_TEXT = 'Use my current location';
const BOTTOM_TEXT = 'Select your preferred\noption at checkout';
const INPUT_PLACEHOLDER = 'Enter your zip code here';
const NATIONAL_TITLE = 'Simple & secure nationwide\ndelivery options.';
const NATIONAL_TEXT = 'Check the delivery options in your area.';
const SUBMIT = 'Submit';
const YOUR_NAME = 'Your name';
const YOUR_EMAIL = 'Your email';
const YOUR_PHONE = 'Your phone';
const MESSAGE_ARTIST = 'Message to the artist';
const SHOULD_BE_FILLED = 'Should be filled';
const UPLOAD_PHOTOS = 'Upload photos';
const SELECT_CHECKOUT = 'Select your preferred option at checkout.';
const UPLOAD_YOUR_PHOTOS = 'Upload your photos';
const POPUP_TITLE_MOBILE = ' Free photo\nevaluation by our lead\nrestoration artist.';
const POPUP_TITLE_SUBMIT = 'Thank you for your\nphoto submission!';
const POPUP_SUBTITLE_SUBMIT = 'Our lead restoration arist will review your\nphoto(s) right away and send you a free photo\nevaluation!'
const PLEASE_ALLOW_DAY = 'Please allow us up to 1 business day.';
const FEEL_FREE_ASSISCTENCE = 'Feel free to call us for assistance';
const OR_CALL_ASSISCTENCE = 'Or call us for assistance';
const CLOSE = 'Close';
const PHONE = '800-916-6076';
const NOTE_TITLE = 'Do you have specific \n instructions?';
const NOTE_DESCRIPTION =
  'Let us know if you have any notes for \nour artists regarding this photo.';
const NOTE_PLACEHOLDER = 'Please enter your notes here...';
const NOTE_BUTTON_TEXT = 'Submit Note';
const COLOR_BUTTON_CANCEL = 'No, thanks';
const MAX_NUMBER_10 = 'Should be up to 10 symbols';

const SCHEMA = Yup.object().shape({
  name: Yup.string()
  .required(SHOULD_BE_FILLED),
  email: Yup.string()
  .required(SHOULD_BE_FILLED),
  phone: Yup.string()
  .test('count', MAX_NUMBER_10, value => (
    value && String(value.replace(/[_|-]/g, '')).length === 10
  ))
  .required(SHOULD_BE_FILLED),
});

export {
  SCHEMA,
  TOP_TEXT,
  BUTTON_LINK,
  YOUR_ZIP_CODE,
  GREAT_NEWS,
  HOME_SHIP_KIT_AVAILABLE,
  SELECT_HOME_SHIP_KIT,
  BOTH_OPTIONS_AVAILABLE,
  SELECT_PREFERED_OPTION,
  HOME_SHIP_KIT,
  AVAILABLE,
  NOT_AVAILABLE,
  ENTER_ZIP_CODE,
  ONE_DAY_MILLISECONDS,
  BUTTON_TITLE,
  BUTTON_TEXT,
  TITLE,
  TEXT,
  CHECK_BOX_TEXT,
  BOTTOM_TEXT,
  INPUT_PLACEHOLDER,
  NATIONAL_TITLE,
  NATIONAL_TEXT,
  SUBMIT,
  YOUR_NAME,
  YOUR_EMAIL,
  YOUR_PHONE,
  MESSAGE_ARTIST,
  UPLOAD_PHOTOS,
  SELECT_CHECKOUT,
  UPLOAD_YOUR_PHOTOS,
  POPUP_TITLE_MOBILE,
  POPUP_TITLE_SUBMIT,
  POPUP_SUBTITLE_SUBMIT,
  FEEL_FREE_ASSISCTENCE,
  OR_CALL_ASSISCTENCE,
  CLOSE,
  PHONE,
  NOTE_TITLE,
  NOTE_DESCRIPTION,
  NOTE_PLACEHOLDER,
  NOTE_BUTTON_TEXT,
  COLOR_BUTTON_CANCEL,
  PLEASE_ALLOW_DAY,
};
