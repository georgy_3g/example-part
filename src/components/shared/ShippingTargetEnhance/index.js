/* eslint no-param-reassign: "error" */
import React, {Component} from 'react';
import PropTypes from 'prop-types';
import bem from 'src/utils/bem';
import getZipCode from 'src/utils/getZipCode';
import ReactDOM from 'react-dom';
import {connect} from 'react-redux';
import {Formik} from 'formik';
import Colors from 'src/styles/colors.json';
import Image from 'next/image';


import RestorationEvaluationButton from 'public/icons/restoration-evaluation-button.svg';
import {PHONE_MASK} from 'src/constants';
import IconPopup from 'public/img/banner-icons/enhance-w.png';
import dynamic from "next/dynamic";
import {storeUtmContentSelector, zipCodeSelector} from "src/redux/location/selectors";
import {billingInfoSelect, zipCodesSelector} from "src/redux/shared/selectors";
import {getToken} from "src/redux/auth/selectors";
import {orderImagesContactSelector} from "src/redux/enhance/selectors";
import {changeUtmContentActions, changeZipCode} from "src/redux/location/actions";
import {changeTemporaryUtmContentActions, getUserBillingInfo} from "src/redux/shared/actions";
import {setImagesContact, submitHelpContact} from "src/redux/enhance/actions";
import styles from './index.module.scss';
import {
  BUTTON_LINK,
  CLOSE,
  COLOR_BUTTON_CANCEL,
  FEEL_FREE_ASSISCTENCE,
  MESSAGE_ARTIST,
  NOTE_BUTTON_TEXT,
  NOTE_DESCRIPTION,
  NOTE_PLACEHOLDER,
  NOTE_TITLE,
  OR_CALL_ASSISCTENCE,
  PHONE,
  PLEASE_ALLOW_DAY,
  POPUP_SUBTITLE_SUBMIT,
  POPUP_TITLE_MOBILE,
  POPUP_TITLE_SUBMIT,
  SCHEMA,
  SUBMIT,
  UPLOAD_PHOTOS,
  UPLOAD_YOUR_PHOTOS,
  YOUR_EMAIL,
  YOUR_NAME,
  YOUR_PHONE,
} from './constants';

const CloseIcon2 = dynamic(() => import('src/components/svg/CloseIcon2'));
const ImageUploader = dynamic(() => import('src/components/widgets/ImageUploader'));
const NewCustomInput = dynamic(() => import('src/components/inputs/NewCustomInput'));
const CustomTextarea = dynamic(() => import('src/components/inputs/CustomTextarea'));
const ImageNoteIcon = dynamic(() => import('src/components/svg/ImageNoteIcon'));
const CheckFull = dynamic(() => import('src/components/svg/CheckFull'));
const ImageNotesPopup = dynamic(() => import('src/components/popups/ImageNotesPopup'));


const whiteColor = Colors['$white-color'];
const redColor = Colors['$burnt-sienna-color'];
const orangeColor = Colors['$burnt-sienna-red-color'];

const b = bem('shipping-target', styles);

const defaultProps = {
  className: '',
  iconDescription: '',
  iconTitle: '',
  descriptionNoLocal: '',
  titleFull: '',
  firstItem: {
    image: {},
    title: '',
    description: '',
    buttonText: '',
    withLink: false,
  },
  secondItem: {
    image: {},
    title: '',
    description: '',
    buttonText: '',
    withLink: false,
  },
  thirdItem: {
    image: {},
    title: '',
    description: '',
    buttonText: '',
    withLink: false,
  },
  billingInfo: {},
  submit: () => {},
  imagesContact: [],
  setContactImages: () => {},
  storeUtmContent: '',
  zipCode: '',
  zipCodes: '',
  changeUtmContent: () => {},
  changeZip: () => {},
  token: '',
  getBillingInfo: () => {},
  changeTemporaryUtmContent: '',
  targetType: '',
};

const propTypes = {
  className: PropTypes.string,
  iconDescription: PropTypes.string,
  iconTitle: PropTypes.string,
  descriptionNoLocal: PropTypes.string,
  titleFull: PropTypes.string,
  firstItem: PropTypes.shape({
    image: PropTypes.shape({}),
    title: PropTypes.string,
    description: PropTypes.string,
    buttonText: PropTypes.string,
    withLink: PropTypes.bool,
  }),
  secondItem: PropTypes.shape({
    image: PropTypes.shape({}),
    title: PropTypes.string,
    description: PropTypes.string,
    buttonText: PropTypes.string,
    withLink: PropTypes.bool,
  }),
  thirdItem: PropTypes.shape({
    image: PropTypes.shape({}),
    title: PropTypes.string,
    description: PropTypes.string,
    buttonText: PropTypes.string,
    withLink: PropTypes.bool,
  }),
  billingInfo: PropTypes.shape({
    zipCode: PropTypes.string,
    state: PropTypes.string,
  }),
  submit: PropTypes.func,
  imagesContact: PropTypes.arrayOf(PropTypes.shape({})),
  setContactImages: PropTypes.func,
  storeUtmContent: PropTypes.string,
  zipCode: PropTypes.string,
  zipCodes: PropTypes.arrayOf(PropTypes.shape({})),
  changeUtmContent: PropTypes.func,
  changeZip: PropTypes.func,
  token: PropTypes.string,
  getBillingInfo: PropTypes.func,
  changeTemporaryUtmContent: PropTypes.func,
  targetType: PropTypes.string,
};

class ShippingTargetEnhance extends Component {
  constructor(props) {
    super(props);
    this.Geolocation = null;
    this.refElement = null;

    this.state = {
      isOpen: false,
      isSubmit: false,
      showPopupNote: false,
      coordinate: null,
      isShowButton: true,
      noteData: {},
    };
  }

  componentDidMount() {
    const { zipCode, token, getBillingInfo, storeUtmContent } = this.props;
    const { utmContent } = this.state;
    this.Geolocation = navigator && navigator.geolocation;

    if (token && (!zipCode || !storeUtmContent)) {
      getBillingInfo();
    }
    if (this.Geolocation && Object.keys(this.Geolocation).length && !zipCode && !utmContent) {
      this.Geolocation.getCurrentPosition((e) => {
        this.getCoordinates(e, !token);
      });
    }
    if ((!this.Geolocation || !Object.keys(this.Geolocation).length)  && !zipCode && !utmContent && !token && window.geoip2) {
      try {
        window.geoip2.city(this.getIpZipCode, () => null);
      }
      catch(e) {
        console.log('error geoip2', e);
      }
    }
  }

  componentDidUpdate(prevProps) {
    const { billingInfo } = this.props;
    const { billingInfo: prevBillingInfo } = prevProps;
    if (
      billingInfo &&
      Object.keys(billingInfo).length &&
      (!prevBillingInfo || !Object.keys(prevBillingInfo).length)
    ) {
      this.checkBillingData();
    }
  }

  componentWillUnmount() {
    this.Geolocation = null;
    this.refElement = null;
  }

  getCoordinates = ({ coords }, isAutoZip) => {
    const { latitude: lat, longitude: lng } = coords;
    this.setState({ coordinate: { lat, lng } }, () => {
      if (isAutoZip) {
        this.getNewZipCode();
      }
    });
  };

  checkBillingData = () => {
    const {
      billingInfo: { zipCode, state } = {},
      storeUtmContent,
      zipCode: storeZipCode,
      zipCodes,
      changeZip,
      changeUtmContent,
    } = this.props;
    const { zip } = this.state;
    if (!zipCode && !state) {
      if (this.Geolocation && Object.keys(this.Geolocation).length) {
        this.Geolocation.getCurrentPosition((e) => {
          this.getCoordinates(e, true);
        });
      } else if(window.geoip2)  {
        try {
          window.geoip2.city(this.getIpZipCode, () => null);
        }
        catch(e) {
          console.log('error geoip2', e);
        }
      }
    } else {
      const zipValue = zipCode || zip || '';
      const isLocal = zipCodes.some(({ zip: itmZip }) => itmZip === zipValue);
      this.setState(
        {
          ...(!storeZipCode ? { zip: zipValue } : {}),
          ...(!storeUtmContent && zipValue
            ? { utmContent: isLocal || state.toLowerCase() === 'fl' ? 'l' : 'n' }
            : {}),
        },
        () => {
          if (!storeZipCode && zipCode) {
            changeZip(zipCode);
          }
          if (!storeUtmContent && (zipCode || state)) {
            changeUtmContent((zipCode && isLocal) || state.toLowerCase() === 'fl' ? 'l' : 'n');
          }
        },
      );
    }
  };

  getNewZipCode = async () => {
    const { coordinate } = this.state;
    const { zipCodes, changeTemporaryUtmContent } = this.props;
    const code = await getZipCode(coordinate);
    if (code) {
      const newValue = zipCodes.some(({ zip }) => zip === code) ? 'l' : 'n';
      this.setState({ utmContent: newValue, zip: code }, () => {
        changeTemporaryUtmContent(newValue);
      });
    } else {
      this.setState({ zip: '' });
    }
  };

  openPopup = () => {
    const { setContactImages } = this.props;
    const { isOpen } = this.state;
    this.setState({ isOpen: !isOpen, isSubmit: false });
    setContactImages([]);
  };

  onSubmit = (values) => {
    const { submit, imagesContact, targetType } = this.props;
    const { name, phone, email, message } = values;

    const imagesFormData = imagesContact.map(item => item.base64);
    const notes = imagesContact.map(item => item.note || '');

    submit({
      images: imagesFormData,
      name,
      phone,
      email,
      description: message,
      reason: targetType,
      notes,
    });

    this.setState({ isSubmit: true });
    this.scrollTo(this.refElement, 0, 150);
  }

  scrollTo = (element, to, duration) => {
    if (duration <= 0) return;
    const difference = to - element.scrollTop;
    const perTick = difference / duration * 10;

    setTimeout(() => {
        element.scrollTop += perTick;
        if (element.scrollTop === to) return;
        this.scrollTo(element, to, duration - 10);
    }, 10);
  }

  deleteImage = (index) => {
    const { imagesContact, setContactImages } = this.props;

    const newImagesContact = imagesContact.filter((item, indexImage) => index !== indexImage);
    setContactImages(newImagesContact);
  }

  changeNote = (data) => {
    const { imagesContact, setContactImages } = this.props;

    const { notes, orderImageId } = data;
    const newImagesContact = imagesContact.map((item, indexImage) => {
      if (indexImage === orderImageId) {
        return {
          ...item,
          note: notes,
        };
      }
      return item;
    });
    setContactImages(newImagesContact);
    this.changePopupNote(false);
  }

  changePopupNote = (show) => {
    this.setState({ showPopupNote: show });
  }

  closeButton = () => {
    this.setState({ isShowButton: false });
  };

  setImageForNote = (data) => {
    this.setState({ noteData: data }, () => { this.changePopupNote(true) });
  }

  render() {
    const {
      className,
      iconTitle,
      iconDescription,
      imagesContact,
      titleFull,
      descriptionNoLocal,
      targetType,
    } = this.props;

    const { isOpen, isSubmit, showPopupNote, isShowButton, noteData } = this.state;

    return (
      <div className={b({ mix: className })}>
        {isShowButton && (
          <div className={b('open-block-wrap')}>
            <button className={b('close-btn')} type="button" onClick={this.closeButton}>
              <CloseIcon2 />
            </button>
            <button
              className={b('open-block')}
              id="shipping-target-open-button"
              type="button"
              onClick={this.openPopup}
            >
              <div className={b('icon-block-wrap')}>
                <div className={b('icon-block')}>
                  <div className={b('back-blue-element')} />
                  <div className={b('back-green-element')} />
                  <div className={b('icon-popup')}>
                    {targetType === 'photo-restoration' ? (
                      <Image
                        className={b('icon-element')}
                        src={RestorationEvaluationButton.src}
                        alt="img"
                        layout="fill"
                      />
                    ) : (
                      <Image
                        className={b('icon-element')}
                        src={IconPopup}
                        alt="img"
                        layout="fill"
                      />
                    )}
                  </div>
                </div>
              </div>
              <div className={b('text-block')}>
                <span className={b('button-title')}>{iconTitle}</span>
                <span className={b('button-text')}>{iconDescription}</span>
                <span className={b('button-link')}>{BUTTON_LINK}</span>
              </div>
            </button>
          </div>
        )}
        {isOpen &&
          ReactDOM.createPortal(
            <div className={b('content-wrap')}>
              <div className={b('veil')} role="button" tabIndex={-1} onClick={this.openPopup} />
              <div className={b('content')}>
                <button
                  className={b('close-btn', { popup: true })}
                  type="button"
                  onClick={this.openPopup}
                >
                  <CloseIcon2 />
                </button>
                <div className={b('popup-block')}>
                  <div
                    className={b('content-block')}
                    ref={(e) => {
                      this.refElement = e;
                    }}
                    id="shipping-target-popup"
                  >
                    <div className={b('stripe-one')} />
                    <div className={b('stripe-two')} />
                    <div className={b('input-block')}>
                      <span className={b('input-title')}>
                        {isSubmit ? POPUP_TITLE_SUBMIT : titleFull}
                      </span>
                      <span className={b('input-title', { mobile: true })}>
                        {isSubmit ? POPUP_TITLE_SUBMIT : POPUP_TITLE_MOBILE}
                      </span>
                      <span className={b('input-text', { 'margin-bottom': isSubmit })}>
                        {isSubmit ? POPUP_SUBTITLE_SUBMIT : descriptionNoLocal}
                      </span>
                      {isSubmit && (
                        <span className={b('input-text', { 'margin-submit': isSubmit })}>
                          {PLEASE_ALLOW_DAY}
                        </span>
                      )}
                      {!isSubmit && (
                        <ImageUploader
                          className={b('upload')}
                          buttonClass={b('upload-btn')}
                          withoutText
                          withoutError
                          withoutBottomText
                          buttonText={UPLOAD_PHOTOS}
                          isContactImages
                        />
                      )}
                      {!isSubmit && (
                        <ImageUploader
                          className={b('upload', { mobile: true })}
                          buttonClass={b('upload-btn')}
                          withoutText
                          withoutError
                          withoutBottomText
                          buttonText={UPLOAD_YOUR_PHOTOS}
                          isContactImages
                        />
                      )}
                      <div className={b('input-contact')}>
                        {isSubmit ? FEEL_FREE_ASSISCTENCE : OR_CALL_ASSISCTENCE}
                      </div>
                      <div className={b('input-contact-phone')}>{PHONE}</div>
                    </div>
                    <div className={b('card')}>
                      <div className={b('enhance-block')}>
                        <div className={b('wizard')}>
                          {imagesContact.map((item, index) => (
                            <div className={b('image-wizard')}>
                              {!isSubmit && (
                                <button
                                  className={b('delete-button')}
                                  type="button"
                                  onClick={() => {
                                    this.deleteImage(index);
                                  }}
                                />
                              )}
                              <div className={b('image-wizard-block')}>
                                <img
                                  className={b('image-wizard-block-img')}
                                  src={item.base64}
                                  alt="img"
                                />
                              </div>
                              <div className={b('image-wizard-bottom')}>
                                <div className={b('image-wizard-menu')}>
                                  {!isSubmit && (
                                    <button
                                      className={b('note-button')}
                                      type="button"
                                      onClick={() => {
                                        this.setImageForNote({ ...item, id: index });
                                      }}
                                    >
                                      <ImageNoteIcon />
                                      {item.note && (
                                        <div className={b('check-icon-wrapper')}>
                                          <CheckFull
                                            className={b('check-icon')}
                                            stroke={redColor}
                                            stroke2={whiteColor}
                                            fill={redColor}
                                          />
                                        </div>
                                      )}
                                    </button>
                                  )}
                                </div>
                              </div>
                            </div>
                          ))}
                        </div>
                        <Formik
                          initialValues={{
                            name: '',
                            email: '',
                            phone: '',
                            message: '',
                          }}
                          validationSchema={SCHEMA}
                          onSubmit={(values) => {
                            this.onSubmit(values);
                          }}
                          render={({ values, setFieldValue, handleSubmit, errors }) => (
                            <form className={b('form')}>
                              <NewCustomInput
                                inputClass={b('input-form')}
                                isTouched={Boolean(errors.name)}
                                value={values.name}
                                onChange={(e) => {
                                  setFieldValue('name', e.target.value);
                                }}
                                placeholder={YOUR_NAME}
                                disabled={isSubmit}
                              />
                              <NewCustomInput
                                inputClass={b('input-form')}
                                isTouched={Boolean(errors.email)}
                                value={values.email}
                                onChange={(e) => {
                                  setFieldValue('email', e.target.value);
                                }}
                                placeholder={YOUR_EMAIL}
                                disabled={isSubmit}
                              />
                              <NewCustomInput
                                inputClass={b('input-form')}
                                isTouched={Boolean(errors.phone)}
                                value={values.phone}
                                onChange={(e) => {
                                  setFieldValue('phone', e.target.value);
                                }}
                                placeholder={YOUR_PHONE}
                                disabled={isSubmit}
                                withMask
                                mask={PHONE_MASK}
                              />
                              <CustomTextarea
                                className={b('input-area-form')}
                                inputClass={b('text-area-input')}
                                isTouched={Boolean(errors.message)}
                                value={values.message}
                                onChange={(e) => {
                                  setFieldValue('message', e.target.value);
                                }}
                                placeholder={MESSAGE_ARTIST}
                                disabled={isSubmit}
                              />
                              {!isSubmit && (
                                <div className={b('submit-form-block')}>
                                  <button
                                    className={b('submit-form')}
                                    type="button"
                                    onClick={isSubmit ? this.openPopup : handleSubmit}
                                  >
                                    {isSubmit ? CLOSE : SUBMIT}
                                  </button>
                                </div>
                              )}
                              {isSubmit && (
                                <div className={b('submit-form-block', { mobile: true })}>
                                  <button
                                    className={b('submit-form')}
                                    type="button"
                                    onClick={isSubmit ? this.openPopup : handleSubmit}
                                  >
                                    {CLOSE}
                                  </button>
                                </div>
                              )}
                            </form>
                          )}
                        />
                      </div>
                      <div className={b('input-contact', { mobile: true })}>
                        {isSubmit ? FEEL_FREE_ASSISCTENCE : OR_CALL_ASSISCTENCE}
                      </div>
                      <div className={b('input-contact-phone', { mobile: true })}>{PHONE}</div>
                    </div>
                  </div>
                </div>
              </div>
            </div>,
            document.getElementById('__next') || null,
          )}
        {showPopupNote && (
          <ImageNotesPopup
            portalId="shipping-target-popup"
            close={() => this.changePopupNote(false)}
            title={NOTE_TITLE}
            description={NOTE_DESCRIPTION}
            placeholder={NOTE_PLACEHOLDER}
            buttonText={NOTE_BUTTON_TEXT}
            cancelText={COLOR_BUTTON_CANCEL}
            submit={this.changeNote}
            image={noteData}
            iconColor={orangeColor}
            buttonBackgroundColor={orangeColor}
          />
        )}
      </div>
    );
  }
}

ShippingTargetEnhance.propTypes = propTypes;
ShippingTargetEnhance.defaultProps = defaultProps;

const stateProps = (state) => ({
  zipCode: zipCodeSelector(state),
  zipCodes: zipCodesSelector(state),
  billingInfo: billingInfoSelect(state),
  token: getToken(state),
  storeUtmContent: storeUtmContentSelector(state),
  imagesContact: orderImagesContactSelector(state),
});

const actions = {
  changeZip: changeZipCode,
  getBillingInfo: getUserBillingInfo,
  changeUtmContent: changeUtmContentActions,
  changeTemporaryUtmContent: changeTemporaryUtmContentActions,
  submit: submitHelpContact,
  setContactImages: setImagesContact,
};

export default connect(stateProps, actions)(ShippingTargetEnhance);
