import React from 'react';
import bem from 'src/utils/bem';
import PropTypes from 'prop-types';
import LazyLoad from 'react-lazyload';
import Image from 'next/image';
import colors from 'src/styles/colors.json';
import dynamic from 'next/dynamic';
import styles from './index.module.scss';

const CheckFullMemoryLane = dynamic(() => import('src/components/svg/CheckFullMemoryLane'));
const ColorButton = dynamic(() => import('src/components/buttons/ColorButton'));
const MEMORY_LANE_TITLE = 'Real artists, really amazing photo restorations';

const orangeColor = colors['$burnt-sienna-color'];
const b = bem('memories', styles);

const propTypes = {
  titleText: PropTypes.string,
  bullets: PropTypes.arrayOf(PropTypes.string),
  imgUrl: PropTypes.oneOfType([PropTypes.string, PropTypes.shape({})]),
  imgAlt: PropTypes.string,
  imgSquare: PropTypes.oneOfType([PropTypes.string, PropTypes.shape({})]),
  isShowRightImage: PropTypes.bool,
  descriptionText: PropTypes.string,
  isWithoutBullets: PropTypes.bool,
  title: PropTypes.string,
  description: PropTypes.string,
  scroll: PropTypes.func,
  scrollButtonText: PropTypes.string,
  isFirst: PropTypes.bool,
  customClass: PropTypes.string,
};

const defaultProps = {
  titleText: MEMORY_LANE_TITLE,
  bullets: [],
  imgUrl: '',
  imgAlt: '******* image',
  imgSquare: '',
  isShowRightImage: true,
  descriptionText: '',
  isWithoutBullets: false,
  isFirst: false,
  title: '',
  description: '',
  scroll: () => {},
  scrollButtonText: '',
  customClass: '',
};

function MemoryLane({
  titleText,
  bullets,
  imgUrl,
  imgAlt,
  imgSquare,
  isShowRightImage,
  descriptionText,
  isWithoutBullets,
  title,
  description,
  scroll,
  scrollButtonText,
  isFirst,
  customClass,
}) {
  return (
    <div className={b()}>
      <div className={b('wrapper', { mix: customClass })}>
        <LazyLoad className={b('lazy-load')} offset={100}>
          {
           isWithoutBullets
             ? null
             : (
               <div className={b('text-wrapper', { isWithoutBullets })}>
                 <h2 className={b('title')}>{titleText}</h2>
                 {descriptionText && (
                   <div className={b('description')}>{descriptionText}</div>
                 )}
               </div>
             )
         }
        </LazyLoad>
        <div className={b('container', { isWithoutBullets, first: isFirst })}>
          {
          isWithoutBullets ?
            (
              <div className={b('bullets-wrapper', { isWithoutBullets, first: isFirst })}>
                <div className={b('bullets-stripe', { isWithoutBullets })} />
                <h2 className={b('title', { isWithoutBullets })}>{title}</h2>
                <div className={b('description', { isWithoutBullets })}>{description}</div>
                <ColorButton
                  className={b('scroll-button', { isWithoutBullets, desktop: true })}
                  onClick={scroll}
                  text={scrollButtonText}
                  backGroundColor={orangeColor}
                />
              </div>
            )
            : (
              <div className={b('bullets-wrapper')}>
                <div className={b('bullets-stripe')} />
                <div className={b('bullets-background')} />
                {
                  bullets.map(item => (
                    <div className={b('bullet')} key={item}>
                      <CheckFullMemoryLane
                        width={46}
                        height={46}
                      />
                      <div className={b('bullet-text')}>{item}</div>
                    </div>
                  ))
                }
              </div>
            )
        }
          <div className={b('image-wrapper', {'small-margin': isFirst})}>
            <div className={b('image', { isWithoutBullets })}>
              <LazyLoad offset={100}>
                <div className={b('image-url')}>
                  <Image
                    className={b('image-url')}
                    src={imgUrl}
                    alt={imgAlt}
                    loading="lazy"
                    layout="fill"
                  />
                </div>
              </LazyLoad>
            </div>
            <div className={b('image-left-square')} />
            {
              (isShowRightImage && imgSquare)
              && (
              <div className={b('image-right-square')}>
                <LazyLoad offset={100}>
                  <Image
                    className={b('image-square')}
                    src={imgSquare}
                    alt="square"
                    loading="lazy"
                    width='100%'
                    height='100%'
                  />
                </LazyLoad>
              </div>
              )
            }
            <div className={b('image-top-square')} />
            <div className={b('image-top-stripe', { isWithoutBullets })} />
          </div>
          {isWithoutBullets && (
          <ColorButton
            className={b('scroll-button', { isWithoutBullets, mobile: true, 'margin-bottom': isFirst })}
            onClick={scroll}
            text={scrollButtonText}
            backGroundColor={orangeColor}
          />
        )}
        </div>
      </div>
      {
      !isWithoutBullets &&  <div className={b('image-background-top-stripe')} />
    }
    </div>
  );
}

MemoryLane.propTypes = propTypes;
MemoryLane.defaultProps = defaultProps;

export default MemoryLane;
