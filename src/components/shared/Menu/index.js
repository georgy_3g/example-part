import React, { Component } from 'react';
import PropTypes from 'prop-types';
import generateCreateTab from 'src/utils/generateCreateTab';
import bem from 'src/utils/bem';
import Colors from 'src/styles/colors.json';
import { CHECK_WHITESPACE, HOMEPAGE } from 'src/constants';
import { FACEBOOK_LINK, INSTAGRAM_LINK, YOUTUBE_LINK } from 'src/config';
import liveHelpIcon from 'public/img/live-help/live-help-icon.png';
import dynamic from 'next/dynamic';
import { CALL_US, NUMBER } from '../Navigation/constants';
import styles from './index.module.scss';

const DropdownIcon = dynamic(() => import('src/components/svg/DropdownIcon'));
const InstagramIcon = dynamic(() => import('src/components/svg/InstagramIcon'));
const FacebookIcon = dynamic(() => import('src/components/svg/FacebookIcon'));
const YouTubeIcon = dynamic(() => import('src/components/svg/YouTubeIcon'));


const b = bem('menu', styles);
const colorGrey = Colors['$gray-color'];
const colorOrange = Colors['$burnt-sienna-color'];
const iconsColor = Colors['$pickled-bluewood-color'];

const liveHelp = 'Live help';


const defaultProps = {
  active: '',
  frameMaterials: [],
  closeMenu: () => {  },
  withImages: true,

};

const propTypes = {
  closeMenu: PropTypes.func,
  active: PropTypes.string,
  frameMaterials: PropTypes.arrayOf(PropTypes.shape({})),
  withImages: PropTypes.bool,
  defaultButtons: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  defaultTabs: PropTypes.shape({
    create: PropTypes.shape({}),
  }).isRequired,
};

class Menu extends Component {
  constructor(props) {
    super(props);

    const { frameMaterials, defaultTabs } = props;

    const tabs = generateCreateTab(frameMaterials);

    this.createTabData = tabs.links && tabs.links.length ? tabs : defaultTabs.create;

    this.state = {
      navTab: '',
    };
  }

  selectTab = tabId => () => {
    const { navTab } = this.state;
    this.setState({
      navTab: (navTab === tabId) ? '' : tabId,
    });
  }

  compareNavtab = (navTab, name) => navTab.toLowerCase() === name.toLowerCase().replace(CHECK_WHITESPACE, '')

  generateTab = ({
    link,
    name,
    buttonText,
    links,
  }) => (
    <div className={b('tab')}>
      <div className={b('tab-container', { big: true })}>
        <div className={b('links-block')}>
          {
            links.map(({ text: listLinkText, link: listLink }) => (
              <a
                className={b('list-link')}
                href={listLink}
                key={`${name}${listLinkText}`}
              >
                <span
                  className={b('list-link-text', { cloud: listLinkText.toLowerCase() === 'cloud' })}
                >
                  {listLinkText}
                </span>
                <DropdownIcon
                  stroke={colorGrey}
                  viewBox="0 0 22 22"
                  strokeWidth="4"
                  className={b('caret-right')}
                />
              </a>
            ))
          }
        </div>
        <a className={b('btn-link')} href={link}>
          <div className={b('btn')} role="button" tabIndex={0}>{buttonText}</div>
        </a>
      </div>
    </div>
  )

  render() {
    const {
      active,
      closeMenu,
      withImages,
      defaultButtons,
      defaultTabs,
    } = this.props;

    const { navTab } = this.state;

    return (
      <div className={b()}>
        <div className={b('navi-buttons')}>
          {defaultButtons.map(({ id, name, link }) => (
            <div className={b('navi-container')} key={id}>
              <div className={b('navi-dropdown')}>
                <a className={b('navi-button', { active: active === name })} href={link}>
                  {name}
                </a>
                <button
                  className={b('button')}
                  type="button"
                  onClick={name !== HOMEPAGE ? this.selectTab(id) : null}
                >
                  <DropdownIcon
                    stroke={this.compareNavtab(navTab, id) ? colorOrange : colorGrey}
                    viewBox="0 0 22 22"
                    strokeWidth="4"
                    menuIsOpen={this.compareNavtab(navTab, id)}
                    className={name === HOMEPAGE ? b('carret', { right: true }) : ''}
                  />
                </button>
              </div>
              {navTab &&
                this.compareNavtab(navTab, id) &&
                this.generateTab(
                  navTab !== 'create' ? defaultTabs[navTab] : this.createTabData,
                  withImages,
                )}
            </div>
          ))}
          <div className={b('live-help')}>
            <div role="button" tabIndex={0} className={b('live-help-btn')} onClick={closeMenu}>
              <div className={b('live-help-btn-icon')}>
                <img
                  className={b('icon-live-help')}
                  src={liveHelpIcon.src}
                  alt="icon-live-help"
                />
              </div>
              <span className={b('live-help-btn-title')}>{liveHelp}</span>
            </div>
          </div>
          <div className={b('call-us-info')}>
            <div className={b('call-us-info-container')}>
              <div className={b('call-us-title')}>{CALL_US}</div>
              <div className={b('call-us-text')}>{NUMBER}</div>
            </div>
          </div>
        </div>
        <div className={b('social-icons')}>
          <a className={b('social-link')} href={FACEBOOK_LINK} target="_blank" rel="noreferrer">
            <FacebookIcon fill={iconsColor} className={b('social-svg')} />
          </a>
          <a className={b('social-link')} href={INSTAGRAM_LINK} target="_blank" rel="noreferrer">
            <InstagramIcon fill={iconsColor} className={b('social-svg')} />
          </a>
          <a className={b('social-link')} href={YOUTUBE_LINK} target="_blank" rel="noreferrer">
            <YouTubeIcon fill={iconsColor} className={b('social-svg')} />
          </a>
        </div>
      </div>
    );
  }
}

Menu.propTypes = propTypes;
Menu.defaultProps = defaultProps;

export default Menu;
