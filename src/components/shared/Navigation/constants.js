import ROUTES from 'src/constants/routes';

const {
  accountDetails,
  accountProofs,
  accountCloud,
  accountTrack,
} = ROUTES;
const LOGO_TEXT = '*******';
const LOGIN_TEXT = 'Login';
const LIVE_HELP = 'Live help';
const GIFT_CARDS = 'Gift cards';
const NUMBER = '561-235-7808';
const CALL_US = 'Call us: ';
const WEEKDAYS_TIME_START = 9;
const WEEKDAYS_TIME_END = 18;
const WORKING_HOLIDAY = 6;
const HOLIDAY_TIME_START = 10;
const HOLIDAY_TIME_END = 16;
const CUSTOMER_REVIEWS = 'Customer Reviews';
const ACCOUNT = 'Account';
const ACCOUNT_LINES = [
  {
    text: 'Orders',
    link: accountDetails,
  },
  {
    text: 'Proofs',
    link: accountProofs,
  },
  {
    text: 'Cloud',
    link: accountCloud,
  },
  {
    text: 'Track',
    link: accountTrack,
  },
]

export {
  LOGO_TEXT,
  LOGIN_TEXT,
  LIVE_HELP,
  GIFT_CARDS,
  NUMBER,
  CALL_US,
  WORKING_HOLIDAY,
  WEEKDAYS_TIME_START,
  WEEKDAYS_TIME_END,
  HOLIDAY_TIME_START,
  HOLIDAY_TIME_END,
  CUSTOMER_REVIEWS,
  ACCOUNT,
  ACCOUNT_LINES,
};
