import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import bem from 'src/utils/bem';
import generateCreateTab from 'src/utils/generateCreateTab';
import deleteStringFirstElement from 'src/utils/deleteStringFirstElement';
import checkDate from 'src/utils/checkTime';
import ordersTypeSelect from 'src/redux/orders/selectors';
import { isOpenCartSelector, loginStatus, popupNameSelector } from 'src/redux/shared/selectors';
import {
  appVersionSelector,
  chatTokenSelector,
  getToken,
  isOpenLoginPopupSelector,
  withoutRedirectSelector,
} from 'src/redux/auth/selectors';
import { frameMaterialsSelector } from 'src/redux/photoArt/selectors';
import { promoSectionSelector } from 'src/redux/promo/selectors';
import { reviewsCountSelect, reviewsRateSelect } from 'src/redux/reviewers/selectors';
import { updateCart } from 'src/redux/orders/actions';
import {
  clearAfterAppVersionChangeAction,
  clearStoragesAction,
  closeLoginPopupAction,
  openLoginPopupAction,
} from 'src/redux/auth/actions';
import { deletePopupData } from 'src/redux/shared/actions';
import { getPromoSection } from 'src/redux/promo/actions';
import { updateStorageImages } from 'src/redux/orderImages/actions';
import { loadNewReviewers } from 'src/redux/reviewers/actions';
import { getFrameMaterialAction } from 'src/redux/photoArt/actions';
import { NEXT_PUBLIC_APP_VERSION } from 'src/config';
import { SHIPPING_PRODUCTS } from 'src/constants';
import { BUTTONS, TABS } from 'src/constants/tabs';
import ROUTES from 'src/constants/routes';
import Colors from 'src/styles/colors.json';
import dynamic from 'next/dynamic';
import {
  ACCOUNT,
  ACCOUNT_LINES,
  CALL_US,
  CUSTOMER_REVIEWS,
  HOLIDAY_TIME_END,
  HOLIDAY_TIME_START,
  LIVE_HELP,
  LOGIN_TEXT,
  NUMBER,
  WEEKDAYS_TIME_END,
  WEEKDAYS_TIME_START,
  WORKING_HOLIDAY,
} from './constants';

import styles from './index.module.scss';

const CrossCloseMenuIcon = dynamic(() => import('src/components/svg/CrossCloseMenuIcon'));
const MenuCard = dynamic(() => import('src/components/cards/MenuCard'));
const LogoMemoya = dynamic(() => import('src/components/svg/LogoMemoya'));
const ColorButton = dynamic(() => import('src/components/buttons/ColorButton'));
const EmailSentForm = dynamic(() => import('src/components/forms/EmailSentForm'));
const LiveHelp = dynamic(() => import('src/components/elements/LiveHelp'));
const MessageWithDotsIcon = dynamic(() => import('src/components/svg/MessageWithDotsIcon'));
const Rate = dynamic(() => import('src/components/elements/Rate'));
const ReviewsBanner = dynamic(() => import('src/components/shared/ReviewsBanner'));
const BurgerMenuIcon = dynamic(() => import('src/components/svg/BurgerMenuIcon'));
const CartIcon2 = dynamic(() => import('src/components/svg/CartIcon2'));
const LoginIcon2 = dynamic(() => import('src/components/svg/LoginIcon2'));
const AccountIcon = dynamic(() => import('src/components/svg/AccountIcon'));
const CloudIcon = dynamic(() => import('src/components/svg/CloudIcon'));
const OrdersIcon = dynamic(() => import('src/components/svg/OrdersIcon'));
const ProofIcon = dynamic(() => import('src/components/svg/ProofIcon'));
const CheckFull = dynamic(() => import('src/components/svg/CheckFull'));
const ForgotPassForm = dynamic(() => import('src/components/forms/ForgotPassForm'));
const ModalContainerNavigation = dynamic(() => import('src/components/modals/ModalContainerNavigation'));
const ModalContainer = dynamic(() => import('src/components/modals/ModalContainer'));
const SignUpForm = dynamic(() => import('src/components/forms/SignUpForm'));
const LoginForm = dynamic(() => import('src/components/forms/LoginForm'));
const UniversalSlider = dynamic(() => import('src/components/widgets/UniversalSlider'));
 const PromoBanner = dynamic(() => import('src/components/shared/PromoBanner'));
const Menu = dynamic(() => import('src/components/shared/Menu'));

const iconColor = Colors['$pickled-bluewood-color'];
const firstStringColor = Colors['$pickled-bluewood-color'];
const secondStringColor = Colors['$pickled-bluewood-color'];

const b = bem('navigation', styles);

const POPUP_NAME = {
  login: 'login',
  signUp: 'signUp',
  forgotPass: 'forgotPass',
  emailSent: 'emailSent',
  liveHelp: 'liveHelp',
  menu: 'menu',
};

const accountIcons = {
  cloud: <CloudIcon height={24} width={24} />,
  orders: <OrdersIcon height={24} width={24} />,
  proofs: <ProofIcon height={24} width={24} />,
  track: <AccountIcon height={24} width={24} />,
}

const defaultProps = {
  orders: {},
  active: '',
  token: null,
  isOpenLogin: false,
  openLoginPopup: () => {},
  closeLoginPopup: () => {},
  deletePopupName: () => {},
  newPopupName: '',
  frameMaterials: [],
  getPromo: () => {},
  promoSection: {},
   updateImages: () => {},
  reviewsCount: 1380,
  reviewsRate: 5,
   loadReviews: () => {},
   loadFrameMaterials: () => {},
   clearStorages: () => {},
   clearAfterAppVersionChange: () => {},
   appVersion: 0,
};

const propTypes = {
  orders: PropTypes.shape({}),
  active: PropTypes.string,
  token: PropTypes.string,
   updateLocalCart: PropTypes.func.isRequired,
  isOpenLogin: PropTypes.bool,
  openLoginPopup: PropTypes.func,
  closeLoginPopup: PropTypes.func,
  deletePopupName: PropTypes.func,
  newPopupName: PropTypes.string,
  frameMaterials: PropTypes.arrayOf(PropTypes.shape({})),
  getPromo: PropTypes.func,
  promoSection: PropTypes.shape({ id: PropTypes.number }),
   updateImages: PropTypes.func,
  reviewsCount: PropTypes.number,
  reviewsRate: PropTypes.number,
   loadReviews: PropTypes.func,
   loadFrameMaterials: PropTypes.func,
   clearStorages: PropTypes.func,
   clearAfterAppVersionChange: PropTypes.func,
   appVersion: PropTypes.oneOfType([PropTypes.number, PropTypes.string])
};

class Navigation extends Component {
  constructor(props) {
    super(props);
    const { frameMaterials } = props;
    const tabs = generateCreateTab(frameMaterials);
    this.block = null;
    this.oneRender = true;
    this.state = {
      navTab: '',
      popupIsOpen: false,
      isOpenMenu: false,
      popupName: '',
      isReviewsPopupOpened: false,
      limit: 6,
      offset: 0,
      order: 'desc',
      scrollY: 0,
      isShowAccountPopup: false,
      createTabData: tabs.links && tabs.links.length ? tabs : TABS.create
    };
  }

  componentDidMount() {
    const { limit, offset, order } = this.state;
    const {
      updateLocalCart,
      getPromo,
      updateImages,
      loadReviews,
      loadFrameMaterials,
      clearStorages,
      clearAfterAppVersionChange,
      appVersion,
    } = this.props;

    if (appVersion !== NEXT_PUBLIC_APP_VERSION) {
      clearAfterAppVersionChange();
      clearStorages(NEXT_PUBLIC_APP_VERSION);
    }
    if (updateLocalCart) {
      updateLocalCart();
    }
    if (updateImages) {
      updateImages();
    }
    const key = deleteStringFirstElement(document.location.pathname);
    const newUrl = key.length > 1 ? key.slice(0, -1) : '/';
    const nestingUrl = newUrl.split('/');
    if (newUrl && newUrl !== 'account' && newUrl !== 'photo-scanning-service' && !nestingUrl.includes('blog')) {
      getPromo({ key: newUrl });
    }
    this.windowWidth = window.innerWidth;
    loadReviews({
      limit: window.innerWidth <= 480 ? 10 : limit,
      order,
      offset,
    });
    window.addEventListener('scroll', this.hiddenElementsNavigation);
    loadFrameMaterials();
  }

  // eslint-disable-next-line
  componentWillReceiveProps(nextProps) {
    const { newPopupName, deletePopupName } = nextProps;
    if (newPopupName) {
      this.setState({ popupIsOpen: true, popupName: newPopupName }, deletePopupName);
    }
  }

  componentDidUpdate(prevProps) {
    const { frameMaterials } = this.props;
    const { frameMaterials: prevPropsFrameMaterials } = prevProps;
    if (!prevPropsFrameMaterials.length && frameMaterials && frameMaterials.length) {
      const tabs = generateCreateTab(frameMaterials);
      this.updateTabs(tabs);
    }
  }

  componentWillUnmount() {
    this.block = null;
    window.removeEventListener('scroll', this.hiddenElementsNavigation);
    this.windowWidth = null;
  }

  updateTabs = (tabs) => {
    this.setState({
      createTabData: tabs.links && tabs.links.length ? tabs : TABS.create,
    });
  };

  closeTab = () => () => {
    this.setState({ navTab: '' });
  };

  saveRef = (node) => {
    this.block = node;
  };

  selectTab = (tabId) => () => {
    this.setState({
      navTab: tabId,
      popupIsOpen: false,
      popupName: '',
    });
  };

  hiddenElementsNavigation = () => {
    const { body } = document;
    const element = document.documentElement;
    const result = element.clientHeight ? element : body;
    this.setState({ scrollY: result.scrollTop });
  };

  openPopup = (value) => {
    this.setState({
      popupIsOpen: true,
      popupName: value,
      navTab: '',
    });
  };

  closePopup = () => {
    this.setState({
      popupIsOpen: false,
      popupName: '',
    });
  };

  backInMenu = () => {
    this.setState({
      popupIsOpen: false,
      popupName: '',
      isOpenMenu: true,
    });
  };

  openAccount = () => {
    const { isShowAccountPopup, popupName } = this.state;
    if (popupName === 'liveHelp') {
      this.closePopup();
    }
    this.setState({ isShowAccountPopup: !isShowAccountPopup });
  }

  liveHelpOpenClose = () => {
    const { popupIsOpen } = this.state;
    if (popupIsOpen) {
      this.closePopup();
    } else {
      this.openPopup('liveHelp');
      this.toggleMenu();
    }
  };

  toggleMenu = () => {
    const { isOpenMenu, popupName, popupIsOpen } = this.state;
    this.setState({
      isOpenMenu: !isOpenMenu,
    });
    if (popupName === 'liveHelp' && popupIsOpen) {
      this.closePopup();
    }
  };

  getOrdersCount = () => {
    const { orders } = this.props;
    const count = Object.keys(orders).reduce((sum, key) => {
      if (!SHIPPING_PRODUCTS.includes(key)) {
        return sum + orders[key].length;
      }
      return sum;
    }, 0);
    return count;
  };


  generateTab = ({ withImages, link, name, text, buttonText, links }) => (
    <div className={b('tab')}>
      <div className={b('tab-container', { big: true })}>
        <div className={b('content-wrap', { create: true })}>
          <div className={b('info-block', { create: true })}>
            <span className={b('tab-name')}>{name}</span>
            <span className={b('tab-text')}>{text}</span>
            <a className={b('btn-link')} href={link}>
              <ColorButton className={b('btn')} text={buttonText} textClass={b('btn-text')} />
            </a>
          </div>
          {withImages ? (
            <div className={b('links-block')}>
              {links.length > 5 ? (
                <div className={b('slider-wrap')}>
                  <div className={b('slider')}>
                    <UniversalSlider
                      cards={links}
                      cardType="menu"
                      fullSHow={5}
                      middleShow={4}
                      tabletShow={4}
                      mediumShow={4}
                      smallShow={3}
                      miniSHow={3}
                      slidesToScroll={1}
                      wrapClass={b('slid')}
                      focusOnSelect={false}
                    />
                  </div>
                </div>
              ) : (
                <>
                  {links.map((item) => (
                    <MenuCard
                      data={item}
                      key={item.img}
                      defaultButtons={BUTTONS}
                      defaultTabs={TABS}
                    />
                  ))}
                </>
              )}
            </div>
          ) : (
            <div className={b('simple-links-block')}>
              {links.map(({ text: listLinkText, link: listLink }) => (
                <a className={b('simple-list-link')} href={listLink} key={`${name}${listLinkText}`}>
                  <CheckFull className={b('simple-list-link-icon')} />
                  <span className={b('simple-list-link-text')}>{listLinkText}</span>
                </a>
              ))}
            </div>
          )}
        </div>
      </div>
    </div>
  );

  openReviews = () => {
    const { loadReviews } = this.props;
    const { offset, order } = this.state;
    loadReviews({
      topBarLimit: window.innerWidth <= 480 ? 10 : 20,
      order,
      offset,
      isTopBar: true,
    });
    this.setState({ isReviewsPopupOpened: true });
  };

  closeReviews = () => {
    this.setState({ isReviewsPopupOpened: false });
  };

  render() {
    const {
      navTab,
      popupIsOpen,
      popupName,
      isOpenMenu,
      isReviewsPopupOpened,
      scrollY,
      isShowAccountPopup,
      createTabData,
    } = this.state;

    const {
      active,
      token,
      isOpenLogin,
      openLoginPopup,
      closeLoginPopup,
      frameMaterials,
      promoSection,
      reviewsCount,
      reviewsRate,
    } = this.props;


    const isAvailable = checkDate({
      weekDaysTimeStart: WEEKDAYS_TIME_START,
      weekDaysTimeEnd: WEEKDAYS_TIME_END,
      holidaysTimeStart: HOLIDAY_TIME_START,
      holidaysTimeEnd: HOLIDAY_TIME_END,
      workingHoliday: WORKING_HOLIDAY,
    });
    return (
      <>
        <nav
          className={b({ menu: isOpenMenu, hidden: scrollY > 300 })}
          id="navigation"
          ref={this.saveRef}
          onMouseLeave={this.closeTab({})}
        >
          <div className={b('navi-wrap')}>
            <div className={b('container', { mix: 'container_big' })}>
              <div className={b('navi-burger-menu', { mobile: true })}>
                <button className={b('burger-button-link')} type="button" onClick={this.toggleMenu}>
                  {isOpenMenu ? (
                    <CrossCloseMenuIcon className={b('icon')} />
                  ) : (
                    <BurgerMenuIcon className={b('icon')} />
                  )}
                </button>
              </div>
              <div className={b('home-container')}>
                <a className={b('home-link')} href={ROUTES.home}>
                  <LogoMemoya
                    className={b('logo-icon')}
                    fillIcon={iconColor}
                    fill={firstStringColor}
                    fillSecondString={secondStringColor}
                  />
                </a>
              </div>
              {
                this.windowWidth && this.windowWidth > 1024 ? (
                  <div className={b('navi-buttons')}>
                    {
                      BUTTONS.filter(({ id }) => id !== 'account').map(({ id, name, link }) => (
                        <a
                          className={b('navi-button', { active: active === name })}
                          key={id}
                          onMouseEnter={this.selectTab(id)}
                          href={link}
                        >
                          {name}
                        </a>
                      ))
                    }
                  </div>
                ) : null
              }
              <div className={b('buttons-block')}>
                {token ? (
                  <div className={b('with-live-help')}>
                    <div className={b('button-with-round')}>
                      <button
                        className={b('live-help-btn')}
                        type="button"
                        onClick={this.liveHelpOpenClose}
                      >
                        <div className={b('round', { unavailable: !isAvailable })} />
                        <MessageWithDotsIcon className={b('icon')} />
                        <span className={b('live-help-btn-title')}>{LIVE_HELP}</span>
                      </button>
                    </div>
                    <button
                      className={b('account-btn', { mobile: true, menu: isOpenMenu })}
                      type="button"
                      onClick={this.openAccount}
                    >
                      <LoginIcon2 className={b('icon')} />
                      <div className={b('account-text')}>{ACCOUNT}</div>
                      {isShowAccountPopup && (
                        <div className={b('account-popup')} role="button" tabIndex={-1} onClick={(e) => e.stopPropagation()}>
                          <div role="button" tabIndex={0} onClick={this.openAccount} className={b('notification-list-fond')} />
                          {ACCOUNT_LINES.map(({ text, link }) => (
                            <div className={b('account-menu-item')}>
                              {accountIcons[text.toLowerCase()]}
                              <a className={b('account-line')} href={link}>{text}</a>
                            </div>
                          ))}
                        </div>
                      )}
                    </button>
                  </div>
                ) : (
                  <div className={b('with-live-help')}>
                    <div className={b('button-with-round')}>
                      <button
                        className={b('live-help-btn')}
                        type="button"
                        onClick={this.liveHelpOpenClose}
                      >
                        <div className={b('round', { unavailable: !isAvailable })} />
                        <MessageWithDotsIcon className={b('icon')} />
                        <span className={b('live-help-btn-title')}>{LIVE_HELP}</span>
                      </button>
                    </div>
                    <button
                      className={b('control-button-link', {
                        mobile: true,
                        menu: isOpenMenu,
                        withText: true,
                      })}
                      type="button"
                      onClick={openLoginPopup}
                    >
                      <LoginIcon2 className={b('icon')} />
                      <span className={b('login-button-text')}>{LOGIN_TEXT}</span>
                    </button>
                  </div>
                )}
                <a
                  className={b('control-button-link', { menu: isOpenMenu, light: true })}
                  href={ROUTES.checkout}
                >
                  <CartIcon2 className={b('icon', { 'is-cart': true })} />
                  {this.getOrdersCount() >= 0 && (
                    <div className={b('orders-counter')}>{this.getOrdersCount()}</div>
                  )}
                </a>
                <div className={b('navi-burger-menu', { tablet: true })}>
                  <button
                    className={b('burger-button-link')}
                    type="button"
                    onClick={this.toggleMenu}
                  >
                    {isOpenMenu ? (
                      <CrossCloseMenuIcon className={b('icon')} />
                    ) : (
                      <BurgerMenuIcon className={b('icon')} />
                    )}
                  </button>
                </div>
              </div>
            </div>
          </div>
          <div className={b('call-us-block')}>
            <div className={b('call-us-info')}>
              <div className={b('call-us-info-container', { phone: true })}>
                <div className={b('call-us-title')}>{CALL_US}</div>
                <div className={b('call-us-text')}>{NUMBER}</div>
              </div>
              <div
                className={b('call-us-info-container', { review: true })}
                role="button"
                tabIndex={-1}
                onClick={this.openReviews}
              >
                <div className={b('call-us-title')}>{CUSTOMER_REVIEWS}</div>
                <Rate rate={reviewsRate} starDimension="min(3.5vw, 13px)" starSpacing="2px" />
                <div className={b('call-us-text', { review: true })}>{reviewsCount}</div>
              </div>
            </div>
            {isReviewsPopupOpened && (
              <ReviewsBanner
                reviewsCount={reviewsCount}
                reviewsRate={reviewsRate}
                closeReviews={this.closeReviews}
              />
            )}
          </div>
          {promoSection && promoSection.id && !popupIsOpen && !isOpenLogin && (
            <PromoBanner className={b('promo-banner')} scrollY={scrollY}>
              {isOpenMenu && (
                <Menu
                  className={b('popup-menu')}
                  changePopup={this.openPopup}
                  closeMenu={this.liveHelpOpenClose}
                  popupName={POPUP_NAME}
                  frameMaterials={frameMaterials}
                  defaultButtons={BUTTONS}
                  defaultTabs={TABS}
                />
              )}
            </PromoBanner>
          )}
          {navTab && this.generateTab(navTab !== 'create' ? {
            ...TABS[navTab],
            links: TABS[navTab].links.map(tab => (
              {
                ...tab,
                img: tab.img,
              }
            ))
          } : createTabData)}
          {popupIsOpen && popupName === POPUP_NAME.liveHelp && (
          <ModalContainerNavigation
            close={this.closePopup}
            // className="modal__live-help"
            isLiveHelp
            popupName={POPUP_NAME.liveHelp}
          >
            <LiveHelp className={b('live-help')} backInMenu={this.backInMenu} />
          </ModalContainerNavigation>
        )}
        </nav>
        {isOpenLogin && (
          <ModalContainer close={closeLoginPopup} closeInTop>
            <LoginForm
              className={b('popup-login')}
              changePopup={this.openPopup}
              popupName={POPUP_NAME}
              redirectLink={ROUTES.account}
              isPopup
            />
          </ModalContainer>
        )}
        {popupIsOpen && popupName === POPUP_NAME.signUp && (
          <ModalContainer
            close={this.closePopup}
            closeInTop
            openLoginPopup={openLoginPopup}
            popupName={POPUP_NAME.signUp}
          >
            <SignUpForm className={b('popup-sign-up')} close={this.closePopup} isPopup />
          </ModalContainer>
        )}
        {popupIsOpen && popupName === POPUP_NAME.forgotPass && (
          <ModalContainer
            close={this.closePopup}
            openLoginPopup={openLoginPopup}
            popupName={POPUP_NAME.forgotPass}
            className="modal__low-container"
          >
            <ForgotPassForm
              className={b('popup-sign-up')}
              close={this.closePopup}
              changePopup={this.openPopup}
            />
          </ModalContainer>
        )}
        {popupIsOpen && popupName === POPUP_NAME.emailSent && (
          <ModalContainer
            close={this.closePopup}
            popupName={POPUP_NAME.emailSent}
            className="modal__low-container"
          >
            <EmailSentForm
              className={b('popup-email-sent')}
              close={this.closePopup}
              openLoginPopup={openLoginPopup}
            />
          </ModalContainer>
        )}
        {isOpenMenu && promoSection && !Object.keys(promoSection).length && (
          <Menu
            className={b('popup-menu')}
            changePopup={this.openPopup}
            closeMenu={this.liveHelpOpenClose}
            popupName={POPUP_NAME}
            frameMaterials={frameMaterials}
            reviewsCount={reviewsCount}
            reviewsRate={reviewsRate}
            openReviews={this.openReviews}
            defaultButtons={BUTTONS}
            defaultTabs={TABS}
          />
        )}
      </>
    );
  }
}

Navigation.propTypes = propTypes;
Navigation.defaultProps = defaultProps;

const stateProps = (state) => ({
  orders: ordersTypeSelect(state),
  isLogin: loginStatus(state),
  token: getToken(state),
  isOpenCart: isOpenCartSelector(state),
  isOpenLogin: isOpenLoginPopupSelector(state),
  newPopupName: popupNameSelector(state),
  frameMaterials: frameMaterialsSelector(state),
  chatToken: chatTokenSelector(state),
  promoSection: promoSectionSelector(state),
  withoutRedirect: withoutRedirectSelector(state),
  reviewsCount: reviewsCountSelect(state),
  reviewsRate: reviewsRateSelect(state),
  appVersion: appVersionSelector(state),
});

const actions = {
  updateLocalCart: updateCart,
  openLoginPopup: openLoginPopupAction,
  closeLoginPopup: closeLoginPopupAction,
  deletePopupName: deletePopupData,
  getPromo: getPromoSection,
  updateImages: updateStorageImages,
  loadReviews: loadNewReviewers,
  loadFrameMaterials: getFrameMaterialAction,
  clearStorages: clearStoragesAction,
  clearAfterAppVersionChange: clearAfterAppVersionChangeAction,
};

export default connect(stateProps, actions)(Navigation);
