import React, { useEffect, useState } from 'react';
import * as yup from 'yup';
import Countdown from 'react-countdown';
import bem from 'src/utils/bem';
import deleteStringFirstElement from 'src/utils/deleteStringFirstElement';
import useMobileStyle from 'src/utils/useMobileStyle';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Formik } from 'formik';
import formatISO from 'date-fns/formatISO';
import debounce from 'lodash/debounce';
import ERRORS from 'src/constants/errors';

import dynamic from 'next/dynamic';
import {
  isLoadPromoCodeSelector,
  promoCodeSelector,
  promoSectionSelector,
} from 'src/redux/promo/selectors';
import { isMobileDeviceSelector } from 'src/redux/auth/selectors';
import { postPromoClient, setPromoCode } from 'src/redux/promo/actions';
import styles from './index.module.scss';
import { EMAIL_TEXT, INFO_TEXT, NAME_TEXT, ONE_WEEK, STYLES_NO_PIXELS } from './constants';

const ArrowIcon = dynamic(() => import('src/components/svg/ArrowIcon'));

const { SHOULD_BE_FILLED, INVALID_EMAIL } = ERRORS;

const b = bem('promo-banner', styles);

const defaultProps = {
  className: '',
  getPromo: () => {},
  isLoadPromoCode: false,
  promoCode: null,
  setPromoCodeToStore: () => {},
  promoSection: {},
  isMobileDevice: true,
  children: null,
  scrollY: 0,
};

const propTypes = {
  className: PropTypes.string,
  getPromo: PropTypes.func,
  isLoadPromoCode: PropTypes.bool,
  promoCode: PropTypes.shape({
    name: PropTypes.string,
  }),
  setPromoCodeToStore: PropTypes.func,
  promoSection: PropTypes.shape({
    steps: PropTypes.arrayOf(PropTypes.shape({})),
    promoCodeId: PropTypes.number,
    promoCode: PropTypes.shape({ name: PropTypes.string }),
    css: PropTypes.shape({
      styleSteps: PropTypes.arrayOf(
        PropTypes.shape({
          titleStyles: PropTypes.string,
          subtitleStyles: PropTypes.string,
          bodyStyles: PropTypes.string,
          disclaimerStyles: PropTypes.string,
          titleMobileStyles: PropTypes.string,
          subtitleMobileStyles: PropTypes.string,
          bodyMobileStyles: PropTypes.string,
          disclaimerMobileStyles: PropTypes.string,
        }),
      ),
    }),
    banner: PropTypes.shape({
      text: PropTypes.string,
      countDown: PropTypes.bool,
      countDownText: PropTypes.string,
      dropDownText: PropTypes.string,
    }),
    time: PropTypes.shape({
      endDate: PropTypes.string,
    }),
    isShowInBanner: PropTypes.bool,
    includeAdvertisementWithBanner: PropTypes.bool,
    loadPageWithAdvertisement: PropTypes.bool,
  }),
  isMobileDevice: PropTypes.bool,
  children: PropTypes.node,
  scrollY: PropTypes.number,
};

const schema = yup.object().shape({
  firstName: yup.string(SHOULD_BE_FILLED).required(),
  email: yup.string(SHOULD_BE_FILLED).required().email(INVALID_EMAIL),
});

function PromoBanner(props) {
  const {
    className,
    getPromo,
    isLoadPromoCode,
    promoCode,
    setPromoCodeToStore,
    promoSection,
    isMobileDevice,
    children,
    scrollY,
  } = props;

  const {
    steps = [],
    promoCodeId,
    css: { styleSteps } = {},
    banner: { text, countDown, countDownText, dropDownText } = {},
    promoCode: { name } = {},
    time: { endDate } = {},
    isShowInBanner = false,
    loadPageWithAdvertisement,
    includeAdvertisementWithBanner,
    promoCode: { name: promoCodeNameProps },
    promoCode: sectionPromoCode,
  } = promoSection;
  const { name: promoCodeName } = promoCode || {};
  const [windowWidth, changeWindowWidth] = useState(1920);
  const [isOpen, openBanner] = useState(loadPageWithAdvertisement);
  const [stepsNull] = steps;
  const firstStep = stepsNull ? steps.findIndex(({ hideStep }) => !hideStep) : null;
  const [selectedStep, changeStep] = useState(firstStep);
  const [isCheckLocalStorage, changeStatus] = useState(false);
  const [isShowNotification, toggleNotification] = useState(false);

  const setLocalPromo = () => {
    const allPromo = JSON.parse(localStorage.getItem('promoCode') || '{}') || {};
    localStorage.setItem(
      'promoCode',
      JSON.stringify({
        ...allPromo,
        [deleteStringFirstElement(document.location.pathname)]: {
          ...promoCode,
          setDate: Number(new Date()),
        },
      }),
    );
    changeStep(steps.length - 1);
  };

  const checkLocalPromo = () => {
    const pageKey = deleteStringFirstElement(document.location.pathname);
    const allPromo = JSON.parse(localStorage.getItem('promoCode') || '{}') || {};
    const { [pageKey]: promoCodeData = {}, ...rest } = allPromo;
    const { id, setDate } = promoCodeData;
    if (id && (setDate + ONE_WEEK < Number(new Date()) || id !== promoCodeId)) {
      localStorage.setItem('promoCode', JSON.stringify({ ...rest }));
    } else if (id) {
      setPromoCodeToStore(promoCodeData);
      changeStep(steps.length - 1);
    }
  };

  const resizeWindow = debounce(() => changeWindowWidth(window.innerWidth), 300);

  useEffect(() => {
    window.addEventListener('resize', resizeWindow)
    return () => window.removeEventListener('resize', resizeWindow)
  }, [])

  useEffect(() => {
    const lastStep = steps[steps.length - 1];
    const { hideStep: hideStepLast } = lastStep || {};
    if (!isCheckLocalStorage) {
      checkLocalPromo();
      changeStatus(true);
    }
    if (!isLoadPromoCode && promoCode && !hideStepLast) {
      setLocalPromo();
    }
  }, [isLoadPromoCode]);

  useEffect(() => {
    if (scrollY > 300) openBanner(false);
  }, [scrollY]);

  const { title, subtitle, body, buttonText, disclaimerText, desktopImage, mobileImage } =
    steps[selectedStep] || {};
  const stylesData = styleSteps;

  const {
    titleStyles,
    subtitleStyles,
    bodyStyles,
    disclaimerStyles,
    titleMobileStyles,
    subtitleMobileStyles,
    bodyMobileStyles,
    disclaimerMobileStyles,
  } = stylesData && stylesData[selectedStep] ? stylesData[selectedStep] : {};

  const isMobileDisplay = useMobileStyle(680, isMobileDevice);

  const clickNext = () => {
    const nextStepObject = steps[selectedStep + 1];
    const { hideStep: hideStepNext } = nextStepObject;
    const nextStep = hideStepNext ? selectedStep + 2 : selectedStep + 1;
    const nextStepObjectTakeIntoHide = steps[nextStep]
    const { hideStep: hideStepNextByHide } = nextStepObjectTakeIntoHide;
    if (nextStep < steps.length && !hideStepNextByHide) {
      changeStep(nextStep);
    }
  };

  const setPromoToSession = () => {
    sessionStorage.setItem('promoCode', JSON.stringify(promoCode || sectionPromoCode));
  };

  const getPromoCode = (values) => {
    getPromo({
      ...values,
      promoCodeId,
      pageKey: deleteStringFirstElement(document.location.pathname),
    });
  };

  const newEndDate = formatISO(new Date(endDate));

  const delayCloseNotification = debounce(() => toggleNotification(false), 3000);

  const getStyleValue = (max, value) => (value / max * 100).toFixed(4)

  const responsiveStyle = (style) => {
    if ( windowWidth > 1024 && windowWidth <= 1920) {
      const newStyle = style && Object.keys(style).length && Object.keys(style).reduce((acc, key) => {
        if (!STYLES_NO_PIXELS.includes(key)) {
          return {
            ...acc,
            [key]: `${getStyleValue(1920, style[key])}vw`
          }
        }
        return {
          ...acc,
          [key]: style[key],
        }

      }, {})
      return newStyle;
    }
    return style;
  }
  return (
    <>
      <div className={b({ mix: className, hidden: scrollY > 300 })}>
        <div
          className={b('top-block', {
            hidden: scrollY > 300,
            withSteps: !isShowInBanner,
          })}
          id="promo-banner"
        >
          <div className={b('top-block-content')}>
            <div className={b('top-text')}>{text}</div>
            {Boolean(countDown && name && endDate) && (
              <div className={b('promo-and-timer-wrap')}>
                {isShowInBanner && (
                  <div
                    className={b('promo-code-wrapper')}
                    role="button"
                    tabIndex="0"
                    onClick={() => {
                      setPromoToSession();
                      toggleNotification(true);
                      delayCloseNotification();
                    }}
                  >
                    <div className={b('top-promo-code')}>
                      {' '}
                      {name}
                    </div>
                    {isShowNotification && (
                      <div className={b('notification-block', { desktop: true })}>
                        <span className={b('notification-text')}>{INFO_TEXT}</span>
                      </div>
                    )}
                  </div>
                )}
                <Countdown
                  renderer={({ days, hours, minutes, seconds }) => (
                    <div className={b('top-timer')}>
                      <div className={b('time-container')}>
                        <div className={b('time-count')}>{hours + days * 24}</div>
                        <div className={b('time-title')}>hrs</div>
                      </div>
                      <div className={b('time-container')}>
                        <div className={b('time-count')}>{minutes}</div>
                        <div className={b('time-title')}>min</div>
                      </div>
                      <div className={b('time-container')}>
                        <div className={b('time-count')}>{seconds}</div>
                        <div className={b('time-title')}>sec</div>
                      </div>
                    </div>
                  )}
                  date={newEndDate}
                />
              </div>
            )}
            {isShowNotification && (
              <div className={b('notification-block')}>
                <span className={b('notification-text')}>{INFO_TEXT}</span>
              </div>
            )}
          </div>
          {includeAdvertisementWithBanner && (
            <div
              className={b('learn-more-block', {
                withSteps: includeAdvertisementWithBanner && !isShowInBanner,
              })}
            >
              <button
                className={b('learn-more', {
                  withSteps: includeAdvertisementWithBanner && !isShowInBanner,
                })}
                type="button"
                onClick={() => openBanner(!isOpen)}
              >
                <span className={b('btn-text')}>{dropDownText}</span>
                <div className={b('arrow-wrap')}>
                  <ArrowIcon className={b('arrow', { open: isOpen })} />
                </div>
              </button>
            </div>
          )}
        </div>
        {isOpen && (
          <div className={b('bottom-block')}>
            <div className={b('background-block')}>
              <div className={b('content-block')}>
                {title && (
                  <span
                    className={b('step-title')}
                    style={
                      isMobileDisplay
                        ? responsiveStyle(titleMobileStyles)
                        : responsiveStyle(titleStyles)
                    }
                  >
                    {title}
                  </span>
                )}
                {subtitle && (
                  <span
                    className={b('step-subtitle')}
                    style={
                      isMobileDisplay
                        ? responsiveStyle(subtitleMobileStyles)
                        : responsiveStyle(subtitleStyles)
                    }
                  >
                    {subtitle}
                  </span>
                )}
                {body && (
                  <span
                    className={b('step-text')}
                    style={
                      isMobileDisplay
                        ? responsiveStyle(bodyMobileStyles)
                        : responsiveStyle(bodyStyles)
                    }
                  >
                    {body}
                  </span>
                )}
                {selectedStep === 0 && (
                  <button className={b('step-btn')} type="button" onClick={() => {}}>
                    {buttonText}
                  </button>
                )}
                {selectedStep === 1 && (
                  <div className={b('input-block')}>
                    <Formik
                      initialValues={{ email: '', firstName: '' }}
                      validationSchema={schema}
                      onSubmit={(values, actions) => {
                        actions.resetForm();
                        getPromoCode(values);
                      }}
                      render={({ errors, handleSubmit, handleChange, values }) => (
                        <form onSubmit={handleSubmit} className={b('form')}>
                          <input
                            className={b('input')}
                            value={values.firstName}
                            onChange={handleChange}
                            name="firstName"
                            placeholder={NAME_TEXT}
                          />
                          <input
                            className={b('input')}
                            value={values.email}
                            onChange={handleChange}
                            name="email"
                            placeholder={EMAIL_TEXT}
                          />
                          <button
                            className={b('step-submit-btn')}
                            type="submit"
                            disabled={errors.email || errors.name}
                          >
                            {buttonText}
                          </button>
                        </form>
                      )}
                    />
                  </div>
                )}
                {selectedStep === steps.length - 1 && (
                  <button className={b('code-btn')} type="button" onClick={setPromoToSession}>
                    {promoCodeName || promoCodeNameProps}
                  </button>
                )}
                {disclaimerText && (
                  <span
                    className={b('step-bottom-text')}
                    style={
                      isMobileDisplay
                        ? responsiveStyle(disclaimerMobileStyles)
                        : responsiveStyle(disclaimerStyles)
                    }
                  >
                    {disclaimerText}
                  </span>
                )}
                {countDown && selectedStep === 0 && (
                  <div className={b('step-expires-block')}>
                    <span className={b('expires-block-text')}>{countDownText}</span>
                    <span className={b('expires-block-time')}>
                      <Countdown date={newEndDate} daysInHours />
                    </span>
                  </div>
                )}
              </div>
            </div>
            <img
              className={b('image-block')}
              src={isMobileDisplay ? mobileImage : desktopImage}
              alt="img"
            />
          </div>
        )}
      </div>
      {children}
    </>
  );
}

PromoBanner.propTypes = propTypes;
PromoBanner.defaultProps = defaultProps;

const stateProps = (state) => ({
  promoSection: promoSectionSelector(state),
  promoCode: promoCodeSelector(state),
  isLoadPromoCode: isLoadPromoCodeSelector(state),
  isMobileDevice: isMobileDeviceSelector(state),
});

const actions = {
  getPromo: postPromoClient,
  setPromoCodeToStore: setPromoCode,
};

export default connect(stateProps, actions)(PromoBanner);
