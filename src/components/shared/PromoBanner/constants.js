const EXPIRES = 'Expires';
const LEARN_MORE = 'Learn more';
const NAME_TEXT = 'Enter your first name';
const EMAIL_TEXT = 'Enter your email address';
const CLICK_TO_APPLY = 'Click to apply:';
const ONE_WEEK = 604800000;
const INFO_TEXT = 'Promo code has been added to your cart';

const STYLES_NO_PIXELS = ['color', 'fontWeight'];

export { 
  EXPIRES,
  LEARN_MORE,
  ONE_WEEK,
  NAME_TEXT,
  EMAIL_TEXT,
  CLICK_TO_APPLY,
  INFO_TEXT,
  STYLES_NO_PIXELS,
};
