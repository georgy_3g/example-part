const TERMS = {
  TITLE: 'Our customers love us.',
  REVIEWS: 'reviews',
  TEXT: 'rating based on',
  RATE_MARK: 'excellent',
  READ_ALL_REVIEWS: 'read all reviews',
};

export default TERMS;
