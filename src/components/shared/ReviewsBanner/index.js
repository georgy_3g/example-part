import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import bem from 'src/utils/bem';
import reviewsFormatText from 'src/utils/reviewsFormatText';
import formatDistance from 'date-fns/formatDistance'
import dynamic from "next/dynamic";
import TERMS from './constants';
import styles from './index.module.scss';

const Rate = dynamic(() => import('src/components/elements/Rate'));
const CloseIcon2 = dynamic(() => import('src/components/svg/CloseIcon2'));

const { TEXT, RATE_MARK, REVIEWS, READ_ALL_REVIEWS } = TERMS;
const b = bem('reviews-banner', styles);

const propTypes = {
  topBarReviewers: PropTypes.arrayOf(PropTypes.shape({})),
  reviewsCount: PropTypes.number,
  reviewsRate: PropTypes.number,
  closeReviews: PropTypes.func,
};

const defaultProps = {
  topBarReviewers: [],
  reviewsCount: 1380,
  reviewsRate: 5,
  closeReviews: () => {},
};

function ReviewsBanner(props) {
  const { reviewsCount, reviewsRate, topBarReviewers, closeReviews } = props;


  const reviewsOnlyGood = topBarReviewers.filter(item => item.rating >= 4);

  return (
    <main className={b()}>
      <div className={b('title')}>
        <div className={b('close-btn')} role="button" tabIndex={-1} onClick={() => closeReviews()}>
          <CloseIcon2 />
        </div>
        <div className={b('title-container')}>
          <div className={b('title-string')}>
            <span className={b('title-text', { bold: true })}>
              {Number(reviewsRate).toFixed(2)}
            </span>
            <span className={b('title-text')}>{TEXT}</span>
          </div>
          <div className={b('title-string')}>
            <span className={b('title-text', { bold: true })}>{reviewsCount}</span>
            <span className={b('title-text')}>{REVIEWS}</span>
          </div>
        </div>
        <div className={b('title-container')}>
          <div className={b('title-text', { bold: true, mark: true })}>{RATE_MARK}</div>
          <div className={b('rating')}>
            <Rate rate={reviewsRate} starDimension="min(18px)" starSpacing="1px" />
          </div>
        </div>
      </div>
      <div className={b('reviews-wrapper', { small: window.innerHeight < 720 })}>
        {reviewsOnlyGood &&
          reviewsOnlyGood.map((review) => {
            const {
              comments,
              date_created: reviewDate,
              rating,
              reviewer: { first_name: firstName = '', last_name: lastName = '' },
            } = review;
            const formatDate = reviewDate.split(" ").join("T");
            return (
              <div className={b('review-wrapper')}>
                <div className={b('name-with-rating')}>
                  <span className={b('reviewer-name')}>{`${firstName} ${lastName}`}</span>
                  <Rate rate={rating} starDimension="min(4vw, 16px)" starSpacing="1px" />
                </div>
                <div className={b('review-comment')}>{reviewsFormatText(comments)}</div>
                <div className={b('review-date')}>{formatDistance(new Date(formatDate), new Date(), { addSuffix: true })}</div>
              </div>
            );
          })}
      </div>
      <div className={b('reviews-footer')}>
        <a
          className={b('link-to-all-reviews')}
          href="http://www.reviews.io/company-reviews/store/*******?utm_source=*******&utm_medium=widget&utm_campaign=vertical"
        >
          {READ_ALL_REVIEWS}
        </a>
      </div>
    </main>
  );
}

ReviewsBanner.propTypes = propTypes;
ReviewsBanner.defaultProps = defaultProps;

const stateProps = ({ reviewers: { topBarReviewers } }) => ({
  topBarReviewers,
});

export default connect(stateProps, null)(ReviewsBanner);
