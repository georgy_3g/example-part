import React from 'react';
import bem from 'src/utils/bem';
import PropTypes from 'prop-types';
import { DATA } from './constants';
import styles from './index.module.scss';

const b = bem('work-time-block', styles);

const propTypes = {
  className: PropTypes.string,
};

const defaultProps = {
  className: '',
};

const WorkTimeBlock = ({ className }) => {

  return (
    <section className={b({ mix: className })}>
      <div className={b('content-block')}>
        {DATA.map(({title, list}) => (
          <h2 className={b('content')} key={title}>
            <span className={b('title')}>{title}</span>
            {list.map(({ subtitle, text, href }) => (
              <div className={b('item-block')} key={`${title}_${subtitle}`}>
                <span className={b('subtitle')}>{subtitle}</span>
                {href
                  ? (<a className={b('link')} href={href}>{text}</a>)
                  : (<span className={b('text')} href={href}>{text}</span>)}
              </div>
            ))}
          </h2>
        )) }
      </div>
    </section>
  );
}

WorkTimeBlock.propTypes = propTypes;
WorkTimeBlock.defaultProps = defaultProps;

export default WorkTimeBlock;
