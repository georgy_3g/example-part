import ROUTES from 'src/constants/routes';

const {
  contact,
} = ROUTES;
const HOURS = {
  title: 'Hours:',
  list: [
    {
      subtitle: 'Monday - Friday',
      text: '9 AM - 6 PM',
    },
    {
      subtitle: 'Saturday',
      text: '11 AM - 4 PM',
    },
  ],
};

const CONTACT = {
  title: 'Contact:',
  list: [
    {
      subtitle: 'Phone number',
      text: '561-235-7808',
    },
    {
      subtitle: 'Email',
      text: 'hello@*******.com',
    },
  ],
}

const LOCATION = {
  title: 'Location:',
  list: [
    {
      subtitle: '1000 Clint Moore Road\nSuite 208\nBoca Raton, FL 33487',
      text: 'Get directions!',
      href: contact
    },
  ],
}

const DATA = [HOURS, LOCATION, CONTACT];

export {
  DATA,
  HOURS,
  CONTACT,
  LOCATION,
}
