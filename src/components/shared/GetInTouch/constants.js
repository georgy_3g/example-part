const PLACEHOLDERS = {
  name: 'Your name',
  email: 'Your email',
  reason: 'Reason for contact',
  subject: 'Subject',
  phone: 'Your phone',
};

const REASON_OPTIONS = [
  { value: 'Question about a product or service', label: 'Question about a product or service' },
  { value: 'Help with an existing order', label: 'Help with an existing order' },
  { value: 'General inquiries', label: 'General inquiries' },
  { value: 'Media relations inquiries', label: 'Media relations inquiries' },
];

export { PLACEHOLDERS, REASON_OPTIONS };
