import React from 'react';
import bem from 'src/utils/bem';
import PropTypes from 'prop-types';

import dynamic from 'next/dynamic';
import { connect } from 'react-redux';
import styles from './index.module.scss';
import { promoSectionSelector } from '../../../redux/promo/selectors';

const GetInTouchInfo = dynamic(() => import('src/components/widgets/GetInTouchInfo'));

const b = bem('get-in-touch', styles);

const propTypes = {
  className: PropTypes.string,
  promoSection: PropTypes.shape({
    id: PropTypes.number,
  }),
};

const defaultProps = {
  className: '',
  promoSection: {},
};

function GetInTouch({ className, promoSection }) {
  return (
    <section className={b({ mix: className })} style={{ marginTop: promoSection.id ? `110px` : '20px' }}>
      <GetInTouchInfo className={b('info-block')} isConnectPage />
    </section>
  );
}

GetInTouch.propTypes = propTypes;
GetInTouch.defaultProps = defaultProps;

const stateProps = (state) => ({
  promoSection: promoSectionSelector(state),
});

export default connect(stateProps, null)(GetInTouch);
