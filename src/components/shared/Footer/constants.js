import ROUTES from 'src/constants/routes';

const {
  tapeTransfer,
  filmTransfer,
  audioTransfer,
  digitalTransfer,
  photoTransfer,
  photoRestoration,
  photoRetouching,
  photoArt,
  howItWorks,
  ourMission,
  giftGuide,
  blog,
  contact,
  digitize,
  enhance,
  create,
  giftCard,
  reviews,
  faq,
  southFloridaPhotoScanning,
  printShop,
} = ROUTES;

const DIGITIZE = {
  name: 'Digitize',
  isTitleLink: true,
  titleUrl: digitize,
  links: [
    {
      text: 'Photo scanning',
      url: photoTransfer,
      key: 'photo_scan',
    },
    {
      text: 'Tape transfer',
      url: tapeTransfer,
      key: 'tape_transfer',
    },
    {
      text: 'Film transfer',
      url: filmTransfer,
      key: 'film_transfer',
    },
    {
      text: 'Audio transfer',
      url: audioTransfer,
      key: 'audio_transfer',
    },
    {
      text: 'Digital transfer',
      url: digitalTransfer,
      key: 'digital_transfer',
    },
    {
      text: 'Boca Raton\nmedia transfer',
      url: southFloridaPhotoScanning,
      key: 'boca_raton_media_transfer',
      desc: true,
    },
  ],
};
const ENHANCE = {
  name: 'Enhance',
  isTitleLink: true,
  titleUrl: enhance,
  links: [
    {
      text: 'Photo restoration',
      url: photoRestoration,
      key: 'photo_restoration',
    },
    {
      text: 'Photo retouching',
      url: photoRetouching,
      key: 'photo_retouching',
    },
  ],
};

const CREATE = {
  name: 'Create',
  isTitleLink: true,
  titleUrl: create,
  links: [
    {
      text: 'Photo art',
      url: photoArt,
      key: 'photo_art',
    },
    {
      text: 'Gift guide',
      url: giftGuide,
      key: 'gift_guide_mob',
    },
    {
      text: 'Gift card',
      url: giftCard,
      key: 'gift_card_mob',
    },
    {
      text: 'Boca Raton\nframe shop',
      url: printShop,
      key: 'boca_raton_frame_shop',
      desc: true,
    },
  ],
};

const MORE = {
  name: 'More',
  isTitleLink: false,
  titleUrl: '',
  links: [
    {
      text: 'Contact',
      url: contact,
      key: 'contact_mob',
    },
    {
      text: 'How it works',
      url: howItWorks,
      key: 'how_it_works_mob',
    },
    {
      text: 'Our mission',
      url: ourMission,
      key: 'our_mission_mob',
    },
    {
      text: 'Reviews',
      url: reviews,
      key: 'reviews_mob',
    },
    {
      text: 'FAQs',
      url: faq,
      key: 'faqs',
    },
  ],
};

const MOBILE_MORE = {
  name: 'More',
  links: [
    {
      text: 'Contact',
      url: contact,
      key: 'contact_mob',
    },
    {
      text: 'How it works',
      url: howItWorks,
      key: 'how_it_works_mob',
    },
    {
      text: 'Our mission',
      url: ourMission,
      key: 'our_mission_mob',
    },
    {
      text: 'Gift guide',
      url: giftGuide,
      key: 'gift_guide_mob',
    },
    {
      text: 'Gift card',
      url: giftCard,
      key: 'gift_card_mob',
    },
    {
      text: 'Blog',
      url: blog,
      key: 'blog_mob',
    },
    {
      text: 'Reviews',
      url: reviews,
      key: 'reviews_mob',
    },
  ],
};

const BOCA_RATON = {
  name: 'Boca Raton',
  isTitleLink: true,
  titleUrl: enhance,
  isString: true,
  links: [
    {
      text: 'Frame shop',
      url: printShop,
      key: 'frame_shop_mob',
    },
    {
      text: 'Media transfer',
      url: southFloridaPhotoScanning,
      key: 'media_transfer_mob',
    },
  ],
};

const TERMS_AND_CONDITIONS = 'Terms & Conditions';
const PRIVACY_POLICY = 'Privacy Policy';
const COPYRIGHT_TEXT = 'Copyright 2021 © Legacy Media Group, LLC';
const SOCIAL = 'Social';
const CHECK_OUT_OUR_BLOG = 'Check out our blog';

export {
  DIGITIZE,
  ENHANCE,
  CREATE,
  MORE,
  MOBILE_MORE,
  TERMS_AND_CONDITIONS,
  PRIVACY_POLICY,
  COPYRIGHT_TEXT,
  SOCIAL,
  CHECK_OUT_OUR_BLOG,
  BOCA_RATON,
};
