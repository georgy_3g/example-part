import React from 'react';
import bem from 'src/utils/bem';
import ROUTES from 'src/constants/routes';
import { FACEBOOK_LINK, INSTAGRAM_LINK, YOUTUBE_LINK } from 'src/config';
import dynamic from 'next/dynamic';
import {
  BOCA_RATON,
  CHECK_OUT_OUR_BLOG,
  COPYRIGHT_TEXT,
  CREATE,
  DIGITIZE,
  ENHANCE,
  MORE,
  PRIVACY_POLICY,
  SOCIAL,
  TERMS_AND_CONDITIONS,
} from './constants';
import styles from './index.module.scss';

const YouTubeIcon = dynamic(() => import('src/components/svg/YouTubeIcon'));
const InstagramIcon = dynamic(() => import('src/components/svg/InstagramIcon'));
const FacebookIcon = dynamic(() => import('src/components/svg/FacebookIcon'));
const LogoMemoya = dynamic(() => import('src/components/svg/LogoMemoya'));

const {
  termsAndConditions,
  privacyAndPolicy,
  home,
  blog,
} = ROUTES;

const b = bem('footer', styles);

const Footer = () => {

  const linkList = ({
    name,
    links,
    isTitleLink,
    titleUrl,
    isString
  }, isSecondClass) => (
    <>
      {!isSecondClass &&
        (isTitleLink ? (
          <a
            className={b('link-title')}
            href={titleUrl}
            key={titleUrl}
          >
            {name}
          </a>
        ) : (
          <h4 className={b('link-title')}>{name}</h4>
        ))}
      <div className={b('link-list', { second: isSecondClass, string: isString })}>
        {[...links].map(({ text, url, key, desc }) => (
          <a className={b('link', { second: isSecondClass, desc, string: isString })} href={url} key={key}>
            {text}
          </a>
        ))}
      </div>
    </>
  );
    return (
      <footer className={b()} id="footer-id">
        <div className={b('container')}>
          <div className={b('logo-container')}>
            <a className={b('logo')} href={home}>
              <LogoMemoya className={b('logo-icon')} />
            </a>
            <div className={b('terms-block')}>
              <a className={b('terms-link')} href={termsAndConditions}>
                {TERMS_AND_CONDITIONS}
              </a>
              <a className={b('terms-link')} href={privacyAndPolicy}>
                {PRIVACY_POLICY}
              </a>
              <span className={b('terms-text')}>{COPYRIGHT_TEXT}</span>
            </div>
          </div>
          <div className={b('container-links')}>
            <div className={b('content-string')}>
              <section className={b('container-wrap')}>{linkList(ENHANCE)}</section>
              <section className={b('container-wrap')}>{linkList(CREATE)}</section>
            </div>
            <div className={b('content-string')}>
              <section className={b('container-wrap')}>{linkList(DIGITIZE)}</section>
              <section className={b('container-wrap', { last: true })}>{linkList(MORE)}</section>
            </div>
          </div>
          <div className={b('container-links', { mob: true })}>
            <div className={b('content-string')}>
              <section className={b('container-wrap')}>{linkList(BOCA_RATON)}</section>
            </div>
          </div>
          <div className={b('container-social')}>
            <div className={b('social')}>
              <span className={b('social-title')}>{SOCIAL}</span>
              <div className={b('social-icons')}>
                <a className={b('icon-link')} href={FACEBOOK_LINK} target="_blank" rel="noreferrer">
                  <FacebookIcon className={b('social-svg')} />
                </a>
                <a className={b('icon-link')} href={INSTAGRAM_LINK} target="_blank" rel="noreferrer">
                  <InstagramIcon className={b('social-svg')} />
                </a>
                <a className={b('icon-link')} href={YOUTUBE_LINK} target="_blank" rel="noreferrer">
                  <YouTubeIcon className={b('social-svg')} />
                </a>
              </div>
              <a className={b('blog-link')} href={blog}>{CHECK_OUT_OUR_BLOG}</a>
            </div>
          </div>
        </div>
        <div className={b('mob-terms-block')}>
          <div className={b('mob-link-block')}>
            <a className={b('terms-link', { mob: true })} href={termsAndConditions}>
              {TERMS_AND_CONDITIONS}
            </a>
            <span className={b('separator')}>|</span>
            <a className={b('terms-link', { mob: true })} href={privacyAndPolicy}>
              {PRIVACY_POLICY}
            </a>
          </div>
          <span className={b('terms-text', { mob: true })}>{COPYRIGHT_TEXT}</span>
        </div>
      </footer>
    );
  }

export default Footer;
