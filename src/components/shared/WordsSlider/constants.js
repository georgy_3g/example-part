const TITLE = 'Your local media transfer service near you.';
const DESCRIPTION = 'Since 2007, we have been the number one media transfer service in South Florida. Come visit our Boca Raton\nheadquarters to transfer your VHS to DVD, scan your family photos & convert your 8mm film reels.';
const WORDS_LIST = [
  'Boca Raton',
  'West Palm Beach',
  'Fort Lauderdale',
  'Delray Beach',
  'Miami',
  'Aventura',
  'Wellington',
  'Davie',
  'Lighthouse Point',
  'Sunrise',
  'Boynton Beach',
  'Lake Worth',
  'Sunny Isles',
  'Deerfield Beach',
];


export {
  TITLE,
  DESCRIPTION,
  WORDS_LIST,
};
