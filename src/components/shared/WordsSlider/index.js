import React, { Component } from 'react';
import PropTypes from 'prop-types';
import debounce from 'lodash/debounce';
import bem from 'src/utils/bem';
import { DESCRIPTION, TITLE, WORDS_LIST } from './constants';
import styles from './index.module.scss';

const b = bem('words-slider', styles);

const defaultProps = {
  className: '',
  wordClass: '',
  animationTime: 500,
  time: 3000,
  slidesToShowResponsive: {
    fullSHow: 8,
    mediumShow: 4,
    smallShow: 2,
    miniSHow: 1,
  },
  slidesToShow: 8,
  slideList: WORDS_LIST,
  title: TITLE,
  description: DESCRIPTION,
};

const propTypes = {
  className: PropTypes.string,
  wordClass: PropTypes.string,
  animationTime: PropTypes.number,
  slidesToShowResponsive: PropTypes.objectOf(PropTypes.number),
  time: PropTypes.number,
  slidesToShow: PropTypes.number,
  slideList: PropTypes.arrayOf(PropTypes.string),
  title: PropTypes.string,
  description: PropTypes.string,
};

class WordsSlider extends Component {
  resizeWindow = debounce(() => {
    this.onResizeWindow();
  }, 300);

  constructor(props) {
    super(props);
    this.list = null;
    this.block = null;
    this.firstSide = 0;
    this.offset = 0;

    const { slidesToShow } = props;

    this.state = {
      slidesToShow,
      isMount: false,
    };
  }

  componentDidMount() {
    window.addEventListener('resize', this.resizeWindow);
    const slidesToShow = this.getSlideCount();
    const blockWidth = this.block.clientWidth;
    this.setState({
      slidesToShow,
      isMount: true,
      slideWidth: blockWidth / slidesToShow,
    });
    this.restartNestStep();
  }

  componentWillUnmount() {
    this.list = null;
    this.block = null;
    this.firstSide = null;
    this.offset = null;
    window.removeEventListener('resize', this.resizeWindow);
  }

  onResizeWindow = () => {
    const slidesToShow = this.getSlideCount();
    const blockWidth = this.block.clientWidth;
    const slideWidth = blockWidth / slidesToShow;
    this.offset = slideWidth * (this.firstSide + 1);
    this.list.style.transform = `translateX(-${this.offset}px)`;


    this.setState({
      slidesToShow,
      slideWidth: blockWidth / slidesToShow,
    });
  }

  getSlideCount = () => {
    const { slidesToShowResponsive = {} , slidesToShow} = this.props;
    const { fullSHow, mediumShow, smallShow, miniSHow } = slidesToShowResponsive || {}

    const windowWidth = window.innerWidth;

    if (windowWidth > 1024) {
      return fullSHow || slidesToShow
    }

    if (windowWidth > 768 && windowWidth <= 1024) {
      return mediumShow || fullSHow || slidesToShow;
    }

    if (windowWidth > 480 && windowWidth <= 768) {
      return smallShow
      || mediumShow
      || fullSHow
      || slidesToShow;
    }

    if (windowWidth < 480) {
      return miniSHow
      || smallShow
      || mediumShow
      || fullSHow
      || slidesToShow;
    }

    return slidesToShow;
  }

  nextSlide = () => {
    const { animationTime } = this.props;
    const { slideWidth } = this.state;
    if (this.list) {
      const newOffset = slideWidth * (this.firstSide + 1);
      this.list.style.transition = `transform ${animationTime / 1000}s ease 0s`;
      this.list.style.transform = `translateX(-${newOffset}px)`;
      this.offset = newOffset;
      setTimeout(() => { this.removeAnimation(); }, animationTime);
      this.restartNestStep();
    }
  }

  removeAnimation = () => {
    const { slideList } = this.props;
    if (this.list) {
      if (slideList.length > this.firstSide + 1) {
        this.list.style.transition = 'none';
        this.list.style.transform = `translateX(-${this.offset}px)`;
        this.firstSide += 1;
      } else {
        this.list.style.transition = 'none';
        this.list.style.transform = `translateX(0px)`;
        this.offset = 0;
        this.firstSide = 0;
      }
    }
  }

  restartNestStep = () => {
    const { time } = this.props;
    setTimeout(() => { this.nextSlide() }, time);
  }

  saveListRef = (e) => {
    this.list = e;
  }

  saveBlockRef = (e) => {
    this.block = e;
  }

  render() {
    const {
      className,
      wordClass,
      slideList,
      title,
      description,
    } = this.props
    const {
      slidesToShow,
      isMount,
      slideWidth,
    } = this.state;

    return (
      <div className={b('', { mix: className} )} ref={this.saveBlockRef} style={isMount ? {} : { opacity: '0' }}>
        <h2 className={b('title')}>{title}</h2>
        <span className={b('description')}>{description}</span>
        <div className={b('list-wrap')}>
          <div className={b('list')} ref={this.saveListRef}>
            {slideList.map((item) => (
              <div className={b('word-wrap')} style={{ width: `${ slideWidth }px` }} key={item}>
                <span className={b('word', { mix: wordClass })}>{item}</span>
              </div>
            ))}
            {slideList.map((item, i) => i < slidesToShow
              ? (
                <div className={b('word-wrap')} key={`repeat_${item}`} style={{ width: `${ slideWidth }px` }}>
                  <span className={b('word')}>{item}</span>
                </div>
              )
              : null
            )}
          </div>
        </div>
      </div>
    );
  }
}

WordsSlider.propTypes = propTypes;
WordsSlider.defaultProps = defaultProps;

export default WordsSlider;
