import React from 'react';
import PropTypes from 'prop-types';
import formatUrl from 'src/utils/formatUrl';
import bem from 'src/utils/bem';
import { connect } from 'react-redux';
import ROUTES from 'src/constants/routes';
import Image from 'next/image';
import { getBlogList } from 'src/redux/blogList/selectors';
import BLOGTEXTS from './constants';
import styles from './index.module.scss';

const b = bem('blog', styles);
const { blog } = ROUTES;
const { blogText, blogTitle, blogSeeAll } = BLOGTEXTS;

const defaultProps = {
  blogs: {},
  className: '',
};

const propTypes = {
  blogs: PropTypes.shape({
    total: PropTypes.string,
    data: PropTypes.arrayOf(PropTypes.shape({})),
  }),
  className: PropTypes.string,
};

function Blog(props) {
  const {
    blogs: { data },
    className,
  } = props;
  return (
    <div className={b({ mix: className })}>
      <div className={b('description')}>
        <p className={b('title')}>{blogTitle}</p>
        <p className={b('text')}>
          {blogText}
          <a className={b('link')} href={blog}>
            {blogSeeAll}
          </a>
        </p>
      </div>
      <div className={b('image-block')}>
        {data.map(({ previewImageUri: imageLink, id, titleSeo }) => (
          <a className={b('image-link')} href={`${blog}${formatUrl(titleSeo)}/`} key={id}>
            <div className={b('image-wrapper')}>
              <Image className={b('image')} src={imageLink} alt={id} width='100%' height='100%' />
            </div>
          </a>
        ))}
      </div>
    </div>
  );
}

const stateProps = (state) => ({
  blogs: getBlogList(state),
});

Blog.propTypes = propTypes;
Blog.defaultProps = defaultProps;

export default connect(stateProps, null)(Blog);
