const BLOGTEXTS = {
  blogTitle: 'Blog',
  blogText: 'Follow us. ',
  blogSeeAll: 'Hear from your new best friends.',
};

export default BLOGTEXTS;
