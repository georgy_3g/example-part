import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import bem from 'src/utils/bem';

import dynamic from 'next/dynamic';
import {
  reviewsCountSelect,
  reviewsLoadingSelect,
  reviewsRateSelect,
  reviewsSelect,
  reviewTotalPagesSelect,
} from 'src/redux/reviewers/selectors';
import { loadNewReviewers } from 'src/redux/reviewers/actions';
import TERMS from './constants';
import styles from './index.module.scss';

const ReviewsImageSlider = dynamic(() => import('src/components/widgets/ReviewsImageSlider'));

const { TITLE } = TERMS;
const b = bem('customer-block', styles);

const propTypes = {
  reviews: PropTypes.arrayOf(PropTypes.shape({})),
  loadReviews: PropTypes.func,
  className: PropTypes.string,
  title: PropTypes.string,
  description: PropTypes.string,
};

const defaultProps = {
  reviews: [],
  loadReviews: () => {},
  className: '',
  title: TITLE,
  description: '',
};

class CustomerBlock extends Component {
  constructor(props) {
    super(props);
    this.state = {
      limit: 6,
      offset: 0,
      order: 'desc',
    };
  }

  componentDidMount() {
    const { limit, offset, order } = this.state;
    const { loadReviews } = this.props;
    loadReviews({ limit, order, offset });
  }

  handlePageClick = ({ selected }) => {
    const { limit, order } = this.state;
    const { loadReviews } = this.props;
    loadReviews({ limit, order, offset: selected });
  };

  render() {
    const {
      reviews,
      className,
      title,
      description,
    } = this.props;
    return (
      <main className={b({ mix: className })}>
        <div className={b('background-stripe')} />
        <h2 className={b('title')}>{title}</h2>
        {description ? (
          <div className={b('description')}>{description}</div>
        ) : null}
        <div className={b('reviews-list')}>
          <ReviewsImageSlider slideList={reviews} customerSlider />
          <div className={b('stripe')} />
        </div>
      </main>
    );
  }
}

CustomerBlock.propTypes = propTypes;
CustomerBlock.defaultProps = defaultProps;

const stateProps = (state) => ({
  reviews: reviewsSelect(state),
  totalReviews: reviewsCountSelect(state),
  averageRating: reviewsRateSelect(state),
  isLoading: reviewsLoadingSelect(state),
  totalPages: reviewTotalPagesSelect(state),
});

const actions = {
  loadReviews: loadNewReviewers,
};

export default connect(stateProps, actions)(CustomerBlock);
