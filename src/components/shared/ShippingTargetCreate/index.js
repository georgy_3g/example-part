import React, { Component } from 'react';
import PropTypes from 'prop-types';
import bem from 'src/utils/bem';
import getZipCode from 'src/utils/getZipCode';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';
import Image from 'next/image';
import ROUTES from 'src/constants/routes';

import mapImg from 'public/img/contact/contact-map.jpg';
import artImage from 'public/img/photoArt/shipping-popup-create.jpeg';
import artImageMobile from 'public/img/photoArt/shipping-popup-create-mobile.jpeg';
import dynamic from 'next/dynamic';
import { storeUtmContentSelector, zipCodeSelector } from 'src/redux/location/selectors';
import { billingInfoSelect, zipCodesSelector } from 'src/redux/shared/selectors';
import { getToken } from 'src/redux/auth/selectors';
import { changeUtmContentActions, changeZipCode } from 'src/redux/location/actions';
import {
  changeTemporaryUtmContentActions,
  getUserBillingInfo,
} from 'src/redux/shared/actions';
import {
  BUTTON_LINK,
  DESCRIPTION_MOBILE,
  GET_DIRECTIONS,
  OR_CALL_ASSISCTENCE,
  OR_CALL_CONSULTATION,
  PHONE,
} from './constants';
import styles from './index.module.scss';

const GeoTagIcon = dynamic(() => import('src/components/svg/GeoTagIcon'));
const CloseIcon2 = dynamic(() => import('src/components/svg/CloseIcon2'));

const b = bem('shipping-target', styles);
const defaultProps = {
  className: '',
  iconDescription: '',
  iconTitle: '',
  description: '',
  titleFull: '',
  firstItem: {
    image: {},
    title: '',
    description: '',
    buttonText: '',
    withLink: false,
  },
  secondItem: {
    image: {},
    title: '',
    description: '',
    buttonText: '',
    withLink: false,
  },
  thirdItem: {
    image: {},
    title: '',
    description: '',
    buttonText: '',
    withLink: false,
  },
  billingInfo: {},
  storeUtmContent: '',
  zipCode: '',
  zipCodes: '',
  changeUtmContent: () => {},
  changeZip: () => {},
  token: '',
  getBillingInfo: () => {},
  changeTemporaryUtmContent: '',
};

const propTypes = {
  className: PropTypes.string,
  iconDescription: PropTypes.string,
  iconTitle: PropTypes.string,
  description: PropTypes.string,
  titleFull: PropTypes.string,
  firstItem: PropTypes.shape({
    image: PropTypes.shape({}),
    title: PropTypes.string,
    description: PropTypes.string,
    buttonText: PropTypes.string,
    withLink: PropTypes.bool,
  }),
  secondItem: PropTypes.shape({
    image: PropTypes.shape({}),
    title: PropTypes.string,
    description: PropTypes.string,
    buttonText: PropTypes.string,
    withLink: PropTypes.bool,
  }),
  thirdItem: PropTypes.shape({
    image: PropTypes.shape({}),
    title: PropTypes.string,
    description: PropTypes.string,
    buttonText: PropTypes.string,
    withLink: PropTypes.bool,
  }),
  billingInfo: PropTypes.shape({
    zipCode: PropTypes.string,
    state: PropTypes.string,
  }),
  storeUtmContent: PropTypes.string,
  zipCode: PropTypes.string,
  zipCodes: PropTypes.arrayOf(PropTypes.shape({})),
  changeUtmContent: PropTypes.func,
  changeZip: PropTypes.func,
  token: PropTypes.string,
  getBillingInfo: PropTypes.func,
  changeTemporaryUtmContent: PropTypes.func,
};

class ShippingTargetCreate extends Component {
  constructor(props) {
    super(props);

    this.Geolocation = null;

    this.state = {
      isOpen: false,
      coordinate: null,
      isShowButton: true,
    };
  }

  componentDidMount() {
    const { zipCode, token, getBillingInfo, storeUtmContent } = this.props;
    const { utmContent } = this.state;
    this.Geolocation = navigator && navigator.geolocation;

    if (token && (!zipCode || !storeUtmContent)) {
      getBillingInfo();
    }
    if (this.Geolocation && !zipCode && !utmContent) {
      this.Geolocation.getCurrentPosition(
        (e) => { this.getCoordinates(e, !token); },
        this.onErrorGetPosition,
      );
    }
  }

  componentDidUpdate(prevProps) {
    const { billingInfo } = this.props;
    const { billingInfo: prevBillingInfo } = prevProps;
    if (
      billingInfo &&
      Object.keys(billingInfo).length &&
      (!prevBillingInfo || !Object.keys(prevBillingInfo).length)
    ) {
      this.checkBillingData();
    }
  }

  componentWillUnmount() {
    this.Geolocation = null;
  }

  getCoordinates = ({ coords }, isAutoZip) => {
    const { latitude: lat, longitude: lng } = coords;
    this.setState({ coordinate: { lat, lng } }, () => {
      if (isAutoZip) {
        this.getNewZipCode();
      }
    });
  };

  onErrorGetPosition = () => {
    const { zipCode, token } = this.props;
    const { utmContent } = this.state;
    if (!zipCode && !utmContent && !token && window.geoip2) {
      try {
          window.geoip2.city(this.getIpZipCode, (e) => console.log('error geoip2', e));
        }
        catch(e) {
          console.log('error geoip2', e);
        }
    }
  }

  getIpZipCode = (data) => {
    const { zipCodes, changeTemporaryUtmContent } = this.props;
    const { zip: stateZip } = this.state;
    const { postal: { code } = {}, subdivisions: [{ names: { en: enName } = {} }] = [{}] } =
      data || {};
    if (code) {
      const newValue = zipCodes.some(({ zip }) => zip === code) ? 'l' : 'n';
      this.setState(
        {
          utmContent: newValue,
          ...(stateZip ? {} : {zip: code}),
        },
        () => {
          changeTemporaryUtmContent(newValue);
        },
      );
    }
    if (!code && enName) {
      this.setState(
        {
          utmContent: enName.toLowerCase() === 'florida' ? 'l' : 'n',
        },
        () => {
          changeTemporaryUtmContent(enName.toLowerCase() === 'florida');
        },
      );
    }
  };

  checkBillingData = () => {
    const {
      billingInfo: { zipCode, state } = {},
      storeUtmContent,
      zipCode: storeZipCode,
      zipCodes,
      changeZip,
      changeUtmContent,
    } = this.props;
    const { zip } = this.state;
    if (!zipCode && !state) {
      if (this.Geolocation && Object.keys(this.Geolocation).length) {
        this.Geolocation.getCurrentPosition((e) => {
          this.getCoordinates(e, true);
        });
      } else if(window.geoip2)  {
        try {
          window.geoip2.city(this.getIpZipCode, () => null);
        }
        catch(e) {
          console.log('error geoip2', e);
        }
      }
    } else {
      const zipValue = zipCode || zip || '';
      const isLocal = zipCodes.some(({ zip: itmZip }) => itmZip === zipValue);
      this.setState(
        {
          ...(!storeZipCode ? { zip: zipValue } : {}),
          ...(!storeUtmContent && zipValue
            ? { utmContent: isLocal || state.toLowerCase() === 'fl' ? 'l' : 'n' }
            : {}),
        },
        () => {
          if (!storeZipCode && zipCode) {
            changeZip(zipCode);
          }
          if (!storeUtmContent && (zipCode || state)) {
            changeUtmContent((zipCode && isLocal) || state.toLowerCase() === 'fl' ? 'l' : 'n');
          }
        },
      );
    }
  };

  getNewZipCode = async () => {
    const { coordinate } = this.state;
    const { zipCodes, changeTemporaryUtmContent } = this.props;
    const code = await getZipCode(coordinate);
    if (code) {
      const newValue = zipCodes.some(({ zip }) => zip === code) ? 'l' : 'n';
      this.setState({ utmContent: newValue, zip: code }, () => {
        changeTemporaryUtmContent(newValue);
      });
    } else {
      this.setState({ zip: '' }, this.onErrorGetPosition);
    }
  };

  openPopup = () => {
    const { isOpen } = this.state;
    this.setState({ isOpen: !isOpen });
  };

  closeButton = () => {
    this.setState({ isShowButton: false });
  };

  clickInputButton = () => {
    window.location.href = ROUTES.contact;
  }

  render() {
    const {
      className,
      iconTitle,
      iconDescription,
      titleFull,
      description,
    } = this.props;

    const { isOpen, isShowButton} = this.state;

    return (
      <div className={b({ mix: className })}>
        {isShowButton && (
          <div className={b('open-block-wrap')}>
            <button className={b('close-btn')} type="button" onClick={this.closeButton}>
              <CloseIcon2 />
            </button>
            <button
              className={b('open-block')}
              id="shipping-target-open-button"
              type="button"
              onClick={this.openPopup}
            >
              <div className={b('icon-block-wrap')}>
                <div className={b('icon-block')}>
                  <div className={b('back-blue-element')} />
                  <div className={b('back-green-element')} />
                  <GeoTagIcon className={b('icon-element')} />
                </div>
              </div>
              <div className={b('text-block')}>
                <span className={b('button-title')}>{iconTitle}</span>
                <span className={b('button-text')}>{iconDescription}</span>
                <span className={b('button-link')}>{BUTTON_LINK}</span>
              </div>
            </button>
          </div>
        )}
        {isOpen &&
          ReactDOM.createPortal(
            <div className={b('content-wrap')}>
              <div className={b('veil')} role="button" tabIndex={-1} onClick={this.openPopup} />
              <div className={b('content')}>
                <button
                  className={b('close-btn', { popup: true })}
                  type="button"
                  onClick={this.openPopup}
                >
                  <CloseIcon2 />
                </button>
                <div className={b('content-block')}>
                  <div className={b('stripe-one')} />
                  <div className={b('stripe-two')} />
                  <div className={b('input-block')}>
                    <span className={b('input-title')}>{titleFull}</span>
                    <span className={b('input-text')}>{description}</span>
                    <span className={b('input-text', { mobile: true })}>{DESCRIPTION_MOBILE}</span>
                    <div className={b('input-block-btn')}>
                      <button
                        className={b('input-btn')}
                        type="button"
                        onClick={this.clickInputButton}
                      >
                        {GET_DIRECTIONS}
                      </button>
                      <span className={b('input-contact')}>{OR_CALL_CONSULTATION}</span>
                      <span className={b('input-contact-phone')}>{PHONE}</span>
                    </div>
                  </div>
                  <div className={b('card')}>
                    <div className={b('photo-art-block')}>
                      <Image
                        className={b('photo-art-block-main-img')}
                        src={mapImg}
                        alt="map"
                        layout="fill"
                      />
                      <div className={b('photo-art-block-img')}>
                        <Image
                          className={b('photo-art-block-img')}
                          src={artImage}
                          alt="art"
                          layout="fill"
                        />
                      </div>
                      <div className={b('photo-art-block-img', { mobile: true })}>
                        <Image
                          className={b('photo-art-block-img')}
                          src={artImageMobile}
                          alt="artMobile"
                          layout="fill"
                        />
                      </div>
                    </div>
                    <div className={b('input-block-btn', { mobile: true })}>
                      <button
                        className={b('input-btn')}
                        type="button"
                        onClick={this.clickInputButton}
                      >
                        {GET_DIRECTIONS}
                      </button>
                      <span className={b('input-contact')}>{OR_CALL_ASSISCTENCE}</span>
                      <span className={b('input-contact-phone')}>{PHONE}</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>,
            document.getElementById('__next') || null,
          )}
      </div>
    );
  }
}

ShippingTargetCreate.propTypes = propTypes;
ShippingTargetCreate.defaultProps = defaultProps;

const stateProps = (state) => ({
  zipCode: zipCodeSelector(state),
  zipCodes: zipCodesSelector(state),
  billingInfo: billingInfoSelect(state),
  token: getToken(state),
  storeUtmContent: storeUtmContentSelector(state),
});

const actions = {
  changeZip: changeZipCode,
  getBillingInfo: getUserBillingInfo,
  changeUtmContent: changeUtmContentActions,
  changeTemporaryUtmContent: changeTemporaryUtmContentActions,
};

export default connect(stateProps, actions)(ShippingTargetCreate);
