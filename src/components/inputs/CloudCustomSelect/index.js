import React, { useState, useEffect, useRef } from 'react';
import bem from 'src/utils/bem';
import incompleteSearch from 'src/utils/incompleteSearch';
import PropTypes from 'prop-types';
import SearchIcon from 'public/icons/search-icon.svg';
import ListIcon from 'public/icons/list-icon.svg';
import Image from 'next/image';
import styles from './index.module.scss';

const b = bem('cloud-custom-select', styles);

const propTypes = {
  className: PropTypes.string,
  options: PropTypes.arrayOf(PropTypes.shape({})),
  value: PropTypes.objectOf(PropTypes.shape({})),
  onChange: PropTypes.func,
  placeholder: PropTypes.string,
  withListBtn: PropTypes.bool,
  withSearchBtn: PropTypes.bool,
  showListByClick: PropTypes.bool,
  title: PropTypes.string,
  readOnly: PropTypes.bool,
  instanceId: PropTypes.string,
};

const defaultProps = {
  className: '',
  placeholder: 'Select...',
  options: [],
  value: null,
  onChange: () => {},
  withListBtn: false,
  withSearchBtn: false,
  showListByClick: false,
  title: '',
  readOnly: false,
  instanceId: '',
};

const CloudCustomSelect = (props) => {
  const wrapper = useRef(null)
  const {
    options,
    className,
    onChange,
    placeholder,
    value,
    withListBtn,
    withSearchBtn,
    showListByClick,
    title,
    readOnly,
    instanceId,
  } = props;

  const [searchInput, setSearchInput] = useState('');
  const [optionsFilter, setOptionsFilter] = useState([]);

  useEffect(() => {
    document.addEventListener('click', () => setOptionsFilter([]));

    return () => {
      document.removeEventListener('click', () => setOptionsFilter([]));
    }
  }, []);

  useEffect(() => {
    if (value) {
      setSearchInput(value.label);
    }
  }, [value]);

  const clickSearchButton = (e) => {
    if (e) {
      e.stopPropagation();
    }
    if (searchInput) {
      const filter =  options.filter(item => incompleteSearch(searchInput, item.label));
      setOptionsFilter(filter);
    }
  }

  const clickListButton = (e) => {
    e.stopPropagation();
    if (optionsFilter.length) {
      setOptionsFilter(optionsFilter)
    } else {
      setOptionsFilter(options)
    }
  }

  const clickItem = (item) => {
    onChange(item);
    setOptionsFilter([]);
  }

  const clickInput = () => {
    setSearchInput('');
  }

  const inputKeyDown = (e) => {
    if (e.keyCode === 13) {
      clickSearchButton();
    }
  }

  return (
    <div className={b({ mix: className })} ref={wrapper}>
      {title && <span className={b('select-title')}>{title}</span>}
      <div
        className={b('select-wrapper', { 'list-opened': optionsFilter.length !== 0 })}
        name={instanceId}
      >
        {withListBtn && (
          <button className={b('list-icon-block')} type="button" onClick={clickListButton}>
            <div className={b('list-icon')}>
              <Image src={ListIcon} layout="fill" />
            </div>
          </button>
        )}
        <input
          className={b('input')}
          placeholder={placeholder}
          value={searchInput}
          onChange={onChange || ((e) => { setSearchInput(e.target.value); })}
          onKeyDown={inputKeyDown}
          onClick={showListByClick ? clickListButton : clickInput}
          readOnly={readOnly}
        />
        {withSearchBtn && (
          <button className={b('search-icon-block')} type="button" onClick={clickSearchButton}>
            <div className={b('search-icon')}>
              <Image src={SearchIcon} layout="fill" />
            </div>
          </button>
        )}
        {optionsFilter.length !== 0 &&
          (
          <div className={b('list')}>
            {optionsFilter.map(item => (
              <button className={b('list-item')} type="button" onClick={() => {clickItem(item)}}>
                {item.label}
              </button>
              )
            )}
          </div>
        )}
      </div>
    </div>
  );
}

CloudCustomSelect.propTypes = propTypes;
CloudCustomSelect.defaultProps = defaultProps;

export default CloudCustomSelect;
