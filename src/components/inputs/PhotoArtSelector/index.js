import React, { Component } from 'react';
import bem from 'src/utils/bem';
import PropTypes from 'prop-types';
import CustomSelect from 'src/components/inputs/CustomSelect';
import debounce from 'lodash/debounce';
import styles from './index.module.scss';

const b = bem('photo-art-selector', styles);

const defaultProps = {
  title: '',
  placeholder: '',
  onChange: () => {},
  value: null,
  options: [],
  className: '',
  inputClass: '',
  titleClass: '',
  containerClass: '',
  isVersionDisabled: false,
  disabled: false,
};

const propTypes = {
  title: PropTypes.string,
  placeholder: PropTypes.string,
  onChange: PropTypes.func,
  options: PropTypes.arrayOf(PropTypes.shape({
    label: PropTypes.string,
  })),
  instanceId: PropTypes.string.isRequired,
  value: PropTypes.shape({}),
  className: PropTypes.string,
  inputClass: PropTypes.string,
  titleClass: PropTypes.string,
  containerClass: PropTypes.string,
  isVersionDisabled: PropTypes.bool,
  disabled: PropTypes.bool,
};

class PhotoArtSelector extends Component {
  resizeWindow = debounce(() => {
    this.setFontSize();
  }, 100);

  constructor(props) {
    super(props);

    this.state = {
      isHintOpen: false,
      stateFontSize: '13px',
    };
  }

  componentDidMount() {
    window.addEventListener('resize', this.resizeWindow);
    this.setFontSize();
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.resizeWindow);
  }

  toggleHint = () => {
    const { isHintOpen } = this.state;
    this.setState({
      isHintOpen: !isHintOpen,
    });
  };

  setFontSize = () => {
    const { stateFontSize } = this.state;
    const newStateFontSize = () => {
      if ((window.innerWidth > 768 && window.innerWidth <= 1024) || window.innerWidth > 1920) {
        return '17px';
      }
      if (window.innerWidth > 1024 && window.innerWidth <= 1920) {
        return '0.88vw';
      }
      return '13px';
    }
    if (stateFontSize !== newStateFontSize()) {
      this.setState({ stateFontSize: newStateFontSize()});
    }
  };

  onMenuOpen = () => {
    this.setState({ menuIsOpened: true })
  }

  onMenuClose = () => {
    this.setState({ menuIsOpened: false })
  }

  render() {
    const {
      title,
      placeholder,
      onChange,
      value,
      options,
      className,
      instanceId,
      inputClass,
      isVersionDisabled,
      disabled,
      titleClass,
      containerClass,
    } = this.props;

    const { stateFontSize, menuIsOpened } = this.state;

    const isHideSelector = isVersionDisabled && options.length < 2;
    return (
      <div className={b({ mix: className })}>
        <div className={b('wrapper')}>
          <span className={b('title', { mix: titleClass })}>{title}</span>
          <div className={b('container', { 'menu-opened': menuIsOpened, mix: containerClass })}>
            {isHideSelector ? (
              <div className={b('selector', { mix: inputClass, mask: true })}>
                <span className={b('text-selector')}>
                  {options.length ? options[0].label : 'None'}
                </span>
              </div>
            ) : (
              <CustomSelect
                options={options}
                className={b('selector', { mix: inputClass })}
                onChange={onChange}
                onMenuOpen={this.onMenuOpen}
                onMenuClose={this.onMenuClose}
                value={value}
                instanceId={instanceId}
                placeholder={placeholder}
                isVersionDisabled={isVersionDisabled}
                disabled={disabled}
                customStyles={{
                  control: (provided) => ({
                    ...provided,
                    minHeight: '30px !important',
                    minWidth: '100%',
                    backgroundColor: 'transparent !important',
                    width: '100%',
                    border: 'none !important',
                    outline: 'none',
                    boxShadow: 'none',
                  }),
                  container: (provided) => ({
                    ...provided,
                    outline: 'none',
                    padding: '0 0 0 10px',
                  }),
                  singleValue: (provided) => ({
                    ...provided,
                    fontSize: stateFontSize,
                  }),
                  menu: (provided) => ({
                    ...provided,
                    marginTop: '1px',
                    borderTopLeftRadius: '0',
                    borderTopRightRadius: '0',
                    borderTop: 'none',
                  }),
                  option: (provided) => ({
                    ...provided,
                    fontSize: stateFontSize,
                  })
                }}
              />
            )}
          </div>
        </div>
      </div>
    );
  }
}

PhotoArtSelector.propTypes = propTypes;
PhotoArtSelector.defaultProps = defaultProps;

export default PhotoArtSelector;
