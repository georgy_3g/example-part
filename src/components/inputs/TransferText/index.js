import React, { Component } from 'react';
import bem from 'src/utils/bem';
import PropTypes from 'prop-types';
import dynamic from "next/dynamic";
import Hint from 'src/components/elements/Hint';
import HintBlock from 'src/components/elements/HintBlock';
import styles from './index.module.scss';

const Toggler = dynamic(() => import('src/components/elements/Toggler'));
const CustomInput = dynamic(() => import('src/components/inputs/CustomInput'));
const b = bem('order-text', styles);
const field = bem('order-field', styles);

const MAX_LENGTH = 24;

const defaultProps = {
  title: '',
  placeholder: '',
  hintText: '',
  isHint: false,
  handleChange: () => {},
  value: '',
  maxLength: MAX_LENGTH,
  hintImages: [],
  className: '',
  isPersonalize: false,
  checked: false,
  price: '',
  personalize: '',
  togglerChange: () => {},
};

const propTypes = {
  title: PropTypes.string,
  placeholder: PropTypes.string,
  isHint: PropTypes.bool,
  hintText: PropTypes.string,
  handleChange: PropTypes.func,
  value: PropTypes.string,
  maxLength: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  hintImages: PropTypes.arrayOf(PropTypes.shape({})),
  className: PropTypes.string,
  isPersonalize: PropTypes.bool,
  checked: PropTypes.bool,
  price: PropTypes.string,
  personalize: PropTypes.string,
  togglerChange: PropTypes.func,
};

class TransferText extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isHintOpen: false,
    };
  }

  toggleHint = () => {
    const { isHintOpen } = this.state;
    this.setState({
      isHintOpen: !isHintOpen,
    });
  };

  render() {
    const {
      title,
      placeholder,
      isHint,
      hintText,
      handleChange,
      value,
      maxLength,
      hintImages,
      className,
      isPersonalize,
      checked,
      price,
      personalize,
      togglerChange,
    } = this.props;
    const { isHintOpen } = this.state;
    return (
      <div className={b('wrapper', { mix: className })}>
        {isPersonalize ? (
          <div className={b({ mix: field({ 'personalize-text': !checked, text: checked }) })}>
            <div className={b('container', { mix: field() })}>
              <div className={b('text-container', { mix: field('text-container') })}>
                <h4 className={b('title', { mix: field('title') })}>{title}</h4>
                <p className={b('price', { mix: field('price') })}>{price}</p>
              </div>
              <div className={b('count-controls')}>
                <div className={b('input-wrap')}>
                  <Toggler checked={checked} togglerChange={togglerChange(personalize)} />
                </div>
                {isHint && <Hint toggleHint={this.toggleHint} />}
              </div>
            </div>
            {checked && (
              <CustomInput
                className={b('input')}
                placeholder={placeholder}
                onChange={handleChange}
                value={value}
                maxLength={maxLength}
              />
            )}
          </div>
        ) : (
          <div className={b({ mix: field({ text: true }) })}>
            <div className={b('container')}>
              <h4 className={b('title', { mix: field('title') })}>{title}</h4>
              {isHint && <Hint toggleHint={this.toggleHint} />}
            </div>
            <CustomInput
              className={b('input')}
              placeholder={placeholder}
              onChange={handleChange}
              value={value}
              maxLength={maxLength}
            />
          </div>
        )}
        {isHintOpen && <HintBlock text={hintText} hintImages={hintImages} name={title} />}
      </div>
    );
  }
}

TransferText.propTypes = propTypes;
TransferText.defaultProps = defaultProps;

export default TransferText;
