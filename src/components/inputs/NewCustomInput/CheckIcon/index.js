import React from 'react';
import PropTypes from 'prop-types';

const propTypes = {
  fill: PropTypes.string,
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
  stroke: PropTypes.string,
};

const defaultProps = {
  fill: 'none',
  className: '',
  width: '13',
  height: '10',
  stroke: '#123C55',
};

function CheckIcon({
  width,
  height,
  fill,
  className,
  stroke,
}) {
  return (
    <svg className={className} width={width} height={height} viewBox="0 0 13 10">
      <path fill={fill} fillRule="evenodd" stroke={stroke} strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M1 4.606l3.606 3.606L11.818 1" />
    </svg>
  );
}

CheckIcon.propTypes = propTypes;
CheckIcon.defaultProps = defaultProps;

export default CheckIcon;
