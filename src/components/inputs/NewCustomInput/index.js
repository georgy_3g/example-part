import React from 'react';
import PropTypes from 'prop-types';
import InputMask from 'react-input-mask';
import bem from 'src/utils/bem';
import styles from './index.module.scss';

const b = bem('new-custom-input', styles);

const propTypes = {
  className: PropTypes.string,
  type: PropTypes.string,
  value: PropTypes.string,
  maxLength: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  name: PropTypes.string,
  onChange: PropTypes.func,
  placeholder: PropTypes.string,
  error: PropTypes.string,
  isValid: PropTypes.bool,
  isTouched: PropTypes.bool,
  withMask: PropTypes.bool,
  mask: PropTypes.string,
  inputClass: PropTypes.string,
  disabled: PropTypes.bool,
  id: PropTypes.string,
  withIcon: PropTypes.bool,
  backGroundImage: PropTypes.string,
};

const defaultProps = {
  className: '',
  type: 'text',
  placeholder: '',
  maxLength: '',
  name: null,
  value: null,
  onChange: null,
  error: '',
  isValid: false,
  isTouched: false,
  withMask: false,
  mask: '',
  inputClass: '',
  disabled: false,
  id: 'null',
  withIcon: false,
  backGroundImage: '',
};

function NewCustomInput(props) {
  const {
    name,
    className,
    onChange,
    placeholder,
    value,
    error,
    isValid,
    isTouched,
    type,
    maxLength,
    withMask,
    mask,
    inputClass,
    disabled,
    id,
    withIcon,
    backGroundImage,
  } = props;

  const isInvalid = !isValid && isTouched;
  return (
    <div className={b({ mix: className })}>
      {withMask ? (
        <InputMask
          id={id}
          className={b('input', {
            mix: inputClass,
            'red-border': isInvalid,
            'card-icon': withIcon,
          })}
          type={type}
          onChange={onChange}
          name={name}
          placeholder={placeholder}
          value={value}
          maxLength={maxLength}
          mask={mask}
          disabled={disabled}
          style={{
            backgroundImage: `url(${withIcon ? backGroundImage : ''})`,
          }}
        />
      ) : (
        <input
          className={b('input', {
            mix: inputClass,
            'red-border': isInvalid,
            'card-icon': withIcon,
          })}
          id={id}
          type={type}
          onChange={onChange}
          name={name}
          placeholder={placeholder}
          value={value}
          maxLength={maxLength}
          disabled={disabled}
          style={{
            backgroundImage: `url(${withIcon ? backGroundImage : ''})`,
          }}
        />
      )}
      {isInvalid && <div className={b('error')}>{error}</div>}
    </div>
  );
}

NewCustomInput.propTypes = propTypes;
NewCustomInput.defaultProps = defaultProps;

export default NewCustomInput;
