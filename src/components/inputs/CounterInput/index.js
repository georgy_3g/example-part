import React, { Component } from 'react';
import bem from 'src/utils/bem';
import PropTypes from 'prop-types';
import Hint from 'src/components/elements/Hint';
import HintBlock from 'src/components/elements/HintBlock';
import styles from './index.module.scss';

const b = bem('counter', styles);
const field = bem('order-field', styles);
const btn = bem('btn', styles);

const BUTTON_TYPE = {
  dec: false,
  inc: true,
};

const defaultProps = {
  className: '',
  title: '',
  price: '',
  hintText: '',
  isHint: false,
  hintImages: [],
  withoutDecInc: false,
  isButtonCountDisabled: false,
  mainCounter: false,
  withInput: false,
  onChange: () => {},
  bemCount: () => {},
  withReplacement: false,
  count: null,
  min: 0,
  changeCount: () => () => {},
};

const propTypes = {
  className: PropTypes.string,
  title: PropTypes.string,
  price: PropTypes.string,
  count: PropTypes.number,
  changeCount: PropTypes.func,
  isHint: PropTypes.bool,
  hintText: PropTypes.string,
  hintImages: PropTypes.arrayOf(PropTypes.shape({})),
  withoutDecInc: PropTypes.bool,
  isButtonCountDisabled: PropTypes.bool,
  mainCounter: PropTypes.bool,
  withInput: PropTypes.bool,
  onChange: PropTypes.func,
  bemCount: PropTypes.func,
  withReplacement: PropTypes.bool,
  min: PropTypes.number,
};

class CounterInput extends Component {
  constructor(props) {
    super(props);
    const { min } = props;

    this.state = {
      isHintOpen: false,
      isFirstEnter: true,
      prevValue: min,
    };
  }

  toggleHint = () => {
    const { isHintOpen } = this.state;
    this.setState({
      isHintOpen: !isHintOpen,
    });
  };

  onInput = (e, withReplacement) => {
    const { value } = e.target;
    const { onChange, count, min } = this.props;
    const { isFirstEnter } = this.state;
    const valueString = String(value);
    const countString = String(count);
    if (
      withReplacement &&
      isFirstEnter &&
      valueString.length >= countString.length &&
      valueString.length <= 5
    ) {
      const newValue = countString
        .split('')
        .reduce((acc, item) => acc.replace(String(item), ''), valueString);
      onChange(Number(newValue) ? Number(newValue) : min);
    }
    if (!withReplacement || !isFirstEnter || value === '') {
      if (valueString && valueString.length > 5 && value <= min) {
        onChange(min);
      } else {
        onChange(value === '' ? null : Number(value) || min);
      }
    }
    this.setState({ isFirstEnter: false });
  };

  onFocus = () => {
    const { count, onChange, withReplacement } = this.props;
    if (withReplacement) {
      this.setState({ prevValue: count, isFirstEnter: false });
      onChange(null);
    }
  };

  onBlur = () => {
    const { count, onChange, min } = this.props;
    const { prevValue } = this.state;
    this.setState({ isFirstEnter: true });
    if (count < min || count === null || Number.isNaN(Number(count))) {
      onChange(prevValue > min ? prevValue : min);
    }
  };

  render() {
    const {
      title,
      price,
      count,
      changeCount,
      isHint,
      hintText,
      hintImages,
      withoutDecInc,
      className,
      isButtonCountDisabled,
      mainCounter,
      withInput,
      bemCount,
      withReplacement,
    } = this.props;
    const { isHintOpen } = this.state;
    return (
      <div className={b('wrapper', { mix: className })}>
        <div className={b({ mix: field() })}>
          <div className={b('text-container', { mix: field('text-container') })}>
            <h4 className={b('title', { mix: field('title') })}>{title}</h4>
            <p className={b('price', { mix: field('price') })}>{price}</p>
          </div>
          <div className={b('controls', { mix: bemCount('controls') })}>
            {!withoutDecInc && (
              <button
                className={b('btn-count', { mix: btn({ minus: true }), main: mainCounter })}
                type="button"
                onClick={changeCount(BUTTON_TYPE.dec)}
                disabled={isButtonCountDisabled}
              />
            )}
            {withInput ? (
              <input
                className={b('input', { main: mainCounter })}
                value={count !== null ? count : ''}
                type="text"
                onChange={(e) => {
                  this.onInput(e, withReplacement);
                }}
                onBlur={this.onBlur}
                onFocus={this.onFocus}
                max="99999"
              />
            ) : (
              <span className={b('count', { main: mainCounter })}>{count}</span>
            )}
            {!withoutDecInc && (
              <button
                className={b('btn-count', { mix: btn({ plus: true }), main: mainCounter })}
                type="button"
                onClick={changeCount(BUTTON_TYPE.inc)}
              />
            )}
            {isHint && <Hint toggleHint={this.toggleHint} />}
          </div>
        </div>
        {isHintOpen && <HintBlock text={hintText} hintImages={hintImages} name={title} />}
      </div>
    );
  }
}

CounterInput.propTypes = propTypes;
CounterInput.defaultProps = defaultProps;

export default CounterInput;
