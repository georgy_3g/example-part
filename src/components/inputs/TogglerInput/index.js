import React, { Component } from 'react';
import bem from 'src/utils/bem';
import PropTypes from 'prop-types';
import dynamic from "next/dynamic";
import Hint from 'src/components/elements/Hint';
import HintBlock from 'src/components/elements/HintBlock';
import styles from './index.module.scss';

const Toggler = dynamic(() => import('src/components/elements/Toggler'));

const b = bem('toggler', styles);
const field = bem('order-field', styles);

const defaultProps = {
  title: '',
  price: '',
  hintText: '',
  isHint: false,
  checked: false,
  togglerChange: () => {},
  hintImages: [],
};

const propTypes = {
  title: PropTypes.string,
  price: PropTypes.string,
  isHint: PropTypes.bool,
  hintText: PropTypes.string,
  checked: PropTypes.bool,
  togglerChange: PropTypes.func,
  hintImages: PropTypes.arrayOf(PropTypes.shape({})),
};

class TogglerInput extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isHintOpen: false,
    };
  }

  toggleHint = () => {
    const { isHintOpen } = this.state;
    this.setState({
      isHintOpen: !isHintOpen,
    });
  };

  render() {
    const { title, price, isHint, hintText, checked, togglerChange, hintImages } = this.props;
    const { isHintOpen } = this.state;
    return (
      <div className={b('wrapper')}>
        <div className={b({ mix: field() })}>
          <div className={b('text-container', { mix: field('text-container') })}>
            <h4 className={b('title', { mix: field('title') })}>{title}</h4>
            <p className={b('price', { mix: field('price') })}>{price}</p>
          </div>
          <div className={b('count-controls')}>
            <div className={b('input-wrap')}>
              <Toggler checked={checked} togglerChange={togglerChange} />
            </div>
            {isHint && <Hint toggleHint={this.toggleHint} />}
          </div>
        </div>
        {isHintOpen && <HintBlock text={hintText} hintImages={hintImages} name={title} />}
      </div>
    );
  }
}

TogglerInput.propTypes = propTypes;
TogglerInput.defaultProps = defaultProps;

export default TogglerInput;
