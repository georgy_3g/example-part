import React from 'react';
import PropTypes from 'prop-types';
import InputMask from 'react-input-mask';
import bem from 'src/utils/bem';
import IconX from './IconX';
import CheckIcon from './CheckIcon';

import styles from './index.module.scss';

const b = bem('custom-input', styles);

const propTypes = {
  className: PropTypes.string,
  type: PropTypes.string,
  value: PropTypes.string,
  maxLength: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  name: PropTypes.string,
  onChange: PropTypes.func,
  placeholder: PropTypes.string,
  error: PropTypes.string,
  isValid: PropTypes.bool,
  isTouched: PropTypes.bool,
  withMask: PropTypes.bool,
  mask: PropTypes.string,
  inputClass: PropTypes.string,
  disabled: PropTypes.bool,
  id: PropTypes.string,
};

const defaultProps = {
  className: '',
  type: 'text',
  placeholder: '',
  maxLength: '',
  name: null,
  value: null,
  onChange: null,
  error: '',
  isValid: false,
  isTouched: false,
  withMask: false,
  mask: '',
  inputClass: '',
  disabled: false,
  id: 'null',
};

function CustomInput(props) {
  const {
    name,
    className,
    onChange,
    placeholder,
    value,
    error,
    isValid,
    isTouched,
    type,
    maxLength,
    withMask,
    mask,
    inputClass,
    disabled,
    id,
  } = props;

  const beforeMaskedValueChange = (newState) => {
    const { value: newValue, selection } = newState;
    if (newValue !== value) {
      onChange({ target: { name, value: newValue } });
    }
    return {
      value: newValue,
      selection,
    };
  };

  const isInvalid = !isValid && isTouched;
  const Icon = isValid ? CheckIcon : IconX;
  return (
    <div className={b({ mix: className })}>
      {withMask ? (
        <InputMask
          id={id}
          className={b('input', { mix: inputClass, 'red-border': isInvalid })}
          type={type}
          onChange={onChange}
          name={name}
          placeholder={placeholder}
          value={value}
          maxLength={maxLength}
          mask={mask}
          disabled={disabled}
          beforeMaskedValueChange={beforeMaskedValueChange}
        />
      ) : (
        <input
          className={b('input', { mix: inputClass, 'red-border': isInvalid })}
          id={id}
          type={type}
          onChange={onChange}
          name={name}
          placeholder={placeholder}
          value={value}
          maxLength={maxLength}
          disabled={disabled}
        />
      )}
      {isTouched && <Icon className={b('icon')} />}
      {isInvalid && <div className={b('error')}>{error}</div>}
    </div>
  );
}

CustomInput.propTypes = propTypes;
CustomInput.defaultProps = defaultProps;

export default CustomInput;
