import React from 'react';
import bem from 'src/utils/bem';
import PropTypes from 'prop-types';
import styles from './index.module.scss';

const b = bem('new-toggler-input', styles);

const defaultProps = {
  onText: 'Yes',
  offText: 'No',
  checked: false,
  disabled: false,
  onChange: () => {},
  className: '',
  inputClassName: '',
  textClass: '',
  withoutText: false,
  minSize: false,
};

const propTypes = {
  checked: PropTypes.bool,
  onText: PropTypes.string,
  offText: PropTypes.string,
  disabled: PropTypes.bool,
  onChange: PropTypes.func,
  className: PropTypes.string,
  inputClassName: PropTypes.string,
  withoutText: PropTypes.bool,
  minSize: PropTypes.bool,
  textClass: PropTypes.string,
};

function NewToggler(props) {
  const {
    onChange,
    onText,
    offText,
    disabled,
    checked,
    className,
    inputClassName,
    withoutText,
    minSize,
    textClass,
  } = props;

  const handleChange = () => {
    if (!disabled) {
      onChange(!checked);
    }
  };

  return (
    <div className={b({ mix: className })}>
      {!withoutText && <div className={b('left-text', { mix: textClass, checked: !checked })}>{offText}</div>}
      <div
        className={b('wrap', {
          mix: inputClassName,
          disabled,
          checked,
          'min-size': minSize,
        })}
        onClick={handleChange}
        role="button"
        tabIndex="0"
      >
        <input type="checkbox" disabled={disabled} checked={checked} onChange={handleChange} />
      </div>
      {!withoutText && <div className={b('right-text', {mix: textClass, checked })}>{onText}</div>}
    </div>
  );
}

NewToggler.defaultProps = defaultProps;
NewToggler.propTypes = propTypes;

export default NewToggler;
