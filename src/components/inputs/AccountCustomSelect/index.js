import React from 'react';
import bem from 'src/utils/bem';
import useMobileStyle from 'src/utils/useMobileStyle';
import PropTypes from 'prop-types';
import ReactSelect from 'react-select';
import colors from 'src/styles/colors.json';
import DropdownIndicator from './DropdownIndicator';
import styles from './index.module.scss';

const b = bem('account-custom-select', styles);

const propTypes = {
  className: PropTypes.string,
  options: PropTypes.arrayOf(PropTypes.shape({})),
  value: PropTypes.objectOf(PropTypes.shape({})),
  onChange: PropTypes.func,
  placeholder: PropTypes.string,
  instanceId: PropTypes.string,
  isTouched: PropTypes.bool,
  isValid: PropTypes.bool,
  error: PropTypes.string,
  round: PropTypes.bool,
  isOpen: PropTypes.bool,
  selectClass: PropTypes.string,
  isVersionDisabled: PropTypes.bool,
  isSearchable: PropTypes.bool,
};

const defaultProps = {
  className: '',
  placeholder: 'Select...',
  options: [],
  value: null,
  onChange: null,
  isTouched: false,
  isValid: false,
  error: 'This field should be filled',
  round: false,
  instanceId: '',
  isOpen: false,
  selectClass: '',
  isVersionDisabled: false,
  isSearchable: false,
};

const AccountCustomSelect = (props) => {
  const {
    options,
    className,
    onChange,
    placeholder,
    instanceId,
    value,
    isTouched,
    isValid,
    error,
    round,
    isOpen,
    selectClass,
    isVersionDisabled,
    isSearchable,
  } = props;

  const isInvalid = !isValid && isTouched;

  const isBig = useMobileStyle(1920);
  const isSmall = useMobileStyle(1024);

  const isMinMax = isBig && !isSmall;

  const components = {
    IndicatorSeparator: null,
    DropdownIndicator,
  };

  const customStyles = {
    menu: (provided) => ({
      ...provided,
      marginTop: isMinMax ? '0.52vw' : '10px',
      left: '0',
      padding: isMinMax ? '0.26vw' : '5px',
      backgroundColor: `${colors['$catskill-white-color']}`,
      borderRadius: isMinMax ? '0.72vw' : '14px',
      border: `1px solid ${colors['$athens-gray-color']}`,
      boxShadow: '0 2px 4px 0 rgba(#000, 0.1)',
    }),
    menuList: (provided) => ({
      ...provided,
      maxHeight: isMinMax ? '6.51vw' : '125px',
      marginRight: isMinMax ? '0.2vw' : '4px',
      flexDirection: 'column',
      width: '100%',
      scrollbarColor: `${colors['$cadet-blue-color']} ${colors['$athens-gray-color']}`,
      ...(round
        ? {
            borderBottomRightRadius: isMinMax ? '1.04vw' : '17px',
            borderBottomLeftRadius: isMinMax ? '1.3vw' : '25px',
          }
        : {}),
    }),
    valueContainer: (provided) => ({
      ...provided,
      padding: '2px 0',
    }),
    singleValue: (provided) => ({
      ...provided,
      fontWeight: '500',
      fontSize: isMinMax ? '0.88vw' : '1.0625rem',
      lineHeight: isMinMax ? '1.66vw' : '32px',
      letterSpacing: '1px',
      color: `${colors['$pickled-bluewood-color']}`,
      margin: '0',
    }),
    placeholder: (provided) => ({
      ...provided,
      fontSize: isMinMax ? '0.88vw' : '1.0625rem',
      color: `${colors['$darker-cadet-blue-color']}`,
      fontWeight: '500',
      lineHeight: isMinMax ? '1.66vw' : '32px',
      letterSpacing: '1px',
    }),
    option: (provided, state) => ({
      ...provided,
      display: 'flex',
      justifyContent: 'center',
      fontSize: isMinMax ? '0.72vw' : '0.875rem',
      color: `${colors['$dark-slate-blue-color']}`,
      fontWeight: '500',
      backgroundColor: state.isFocused ? `${colors['$wild-sand-color']}` : 'transparent',
      borderRadius: isMinMax ? '0.52vw' : '10px',
      ...(round ? { borderRadius: isMinMax ? '1.3vw' : '25px' } : {}),
    }),
    control: (provided) => ({
      ...provided,
      borderRadius: isMinMax ? '0.72vw' : '14px',
      border: `1px solid ${colors['$athens-gray-color']}`,
      borderColor: `${colors['$athens-gray-color']} !important`,
      backgroundColor: `${colors['$catskill-white-color']}`,
      height: isMinMax ? '3.12vw' : '60px',
      maxHeight: '100%',
      padding: isMinMax ? '0 0.83vw 0 1.04vw' : '0 16px 0 20px',
      fontSize: isMinMax ? '0.78vw' : '0.95rem',
      boxShadow: 'none !important',
    }),
  };

  return (
    <div className={b({ mix: className, 'red-border': isInvalid })}>
      <ReactSelect
        options={options}
        className={b('select', { round, mix: selectClass })}
        classNamePrefix={b('select')}
        onChange={onChange}
        components={components}
        placeholder={placeholder}
        instanceId={instanceId}
        value={value}
        styles={round && customStyles}
        defaultMenuIsOpen={isOpen}
        isSearchable={isSearchable}
        isDisabled={isVersionDisabled && options.length < 2}
      />
      {!isValid && isTouched && <div className={b('error')}>{error}</div>}
    </div>
  );
}

AccountCustomSelect.propTypes = propTypes;
AccountCustomSelect.defaultProps = defaultProps;

export default AccountCustomSelect;
