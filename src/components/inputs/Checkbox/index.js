import React from 'react';
import PropTypes from 'prop-types';
import bem from 'src/utils/bem';
import Colors from 'src/styles/colors.json';

import dynamic from "next/dynamic";
import styles from './index.module.scss';

const CheckedIcon = dynamic(() => import('src/components/svg/CheckedIcon'));

const b = bem('checkbox', styles);
const pickledBluewoodColor = Colors['$pickled-bluewood-color'];

const defaultProps = {
  className: '',
  id: '',
  checked: false,
  bemParents: () => {},
  onChange: () => {},
  label: '',
};

const propsTypes = {
  className: PropTypes.string,
  id: PropTypes.string,
  checked: PropTypes.bool,
  bemParents: PropTypes.func,
  onChange: PropTypes.func,
  label: PropTypes.string,
};

function Checkbox(props) {
  const { className, id, bemParents, label, checked, onChange } = props;
  return (
    <label htmlFor={id} className={b({ mix: className })}>
      <input
        id={id}
        className={b('input')}
        name={id}
        type="checkbox"
        onChange={onChange}
        checked={checked}
      />
      <CheckedIcon
        className="checkbox__checkbox-svg"
        markStroke={checked ? pickledBluewoodColor : 'none'}
      />
      <span className={b('label', { mix: bemParents('check-text') })}>{label}</span>
    </label>
  );
}

Checkbox.defaultProps = defaultProps;
Checkbox.propTypes = propsTypes;

export default Checkbox;
