import React, { useState } from 'react';
import bem from 'src/utils/bem';
import useMobileStyle from 'src/utils/useMobileStyle';
import PropTypes from 'prop-types';
import ReactSelect from 'react-select';
import colors from 'src/styles/colors.json';
import DropdownIndicator from './DropdownIndicator';
import styles from './index.module.scss';

const b = bem('custom-select', styles);

const propTypes = {
  className: PropTypes.string,
  options: PropTypes.arrayOf(PropTypes.shape({})),
  value: PropTypes.shape({}),
  onChange: PropTypes.func,
  onMenuOpen: PropTypes.func,
  onMenuClose: PropTypes.func,
  placeholder: PropTypes.string,
  instanceId: PropTypes.string,
  isTouched: PropTypes.bool,
  isValid: PropTypes.bool,
  error: PropTypes.string,
  round: PropTypes.bool,
  isOpen: PropTypes.bool,
  selectClass: PropTypes.string,
  isVersionDisabled: PropTypes.bool,
  isSearchable: PropTypes.bool,
  disabled: PropTypes.bool,
  id: PropTypes.string,
  customStyles: PropTypes.shape({}),
  isDropDownIndicator: PropTypes.bool,
};

const defaultProps = {
  className: '',
  placeholder: 'Select...',
  options: [],
  value: null,
  onChange: null,
  onMenuOpen: null,
  onMenuClose: null,
  isTouched: false,
  isValid: false,
  error: 'This field should be filled',
  round: false,
  instanceId: '',
  isOpen: false,
  selectClass: '',
  isVersionDisabled: false,
  isSearchable: false,
  disabled: false,
  id: null,
  customStyles: {},
  isDropDownIndicator: true,
};

function CustomSelect(props) {
  const {
    options,
    className,
    onChange,
    onMenuOpen,
    onMenuClose,
    placeholder,
    instanceId,
    value,
    isTouched,
    isValid,
    error,
    round,
    isOpen,
    selectClass,
    isVersionDisabled,
    isSearchable,
    disabled,
    id,
    customStyles,
    isDropDownIndicator,
  } = props;

  const [menuIsOpened, setMenuOpened] = useState(false);

  const isBig = useMobileStyle(1920);
  const isSmall = useMobileStyle(1024);

  const isMinMax = isBig && !isSmall;

  const isInvalid = !isValid && isTouched;

  const components = {
    IndicatorSeparator: null,
    DropdownIndicator: isDropDownIndicator ? DropdownIndicator : null,
  };

  const {
    menu: menuStyle = () => { return {} },
    menuList: menuListStyle = () => { return {} },
    valueContainer: valueContainerStyle = () => { return {} },
    singleValue: singleValueStyle = () => { return {} },
    placeholder: placeholderStyle = () => { return {} },
    option: optionStyle = () => { return {} },
    control: controlStyle = () => { return {} },
    ...rest
  } = customStyles || {};

  const allStyles = {
    menu: (provided, state) => ({
      ...provided,
      left: '0 !important',
      padding: isMinMax ? '0.26vw' : '5px',
      backgroundColor: `${colors['$catskill-white-color']}`,
      borderRadius: isMinMax ? '0.72vw' : '14px',
      border: `1px solid ${colors['$athens-gray-color']}`,
      boxShadow: '0 2px 4px 0 rgba(#000, 0.1)',
      marginTop: '1px',
      borderTopLeftRadius: '0',
      borderTopRightRadius: '0',
      borderTop: 'none',
      ...(round ? { borderBottomRightRadius: '25px !important' } : {}),
      ...(round ? { borderBottomLeftRadius: '25px !important' } : {}),
      ...menuStyle({}, state),
    }),
    menuList: (provided, state) => ({
      ...provided,
      maxHeight: '125px',
      marginRight: '4px',
      scrollbarColor: `${colors['$cadet-blue-color']} ${colors['$athens-gray-color']}`,
      ...(round
        ? {
            borderBottomRightRadius: isMinMax ? '1.04vw' : '17px',
            borderBottomLeftRadius: isMinMax ? '1.3vw' : '25px',
          }
        : {}),
      ...menuListStyle({}, state),
    }),
    valueContainer: (provided, state) => ({
      ...provided,
      padding: '2px 0',
      ...valueContainerStyle({}, state),
    }),
    singleValue: (provided, state) => ({
      ...provided,
      fontWeight: '500',
      fontSize: isMinMax ? '0.88vw' : '1.0625rem',
      lineHeight: isMinMax ? '1.66vw' : '32px',
      letterSpacing: '1px',
      color: `${colors['$pickled-bluewood-color']}`,
      margin: '0',
      ...singleValueStyle({}, state),
    }),
    placeholder: (provided, state) => ({
      ...provided,
      fontSize: isMinMax ? '0.88vw' : '1.0625rem',
      color: `${colors['$darker-cadet-blue-color']}`,
      fontWeight: '500',
      lineHeight: '32px',
      letterSpacing: '1px',
      ...placeholderStyle({}, state),
    }),
    option: (provided, state) => ({
      ...provided,
      fontSize: isMinMax ? '0.88vw' : '1.0625rem',
      color: `${colors['$dark-slate-blue-color']}`,
      fontWeight: '500',
      backgroundColor: state.isFocused ? `${colors['$wild-sand-color']}` : 'transparent',
      borderRadius: '10px',
      ...(round ? { borderRadius: '25px !important' } : {}),
      ...optionStyle({}, state),
    }),
    control: (provided, state) => ({
      ...provided,
      boxShadow: 'none',
      minHeight: '30px',
      ...(round
        ? {
            border: 'none',
            backgroundColor: 'transparent',
            borderRadius: isMinMax ? '1.3vw' : '25px',
          }
        : {}),
      ...(isInvalid
        ? {
            borderColor: `${colors['$burnt-sienna-color']} !important`,
            minHeight: '30px',
          }
        : {}),
      ...controlStyle({}, state),
    }),
    ...(rest || {}),
  };

  return (
    <div className={b({ 'menu-opened': menuIsOpened, mix: className, 'red-border': isInvalid })}>
      <ReactSelect
        inputId={id}
        options={options}
        className={b('select', { round, mix: selectClass })}
        classNamePrefix={b('select')}
        onChange={onChange}
        onMenuOpen={onMenuOpen || (() => setMenuOpened(true))}
        onMenuClose={onMenuClose || (() => setMenuOpened(false))}
        components={components}
        placeholder={placeholder}
        instanceId={instanceId}
        value={value}
        styles={allStyles}
        defaultMenuIsOpen={isOpen}
        isSearchable={isSearchable}
        isDisabled={(isVersionDisabled && options.length < 2) || disabled}
        dropdownIndicator={false}
      />
      {!isValid && isTouched && <div className={b('error')}>{error}</div>}
    </div>
  );
}

CustomSelect.propTypes = propTypes;
CustomSelect.defaultProps = defaultProps;

export default CustomSelect;
