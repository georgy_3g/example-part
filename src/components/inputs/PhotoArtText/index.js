import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';
import bem from 'src/utils/bem';
import PropTypes from 'prop-types';
import colors from 'src/styles/colors.json';
import dynamic from 'next/dynamic';
import { isShowPersonalizeSelector } from 'src/redux/photoArt/selectors';
import {
  clearPersonalizeAction,
  selectPersonalizeAction,
  togglePersonalizePopupAction,
} from 'src/redux/photoArt/actions';
import styles from './index.module.scss';

const CustomInput = dynamic(() => import('src/components/inputs/CustomInput'));
const Toggler = dynamic(() => import('src/components/elements/Toggler'));
const PersonalizePopup = dynamic(() => import('src/components/popups/PersonalizePopup'));
const EyeIcon = dynamic(() => import('src/components/svg/EyeIcon'));

const darkBlue = colors['$dark-slate-blue-color'];

const b = bem('photo-art-text', styles);

const TOP_TEXT = 'Use the service';
const INFO_TEXT = 'Enter your engraving text';

const defaultProps = {
  className: '',
  title: '',
  placeholder: '',
  handleChange: () => {},
  value: '',
  prise: 10,
  changeToggler: () => {},
  isPersonalizeOn: false,
  position: {},
  disabled: false,
  options: {},
  disableFormElements: () => {},
  selectedOption: {},
  selectPersonalize: () => {},
  clearPersonalize: () => {},
  isShowPersonalize: false,
  togglePersonalizePopup: () => {},
  displayValue: null,
};

const propTypes = {
  className: PropTypes.string,
  title: PropTypes.string,
  placeholder: PropTypes.string,
  handleChange: PropTypes.func,
  value: PropTypes.string,
  prise: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  changeToggler: PropTypes.func,
  isPersonalizeOn: PropTypes.bool,
  position: PropTypes.shape({
    photoBottom: PropTypes.number,
  }),
  disabled: PropTypes.bool,
  options: PropTypes.shape({
    selectedOption: PropTypes.shape({}),
  }),
  disableFormElements: PropTypes.func,
  selectedOption: PropTypes.shape({}),
  selectPersonalize: PropTypes.func,
  clearPersonalize: PropTypes.func,
  isShowPersonalize: PropTypes.bool,
  togglePersonalizePopup: PropTypes.func,
  displayValue: PropTypes.shape({
    id: PropTypes.number,
  }),
};

class PhotoArtText extends Component {
  constructor(props) {
    super(props);

    const { displayValue } = props;

    const { frameDisplaysPersonalize = [] } = displayValue || {};

    const personalizeData = (frameDisplaysPersonalize || [])
      .find(item => item && item.isPersonalizeActive);

    const isPersonalize = Boolean(personalizeData);

    this.state = {
      personalizeData: {...personalizeData},
      isPersonalize,
    };
  }

  componentDidUpdate(prevProps) {
    const { displayValue } = this.props;
    const { displayValue: prevDisplay } = prevProps;
    const { id } = displayValue;
    const { id: prevId } = prevDisplay;
    if (id !== prevId) {
      this.updatePersonalizeData();
    }
  }

  updatePersonalizeData = () => {
    const { displayValue } = this.props;

    const { frameDisplaysPersonalize = [] } = displayValue || {};

    const personalizeData = (frameDisplaysPersonalize || [])
      .find(item => item && item.isPersonalizeActive);

    const isPersonalize = Boolean(personalizeData);

    this.setState({
      personalizeData: {...personalizeData},
      isPersonalize,
    });
  };

  showPopup = () => {
    const { disableFormElements, isShowPersonalize, togglePersonalizePopup } = this.props;
    togglePersonalizePopup(!isShowPersonalize);
    disableFormElements(!isShowPersonalize);
  };

  setBreakToString = (string) => {
    const { selectedOption: { personalizeImage = {} } = {} } = this.props;
    const { character_limit: limit } = personalizeImage;

    const text = string.split(' ').reduce(
      (acc, item) => {
        const { value, length, isSecondLine } = acc;
        if (length + item.length + 1 < limit || isSecondLine) {
          return {
            value: length === 0 ? item : `${value} ${item}`,
            length: length === 0 ? length + item.length : length + item.length + 1,
            isSecondLine,
          };
        }
        return {
          value: `${value}\n${item}`,
          length: length + item.length + 1,
          isSecondLine: true,
        };
      },
      { value: '', length: 0, isSecondLine: false },
    );
    return text.value;
  };

  onInputChange = (e, index) => {
    const { handleChange, value: oldValue } = this.props;
    const { personalizeData } = this.state;

    const { characterLimit, lineLimit } = personalizeData;

    const { value } = e.target;

    const values = oldValue.split('\n');
    if (value.length > characterLimit) {
      return null;
    }

    if (values.length !== lineLimit) {
      let newValue = '';

      for (let i = 0; i < lineLimit; i += 1) {
        newValue = `${newValue}${i === index ? value : ''}`;
        newValue = `${newValue}${i !== lineLimit - 1 ? '\n' : ''}`;
      }
      handleChange({ ...e, target: { ...e.target, value: newValue } });
    } else {
      const newValue = values.reduce(
        (acc, item, number) =>
          `${acc}${number !== 0 ? '\n' : ''}${number === index ? value : item}`,
        '',
      );
      handleChange({ ...e, target: { ...e.target, value: newValue } });
    }

    return null;
  };

  updateToggle = (value) => {
    const { changeToggler, selectPersonalize, clearPersonalize, togglePersonalizePopup } =
      this.props;
    const { personalizeData } = this.state;
    changeToggler(value);
    if (value) {
      selectPersonalize(personalizeData);
    } else {
      clearPersonalize();
      togglePersonalizePopup(false);
    }
  };

  generateInputsData = (personalizeData) => {
    const { placeholder, value } = this.props;
    const values = value.split('\n');
    const { characterLimit, lineLimit } = personalizeData;

    const inputs = Array.from({ length: lineLimit }, (item, i) => (
      <CustomInput
        className={b('input-block')}
        inputClass={b('input')}
        placeholder={placeholder}
        onChange={(e) => {
          this.onInputChange(e, i);
        }}
        value={values[i]}
        maxLength={characterLimit}
      />
    ));

    return inputs;
  };

  render() {
    const {
      className,
      title,
      value,
      prise,
      isPersonalizeOn,
      disabled,
      selectedOption,
      isShowPersonalize,
    } = this.props;

    const { personalizeData, isPersonalize } = this.state;

    return (
      <div className={b({ mix: className })}>
        <div className={b('title-block')}>
          <span className={b('title')}>{title}</span>
          <span className={b('price', { active: isPersonalizeOn })}>{`+$${prise}`}</span>
        </div>
        <div className={b('wrapper', { 'is-open': isPersonalizeOn })}>
          <div className={b('container')}>
            <span className={b('top-text')}>{TOP_TEXT}</span>
            <Toggler
              className={b('toggler')}
              inputClassName={b('toggler-input')}
              togglerChange={this.updateToggle}
              checked={isPersonalizeOn}
              onText=""
              offText=""
              disabled={!isPersonalize || disabled}
            />
          </div>
          {isPersonalizeOn && (
            <div className={b('bottom-block')}>
              <div className={b('info-block')}>
                <span className={b('info-text')}>{INFO_TEXT}</span>
                <button className={b('show-button')} type="button" onClick={this.showPopup}>
                  <EyeIcon className={b('button-icon')} stroke={darkBlue} />
                </button>
              </div>
              {this.generateInputsData(personalizeData)}
            </div>
          )}
        </div>
        {isShowPersonalize &&
          1 === 2 &&
          ReactDOM.createPortal(
            <PersonalizePopup
              close={this.showPopup}
              selectedOption={selectedOption}
              personalizeText={value}
            />,
            document.getElementById('photo-art-constructor-wrap') || null,
          )}
      </div>
    );
  }
}

PhotoArtText.propTypes = propTypes;
PhotoArtText.defaultProps = defaultProps;

const stateProps = (state) => ({
  isShowPersonalize: isShowPersonalizeSelector(state),
});

const actions = {
  selectPersonalize: selectPersonalizeAction,
  clearPersonalize: clearPersonalizeAction,
  togglePersonalizePopup: togglePersonalizePopupAction,
};

export default connect(stateProps, actions)(PhotoArtText);
