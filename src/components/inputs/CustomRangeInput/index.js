import React, { Component } from 'react';
import PropTypes from 'prop-types';
import debounce from 'lodash/debounce';
import bem from 'src/utils/bem';
import styles from './index.module.scss';

const b = bem('custom-range-input', styles);

const defaultProps = {
  min: 100,
  max: 200,
  value: 100,
  onChange: () => {},
  onChangeEnd: () => {},
  className: '',
  step: 1,
  disabled: false,
  inputClass: '',
  plusBtnClass: '',
  minusBtnClass: '',
  isMobPhotoArt: false,
  isPhotoArt: false,
};

const propTypes = {
  min: PropTypes.number,
  max: PropTypes.number,
  value: PropTypes.number,
  onChange: PropTypes.func,
  onChangeEnd: PropTypes.func,
  className: PropTypes.string,
  step: PropTypes.number,
  disabled: PropTypes.bool,
  inputClass: PropTypes.string,
  plusBtnClass: PropTypes.string,
  minusBtnClass: PropTypes.string,
  isMobPhotoArt: PropTypes.bool,
  isPhotoArt: PropTypes.bool,
};

class CustomRangeInput extends Component {
  afterClicks = debounce((value) => {
    const { onChangeEnd } = this.props;
    onChangeEnd(value);
  }, 1500);

  constructor(props) {
    super(props);

    this.state = {
      isTouch: false,
    };
  }

  componentDidMount() {
    document.addEventListener('mouseup', this.onChangeValueEnd);
    document.addEventListener('touchend', this.onChangeValueEnd);
  }

  componentWillUnmount() {
    document.removeEventListener('mouseup', this.onChangeValueEnd);
    document.removeEventListener('touchend', this.onChangeValueEnd);
  }

  onChangeValue = (e) => {
    const { onChange, disabled } = this.props;
    if (!disabled) {
      onChange(Number(e.target.value));
    }
  };

  onChangeValueEnd = () => {
    const { value, onChangeEnd, disabled } = this.props;
    const { isTouch } = this.state;
    if (isTouch && !disabled) {
      onChangeEnd(Number(value));
      this.setState({ isTouch: false });
    }
  };

  onMouseDown = () => {
    const { disabled } = this.props;
    if (!disabled) {
      this.setState({ isTouch: true });
    }
  };

  onClick = (val) => () => {
    const { value, onChange, min, max, disabled } = this.props;
    const newValue = Number(value) + val;
    if (newValue >= min && newValue <= max && !disabled) {
      onChange(newValue);
      this.afterClicks(newValue);
    }
  };

  render() {
    const {
      min,
      max,
      value,
      className,
      step,
      disabled,
      inputClass,
      plusBtnClass,
      minusBtnClass,
      isMobPhotoArt,
      isPhotoArt,
    } = this.props;

    const backwardStep = -1 * step;

    return (
      <div
        className={b({
          mix: className,
          'mob-photo-art': isMobPhotoArt,
          'photo-art': isPhotoArt,
        })}
        role="button"
        tabIndex="0"
      >
        <button
          className={b('btn', {
            plus: true,
            'mob-photo-art': isMobPhotoArt,
            mix: plusBtnClass,
            'photo-art-plus': isPhotoArt,
          })}
          type="button"
          onClick={this.onClick(step)}
          disabled={disabled}
        />
        <input
          type="range"
          min={min}
          max={max}
          value={value}
          className={b('range-slider', {
            'mob-photo-art': isMobPhotoArt,
            'photo-art': isPhotoArt,
            mix: inputClass,
          })}
          onChange={this.onChangeValue}
          onMouseDown={this.onMouseDown}
          onTouchStart={this.onMouseDown}
          disabled={disabled}
        />
        <button
          className={b('btn', {
            'mob-photo-art': isMobPhotoArt,
            'photo-art': isPhotoArt,
            mix: minusBtnClass,
          })}
          type="button"
          onClick={this.onClick(backwardStep)}
          disabled={disabled}
        />
      </div>
    );
  }
}

CustomRangeInput.propTypes = propTypes;
CustomRangeInput.defaultProps = defaultProps;

export default CustomRangeInput;
