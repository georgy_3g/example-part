import React, { Component } from 'react';
import bem from 'src/utils/bem';
import PropTypes from 'prop-types';
import Colors from 'src/styles/colors.json';
import dynamic from "next/dynamic";
import Image from 'next/image';
import styles from './index.module.scss';
import HELP_CENTER from './constants';

const ArrowIcon = dynamic(() => import('src/components/svg/ArrowIcon'));
const CloseIcon2 = dynamic(() => import('src/components/svg/CloseIcon2'));
const YouTubeVideo = dynamic(() => import('src/components/widgets/YouTubeVideo'));

const b = bem('help-center-card', styles);

const burntSiennaColor = Colors['$burnt-sienna-color'];
const hitGrayColor = Colors['$hit-gray-color'];

const defaultProps = {
  closePopUp: () => {},
  helpList: [],
};

const propTypes = {
  closePopUp: PropTypes.func,
  helpList: PropTypes.arrayOf(PropTypes.shape({})),
};

class HelpCenterCard extends Component {
  
  constructor(props) {
    super(props);
    this.veil = React.createRef();

    this.state = {
      helpItem: {},
      selectOn: true,
      selectedImage: {},
    };
  }

  closeModal = (e) => {
    const { closePopUp } = this.props;
    if (e.target === this.veil.current) {
      closePopUp();
    }
  };

  closeModalButton = () => {
    const { closePopUp } = this.props;
    closePopUp();
  };

  selectItem = (item) => {
    this.setState({
      helpItem: item,
      selectedImage: item.images[0] || {},
    });
  };

  changeSelect = () => {
    const { selectOn } = this.state;
    this.setState({ selectOn: !selectOn });
  };

  selectImage = (image) => {
    this.setState({ selectedImage: image });
  }

  render() {
    const { helpList } = this.props;
    const { helpItem, selectOn, selectedImage } = this.state;
    const { description } = helpItem;

    const {images} = helpItem;

    return (
      <div className={b()} ref={this.veil} onClick={this.closeModal} role="button" tabIndex="0">
        <div className={b('wrapper')}>
          <button 
            className={b('select')}
            type="button"
            onClick={this.changeSelect}
          >
            <span className={b('select-title')}>{helpItem.title || HELP_CENTER}</span>
            <div className={b('select-icon', { selected: selectOn })}>
              <ArrowIcon stroke={burntSiennaColor} strokeWidth={3} />
            </div>
            {selectOn && (
              <div className={b('list')}>
                {helpList.map(item => (
                  <button 
                    className={b('list-item')}
                    type='button'
                    onClick={() => { this.selectItem(item); }}
                  >
                    <span className={b('list-item-title')}>{item.title}</span>
                    <div className={b('list-item-icon')}>
                      <ArrowIcon stroke={hitGrayColor} strokeWidth={3} />
                    </div>
                  </button>
                ))}
              </div>
            )}
          </button>
          {helpItem.id && (
            <div className={b('content')}>
              <div className={b('block-images')}>
                <div className={b('main-image')}>
                  {selectedImage.image[0] === '<' ? (
                    <YouTubeVideo 
                      className={b('video')}
                      contentIframe={selectedImage.image}
                    />
                  ) : (
                    <Image 
                      className={b('image')}
                      src={selectedImage.image}
                      layout="fill"
                    />
                  )}
                </div>
                <div className={b('images-list')}>
                  {images.map(item => (
                    <button 
                      className={b('item-image')}
                      type="button"
                      onClick={() => { this.selectImage(item); }}
                    >
                      {item.image[0] === '<' ? (
                        <> 
                          <YouTubeVideo
                            className={b('video')}
                            contentIframe={item.image}
                          />
                          <div className={b('veil-video')} />
                        </>

                      ) : (
                        <Image 
                          className={b('image')}
                          src={item.image}
                          layout="fill"
                        />
                      )}
                    </button>
                  ))}
                </div>
              </div>
              <div
                className={b('description')}
                dangerouslySetInnerHTML={{ __html: description }}
              />
            </div>
          )}
          <button className={b('close-btn')} type="button" onClick={this.closeModalButton}>
            <CloseIcon2 />
          </button>
        </div>
      </div>
    );
  }
}

HelpCenterCard.propTypes = propTypes;
HelpCenterCard.defaultProps = defaultProps;

export default HelpCenterCard;
