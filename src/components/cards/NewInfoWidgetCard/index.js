import React from 'react';
import bem from 'src/utils/bem';
import PropTypes from 'prop-types';
import styles from './index.module.scss';

const b = bem('new-info-widget-card', styles);

const defaultProps = {
  cardBackgroundStripeColor: '',
  cardBackgroundSecondStripeColor: '',
  cardImage: {},
  isRight: false,
  mission: false,
};

const propTypes = {
  cardBackgroundStripeColor: PropTypes.string,
  cardBackgroundSecondStripeColor: PropTypes.string,
  cardImage: PropTypes.shape({
    src: PropTypes.string,
  }),
  isRight: PropTypes.bool,
  mission: PropTypes.bool,
};

const NewInfoWidgetCard = (props) => {
  const {
    cardImage,
    cardBackgroundStripeColor,
    cardBackgroundSecondStripeColor,
    isRight,
    mission,
  } = props;
  return (
    <div className={b()}>
      <div className={b('wrapper', { 'is-right': isRight })}>
        <div
          className={b('img-container', {
            'is-right': isRight,
            'mission-page-img-container': mission,
          })}
        >
          <img
            className={b('img', { mix: 'lazyload', mission })}
            src={cardImage.src || cardImage}
            alt="card"
          />
          <div
            className={b('image-stripe-one', { 'is-right': isRight })}
            style={{ backgroundColor: cardBackgroundStripeColor }}
          />
          <div
            className={b('image-stripe-two', { 'is-right': isRight, mission })}
            style={{ backgroundColor: cardBackgroundSecondStripeColor }}
          />
        </div>
      </div>
    </div>
  );
}

NewInfoWidgetCard.propTypes = propTypes;
NewInfoWidgetCard.defaultProps = defaultProps;

export default NewInfoWidgetCard;
