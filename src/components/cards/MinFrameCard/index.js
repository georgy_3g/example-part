import React, { useState } from 'react';
import bem from 'src/utils/bem';
import generateRotateStyle from 'src/utils/generateRotateStyle';
import useImageOrientation from 'src/utils/useImageOrientation';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { ORIENTATION_TYPES } from 'src/constants';
import ReactDOM from 'react-dom';
import dynamic from 'next/dynamic';
import HelpCenterFrameCard from 'src/components/cards/HelpCenterFrameCard';
import { approvedImageSelect } from 'src/redux/approvedOrderImage/selectors';
import { DOLLAR, FROM_TEXT } from './constants';
import styles from './index.module.scss';

const HintWithBorderIcon = dynamic(() => import('src/components/svg/HintWithBorderIcon'));

const b = bem('min-frame-card', styles);

const defaultProps = {
  data: {},
  selected: {},
  className: '',
  select: () => {  },
  photo: {},
  imageData: null,
  withOutStorePhoto: false,
};

const propTypes = {
  data: PropTypes.shape({
    photoArtImages: PropTypes.arrayOf(PropTypes.shape({})),
    id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    displayName: PropTypes.string,
    price: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    hint: PropTypes.string,
    productHintImages: PropTypes.arrayOf(PropTypes.shape({})),
    frameDisplays: PropTypes.arrayOf(PropTypes.shape({})),
    options: PropTypes.arrayOf(PropTypes.shape({})),
    imageUri: PropTypes.string,
    hintStyles: PropTypes.shape({}),
    hintTitle: PropTypes.string,
  }),
  photo: PropTypes.shape({
    imageUri: PropTypes.string,
    id: PropTypes.number,
  }),
  selected: PropTypes.shape({
    id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  }),
  className: PropTypes.string,
  select: PropTypes.func,
  imageData: PropTypes.shape({
    imageUri: PropTypes.string,
    id: PropTypes.number,
  }),
  withOutStorePhoto: PropTypes.bool,
};

function MinFrameCard(props) {
  const [isHintOpen, toggleHint] = useState(false);

  const {
    data,
    select,
    className,
    photo,
    imageData,
    withOutStorePhoto,
    selected,
  } = props;

  const {
    displayName,
    hint,
    productHintImages = [],
    options,
    imageUri: defaultPhoto,
    hintStyles,
  } = data;

  const helpItem = {
    images: productHintImages,
    title: displayName,
    description: hint,
    styles: hintStyles,
  };

  const storePhoto = withOutStorePhoto ? null : photo;

  const { imageUri: photoUri } = imageData || storePhoto || { imageUri: defaultPhoto };

  const { isVertical } = useImageOrientation(photoUri);

  const onClick = () => {
    select(data.id);
  };
  const imageOrientation = isVertical ? ORIENTATION_TYPES.vertical : ORIENTATION_TYPES.horizontal;

  const defaultOption = options.find(
    item => item.frameOrientation === imageOrientation && item.isDefaultMinOption,
  );

  const randomOption = options.find(item => item.frameOrientation === imageOrientation);

  const selectedOption = defaultOption || randomOption || options[0];

  const { minDisplay, frameImageOptions, frameImageUri } = selectedOption.frameDisplays.find(({
    isDefault,
  }) => isDefault)
    || selectedOption.frameDisplays[0] || {};

  const {
    imageUri: displaysUri,
    frameRotateX,
    frameRotateY,
    frameRotateZ,
    frameScale,
    frameScaleX,
    frameTop,
    frameLeft,
    framePerspective,
    framePerspectiveOrigin,
    backgroundLeft,
    backgroundScale,
    backgroundTop,
  } = minDisplay || {};

  const minPrice = Math.min(...options.map(({ price }) => price));

  const { frameColor: { name: color = '' } = {}, frameSize: { name: size = '' } = {} } = selectedOption || {};

  const alt = `${displayName} ${color} ${size}`;

  const top = 0;
  const left = 0;

  const frameBlockStyle = {
    height: `${frameScale}%`,
    width: `${frameScaleX}%`,
    left: `${frameLeft}%`,
    top: `${frameTop}%`,
    perspective: `${framePerspective}px`,
    perspectiveOrigin: framePerspectiveOrigin,
  };

  const {
    height,
    width,
    x,
    y,
  } = frameImageOptions || {};

  const blockStyle = {
    height: `${height}%`,
    width: `${width}%`,
    left: `${x}%`,
    top: `${y}%`,
    transform: generateRotateStyle(frameRotateX, frameRotateY, frameRotateZ),
  };

  const imageStyle = {
    height: `${100 - top * 2}%`,
    width: `${100 - left * 2}%`,
    top: `${top}%`,
    left: `${left}%`,
  };

  return (
    <article className={b({ mix: className, select: selected === data.id })}>
      <div
        className={b('image-block')}
        role="button"
        onClick={onClick}
        tabIndex="0"
      >
        <img
          className={b('background-image')}
          src={displaysUri}
          alt={alt}
          style={
            {
              width: `${backgroundScale}%`,
              left: `-${backgroundLeft}%`,
              top: `-${backgroundTop}%`,
            }
          }
        />
        <div className={b('frame-wrap')} style={frameBlockStyle}>
          <div className={b('image-wrap')} style={blockStyle}>
            <img
              className={b('art-photo')}
              style={imageStyle}
              src={photoUri}
              alt="art"
            />
          </div>
          <img
            className={b('frame-image')}
            src={frameImageUri}
            alt="frame"
          />
        </div>
      </div>
      <div className={b('info-block', { select: selected === data.id })}>
        <span className={b('name')}>
          {displayName}
        </span>
        <span className={b('price')}>
          {`${FROM_TEXT} ${DOLLAR}${minPrice}`}
        </span>
        <div
          className={b('hint-button')}
          role="button"
          tabIndex={0}
          onClick={() => toggleHint(true)}
        >
          <HintWithBorderIcon className={b('hint-icon')} />
        </div>
      </div>
      {isHintOpen && (
        ReactDOM.createPortal(
          <HelpCenterFrameCard helpItem={helpItem} closePopUp={() => toggleHint(false)} />,
          document.getElementById('__next') || null,
        )
      )}
    </article>
  );
}

MinFrameCard.propTypes = propTypes;
MinFrameCard.defaultProps = defaultProps;

const stateProps = state => ({
  photo: approvedImageSelect(state),
});

export default connect(stateProps)(MinFrameCard);
