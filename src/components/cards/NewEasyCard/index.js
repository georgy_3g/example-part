import React from 'react';
import bem from 'src/utils/bem';
import PropTypes from 'prop-types';
import styles from './index.module.scss';

const b = bem('new-easy-card', styles);

const defaultProps = {
  card: {},
};

const propTypes = {
  card: PropTypes.shape({
    cardImage: PropTypes.shape({
      src: PropTypes.string,
    }),
    cardText: PropTypes.string,
    cardTitle: PropTypes.string,
  }),
};

function NewEasyCard(props) {
  const { card } = props;
  const { cardImage, cardText, cardTitle } = card;

  return (
    <div
      className={b()}
    >
      <div className={b('img-container')}>
        <img
          className={b('img')}
          src={cardImage.src}
          alt="card"
          loading="lazy"
        />
      </div>
      <div className={b('title')}>{cardTitle}</div>
      <div className={b('text')}>{cardText}</div>
    </div>
  );
}

NewEasyCard.propTypes = propTypes;
NewEasyCard.defaultProps = defaultProps;

export default NewEasyCard;
