const TERMS = {
  TITLE: 'Our mission',
  DESCRIPTION:
    'We are a dedicated team of archivists and creators passionate \nabout helping you create unique personalized memorabilia. \nThis is our mission and we are excited to share it with you.',
};

export default TERMS;
