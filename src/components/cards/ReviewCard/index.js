import React, { useState } from 'react';
import PropTypes from 'prop-types';
import ReactDOM from 'react-dom';
import bem from 'src/utils/bem';
import reviewsFormatText from 'src/utils/reviewsFormatText';
import format from 'date-fns/format';
import colors from 'src/styles/colors.json';
import Image from 'next/image';
import dynamic from "next/dynamic";
import styles from './index.module.scss';

const Rate = dynamic(() => import('src/components/elements/Rate'));
const ImagePopup = dynamic(() => import('src/components/popups/ImagePopup'));
const MagnifyingPlus = dynamic(() => import('src/components/svg/MagnifyingPlus'));

const b = bem('review-card', styles);
const whiteColor = colors['$white-color'];
const propTypes = {
  review: PropTypes.shape({
    reviewer: PropTypes.shape({
      first_name: PropTypes.string,
      last_name: PropTypes.string,
    }),
    rating: PropTypes.number,
    date_created: PropTypes.string,
    comments: PropTypes.string,
    images: PropTypes.arrayOf(PropTypes.shape({})),
  }),
  className: PropTypes.string,
  isReverseContent: PropTypes.bool,
  isCustomerCard: PropTypes.bool,
  isScrollWrap: PropTypes.bool,
};

const defaultProps = {
  review: {},
  className: '',
  isReverseContent: false,
  isCustomerCard: false,
  isScrollWrap: false,
};

function ReviewCard(props) {
  const [popupImageUri, showImagePopup] = useState(null);

  const { review, className, isReverseContent, isCustomerCard, isScrollWrap } = props;
  const {
    reviewer: { first_name: name, last_name: lastName },
    date_created: date,
    rating,
    comments,
    images,
  } = review;

  const formatDate = date.split(" ").join("T");

  return (
    <div className={b({ mix: className, customer: isCustomerCard, 'is-scroll': isScrollWrap })}>
      <div className={b('wrapper', { 'is-scroll': isScrollWrap })}>
        <div className={b('order-wrapper', { reverse: isReverseContent })}>
          <div className={b('title')}>
            <div className={b('title-wrapper')}>
              <div className={b('reviewer')}>
                <span className={b('reviewer-name')}>{`${name || ''} ${lastName || ''}`}</span>
              </div>
              <div className={b('review-date')}>{format(new Date(formatDate), 'dd.MM.yyyy')}</div>
            </div>
            <div className={b('rating')}>
              <Rate rate={rating} />
            </div>
          </div>
          <div className={b('comment', { 'without-scroll': isScrollWrap })}>{reviewsFormatText(comments)}</div>
        </div>
        {images && images.length ? (
          <div className={b('image-wrapper')}>
            {images.map(({ image, store_review_image_id: id }) => (
              <div className={b('image')} key={id}>
                <Image className={b('image-url')} src={image} alt="review" layout="fill" />
                <button
                  className={b('control-btn')}
                  type="button"
                  onClick={() => showImagePopup(image)}
                  width='100%'
                  height='100%'
                >
                  <MagnifyingPlus className={b('button-icon')} fill={whiteColor} />
                </button>
              </div>
            ))}
          </div>
        ) : null}
      </div>
      {popupImageUri &&
        ReactDOM.createPortal(
          <ImagePopup imageUri={popupImageUri} close={() => showImagePopup(null)} />,
          document.getElementById('__next') || null,
        )}
    </div>
  );
}

ReviewCard.propTypes = propTypes;
ReviewCard.defaultProps = defaultProps;

export default ReviewCard;
