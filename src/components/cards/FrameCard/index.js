import React, { useEffect, useState } from 'react';
import bem from 'src/utils/bem';
import generateRotateStyle from 'src/utils/generateRotateStyle';
import useImageOrientation from 'src/utils/useImageOrientation';
import PropTypes from 'prop-types';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';
import { ORIENTATION_TYPES } from 'src/constants';
import dynamic from 'next/dynamic';
import HelpCenterFrameCard from 'src/components/cards/HelpCenterFrameCard';
import { approvedImageSelect } from 'src/redux/approvedOrderImage/selectors';
import { CREATE_TEXT, DOLLAR, FROM_TEXT } from './constants';
import styles from './index.module.scss';

const BlueButton = dynamic(() => import('src/components/buttons/BlueButton'));
const HintWithBorderIcon = dynamic(() => import('src/components/svg/HintWithBorderIcon'));

const b = bem('frame-card', styles);

const defaultProps = {
  data: {},
  selected: {},
  className: '',
  select: () => {  },
  photo: {},
  imageData: null,
  withOutHints: false,
  withOutStorePhoto: false,
  isPhotoArt: false,
};

const propTypes = {
  isPhotoArt: PropTypes.bool,
  data: PropTypes.shape({
    photoArtImages: PropTypes.arrayOf(PropTypes.shape({})),
    id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    displayName: PropTypes.string,
    price: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    hint: PropTypes.string,
    productHintImages: PropTypes.arrayOf(PropTypes.shape({})),
    frameDisplays: PropTypes.arrayOf(PropTypes.shape({})),
    options: PropTypes.arrayOf(PropTypes.shape({})),
    imageUri: PropTypes.string,
    hintStyles: PropTypes.shape({}),
    hintTitle: PropTypes.string,
    minPrice: PropTypes.shape({
      price: PropTypes.number,
      additionalDiscount: PropTypes.number,
    })
  }),
  photo: PropTypes.shape({
    imageUri: PropTypes.string,
    id: PropTypes.number,
  }),
  selected: PropTypes.shape({
    id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  }),
  className: PropTypes.string,
  select: PropTypes.func,
  imageData: PropTypes.shape({
    imageUri: PropTypes.string,
    id: PropTypes.number,
  }),
  withOutHints: PropTypes.bool,
  withOutStorePhoto: PropTypes.bool,
};

function FrameCard(props) {
  const [isHintOpen, toggleHint] = useState(false);
  const [isError, toggleError] = useState(false);

  const {
    data,
    select,
    className,
    photo,
    imageData,
    withOutHints,
    withOutStorePhoto,
    isPhotoArt,
  } = props;

  useEffect(() => {
    if (
      !withOutStorePhoto
      && isError
      && photo
      && photo.imageUri
    ) {
      toggleError(false);
    }
  }, [photo]);

  useEffect(() => {
    if (
      withOutStorePhoto
      && isError
      && imageData
      && imageData.imageUri
    ) {
      toggleError(false);
    }
  }, [imageData]);

  const {
    displayName,
    hint,
    productHintImages,
    options,
    imageUri: defaultPhoto,
    hintStyles,
    minPrice: {
      price: minFramePrice = 0,
      additionalDiscount: minAdditionalDiscount = 0,
    } = {},
  } = data;


  const storePhoto = withOutStorePhoto ? null : photo;

  const { imageUri: photoUri } = imageData || storePhoto || { imageUri: defaultPhoto };

  const { isVertical } = useImageOrientation(photoUri);

  const btnText = CREATE_TEXT;

  const onClick = (idOption) => {
    select(data, idOption);
  };
  const imageOrientation = isVertical ? ORIENTATION_TYPES.vertical : ORIENTATION_TYPES.horizontal;

  const defaultOption = options.find(
    item => item.frameOrientation === imageOrientation && item.isDefaultMinOption,
  );

  const randomOption = options.find(item => item.frameOrientation === imageOrientation);

  const selectedOption = defaultOption || randomOption || options[0];

  const {
    minDisplay,
    frameImageOptions,
    frameImageUri,
    frameDisplaysPersonalize = [],
  } = selectedOption.frameDisplays.find(({ isDefault }) => isDefault)
    || selectedOption.frameDisplays[0];

  const {
    imageUri: displaysUri,
    frameRotateX,
    frameRotateY,
    frameRotateZ,
    frameScale,
    frameScaleX,
    frameTop,
    frameLeft,
    framePerspective,
    framePerspectiveOrigin,
    backgroundLeft,
    backgroundScale,
    backgroundTop,
  } = minDisplay || {};

  const { frameColor: { name: color = '' } = {}, frameSize: { name: size = '' } = {}, id: idOption } = selectedOption || {};

  const alt = `${displayName} ${color} ${size}`;

  const top = 0;
  const left = 0;

  const frameBlockStyle = {
    height: `${frameScale}%`,
    width: `${frameScaleX}%`,
    left: `${frameLeft}%`,
    top: `${frameTop}%`,
    perspective: `${framePerspective}px`,
    perspectiveOrigin: framePerspectiveOrigin,
  };

  const {
    height,
    width,
    x,
    y,
  } = frameImageOptions || {};

  const blockStyle = {
    height: `${height}%`,
    width: `${width}%`,
    left: `${x}%`,
    top: `${y}%`,
    transform: generateRotateStyle(frameRotateX, frameRotateY, frameRotateZ),
  };

  const imageStyle = {
    height: `${100 - top * 2}%`,
    width: `${100 - left * 2}%`,
    top: `${top}%`,
    left: `${left}%`,
  };

  const helpItem = {
    images: productHintImages,
    title: displayName,
    description: hint,
    styles: hintStyles,
  };

  const personalizeData = (frameDisplaysPersonalize || [])
    .find(item => item && item.isPersonalizeActive);

  const isPersonalize = Boolean(personalizeData);

  const { id } = personalizeData || {};

  const selectedPersonalize = id ? { ...personalizeData } : {};

  const {
    personalizeDisplayDate,
    framePersonalizeUri,
    isPersonalizeDisplayPreview,
  } = selectedPersonalize || {};
  const {
    rotateX,
    rotateY,
    rotateZ,
    scale: persScale,
    scaleX: persScaleX,
    top: persTop,
    left: presLeft,
  } = personalizeDisplayDate || {};


  const personalizeBlockStyle = isPersonalize
    ? {
      height: `${persScale}%`,
      width: `${persScaleX}%`,
      left: `${presLeft}%`,
      top: `${persTop}%`,
      transform: generateRotateStyle(rotateX, rotateY, rotateZ),
      ...((framePersonalizeUri) ? { backgroundImage: `url(${framePersonalizeUri})` } : {}),
    }
    : {};

  return (
    <article className={b({ mix: className, create: isPhotoArt })}>
      <div
        className={b('image-block', { create: isPhotoArt })}
        role="button"
        onClick={() => onClick(idOption)}
        tabIndex="0"
      >
        <img
          className={b('background-image')}
          src={displaysUri}
          alt={alt}
          style={
            {
              width: `${backgroundScale}%`,
              left: `-${backgroundLeft}%`,
              top: `-${backgroundTop}%`,
            }
          }
        />
        <div className={b('frame-wrap')} style={frameBlockStyle}>
          <div className={b('image-wrap')} style={blockStyle}>
            <img
              className={b('art-photo')}
              style={imageStyle}
              src={isError ? defaultPhoto : photoUri || defaultPhoto}
              alt="art"
              onError={() => { toggleError(true); }}
            />
          </div>
          <img
            className={b('frame-image')}
            src={frameImageUri}
            alt="frame"
          />
          {isPersonalizeDisplayPreview && (
            <div
              className={b('personalize-data-block')}
              style={personalizeBlockStyle || {}}
            />
          )}
        </div>
      </div>
      <div className={b('info-block')}>
        <h3 className={b('name')}>
          {displayName}
        </h3>
        <span className={b('price')}>
          {FROM_TEXT}
          <span className={b('price-value', { origin: Boolean(minAdditionalDiscount) })}>{`${DOLLAR}${minFramePrice}`}</span>
          {minAdditionalDiscount ? (
            <span className={b('price-value')}>
              {`$${(minFramePrice - minAdditionalDiscount)}`}
            </span>
            ) : null}
        </span>
        <div className={b('button-wrap')}>
          <BlueButton
            className={b('btn')}
            onClick={() => onClick(idOption)}
          >
            {btnText}
          </BlueButton>
          {!withOutHints && (
            <button className={b('hint-button')} type="button" onClick={() => toggleHint(!isHintOpen)}>
              <HintWithBorderIcon className={b('hint-icon')} />
            </button>
          )}
        </div>
        {isHintOpen && (
          ReactDOM.createPortal(
            <HelpCenterFrameCard helpItem={helpItem} closePopUp={() => toggleHint(false)} />,
            document.getElementById('__next') || null,
          )
        )}
      </div>
    </article>
  );
}

FrameCard.propTypes = propTypes;
FrameCard.defaultProps = defaultProps;

const stateProps = state => ({
  photo: approvedImageSelect(state),
});

export default connect(stateProps)(FrameCard);
