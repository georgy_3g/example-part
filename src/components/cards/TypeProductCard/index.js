import React from 'react';
import PropTypes from 'prop-types';
import bem from 'src/utils/bem';
import useMobileStyle from 'src/utils/useMobileStyle';
import { connect } from 'react-redux';
import LazyLoad from 'react-lazyload';
import Image from 'next/image';
import dynamic from 'next/dynamic';
import { isMobileDeviceSelector } from 'src/redux/auth/selectors';
import styles from './index.module.scss';

const BlueButton = dynamic(() => import('src/components/buttons/BlueButton'));

const b = bem('type-product-card', styles);

const defaultProps = {
  onClick: () => {},
  title: '',
  desc: '',
  textBtn: '',
  imgUrl: {},
  mobileImgUrl: {},
  imgAlt: '',
  url: '',
  isMobileDevice: true,
};

const propTypes = {
  onClick: PropTypes.func,
  title: PropTypes.string,
  desc: PropTypes.string,
  textBtn: PropTypes.string,
  imgUrl: PropTypes.shape({
    full: PropTypes.shape({}),
    middle: PropTypes.shape({}),
    defaultImg: PropTypes.shape({}),
  }),
  mobileImgUrl: PropTypes.shape({
    src: PropTypes.string,
    defaultImg: PropTypes.shape({}),
  }),
  imgAlt: PropTypes.string,
  url: PropTypes.string,
  isMobileDevice: PropTypes.bool,
};

const TypeProductCard = (props) => {
  const {
    onClick,
    title,
    desc,
    textBtn,
    imgUrl,
    imgAlt,
    url,
    mobileImgUrl,
    isMobileDevice,
  } = props;

  const isMobileDisplay = useMobileStyle(480, isMobileDevice);

  const image = mobileImgUrl.src && isMobileDisplay ? mobileImgUrl.defaultImg : imgUrl.defaultImg
  return (
    <article className={b()}>
      <a className={b('image-link')} href={url}>
        <div className={b('image-block')}>
          <LazyLoad offset={100}>
            <Image
              className={b('image')}
              src={image}
              alt={imgAlt}
              layout="fill"
            />
          </LazyLoad>
        </div>
        <div className={b('text-block')}>
          <h3 className={b('title')}>{title}</h3>
          <p className={b('desc')}>{desc}</p>
          <BlueButton className={b('button')} onClick={onClick}>
            {textBtn}
          </BlueButton>
        </div>
      </a>
    </article>
  );
}

TypeProductCard.propTypes = propTypes;
TypeProductCard.defaultProps = defaultProps;

const stateProps = (state) => ({
  isMobileDevice: isMobileDeviceSelector(state),
});

export default connect(stateProps, null)(TypeProductCard);
