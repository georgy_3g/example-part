import React from 'react';
import PropTypes from 'prop-types';
import bem from 'src/utils/bem';
import styles from './index.module.scss';

const b = bem('presentation-card', styles);

const defaultProps = {
  data: {
    title: '',
    desc: '',
    imgUrl: {},
  },
  className: '',
};

const propTypes = {
  data: PropTypes.shape({
    title: PropTypes.string,
    desc: PropTypes.string,
    imgUrl: PropTypes.shape({ defaultImg: PropTypes.shape({
        blurDataURL: PropTypes.string,
        src: PropTypes.string,
    }) }),
  }),
  className: PropTypes.string,
};

function PresentationCard({
  data: {
    title,
    imgUrl: {
      defaultImg,
    },
  },
  className,
}) {
  return (
    <section className={`${b()} ${className}`}>
      <img
        className={b('image', { mix: 'lazyload' })}
        src={defaultImg.src}
        alt={`${defaultImg}-${title}`}
      />
    </section>
  )
}

PresentationCard.propTypes = propTypes;
PresentationCard.defaultProps = defaultProps;

export default PresentationCard;
