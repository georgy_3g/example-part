import React from 'react';
import bem from 'src/utils/bem';
import PropTypes from 'prop-types';
import Image from 'next/image';
import styles from './index.module.scss';

const b = bem('what-transfer-card', styles);

const defaultProps = {
  card: {},
  isFirst: false,
  isLast: false,
};

const propTypes = {
  card: PropTypes.shape({
    cardImage: PropTypes.shape({
      blurDataURL: PropTypes.string,
    }),
    cardText: PropTypes.string,
    isLink: PropTypes.bool,
    link: PropTypes.string,
    linkText: PropTypes.string,
    cardTitle: PropTypes.string,
  }),
  isFirst: PropTypes.bool,
  isLast: PropTypes.bool,

};

function WhatTransferCard(props) {
  const { card, isFirst, isLast } = props;
  const { cardImage, cardText, cardTitle, isLink, link, linkText } = card;
  return (
    <div className={b({ isFirst, isLast })}>
      <div className={b('img-container')}>
        <Image
          className={b('img')}
          src={cardImage}
          width="77px"
          height="66px"
          alt="transfer card"
        />
      </div>
      <div className={b('title')}>{cardTitle}</div>
      <div className={b('text-container')}>
        <div className={b('text')}>{cardText}</div>
        {isLink && (
          <a className={b('link')} href={link}>
            <div className={b('link-text')}>{linkText}</div>
          </a>
        )}
      </div>
    </div>
  );
}

WhatTransferCard.propTypes = propTypes;
WhatTransferCard.defaultProps = defaultProps;

export default WhatTransferCard;
