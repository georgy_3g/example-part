import React from 'react';
import bem from 'src/utils/bem';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { isMobileDeviceSelector } from 'src/redux/auth/selectors';
import styles from './index.module.scss';

const b = bem('make-us-card', styles);

const defaultProps = {
  card: {},
  isFirst: false,
  isLast: false,
  displayType: 'desc',
};

const propTypes = {
  card: PropTypes.shape({
    cardImage: PropTypes.shape({}),
    cardImageTab: PropTypes.shape({}),
    cardText: PropTypes.string,
    isLink: PropTypes.bool,
    link: PropTypes.string,
    linkText: PropTypes.string,
  }),
  isFirst: PropTypes.bool,
  isLast: PropTypes.bool,
  displayType: PropTypes.string,
};

function MakeUsCard(props) {
  const { card, isFirst, isLast, displayType } = props;
  const { cardImage, cardImageTab, cardText, isLink, link, linkText } = card;

  const getContent = ([first, second, third]) => {
    switch (displayType) {
      case 'mob':
        return first;
      case 'tab':
        return second || first;
      default:
        return third || second || first;
    }
  };

  return (
    <div className={b({ isFirst, isLast })}>
      <div className={b('img-container')}>
        <img
          className={b('img')}
          src={getContent([cardImageTab, cardImageTab, cardImage]).src}
          alt={cardText}
        />
      </div>
      <div className={b('text')}>{cardText}</div>
      {isLink && (
        <a className={b('link')} href={link}>
          <div className={b('link-text')}>{linkText}</div>
        </a>
      )}
    </div>
  );
}

MakeUsCard.propTypes = propTypes;
MakeUsCard.defaultProps = defaultProps;

const stateProps = (state) => ({
  isMobileDevice: isMobileDeviceSelector(state),
});

export default connect(stateProps, null)(MakeUsCard);
