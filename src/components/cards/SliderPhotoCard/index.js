import React, { useState } from 'react';
import ReactDOM from 'react-dom';
import bem from 'src/utils/bem';
import PropTypes from 'prop-types';
import colors from 'src/styles/colors.json';
import dynamic from "next/dynamic";
import styles from './index.module.scss';

const CheckFull = dynamic(() => import('src/components/svg/CheckFull'));
const DeleteIcon = dynamic(() => import('src/components/svg/DeleteIcon'));
const ImageComponent = dynamic(() => import('src/components/elements/Image'));
const ImagePopup = dynamic(() => import('src/components/popups/ImagePopup'));
const MagnifyingPlus = dynamic(() => import('src/components/svg/MagnifyingPlus'));

const whiteColor = colors['$white-color'];
const turquoiseColor = colors['$turquoise-color'];

const b = bem('slider-photo-card', styles);

const defaultProps = {
  data: {},
  className: '',
  select: () => {},
  index: 0,
  selected: 0,
  withoutText: false,
  iconClass: '',
  deleteImageFromSlider: () => {},
  isSelected: false,
  onImageError: () => {},
  onLoadImage: () => {},
};

const propTypes = {
  data: PropTypes.shape({
    id: PropTypes.number,
    imageUri: PropTypes.string,
    parentId: PropTypes.number,
  }),
  className: PropTypes.string,
  select: PropTypes.func,
  index: PropTypes.number,
  selected: PropTypes.number,
  withoutText: PropTypes.bool,
  iconClass: PropTypes.string,
  deleteImageFromSlider: PropTypes.func,
  isSelected: PropTypes.bool,
  onImageError: PropTypes.func,
  onLoadImage: PropTypes.func,
};

function SliderPhotoCard(props) {
  const {
    data,
    className,
    select,
    index,
    selected,
    isSelected,
    withoutText,
    iconClass,
    deleteImageFromSlider,
    onImageError,
    onLoadImage,
  } = props;

  let imageRef = null;

  const [isVertical, setIsVertical] = useState(false);
  const [isShowImage, toggleImagePopup] = useState(false);

  const onClick = (e, isAddButton) => {
    const value = {
      ...data,
      index,
    };
    if (e.target === imageRef || isAddButton) {
      select(value);
    }
  };

  const addRef = (e) => {
    imageRef = e;
  };

  const { id, imageUri, parentId } = data;
  const title = parentId ? `v${index + 1}` : `#${id}`;
  const isImageSelected = id === selected || isSelected;
  return (
    <div
      role="button"
      tabIndex="0"
      className={b({ vertical: isVertical, mix: className })}
      onClick={onClick}
    >
      <div className={b('image-wrap', { vertical: isVertical })}>
        <ImageComponent
          setIsVertical={setIsVertical}
          className={b('photo')}
          imageUri={imageUri}
          addRef={addRef}
          onImageError={onImageError}
          onLoadImage={onLoadImage}
        />
        <button className={b('select-btn')} type="button" onClick={(e) => onClick(e, true)}>
          <CheckFull
            className={b('select-icon', { mix: iconClass })}
            stroke={whiteColor}
            fill={isImageSelected ? turquoiseColor : 'none'}
          />
        </button>
        <button
          className={b('control-btn', { vertical: isVertical })}
          type="button"
          onClick={toggleImagePopup}
        >
          <MagnifyingPlus className={b('button-icon', { mix: iconClass })} fill={whiteColor} />
        </button>
        <button
          className={b('delete-btn')}
          type="button"
          onClick={() => {
            deleteImageFromSlider(data, isImageSelected);
          }}
        >
          <DeleteIcon className={b('delete-icon', { mix: iconClass })} />
        </button>
      </div>
      {!withoutText && <span className={b('title')}>{title}</span>}
      {isShowImage &&
        ReactDOM.createPortal(
          <ImagePopup imageUri={imageUri} close={() => toggleImagePopup(false)} />,
          document.getElementById('__next') || null,
        )}
    </div>
  );
}

SliderPhotoCard.propTypes = propTypes;
SliderPhotoCard.defaultProps = defaultProps;

export default SliderPhotoCard;
