import React, { Component } from 'react';
import bem from 'src/utils/bem';
import PropTypes from 'prop-types';
import debounce from 'lodash/debounce';
import Image from 'next/image';
import LazyLoad from 'react-lazyload';
import styles from './index.module.scss';

const b = bem('compare-slide-card', styles);

const defaultProps = {
  item: {},
  imageClass: '',
  compareImageClass: '',
};

const propTypes = {
  item: PropTypes.shape({
    url: PropTypes.shape({
      src: PropTypes.string,
    }),
    secondUrl: PropTypes.shape({
      src: PropTypes.string,
    }),
    id: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    alt: PropTypes.string,
  }),
  imageClass: PropTypes.string,
  compareImageClass: PropTypes.string,
};

class CompareSlideCard extends Component {
  resizeWindow = debounce(() => {
    this.windowWidth = window.innerWidth;
  }, 300);

  constructor(props) {
    super(props);
    this.card = null;
    this.imageBlock = null;
    this.coordinates = null;
    this.startCoordinates = null;
    this.state = {
      position: {
        x: 50,
        y: 50,
        clip: 'inset(0 calc(50%) 0 0)',
      },
      isDraggable: false,
    };
    this.slideBtn = null;
  }

  componentDidMount() {
    document.addEventListener('mouseup', this.mouseOut);
    document.addEventListener('touchend', this.mouseOut);
    window.addEventListener('resize', this.resizeWindow);
    this.windowWidth = window.innerWidth;
  }

  componentWillUnmount() {
    document.removeEventListener('mouseup', this.mouseOut);
    document.removeEventListener('touchend', this.mouseOut);
    window.removeEventListener('resize', this.resizeWindow);
    this.windowWidth = null;
    this.card = null;
    this.coordinates = null;
    this.startCoordinates = null;
    this.imageBlock = null;
    this.slideBtn = null;
  }

  getValidCoordinates = (x, y) => {
    const { clientWidth, clientHeight } = this.imageBlock;

    const temporaryX = x > -20 ? clientWidth : -20;
    const temporaryY = y > 20 ? clientHeight - 20 : 20;

    return {
      validX: x >= -12 && x <= clientWidth ? x : temporaryX,
      validY: y >= 25 && y <= clientHeight - 25 ? y : temporaryY,
    };
  };

  onMouseMove = (e) => {
    if (this.imageBlock) {
      const { isDraggable } = this.state;
      const { clientWidth, clientHeight } = this.imageBlock;
      if (isDraggable) {
        const { screenX = 0, screenY = 0, touches } = e;
        const { x: startX, y: startY } = this.startCoordinates;
        const { screenX: mobScreenX, clientY: mobScreenY } =
          touches && touches.length ? touches[0] : { screenX: 0, screenY: 0 };
        const { x: oldX, y: oldY } = this.coordinates;

        const oldPositionX = (clientWidth * startX) / 100;

        const oldPositionY = (clientHeight * startY) / 100;

        const newX = oldPositionX - ((screenX || mobScreenX) - oldX);
        const newY = oldPositionY + ((screenY || mobScreenY) - oldY);

        const { validX, validY } = this.getValidCoordinates(newX, newY);

        this.setState({
          position: {
            x: (validX / clientWidth) * 100,
            y: (validY / clientHeight) * 100,
            clip: `inset(0 ${(validX / clientWidth) * 100}% 0 0)`,
          },
        });
      }
    }
  };

  mouseClick = (e) => {
    const { screenX, screenY, touches } = e;
    const { position } = this.state;
    const { screenX: mobScreenX, clientY: mobScreenY } =
      touches && touches.length ? touches[0] : { screenX: 0, screenY: 0 };
    this.coordinates = { x: screenX || mobScreenX, y: screenY || mobScreenY };
    this.startCoordinates = { ...position };
    this.setState({ isDraggable: true });
  };

  mouseOut = () => {
    const { isDraggable } = this.state;
    if (isDraggable) {
      this.coordinates = null;
      this.startCoordinates = null;
      this.setState({ isDraggable: false });
    }
  };

  render() {
    const { item, imageClass, compareImageClass } = this.props;
    const { url, secondUrl, alt } = item;
    const { position } = this.state;
    const { offsetWidth, offsetHeight } = this.slideBtn || {};
    return (
      <div
        className={b()}
        onMouseMove={this.onMouseMove}
        onTouchMove={this.onMouseMove}
        ref={(e) => {
          this.card = e;
        }}
      >
        <LazyLoad offset={100}>
          <div
            className={b('inner-slide', { mix: imageClass })}
            ref={(e) => {
            this.imageBlock = e;
          }}
          >
            <Image
              className={b('inner-slide', { mix: imageClass })}
              src={url}
              alt={alt || url}
              layout="responsive"
              height={524}
              width={524}
            />
          </div>
          <div
            className={b('inner-compare-slide', { mix: compareImageClass })}
            style={{
            WebkitClipPath: position.clip,
            clipPath: position.clip,
          }}
          >
            <Image
              className={b('inner-slide', { mix: imageClass })}
              src={secondUrl}
              alt={alt || secondUrl}
              layout="responsive"
              height={524}
              width={524}
            />
          </div>
        </LazyLoad>
        <div
          className={b('compare-btn')}
          style={{
            top: `calc(${position.y}% - ${(offsetHeight || 50) / 2}px)`,
            right: `calc(${position.x}% - ${(offsetWidth || 50) / 2}px)`,
          }}
          role="button"
          tabIndex={-1}
          onMouseDown={this.mouseClick}
          onTouchStart={this.mouseClick}
          ref={(e) => {
            this.slideBtn = e;
          }}
        >
          <div className={b('compare-btn-arrow', { left: true })} />
          <div className={b('compare-btn-arrow', { right: true })} />
        </div>
      </div>
    );
  }
}

CompareSlideCard.propTypes = propTypes;
CompareSlideCard.defaultProps = defaultProps;

export default CompareSlideCard;
