import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import bem from 'src/utils/bem';
import { connect } from 'react-redux';
import colors from 'src/styles/colors.json';
import note from 'public/img/account/audio.png';
import dynamic from 'next/dynamic';
import { stopAllPlayersSelector } from 'src/redux/myCloud/selectors';
import { changePlayerFlagAction } from 'src/redux/myCloud/actions';
import styles from './index.module.scss';

const CheckFull = dynamic(() => import('src/components/svg/CheckFull'));
const NewMagnifyingMinusIcon = dynamic(() => import('src/components/svg/NewMagnifyingMinusIcon'));
const ImageMenu = dynamic(() => import('src/components/elements/ImageMenu'));

const b = bem('big-audio-card', styles);
const darkBlue = colors['$dark-slate-blue-color'];
const whiteColor = colors['$white-color'];
const greenColor = colors['$summer-green-color'];
const grayColor = colors['$slate-gray-color'];


const defaultProps = {
  fileData: null,
  className: '',
  selected: [],
  addToSelected: () => {},
  removeFromSelected: () => {},
  forSharedPage: false,
  selectTailView: () => {},
  stopAllPlayers: false,
  changePlayerFlag: () => {},
};

const propTypes = {
  fileData: PropTypes.shape({
    audioUri: PropTypes.string,
    id: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    name: PropTypes.string,
  }),
  className: PropTypes.string,
  selected: PropTypes.arrayOf(PropTypes.shape({})),
  addToSelected: PropTypes.func,
  removeFromSelected: PropTypes.func,
  forSharedPage: PropTypes.bool,
  selectTailView: PropTypes.func,
  stopAllPlayers: PropTypes.bool,
  changePlayerFlag: PropTypes.func,
};

function BigAudioCard(props) {
  const {
    className,
    fileData,
    selected,
    addToSelected,
    removeFromSelected,
    forSharedPage,
    selectTailView,
    stopAllPlayers,
    changePlayerFlag,
  } = props;

  let player = null;

  useEffect(() => {
    if (stopAllPlayers && player) {
      player.pause();
    }
  }, [stopAllPlayers]);

  const [wrapRef, addWrapRef] = useState(null);

  const { audioUri, id } = fileData;

  const isSelect = !!selected.find((item) => item.id === id && item.audioUri === audioUri);

  const onClick = () => {
    if (isSelect) {
      removeFromSelected(fileData);
    } else {
      addToSelected(fileData);
    }
  };

  return (
    <div
      className={b({ mix: className, select: isSelect })}
      role="button"
      tabIndex={-1}
      onClick={onClick}
    >
      <div className={b('content-wrap')} ref={addWrapRef}>
        <button className={b('select-btn')} type="button" onClick={onClick}>
          <CheckFull
            className={b('select-icon')}
            stroke={isSelect ? greenColor : whiteColor}
            stroke2={isSelect ? whiteColor : 'none'}
            fill={isSelect ? greenColor : `${grayColor}88`}
          />
        </button>
        <div className={b('audio-block')}>
          <div
            className={b('image-wrap')}
            style={{ backgroundImage: `url(${note})` }}
          />
          <audio
            className={b('audio')}
            width="100%"
            controls
            ref={(e) => {
              player = e;
            }}
            onPlay={() => {
              changePlayerFlag(true);
            }}
          >
            <source src={audioUri} />
            <track kind="captions" />
          </audio>
        </div>
        <div className={b('button-block')}>
          <div className={b('button-wrap')}>
            <button className={b('zoom-button')} type="button" onClick={selectTailView}>
              <NewMagnifyingMinusIcon className={b('zoom-icon')} />
            </button>
          </div>
          <div className={b('menu-button', { end: true })}>
            <ImageMenu
              className={b('button-icon')}
              iconFillColor={darkBlue}
              imageData={fileData}
              forSharedPage={forSharedPage}
              portalBlock={wrapRef}
              withOutOrderPrint
              slider
              type="audio"
            />
          </div>
        </div>
      </div>
    </div>
  );
}

BigAudioCard.propTypes = propTypes;
BigAudioCard.defaultProps = defaultProps;

const stateProps = (state) => ({
  stopAllPlayers: stopAllPlayersSelector(state),
});

const actions = {
  changePlayerFlag: changePlayerFlagAction,
};
export default connect(stateProps, actions)(BigAudioCard);
