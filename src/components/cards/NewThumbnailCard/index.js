import React, { useEffect, useState } from 'react';
import ReactDOM from 'react-dom';
import bem from 'src/utils/bem';
import formattedImage from 'src/utils/formattedImage';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import dynamic from 'next/dynamic';
import { loadImages, loadImagesOrderEnhance } from 'src/redux/orderImages/actions';
import { getToken } from 'src/redux/auth/selectors';
import styles from './index.module.scss';

const MagnifyingPlus2 = dynamic(() => import('src/components/svg/MagnifyingPlus2'));
const ImagePopup = dynamic(() => import('src/components/popups/ImagePopup'));
const CropPopup = dynamic(() => import('src/components/popups/CropPopup'));

const b = bem('new-thumbnail-card', styles);

const defaultProps = {
  data: {},
  className: '',
  select: () => {},
  index: 0,
  selected: 0,
  isSelected: false,
  forSharedPage: false,
  fromMyFiles: false,
  selectedArr: [],
  removeFromSelected: () => {},
  addToSelected: () => {},
  deleteImageFromSlider: () => {},
  withCrop: false,
  isEnhanceProduct: false,
  uploadImages: () => {},
  uploadImagesEnhance: () => {},
  token: '',
  showPopup: () => {},
  multiSelect: false,
  withOutZoom: false,
  onImageError: () => {},
  withoutMenu: false,
  withChildren: false,
  children: null,
  withOutDelete: false,
};

const propTypes = {
  data: PropTypes.shape({
    id: PropTypes.number,
    imageUri: PropTypes.string,
    imageKey: PropTypes.string,
    watermarkedImageUri: PropTypes.string,
    parentId: PropTypes.number,
    isApproved: PropTypes.bool,
    isTemplate: PropTypes.bool,
    name: PropTypes.string,
    productType: PropTypes.shape({ name: PropTypes.string }),
  }),
  className: PropTypes.string,
  select: PropTypes.func,
  index: PropTypes.number,
  selected: PropTypes.number,
  isSelected: PropTypes.bool,
  forSharedPage: PropTypes.bool,
  showPopup: PropTypes.func,
  fromMyFiles: PropTypes.bool,
  selectedArr: PropTypes.arrayOf(PropTypes.shape({})),
  removeFromSelected: PropTypes.func,
  addToSelected: PropTypes.func,
  deleteImageFromSlider: PropTypes.func,
  withCrop: PropTypes.bool,
  isEnhanceProduct: PropTypes.bool,
  token: PropTypes.string,
  uploadImages: PropTypes.func,
  uploadImagesEnhance: PropTypes.func,
  multiSelect: PropTypes.bool,
  withOutZoom: PropTypes.bool,
  onImageError: PropTypes.func,
  withoutMenu: PropTypes.bool,
  withChildren: PropTypes.bool,
  children: PropTypes.node,
  withOutDelete: PropTypes.bool,
};

function NewThumbnailCard(props) {
  const {
    data,
    className,
    select,
    index,
    selected,
    isSelected,
    forSharedPage,
    showPopup,
    fromMyFiles,
    selectedArr,
    removeFromSelected,
    addToSelected,
    deleteImageFromSlider,
    withCrop,
    isEnhanceProduct,
    token,
    uploadImagesEnhance,
    uploadImages,
    multiSelect,
    withOutZoom,
    onImageError,
    withoutMenu,
    withChildren,
    children,
    withOutDelete,
  } = props;

  const [isShowImage, toggleImagePopup] = useState(false);
  const [wrapRef, addWrapRef] = useState(null);

  useEffect(() => {
    if (!wrapRef) {
      const photoImg = new Image();
      photoImg.onerror = () => {
        onImageError(data.imageUri);
      };
      photoImg.src = data.imageUri;
    }
  });

  const upload = (file) => {
    const formData = new FormData();
    formData.append('files[]', file, file.name);
    formData.append('isCropped', false);

    const uploadData = { formData, token };
    if (isEnhanceProduct) {
      uploadImagesEnhance(uploadData);
    } else {
      uploadImages(uploadData);
    }
    toggleImagePopup(false);
  };

  const checkSelect = () => {
    const { id, imageUri } = data;
    if (fromMyFiles || forSharedPage || multiSelect) {
      return Boolean(selectedArr.find((item) => item.id === id && item.imageUri === imageUri));
    }
    return id === selected || isSelected;
  };

  const onClick = (e, isAddButton) => {
    const value = {
      ...data,
      index,
    };
    if (e.target === wrapRef || isAddButton) {
      if (fromMyFiles || forSharedPage) {
        if (checkSelect()) {
          removeFromSelected(data);
        } else {
          addToSelected(data);
        }
      } else {
        select(value);
      }
    }
  };

  const openImagePopup = () => {
    if (fromMyFiles || forSharedPage) {
      showPopup(data);
    } else {
      toggleImagePopup(true);
    }
  };

  const { imageUri, imageKey, name } = data;

  const isImageSelected = checkSelect();

  return (
    <div className={b({ mix: className, select: isImageSelected })} ref={addWrapRef}>
      <div className={b('content-block', { select: isImageSelected })}>
        <button
          className={b('image-button')}
          type="button"
          onClick={(e) => onClick(e, true)}
        >
          <div className={b('image-wrap')}>
            <img
              className={b('image-block')}
              src={imageKey ? formattedImage({imagePath: imageKey, needResize: true, imageSize: '126x'}) : imageUri}
              alt={name}
            />
          </div>
        </button>
        {!withOutDelete && (
          <button
            className={b('delete-button')}
            type="button"
            onClick={() => {
              deleteImageFromSlider(data, isImageSelected);
            }}
          />
        )}
      </div>
      {!withoutMenu && (
        <div className={b('menu-block')}>
          {!withOutZoom && (
            <button className={b('zoom-button')} type="button" onClick={openImagePopup}>
              <MagnifyingPlus2 className={b('zoom-icon')} />
            </button>
          )}
          {withChildren && children}
        </div>
      )}
      {isShowImage &&
        ReactDOM.createPortal(
          withCrop ? (
            <CropPopup imageUri={imageUri} close={() => toggleImagePopup(false)} upload={upload} />
          ) : (
            <ImagePopup imageUri={imageUri} close={() => toggleImagePopup(false)} />
          ),
          document.getElementById('__next') || null,
        )}
    </div>
  );
}

NewThumbnailCard.propTypes = propTypes;
NewThumbnailCard.defaultProps = defaultProps;

const actions = {
  uploadImages: loadImages,
  uploadImagesEnhance: loadImagesOrderEnhance,
};

const stateProps = (state) => ({
  token: getToken(state),
});

export default connect(stateProps, actions)(NewThumbnailCard);
