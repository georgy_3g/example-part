import React from 'react';
import bem from 'src/utils/bem';
import generateRotateStyle from 'src/utils/generateRotateStyle';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { PHOTO_ART_TERMS } from 'src/terms';
import { ENHANCE_PRODUCTS } from 'src/constants';

import orderImage from 'public/img/shared/navi-img.png';
import audioImage from 'public/img/shared/product-icons/audio-checkout-icon-225x160.jpg';
import deviceImage from 'public/img/shared/product-icons/device-checkout-icon-225x160.jpg';
import filmImage from 'public/img/shared/product-icons/film-checkout-icon-225x160.jpg';
import scanImage from 'public/img/shared/product-icons/scan-checkout-icon-225x160.jpg';
import tapeImage from 'public/img/shared/product-icons/tape-checkout-icon-225x160.jpg';
import easyScanImage from 'public/img/shared/product-icons/ENHANCE-KIT-CHECKOUT.jpg';
import enhanceImage from 'public/img/shared/product-icons/ENHANCE-UPLOAD-CHECKOUT.jpg';
import { orderDetailsSelect } from 'src/redux/ordersList/selectors';
import { photoArtProductsSelector } from 'src/redux/photoArt/selectors';
import styles from './index.module.scss';
import TERMS from './constants';

const { PHOTO_ART_PRODUCT } = PHOTO_ART_TERMS;

const b = bem('order-details-card', styles);

const {
  GIFT,
  ADD_WRAPPING,
  NO,
  COLON,
  DIGITIZE_PRODUCTS,
  SCAN_SHIP_KIT_DETAILS,
  LARGEST_SIZE,
  MATERIAL_TYPE,
  SIZE,
  COLOR,
  COLLECTION,
  MATTING,
  MATTING_COLOR,
  PERSONALIZE,
  N_A,
  PERSONALIZE_PRICE,
  DISPLAY_OPTION,
  PHOTO_ART,
} = TERMS;

const defaultProps = {
  orderDetails: {
    id: '',
    items: [],
  },
  photoArtProducts: {},
};

const propTypes = {
  orderDetails: PropTypes.shape({
    id: PropTypes.number,
    items: PropTypes.arrayOf(PropTypes.shape({})),
  }),
  photoArtProducts: PropTypes.shape({}),
};

const OrderDetailsCard = (props) => {
  const { orderDetails, photoArtProducts } = props;
  const items = orderDetails.items ? orderDetails.items : [];

  const getDefaultImage = (typeProduct, isEasyScanKit) => {
    switch (typeProduct) {
      case 'tape_transfer':
        return { imageUri: tapeImage };
      case 'film_transfer':
        return { imageUri: filmImage };
      case 'audio_transfer':
        return { imageUri: audioImage };
      case 'digital_transfer':
        return { imageUri: deviceImage };
      case 'photo_scan':
        return { imageUri: scanImage };
      case 'photo_restoration':
      case 'photo_colorization':
      case 'photo_retouching':
        return { imageUri: isEasyScanKit ? easyScanImage : enhanceImage };
      default:
        return { imageUri: orderImage };
    }
  };

  const getPhotoArtPhoto = (order) => {
    const {
      options,
      product: {
        id: productId,
        frameMaterial: {
          id: materialId
        } = {}
      } = {},
      frameDisplay,
    } = order || {};

    const { product: { id: optionId } = {} } = options[0] || {};
    const orderImages = order.orderImages || [];

    const image = orderImages.find((item) => item.isCropped) || orderImages[0];

    const photoImageUry = image ? image.imageUri : '';

    const { frames = [] } = photoArtProducts[materialId] || {};

    const selectedProduct = frames.find(({ id }) => id === productId);

    const { options: productOptions } = selectedProduct;

    const selectedOption = productOptions.find(({ id }) => id === optionId);

    const selectedDisplay = selectedOption.frameDisplays.find(({ id }) => frameDisplay && id === frameDisplay.id)
      || selectedOption.frameDisplays[0];
    const { minDisplay, frameImageOptions, frameImageUri } = selectedDisplay || {};


    const {
      frameScale,
      frameScaleX,
      frameLeft,
      frameTop,
      frameRotateX,
      frameRotateY,
      frameRotateZ,
      framePerspective,
      framePerspectiveOrigin,
      imageUri,
      backgroundLeft,
      backgroundScale,
      backgroundTop,
    } = minDisplay;

    const top = 0;
    const left = 0;

    const { height, width, x, y } = frameImageOptions || {};

    const frameBlockStyle = {
      height: `${frameScale}%`,
      width: `${frameScaleX}%`,
      left: `${frameLeft}%`,
      top: `${frameTop}%`,
      perspective: `${framePerspective}px`,
      perspectiveOrigin: framePerspectiveOrigin,
    };

    const blockStyle = {
      height: `${height}%`,
      width: `${width}%`,
      left: `${x}%`,
      top: `${y}%`,
      transform: generateRotateStyle(frameRotateX, frameRotateY, frameRotateZ),
    };

    const imageStyle = {
      height: `${100 - top * 2}%`,
      width: `${100 - left * 2}%`,
      top: `${top}%`,
      left: `${left}%`,
    };

    return (
      <div className={b('art-photo-block')}>
        <div className={b('art-wrap')}>
          <img
            className={b('display')}
            src={imageUri}
            alt="display"
            style={{
              width: `${backgroundScale}%`,
              left: `-${backgroundLeft}%`,
              top: `-${backgroundTop}%`,
            }}
          />
          <div className={b('frame-wrap')} style={frameBlockStyle}>
            <div className={b('image-wrap')} style={blockStyle}>
              <img
                className={b('art-photo')}
                style={imageStyle}
                src={photoImageUry}
                alt="art"
              />
            </div>
            <img
              className={b('frame-image')}
              src={frameImageUri}
              alt="frame"
            />
          </div>
        </div>
      </div>
    );
  };

  const generateItem = (itemData) => {
    const {
      product,
      quantity,
      options,
      matting,
      mattingColor,
      price,
      personalDiscount,
      isGift,
      giftOptions,
      customText,
      frameDisplay,
    } = itemData;
    const productCustomPrice = personalDiscount ? (price - personalDiscount) : price;
    const isEasyScanKit = options && options.length && options[0].name === 'easy_scan_ship_kit';
    const defaultImageObject = getDefaultImage(product.name, isEasyScanKit);
    const giftSum = isGift && giftOptions.addWrapping ? 15 : 0;
    const newGiftOptions = giftOptions ? Object.entries(giftOptions).reduce((acc, [key, value]) => {
      if (key === ADD_WRAPPING) {
        return {
          ...acc,
          wrapping: value ? '1 x 15 = $15' : NO,
        };
      }
      return {
        ...acc,
        [key]: value,
      }}, {})
    : {};
    const getProductItemName = (type, optionName, optionQuantity) => {
      const { name, productType } = product;
      if (type !== 'option' && (productType === 'digitize' || productType === 'enhance')) {
        const productName = name.split('_')[0];
        if (productName === 'digital') {
          return quantity > 1 ? `${quantity} items` : `${quantity} item`;
        }
        if (productName === 'film') {
          return quantity > 1 ? `${quantity} feet` : `${quantity} foot`;
        }
        return quantity > 1 ? `${quantity} ${productName}s` : `${quantity} ${productName}`;
      }
      if (type === 'option' && optionQuantity) {
        return optionName === 'cloud_download' ? 'yes' : optionQuantity;
      }
      return '';
    }
    const {
      optionsSum,
      optionsFullSum,
      enhancement,
      deliverables,
    } = options.reduce(
      (acc, option) => {
        const {
          quantity: optionQuantity,
          product: optionProduct,
          price: optionPrice,
          personalDiscount: optionPersonalDiscount,
        } = option;
        const { name, display_name: optionDisplayName, displayName } = optionProduct;
        const optionCustomPrice = optionPersonalDiscount ? (optionPrice - optionPersonalDiscount) : optionPrice;
        if (name === 'enhance_my_transfer') {
          return {
            ...acc,
            optionsSum: acc.optionsSum + optionCustomPrice * optionQuantity + giftSum,
            optionsFullSum: acc.optionsFullSum + optionPrice * optionQuantity + giftSum,
            enhancement: {
              name,
              displayName: displayName || optionDisplayName,
              quantity: optionQuantity,
              optionPrice,
              personalDiscount: optionPersonalDiscount,
              optionSum: optionQuantity * optionCustomPrice,
            },
          };
        }
        return {
          ...acc,
          optionsSum: acc.optionsSum + optionCustomPrice * optionQuantity,
          optionsFullSum: acc.optionsFullSum + optionPrice * optionQuantity,
          deliverables: [
            ...acc.deliverables,
            {
              name,
              displayName: displayName || optionDisplayName,
              quantity: optionQuantity,
              optionPrice,
              personalDiscount: optionPersonalDiscount,
              optionSum: optionQuantity * optionCustomPrice,
            },
          ],
        };
      },
      {
        optionsSum: 0,
        optionsFullSum: 0,
        enhancement: null,
        deliverables: [],
      },
    );
    const productSum = quantity * productCustomPrice + optionsSum;
    const productFullSum = quantity * price + optionsFullSum;
    if (product.productType === 'shipping') {
      return null;
    }
    switch (product.name) {
      case PHOTO_ART_PRODUCT: {
        return (
          <div className={b('product-wrapper')}>
            {getPhotoArtPhoto(itemData)}
            <div className={b('product-card')}>
              <div className={b('product-title')}>
                <div className={b('product-name')}>{PHOTO_ART}</div>
                <div className={b('product-price-wrapper')}>
                  <span className={b('product-price', { origin: productFullSum !== productSum })}>
                    {customText
                      ? `$${(productFullSum + PERSONALIZE_PRICE).toFixed(2)}`
                      : `$${productFullSum.toFixed(2)}`}
                  </span>
                  {productFullSum !== productSum && (
                    <span className={b('product-price')}>
                      {customText
                        ? `$${(productSum + PERSONALIZE_PRICE).toFixed(2)}`
                        : `$${productSum.toFixed(2)}`}
                    </span>
                  )}
                </div>
              </div>
              <div className={b('product-info', { art: true })}>
                <div className={b('product-options', { art: true })}>
                  {options.map(({
                    quantity: optionQuantity,
                    price: optionPrice,
                    personalDiscount: optionPersonalDiscount,
                    product: {
                      name: optionName,
                    }
                  }) => (
                    <div className={b('product-quantity')}>
                      <div className={b('product-name-wrapper')}>
                        <span className={b('product-quantity-title')}>{product.displayName}</span>
                        <div className={b('product-quantity-value-wrapper')}>
                          <span className={b('product-quantity-value', { origin: Boolean(optionPersonalDiscount) })}>
                            {`$${Number(optionPrice).toFixed(2)}`}
                          </span>
                          {Boolean(optionPersonalDiscount) && (
                            <span className={b('product-quantity-value', { desktop: true })}>
                              {`$${Number(optionPrice - optionPersonalDiscount).toFixed(2)}`}
                            </span>
                          )}
                        </div>
                      </div>
                      <div className={b('product-quantity-count')}>
                        <span className={b('product-quantity-value')}>
                          {getProductItemName('option', optionName, optionQuantity)}
                        </span>
                      </div>
                      <div className={b('product-quantity-sum')}>
                        <span className={b('product-quantity-value', { desktop: true })}>
                          {`$${Number(optionQuantity * (optionPersonalDiscount ? (optionPrice - optionPersonalDiscount) : optionPrice)).toFixed(2)}`}
                        </span>
                      </div>
                    </div>
                  ))}
                  <div className={b('product-deliverables')}>
                    {options.map(({ product: { frameSize, frameColor } }) => (
                      <>
                        <div className={b('product-option')}>
                          <span className={b('product-option-title')}>{COLLECTION}</span>
                          <span className={b('product-option-value')}>
                            {product.frameMaterial ? product.frameMaterial.name : ''}
                          </span>
                        </div>
                        <div className={b('product-option')}>
                          <span className={b('product-option-title')}>{SIZE}</span>
                          <span className={b('product-option-value')}>
                            {frameSize ? frameSize.name : ''}
                          </span>
                        </div>
                        {frameColor && frameColor.name !== N_A && (
                          <div className={b('product-option')}>
                            <span className={b('product-option-title')}>{COLOR}</span>
                            <span className={b('product-option-value')}>
                              {frameColor ? frameColor.name : ''}
                            </span>
                          </div>
                        )}
                        {customText ? (
                          <div className={b('product-option')}>
                            <span className={b('product-option-title')}>{`${PERSONALIZE}:`}</span>
                            <span
                              className={b('product-option-value')}
                            >
                              {`+${PERSONALIZE_PRICE}`}
                            </span>
                          </div>
                        ) : null}
                        {frameDisplay && frameDisplay.id ? (
                          <div className={b('product-option')}>
                            <span className={b('product-option-title')}>{DISPLAY_OPTION}</span>
                            <span className={b('product-option-value')}>{frameDisplay.name}</span>
                          </div>
                        ) : null}
                      </>
                    ))}
                  </div>
                  {matting ? (
                    <div className={b('product-deliverables')}>
                      <div className={b('product-deliverables-title')}>{MATTING}</div>
                      <div className={b('product-option')}>
                        <span className={b('product-option-title')}>{MATTING_COLOR}</span>
                        <span className={b('product-option-value')}>
                          {mattingColor && mattingColor.name ? mattingColor.name : ''}
                        </span>
                      </div>
                    </div>
                  ) : null}
                  {isGift ? (
                    <div className={b('product-gift')}>
                      <div className={b('product-gift-title')}>{GIFT}</div>
                      {Object.entries(newGiftOptions).map(([key, value]) => (
                        <div className={b('product-option')}>
                          <span className={b('product-option-title')}>{`${key}${COLON}`}</span>
                          <span className={b('product-option-value')}>{`${value}`}</span>
                        </div>
                      ))}
                    </div>
                  ) : null}
                </div>
              </div>
            </div>
          </div>
        );
      }
      default: {
        return (
          <div className={b('product-wrapper')}>
            <img className={b('product-image')} src={defaultImageObject.imageUri.src} alt="art" />
            <div className={b('product-card')}>
              <div className={b('product-title')}>
                <div className={b('product-name')}>{product.displayName}</div>
                <div className={b('product-price-wrapper')}>
                  <span className={b('product-price', { origin: productFullSum !== productSum })}>
                    {`$${productFullSum.toFixed(2)}`}
                  </span>
                  {productFullSum !== productSum && (
                    <span className={b('product-price')}>{`$${productSum.toFixed(2)}`}</span>
                  )}
                </div>
              </div>
              <div className={b('product-info')}>
                <div className={b('product-main-info')}>
                  <div className={b('product-quantity')}>
                    <div className={b('product-name-wrapper')}>
                      <span className={b('product-quantity-title')}>{product.displayName}</span>
                      <div className={b('product-quantity-value-wrapper')}>
                        <span className={b('product-quantity-value', { origin: Boolean(personalDiscount) })}>
                          {`$${Number(price).toFixed(2)}`}
                        </span>
                        {Boolean(personalDiscount) && (
                          <span className={b('product-quantity-value', { desktop: true })}>
                            {`$${Number(price - personalDiscount).toFixed(2)}`}
                          </span>
                        )}
                      </div>
                    </div>
                    <div className={b('product-quantity-count')}>
                      <span className={b('product-quantity-value')}>
                        {getProductItemName()}
                      </span>
                    </div>
                    <div className={b('product-quantity-sum')}>
                      <span className={b('product-quantity-value', { desktop: true })}>
                        {`$${Number(quantity * (personalDiscount ? (price - personalDiscount) : price)).toFixed(2)}`}
                      </span>
                    </div>
                  </div>
                  {enhancement && Object.keys(enhancement).length ? (
                    <div className={b('product-quantity')}>
                      <div className={b('product-name-wrapper')}>
                        <span className={b('product-quantity-title')}>{enhancement.displayName}</span>
                        <div className={b('product-quantity-value-wrapper')}>
                          <span className={b('product-quantity-value', { origin: Boolean(enhancement.personalDiscount) })}>
                            {`$${Number(enhancement.optionPrice).toFixed(2)}`}
                          </span>
                          {Boolean(enhancement.personalDiscount) && (
                            <span className={b('product-quantity-value', { desktop: true })}>
                              {`$${Number(enhancement.optionPrice - enhancement.personalDiscount).toFixed(2)}`}
                            </span>
                          )}
                        </div>
                      </div>
                      <div className={b('product-quantity-count')}>
                        <span className={b('product-quantity-value')}>
                          {getProductItemName('option', enhancement.name, enhancement.quantity)}
                        </span>
                      </div>
                      <div className={b('product-quantity-sum')}>
                        <span className={b('product-quantity-value', { desktop: true })}>
                          {`$${Number(enhancement.optionSum).toFixed(2)}`}
                        </span>
                      </div>
                    </div>
                  ) : null}
                </div>
                <div className={b('product-options')}>
                  {deliverables &&
                    Boolean(deliverables.length) &&
                    (DIGITIZE_PRODUCTS.includes(product.name) ||
                      (ENHANCE_PRODUCTS.includes(product.name) && options.length)) && (
                      <div className={b('product-deliverables')}>
                        {deliverables.map(
                          (item) =>
                          item.quantity !== 0 && (
                            <div className={b('product-quantity')}>
                              <div className={b('product-name-wrapper')}>
                                <span className={b('product-quantity-title')}>{item.displayName}</span>
                                <div className={b('product-quantity-value-wrapper')}>
                                  <span className={b('product-quantity-value', { origin: Boolean(item.personalDiscount) })}>
                                    {`$${Number(item.optionPrice).toFixed(2)}`}
                                  </span>
                                  {Boolean(item.personalDiscount) && (
                                    <span className={b('product-quantity-value', { desktop: true })}>
                                      {`$${Number(item.optionPrice - item.personalDiscount).toFixed(2)}`}
                                    </span>
                                  )}
                                </div>
                              </div>
                              <div className={b('product-quantity-count')}>
                                <span className={b('product-quantity-value')}>
                                  {getProductItemName('option', item.name, item.quantity)}
                                </span>
                              </div>
                              <div className={b('product-quantity-sum')}>
                                <span className={b('product-quantity-value', { desktop: true })}>
                                  {`$${Number(item.optionSum).toFixed(2)}`}
                                </span>
                              </div>
                            </div>
                          ),
                        )}
                      </div>
                    )}
                  {options &&
                    Boolean(options.length) &&
                    options[0].product.name === 'easy_scan_ship_kit' &&
                    ENHANCE_PRODUCTS.includes(product.name) && (
                      <div className={b('product-deliverables')}>
                        <div className={b('product-deliverables-title')}>
                          {SCAN_SHIP_KIT_DETAILS}
                        </div>
                        {options.map(
                          ({ product: { shipKitMaterial, shipKitSize, name } }) =>
                            name === 'easy_scan_ship_kit' && (
                              <>
                                <div className={b('product-option')}>
                                  <span className={b('product-option-title')}>{LARGEST_SIZE}</span>
                                  <span className={b('product-option-value')}>
                                    {shipKitSize ? shipKitSize.name : ''}
                                  </span>
                                </div>
                                <div className={b('product-option')}>
                                  <span className={b('product-option-title')}>{MATERIAL_TYPE}</span>
                                  <span className={b('product-option-value')}>
                                    {shipKitMaterial ? shipKitMaterial.name : ''}
                                  </span>
                                </div>
                              </>
                            ),
                        )}
                      </div>
                    )}
                  {isGift ? (
                    <div className={b('product-gift')}>
                      <div className={b('product-gift-title')}>{GIFT}</div>
                      {Object.entries(newGiftOptions).map(([key, value]) => (
                        <div className={b('product-option')}>
                          <span className={b('product-option-title')}>{`${key}${COLON}`}</span>
                          <span className={b('product-option-value')}>{`${value}`}</span>
                        </div>
                      ))}
                    </div>
                  ) : null}
                </div>
              </div>
            </div>
          </div>
        );
      }
    }
  };

  return (
    <>
      {items.map(
        ({
          id,
          product,
          quantity,
          options,
          matting,
          price,
          personalDiscount,
          mattingColor,
          customText,
          shipKitMaterial,
          shipKitSize,
          isGift,
          giftOptions,
          orderImages,
          frameDisplay,
        }) => (
          <div key={id} className={b('order-info')}>
            {generateItem({
              product,
              quantity,
              options,
              matting,
              mattingColor,
              customText,
              shipKitMaterial,
              shipKitSize,
              price,
              personalDiscount,
              isGift,
              giftOptions,
              orderImages,
              frameDisplay,
            })}
          </div>
        ),
      )}
    </>
  );
}

OrderDetailsCard.propTypes = propTypes;
OrderDetailsCard.defaultProps = defaultProps;

const stateProps = (state) => ({
  orderDetails: orderDetailsSelect(state),
  photoArtProducts: photoArtProductsSelector(state),
});

export default connect(stateProps, null)(OrderDetailsCard);
