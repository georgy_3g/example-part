import React, { useState } from 'react';
import ReactDOM from 'react-dom';
import bem from 'src/utils/bem';
import formattedImage from 'src/utils/formattedImage';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import colors from 'src/styles/colors.json';
import dynamic from 'next/dynamic';
import { loadImages, loadImagesOrderEnhance } from 'src/redux/orderImages/actions';
import { getToken } from 'src/redux/auth/selectors';
import styles from './index.module.scss';

const CheckFull = dynamic(() => import('src/components/svg/CheckFull'));
const MagnifyingPlus2 = dynamic(() => import('src/components/svg/MagnifyingPlus2'));
const ImageMenu = dynamic(() => import('src/components/elements/ImageMenu'));
const ImagePopup = dynamic(() => import('src/components/popups/ImagePopup'));
const CropPopup = dynamic(() => import('src/components/popups/CropPopup'));

const whiteColor = colors['$white-color'];
const greenColor = colors['$summer-green-color'];
const greyColor = colors['$slate-gray-color'];
const darkBlue = colors['$shark-gray-color'];

const b = bem('cloud-image-card', styles);

const defaultProps = {
  data: {},
  className: '',
  select: () => {},
  selected: 0,
  isSelected: false,
  forSharedPage: false,
  fromMyFiles: false,
  selectedArr: [],
  removeFromSelected: () => {},
  addToSelected: () => {},
  withCrop: false,
  isEnhanceProduct: false,
  uploadImages: () => {},
  uploadImagesEnhance: () => {},
  token: '',
  showPopup: () => {},
  multiSelect: false,
  withOutZoom: false,
  orderPrints: () => {},
  isLockDigitizeFiles: false,
  enhancePhoto: () => {},
  withOutSelect: false,
  clickOrderPrints: () => {},
};

const propTypes = {
  data: PropTypes.shape({
    id: PropTypes.number,
    imageUri: PropTypes.string,
    imageKey: PropTypes.string,
    watermarkedImageUri: PropTypes.string,
    parentId: PropTypes.number,
    isApproved: PropTypes.bool,
    isTemplate: PropTypes.bool,
    name: PropTypes.string,
    productType: PropTypes.shape({ name: PropTypes.string }),
  }),
  className: PropTypes.string,
  select: PropTypes.func,
  selected: PropTypes.number,
  isSelected: PropTypes.bool,
  forSharedPage: PropTypes.bool,
  showPopup: PropTypes.func,
  fromMyFiles: PropTypes.bool,
  selectedArr: PropTypes.arrayOf(PropTypes.shape({})),
  removeFromSelected: PropTypes.func,
  addToSelected: PropTypes.func,
  withCrop: PropTypes.bool,
  isEnhanceProduct: PropTypes.bool,
  token: PropTypes.string,
  uploadImages: PropTypes.func,
  uploadImagesEnhance: PropTypes.func,
  multiSelect: PropTypes.bool,
  withOutZoom: PropTypes.bool,
  orderPrints: PropTypes.func,
  isLockDigitizeFiles: PropTypes.bool,
  enhancePhoto: PropTypes.func,
  withOutSelect: PropTypes.bool,
  clickOrderPrints: PropTypes.func,
};

const CloudImageCard = (props) => {
  const {
    data,
    className,
    select,
    selected,
    isSelected,
    forSharedPage,
    showPopup,
    fromMyFiles,
    selectedArr,
    removeFromSelected,
    addToSelected,
    withCrop,
    isEnhanceProduct,
    token,
    uploadImagesEnhance,
    uploadImages,
    multiSelect,
    withOutZoom,
    orderPrints,
    isLockDigitizeFiles,
    enhancePhoto,
    withOutSelect,
    clickOrderPrints,
  } = props;

  const [isShowImage, toggleImagePopup] = useState(false);
  const [wrapRef, addWrapRef] = useState(null);

  const upload = (file) => {
    const formData = new FormData();
    formData.append('files[]', file, file.name);
    formData.append('isCropped', false);

    const uploadData = { formData, token };
    if (isEnhanceProduct) {
      uploadImagesEnhance(uploadData);
    } else {
      uploadImages(uploadData);
    }
    toggleImagePopup(false);
  };

  const checkSelect = () => {
    const { id, imageUri } = data;
    if (fromMyFiles || forSharedPage || multiSelect) {
      return selectedArr.some((item) => item.id === id && item.imageUri === imageUri);
    }
    return id === selected || isSelected;
  };

  const onClick = (e, isAddButton) => {
    const value = { ...data, isMyCloud: true };
    if (e.target === wrapRef || isAddButton) {
      if (fromMyFiles || forSharedPage) {
        if (checkSelect()) {
          removeFromSelected(value);
        } else {
          addToSelected(value);
        }
      } else {
        select(value);
      }
    }
    e.stopPropagation();
  };

  const openImagePopup = () => {
    if (fromMyFiles || forSharedPage) {
      showPopup(data);
    } else {
      toggleImagePopup(true);
    }
  };

  const { imageUri, imageKey, name } = data;

  const isImageSelected = checkSelect();

  return (
    <div className={b({ mix: className, select: isImageSelected })} ref={addWrapRef}>
      <div className={b('content-block')}>
        <button className={b('image-button')} type="button" onClick={openImagePopup}>
          <div className={b('image-wrap')}>
            <img className={b('image-block')} src={formattedImage({imagePath: imageKey, needResize: true, imageSize: '200x'}) || imageUri} alt={name} />
          </div>
        </button>
        <button className={b('select-btn')} type="button" onClick={(e) => onClick(e, true)}>
          <CheckFull
            className={b('select-icon')}
            stroke={isImageSelected ? greenColor : whiteColor}
            stroke2={isImageSelected ? whiteColor : 'none'}
            fill={isImageSelected ? greenColor : `${greyColor}88`}
          />
        </button>
      </div>
      <div className={b('menu-block')}>
        <p className={b('name-image')}>{name}</p>
        {!withOutZoom && (
          <button className={b('zoom-button')} type="button" onClick={openImagePopup}>
            <MagnifyingPlus2 className={b('zoom-icon')} />
          </button>
        )}
        <button className={b('menu-button')} type="button">
          <ImageMenu
            className={b('menu-icon')}
            iconFillColor={darkBlue}
            imageData={data}
            forSharedPage={forSharedPage}
            enhancePhoto={enhancePhoto}
            portalBlock={wrapRef}
            orderPrints={orderPrints}
            selectToCreate={onClick}
            withOutSelect={withOutSelect}
            isLockDigitizeFiles={isLockDigitizeFiles}
            clickOrderPrints={clickOrderPrints}
            withText
            text="Actions"
          />
        </button>
      </div>
      {isShowImage &&
        ReactDOM.createPortal(
          withCrop ? (
            <CropPopup imageUri={imageUri} close={() => toggleImagePopup(false)} upload={upload} />
          ) : (
            <ImagePopup imageUri={imageUri} close={() => toggleImagePopup(false)} />
          ),
          document.getElementById('__next') || null,
        )}
    </div>
  );
}

CloudImageCard.propTypes = propTypes;
CloudImageCard.defaultProps = defaultProps;

const actions = {
  uploadImages: loadImages,
  uploadImagesEnhance: loadImagesOrderEnhance,
};

const stateProps = (state) => ({
  token: getToken(state),
});

export default connect(stateProps, actions)(CloudImageCard);
