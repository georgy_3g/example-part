import React, { Component } from 'react';
import bem from 'src/utils/bem';
import PropTypes from 'prop-types';
import dynamic from "next/dynamic";
import Image from 'next/image';
import styles from './index.module.scss';
import HELP_CENTER from './constants';

const CloseIcon2 = dynamic(() => import('src/components/svg/CloseIcon2'));
const YouTubeVideo = dynamic(() => import('src/components/widgets/YouTubeVideo'));

const b = bem('help-center-card', styles);

const defaultProps = {
  closePopUp: () => {},
  helpItem: {},
};

const propTypes = {
  closePopUp: PropTypes.func,
  helpItem: PropTypes.shape({
    title: PropTypes.string,
    description: PropTypes.string,
    images: PropTypes.arrayOf(PropTypes.shape({})),
    id: PropTypes.number,
  })
};

class HelpCenterFrameCard extends Component {
  
  constructor(props) {
    super(props);
    this.veil = React.createRef();

    const { helpItem } = props;
    const { images } = helpItem;

    this.state = {
      selectedImage: images ? images[0] : {},
    };
  }

  closeModal = (e) => {
    const { closePopUp } = this.props;
    if (e.target === this.veil.current) {
      closePopUp();
    }
  };

  closeModalButton = () => {
    const { closePopUp } = this.props;
    closePopUp();
  };

  selectImage = (image) => {
    this.setState({ selectedImage: image });
  }

  render() {
    const { helpItem } = this.props;
    const { selectedImage } = this.state;
    const { description } = helpItem;

    const { images } = helpItem;

    const newImages = images && images.length && [...images].sort((itemOne, itemTwo) => itemOne.index - itemTwo.index);

    return (
      <div className={b()} ref={this.veil} onClick={this.closeModal} role="button" tabIndex="0">
        <div className={b('wrapper')}>
          <div className={b('select')}>
            <span className={b('select-title')}>{helpItem.title || HELP_CENTER}</span>
          </div>
          <div className={b('content', { 'no-images': !newImages })}>
            { Boolean(newImages) && (
              <div className={b('block-images')}>
                <div className={b('main-image')}>
                  {selectedImage.iFrame ? (
                    <YouTubeVideo 
                      className={b('video')}
                      contentIframe={selectedImage.iFrame}
                    />
                    ) : (
                      <Image 
                        className={b('image')}
                        src={selectedImage.imageUri}
                        layout="fill"
                      />
                    )}
                </div>
                <div className={b('images-list')}>
                  {newImages.map(item => (
                    <button 
                      className={b('item-image')}
                      type="button"
                      onClick={() => { this.selectImage(item); }}
                    >
                      {item.iFrame ? (
                        <> 
                          <YouTubeVideo
                            className={b('video')}
                            contentIframe={item.iFrame}
                          />
                          <div className={b('veil-video')} />
                        </>

                        ) : (
                          <Image 
                            className={b('image')}
                            src={item.imageUri}
                            layout="fill"
                          />
                        )}
                    </button>
                    ))}
                </div>
              </div>
            )}
            <div
              className={b('description', { 'no-images': !newImages})}
              dangerouslySetInnerHTML={{ __html: description }}
            />
          </div>
          <button className={b('close-btn')} type="button" onClick={this.closeModalButton}>
            <CloseIcon2 />
          </button>
        </div>
      </div>
    );
  }
}

HelpCenterFrameCard.propTypes = propTypes;
HelpCenterFrameCard.defaultProps = defaultProps;

export default HelpCenterFrameCard;
