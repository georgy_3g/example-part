const SELECTED_TEXT = 'Selected';
const CREATE_TEXT = 'Create';
const FROM_TEXT = 'from ';
const DOLLAR = '$';
const INTERVAL_SLIDER = 4000;

export { SELECTED_TEXT, CREATE_TEXT, FROM_TEXT, DOLLAR, INTERVAL_SLIDER };
