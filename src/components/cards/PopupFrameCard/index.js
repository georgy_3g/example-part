import React from 'react';
import bem from 'src/utils/bem';
import generateRotateStyle from 'src/utils/generateRotateStyle';
import useImageOrientation from 'src/utils/useImageOrientation';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { ORIENTATION_TYPES } from 'src/constants';
import { approvedImageSelect } from 'src/redux/approvedOrderImage/selectors';
import { CREATE_TEXT, DOLLAR, FROM_TEXT } from './constants';
import styles from './index.module.scss';

const b = bem('popup-frame-card', styles);

const defaultProps = {
  data: {},
  selected: {},
  className: '',
  select: () => {},
  photo: {},
  imageData: null,
  withOutStorePhoto: false,
};

const propTypes = {
  data: PropTypes.shape({
    photoArtImages: PropTypes.arrayOf(PropTypes.shape({})),
    id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    displayName: PropTypes.string,
    price: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    hint: PropTypes.string,
    productHintImages: PropTypes.arrayOf(PropTypes.shape({})),
    frameDisplays: PropTypes.arrayOf(PropTypes.shape({})),
    options: PropTypes.arrayOf(PropTypes.shape({})),
    imageUri: PropTypes.string,
  }),
  photo: PropTypes.shape({
    imageUri: PropTypes.string,
    id: PropTypes.number,
  }),
  selected: PropTypes.shape({
    id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  }),
  className: PropTypes.string,
  select: PropTypes.func,
  imageData: PropTypes.shape({
    imageUri: PropTypes.string,
    id: PropTypes.number,
  }),
  withOutStorePhoto: PropTypes.bool,
};

function PopupFrameCard(props) {
  const { data, select, className, photo, imageData, withOutStorePhoto } = props;

  const { displayName, options, imageUri: defaultPhoto } = data;

  const storePhoto = withOutStorePhoto ? null : photo;

  const { imageUri: photoUri } = imageData || storePhoto || { imageUri: defaultPhoto };

  const { isVertical } = useImageOrientation(photoUri);

  const btnText = CREATE_TEXT;

  const onClick = () => {
    select(data);
  };
  const imageOrientation = isVertical ? ORIENTATION_TYPES.vertical : ORIENTATION_TYPES.horizontal;

  const defaultOption = options.find(
    (item) => item.frameOrientation === imageOrientation && item.isDefaultOption,
  );

  const randomOption = options.find((item) => item.frameOrientation === imageOrientation);

  const selectedOption = defaultOption || randomOption || options[0];

  const {
    imageUri: displaysUri,
    frameRotateX,
    frameRotateY,
    frameRotateZ,
    frameScale,
    frameScaleX,
    frameTop,
    frameLeft,
    framePerspective,
    framePerspectiveOrigin,
  } = selectedOption.frameDisplays[0].minDisplay || {};

  const wrapStyle = {
    height: `${frameScale}%`,
    width: `${frameScaleX}%`,
    left: `${frameLeft}%`,
    top: `${frameTop}%`,
    transform: generateRotateStyle(frameRotateX, frameRotateY, frameRotateZ),
    backgroundImage: `url(${photoUri})`,
  };

  const minPrice = Math.min(...options.map(({ price }) => price));

  const { frameColor: { name: color = '' } = {}, frameSize: { name: size = '' } = {} } =
    selectedOption || {};

  const alt = `${displayName} ${color} ${size}`;

  return (
    <article className={b({ mix: className })}>
      <div className={b('image-block')} role="button" onClick={onClick} tabIndex="0">
        <div className={b('photo-block')}>
          <div
            className={b('photo-wrap')}
            style={{
              perspective: `${framePerspective}px`,
              perspectiveOrigin: framePerspectiveOrigin,
            }}
          >
            <div className={b('image')} style={wrapStyle} />
          </div>
          <div className={b('image-frame')} style={{ backgroundImage: `url(${displaysUri})` }}>
            <img className={b('image-frame-abs')} src={displaysUri} alt={alt} />
          </div>
        </div>
      </div>
      <div className={b('info-block')}>
        <span className={b('name')}>{displayName}</span>
        <span className={b('price')}>{`${FROM_TEXT} ${DOLLAR}${minPrice}`}</span>
        <div className={b('button-wrap')}>
          <button className={b('btn')} onClick={onClick} type="button">
            {btnText}
          </button>
        </div>
      </div>
    </article>
  );
}

PopupFrameCard.propTypes = propTypes;
PopupFrameCard.defaultProps = defaultProps;

const stateProps = (state) => ({
  photo: approvedImageSelect(state),
});

export default connect(stateProps)(PopupFrameCard);
