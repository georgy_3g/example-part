export interface propsInterface {
  item: {
    images: {
      url: string,
      id: string,
      alt: string,
    }[],
    title: string,
    description: string,
    id: number,
    personalize: {
      frameDisplaysPersonalize: {
        characterLimit: number,
        lineLimit: number,
      }[],
    }
  },
  closePopUp: () => void,
  onSubmit: (idItem: number, text: string, isPersonalize: boolean) => void,
  initialText: string,
  minHeight: boolean,
}