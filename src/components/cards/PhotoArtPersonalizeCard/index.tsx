import React, { useRef, useState, FunctionComponent } from 'react';
import bem from 'src/utils/bem';
import colors from 'src/styles/colors.json';
import Colors from "@styles/colors.json";
import dynamic from "next/dynamic";
import {
  PLACEHOLDER,
  ADDITIONAL,
  NO_THANKS,
  YES_ADD,
  PRICE,
} from './constants';
import { propsInterface } from './interfase'
import styles from './index.module.scss';

const ColorButton = dynamic(() => import('src/components/buttons/ColorButton'));
const NewCustomInput = dynamic(() => import('src/components/inputs/NewCustomInput'));
const CloseIcon2 = dynamic(() => import('src/components/svg/CloseIcon2'));

const whiteColor = Colors['$white-color'];
const darkSlateBlueColor = Colors['$dark-slate-blue-color'];
const darkBlueColor = colors['$slate-gray-color'];
const redColor = colors['$burnt-sienna-red-color'];

const b = bem('photo-art-personalize-card', styles);

const defaultProps = {
  item: {
    images : [],
    title: '',
    description: '',
    id: 0,
    personalize: {
      frameDisplaysPersonalize: [],
    }
  },
  closePopUp: () => { return false; },
  onSubmit: () => { return false; },
  initialText: '',
  minHeight: false,
};

const PhotoArtPersonalizeCard: FunctionComponent<propsInterface> = (props) => {

  const {
    initialText,
    closePopUp,
    onSubmit,
    item,
    minHeight,
  } = props;

  const {
    title,
    description,
    personalize: {
      frameDisplaysPersonalize,
    },
  } = item;

  const { characterLimit, lineLimit } = frameDisplaysPersonalize[0];

  const veil = useRef<HTMLDivElement>(null);


  const emptyArray = Array.from({ length: lineLimit }, () => '');
  const arrayText = initialText ? initialText.split('\n') : emptyArray;

  const [arrText, setArrText] = useState(arrayText);

  const closeModal = (e: React.MouseEvent) => {
    if (e.target === veil.current) {
      closePopUp();
    }
  };

  const closeButton = () => {
    closePopUp();
  };

  const onChange = (value: string, indexArr: number) => {

    const newArrText = arrText.map((text, index) => {
      if (indexArr === index) {
        return value;
      }
      return text;
    });
    setArrText(newArrText);
  }

  const submit = (isPersonalize: boolean) => {
    const text = arrText.reduce((acc, itemText, index) => {
      if (index === 0) {
        return itemText;
      }
      return `${acc}\n${itemText}`;
    }, '');
    onSubmit(item.id, text, isPersonalize);
  }

    return (
      <div
        className={b()}
        ref={veil}
        onClick={closeModal}
        role="button"
        tabIndex={0}
      >
        <div className={b('wrapper', { 'min-height': minHeight })}>
          <div className={b('close-btn')} onClick={closeButton} role="button" tabIndex={0}>
            <CloseIcon2
              fill={darkSlateBlueColor}
              crossFill={whiteColor}
              className={b('close-icon')}
              width={30}
              height={30}
            />
          </div>
          <div className={b('card-content')}>
            <div className={b('title')}>{title}</div>
            <div className={b('sub-title')}>{description}</div>
            { arrText.map((text, index) => (
              <div className={b('input-wrapper')}>
                <NewCustomInput
                  className={b('input')}
                  onChange={({ target: { value } }: React.ChangeEvent<HTMLInputElement>) => onChange(value, index)}
                  name="text"
                  placeholder={PLACEHOLDER}
                  value={text}
                  maxLength={characterLimit}
                />
              </div>
            )
            )}

            <div className={b('description')}>{`${ADDITIONAL} ${PRICE}`}</div>
            <div className={b('buttons')}>
              <ColorButton
                backGroundColor={darkBlueColor}
                text={NO_THANKS}
                onClick={() => submit(false)}
              />
              <ColorButton
                backGroundColor={redColor}
                text={YES_ADD}
                onClick={() => submit(true)}
              />
            </div>
          </div>
        </div>
      </div>
    );
  };

PhotoArtPersonalizeCard.defaultProps = defaultProps;

export default PhotoArtPersonalizeCard;
