import React from 'react';
import PropTypes from 'prop-types';
import ReadMoreReact from 'read-more-react';
import bem from 'src/utils/bem';
import format from 'date-fns/format';
import Image from 'next/image';
import dynamic from "next/dynamic";
import styles from './index.module.scss';

const Rate = dynamic(() => import('src/components/elements/Rate'));

const b = bem('text-card', styles);

const defaultProps = {
  date_created: '',
  review: '',
  comments: '',
  reviewer: {},
  rating: '0',
  withReadMore: false,
  images: [],
};

const propTypes = {
  date_created: PropTypes.string,
  review: PropTypes.string,
  comments: PropTypes.string,
  reviewer: PropTypes.shape({
    first_name: PropTypes.string,
    last_name: PropTypes.string,
  }),
  rating: PropTypes.string,
  withReadMore: PropTypes.bool,
  images: PropTypes.arrayOf(PropTypes.shape({
    image: PropTypes.string,
  })),
};

function ClientCard(props) {
  const {
    date_created: date,
    review,
    comments,
    reviewer: { first_name: firstName, last_name: lastName },
    rating,
    images,
    withReadMore,
  } = props;
  return (
    <article className={b()}>
      <div className={b('header')}>
        <p className={b('date')}>{format(new Date(date), 'mm/dd/yyyy')}</p>
        <Rate rate={rating} />
      </div>
      <div className={b('review-container', { withImage: images.length })}>
        {images.length ? (
          <div className={b('img-container')}>
            <Image className={b('img')} src={images[0].image} alt="review img" width='100%' height='100%' />
          </div>
        ) : null}
        {withReadMore ? (
          <p className={b('text', { 'without-limit': withReadMore })}>
            <ReadMoreReact
              text={review || comments}
              min={80}
              ideal={100}
              max={200}
              readMoreText="Read more"
            />
          </p>
        ) : (
          <p className={b('text')}>{review || comments}</p>
        )}
      </div>
      <p className={b('name')}>{`${firstName} ${lastName}`}</p>
    </article>
  );
}

ClientCard.propTypes = propTypes;
ClientCard.defaultProps = defaultProps;

export default ClientCard;
