import React, { Component } from 'react';
import PropTypes from 'prop-types';
import bem from 'src/utils/bem';
import { connect } from 'react-redux';
import colors from 'src/styles/colors.json';

import poster from 'public/img/temp/black-background.jpg';
import dynamic from 'next/dynamic';
import { stopAllPlayersSelector } from 'src/redux/myCloud/selectors';
import { changePlayerFlagAction } from 'src/redux/myCloud/actions';
import styles from './index.module.scss';

const CheckFull = dynamic(() => import('src/components/svg/CheckFull'));
const MagnifyingPlus2 = dynamic(() => import('src/components/svg/MagnifyingPlus2'));
const ImageMenu = dynamic(() => import('src/components/elements/ImageMenu'));
const MediaButton = dynamic(() => import('src/components/elements/MediaButton'));


const b = bem('video-card', styles);
const darkBlue = colors['$dark-slate-blue-color'];
const whiteColor = colors['$white-color'];
const greenColor = colors['$summer-green-color'];
const greyColor = colors['$slate-gray-color'];

const defaultProps = {
  fileData: null,
  className: '',
  withMenu: false,
  showPopup: () => {},
  selected: [],
  addToSelected: () => {},
  removeFromSelected: () => {},
  forSharedPage: false,
  keyValue: null,
  stopAllPlayers: false,
  changePlayerFlag: () => {},
  isLockDigitizeFiles: false,
};

const propTypes = {
  fileData: PropTypes.shape({
    videoUri: PropTypes.string,
    id: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    name: PropTypes.string,
  }),
  className: PropTypes.string,
  withMenu: PropTypes.bool,
  showPopup: PropTypes.func,
  selected: PropTypes.arrayOf(PropTypes.shape({})),
  addToSelected: PropTypes.func,
  removeFromSelected: PropTypes.func,
  forSharedPage: PropTypes.bool,
  keyValue: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  stopAllPlayers: PropTypes.bool,
  changePlayerFlag: PropTypes.func,
  isLockDigitizeFiles: PropTypes.bool,
};

class VideoCard extends Component {
  constructor(props) {
    super(props);
    this.player = null;
    this.state = {
      isPlay: false,
      wrapRef: null,
    };
  }

  componentDidUpdate(prevProps) {
    const { stopAllPlayers } = this.props;
    const { stopAllPlayers: oldFlag } = prevProps;

    if (!oldFlag && stopAllPlayers) {
      this.clickPause();
    }
  }

  clickPlay = (e) => {
    const { changePlayerFlag } = this.props;
    changePlayerFlag(false);
    this.player.play();
    this.setState({ isPlay: true });
    if (e) {
      e.stopPropagation();
    }
  };

  clickPause = (e) => {
    this.player.pause();
    this.setState({ isPlay: false });
    if (e) {
      e.stopPropagation();
    }
  };

  addWrapRef = (e) => {
    this.setState({ wrapRef: e });
  };

  onClick = () => {
    const { removeFromSelected, addToSelected, fileData, selected } = this.props;
    const { id, videoUri } = fileData;
    const isSelect = !!selected.find((item) => item.id === id && item.videoUri === videoUri);

    if (isSelect) removeFromSelected(fileData);
    if (!isSelect) addToSelected(fileData);
  };

  openPopup = (fileData, e) => {
    const { showPopup } = this.props;
    this.clickPause(e);
    showPopup(fileData);
  };

  render() {
    const {
      className,
      fileData,
      selected,
      withMenu,
      forSharedPage,
      keyValue,
      isLockDigitizeFiles,
    } = this.props;
    const { isPlay, wrapRef } = this.state;

    const { videoUri, id, name } = fileData;

    const isSelect = !!selected.find((item) => item.id === id && item.videoUri === videoUri);

    const isSafari = /^((?!chrome|android).)*safari/i.test(navigator.userAgent);

    return (
      <div className={b({ mix: className, select: isSelect })} ref={this.addWrapRef} key={keyValue}>
        <div className={b('content-block')}>
          <button className={b('image-button')} type="button" onClick={this.onClick}>
            <div className={b('image-wrap')}>
              {isSafari ? (
                <video
                  className={b('video')}
                  width="100%"
                  ref={(e) => {
                    this.player = e;
                  }}
                  poster={poster}
                >
                  <source src={videoUri} />
                  <track kind="captions" />
                </video>
              ) : (
                <video
                  className={b('video')}
                  width="100%"
                  ref={(e) => {
                    this.player = e;
                  }}
                >
                  <source src={videoUri} />
                  <track kind="captions" />
                </video>
              )}
              <MediaButton
                className={b('media-button')}
                isPlay={isPlay}
                onClick={isPlay ? this.clickPause : this.clickPlay}
              />
            </div>
          </button>
          <button className={b('select-btn')} type="button" onClick={this.onClick}>
            <CheckFull
              className={b('select-icon')}
              stroke={isSelect ? greenColor : whiteColor}
              stroke2={isSelect ? whiteColor : 'none'}
              fill={isSelect ? greenColor : `${greyColor}88`}
            />
          </button>
        </div>
        {withMenu && (
          <div className={b('menu-block')}>
            <p className={b('name-image')}>{name}</p>
            <button
              className={b('zoom-button')}
              type="button"
              onClick={(e) => this.openPopup(fileData, e)}
            >
              <MagnifyingPlus2 className={b('zoom-icon')} />
            </button>
            <button className={b('menu-button')} type="button">
              <ImageMenu
                className={b('menu-icon')}
                iconFillColor={darkBlue}
                imageData={fileData}
                forSharedPage={forSharedPage}
                withOutOrderPrint
                portalBlock={wrapRef}
                isLockDigitizeFiles={isLockDigitizeFiles}
                withText
                text="Actions"
                type="video"
              />
            </button>
          </div>
        )}
      </div>
    );
  }
}

VideoCard.propTypes = propTypes;
VideoCard.defaultProps = defaultProps;

const stateProps = (state) => ({
  stopAllPlayers: stopAllPlayersSelector(state),
});

const actions = {
  changePlayerFlag: changePlayerFlagAction,
};
export default connect(stateProps, actions)(VideoCard);
