import React from 'react';
import PropTypes from 'prop-types';
import { ORIENTATION_TYPES } from 'src/constants';
import bem from 'src/utils/bem';
import generateRotateStyle from 'src/utils/generateRotateStyle';
import useImageOrientation from 'src/utils/useImageOrientation';
import LazyLoad from 'react-lazyload';
import Image from 'next/image';
import styles from './index.module.scss';

const b = bem('frame-slider-card', styles);

const defaultProps = {
  data: {},
  photo: {},
  withOutStorePhoto: true,
};

const propTypes = {
  data: PropTypes.shape({
    id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    displayName: PropTypes.string,
    price: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    hint: PropTypes.string,
    frameDisplays: PropTypes.arrayOf(PropTypes.shape({})),
    options: PropTypes.arrayOf(PropTypes.shape({})),
    imageUri: PropTypes.string,
    hintTitle: PropTypes.string,
  }),
  photo: PropTypes.shape({
    imageUri: PropTypes.string,
    id: PropTypes.number,
  }),
  withOutStorePhoto: PropTypes.bool,
};

const FrameSliderCard = (props) => {
  const { data, photo, withOutStorePhoto } = props;

  const { displayName, options = [], imageUri: defaultPhoto } = data;

  const storePhoto = withOutStorePhoto ? null : photo;

  const { imageUri: photoUri } = storePhoto || { imageUri: defaultPhoto };

  const { isVertical } = useImageOrientation(photoUri);

  const imageOrientation = isVertical ? ORIENTATION_TYPES.vertical : ORIENTATION_TYPES.horizontal;

  const defaultOption = options.find(
    (item) => item.frameOrientation === imageOrientation && item.isDefaultMinOption,
  );

  const randomOption = options.find((item) => item.frameOrientation === imageOrientation);

  const selectedOption = defaultOption || randomOption || options[0];

  const { minDisplay, frameImageOptions, frameImageUri } = selectedOption && Object.keys(selectedOption).length
    ? (selectedOption.frameDisplays.find(({ isDefault }) => isDefault) || selectedOption.frameDisplays[0] || {})
    : {};

  const {
    imageUri: displaysUri,
    frameRotateX,
    frameRotateY,
    frameRotateZ,
    frameScale,
    frameScaleX,
    frameTop,
    frameLeft,
    framePerspective,
    framePerspectiveOrigin,
    backgroundLeft,
    backgroundScale,
    backgroundTop,
  } = minDisplay || {};

  const top = 0;
  const left = 0;

  const frameBlockStyle = {
    height: `${frameScale}%`,
    width: `${frameScaleX}%`,
    left: `${frameLeft}%`,
    top: `${frameTop}%`,
    perspective: `${framePerspective}px`,
    perspectiveOrigin: framePerspectiveOrigin,
  };

  const { height, width, x, y } = frameImageOptions || {};

  const blockStyle = {
    height: `${height}%`,
    width: `${width}%`,
    left: `${x}%`,
    top: `${y}%`,
    transform: generateRotateStyle(frameRotateX, frameRotateY, frameRotateZ),
  };

  const imageStyle = {
    height: `${100 - top * 2}%`,
    width: `${100 - left * 2}%`,
    top: `${top}%`,
    left: `${left}%`,
  };

  return (
    <div className={b()}>
      <div className={b('image-block')}>
        <LazyLoad offset={100}>
          <img
            className={b('background-image')}
            src={displaysUri}
            alt={displayName}
            style={{
              width: `${backgroundScale}%`,
              left: `-${backgroundLeft}%`,
              top: `-${backgroundTop}%`,
            }}
          />
          <div className={b('frame-wrap')} style={frameBlockStyle}>
            <div className={b('image-wrap')} style={blockStyle}>
              <Image className={b('art-photo')} style={imageStyle} src={photoUri} alt="art" layout="fill" />
            </div>
            <img className={b('frame-image')} src={frameImageUri} alt="frame" />
          </div>
        </LazyLoad>
      </div>
    </div>
  );
}
FrameSliderCard.propTypes = propTypes;
FrameSliderCard.defaultProps = defaultProps;

export default FrameSliderCard;
