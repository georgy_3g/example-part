import React, { FunctionComponent, useEffect, useRef, useState } from 'react';
import bem from 'src/utils/bem';
import formattedImage from 'src/utils/formattedImage';
import useMobileStyle from 'src/utils/useMobileStyle';
import { connect } from 'react-redux';
import isEqual from 'lodash/isEqual';
import colors from 'src/styles/colors.json';
import printIcon from 'public/icons/print-new-icon.svg';
import Image from 'next/image';
import dynamic from 'next/dynamic';
import { getToken, isMobileDeviceSelector } from 'src/redux/auth/selectors';
import { enhanceOptionImageSelector } from 'src/redux/enhance/selectors';
import {
  loadImages,
  loadImagesOrderEnhance,
  setEnhanceImageNote,
} from 'src/redux/orderImages/actions';
import {
  ADD,
  COLOR_BUTTON_CANCEL,
  COLOR_BUTTON_TEXT,
  COLOR_TITLE,
  COLORIZATION,
  IMAGE_ALT,
  NOTE_BUTTON_TEXT,
  NOTE_DESCRIPTION,
  NOTE_PLACEHOLDER,
  NOTE_TITLE,
  NOTES,
  PHOTO_COLORIZATION,
  PHOTO_RESTORATION,
  POPUP_IMAGE,
  PRINTS,
} from './constants';
import styles from './index.module.scss';
import { noteInterface, propsInterface } from './interface';

const CheckFull = dynamic(() => import('src/components/svg/CheckFull'));
const ColorizationIcon = dynamic(() => import('src/components/svg/ColorizationIcon'));
const ImageNoteIcon = dynamic(() => import('src/components/svg/ImageNoteIcon'));
const ImageNotesPopup = dynamic(() => import('src/components/popups/ImageNotesPopup'));
const ImageColorizationPopup = dynamic(() => import('src/components/popups/ImageColorizationPopup'));
const ButtonEnhanceOptions = dynamic(() => import('src/components/elements/ButtonEnhanceOptions'));

const whiteColor = colors['$white-color'];
const redColor = colors['$burnt-sienna-color'];
const greyColor = colors['$slate-gray-color'];
const orangeColor = colors['$burnt-sienna-red-color'];

const b = bem('enhance-image-card', styles);

const defaultProps = {
  data: {
    id: 0,
    imageUri: '',
    imageKey: '',
  },
  optionImageEnhance: [],
  className: '',
  select: () => { return false },
  index: 0,
  selected: 0,
  isSelected: false,
  forSharedPage: false,
  fromMyFiles: false,
  selectedArr: [],
  removeFromSelected: () => { return false },
  addToSelected: () => { return false },
  deleteImageFromSlider: () => { return false },
  multiSelect: false,
  colorizationImages: [],
  selectColorizationImage: () => { return false },
  setImageNote: () => { return false },
  isRetouching: false,
  withChildren: false,
  children: null,
  withoutMenu: false,
  withOption: false,
  portalId: '',
  isFullScreen: false,
  isCheckout: false,
  clickPrint: () => { return false },
  nameOperation: '',
  idPhoto: 0,
  appendOptionEnhance: [],
  changeAppendOptions: () => { return false },
  refsRoot: null,
  positionData: null,
  scrollTop: 0,
  zIndex: 0,
  setCardBottomPopupProps: () => { return false },
};

const EnhanceImageCard: FunctionComponent<propsInterface> = (props) => {
  const {
    data,
    className,
    select,
    index,
    selected,
    isSelected,
    forSharedPage,
    fromMyFiles,
    selectedArr,
    removeFromSelected,
    addToSelected,
    deleteImageFromSlider,
    multiSelect,
    colorizationImages,
    selectColorizationImage,
    setImageNote,
    isRetouching,
    optionImageEnhance,
    children,
    withChildren,
    withoutMenu,
    withOption,
    portalId,
    isFullScreen,
    isCheckout,
    nameOperation,
    idPhoto,
    appendOptionEnhance,
    changeAppendOptions,
    refsRoot,
    isShipKit,
    withoutDelete,
    scrollRef,
    zIndex,
    setCardBottomPopupProps,
    isMobileDevice,
  } = props;

  const { id: idImage } = data || {};

  const [isShowImageNote, toggleImageNotePopup] = useState(false);
  const [isShowImageColorization, toggleImageColorizationPopup] = useState(false);
  const [cardBottomPopup, setCardBottomPopup] = useState('');


  const wrapRef = useRef<HTMLDivElement>(null);


  const [step, setStep] = useState(1);

  const [show, setShow] = useState<{
    open: boolean,
    optionShow: boolean,
    id?: number,
  }>({ open: isCheckout || false, optionShow: false, id: idImage });

  const [infoScroll, setScroll] = useState({
    scrollTop: 0,
    scrollLeft: 0,
  })

  const onScroll = () => {
    setScroll({
      scrollTop: scrollRef?.current?.scrollTop || 0,
      scrollLeft: scrollRef?.current?.scrollLeft || 0,
    })
  }

  useEffect(() => {
    if (scrollRef) {
      scrollRef.current?.addEventListener('scroll', onScroll);
    }
  }, []);

  useEffect(() => {
    setCardBottomPopup('');

  }, [isShowImageNote, isShowImageColorization]);

  useEffect(() => {
    if (setCardBottomPopupProps && idImage) {
      setCardBottomPopupProps(idImage, cardBottomPopup);
    }
  }, [cardBottomPopup]);

  const checkSelect = () => {
    const { id, imageUri } = data || {};
    if (fromMyFiles || forSharedPage || multiSelect) {
      return Boolean((selectedArr || []).find(item => (item.id === id && item.imageUri === imageUri)));
    }
    return (id === selected) || isSelected || false;
  };

  const checkColorization = () => {
    const { id, imageUri } = data || {};
    return Boolean(colorizationImages.find(item => (
      item.id === id
      && item.imageUri === imageUri
      && item.isSelected
    )));
  };

  const onClick = (e: React.MouseEvent, isAddButton: boolean) => {
    const value = {
      ...data,
      index,
    };
    if (wrapRef.current && (isEqual(e.target === wrapRef.current, isEqual) || isAddButton)) {
      if (fromMyFiles || forSharedPage) {
        if (checkSelect() && removeFromSelected && data) {
          removeFromSelected(data);
        } else if (addToSelected && data){
          addToSelected(data);
        }
      } else if (value){
        select(value);
      }
    }
  };

  const checkNotes = () => {
    const { id, imageUri } = data || {};
    return Boolean((selectedArr || []).find(item => (
      item.id === id
      && item.imageUri === imageUri
      && item.notes
    )));
  };

  const { isColorization, id } = data || {};
  const isImageSelected = checkSelect();
  const isImageColorization = isCheckout ? isColorization : checkColorization();
  const isImageNotes = checkNotes();
  const isOptions = appendOptionEnhance?.some(option => {
    if (option.relationImagesOption) {
      const { relationImagesOption } = option;
      return relationImagesOption.some(({imageId}) => imageId === id);
    }
    return false;
  });

  const ishipKitOptions = appendOptionEnhance?.some(({ name }) => name === 'easy_scan_ship_kit');


  const imageNotes = (value: noteInterface) => {
    if (setImageNote) setImageNote(value);
    toggleImageNotePopup(false);
  };

  const imageColorization = (selectValue: boolean) => {
    if(data) {selectColorizationImage(data, selectValue);}
    toggleImageColorizationPopup(false);
  };

  const { imageKey, name: imageName } = data || {};

  const descriptionColorization = () => {
    const { price: priceColorization = 0, additionalDiscount: colorAdditionalDiscount = 0 } = optionImageEnhance ? optionImageEnhance.find(({
      name,
    }) => name === PHOTO_COLORIZATION) || {} : {};
    const { price: priceRestoration = 0, additionalDiscount: restorationAdditionalDiscount = 0 } = optionImageEnhance ? optionImageEnhance.find(({
      name,
    }) => name === PHOTO_RESTORATION) || {} : {};
    const additionalSum = priceColorization - colorAdditionalDiscount - priceRestoration - restorationAdditionalDiscount;
    return `Add colorization to your black and white \nphoto for an additional $${Math.abs(additionalSum).toFixed(2)} each.`;
  }

  const managementOpenWindow = (windowId: number) => {
    if (!show.open && !show.optionShow) {
      setShow({ ...show, open: true, id: windowId });
    }
    if (show.open && show.optionShow) {
      setShow({ ...show, optionShow: false, id: windowId });
    }
    if (show.open && !show.optionShow) {

      setShow({ ...show, open: isCheckout || false, id: windowId });
      setStep(1);
    }
  };

  const hoverMenuButton = (type: string) => {
    if (type) {
      setCardBottomPopup(type);
    } else if(cardBottomPopup) {
      setCardBottomPopup('');
    }
  };

  const menuPrint = () => {
    setStep(2);
    setCardBottomPopup('');
  }

  const clickCardBottom = () => {
    switch (cardBottomPopup) {
      case COLORIZATION: {
        toggleImageColorizationPopup(true);
        break;
      }
      case NOTES: {
        toggleImageNotePopup(true);
        break;
      }
      case PRINTS : {
        menuPrint();
        break;
      }
      default: {
        break;
      }
    }

    setCardBottomPopup('');
  };
  const isMobileDisplay = useMobileStyle(480, isMobileDevice);

  return (
    <div
      className={b({
        mix: className,
        margin: cardBottomPopup,
      })}
      ref={wrapRef}
      onMouseLeave={() => { hoverMenuButton(''); }}
    >
      <div
        className={b('main-block', {
          select: isImageSelected,
          option: withOption,
        })}
      >
        <div className={b('content-block', { select: isImageSelected, 'no-before': isCheckout })}>
          <div
            className={b('image-button')}
            onClick={e => onClick(e, true)}
            role="button"
            tabIndex={0}
          >
            <div className={b('image-wrap')}>
              <img
                className={b('image-block')}
                src={formattedImage({ imagePath: imageKey, needResize: true, imageSize: isMobileDisplay ? '155x' : '175x'})}
                alt={imageName}
              />
              {(isShipKit || ishipKitOptions) && <div className={b('image-index')}>{(index || 0) + 1}</div>}
            </div>
            { !isCheckout
              && !withoutDelete
              && (
              <button
                className={b('delete-button')}
                type="button"
                onClick={() => { if (data) deleteImageFromSlider(data, isImageSelected); }}
              />
            )}
          </div>
        </div>
        <div className={b('menu-block', { retouching: isRetouching })}>
          {(!withoutMenu && step !== 2) && (
          <>
            {!isRetouching && (
              <div
                className={b('colorization-button')}
                role="button"
                tabIndex={0}
                onClick={() => { toggleImageColorizationPopup(true); }}
                onMouseEnter={() => { hoverMenuButton(COLORIZATION); }}
              >
                <ColorizationIcon className={b('colorization-button-icon')} />
                {isImageColorization && (
                  <div className={b('check-icon-wrapper')}>
                    <CheckFull
                      className={b('check-icon')}
                      stroke={redColor}
                      stroke2={whiteColor}
                      fill={redColor}
                    />
                  </div>
                )}
              </div>
            )}
            <div
              className={b('note-button')}
              role="button"
              tabIndex={0}
              onClick={() => toggleImageNotePopup(true)}
              onMouseEnter={() => { hoverMenuButton(NOTES); }}
            >
              <ImageNoteIcon />
              {isImageNotes && (
              <div className={b('check-icon-wrapper')}>
                <CheckFull
                  className={b('check-icon')}
                  stroke={redColor}
                  stroke2={whiteColor}
                  fill={redColor}
                />
              </div>
                )}
            </div>
            {isCheckout && (
            <button
              className={b('print-button')}
              type="button"
              onClick={menuPrint}
              onMouseEnter={() => { hoverMenuButton(PRINTS); }}
            >
              <Image src={printIcon} alt='prints' width='100%' height='100%' />
              {isOptions && (
                <div className={b('check-icon-wrapper')}>
                  <CheckFull
                    className={b('check-icon')}
                    stroke={redColor}
                    stroke2={whiteColor}
                    fill={redColor}
                  />
                </div>
              )}
            </button>
          )}
          </>
        )}
          {withChildren && children}
          { (isCheckout && step === 2) && (
          <ButtonEnhanceOptions
            className="btn-options"
            nameOperation={nameOperation}
            idPhoto={idPhoto}
            appendOptionEnhance={appendOptionEnhance}
            changeAppendOptions={changeAppendOptions}
            refsRoot={refsRoot ? refsRoot.current : null}
            positionData={wrapRef.current?.parentElement || null}
            managementOpenWindow={managementOpenWindow}
            show={show}
            setShow={setShow}
            isCheckout={isCheckout}
            scroll={infoScroll.scrollTop}
            scrollX={infoScroll.scrollLeft}
            zIndex={zIndex}
          />
        )}
        </div>
        {isShowImageNote && (
        <ImageNotesPopup
          portalId={portalId}
          close={() => toggleImageNotePopup(false)}
          title={NOTE_TITLE}
          description={NOTE_DESCRIPTION}
          placeholder={NOTE_PLACEHOLDER}
          buttonText={NOTE_BUTTON_TEXT}
          submit={imageNotes}
          image={data}
          buttonBackgroundColor={orangeColor}
          iconColor={orangeColor}
          cancelText={COLOR_BUTTON_CANCEL}
          cancelBackgroundColor={greyColor}
          isFullScreen={isFullScreen}
        />
      )}
        {isShowImageColorization && (
        <ImageColorizationPopup
          portalId={portalId}
          close={() => toggleImageColorizationPopup(false)}
          title={COLOR_TITLE}
          description={descriptionColorization()}
          buttonText={COLOR_BUTTON_TEXT}
          cancelText={COLOR_BUTTON_CANCEL}
          cancelBackgroundColor={greyColor}
          submit={imageColorization}
          buttonBackgroundColor={orangeColor}
          iconColor={orangeColor}
          imageUrl={POPUP_IMAGE}
          imageAlt={IMAGE_ALT}
          isFullScreen={isFullScreen}
          isMobileDisplay={isMobileDisplay}
        />
      )}

      </div>
      { cardBottomPopup && (
        <div className={b('card-bottom-popup')} onClick={clickCardBottom} role="button" tabIndex={0}>
          {`${ADD} ${cardBottomPopup}`}
        </div>
      )}
    </div>
  );
};

EnhanceImageCard.defaultProps = defaultProps;

const actions = {
  uploadImages: loadImages,
  uploadImagesEnhance: loadImagesOrderEnhance,
  setImageNote: setEnhanceImageNote,
};

const stateProps = (state: any) => ({
  token: getToken(state),
  optionImageEnhance: enhanceOptionImageSelector(state),
  isMobileDevice: isMobileDeviceSelector(state),
});

export default connect(stateProps, actions)(EnhanceImageCard);
