import colorizationPopupImage from  'public/img/photo-colorization/colorizationPopup.jpg';

const ORDER_PRINTS = 'Order Prints';
const DOWNLOAD = 'Download';
const SHARE = 'Share';
const RETOUCH_PHOTO = 'Retouch Photo';
const RESTORE_PHOTO = 'Restore Photo';
const COLORIZE_PHOTO = 'Colorize Photo';
const NOTE_TITLE = 'Do you have specific \n instructions?';
const NOTE_DESCRIPTION = 'Let us know if you have any notes for \nour artists regarding this photo.';
const NOTE_PLACEHOLDER = 'Please enter your notes here...';
const NOTE_BUTTON_TEXT = 'Submit Note';
const COLOR_TITLE = 'Do you want to add \ncolorization?';
const COLOR_BUTTON_TEXT = 'Yes, Add!';
const COLOR_BUTTON_CANCEL = 'No, thanks';
const POPUP_IMAGE = colorizationPopupImage;
const IMAGE_ALT = 'colorization image';
const PHOTO_COLORIZATION = 'photo_colorization';
const PHOTO_RESTORATION = 'photo_restoration';
const ADD = 'Add';
const COLORIZATION = 'colorization';
const NOTES = 'notes';
const PRINTS = 'prints';

export {
  ORDER_PRINTS,
  DOWNLOAD,
  SHARE,
  RETOUCH_PHOTO,
  RESTORE_PHOTO,
  COLORIZE_PHOTO,
  NOTE_BUTTON_TEXT,
  NOTE_DESCRIPTION,
  NOTE_PLACEHOLDER,
  NOTE_TITLE,
  COLOR_BUTTON_TEXT,
  COLOR_TITLE,
  COLOR_BUTTON_CANCEL,
  POPUP_IMAGE,
  PHOTO_COLORIZATION,
  PHOTO_RESTORATION,
  IMAGE_ALT,
  ADD,
  COLORIZATION,
  NOTES,
  PRINTS,
};
