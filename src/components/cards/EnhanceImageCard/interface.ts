export interface imagesOptionInteraface {
  imageId: number;
  quantity: number;
}
export interface enhanceOptionInterface {
  optionId?: number;
  name?: string;
  relationImagesOption?: imagesOptionInteraface[];
  quantity?: number;
}
export interface imageInterface {
  id: number;
  imageUri: string;
  imageKey?: string;
  index?: number;
  isSelected?: boolean;
  notes?: string;
  name?: string;
  isColorization?: boolean;
}

export interface noteInterface {
  orderImageId: number;
  notes: string;
  isNew: boolean;
  deleteNote: boolean;
  notesId?: number;
}

export interface propsInterface {
  data?: imageInterface;
  optionImageEnhance?: {
    price: number;
    additionalDiscount: number;
    name: string;
  }[];
  className?: string;
  select: (img: Partial<imageInterface>) => void;
  index?: number;
  selected?: number;
  isSelected?: boolean;
  forSharedPage?: boolean;
  fromMyFiles?: boolean;
  selectedArr?: imageInterface[];
  removeFromSelected?: (img: imageInterface) => void;
  setCardBottomPopupProps?: (id: number, value: string) => void;
  addToSelected?: (img: imageInterface) => void;
  deleteImageFromSlider: (data: Partial<imageInterface>, isSelect: boolean) => void;
  multiSelect?: boolean;
  colorizationImages: imageInterface[];
  selectColorizationImage: (data: Partial<imageInterface>, isColor: boolean) => void;
  setImageNote?: (note: noteInterface) => void;
  isRetouching?: boolean;
  withChildren?: boolean;
  children?: React.ReactNode;
  withoutMenu?: boolean;
  withOption?: boolean;
  portalId?: string;
  isFullScreen?: boolean;
  isCheckout?: boolean;
  clickPrint?: () => void;
  nameOperation?: string;
  withoutDelete?: boolean;
  idPhoto?: number;
  appendOptionEnhance?: enhanceOptionInterface[];
  changeAppendOptions?: (array: enhanceOptionInterface[]) => void;
  refsRoot?: {
    current: HTMLElement;
  } | null;
  positionData?: HTMLDivElement | HTMLElement | null;
  scrollTop?: number;
  scrollRef?: {
    current: HTMLElement;
  } | null;
  zIndex?: number;
  isShipKit?: boolean;
  isMobileDevice?: boolean;
}
