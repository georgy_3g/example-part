import React from 'react';
import PropTypes from 'prop-types';
import bem from 'src/utils/bem';
import useMobileStyle from 'src/utils/useMobileStyle';
import convertToPrice from 'src/utils/convertToPrice';
import { connect } from 'react-redux';
import LazyLoad from 'react-lazyload';
import dynamic from 'next/dynamic';
import { isMobileDeviceSelector } from 'src/redux/auth/selectors';
import styles from './index.module.scss';

const Picture = dynamic(() => import('src/components/elements/Picture'));
const BlueButton = dynamic(() => import('src/components/buttons/BlueButton'));

const b = bem('service-card', styles);

const defaultProps = {
  onClick: () => {},
  title: '',
  desc: '',
  textBtn: '',
  imgUrl: {},
  mobileImgUrl: {},
  imgAlt: '',
  url: '',
  isMobileDevice: true,
  price: 0,
  discount: 0,
  perText: '',
  withPrice: false,
};

const propTypes = {
  onClick: PropTypes.func,
  title: PropTypes.string,
  desc: PropTypes.string,
  textBtn: PropTypes.string,
  imgUrl: PropTypes.shape({
    full: PropTypes.shape({}),
    middle: PropTypes.shape({}),
    defaultImg: PropTypes.shape({}),
  }),
  mobileImgUrl: PropTypes.shape({
    src: PropTypes.string,
  }),
  imgAlt: PropTypes.string,
  url: PropTypes.string,
  isMobileDevice: PropTypes.bool,
  price: PropTypes.number,
  discount: PropTypes.number,
  perText: PropTypes.string,
  withPrice: PropTypes.bool,
};

const ServiceCard = (props) => {
  const {
    onClick,
    title,
    desc,
    textBtn,
    imgUrl,
    imgAlt,
    url,
    mobileImgUrl,
    isMobileDevice,
    price,
    discount,
    perText,
    withPrice,
  } = props;

  const isMobileDisplay = useMobileStyle(480, isMobileDevice);

  return (
    <article className={b()}>
      <a className={b('image-link', { 'with-price': withPrice })} href={url}>
        <LazyLoad offset={100}>
          <Picture
            bem={b}
            imgUrl={mobileImgUrl && isMobileDisplay ? mobileImgUrl : imgUrl}
            imgAlt={imgAlt}
            isMobileDisplay={isMobileDisplay}
          />
        </LazyLoad>
      </a>
      <h3 className={b('title')}>{title}</h3>
      <p className={b('desc')}>{desc}</p>
      <a className={b('link')} href={url}>
        <BlueButton className={b('button')} onClick={onClick}>
          {textBtn}
        </BlueButton>
      </a>
      {withPrice && (
        <div className={b('price-block')}>
          <span className={b('price', { 'with-discount': discount })}>{`${convertToPrice(price)}`}</span>
          {Boolean(discount) && (
            <span className={b('discount')}>{`${convertToPrice(price - discount)}`}</span>
          )}
          <span className={b('per-text')}>{perText}</span>
        </div>
      )}
    </article>
  );
}

ServiceCard.propTypes = propTypes;
ServiceCard.defaultProps = defaultProps;

const stateProps = (state) => ({
  isMobileDevice: isMobileDeviceSelector(state),
});

export default connect(stateProps, null)(ServiceCard);
