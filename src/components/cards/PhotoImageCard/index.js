import React, { useState } from 'react';
import ReactDOM from 'react-dom';
import bem from 'src/utils/bem';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import colors from 'src/styles/colors.json';
import dynamic from 'next/dynamic';
import { getToken } from 'src/redux/auth/selectors';
import { loadImages, loadImagesOrderEnhance } from 'src/redux/orderImages/actions';
import styles from './index.module.scss';

const CheckFull = dynamic(() => import('src/components/svg/CheckFull'));
const ImagePopup = dynamic(() => import('src/components/popups/ImagePopup'));
const CropPopup = dynamic(() => import('src/components/popups/CropPopup'));
const ImageComponent = dynamic(() => import('src/components/elements/Image'));
const MagnifyingPlus = dynamic(() => import('src/components/svg/MagnifyingPlus'));
const ImageMenu = dynamic(() => import('src/components/elements/ImageMenu'));
const DeleteIcon = dynamic(() => import('src/components/svg/DeleteIcon'));
const TriangleIcon = dynamic(() => import('src/components/svg/TriangleIcon'));

const grayColor = colors['$gray-color'];
const turquoiseColor = colors['$turquoise-color'];
const darkBlue = colors['$dark-slate-blue-color'];

const b = bem('photo-image-card', styles);

const defaultProps = {
  data: {},
  className: '',
  contentClass: '',
  select: () => {},
  index: 0,
  selected: 0,
  withoutText: false,
  iconClass: '',
  isSelected: false,
  onImageError: () => {},
  onLoadImage: () => {},
  forSharedPage: false,
  enhancePhoto: () => {},
  withMenu: false,
  showPopup: () => {},
  fromMyFiles: false,
  selectedArr: [],
  removeFromSelected: () => {},
  addToSelected: () => {},
  withDeleteBtn: false,
  deleteImageFromSlider: () => {},
  withCrop: false,
  isEnhanceProduct: false,
  uploadImages: () => {},
  uploadImagesEnhance: () => {},
  token: '',
  withOutSelect: false,
  multiSelect: false,
  withDownload: false,
  withOutPlus: false,
  centerSelectIcon: false,
  cardTitle: null,
  imageClass: '',
  buttonsClass: '',
  verticalImageClass: '',
  verticalButtonsClass: '',
  orderId: 0,
  orderPrints: () => {},
  isLockDigitizeFiles: false,
};

const propTypes = {
  data: PropTypes.shape({
    id: PropTypes.number,
    imageUri: PropTypes.string,
    watermarkedImageUri: PropTypes.string,
    parentId: PropTypes.number,
    isApproved: PropTypes.bool,
    isTemplate: PropTypes.bool,
    name: PropTypes.string,
    productType: PropTypes.shape({ name: PropTypes.string }),
  }),
  className: PropTypes.string,
  contentClass: PropTypes.string,
  select: PropTypes.func,
  index: PropTypes.number,
  selected: PropTypes.number,
  withoutText: PropTypes.bool,
  iconClass: PropTypes.string,
  isSelected: PropTypes.bool,
  onImageError: PropTypes.func,
  onLoadImage: PropTypes.func,
  forSharedPage: PropTypes.bool,
  enhancePhoto: PropTypes.func,
  withMenu: PropTypes.bool,
  showPopup: PropTypes.func,
  fromMyFiles: PropTypes.bool,
  selectedArr: PropTypes.arrayOf(PropTypes.shape({})),
  removeFromSelected: PropTypes.func,
  addToSelected: PropTypes.func,
  withDeleteBtn: PropTypes.bool,
  deleteImageFromSlider: PropTypes.func,
  withCrop: PropTypes.bool,
  isEnhanceProduct: PropTypes.bool,
  token: PropTypes.string,
  uploadImages: PropTypes.func,
  uploadImagesEnhance: PropTypes.func,
  withOutSelect: PropTypes.bool,
  multiSelect: PropTypes.bool,
  withDownload: PropTypes.bool,
  withOutPlus: PropTypes.bool,
  centerSelectIcon: PropTypes.bool,
  cardTitle: PropTypes.string,
  imageClass: PropTypes.string,
  buttonsClass: PropTypes.string,
  verticalImageClass: PropTypes.string,
  verticalButtonsClass: PropTypes.string,
  orderId: PropTypes.number,
  orderPrints: PropTypes.func,
  isLockDigitizeFiles: PropTypes.bool,
};

function PhotoImageCard(props) {
  const {
    data,
    className,
    contentClass,
    select,
    index,
    selected,
    isSelected,
    withoutText,
    iconClass,
    onImageError,
    onLoadImage,
    forSharedPage,
    enhancePhoto,
    withMenu,
    showPopup,
    fromMyFiles,
    selectedArr,
    removeFromSelected,
    addToSelected,
    withDeleteBtn,
    deleteImageFromSlider,
    withCrop,
    isEnhanceProduct,
    token,
    uploadImagesEnhance,
    uploadImages,
    withOutSelect,
    multiSelect,
    withDownload,
    withOutPlus,
    centerSelectIcon,
    cardTitle,
    imageClass,
    buttonsClass,
    verticalImageClass,
    verticalButtonsClass,
    orderId,
    orderPrints,
    isLockDigitizeFiles,
  } = props;

  let imageRef = null;

  const [isVertical, setIsVertical] = useState(false);
  const [isShowImage, toggleImagePopup] = useState(false);
  const [wrapRef, addWrapRef] = useState(null);

  const addRef = (e) => {
    imageRef = e;
  };

  const upload = (file) => {
    const formData = new FormData();
    formData.append('files[]', file, file.name);
    formData.append('isCropped', false);

    const uploadData = { formData, token };
    if (isEnhanceProduct) {
      uploadImagesEnhance(uploadData);
    } else {
      uploadImages(uploadData);
    }
    toggleImagePopup(false);
  };

  const checkSelect = () => {
    const { id, imageUri } = data;
    if (fromMyFiles || forSharedPage || multiSelect) {
      return Boolean(selectedArr.find((item) => item.id === id && item.imageUri === imageUri));
    }
    return id === selected || isSelected;
  };

  const onClick = (e, isAddButton) => {
    const value = {
      ...data,
      index,
    };
    if (e.target === imageRef || isAddButton) {
      if (fromMyFiles || forSharedPage) {
        if (checkSelect()) {
          removeFromSelected(data);
        } else {
          addToSelected(data);
        }
      } else {
        select(value);
      }
    }
  };

  const openImagePopup = () => {
    if (fromMyFiles || forSharedPage) {
      showPopup(data);
    } else {
      toggleImagePopup(true);
    }
  };

  const { id, imageUri, watermarkedImageUri, parentId, isApproved, isTemplate, name } = data;

  const cardName = name || `${orderId || `${orderId}-` || ''}${id}`;
  const title = parentId && !isApproved ? `Version ${index + 1}` : cardTitle || cardName;
  const isImageSelected = checkSelect();

  return (
    <div role="button" tabIndex="0" className={b({ mix: className })} onClick={onClick}>
      <div
        className={b('content-wrap', { mix: contentClass, vertical: isVertical })}
        ref={addWrapRef}
      >
        <div
          className={b('image-wrap', {
            mix: isVertical ? verticalImageClass : imageClass,
            vertical: isVertical,
          })}
        >
          <ImageComponent
            setIsVertical={setIsVertical}
            className={b('photo')}
            imageUri={imageUri || watermarkedImageUri}
            addRef={addRef}
            onImageError={onImageError}
            onLoadImage={onLoadImage}
            isTemplate={isTemplate}
          />
          <div className={b('veil')} />
        </div>
        <div
          className={b('button-block', {
            mix: isVertical ? verticalButtonsClass : buttonsClass,
            vertical: isVertical,
          })}
        >
          {isTemplate && <div className={b('button-block-veil')} />}
          {!withOutPlus && (
            <div className={b('button-wrap', { vertical: isVertical })}>
              {!isLockDigitizeFiles && (
                <button className={b('control-btn')} type="button" onClick={openImagePopup}>
                  <MagnifyingPlus
                    className={b('button-icon', { mix: iconClass })}
                    fill={grayColor}
                  />
                </button>
              )}
            </div>
          )}
          {!withOutSelect && (
            <div
              className={b('button-wrap', {
                vertical: isVertical,
                center: true,
                'in-center': centerSelectIcon,
              })}
            >
              <button className={b('select-btn')} type="button" onClick={(e) => onClick(e, true)}>
                <CheckFull
                  className={b('select-icon', { mix: iconClass })}
                  stroke={isImageSelected ? darkBlue : grayColor}
                  fill={isImageSelected ? turquoiseColor : 'none'}
                />
              </button>
            </div>
          )}
          {withMenu && (
            <div className={b('button-wrap', { vertical: isVertical, end: true })}>
              <ImageMenu
                className={b('button-icon')}
                iconFillColor={darkBlue}
                imageData={data}
                forSharedPage={forSharedPage}
                enhancePhoto={enhancePhoto}
                portalBlock={wrapRef}
                orderPrints={orderPrints}
                selectToCreate={onClick}
                withOutSelect={withOutSelect}
                isLockDigitizeFiles={isLockDigitizeFiles}
              />
            </div>
          )}
          {withDownload && (
            <div className={b('button-wrap', { vertical: isVertical, end: true })}>
              <a className={b('list-link')} href={imageUri} download>
                <TriangleIcon className={b('icon-btn')} />
              </a>
            </div>
          )}
          {withDeleteBtn && (
            <div className={b('button-wrap', { vertical: isVertical, end: true })}>
              <button
                className={b('delete-btn')}
                type="button"
                onClick={() => {
                  deleteImageFromSlider(data, isImageSelected);
                }}
              >
                <DeleteIcon className={b('delete-icon', { mix: iconClass })} fill={grayColor} />
              </button>
            </div>
          )}
        </div>
      </div>
      {!withoutText && <span className={b('title')}>{title}</span>}
      {isShowImage &&
        ReactDOM.createPortal(
          withCrop ? (
            <CropPopup imageUri={imageUri} close={() => toggleImagePopup(false)} upload={upload} />
          ) : (
            <ImagePopup imageUri={imageUri} close={() => toggleImagePopup(false)} />
          ),
          document.getElementById('__next') || null,
        )}
    </div>
  );
}

PhotoImageCard.propTypes = propTypes;
PhotoImageCard.defaultProps = defaultProps;

const actions = {
  uploadImages: loadImages,
  uploadImagesEnhance: loadImagesOrderEnhance,
};

const stateProps = (state) => ({
  token: getToken(state),
});

export default connect(stateProps, actions)(PhotoImageCard);
