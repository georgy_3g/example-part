import React, { Component } from 'react';
import PropTypes from 'prop-types';
import bem from 'src/utils/bem';
import formattedImage from 'src/utils/formattedImage';
import colors from 'src/styles/colors.json';
import dynamic from "next/dynamic";
import { EMPTY, VIEW, FILES } from './constants';
import styles from './index.module.scss';

const FolderIcon = dynamic(() => import('src/components/svg/FolderIcon'));
const CheckFull = dynamic(() => import('src/components/svg/CheckFull'));
const ImageMenu = dynamic(() => import('src/components/elements/ImageMenu'));

const b = bem('folder-card', styles);

const darkBlue = colors['$dark-slate-blue-color'];
const whiteColor = colors['$white-color'];
const greenColor = colors['$summer-green-color'];
const greyColor = colors['$slate-gray-color'];

const defaultProps = {
  className: PropTypes.string,
  removeFromSelected: PropTypes.func,
  addToSelected: PropTypes.func,
  selected: PropTypes.shape({}),
  getFolderById: PropTypes.func,
  orderId: PropTypes.number,
  data: PropTypes.shape,
};

const propTypes = {
  className: '',
  removeFromSelected: () => {},
  addToSelected: () => {},
  selected: {},
  getFolderById: () => {},
  orderId: null,
  data: {},
};

class FolderCard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      wrapRef: null,
    };
  }

  addWrapRef = (e) => {
    this.setState({ wrapRef: e });
  };

  isSelected = () => {
    const { selected, data } = this.props;
    const { id } = data || {};
    return selected.some((item) => item.id === id);
  };

  onClick = (e) => {
    const { data } = this.props;
    const { id } = data || {};
    e.stopPropagation();
    const { addToSelected, removeFromSelected } = this.props;
    if (this.isSelected()) {
      removeFromSelected({ id, isFolder: true });
    } else {
      addToSelected({ id, isFolder: true });
    }
  };

  render() {
    const { data, className, orderId, getFolderById } = this.props;
    const { wrapRef } = this.state;
    const { id, preview = [], name, countFiles = 0 } = data || {};
    const isSelect = this.isSelected();

    const previewIcons = (preview || []).filter(
      ({ url, fileType }, index) => url && fileType === 'image' && index < 5,
    ) || [];

    return (
      <div className={b({ mix: className, select: isSelect })} ref={this.addWrapRef}>
        <div className={b('content-block')}>
          <button
            className={b('image-button')}
            type="button"
            onClick={() => {
              getFolderById(id, name);
            }}
          >
            <div className={b('image-wrap')}>
              {preview && preview.length ? (
                <div className={b('view-folder')}>
                  {previewIcons.map(({ id: imageId, url, key }, index) => (
                    <div
                      key={imageId}
                      className={b('view-image', { [`image_${index}`]: true })}
                      style={{ backgroundImage: `url(${formattedImage({imagePath: key, needResize: true, imageSize: '100x'}) || url})` }}
                    />
                  ))}
                  <div className={b('view-container')}>
                    <FolderIcon className={b('icon-from-folder')} />
                    <p className={b('folder-name', { view: true })}>{VIEW}</p>
                    <p className={b('count-folder')}>{`${countFiles} ${FILES}`}</p>
                  </div>
                </div>
              ) : (
                <div className={b('empty-folder')}>
                  <FolderIcon />
                  <p className={b('folder-name', { empty: true })}>
                    {Number(countFiles) ? VIEW : EMPTY}
                  </p>
                </div>
              )}
            </div>
          </button>
          <button className={b('select-btn')} type="button" onClick={this.onClick}>
            <CheckFull
              className={b('select-icon')}
              stroke={isSelect ? greenColor : whiteColor}
              stroke2={isSelect ? whiteColor : 'none'}
              fill={isSelect ? greenColor : `${greyColor}88`}
            />
          </button>
        </div>
        <div className={b('menu-block')}>
          <p className={b('name-image')}>{name}</p>
          <button className={b('menu-button')} type="button">
            <ImageMenu
              className={b('menu-icon')}
              iconFillColor={darkBlue}
              imageData={{ id }}
              withOutOrderPrint
              portalBlock={wrapRef}
              isFolder
              item={{
                orderId,
                ids: [id],
              }}
              withText
              text="Actions"
            />
          </button>
        </div>
      </div>
    );
  }
}

FolderCard.propTypes = propTypes;
FolderCard.defaultProps = defaultProps;

export default FolderCard;
