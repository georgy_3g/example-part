const ORDER_PRINTS = 'Order Prints';
const DOWNLOAD = 'Download';
const SHARE = 'Share';
const RETOUCH_PHOTO = 'Retouch Photo';
const RESTORE_PHOTO = 'Restore Photo';
const COLORIZE_PHOTO = 'Colorize Photo';

export { ORDER_PRINTS, DOWNLOAD, SHARE, RETOUCH_PHOTO, RESTORE_PHOTO, COLORIZE_PHOTO };
