import React, { useEffect, useState } from 'react';
import ReactDOM from 'react-dom';
import bem from 'src/utils/bem';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import colors from 'src/styles/colors.json';
import dynamic from 'next/dynamic';
import { loadImages, loadImagesOrderEnhance } from 'src/redux/orderImages/actions';
import { getToken } from 'src/redux/auth/selectors';
import styles from './index.module.scss';

const CheckFull = dynamic(() => import('src/components/svg/CheckFull'));
const NewMagnifyingPlusIcon = dynamic(() => import('src/components/svg/NewMagnifyingPlusIcon'));
const ImagePopup = dynamic(() => import('src/components/popups/ImagePopup'));
const CropPopup = dynamic(() => import('src/components/popups/CropPopup'));
const TrashIcon2 = dynamic(() => import('src/components/svg/TrashIcon2'));

const whiteColor = colors['$white-color'];
const greenColor = colors['$summer-green-color'];
const greyColor = colors['$slate-gray-color'];

const b = bem('image-card', styles);

const defaultProps = {
  data: {},
  className: '',
  select: () => {},
  index: 0,
  selected: 0,
  isSelected: false,
  forSharedPage: false,
  fromMyFiles: false,
  selectedArr: [],
  removeFromSelected: () => {},
  addToSelected: () => {},
  deleteImageFromSlider: () => {},
  withCrop: false,
  isEnhanceProduct: false,
  uploadImages: () => {},
  uploadImagesEnhance: () => {},
  token: '',
  showPopup: () => {},
  multiSelect: false,
  isSmallCard: false,
  withOutZoom: false,
  isResponsiveIcon: false,
  onImageError: () => {},
};

const propTypes = {
  data: PropTypes.shape({
    id: PropTypes.number,
    imageUri: PropTypes.string,
    watermarkedImageUri: PropTypes.string,
    parentId: PropTypes.number,
    isApproved: PropTypes.bool,
    isTemplate: PropTypes.bool,
    name: PropTypes.string,
    productType: PropTypes.shape({ name: PropTypes.string }),
  }),
  className: PropTypes.string,
  select: PropTypes.func,
  index: PropTypes.number,
  selected: PropTypes.number,
  isSelected: PropTypes.bool,
  forSharedPage: PropTypes.bool,
  showPopup: PropTypes.func,
  fromMyFiles: PropTypes.bool,
  selectedArr: PropTypes.arrayOf(PropTypes.shape({})),
  removeFromSelected: PropTypes.func,
  addToSelected: PropTypes.func,
  deleteImageFromSlider: PropTypes.func,
  withCrop: PropTypes.bool,
  isEnhanceProduct: PropTypes.bool,
  token: PropTypes.string,
  uploadImages: PropTypes.func,
  uploadImagesEnhance: PropTypes.func,
  multiSelect: PropTypes.bool,
  isSmallCard: PropTypes.bool,
  withOutZoom: PropTypes.bool,
  isResponsiveIcon: PropTypes.bool,
  onImageError: PropTypes.func,
};

function PhotoImageCard(props) {
  const {
    data,
    className,
    select,
    index,
    selected,
    isSelected,
    forSharedPage,
    showPopup,
    fromMyFiles,
    selectedArr,
    removeFromSelected,
    addToSelected,
    deleteImageFromSlider,
    withCrop,
    isEnhanceProduct,
    token,
    uploadImagesEnhance,
    uploadImages,
    multiSelect,
    isSmallCard,
    withOutZoom,
    isResponsiveIcon,
    onImageError,
  } = props;

  const [isShowImage, toggleImagePopup] = useState(false);
  const [wrapRef, addWrapRef] = useState(null);

  useEffect(() => {
    if (!wrapRef) {
      const photoImg = new Image();
      photoImg.onerror = () => {
        onImageError(data.imageUri);
      };
      photoImg.src = data.imageUri;
    }
  });

  const upload = (file) => {
    const formData = new FormData();
    formData.append('files[]', file, file.name);
    formData.append('isCropped', false);

    const uploadData = { formData, token };
    if (isEnhanceProduct) {
      uploadImagesEnhance(uploadData);
    } else {
      uploadImages(uploadData);
    }
    toggleImagePopup(false);
  };

  const checkSelect = () => {
    const { id, imageUri } = data;
    if (fromMyFiles || forSharedPage || multiSelect) {
      return Boolean(selectedArr.find((item) => item.id === id && item.imageUri === imageUri));
    }
    return id === selected || isSelected;
  };

  const onClick = (e, isAddButton) => {
    const value = {
      ...data,
      index,
    };
    if (e.target === wrapRef || isAddButton) {
      if (fromMyFiles || forSharedPage) {
        if (checkSelect()) {
          removeFromSelected(data);
        } else {
          addToSelected(data);
        }
      } else {
        select(value);
      }
    }
  };

  const openImagePopup = () => {
    if (fromMyFiles || forSharedPage) {
      showPopup(data);
    } else {
      toggleImagePopup(true);
    }
  };

  const { imageUri } = data;

  const isImageSelected = checkSelect();

  return (
    <div
      role="button"
      tabIndex="0"
      className={b({ mix: className, select: isImageSelected, small: isSmallCard })}
      onClick={onClick}
      ref={addWrapRef}
      style={{ backgroundImage: `url(${imageUri})` }}
    >
      <button
        className={b('select-btn', { small: isSmallCard, responsive: isResponsiveIcon })}
        type="button"
        onClick={(e) => onClick(e, true)}
      >
        <CheckFull
          className={b('select-icon', { small: isSmallCard, responsive: isResponsiveIcon })}
          stroke={isImageSelected ? greenColor : whiteColor}
          stroke2={isImageSelected ? whiteColor : 'none'}
          fill={isImageSelected ? greenColor : `${greyColor}88`}
        />
      </button>
      <button
        className={b('zoom-button', {
          hidden: withOutZoom,
          small: isSmallCard,
          responsive: isResponsiveIcon,
        })}
        type="button"
        onClick={openImagePopup}
      >
        <NewMagnifyingPlusIcon
          className={b('zoom-icon', { small: isSmallCard, responsive: isResponsiveIcon })}
        />
      </button>
      <button
        className={b('trash-button', { small: isSmallCard, responsive: isResponsiveIcon })}
        type="button"
        onClick={() => {
          deleteImageFromSlider(data, isImageSelected);
        }}
      >
        <TrashIcon2
          className={b('trash-icon', { small: isSmallCard, responsive: isResponsiveIcon })}
        />
      </button>
      {isShowImage &&
        ReactDOM.createPortal(
          withCrop ? (
            <CropPopup imageUri={imageUri} close={() => toggleImagePopup(false)} upload={upload} />
          ) : (
            <ImagePopup imageUri={imageUri} close={() => toggleImagePopup(false)} />
          ),
          document.getElementById('__next') || null,
        )}
    </div>
  );
}

PhotoImageCard.propTypes = propTypes;
PhotoImageCard.defaultProps = defaultProps;

const actions = {
  uploadImages: loadImages,
  uploadImagesEnhance: loadImagesOrderEnhance,
};

const stateProps = (state) => ({
  token: getToken(state),
});

export default connect(stateProps, actions)(PhotoImageCard);
