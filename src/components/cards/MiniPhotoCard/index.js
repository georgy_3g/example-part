import React, { useState } from 'react';
import PropTypes from 'prop-types';
import bem from 'src/utils/bem';
import dynamic from "next/dynamic";
import styles from './index.module.scss';

const ImageComponent = dynamic(() => import('src/components/elements/Image'));

const b = bem('mini-photo-card', styles);

const defaultProps = {
  data: {},
  className: '',
  select: () => {},
  index: 0,
  selected: 0,
  withoutText: false,
};

const propTypes = {
  data: PropTypes.shape({
    id: PropTypes.number,
    imageUri: PropTypes.string,
    parentId: PropTypes.number,
    version: PropTypes.number,
  }),
  className: PropTypes.string,
  select: PropTypes.func,
  index: PropTypes.number,
  selected: PropTypes.number,
  withoutText: PropTypes.bool,
};

function MiniPhotoCard(props) {
  const { data, className, select, index, selected, withoutText } = props;

  const [isVertical, setIsVertical] = useState(false);

  const onClick = () => {
    const value = {
      ...data,
      index,
    };
    select(value);
  };

  const { id, imageUri, parentId, version } = data;
  const title = parentId ? `v${version}` : `#${id}`;
  const selectedImage = id === selected;
  return (
    <div className={b({ mix: className })}>
      <div className={b('image-wrapper')}>
        <div
          className={b('image-wrap', { vertical: isVertical, 'selected-image': selectedImage })}
          onClick={onClick}
          tabIndex="0"
          role="button"
        >
          <ImageComponent
            setIsVertical={setIsVertical}
            className={b('photo')}
            imageUri={imageUri}
          />
        </div>
      </div>
      {!withoutText && <span className={b('title')}>{title}</span>}
    </div>
  );
}

MiniPhotoCard.propTypes = propTypes;
MiniPhotoCard.defaultProps = defaultProps;

export default MiniPhotoCard;
