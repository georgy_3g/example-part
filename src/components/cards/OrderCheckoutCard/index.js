import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import ReactDOM from 'react-dom';
import bem from 'src/utils/bem';
import generateRotateStyle from 'src/utils/generateRotateStyle';
import formattedPrice from 'src/utils/formattedPrice';
import { EASY_SCAN_SHIP_KIT_TERMS, ENHANCE_TERMS, PHOTO_ART_TERMS } from 'src/terms';
import { DIGITIZE_PRODUCTS, ENHANCE_PRODUCTS } from 'src/constants';
import ERRORS from 'src/constants/errors';
import Colors from 'src/styles/colors.json';
import ChooseAnyPhoto from 'src/components/widgets/ChooseAnyPhoto';

import orderImage from 'public/img/shared/navi-img.png';
import audioImage from 'public/img/shared/product-icons/audio-checkout-icon-225x160.jpg';
import deviceImage from 'public/img/shared/product-icons/device-checkout-icon-225x160.jpg';
import filmImage from 'public/img/shared/product-icons/film-checkout-icon-225x160.jpg';
import scanImage from 'public/img/shared/product-icons/scan-checkout-icon-225x160.jpg';
import tapeImage from 'public/img/shared/product-icons/tape-checkout-icon-225x160.jpg';
import guarantyImage from  'public/img/how-it-works/heppiness-logo.jpg';
import photoRestorationPhoto from  'public/img/navigation/restore-retouch-photo-service-drop-down-menu-old-photo-restoration-200x150.jpg';
import photoRetouchingPhoto from  'public/img/navigation/restore-retouch-photo-service-drop-down-menu-photo-retouching-service-200x150.jpg';
import dynamic from 'next/dynamic';
import ordersTypeSelect from 'src/redux/orders/selectors';
import {
  orderPriceSelect,
  photoColorizationSelect,
  photoRestorationSelect,
  priceSelect,
} from 'src/redux/prices/selectors';
import { orderAllImagesSelector } from 'src/redux/orderImages/selectors';
import { frameMaterialsSelector } from 'src/redux/photoArt/selectors';
import { photoArtProductListSelector } from 'src/redux/shared/selectors';
import { calculateOrderInfoSelector } from 'src/redux/orderCreate/selectors';
import {
  changeOrder,
  deleteFromCart,
  orderChangefromCart,
  orderChangeInputFromCart,
} from 'src/redux/orders/actions';
import { clearCoupon } from 'src/redux/coupon/actions';
import {
  CONFIRM_QUESTION,
  CURRENCY,
  EDIT,
  EDIT_PRODUCT,
  GIFT,
  NO,
  THIS_IS_GIFT,
  TOTAL,
  USD,
  YES,
  HAPPINESS_TITLE,
  HAPPINESS_TEXT,
  OUR_GUARANTEE,
  GUARANTEE,
} from './constants';
import styles from './index.module.scss';
import Product from './product';

const TrashIcon2 = dynamic(() => import('src/components/svg/TrashIcon2'));
const IsGiftOrder = dynamic(() => import('src/components/elements/IsGiftOrder'));
const GiftIcon = dynamic(() => import('src/components/svg/GiftIcon'));
const HeartIcon = dynamic(() => import('src/components/svg/HeartIcon'));
const NewTogglerInput = dynamic(() => import('src/components/inputs/NewTogglerInput'));
const EditIcon = dynamic(() => import('src/components/svg/EditIcon'));
const PhotoArtPersonalizeCard = dynamic(() => import('src/components/cards/PhotoArtPersonalizeCard'));

const b = bem('checkout-card', styles);
const slateGrayColor = Colors['$slate-gray-color-2'];
const pickledBluewoodColor = Colors['$pickled-bluewood-color'];

const { CHOOSE_DELIVERABLES } = ERRORS;


const orderParams = {
  isGift: 'isGift',
};

const MAX_QT = 9999;

const {
  RESTORATION_PRODUCT,
  RETOUCHING_PRODUCT,
  COLORIZATION_PRODUCT,
} = ENHANCE_TERMS;

const {
  PHOTO_ART_PRODUCT,
} = PHOTO_ART_TERMS;

const {
  EASY_SCAN_SHIP_KIT_TYPE,
} = EASY_SCAN_SHIP_KIT_TERMS;

const defaultProps = {
  data: [],
  price: {},
  currency: '$',
  order: [],
  deleteOrder: () => {  },
  orderChange: () => {  },
  openPopup: () => {  },
  clearPromo: () => {  },
  selectPrice: {},
  isDeletePopup: false,
  orderId: null,
  orderType: null,
  allOrderImages: {},
  photoArtProducts: [],
  orderInputChange: () => {  },
  changeOrdersCount: () => {  },
  frameMaterials: [],
  getOrderPaymentInfo: () => {  },
  orderCreateCalculations: {},
  photoRestorationProduct: {},
  photoColorizationProduct: {},
};

const propTypes = {
  data: PropTypes.arrayOf(PropTypes.shape({})),
  price: PropTypes.shape({}),
  order: PropTypes.arrayOf(),
  currency: PropTypes.string,
  deleteOrder: PropTypes.func,
  orderChange: PropTypes.func,
  clearPromo: PropTypes.func,
  selectPrice: PropTypes.shape({}),
  isDeletePopup: PropTypes.bool,
  orderId: PropTypes.number,
  orderType: PropTypes.string,
  openPopup: PropTypes.func,
  allOrderImages: PropTypes.shape({}),
  photoArtProducts: PropTypes.arrayOf(PropTypes.shape({})),
  orderInputChange: PropTypes.func,
  changeOrdersCount: PropTypes.func,
  frameMaterials: PropTypes.arrayOf(),
  getOrderPaymentInfo: PropTypes.func,
  orderCreateCalculations: PropTypes.shape({
    details: PropTypes.arrayOf(PropTypes.shape({})),
  }),
  photoRestorationProduct: PropTypes.shape({
    id: PropTypes.number,
    options: PropTypes.arrayOf(PropTypes.shape({})),
    details: PropTypes.arrayOf(PropTypes.shape({})),
  }),
  photoColorizationProduct: PropTypes.shape({
    id: PropTypes.number,
    options: PropTypes.arrayOf(PropTypes.shape({})),
    details: PropTypes.arrayOf(PropTypes.shape({})),
  }),
};

class OrderCheckoutCard extends Component {
  constructor(props) {
    super(props);
    const { order } = this.props;

    const { photo_restoration: photoRestoration, photo_colorization: photoColorization } = order;
    const photoRestorationImages = photoRestoration.map(({images}) => images);
    const photoColorizationImages = photoColorization.map(({images}) => images);
    const fullImages = [...photoRestorationImages, ...photoColorizationImages];

    this.state = {
      open: [],
      popupOpen: false,
      colorizationImages: this.getPhotoFromStore(fullImages),
      showPhotoArtPersonalizePopup: false,
      selectItem: {},
      openGuarantee: [],
      openGift: [],
    };
  }

  componentDidUpdate(prevProps) {
    const { order: prevOrder } = prevProps || {};
    const { order, clearPromo } = this.props;
    if (!order.length && prevOrder.length) {
      clearPromo();
    }
  }

  setEditStatus = () => {
    localStorage.setItem('editOrder', JSON.stringify(true));
  }

  closePopupOpen = (id) => {
    this.setState({ popupOpen: id });
  }

  openDeletePopup = (id, type) => () => {
    const { openPopup } = this.props;
    openPopup({
      deletePopup: true,
      orderId: id,
      orderType: type,
    });
  }

  closeDeletePopup = () => {
    const { openPopup } = this.props;
    openPopup({
      deletePopup: false,
      orderId: null,
      orderType: null,
    });
  }



  togglePopup = (id, type, name) => {
    const { [name]: arrayProducts } = this.state;
    const key = `${id}_${type}`;

    const isOpen = arrayProducts.includes(key);
    if (name === 'openGift') {
      const { orderChange, getOrderPaymentInfo } = this.props;
      const order = {
        typeProduct: type,
        itemId: id,
        counter: orderParams.isGift,
      };
      this.setEditStatus();
      orderChange(order);
      getOrderPaymentInfo();
    }
    this.setState({ [name]: isOpen ? arrayProducts.filter(item => item !== key) : [...arrayProducts, key] })
  }


  validateInputs = (value, key) => {
    switch (true) {
      case value > MAX_QT:
        return MAX_QT;
      case value <= 0 && key === 'quantity':
        return 1;
      case value < 0:
        return 0;
      default:
        return value;
    }
  }

  validateInputCount = (value, inc, key) => {
    if ((value >= MAX_QT && inc > 0)
    || (value <= 1 && inc < 1 && key === 'quantity')
    || (value < 1 && inc <= 0)) { return 0; }
    return inc ? 1 : -1;
  }

  changePersonalize = (value, valueSecond, clear, typeProduct, itemId) => {
    const { changeOrdersCount, order, getOrderPaymentInfo } = this.props;
    const newState = order[typeProduct].map((item) => {
      const newOptions = item.options.map((option) => {
        if (option.name === 'personalize') {
          return { ...option, quantity: clear };
        }
        return option;
      });

      if (itemId === item.id) {
        return {
          ...item, options: newOptions, text: clear ? value : '', textSecond: clear ? valueSecond : '',
        };
      }
      return item;
    });
    this.setEditStatus();
    changeOrdersCount({ typeProduct, value: newState });
    getOrderPaymentInfo();
    this.closePopupOpen(null);
  }

  changeCount = ({
   isAdd,
   typeProduct,
   itemId,
   key,
   value,
   isDependProduct,
  }) => {
    const { orderChange, getOrderPaymentInfo } = this.props;
    const inc = this.validateInputCount(value, isAdd, key);
    const order = {
      typeProduct,
      itemId,
      inc: isDependProduct ? isAdd : inc,
      counter: key,
      isDependProduct,
    };
    this.setEditStatus();
    orderChange(order);
    getOrderPaymentInfo();
  };

  enterQuantity = (value, typeProduct, itemId, key) => {
    const newValue = String(value).length <= 5 ? value : MAX_QT;
    const { orderInputChange, getOrderPaymentInfo } = this.props;
    const order = {
      typeProduct,
      itemId,
      value: newValue,
      counter: key,
    };
    this.setEditStatus();
    orderInputChange(order);
    if (value) {
      getOrderPaymentInfo();
    }
  }

  handleGift = (typeProduct, itemId, counter) => {
    const { orderChange, getOrderPaymentInfo } = this.props;
    const order = {
      typeProduct,
      itemId,
      counter,
    };
    this.setEditStatus();
    orderChange(order);
    getOrderPaymentInfo();
  }

  editOptions = (id, typeProduct) => {
    const { open } = this.state;
    if (open.some(item => item.id === id && item.typeProduct === typeProduct)) {
      this.setState({ open: open.filter(el => el.id !== id && el.typeProduct !== typeProduct) });
    } else {
      this.setState({ open: [...open, { id, typeProduct }] });
    }
  }

  getDefaultImage = (typeProduct) => {
    switch (typeProduct) {
      case 'tape_transfer':
        return { imageUri: tapeImage };
      case 'film_transfer':
        return { imageUri: filmImage };
      case 'audio_transfer':
        return { imageUri: audioImage };
      case 'digital_transfer':
        return { imageUri: deviceImage };
      case 'photo_scan':
        return { imageUri: scanImage };
      case 'photo_restoration':
      case 'photo_colorization':
        return { imageUri: photoRestorationPhoto };
      case 'photo_retouching':
        return { imageUri: photoRetouchingPhoto };
      default:
        return { imageUri: orderImage};
    }
  }

  selectProductImage = (item) => {
    const { allOrderImages } = this.props;
    const { images, typeProduct, options } = item || {};
    const isEasyScanKit = options && options.length && options[0].name === 'easy_scan_ship_kit';
    const defaultImageObject = { imageUri : this.getDefaultImage(typeProduct, isEasyScanKit).imageUri.src };
    const allImages = Object.values(allOrderImages)
      .reduce((acc, arr) => [...acc, ...(arr && arr.length ? arr : [])]);


    if (ENHANCE_PRODUCTS.includes(typeProduct)) {
      return defaultImageObject;
    }

    if (typeProduct === 'photo_art') {
      return images
        ? allImages
          .find(itm => (itm && images.includes(itm.id)) && itm.isCropped) || defaultImageObject
      : defaultImageObject;
    }

    return images
      ? allImages.find(({ id }) => id === images[0]) || defaultImageObject
    : defaultImageObject;
  }

  changeAppendOptions = (value, typeProduct, itemId) => {
    const { changeOrdersCount, getOrderPaymentInfo, order: orderOption } = this.props;

    const product = orderOption[typeProduct] || [];

    const newState = product.map((el) => {
      const { id = 0 } = el;
      if (id === itemId) return { ...el, options: value };
      return el;
    });

    this.setEditStatus();
    changeOrdersCount({ typeProduct, value: newState });
    getOrderPaymentInfo();
  };

  getPhotoFromStore = (imagesId) => {
    const photos = (JSON.parse(localStorage.getItem('enhance/Option') || '[]') || []);
    return photos.filter(({ id }) => (imagesId || []).includes(id));
  };

  formatOption = (value, imageId) => {
    const { options = [] } = value.find((el) => {
      const { images = [] } = el || {};
      return images.some(item => imageId.includes(item));
    }) || {};
    return options;
  }

  getOptionsProduct = (item) => {
    const { selectPrice } = this.props;
    const { options = [] } = item || {};

    return options.map((el) => {
      const { quantity = 0, optionId } = el || {};

      const { [item.typeProduct]: order = [] } = selectPrice || {};
      const { options: optionOrder = [] } = order || {};
      const { displayName = '', price = 0, name = '', additionalDiscount = 0 } = optionOrder.find(({ id }) => id === optionId) || {};
      const optionCustomPrice = additionalDiscount ? price - additionalDiscount : price;

      if (name === 'enhance_prints') {
        return (
          <div className={b('option')} key={optionId}>
            <div className={b('description-wrapper')}>
              <span className={b('description')}>{displayName}</span>
              <span className={b('description-price', { desktop: true })}>{`$${optionCustomPrice.toFixed(2)}`}</span>
            </div>
            <span className={b('count', { art: true })}>{quantity}</span>
            <span className={b('description-sum', { art: true })}>{`$${(optionCustomPrice * quantity).toFixed(2)}`}</span>
          </div>
        );
      }
      return null;
    });
  }

  getPhotoArtPhoto = (order) => {
    const { photoArtProducts } = this.props;
    const {
      productId,
      options = [],
      display,
    } = order || {};
    const { optionId } = options[0] || {};
    const selectedProduct = photoArtProducts.find(({ id }) => id === productId);

    const {
      options: productOptions,
    } = selectedProduct || {};

    const selectedOption = productOptions.find(({ id }) => id === optionId) || {};
    const selectedDisplay = selectedOption.frameDisplays.find(({ id }) => display && id === display.id)
      || selectedOption.frameDisplays[0];
    const { minDisplay, frameImageOptions, frameImageUri } = selectedDisplay || {};

    const {
      frameScale,
      frameScaleX,
      frameLeft,
      frameTop,
      frameRotateX,
      frameRotateY,
      frameRotateZ,
      framePerspective,
      framePerspectiveOrigin,
      imageUri,
      backgroundLeft,
      backgroundScale,
      backgroundTop,
    } = minDisplay;

    const top = 0;
    const left = 0;

    const {
      height,
      width,
      x,
      y,
    } = frameImageOptions || {};

    const frameBlockStyle = {
      height: `${frameScale}%`,
      width: `${frameScaleX}%`,
      left: `${frameLeft}%`,
      top: `${frameTop}%`,
      perspective: `${framePerspective}px`,
      perspectiveOrigin: framePerspectiveOrigin,
    };

    const blockStyle = {
      height: `${height}%`,
      width: `${width}%`,
      left: `${x}%`,
      top: `${y}%`,
      transform: generateRotateStyle(frameRotateX, frameRotateY, frameRotateZ),
    };

    const imageStyle = {
      height: `${100 - top * 2}%`,
      width: `${100 - left * 2}%`,
      top: `${top}%`,
      left: `${left}%`,
    };

    return (
      <div className={b('art-photo-block')}>
        <div
          className={b('art-wrap')}
        >
          <img
            className={b('display')}
            src={imageUri}
            alt="display"
            style={
              {
                width: `${backgroundScale}%`,
                left: `-${backgroundLeft}%`,
                top: `-${backgroundTop}%`,
              }
            }
          />
          <div className={b('frame-wrap')} style={frameBlockStyle}>
            <div className={b('image-wrap')} style={blockStyle}>
              <img
                className={b('art-photo')}
                style={imageStyle}
                src={this.selectProductImage(order).imageUri}
                alt="art"
              />
              <div className={b('shadow-block')} style={imageStyle} />
            </div>
            <img
              className={b('frame-image')}
              src={frameImageUri}
              alt="frame"
            />
          </div>
        </div>
      </div>
    );
  }

  deleteItem = (item) => {
    const { isTrial } = item;
    const { deleteOrder, clearPromo } = this.props;
    if (isTrial) {
      clearPromo();
    }
    this.setEditStatus();
    deleteOrder(item);
  }

  selectColorizationImage = (image, isSelected) => {
    const {
      order,
      changeOrdersCount,
      photoColorizationProduct,
      photoRestorationProduct,
      getOrderPaymentInfo,
    } = this.props;

    const { photo_restoration: photoRestoration, photo_colorization: photoColorization } = order;
    const { id: photoColorizationProductId, options: photoColorizationOptions } = photoColorizationProduct;
    const { id: photoRestorationProductId, options: photoRestorationOptions } = photoRestorationProduct;

    const { id: idImage} = image;

    const isPhotoRestorationList = photoRestoration.some(({ images }) => images.some(id => id===idImage));


    if (isPhotoRestorationList && isSelected) {
      const photoRestorationItem = photoRestoration.find(({ images }) => images.some(id => id===idImage));
      const { options: optionsItem = [] } = photoRestorationItem || {};

      const newOptions = optionsItem.reduce((acc, option) => {
        const { relationImagesOption, optionId, name } = option;
        if (name === 'easy_scan_ship_kit') {
          return [...acc, option];
        }
        const relationImageOption = relationImagesOption.find(({ imageId: reletionImageOptionImageId }) => reletionImageOptionImageId === idImage) || {};
        if (relationImageOption.imageId) {
          const { displayName: optionDisplayName } = photoRestorationOptions.find(({ id: photoColorizationOptionId }) => photoColorizationOptionId === optionId) || {};
          const photoColorizationOption = photoColorizationOptions.find(({ displayName: photoColorizationOptionName }) => photoColorizationOptionName === optionDisplayName) || {};
          const newRelationImagesOption = relationImagesOption.filter(({ imageId: reletionImageOptionImageId }) => reletionImageOptionImageId === idImage)

          return [...acc, {
            ...option,
            quantity: relationImageOption.quantity,
            relationImagesOption: newRelationImagesOption,
            optionId: photoColorizationOption.id,
          }];
        }
        return acc;
      }, []);
      const newPhotoRestorationItem = {
        ...photoRestorationItem,
        typeProduct: !isSelected ? 'photo_restoration' : 'photo_colorization',
        productId: !isSelected ? photoRestorationProductId : photoColorizationProductId,
        images: [idImage],
        options: newOptions,
        quantity: 1,
      };

      const newPhotoColorization = photoColorization.length ?
        photoColorization.map((item, index) => {
          if (index === 0) {
            const { images, options = [], quantity } = item || {};
            const optionsSame = options.map(option => {
              const optionSame = newOptions.find(optionTwo => option.optionId === optionTwo.optionId) || {};
              if (optionSame.optionId && optionSame.name !== 'easy_scan_ship_kit') {
                return {
                  ...option,
                  quantity: option.quantity + optionSame.quantity,
                  relationImagesOption: [...option.relationImagesOption, ...optionSame.relationImagesOption],
                }
              }
              return option;
            });

            const newOptionsSame = newOptions.filter(option => !options.some(optionTwo => option.optionId === optionTwo.optionId));

            const optionsNew = [...optionsSame, ...newOptionsSame];


            const easyScanKitOption = optionsNew.find(({ name }) => name === 'easy_scan_ship_kit');
            const optionNewFilter = optionsNew.filter(({ name }) => name !== 'easy_scan_ship_kit');
            return {
              ...item,
              images: [...images, idImage],
              options: easyScanKitOption ? [easyScanKitOption, ...optionNewFilter] : optionNewFilter,
              quantity: quantity + 1,
            }
          }
          return item;
        })
        : [...photoColorization, newPhotoRestorationItem];

      const newPhotoRestoration = photoRestorationItem.images.length === 1 ?
        photoRestoration.filter(({ images }) => !images.some(id => id === idImage))
        : photoRestoration.map(item => {
          const { images, id, options: optionsItm = [], quantity } = item || {};
          if (photoRestorationItem.id === id) {
            const newImages = images.filter(idPhoto => idPhoto !== idImage);
            const newOptionsItem = optionsItm.reduce((acc, itm) => {
              const { relationImagesOption, name } = itm;
              if (name === 'easy_scan_ship_kit') {
                return acc;
              }
              const relationImageOption = relationImagesOption.find(({ imageId: reletionImageOptionImageId }) => reletionImageOptionImageId === idImage) || {};
              if (relationImageOption.imageId) {
                const newRelationImagesOption = relationImagesOption.filter(({ imageId: reletionImageOptionImageId }) => reletionImageOptionImageId !== idImage)
                if (itm.quantity - relationImageOption.quantity === 0) {
                  return acc;
                }
                return [...acc, {
                  ...itm,
                  relationImagesOption: newRelationImagesOption,
                  quantity: itm.quantity - relationImageOption.quantity,
                }]
              }
              return [...acc, itm];
            }, []);
            return {
              ...item,
              images: newImages,
              options: newOptionsItem,
              quantity: quantity - 1,
            };
          }
          return item;
        });

      this.setEditStatus();
      changeOrdersCount({ typeProduct: 'photo_restoration', value: newPhotoRestoration });
      changeOrdersCount({ typeProduct: 'photo_colorization', value: newPhotoColorization });
      getOrderPaymentInfo();

      const enhanceImages = (JSON.parse(localStorage.getItem('enhance/Option') || '[]') || []);
      const newImages = enhanceImages.map((item) => {
        const { id: itemId } = item;
        if (itemId === idImage) {
          return { ...item, isColorization: isSelected };
        }
        return item;
      });
      localStorage.setItem('enhance/Option', JSON.stringify(newImages));
      this.setState({ colorizationImages: newImages });
    }

    if (!isPhotoRestorationList && !isSelected) {
      const photoColorizationItem = photoColorization.find(({ images }) => images.some(id => id === idImage));
      const { options: optionsItem = [] } = photoColorizationItem || {};

      const newOptions = optionsItem.reduce((acc, option) => {
        const { relationImagesOption, optionId, name } = option;
        if (name === 'easy_scan_ship_kit') {
          return [...acc, option];
        }
        const relationImageOption = relationImagesOption.find(({ imageId: reletionImageOptionImageId }) => reletionImageOptionImageId === idImage) || {};
        if (relationImageOption.imageId) {
          const { displayName: optionDisplayName } = photoColorizationOptions.find(({ id: photoColorizationOptionId }) => photoColorizationOptionId === optionId) || {};
          const photoRestorationOption = photoRestorationOptions.find(({ displayName: photoColorizationOptionName }) => photoColorizationOptionName === optionDisplayName) || {};
          const newRelationImagesOption = relationImagesOption.filter(({ imageId: reletionImageOptionImageId }) => reletionImageOptionImageId === idImage)

          return [...acc, {
            ...option,
            quantity: relationImageOption.quantity,
            relationImagesOption: newRelationImagesOption,
            optionId: photoRestorationOption.id,
          }];
        }
        return acc;
      }, []);
      const newPhotoColorizationItem = {
        ...photoColorizationItem,
        typeProduct: !isSelected ? 'photo_restoration' : 'photo_colorization',
        productId: !isSelected ? photoRestorationProductId : photoColorizationProductId,
        images: [idImage],
        options: newOptions,
        quantity: 1,
      };
      const newPhotoRestoration = photoRestoration.length ?
      photoRestoration.map((item, index) => {
          if (index === 0) {
            const { images, options = [], quantity } = item || {};

            const optionsSame = options.map(option => {
              const optionSame = newOptions.find(optionTwo => option.optionId === optionTwo.optionId) || {};
              if (optionSame.optionId && optionSame.name !== 'easy_scan_ship_kit') {
                return {
                  ...option,
                  quantity: option.quantity + optionSame.quantity,
                  relationImagesOption: [...option.relationImagesOption, ...optionSame.relationImagesOption],
                }
              }
              return option;
            });

            const newOptionsSame = newOptions.filter(option => !options.some(optionTwo => option.optionId === optionTwo.optionId));
            const optionsNew = [...optionsSame, ...newOptionsSame];
            const easyScanKitOption = optionsNew.find(({ name }) => name === 'easy_scan_ship_kit');
            const optionNewFilter = optionsNew.filter(({ name }) => name !== 'easy_scan_ship_kit');
            return {
              ...item,
              images: [...images, idImage],
              options: easyScanKitOption ? [easyScanKitOption, ...optionNewFilter] : optionNewFilter,
              quantity: quantity + 1,
            }
          }
          return item;
        })
        : [...photoRestoration, newPhotoColorizationItem];
      const newPhotoColorization = photoColorizationItem.images.length === 1 ?
        photoColorization.filter(({ images }) => !images.some(id => id === idImage))
        : photoColorization.map(item => {
          const { images, id, quantity } = item;
          if (photoColorizationItem.id === id) {
            const newImages = images.filter(idPhoto => idPhoto !== idImage);
            const newOptionsItem = optionsItem.reduce((acc, itm) => {
              const { relationImagesOption, name } = itm;
              if (name === 'easy_scan_ship_kit') {
                return acc;
              }
              const relationImageOption = relationImagesOption.find(({ imageId: reletionImageOptionImageId }) => reletionImageOptionImageId === idImage) || {};
              if (relationImageOption.imageId) {
                const newRelationImagesOption = relationImagesOption.filter(({ imageId: reletionImageOptionImageId }) => reletionImageOptionImageId !== idImage)
                if (itm.quantity - relationImageOption.quantity === 0) {
                  return acc;
                }
                return [...acc, {
                  ...itm,
                  relationImagesOption: newRelationImagesOption,
                  quantity: itm.quantity - relationImageOption.quantity,
                }]
              }
              return [...acc, itm];
            }, []);
            return {
              ...item,
              images: newImages,
              options: newOptionsItem,
              quantity: quantity - 1,
            };
          }
          return item;
        });

      this.setEditStatus();
      changeOrdersCount({ typeProduct: 'photo_restoration', value: newPhotoRestoration });
      changeOrdersCount({ typeProduct: 'photo_colorization', value: newPhotoColorization });
      getOrderPaymentInfo();

      const enhanceImages = (JSON.parse(localStorage.getItem('enhance/Option') || '[]') || []);
      const newImages = enhanceImages.map((item) => {
        const { id: itemId } = item;
        if (itemId === idImage) {
          return { ...item, isColorization: isSelected };
        }
        return item;
      });
      localStorage.setItem('enhance/Option', JSON.stringify(newImages));
      this.setState({ colorizationImages: newImages });
    }
  }

  changePersonalizePhotoArt = (idItem) => {
    const {
      order,
      changeOrdersCount,
      getOrderPaymentInfo,
    } = this.props;

    const { photo_art: photoArt } = order;

    const newPhotoArt = photoArt.map(item => {
      if (item.id === idItem) {
        return {
          ...item,
          customText: item.customText ? '' : item.isPersonalizeOn,
          isPersonalizeOn: item.customText ? item.customText : item.isPersonalizeOn,
        }
      }
      return item;
    });

    this.setEditStatus();
    changeOrdersCount({ typeProduct: 'photo_art', value: newPhotoArt });
    getOrderPaymentInfo();
  }

  changePersonalizePhotoArtText = (idItem, text, isPersonalize) => {
    const {
      order,
      changeOrdersCount,
      getOrderPaymentInfo,
    } = this.props;

    const { photo_art: photoArt } = order;

    const newPhotoArt = photoArt.map(item => {
      if (item.id === idItem) {
        return {
          ...item,
          customText: isPersonalize ? text : '',
          isPersonalizeOn: isPersonalize ? text : '',
        }
      }
      return item;
    });

    this.setState({ showPhotoArtPersonalizePopup: false});
    this.setEditStatus();
    changeOrdersCount({ typeProduct: 'photo_art', value: newPhotoArt });
    getOrderPaymentInfo();
  }

  getGuaranteeContent = (type) => {
    if (type === 'photo_art') {
      return { title: HAPPINESS_TITLE.create, text: HAPPINESS_TEXT.create }
    }

    if (DIGITIZE_PRODUCTS.includes(type)) {
      return { title: HAPPINESS_TITLE.digitize, text: HAPPINESS_TEXT.digitize }
    }

    if (ENHANCE_PRODUCTS.includes(type)) {
      return { title: HAPPINESS_TITLE.enhance, text: HAPPINESS_TEXT.enhance }
    }

    return {};
  }

  render() {
    const {
      currency,
      data,
      selectPrice,
      isDeletePopup,
      orderId,
      orderType,
      frameMaterials,
      orderCreateCalculations: {
        details,
      },
      order,
    } = this.props;

    const { colorizationImages, showPhotoArtPersonalizePopup, selectItem } = this.state;

    const newData = data.map((product, productIndex) => {
      const productDetails = details ? details.find((detail, index) => detail.productId === product.productId && index === productIndex) : null;
      if (productDetails) {
        const {
          children,
          personalDiscount,
          quantity,
          discount,
          price,
        } = productDetails;

        const numberPrice = Number(price);
        const numberQuantity = Number(quantity);
        const numberPersonalDiscount = Number(personalDiscount);

        const summOption = children.reduce((
          acc,
          item
        ) => {
          const itemDiscount = Number(item.personalDiscount);
          const itemQuantity = Number(item.quantity);
          const itemPrice = Number(item.price);

          return acc + (item.personalDiscount !== '0.00'
            ? (itemPrice - itemDiscount) * itemQuantity
            : itemPrice * itemQuantity)
          }, 0);

        const fullOptionSum = children.reduce((acc, item) => acc + (Number(item.price) * Number(item.quantity)), 0);

        return {
          ...product,
          summ: ((personalDiscount !== '0.00'
            ? (numberPrice - numberPersonalDiscount) * numberQuantity
            : numberPrice * numberQuantity)
            + summOption).toFixed(2),
          fullSumm: ((numberPrice * numberQuantity) + fullOptionSum).toFixed(2),
          discount: product.typeProduct === 'photo_art' ? children[0].discount : discount,
        };
      }
      return product;
    });

    const {
      open,
      popupOpen,
      openGuarantee,
      openGift,
    } = this.state;
    return (
      <article className={b()}>
        {newData.map((item) => {
          const {
            orderCheckoutTitle,
            typeProduct,
            checkoutOrderInfo,
            photoArtInfo,
            easyScanOptions,
          } = formattedPrice(item, currency, selectPrice[item.typeProduct], [], [...frameMaterials]);
          const {
            id: itemId,
            summ,
            typeProduct: itemTypeProduct,
            isInvalidOrder,
            text,
            textSecond,
            options = [],
            fullSumm,
            discount,
            customText,
          } = item || {};
          const isFree = (options || []).some(({ name, price }) => name === 'cloud_download' && price === 0);
          const isDisabledPersonalize = (options || []).some(({ name, quantity }) => name === '*******_safe' && quantity === 0);
          const isGuaranteeOpen = openGuarantee.includes(`${itemId}_${typeProduct}`);
          const isGiftOpen = openGift.includes(`${itemId}_${typeProduct}`);
          const guaranteeContent = this.getGuaranteeContent(typeProduct);
          const isOpen = open.some(el => el.id === item.id && item.typeProduct === el.typeProduct)
          return (
            <div className={b('order-wrapper')} key={`${typeProduct}-${itemId}`}>
              <div className={b('order-background')}>
                <div className={b('order', {'no-block-bottom': ENHANCE_PRODUCTS.includes(typeProduct) && !this.getPhotoFromStore(item.images).length })} key={itemId}>
                  <div className={b('order-info-wrapper')}>
                    <div className={b('order-image')}>
                      <div className={b('order-image-wrapper')}>
                        { itemTypeProduct === PHOTO_ART_PRODUCT
                          ? this.getPhotoArtPhoto(item)
                          : (
                            <img
                              className={b('order-image')}
                              src={this.selectProductImage(item).imageUri}
                              alt="order"
                            />
                        )}
                      </div>
                      <div
                        className={b('btn-delete', { mobile: true })}
                        role="button"
                        tabIndex="0"
                        onClick={this.openDeletePopup(itemId, itemTypeProduct)}
                      >
                        <TrashIcon2 className={b('delete-icon')} />
                      </div>
                    </div>
                    <div className={b('order-info')}>
                      <div className={b('order-title-wrapper')}>
                        <div className={b('order-title')}>{orderCheckoutTitle}</div>
                        <div
                          className={b('btn-delete', { desktop: true })}
                          role="button"
                          tabIndex="0"
                          onClick={this.openDeletePopup(itemId, itemTypeProduct)}
                        >
                          <TrashIcon2 className={b('delete-icon')} />
                        </div>
                      </div>
                      {Object.entries(checkoutOrderInfo).map(([key, value]) => {
                        const {
                          name,
                          quantity,
                          hide,
                          minValue,
                          isNotEdit,
                        } = value;
                        if ((name === 'easy_scan-ship_kit' && quantity === 0) || hide) {
                          return null;
                        }
                        return (
                          Boolean(value) && (
                            <Product
                              item={value}
                              isFree={isFree}
                              isDisabledPersonalize={isDisabledPersonalize}
                              typeProduct={typeProduct}
                              key={key}
                              name={key}
                              id={itemId}
                              text={text}
                              textSecond={textSecond}
                              popupOpen={popupOpen}
                              setPopupOpen={this.closePopupOpen}
                              changeCount={this.changeCount}
                              enterQuantity={this.enterQuantity}
                              onSubmit={(backValue, backValueSecond, clear) => {
                                this.changePersonalize(
                                  backValue,
                                  backValueSecond,
                                  clear,
                                  typeProduct,
                                  itemId,
                                );
                              }}
                              minValue={minValue}
                              isNotEdit={isNotEdit}
                            />
                          )
                        );
                      })}
                      {[RESTORATION_PRODUCT,
                          RETOUCHING_PRODUCT,
                          COLORIZATION_PRODUCT].includes(typeProduct)
                        && this.getOptionsProduct(item)}
                      {typeProduct === PHOTO_ART_PRODUCT
                        && photoArtInfo.map(({
                          id,
                          title,
                          name,
                          summ: summOption,
                        }, index) => (
                          <div className={b('option', { 'no-bottom': index !== photoArtInfo.length - 1, first: index === 0 })} key={id}>
                            <div className={b('description-block', { personalize: title === 'Personalize: ' })}>
                              <p className={b('description', { art: true })}>{title}</p>
                              <span className={b('description-name', { art: true })}>{name}</span>
                              { title === 'Personalize: ' && (
                              <button
                                className={b('btn-edit')}
                                type="button"
                                tabIndex={0}
                                disabled={name === 'personalize' && isDisabledPersonalize}
                                onClick={() => { this.setState( { showPhotoArtPersonalizePopup: true, selectItem: item }) }}
                              >
                                <EditIcon className={b('edit-icon')} />
                              </button>
                                )}
                            </div>
                            <div className={b('controls')}>
                              {title === 'Personalize: ' && (
                                <NewTogglerInput
                                  checked={customText}
                                  minSize
                                  withoutText
                                  onChange={() => { this.changePersonalizePhotoArt(itemId) }}
                                />
                              )}
                            </div>
                            <div className={b('description-sum')}>{summOption >=0 ? `$${(summOption).toFixed(2)}` : ''}</div>
                          </div>
                        )
                      )}
                      {(typeProduct === EASY_SCAN_SHIP_KIT_TYPE
                        && easyScanOptions
                        && easyScanOptions.length)
                        && easyScanOptions.map(({ id, title, name }) => (
                          <div className={b('option')} key={id}>
                            <p className={b('description')}>{title}</p>
                            <span className={b('description-name')}>{name}</span>
                          </div>
                          )
                        )}
                      {(DIGITIZE_PRODUCTS.includes(typeProduct) && isInvalidOrder) && (
                        <span className={b('error-text')}>{CHOOSE_DELIVERABLES}</span>
                      )}
                      <div className={b('order-summ')}>
                        <div className={b('order-summ-top')} />
                        <div className={b('order-summ-content')}>
                          <span className={b('order-summ-description')}>{TOTAL}</span>
                          <div className={b('order-summ-wrapper')}>
                            <span className={b('order-summ-prefix')}>{USD}</span>
                            <span className={b('order-summ-price', { origin: discount !== '0.00' })}>
                              {`${CURRENCY}${fullSumm}`}
                            </span>
                          </div>
                          {discount !== '0.00' ? (
                            <span className={b('order-summ-price', { custom: discount !== '0.00' })}>
                              {`${CURRENCY}${summ}`}
                            </span>
                          ) : null}
                        </div>
                      </div>
                    </div>
                  </div>
                  {(isDeletePopup && orderId === itemId && itemTypeProduct === orderType) && (
                    <div className={b('delete-popup')}>
                      <span className={b('delete-title')}>{CONFIRM_QUESTION}</span>
                      <div className={b('confirmation-wrapper')}>
                        <button className={b('delete-confirm')} onClick={() => this.deleteItem(item)} type="button">{YES}</button>
                        <button className={b('delete-cancel')} onClick={this.closeDeletePopup} type="button">{NO}</button>
                      </div>
                    </div>
                  )}
                  { !ENHANCE_PRODUCTS.includes(typeProduct) ? (
                    <div className={b('gift-button-wrapper')}>
                      <div
                        className={b('gift-button', { checked: isGiftOpen })}
                        role="button"
                        tabIndex={-1}
                        onClick={() => {
                          if (isGuaranteeOpen) { this.togglePopup(itemId, typeProduct, 'openGuarantee'); }
                          this.togglePopup(itemId, typeProduct, 'openGift');
                        }}
                      >
                        <div className={b('gift-button-icon')}>
                          <GiftIcon stroke={isGiftOpen ? pickledBluewoodColor : slateGrayColor} />
                        </div>
                        <div className={b('gift-button-text', { checked: isGiftOpen, desktop: true })}>
                          {THIS_IS_GIFT}
                        </div>
                        <div className={b('gift-button-text', { checked: isGiftOpen, mobile: true })}>
                          {GIFT}
                        </div>
                      </div>
                      <div
                        className={b('gift-button', { checked: isGuaranteeOpen })}
                        role="button"
                        tabIndex={-1}
                        onClick={() => {
                          if (isGiftOpen) { this.togglePopup(itemId,typeProduct, 'openGift') }
                          this.togglePopup(itemId, typeProduct, 'openGuarantee');
                        }}
                      >
                        <div className={b('gift-button-icon')}>
                          <HeartIcon stroke={isGuaranteeOpen ? pickledBluewoodColor : slateGrayColor} />
                        </div>
                        <div className={b('gift-button-text', { checked: isGuaranteeOpen, desktop: true })}>
                          {OUR_GUARANTEE}
                        </div>
                        <div className={b('gift-button-text', { checked: isGuaranteeOpen, mobile: true })}>
                          {GUARANTEE}
                        </div>
                      </div>
                    </div>
                  ) : (
                    Boolean(this.getPhotoFromStore(item.images).length) && (
                    <div
                      className={b('gift-button-wrapper', { enhance: true })}
                    >
                      <div
                        className={b('gift-button', { checked: isOpen})}
                        role="button"
                        tabIndex={-1}
                        onClick={() => {
                          if (isGuaranteeOpen) { this.togglePopup(itemId, typeProduct, 'openGuarantee'); }
                          if (isGiftOpen) { this.togglePopup(itemId,typeProduct, 'openGift') }
                          this.editOptions(item.id, typeProduct)
                        }}
                      >
                        <div className={b('edit-product-icon')}>
                          <EditIcon
                            stroke={isOpen ? pickledBluewoodColor : slateGrayColor}
                          />
                        </div>
                        <div className={b('gift-button-text', { checked: isOpen, desktop: true })}>
                          {EDIT_PRODUCT}
                        </div>
                        <div className={b('gift-button-text', { checked: isOpen, mobile: true })}>
                          {EDIT}
                        </div>
                      </div>
                      <div
                        className={b('gift-button', { checked: isGuaranteeOpen })}
                        role="button"
                        tabIndex={-1}
                        onClick={() => {
                          if (open.some(el => el.id === item.id)) { this.editOptions(item.id, typeProduct); }
                          if (isGiftOpen) { this.togglePopup(itemId,typeProduct, 'openGift') }
                          this.togglePopup(itemId, typeProduct, 'openGuarantee');
                        }}
                      >
                        <div className={b('gift-button-icon')}>
                          <HeartIcon
                            stroke={isGuaranteeOpen ? pickledBluewoodColor : slateGrayColor}
                          />
                        </div>
                        <div className={b('gift-button-text', { checked: isGuaranteeOpen, desktop: true })}>
                          {OUR_GUARANTEE}
                        </div>
                        <div className={b('gift-button-text', { checked: isGuaranteeOpen, mobile: true })}>
                          {GUARANTEE}
                        </div>
                      </div>
                    </div>
                  ))}
                </div>
                {isGiftOpen && (
                  <div className={b('gift-wrapper')}>
                    <IsGiftOrder
                      item={item}
                      typeProduct={typeProduct}
                      close={() => this.togglePopup(itemId, typeProduct, 'openGift')}
                    />
                  </div>
                )}
                {isGuaranteeOpen && (
                  <div className={b('guaranty-wrap')}>
                    <div className={b('guaranty-image-block')}>
                      <img
                        className={b('guaranty-image')}
                        src={guarantyImage.src}
                        alt="logo"
                        loading="lazy"
                      />
                    </div>
                    <div className={b('guaranty-text-block')}>
                      <span className={b('guaranty-title')}>{guaranteeContent.title || ''}</span>
                      <span className={b('guaranty-text')}>{guaranteeContent.text || ''}</span>
                    </div>
                  </div>
                )}
                {isOpen && (
                  <div className={b('options-wrapper', { visible: isOpen })}>
                    <ChooseAnyPhoto
                      className={b('options')}
                      nameOperation={item.typeProduct}
                      orderImagesEnhance={this.getPhotoFromStore(item.images)}
                      appendOptionEnhance={this.formatOption(order[item.typeProduct], item.images)}
                      changeAppendOptions={value => this.changeAppendOptions(value, item.typeProduct, item.id)}
                      hidden
                      isCheckout
                      selectColorizationImage={this.selectColorizationImage}
                      colorizationImages={colorizationImages}
                    />
                  </div>
                )}
              </div>
            </div>
          );
        })}
        { showPhotoArtPersonalizePopup && ReactDOM.createPortal(
          <PhotoArtPersonalizeCard
            initialText={selectItem.isPersonalizeOn}
            minHeight
            onSubmit={this.changePersonalizePhotoArtText}
            item={selectItem}
            dollar=""
            closePopUp={() => this.setState({ showPhotoArtPersonalizePopup: false })}
          />, document.getElementById('__next') || null,
        )}
      </article>
    );
  }
}

OrderCheckoutCard.propTypes = propTypes;
OrderCheckoutCard.defaultProps = defaultProps;

const stateProps = state => ({
  order: ordersTypeSelect(state),
  price: priceSelect(state),
  selectPrice: orderPriceSelect(state),
  allOrderImages: orderAllImagesSelector(state),
  frameMaterials: frameMaterialsSelector(state),
  photoArtProducts: photoArtProductListSelector(state),
  orderCreateCalculations: calculateOrderInfoSelector(state),
  photoRestorationProduct: photoRestorationSelect(state),
  photoColorizationProduct: photoColorizationSelect(state),
});

const actions = {
  deleteOrder: deleteFromCart,
  orderChange: orderChangefromCart,
  orderInputChange: orderChangeInputFromCart,
  changeOrdersCount: changeOrder,
  clearPromo: clearCoupon,
};

export default connect(stateProps, actions)(OrderCheckoutCard);
