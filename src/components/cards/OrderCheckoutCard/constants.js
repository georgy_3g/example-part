const CONFIRM_QUESTION = 'Are you sure?';
const THIS_IS_GIFT = 'Make this a gift';
const EDIT_PRODUCT = 'Edit product';
const YES = 'Yes';
const NO = 'No';
const FREE = 'Free';
const TOTAL = 'Total';
const USD = 'USD';
const CURRENCY = '$';
const EDIT = 'Edit';
const GIFT = 'Gift';
const TOGGLER_PRODUCT_DVD = ['tape_transfer', 'audio_transfer'];
const DVD_SET = 'archival_dvd_set';
const OUR_GUARANTEE = 'Our guarantee';
const GUARANTEE = 'Guarantee';
const HAPPINESS_TITLE = {
  enhance: '*******\nLove it guarantee.',
  create: '******* \nHappiness Guarantee.',
  digitize: '******* \nHappiness Guarantee.',
};

const HAPPINESS_TEXT = {
  enhance:
    'Unlimited revisions to your photo are included. If you\nare for not satisfied with your restoration, you may give\nus a different photo to restore or receive a store credit\nfor the full amount of your restoration.',
  create:
    'If you are not satisfied with your photo product, \nplease let us know within 60 days, and we will \ndo everything we can to make your order right.',
  digitize:
    'If you are not satisfied with your digitize \nproduct, please let us know within 60 days and \nwe promise we will do everything we can to \nmake your order right.',
};

export {
  THIS_IS_GIFT,
  EDIT_PRODUCT,
  CONFIRM_QUESTION,
  YES,
  NO,
  FREE,
  TOTAL,
  USD,
  CURRENCY,
  EDIT,
  GIFT,
  DVD_SET,
  TOGGLER_PRODUCT_DVD,
  OUR_GUARANTEE,
  GUARANTEE,
  HAPPINESS_TITLE,
  HAPPINESS_TEXT,
};
