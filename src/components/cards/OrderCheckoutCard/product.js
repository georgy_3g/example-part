import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import bem from 'src/utils/bem';
import { ENHANCE_TERMS } from 'src/terms';
import Dom from 'react-dom';
import BigCountInput from 'src/components/inputs/BigCountInput';
import dynamic from 'next/dynamic';
import { personalizeSectionsSelector } from 'src/redux/wizardsCards/selectors';
import { getWizardCardsToCartAction } from 'src/redux/wizardsCards/actions';
import { DVD_SET, FREE, TOGGLER_PRODUCT_DVD } from './constants';
import styles from './index.module.scss';

const DigitizePersonalizeCard = dynamic(() =>
  import('src/components/cards/DigitizePersonalizeCard'),
);
const EditIcon = dynamic(() => import('src/components/svg/EditIcon'));
const NewTogglerInput = dynamic(() => import('src/components/inputs/NewTogglerInput'));

const b = bem('checkout-card', styles);

const BUTTON_TYPE = {
  dec: false,
  inc: true,
};

const { RESTORATION_PRODUCT, COLORIZATION_PRODUCT, RETOUCHING_PRODUCT } = ENHANCE_TERMS;

const defaultProps = {
  item: {},
  isFree: false,
  isDisabledPersonalize: false,
  typeProduct: '',
  name: '',
  id: 0,
  text: '',
  textSecond: '',
  popupOpen: null,
  setPopupOpen: () => {},
  changeCount: () => {},
  enterQuantity: () => {},
  onSubmit: () => {},
  minValue: 0,
  isNotEdit: false,
  personalizeSections: {},
  getWizard: () => {},
};

const propTypes = {
  item: PropTypes.shape({
    title: PropTypes.string,
    price: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    fullPrice: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    quantity: PropTypes.number,
    sum: PropTypes.number,
    withoutSum: PropTypes.bool,
    name: PropTypes.string,
    optionId: PropTypes.number,
    description: PropTypes.string,
  }),
  isFree: PropTypes.bool,
  isDisabledPersonalize: PropTypes.bool,
  typeProduct: PropTypes.string,
  name: PropTypes.string,
  id: PropTypes.number,
  changeCount: PropTypes.func,
  enterQuantity: PropTypes.func,
  onSubmit: PropTypes.func,
  setPopupOpen: PropTypes.func,
  popupOpen: PropTypes.number,
  text: PropTypes.string,
  textSecond: PropTypes.string,
  minValue: PropTypes.number,
  isNotEdit: PropTypes.bool,
  personalizeSections: PropTypes.shape({}),
  getWizard: PropTypes.func,
};

function Product(props) {
  const {
    text,
    textSecond,
    item,
    typeProduct,
    name,
    id,
    changeCount,
    enterQuantity,
    onSubmit,
    popupOpen,
    setPopupOpen,
    isFree,
    isDisabledPersonalize,
    minValue,
    isNotEdit,
    personalizeSections = {},
    getWizard,
  } = props;

  const {
    title,
    description,
    price,
    fullPrice,
    quantity,
    sum,
    withoutSum,
    optionId,
  } = item;

  useEffect(() => {
    if (!personalizeSections[optionId] && name === 'personalize') {
      getWizard({ product: typeProduct, id: optionId });
    }
  });

  const { images = [] } = personalizeSections[optionId] || {};

  const isProductIncluded = [
    RESTORATION_PRODUCT,
    COLORIZATION_PRODUCT,
    RETOUCHING_PRODUCT,
  ].includes(typeProduct);

  const toggleNumber = TOGGLER_PRODUCT_DVD.includes(typeProduct) && name === DVD_SET;

  return (
    title && (
      <div className={b('option')}>
        <div className={b('description-wrapper', { 'full-width': withoutSum })}>
          <div className={b('description', { bottom: withoutSum })}>
            {title}
            {name === 'personalize' && (
              <button
                className={b('btn-edit')}
                type="button"
                tabIndex={0}
                onClick={() => setPopupOpen(id)}
                disabled={name === 'personalize' && isDisabledPersonalize}
              >
                <EditIcon className={b('edit-icon')} />
              </button>
            )}
          </div>
          <span className={b('description', { 'full-width': withoutSum })}>{description}</span>
          {!withoutSum && (
            <div className={b('description-price-wrapper')}>
              <span className={b('description-price', { desktop: true, origin: price !== fullPrice })}>
                {isFree && name === 'cloud_download' ? FREE : fullPrice}
              </span>
              {price !== fullPrice && (
                <span className={b('description-price', { desktop: true })}>
                  {isFree && name === 'cloud_download' ? FREE : price}
                </span>
              )}
            </div>
          )}
        </div>
        {!withoutSum && (
          <div className={b('option-description')}>
            {!withoutSum && (
              <span className={b('description-price', { mobile: true })}>
                {isFree && name === 'cloud_download' ? FREE : price}
              </span>
            )}
            <div className={b('controls-wrapper')}>
              {isProductIncluded ? (
                <div className={b('controls')}>
                  <span className={b('count')}>
                    {`${quantity} ${quantity > 1 ? 'photos' : 'photo'}`}
                  </span>
                </div>
              ) : (
                <div className={b('controls')}>
                  {typeof quantity === 'boolean' || toggleNumber  ? (
                    <NewTogglerInput
                      checked={quantity}
                      minSize
                      onChange={(checked) => {
                        const value = checked ? BUTTON_TYPE.inc : BUTTON_TYPE.dec;
                        changeCount({
                          isAdd: value,
                          typeProduct,
                          itemId: id,
                          key: name,
                          isDependProduct: toggleNumber,
                        })
                      }}
                      withoutText
                      disabled={(name === 'personalize' && isDisabledPersonalize) || isNotEdit}
                    />
                  ) : (
                    <BigCountInput
                      className={b('quantity')}
                      inputClass={b('width-input')}
                      autoWidth
                      name={name}
                      count={quantity}
                      bemCount={b}
                      withInput
                      mainCounter
                      withReplacement
                      minSize
                      hiddenMobileControls
                      min={minValue || (name === 'quantity' ? 1 : 0)}
                      onChange={(value) => enterQuantity(value, typeProduct, id, name)}
                      changeCount={(isAdd) => () => changeCount({
                        isAdd,
                        typeProduct,
                        itemId: id,
                        key: name,
                        value: quantity,
                      })}
                      disabled={isNotEdit}
                    />
                  )}
                </div>
              )}
            </div>
            {!withoutSum && (
              <span className={b('description-sum', { disabled: !quantity, mobile: true })}>
                {isFree && name === 'cloud_download' ? FREE : sum}
              </span>
            )}
          </div>
        )}
        {!withoutSum && (
          <span className={b('description-sum', { disabled: !quantity, desktop: true })}>
            {isFree && name === 'cloud_download' ? FREE : sum}
          </span>
        )}
        {popupOpen === id &&
          name === 'personalize' &&
          Dom.createPortal(
            <DigitizePersonalizeCard
              initialText={{ text, textSecond }}
              price={price}
              minHeight
              onSubmit={onSubmit}
              item={{ ...item, images }}
              dollar=""
              closePopUp={() => setPopupOpen(null)}
            />,
            document.getElementById('__next') || null,
          )}
      </div>
    )
  );
}

Product.propTypes = propTypes;
Product.defaultProps = defaultProps;

const stateProps = (state) => ({
  personalizeSections: personalizeSectionsSelector(state),
});

const actions = {
  getWizard: getWizardCardsToCartAction,
};

export default connect(stateProps, actions)(Product);
