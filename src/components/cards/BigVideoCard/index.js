import React, { Component } from 'react';
import PropTypes from 'prop-types';
import bem from 'src/utils/bem';
import { connect } from 'react-redux';
import colors from 'src/styles/colors.json';
import dynamic from 'next/dynamic';
import { stopAllPlayersSelector } from 'src/redux/myCloud/selectors';
import { changePlayerFlagAction } from 'src/redux/myCloud/actions';
import styles from './index.module.scss';

const CheckFull = dynamic(() => import('src/components/svg/CheckFull'));
const NewMagnifyingMinusIcon = dynamic(() => import('src/components/svg/NewMagnifyingMinusIcon'));
const ImageMenu = dynamic(() => import('src/components/elements/ImageMenu'));
const MediaButton = dynamic(() => import('src/components/elements/MediaButton'));
const FullScreenIcon = dynamic(() => import('src/components/svg/FullScreenIcon'));

const darkBlue = colors['$dark-slate-blue-color'];
const whiteColor = colors['$white-color'];
const greenColor = colors['$summer-green-color'];
const grayColor = colors['$slate-gray-color'];

const b = bem('big-video-card', styles);

const defaultProps = {
  fileData: null,
  className: '',
  selected: [],
  addToSelected: () => {},
  removeFromSelected: () => {},
  forSharedPage: false,
  selectTailView: () => {},
  stopAllPlayers: false,
  changePlayerFlag: () => {},
};

const propTypes = {
  fileData: PropTypes.shape({
    videoUri: PropTypes.string,
    id: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    name: PropTypes.string,
  }),
  className: PropTypes.string,
  selected: PropTypes.arrayOf(PropTypes.shape({})),
  addToSelected: PropTypes.func,
  removeFromSelected: PropTypes.func,
  forSharedPage: PropTypes.bool,
  selectTailView: PropTypes.func,
  stopAllPlayers: PropTypes.bool,
  changePlayerFlag: PropTypes.func,
};

class BigVideoCard extends Component {
  constructor(props) {
    super(props);
    this.player = null;
    this.state = {
      isPlay: false,
      wrapRef: null,
    };
  }

  componentDidUpdate(prevProps) {
    const { stopAllPlayers } = this.props;
    const { stopAllPlayers: oldFlag } = prevProps;

    if (!oldFlag && stopAllPlayers) {
      this.clickPause();
    }
  }

  componentWillUnmount() {
    this.player = null;
  }

  checkSelect = () => {
    const {
      fileData: { id, videoUri },
      selected,
    } = this.props;
    return Boolean(selected.find((item) => item.id === id && item.videoUri === videoUri));
  };

  clickPlay = (e) => {
    const { changePlayerFlag } = this.props;
    changePlayerFlag(false);
    this.player.play();
    this.setState({ isPlay: true });
    if (e) {
      e.stopPropagation();
    }
  };

  clickPause = (e) => {
    this.player.pause();
    this.setState({ isPlay: false });
    if (e) {
      e.stopPropagation();
    }
  };

  addWrapRef = (e) => {
    this.setState({ wrapRef: e });
  };

  onClick = () => {
    const { fileData, removeFromSelected, addToSelected } = this.props;
    const isSelect = this.checkSelect();
    if (isSelect) {
      removeFromSelected(fileData);
    } else {
      addToSelected(fileData);
    }
  };

  openFullscreen = (e) => {
    this.player.requestFullscreen();
    if (e) {
      e.stopPropagation();
    }
  };

  saveRef = (e) => {
    if (e) {
      e.requestFullscreen =
        e.requestFullscreen ||
        e.mozRequestFullscreen ||
        e.webkitRequestFullscreen ||
        e.msRequestFullscreen;
    }
    this.player = e;
  };

  render() {
    const { isPlay, wrapRef } = this.state;

    const { fileData, className, forSharedPage, selectTailView } = this.props;

    const { videoUri } = fileData;

    const isSelect = this.checkSelect();

    return (
      <div
        className={b({ mix: className, select: isSelect })}
        role="button"
        tabIndex={-1}
        onClick={this.onClick}
      >
        <button className={b('select-btn')} type="button" onClick={this.onClick}>
          <CheckFull
            className={b('select-icon')}
            stroke={isSelect ? greenColor : whiteColor}
            stroke2={isSelect ? whiteColor : 'none'}
            fill={isSelect ? greenColor : `${grayColor}88`}
          />
        </button>
        <div className={b('content-wrap')} ref={this.addWrapRef}>
          <div className={b('video-block')}>
            <video className={b('video')} width="100%" ref={this.saveRef}>
              <source src={videoUri} />
              <track kind="captions" />
            </video>
            <MediaButton
              className={b('media-button')}
              isPlay={isPlay}
              onClick={isPlay ? this.clickPause : this.clickPlay}
            />
            <button className={b('full-screen-btn')} type="button" onClick={this.openFullscreen}>
              <FullScreenIcon />
            </button>
            <div className={b('button-block')}>
              <div className={b('button-wrap')}>
                <button className={b('zoom-button')} type="button" onClick={selectTailView}>
                  <NewMagnifyingMinusIcon className={b('zoom-icon')} />
                </button>
              </div>
              <div className={b('menu-button', { end: true })}>
                <ImageMenu
                  className={b('button-icon')}
                  iconFillColor={darkBlue}
                  imageData={fileData}
                  forSharedPage={forSharedPage}
                  portalBlock={wrapRef}
                  withOutOrderPrint
                  slider
                  type="video"
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

BigVideoCard.propTypes = propTypes;
BigVideoCard.defaultProps = defaultProps;

const stateProps = (state) => ({
  stopAllPlayers: stopAllPlayersSelector(state),
});

const actions = {
  changePlayerFlag: changePlayerFlagAction,
};
export default connect(stateProps, actions)(BigVideoCard);
