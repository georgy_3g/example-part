import React from 'react';
import bem from 'src/utils/bem';
import PropTypes from 'prop-types';
import LazyLoad from 'react-lazyload';
import Image from 'next/image';
import styles from './index.module.scss';

const b = bem('easy-card', styles);

const defaultProps = {
  card: {},
  isFirst: false,
  isLast: false,
  className: '',
  isLargeCards: false,
};

const propTypes = {
  card: PropTypes.shape({
    cardImage: PropTypes.shape({}),
    cardText: PropTypes.string,
    isLink: PropTypes.bool,
    link: PropTypes.string,
    linkText: PropTypes.string,
  }),
  isFirst: PropTypes.bool,
  isLast: PropTypes.bool,
  className: PropTypes.string,
  isLargeCards: PropTypes.bool,
};

const EasyCard = (props) => {
  const { card, isFirst, isLast, className, isLargeCards } = props;
  const { cardImage, cardText, isLink, link, linkText } = card;

  return (
    <div
      className={b({
        isFirst,
        isLast,
        isLarge: isLargeCards,
        mix: className,
      })}
    >
      <LazyLoad offset={100}>
        <div className={b('img-container')}>
          <Image
            className={b('img')}
            src={cardImage}
            alt="card"
            layout="responsive"
            width={170}
            height={135}
          />
        </div>
      </LazyLoad>
      <div className={b('text', { isLarge: isLargeCards })}>{cardText}</div>
      {isLink && (
        <a className={b('link')} href={link}>
          <div className={b('link-text')}>{linkText}</div>
        </a>
      )}
    </div>
  );
}

EasyCard.propTypes = propTypes;
EasyCard.defaultProps = defaultProps;

export default EasyCard;
