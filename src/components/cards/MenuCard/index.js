import React from 'react';
import PropTypes from 'prop-types';
import bem from 'src/utils/bem';
import styles from './index.module.scss';

const b = bem('menu-card', styles);

const defaultProps = {
  data: {},
};

const propTypes = {
  data: PropTypes.shape({
    text: PropTypes.string,
    link: PropTypes.string,
    img: PropTypes.string,
  }),
};

function MenuCard(props) {
  const { data } = props;

  const { text, link, img } = data;
  return (
    <a className={b()} href={link} key={text}>
      <img className={b('list-link-img')} src={img.src || img} alt={text} />
      <span className={b('list-link-text')}>{text}</span>
    </a>
  );
}

MenuCard.propTypes = propTypes;
MenuCard.defaultProps = defaultProps;

export default MenuCard;
