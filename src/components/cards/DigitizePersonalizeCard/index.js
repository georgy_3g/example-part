import React, { Component } from 'react';
import bem from 'src/utils/bem';
import PropTypes from 'prop-types';
import uniqueId from 'lodash/uniqueId';
import colors from 'src/styles/colors.json';
import Image from 'next/image';
import dynamic from "next/dynamic";
import {
  PLACEHOLDER,
  PLACEHOLDER_SECOND,
  ADDITIONAL,
  NO_THANKS,
  YES_ADD,
  MAX_LENGTH_TOP_LINE,
  MAX_LENGTH_TOP_BOTTOM,
} from './constants';
import styles from './index.module.scss';

const ColorButton = dynamic(() => import('src/components/buttons/ColorButton'));
const NewCustomInput = dynamic(() => import('src/components/inputs/NewCustomInput'));
const CloseIcon2 = dynamic(() => import('src/components/svg/CloseIcon2'));

const darkBlueColor = colors['$slate-gray-color'];
const redColor = colors['$burnt-sienna-red-color'];

const b = bem('digitize-personalize-card', styles);

const defaultProps = {
  item: {},
  closePopUp: () => {},
  onSubmit: () => {},
  price: 0,
  initialText: '',
  minHeight: false,
  hidenSlider: false,
  dollar: '$',
};

const propTypes = {
  item: PropTypes.shape({
    images: PropTypes.arrayOf(PropTypes.shape({})),
    title: PropTypes.string,
    subTitle: PropTypes.string,
    description: PropTypes.string,
  }),
  closePopUp: PropTypes.func,
  onSubmit: PropTypes.func,
  price: PropTypes.number,
  initialText: PropTypes.string,
  minHeight: PropTypes.bool,
  hidenSlider: PropTypes.bool,
  dollar: PropTypes.string,
};

class DigitizePersonalizeCard extends Component {
  constructor(props) {
    super(props);
    const { initialText } = props;

    this.veil = React.createRef();
    this.state = {
      text: initialText.text || '',
      textSecond: initialText.textSecond || '',
    };
  }

  closeModal = (e) => {
    const { closePopUp } = this.props;
    if (e.target === this.veil.current) {
      closePopUp();
    }
  };

  closeButton = () => {
    const { closePopUp } = this.props;
    closePopUp();
  };

  onChange = (name, value) => {
    this.setState({ [name]: value });
  };

  submit = (isPersonalize) => {
    const { onSubmit } = this.props;
    const { text, textSecond } = this.state;
    onSubmit(text, textSecond, isPersonalize);
  };

  render() {
    const { text, textSecond } = this.state;
    const { item = {}, price, dollar, hidenSlider, minHeight } = this.props;
    const { title, subTitle = '', description } = item;
    const images = item.images
      ? item.images.map((url) => ({ url, id: uniqueId(), alt: 'digitize image' }))
      : [];
    return (
      <div className={b()} ref={this.veil} onClick={this.closeModal} role="button" tabIndex="0">
        <div className={b('wrapper', { 'min-height': minHeight })}>
          <div className={b('close-btn')} onClick={this.closeButton} role="button" tabIndex={0}>
            <CloseIcon2 />
          </div>
          <div className={b('card-slider', { hidden: hidenSlider })}>
            <div className={b('card-slider-text')}>
              { text ? <p className={b('card-slider-text-content', { first: true })}>{text}</p> : null }
              { textSecond ? <p className={b('card-slider-text-content', { second: true })}>{textSecond}</p> : null }
            </div>
            <Image className={b('image')} src={images[0].url} layout="fill" />
          </div>
          <div className={b('card-content')}>
            <div className={b('title')}>{title}</div>
            { description ?  <div className={b('sub-title')}>{description}</div> : null }
            <div className={b('input-wrapper')}>
              <NewCustomInput
                className={b('input')}
                onChange={({ target: { name, value } }) => this.onChange(name, value)}
                name="text"
                placeholder={PLACEHOLDER}
                value={text}
                maxLength={MAX_LENGTH_TOP_LINE}
              />
              <NewCustomInput
                className={b('input')}
                onChange={({ target: { name, value } }) => this.onChange(name, value)}
                name="textSecond"
                placeholder={PLACEHOLDER_SECOND}
                value={textSecond}
                maxLength={MAX_LENGTH_TOP_BOTTOM}
              />
            </div>
            <div className={b('description')}>{`${ADDITIONAL} ${dollar}${price} ${subTitle}`}</div>
            <div className={b('buttons')}>
              <ColorButton
                backGroundColor={darkBlueColor}
                text={NO_THANKS}
                onClick={() => this.submit(false)}
              />
              <ColorButton
                backGroundColor={redColor}
                text={YES_ADD}
                onClick={() => this.submit(true)}
              />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

DigitizePersonalizeCard.propTypes = propTypes;
DigitizePersonalizeCard.defaultProps = defaultProps;

export default DigitizePersonalizeCard;
