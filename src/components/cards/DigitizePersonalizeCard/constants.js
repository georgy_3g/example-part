const SELECTED_TEXT = 'Selected';
const CREATE_TEXT = 'Create';
const FROM_TEXT = 'from ';
const DOLLAR = '$';
const PLACEHOLDER = 'Enter your personalization title...';
const PLACEHOLDER_SECOND = 'Enter your personalization here..';
const ADDITIONAL = 'Additional';
const NO_THANKS = 'No, thanks';
const YES_ADD = 'Yes, Add!';
const MAX_LENGTH_TOP_LINE = 20;
const MAX_LENGTH_TOP_BOTTOM = 30;

export {
  SELECTED_TEXT,
  CREATE_TEXT,
  FROM_TEXT,
  DOLLAR,
  PLACEHOLDER,
  PLACEHOLDER_SECOND,
  ADDITIONAL,
  NO_THANKS,
  YES_ADD,
  MAX_LENGTH_TOP_LINE,
  MAX_LENGTH_TOP_BOTTOM,
};
