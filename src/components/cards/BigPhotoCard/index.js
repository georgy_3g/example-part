import React, { useState } from 'react';
import bem from 'src/utils/bem';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import colors from 'src/styles/colors.json';
import dynamic from 'next/dynamic';
import { loadImages, loadImagesOrderEnhance } from 'src/redux/orderImages/actions';
import { getToken } from 'src/redux/auth/selectors';
import styles from './index.module.scss';

const CheckFull = dynamic(() => import('src/components/svg/CheckFull'));
const ImageComponent = dynamic(() => import('src/components/elements/Image'));
const ImageMenu = dynamic(() => import('src/components/elements/ImageMenu'));

const grayColor = colors['$gray-color'];
const turquoiseColor = colors['$summer-green-color'];
const darkBlue = colors['$white-color'];

const b = bem('big-photo-card', styles);

const defaultProps = {
  data: {},
  className: '',
  contentClass: '',
  select: () => {},
  index: 0,
  selected: 0,
  withoutText: false,
  iconClass: '',
  isSelected: false,
  onImageError: () => {},
  onLoadImage: () => {},
  forSharedPage: false,
  enhancePhoto: () => {},
  fromMyFiles: false,
  selectedArr: [],
  removeFromSelected: () => {},
  addToSelected: () => {},
  orderPrints: () => {},
  cardTitle: null,
  imageClass: '',
  buttonsClass: '',
  verticalImageClass: '',
  verticalButtonsClass: '',
  cloud: false,
  multiSelect: false,
  orderId: 0,
};

const propTypes = {
  data: PropTypes.shape({
    id: PropTypes.number,
    imageUri: PropTypes.string,
    watermarkedImageUri: PropTypes.string,
    parentId: PropTypes.number,
    isApproved: PropTypes.bool,
    isTemplate: PropTypes.bool,
    name: PropTypes.string,
    productType: PropTypes.shape({ name: PropTypes.string }),
  }),
  className: PropTypes.string,
  contentClass: PropTypes.string,
  select: PropTypes.func,
  index: PropTypes.number,
  selected: PropTypes.number,
  withoutText: PropTypes.bool,
  iconClass: PropTypes.string,
  isSelected: PropTypes.bool,
  onImageError: PropTypes.func,
  onLoadImage: PropTypes.func,
  forSharedPage: PropTypes.bool,
  enhancePhoto: PropTypes.func,
  fromMyFiles: PropTypes.bool,
  selectedArr: PropTypes.arrayOf(PropTypes.shape({})),
  removeFromSelected: PropTypes.func,
  orderPrints: PropTypes.func,
  addToSelected: PropTypes.func,
  cardTitle: PropTypes.string,
  imageClass: PropTypes.string,
  buttonsClass: PropTypes.string,
  verticalImageClass: PropTypes.string,
  verticalButtonsClass: PropTypes.string,
  multiSelect: PropTypes.bool,
  orderId: PropTypes.number,
  cloud: PropTypes.bool,
};

function BigPhotoCard(props) {
  const {
    data,
    className,
    contentClass,
    select,
    index,
    selected,
    isSelected,
    withoutText,
    iconClass,
    onImageError,
    onLoadImage,
    forSharedPage,
    enhancePhoto,
    fromMyFiles,
    selectedArr,
    removeFromSelected,
    addToSelected,
    multiSelect,
    cardTitle,
    imageClass,
    buttonsClass,
    verticalImageClass,
    verticalButtonsClass,
    orderPrints,
    cloud,
    orderId,
  } = props;

  let imageRef = null;

  const [isVertical, setIsVertical] = useState(false);
  const [wrapRef, addWrapRef] = useState(null);

  const addRef = (e) => {
    imageRef = e;
  };

  const checkSelect = () => {
    const { id, imageUri } = data;
    if (fromMyFiles || forSharedPage || multiSelect) {
      return Boolean(selectedArr.find((item) => item.id === id && item.imageUri === imageUri));
    }
    return id === selected || isSelected;
  };

  const onClick = (e, isAddButton) => {
    const value = {
      ...data,
      index,
    };
    if (e.target === imageRef || isAddButton) {
      if (fromMyFiles || forSharedPage) {
        if (checkSelect()) {
          removeFromSelected(data);
        } else {
          addToSelected(data);
        }
      } else {
        select(value);
      }
    }
  };

  const { id, imageUri, watermarkedImageUri, parentId, isApproved, isTemplate, name } = data;

  const cardName = name || `${orderId || `${orderId}-` || ''}${id}`;

  const title = parentId && !isApproved ? `Version ${index + 1}` : cardTitle || cardName;
  const isImageSelected = checkSelect();

  return (
    <div role="button" tabIndex="0" className={b({ mix: className })} onClick={onClick}>
      <div
        className={b('content-wrap', { mix: contentClass, vertical: isVertical })}
        ref={addWrapRef}
      >
        <div
          className={b('image-wrap', {
            mix: isVertical ? verticalImageClass : imageClass,
            vertical: isVertical,
            'wrapper-cloud': cloud,
          })}
        >
          <div className={b('image-block', { 'image-cloud': cloud })}>
            <ImageComponent
              setIsVertical={setIsVertical}
              className={b('photo', { 'max-width': cloud })}
              imageUri={imageUri || watermarkedImageUri}
              addRef={addRef}
              onImageError={onImageError}
              onLoadImage={onLoadImage}
              isTemplate={isTemplate}
            />
            <div className={b('select-btn-wrap')}>
              <button className={b('select-btn')} type="button" onClick={(e) => onClick(e, true)}>
                <CheckFull
                  className={b('select-icon', { mix: iconClass })}
                  stroke={isImageSelected ? turquoiseColor : grayColor}
                  stroke2={isImageSelected ? darkBlue : grayColor}
                  fill={isImageSelected ? turquoiseColor : 'none'}
                />
              </button>
            </div>
          </div>
        </div>
      </div>
      <div
        className={b('button-block', {
          mix: isVertical ? verticalButtonsClass : buttonsClass,
          vertical: isVertical,
        })}
      >
        <div className={b('button-wrap')}>
          <ImageMenu
            className={b('button-icon')}
            iconFillColor={darkBlue}
            imageData={data}
            forSharedPage={forSharedPage}
            enhancePhoto={enhancePhoto}
            portalBlock={wrapRef}
            orderPrints={orderPrints}
            selectToCreate={onClick}
            isNoDropDown
          />
        </div>
      </div>
      {!withoutText && <span className={b('title')}>{title}</span>}
    </div>
  );
}

BigPhotoCard.propTypes = propTypes;
BigPhotoCard.defaultProps = defaultProps;

const actions = {
  uploadImages: loadImages,
  uploadImagesEnhance: loadImagesOrderEnhance,
};

const stateProps = (state) => ({
  token: getToken(state),
});

export default connect(stateProps, actions)(BigPhotoCard);
