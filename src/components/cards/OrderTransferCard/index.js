import React from 'react';
import formattedPrice from 'src/utils/formattedPrice';
import bem from 'src/utils/bem';
import ROUTES from 'src/constants/routes';
import PropTypes from 'prop-types';
import {
  EASY_SCAN_SHIP_KIT_TERMS,
  ENHANCE_TERMS,
  GIFT_CARD_TERMS,
  PHOTO_ART_TERMS,
} from 'src/terms';
import { connect } from 'react-redux';
import dynamic from 'next/dynamic';
import { orderPriceSelect, priceSelect } from 'src/redux/prices/selectors';
import { frameMaterialsSelector } from 'src/redux/photoArt/selectors';
import { deleteFromCart } from 'src/redux/orders/actions';
import { clearCoupon } from 'src/redux/coupon/actions';
import styles from './index.module.scss';
import { CONFIRM_QUESTION, EDIT, NO, PHOTO_ART, QUANTITY, YES } from './constants';

const TrashIcon2 = dynamic(() => import('src/components/svg/TrashIcon2'));

const b = bem('order-card', styles);
const { checkout } = ROUTES;

const { GIFT_CARD_TYPE } = GIFT_CARD_TERMS;

const {
  PHOTO_ART_PRODUCT,
  PHOTO_ART_COLOR,
  PHOTO_ART_SIZE,
  PHOTO_ART_COLLECTION,
  PHOTO_ART_MAT,
  PHOTO_ART_PERSONALIZE,
  DISPLAY_OPTION,
} = PHOTO_ART_TERMS;

const {
  EASY_SCAN_SHIP_KIT_TYPE,
  EASY_SCAN_SHIP_KIT_SIZE,
  EASY_SCAN_SHIP_KIT_MATERIAL,
  EASY_SCAN_SHIP_KIT,
} = EASY_SCAN_SHIP_KIT_TERMS;

const {
  RESTORATION_PRODUCT,
  COLORIZATION_PRODUCT,
  RETOUCHING_PRODUCT,
} = ENHANCE_TERMS;

const defaultProps = {
  data: [],
  price: {},
  currency: '$',
  deleteOrder: () => {  },
  selectPrice: {},
  selectedItem: {},
  clickDelete: () => {  },
  clearCouponCart: () => {  },
  frameMaterials: [],
};

const propTypes = {
  data: PropTypes.arrayOf(PropTypes.shape({})),
  price: PropTypes.shape({}),
  currency: PropTypes.string,
  deleteOrder: PropTypes.func,
  selectPrice: PropTypes.shape({}),
  selectedItem: PropTypes.shape({
    typeProduct: PropTypes.string,
    id: PropTypes.number,
  }),
  clickDelete: PropTypes.func,
  clearCouponCart: PropTypes.func,
  frameMaterials: PropTypes.arrayOf(),
};

const SUMMARY = {
  qt: 'Quantity:',
  addEnhancement: 'Add Enhancement',
  enhancement: 'Enhancement:',
  deliverables: 'Deliverables:',
  *******Safe: '*******',
  cloudDownload: 'Cloud download',
  archivalSet: 'Archival DVD Set',
  // --- Hidden for the future ---
  // *******Box: '******* Box',
};


function OrderTransferCard(props) {
  const {
    currency,
    data,
    deleteOrder,
    selectPrice,
    selectedItem,
    clickDelete,
    frameMaterials,
    clearCouponCart,
  } = props;

  const openDeletePopup = item => () => {
    clickDelete(item);
  };

  const closeDeletePopup = () => {
    clickDelete({});
  };

  const deleteOrderByItem = item => () => {
    const { isTrial } = item;
    if (isTrial) {
      clearCouponCart();
    }
    localStorage.setItem('editOrder', JSON.stringify(true));
    deleteOrder(item);
  };

  const generateOptionElement = (itemData) => {
    const { title, text, key } = itemData;
    return text && text !== 'N/A' ? (
      <div
        className={b('options', {
          'half-string': title === PHOTO_ART_PERSONALIZE || title === PHOTO_ART_MAT,
          'full-string': title === DISPLAY_OPTION,
        })}
        key={key}
      >
        <p className={b('info-title')}>{title}</p>
        <p className={b('description')}>{text}</p>
      </div>
    ) : null;
  };

  const generateItem = (itemData) => {
    const {
      typeProduct,
      enhanceInfo,
      deliverablesInfo,
      material,
      size,
      color,
      mating,
      personalize,
      id,
      easyScanShipKitInfo,
      display,
    } = itemData;

    const { name: materialName = '' } = frameMaterials.find(({ id: idMat }) => idMat === material) || {};
    switch (typeProduct) {
      case GIFT_CARD_TYPE:
        return null;

      case PHOTO_ART_PRODUCT: {
        const optionsData = [
          {
            title: PHOTO_ART_COLLECTION,
            text: materialName,
            key: `${id}_${PHOTO_ART_COLLECTION}_${material}`,
          },
          {
            title: PHOTO_ART_SIZE,
            text: size,
            key: `${id}_${PHOTO_ART_SIZE}_${size}`,
          },
          {
            title: PHOTO_ART_COLOR,
            text: color,
            key: `${id}_${PHOTO_ART_COLOR}_${color}`,
          },
          {
            title: PHOTO_ART_MAT,
            text: mating,
            key: `${id}_${PHOTO_ART_MAT}_${mating}`,
          },
          {
            title: PHOTO_ART_PERSONALIZE,
            text: personalize,
            key: `${id}_${PHOTO_ART_PERSONALIZE}_${personalize}`,
          },
          ...(display
            ? [{
              title: DISPLAY_OPTION,
              text: display,
              key: `${id}_${DISPLAY_OPTION}_${display}`,
            }]
            : []
          ),
        ];
        return optionsData.map(item => generateOptionElement(item));
      }

      case EASY_SCAN_SHIP_KIT_TYPE: {
        const optionsData = [
          {
            title: EASY_SCAN_SHIP_KIT_MATERIAL,
            text: material,
            key: `${id}_${EASY_SCAN_SHIP_KIT_MATERIAL}_${material}`,
          },
          {
            title: EASY_SCAN_SHIP_KIT_SIZE,
            text: size,
            key: `${id}_${EASY_SCAN_SHIP_KIT_SIZE}_${size}`,
          },
        ];
        return optionsData.map(item => generateOptionElement(item));
      }

      case RESTORATION_PRODUCT:
      case COLORIZATION_PRODUCT:
      case RETOUCHING_PRODUCT: {
        const optionsData = [
          {
            title: EASY_SCAN_SHIP_KIT,
            text: easyScanShipKitInfo,
            key: `${id}_${EASY_SCAN_SHIP_KIT}_${easyScanShipKitInfo}`,
          },
        ];
        return easyScanShipKitInfo
          ? optionsData.map(item => generateOptionElement(item))
        : null;
      }

      default: {
        const regexMaterial = /\$0/;
        const newDeliverablesInfo = deliverablesInfo.replace(regexMaterial, 'Free');
        const optionsData = [
          {
            title: SUMMARY.enhancement,
            text: enhanceInfo,
            key: `${id}_${SUMMARY.enhancement}_${enhanceInfo}`,
          },
          {
            title: SUMMARY.deliverables,
            text: newDeliverablesInfo,
            key: `${id}_${SUMMARY.deliverables}_${newDeliverablesInfo}`,
          },
        ];
        return optionsData.map(item => generateOptionElement(item));
      }
    }
  };

  const getInfoAboutOptions = (item) => {
    const { options = [] } = item || {};

    return options.map((option) => {
      const { optionId, quantity, name = '' } = option || {};
      const { [item.typeProduct]: prices = [] } = selectPrice || {};
      const { options: getAllPrice = [] } = prices || {};
      const { displayName = '', price = 0 } = getAllPrice.find(({ id }) => id === optionId) || {};

      return (
        name === 'enhance_prints' ? (
          <p className={b('description')} key={optionId}>
            {`${displayName} x${quantity} = ${price * quantity}`}
          </p>
        ) : null
      );
    });
  };

  return (
    <article className={b()}>
      {
        data.map((item) => {
          const {
            checkoutOrderInfo,
            qtInfo,
            orderTitle,
            enhanceInfo,
            deliverablesInfo,
            typeProduct,
            color,
            material,
            size,
            mating,
            isMatting,
            personalize,
            customText,
            id,
            easyScanShipKitInfo,
            display,
          } = formattedPrice(item, currency, selectPrice[item.typeProduct], [], [...frameMaterials]);

          const itemData = {
            enhanceInfo,
            deliverablesInfo,
            typeProduct,
            color,
            material,
            size,
            mating,
            isMatting,
            personalize,
            customText,
            id,
            easyScanShipKitInfo,
            display,
          };
          const {
            quantity: {
              title = '',
              quantity = null,
              sum = 0,
            } = {},
          } = checkoutOrderInfo || {};

          const ship = `${title} x${quantity} = ${sum}`;

          return (
            <div className={b('order')} key={item.id}>
              <div className={b('order-info')}>
                <div className={b('order-title')}>{orderTitle}</div>
                <div className={b('quantity')}>
                  <p className={b('info-title')}>{QUANTITY}</p>
                  <p className={b('description')}>{qtInfo || ship }</p>
                  {getInfoAboutOptions(item)}
                </div>
                <div className={b(orderTitle === PHOTO_ART ? 'photo-art-options' : '')}>
                  {generateItem(itemData)}
                </div>
              </div>
              <div className={b('buttons-wrapper')}>
                <div className={b('buttons')}>
                  <a className={b('btn-edit')} href={checkout}>{EDIT}</a>
                  <div
                    className={b('btn-delete')}
                    role="button"
                    tabIndex="0"
                    onClick={openDeletePopup(item)}
                  >
                    <TrashIcon2 className={b('delete-icon')} />
                  </div>
                </div>
                {
                  (selectedItem.id === item.id
                    && selectedItem.typeProduct === item.typeProduct
                  ) && (
                    <div className={b('delete-popup')} name={`${item.typeProduct} ${item.id}`}>
                      <span className={b('confirm-question')}>{CONFIRM_QUESTION}</span>
                      <div className={b('confirmation-wrapper')}>
                        <button
                          className={b('delete-confirm')}
                          type="button"
                          onClick={deleteOrderByItem(item)}
                        >
                          {YES}
                        </button>
                        <button
                          className={b('delete-cancel')}
                          type="button"
                          onClick={closeDeletePopup}
                        >
                          {NO}
                        </button>
                      </div>
                    </div>
                  )
                }
              </div>
            </div>
          );
        })
      }
    </article>
  );
}

OrderTransferCard.propTypes = propTypes;
OrderTransferCard.defaultProps = defaultProps;

const stateProps = state => ({
  price: priceSelect(state),
  selectPrice: orderPriceSelect(state),
  frameMaterials: frameMaterialsSelector(state),
});

const actions = {
  deleteOrder: deleteFromCart,
  clearCouponCart: clearCoupon,
};

export default connect(stateProps, actions)(OrderTransferCard);
