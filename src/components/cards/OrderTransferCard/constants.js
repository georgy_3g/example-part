const QUANTITY = 'Quantity:';
const EDIT = 'Edit';
const CONFIRM_QUESTION = 'Are you sure?';
const YES = 'Yes';
const NO = 'No';
const PHOTO_ART = 'Photo art';

export { QUANTITY, EDIT, CONFIRM_QUESTION, YES, NO, PHOTO_ART };
