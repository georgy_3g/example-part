import React from 'react';
import bem from 'src/utils/bem';
import convertToPrice from 'src/utils/convertToPrice';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Image from 'next/image';
import dynamic from 'next/dynamic';
import { wizardHintSelector } from 'src/redux/wizardsCards/selectors';
import { getWizardHintsAction } from 'src/redux/wizardsCards/actions';
import styles from './index.module.scss';

const HintPopup = dynamic(() => import('src/components/popups/HintPopUp'));
const ColorButton = dynamic(() => import('src/components/buttons/ColorButton'));

const b = bem('product-card', styles);

const defaultProps = {
  className: '',
  title: '',
  text: '',
  id: null,
  select: () => {},
  img: '',
  withoutSelect: false,
  name: '',
  hint: PropTypes.shape({
    title: '',
    description: '',
    hintImages: [],
  }),
  isHintOpen: false,
  toggleHint: () => {},
  selectedHintId: null,
  selectButtonText: '',
  imageClassName: '',
  textClassName: '',
  priceText: '',
  basePrice: '',
  customPrice: '',
  backGroundColor: '',
  windowWidth: 1920,
  easyScanShipKit: {},
  firstOrigin: false,
};

const propTypes = {
  className: PropTypes.string,
  title: PropTypes.string,
  text: PropTypes.string,
  id: PropTypes.number,
  select: PropTypes.func,
  img: PropTypes.string,
  withoutSelect: PropTypes.bool,
  name: PropTypes.string,
  hint: PropTypes.shape({
    title: PropTypes.string,
    description: PropTypes.string,
    hintImages: PropTypes.arrayOf(PropTypes.shape({})),
  }),
  isHintOpen: PropTypes.bool,
  toggleHint: PropTypes.func,
  selectedHintId: PropTypes.number,
  selectButtonText: PropTypes.string,
  imageClassName: PropTypes.string,
  textClassName: PropTypes.string,
  priceText: PropTypes.string,
  basePrice: PropTypes.string,
  customPrice: PropTypes.string,
  backGroundColor: PropTypes.string,
  windowWidth: PropTypes.number,
  easyScanShipKit: PropTypes.shape({
    price: PropTypes.number,
    additionalDiscount: PropTypes.number,
  }),
  firstOrigin: PropTypes.bool,
};

function ProductCard(props) {
  const {
    className,
    title,
    text,
    img,
    id,
    select,
    withoutSelect,
    name,
    hint,
    isHintOpen,
    toggleHint,
    selectedHintId,
    selectButtonText,
    imageClassName,
    textClassName,
    priceText,
    basePrice,
    customPrice,
    backGroundColor,
    windowWidth,
    easyScanShipKit,
    firstOrigin,
  } = props;

  const { title: hintTitle, description, hintImages } = hint;

  const isHintSelected = selectedHintId === id;

  const onClick = () => {
    select(name || id);
  };

  const getPriceText = () => {
    const { price, additionalDiscount } = easyScanShipKit;
    if (name === 'shipKit') {
      return (
        <div className={b('price-text-wrapper', { small: windowWidth < 581 })}>
          <span className={b('price-text', { small: windowWidth < 581 })}>per photo</span>
          <div className={b('custom-price-text-wrapper', { small: windowWidth < 581 })}>
            <span className={b('price-text', { small: windowWidth < 581, plus: windowWidth > 580 })}>+</span>
            <div className={b('custom-price-wrapper')}>
              <span className={b('price-text', {
                origin: additionalDiscount,
                base: !additionalDiscount,
                small: windowWidth < 581,
                })}
              >
                {convertToPrice(price)}
              </span>
              {additionalDiscount ? (
                <span className={b('price-text', { custom: true })}>
                  {convertToPrice(price - additionalDiscount)}
                </span>
              ) : null}
            </div>
            <span className={b('price-text', { small: windowWidth < 581 })}>
              for the kit
            </span>
          </div>
        </div>
      )
    }
    if (priceText) {
      return (
        <span className={b('price-text')}>{priceText}</span>
      )
    }
    return null;
  };

  return (
    <>
      <div className={b({ mix: className })}>
        {windowWidth > 580 ? (
          <>
            <div className={b('image-block', { mix: imageClassName })}>
              <div
                className={b('slide-wrap')}
                key={img}
                role="button"
                onClick={onClick}
                tabIndex="0"
              >
                <Image
                  className={b('slide', { mix: imageClassName })}
                  src={img}
                  alt={img}
                  layout="responsive"
                  width={540}
                  height={576}
                />
              </div>
            </div>
            <div className={b('text-block', { mix: textClassName })}>
              <div role="button" onClick={onClick} tabIndex="0">
                <h3 className={b('title')}>{title}</h3>
              </div>
              <span className={b('text')}>{text}</span>
              {(customPrice || basePrice) && (
                <div className={b('price-wrapper')}>
                  {basePrice && (
                    <span className={b('base-price-text', { origin: true, firstOrigin })} style={{ color: backGroundColor }}>
                      {basePrice}
                    </span>
                  )}
                  {customPrice !== basePrice && (
                    <span className={b('base-price-text')} style={{ color: backGroundColor }}>
                      {customPrice}
                    </span>
                  )}
                </div>
              )}
              {getPriceText()}
              {!withoutSelect && (
                <ColorButton
                  className={b('color-button')}
                  onClick={onClick}
                  backGroundColor={backGroundColor}
                  text={selectButtonText}
                />
              )}
            </div>
          </>
        ) : (
          <>
            <div className={b('title')} role="button" onClick={onClick} tabIndex="0">
              {title}
            </div>
            <div className={b('row')}>
              <div
                className={b('image-block', { mix: imageClassName, shipKit: name === 'shipKit' })}
              >
                <div
                  className={b('slide-wrap')}
                  key={img}
                  role="button"
                  onClick={onClick}
                  tabIndex="0"
                >
                  <img className={b('slide', { mix: imageClassName })} src={img} alt={img} />
                </div>
              </div>
              <div className={b('col')}>
                <span className={b('text')}>{text}</span>
                {(customPrice || basePrice) && (
                  <div className={b('price-wrapper')}>
                    {basePrice && (
                      <span className={b('base-price-text', { origin: true })} style={{ color: backGroundColor }}>
                        {basePrice}
                      </span>
                    )}
                    {customPrice && (
                      <span className={b('base-price-text')} style={{ color: backGroundColor }}>
                        {customPrice}
                      </span>
                    )}
                  </div>
                )}
                {getPriceText()}
                {!withoutSelect && (
                  <ColorButton
                    className={b('color-button')}
                    onClick={onClick}
                    backGroundColor={backGroundColor}
                    text={selectButtonText}
                  />
                )}
              </div>
            </div>
          </>
        )}
      </div>
      {isHintOpen && isHintSelected ? (
        <HintPopup
          hintImages={hintImages}
          title={hintTitle}
          description={description}
          toggleHint={toggleHint}
        />
      ) : null}
    </>
  );
}

ProductCard.propTypes = propTypes;
ProductCard.defaultProps = defaultProps;

const stateProps = (state) => ({
  hint: wizardHintSelector(state),
});

const actions = {
  getWizardHints: getWizardHintsAction,
};

export default connect(stateProps, actions)(ProductCard);
