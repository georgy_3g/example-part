const SELECTED_TEXT = 'Selected';
const CREATE_TEXT = 'Create';
const FROM_TEXT = 'from ';
const DOLLAR = '$';

export { SELECTED_TEXT, CREATE_TEXT, FROM_TEXT, DOLLAR };
