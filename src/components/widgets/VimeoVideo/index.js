import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Vimeo from '@u-wave/react-vimeo';

const style = {
  width: '100%',
  height: '100%',
};

const propTypes = {
  contentIframe: PropTypes.string,
  clickPause: PropTypes.func,
};
const defaultProps = {
  contentIframe: '',
  clickPause: () => {},
};

class VimeoVideo extends Component {
  constructor(props) {
    super(props);
    const { contentIframe } = this.props;
    const regexFindId = /\/\w+[?|"]/g;
    const [id] = contentIframe.match(regexFindId);
    const idNoSymbols = id.slice(1, id.length - 1);
    this.id = idNoSymbols;
  }

  componentWillUnmount() {
    this.id = null;
  }

  render() {
    const { clickPause } = this.props;

    return <Vimeo video={this.id} onPlay={clickPause} style={style} />;
  }
}

VimeoVideo.propTypes = propTypes;
VimeoVideo.defaultProps = defaultProps;

export default VimeoVideo;
