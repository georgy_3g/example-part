import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import ReactDOM from 'react-dom';
import bem from 'src/utils/bem';
import ROUTES from 'src/constants/routes';
import Image from 'next/image';
import note from 'public/img/account/note.png';
import dynamic from 'next/dynamic';
import {
  approvedOrderImagesSelector,
  isDownloadingSelector,
  orderAudiosSelect,
  orderDetailsSelect,
  orderVideosSelect,
} from 'src/redux/ordersList/selectors';
import {
  cloudStatus,
  isSixtyDaysHavePassedSelector,
  myCloudAudio,
  myCloudFolder,
  myCloudImages,
  myCloudVideo,
} from 'src/redux/myCloud/selectors';
import { getFileSelector, getRootFilesSelector } from 'src/redux/folder/selectors';
import { getToken } from 'src/redux/auth/selectors';
import {
  frameMaterialsSelector,
  photoArtProductsSelector,
} from 'src/redux/photoArt/selectors';
import {
  cleanFolder,
  clearRootFolder,
  getArchiveActions,
  getFolderById,
  getFolderRoot,
  sortFolderAction,
} from 'src/redux/folder/actions';
import { addImageListAction, setApprovedImageUrl } from 'src/redux/approvedOrderImage/actions';
import { addToCart } from 'src/redux/orders/actions';
import {
  addAllFilesToCloud,
  addFileToCloud,
  changePlayerFlagAction,
  deleteAllFilesFromCloud,
  deleteFileFromCloud,
} from 'src/redux/myCloud/actions';
import styles from './index.module.scss';
import {
  ALL_FILES,
  ALL_MEDIA,
  APPLICATION,
  AUDIO,
  CLOUD_AUDIO,
  CLOUD_IMAGES,
  CLOUD_VIDEO,
  DIGITIZE_FILE_TYPES,
  ERROR_TEXT,
  FILTER_BUTONS,
  FOLDER,
  HINT_LIST,
  HINT_POPUP_BUTTON_TEXT,
  HINT_POPUP_TITLE,
  IMAGE,
  MAX_IMAGE_COUNT,
  ORDER_AUDIOS,
  ORDER_IMAGES,
  ORDER_SCAN,
  ORDER_VIDEOS,
  RENEW_POPUP_TEXT,
  VIDEO,
  VIEW_TYPES,
} from './constants';

const CloudImageCard = dynamic(() => import('src/components/cards/CloudImageCard'));
const VideoCard = dynamic(() => import('src/components/cards/VideoCard'));
const AudioCard = dynamic(() => import('src/components/cards/AudioCard'));
const ImageSlider = dynamic(() => import('src/components/widgets/ImageSlider'));
const MyFilesHeader = dynamic(() => import('src/components/widgets/MyFilesHeader'));
const SharePopup = dynamic(() => import('src/components/popups/SharePopup'));
const DownloadProgressPopup = dynamic(() => import('src/components/popups/DownloadProgressPopup'));
const SlideshowBlock = dynamic(() => import('src/components/widgets/SlideshowBlock'));
const MemoyaCloudRenewPopup = dynamic(() => import('src/components/popups/MemoyaCloudRenewPopup'));
const CloudHintPopup = dynamic(() => import('src/components/popups/CloudHintPopUp'));
const FolderCard = dynamic(() => import('src/components/cards/FolderCard'));
const CloseIcon2 = dynamic(() => import('src/components/svg/CloseIcon2'));

const b = bem('my-cloud-files', styles);
const { photoArtEditor } = ROUTES;

const defaultProps = {
  className: '',
  orderImages: [],
  orderVideos: [],
  orderAudios: [],
  orderDetails: {},
  downloadArchive: () => {},
  token: '',
  addOrder: () => {},
  query: {},
  setNewUrl: () => {},
  changeScene: () => {},
  addCloudFile: () => {},
  deleteCloudFile: () => {},
  cloudImages: [],
  cloudAudio: [],
  cloudVideo: [],
  orderFolder: [],
  rootFolder: [],
  foldersParent: [],
  addAllFiles: () => {},
  deleteAllFiles: () => {},
  isComponentShow: true,
  isDownloading: false,
  changePlayerFlag: () => {},
  clearAllRootFolder: () => {},
  isSixtyDaysHavePassed: null,
  cloudCount: 0,
  sortFolder: () => {},
  clearFolders: () => {},
  getAllFolders: () => {},
  openFolder: () => {},
  addImageList: () => {},
  photoArtProducts: {},
  frameMaterials: [],
};

const propTypes = {
  className: PropTypes.string,
  orderImages: PropTypes.arrayOf(PropTypes.shape({})),
  orderVideos: PropTypes.arrayOf(PropTypes.shape({})),
  orderAudios: PropTypes.arrayOf(PropTypes.shape({})),
  orderDetails: PropTypes.shape({ 
    id: PropTypes.number,
    orderPayments: PropTypes.arrayOf(PropTypes.shape({})),
    price: PropTypes.number,
  }),
  downloadArchive: PropTypes.func,
  token: PropTypes.string,
  addOrder: PropTypes.func,
  query: PropTypes.shape({
    step: PropTypes.string,
    image: PropTypes.string,
  }),
  setNewUrl: PropTypes.func,
  changeScene: PropTypes.func,
  addCloudFile: PropTypes.func,
  deleteCloudFile: PropTypes.func,
  cloudImages: PropTypes.arrayOf(PropTypes.shape({})),
  cloudAudio: PropTypes.arrayOf(PropTypes.shape({})),
  cloudVideo: PropTypes.arrayOf(PropTypes.shape({})),
  orderFolder: PropTypes.arrayOf({}),
  rootFolder: PropTypes.arrayOf({}),
  foldersParent: PropTypes.arrayOf({}),
  addAllFiles: PropTypes.func,
  deleteAllFiles: PropTypes.func,
  isComponentShow: PropTypes.bool,
  isDownloading: PropTypes.bool,
  changePlayerFlag: PropTypes.func,
  isSixtyDaysHavePassed: PropTypes.string,
  clearAllRootFolder: PropTypes.func,
  cloudCount: PropTypes.number,
  sortFolder: PropTypes.func,
  clearFolders: PropTypes.func,
  getAllFolders: PropTypes.func,
  openFolder: PropTypes.func,
  addImageList: PropTypes.func,
  photoArtProducts: PropTypes.shape({}),
  frameMaterials: PropTypes.arrayOf(PropTypes.shape({})),
};

class MyCloudFiles extends Component {
  constructor(props) {
    super(props);
    const { orderDetails } = props;
    this.veil = React.createRef();
    this.state = {
      filterType: FILTER_BUTONS[0].type,
      viewType: VIEW_TYPES.tile,
      fileData: null,
      orderId: orderDetails && orderDetails.id ? orderDetails.id : null,
      showSharedPopup: false,
      isShowPhotoSlider: false,
      isShowDownloadPopup: false,
      isLoaded: false,
      isShowSlideshow: false,
      sliderIndex: 0,
      isShowSubscribePopup: false,
      isShowHintPopup: false,
      idOpenFolder: {},
    };
  }

  componentDidMount() {
    const { orderId } = this.state;
    if (orderId) {
      this.allFoldersRoot(orderId);
    }
  }

  componentDidUpdate(prevProps, prevState) {
    const { isDownloading: oldFlag } = prevProps;
    const { orderId: prevOrderId } = prevState;
    const { orderId, isShowDownloadPopup } = this.state;
    const { isDownloading } = this.props;

    if (oldFlag && !isDownloading) {
      this.updateLoadStatus(true);
    }

    if (isDownloading && !isShowDownloadPopup) {
      this.setDownloadPopup(isDownloading);
    }

    if (!prevOrderId && orderId) {
      this.allFoldersRoot(orderId);
    }

    if (prevOrderId && orderId && prevOrderId !== orderId) {
      this.setOpenFolder();
      this.clearAllFolder();
      this.allFoldersRoot(orderId);
    }
  }

  componentWillUnmount() {
    this.clearRootFolders();
    this.clearAllFolder();
    this.removeAll();
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    const { orderDetails } = nextProps;
    const { orderId } = prevState;
    const newOrderId = orderDetails && orderDetails.id ? orderDetails.id : null;
    if (orderId !== newOrderId) {
      return {
        orderId: newOrderId,
        fileData: null,
        selected: [],
        sliderIndex: 0,
      };
    }
    return null;
  }

  allFoldersRoot = (orderId) => {
    const { token, getAllFolders } = this.props;
    getAllFolders({ orderId, token });
  };

  clearAllFolder = () => {
    const { clearFolders } = this.props;
    clearFolders();
  };

  clearRootFolders = () => {
    const { clearAllRootFolder } = this.props;
    clearAllRootFolder();
  };

  getFolderById = (folderId, folderName) => {
    const { openFolder, token } = this.props;
    openFolder({ folderId, token });
    this.setOpenFolder(folderId, folderName);
  };

  setDownloadPopup = (value) => {
    this.setState({ isShowDownloadPopup: value });
  };

  closeDownloadPopup = () => {
    this.setState({ isShowDownloadPopup: false, isLoaded: false });
  };

  updateLoadStatus = (value) => {
    this.setState({ isLoaded: value });
  };

  showPopup = (data) => {
    this.setState({ fileData: data });
  };

  changeFilterType = (filterType) => {
    const { viewType, orderId } = this.state;
    const { cloudCount } = this.props;
    const isLockDigitizeFiles = cloudCount <= 0 && DIGITIZE_FILE_TYPES.includes(filterType);
    if (filterType === ALL_MEDIA) {
      this.setState({ idOpenFolder: {} });
      this.allFoldersRoot(orderId);
    } else {
      this.getFileByType(filterType);
    }
    this.setState({
      filterType,
      viewType: isLockDigitizeFiles ? VIEW_TYPES.tile : viewType,
    });
  };

  getFileByType = (filterType) => {
    const { sortFolder, token } = this.props;
    const { orderId } = this.state;
    const filters = [
      { origin: 'fileType', operator: '=', target: this.switchTypeFile(filterType) },
      { origin: 'orderId', operator: '=', target: orderId },
    ];
    sortFolder({
      token,
      filters:
        filterType === ORDER_IMAGES
          ? [...filters, { origin: 'systemType', operator: '=', target: 'enhance' }]
          : filters,
    });
  };

  switchTypeFile = (type) => {
    switch (type) {
      case ORDER_SCAN:
      case ORDER_IMAGES:
        return IMAGE;
      case ORDER_VIDEOS:
        return APPLICATION;
      case ORDER_AUDIOS:
        return AUDIO;
      default:
        return '';
    }
  };

  changeViewType = (viewType) => {
    this.setState({ viewType, sliderIndex: 0 });
  };

  addToSelected = (data) => {
    const { addCloudFile } = this.props;
    const { imageUri, videoUri, audioUri, id, isFolder } = data;
    if (imageUri) {
      addCloudFile({
        fileType: CLOUD_IMAGES,
        file: data,
      });
    }
    if (videoUri) {
      addCloudFile({
        fileType: CLOUD_VIDEO,
        file: data,
      });
    }
    if (audioUri) {
      addCloudFile({
        fileType: CLOUD_AUDIO,
        file: data,
      });
    }
    if (id && isFolder) {
      addCloudFile({
        fileType: FOLDER,
        file: { id },
      });
    }
  };

  removeFromSelected = (data) => {
    const { deleteCloudFile } = this.props;
    const { imageUri, videoUri, audioUri, id, isFolder } = data;
    if (imageUri) {
      deleteCloudFile({
        fileType: CLOUD_IMAGES,
        fileId: id,
      });
    }
    if (videoUri) {
      deleteCloudFile({
        fileType: CLOUD_VIDEO,
        fileId: id,
      });
    }
    if (audioUri) {
      deleteCloudFile({
        fileType: CLOUD_AUDIO,
        fileId: id,
      });
    }
    if (id && isFolder) {
      deleteCloudFile({
        fileType: FOLDER,
        fileId: id,
      });
    }
  };

  selectAll = () => {
    const { addAllFiles, rootFolder, foldersParent } = this.props;
    const { idOpenFolder, filterType } = this.state;
    const typeFile =
      Object.keys(idOpenFolder).length || filterType !== ALL_MEDIA ? foldersParent : rootFolder;
    const data = this.completionData(typeFile);
    const { cloudImages = [], cloudAudio = [], cloudVideo = [], cloudFolder = [] } = data || {};
    addAllFiles({
      cloudImages,
      cloudAudio,
      cloudVideo,
      folder: cloudFolder,
    });
  };

  removeAll = () => {
    const { deleteAllFiles } = this.props;
    deleteAllFiles();
  };

  completionData = (data) => {
    const { filterType } = this.state;
    const { files = [] } = data || {};
    return files.reduce(
      (
        acc,
        {
          id,
          countFiles,
          identifier,
          originType,
          systemType,
          fileType,
          preview,
          url,
          name,
          key,
          orderImageId,
        },
      ) => {
        if (originType === 'folder') {
          return {
            ...acc,
            cloudFolder: [
              ...(acc.cloudFolder || []),
              {
                id,
                preview,
                name,
                countFiles,
              },
            ],
          };
        }
        if (originType === IMAGE || fileType === IMAGE || systemType === 'enhance') {
          if (
            filterType === ORDER_IMAGES &&
            (!identifier || identifier.indexOf('ORIGINAL') !== -1)
          ) {
            return acc;
          }
          return {
            ...acc,
            cloudImages: [
              ...(acc.cloudImages || []),
              {
                id,
                imageUri: url,
                name,
                imageKey: key,
                orderImageId,
              },
            ],
          };
        }
        if (originType === APPLICATION || fileType === APPLICATION || fileType === VIDEO) {
          return {
            ...acc,
            cloudVideo: [...(acc.cloudVideo || []), { id, videoUri: url, name }],
          };
        }
        if (originType === AUDIO || fileType === AUDIO) {
          return {
            ...acc,
            cloudAudio: [...(acc.cloudAudio || []), { id, audioUri: url, name }],
          };
        }
        return acc;
      },
      {},
    );
  };

  downloadCloudImages = () => {
    const { cloudImages, cloudVideo, cloudAudio, orderFolder } = this.props;
    const allSelected = [...cloudImages, ...cloudVideo, ...cloudAudio, ...orderFolder];
    if (allSelected.length === 1 && !orderFolder.length) {
      const { imageUri, videoUri, audioUri } = allSelected[0];
      const link = document.createElement('a');
      link.href = imageUri || videoUri || audioUri;
      link.setAttribute('download', true);
      document.body.appendChild(link);
      link.click();
    }
    if (allSelected.length > 1 || orderFolder.length) {
      const { orderDetails, downloadArchive, token } = this.props;
      const { id: orderId } = orderDetails;
      const ids = allSelected.map((item) => item.id);
      const objData = { ids, orderId, token };

      this.setState({ isShowDownloadPopup: true, isLoaded: false }, () => {
        downloadArchive(objData);
      });
    }
  };

  orderPrints = (selectedPhotos) => {
    const { addImageList, photoArtProducts } = this.props;

    const { frames, id: collectionId } =
      Object.values(photoArtProducts).find(({ name }) => name.toLowerCase() === 'classic') ||
      Object.values(photoArtProducts)[0];

    const { id: frameId } =
      frames.find(({ isDefault }) => isDefault) || frames[0];

    const date = new Date();

    const newImages = selectedPhotos.map((item) => ({
      ...item,
      addedAt: date.toISOString(),
    }));

    addImageList(newImages);
    window.location.href = `${photoArtEditor}?collection=${collectionId}&frame=${frameId}&tab=size`;
  };

  getSelected = (type) => {
    const { cloudImages, cloudVideo, cloudAudio, orderFolder } = this.props;

    switch (type) {
      case ORDER_IMAGES: {
        return cloudImages;
      }

      case ORDER_VIDEOS: {
        return cloudVideo;
      }

      case ORDER_AUDIOS: {
        return cloudAudio;
      }

      case ALL_FILES:
      case ALL_MEDIA: {
        return [...cloudImages, ...cloudVideo, ...cloudAudio, ...orderFolder];
      }

      case ORDER_SCAN: {
        return cloudImages.filter((item) => !item.isApproved);
      }

      default: {
        return [];
      }
    }
  };

  openSharedPopup = () => {
    this.setState({ showSharedPopup: true });
  };

  closeSharedPopup = () => {
    this.setState({ showSharedPopup: false });
  };

  checkShowPopup = (name) => {
    const { viewType, fileData } = this.state;
    return viewType === VIEW_TYPES.tile && fileData && fileData[name];
  };

  closeModal = (e, isImage) => {
    if (e.target === this.veil.current || isImage) {
      this.showPopup(null);
    }
  };

  enhancePhoto = (typeProduct, productId, photo) => {
    const { addOrder, cloudImages } = this.props;

    const order = {
      quantity: photo ? 1 : cloudImages.length,
      typeProduct,
      productId,
      isGift: false,
      images: photo ? [photo] : cloudImages,
      options: [],
      notes: '',
    };
    addOrder(order);
    window.location.href = ROUTES.checkout;
  };

  openSlideshow = () => {
    const { changePlayerFlag } = this.props;
    changePlayerFlag(true);
    this.setState({ isShowSlideshow: true });
  };

  closeSlideshow = () => {
    this.setState({ isShowSlideshow: false });
  };

  getFilesToSlideshow = (files) => {
    const { cloudImages, cloudVideo, cloudAudio } = this.props;

    const { sliderIndex, viewType } = this.state;

    const { id, imageUri, videoUri, audioUri } = files && files.length ? files[sliderIndex] : {};

    const selectedFiles = [
      ...(cloudImages && cloudImages.length ? cloudImages : []),
      ...(cloudVideo && cloudVideo.length ? cloudVideo : []),
      ...(cloudAudio && cloudAudio.length ? cloudAudio : []),
    ];

    const filesToSlideshow = selectedFiles.length ? selectedFiles : files;

    const index = filesToSlideshow.findIndex(
      (item) =>
        item.id === id &&
        (item.imageUri === imageUri || item.videoUri === videoUri || item.audioUri === audioUri),
    );

    const startIndex = index >= 0 && viewType === VIEW_TYPES.slider ? index : 0;

    return {
      filesToSlideshow,
      startIndex,
    };
  };

  afterChangeSlider = (index) => {
    this.setState({ sliderIndex: index });
  };

  mediaRef = (e) => {
    if (e) {
      e.play();
    }
  };

  openSubscribePopup = () => {
    this.setState({ isShowSubscribePopup: true });
  };

  closeSubscribePopup = () => {
    this.setState({ isShowSubscribePopup: false });
  };

  closeHintPopup = () => {
    this.setState({ isShowHintPopup: false });
  };

  openHint = () => {
    this.setState({ isShowHintPopup: true });
  };

  onRenew = () => {
    this.setState({
      isShowHintPopup: false,
      isShowSubscribePopup: true,
    });
  };

  setOpenFolder = (folderId, nameFolder) => {
    const { clearFolders } = this.props;
    const setFolder = folderId ? { [folderId]: nameFolder } : {};
    this.setState({ idOpenFolder: setFolder, sliderIndex: 0 });
    clearFolders();
  };

  clickOrderPrints = (selectedPhotos) => {
    const { addImageList, photoArtProducts, frameMaterials } = this.props;

    const { id: idCollection } =
      frameMaterials.find(({ isDefault }) => isDefault) || Object.values(frameMaterials)[0];

    const { id: frameId } =
      photoArtProducts[idCollection].frames.find(({ isDefault }) => isDefault) ||
      photoArtProducts[idCollection].frames[0];

    const date = new Date();

    const images = selectedPhotos.map((item) => ({
      ...item,
      addedAt: date.toISOString(),
    }));
    addImageList(images);
    window.location.href = `${photoArtEditor}?collection=${idCollection}&frame=${frameId}&tab=size`;
  };

  render() {
    const {
      className,
      orderImages,
      orderVideos,
      orderAudios,
      orderFolder,
      cloudImages,
      cloudAudio,
      cloudVideo,
      query,
      setNewUrl,
      changeScene,
      isComponentShow,
      orderDetails,
      cloudCount,
      isSixtyDaysHavePassed,
      foldersParent,
      rootFolder,
    } = this.props;

    const { id: orderId, orderPayments, price } = orderDetails;

    const {
      filterType,
      viewType,
      fileData,
      showSharedPopup,
      isShowPhotoSlider,
      isShowDownloadPopup,
      isLoaded,
      isShowSlideshow,
      isShowSubscribePopup,
      isShowHintPopup,
      idOpenFolder,
    } = this.state;

    const isLockDigitizeFiles = cloudCount <= 0 && DIGITIZE_FILE_TYPES.includes(filterType);
    const isLockDigitizeTabs = isSixtyDaysHavePassed && DIGITIZE_FILE_TYPES.includes(filterType);

    const folderData =
      Object.keys(idOpenFolder).length || filterType !== ALL_MEDIA ? foldersParent : rootFolder;

    const data = this.completionData(folderData);

    const {
      cloudImages: images = [],
      cloudAudio: audio = [],
      cloudVideo: video = [],
      cloudFolder: folder = [],
    } = data || {};

    const allFiles = {
      orderScan: [...images],
      orderImages: [...images],
      orderVideos: [...video],
      orderAudios: [...audio],
      allFiles: [...orderImages, ...orderVideos, ...orderAudios],
      allMedia: [...images, ...audio, ...video, ...folder],
    };

    const files = allFiles[filterType];
    const filesForSlider = [...images, ...audio, ...video];

    const selected = this.getSelected(filterType);

    const selectedCount =
      cloudImages.length + cloudVideo.length + cloudAudio.length + orderFolder.length;

    const selectedData = {
      images: cloudImages,
      videos: cloudVideo,
      audios: cloudAudio,
      folder: orderFolder,
    };

    const isButtonDisabled =
      !cloudImages.length && !cloudVideo.length && !cloudAudio.length && !orderFolder.length;

    const isButtonOrderPrintDisabled = !(
      cloudImages.length &&
      (filterType === ORDER_IMAGES ||
        filterType === ALL_FILES ||
        filterType === ORDER_SCAN ||
        filterType === ALL_MEDIA)
    );

    const { filesToSlideshow, startIndex } = this.getFilesToSlideshow(filesForSlider);

    const paymentPrice = orderPayments.reduce((acc, item )=> {
      return acc + item.amount || 0;
    }, 0);

    return (
      <section className={b({ mix: className, isComponentShow })}>
        <MyFilesHeader
          filterType={filterType}
          viewType={viewType}
          changeFilter={this.changeFilterType}
          changeView={this.changeViewType}
          downloadCloudImages={this.downloadCloudImages}
          selectAll={this.selectAll}
          removeAll={this.removeAll}
          orderPrints={this.orderPrints}
          numberOfSelected={selectedCount}
          clickShared={this.openSharedPopup}
          isButtonDisabled={isButtonDisabled}
          isButtonOrderPrintDisabled={isButtonOrderPrintDisabled}
          isSlideshowDisabled={!filesToSlideshow || !filesToSlideshow.length}
          enhancePhoto={this.enhancePhoto}
          query={query}
          setNewUrl={setNewUrl}
          changeScene={changeScene}
          openSlideshow={this.openSlideshow}
          openSubscribePopup={this.openSubscribePopup}
          isLockDigitizeFiles={isLockDigitizeFiles}
          isLockDigitizeTabs={isLockDigitizeTabs}
          openHint={this.openHint}
          isOpenFolder={idOpenFolder}
          goBack={this.setOpenFolder}
          disableSlider={!filesForSlider.length}
          clickOrderPrints={this.clickOrderPrints}
          cloudImages={cloudImages}
        />
        {isShowPhotoSlider &&
          Boolean(cloudImages && cloudImages.length) &&
          cloudImages.length > MAX_IMAGE_COUNT && (
            <div className={b('error-block')}>
              <span className={b('error-icon')}>!</span>
              <span className={b('error-text')}>{ERROR_TEXT}</span>
            </div>
          )}
        {(viewType === VIEW_TYPES.tile && price <= paymentPrice) && (
          <div className={b('image-block')}>
            {files.map((item) => ( item.name !== 'Enhance' && (
              <>
                {filterType === ALL_MEDIA && 'preview' in item && (
                  <FolderCard
                    className={b('all-media')}
                    data={item}
                    showPopup={this.showPopup}
                    selected={selected}
                    addToSelected={this.addToSelected}
                    removeFromSelected={this.removeFromSelected}
                    getFolderById={this.getFolderById}
                    orderId={orderId}
                  />
                )}
                {(filterType === ORDER_IMAGES ||
                  filterType === ALL_FILES ||
                  filterType === ORDER_SCAN ||
                  filterType === ALL_MEDIA) &&
                  item.imageUri && (
                    <CloudImageCard
                      className={b('image')}
                      data={item}
                      showPopup={this.showPopup}
                      selectedArr={selected}
                      addToSelected={this.addToSelected}
                      removeFromSelected={this.removeFromSelected}
                      fromMyFiles
                      orderPrints={this.orderPrints}
                      isLockDigitizeFiles={isLockDigitizeFiles}
                      enhancePhoto={this.enhancePhoto}
                      clickOrderPrints={this.clickOrderPrints}
                    />
                  )}
                {(filterType === ORDER_VIDEOS ||
                  filterType === ALL_FILES ||
                  filterType === ALL_MEDIA) &&
                  item.videoUri && (
                    <VideoCard
                      className={b('image')}
                      contentClass={b('image-card-item')}
                      buttonsClass={b('image-card-actions')}
                      imageClass={b('image-card-image')}
                      iconClass={b('image-buttons')}
                      fileData={item}
                      showPopup={this.showPopup}
                      withMenu
                      selected={selected}
                      addToSelected={this.addToSelected}
                      removeFromSelected={this.removeFromSelected}
                      keyValue={`video-card-${item.id}`}
                      isLockDigitizeFiles={isLockDigitizeFiles}
                      orderId={orderId}
                      name={item.name}
                    />
                  )}
                {(filterType === ORDER_AUDIOS ||
                  filterType === ALL_FILES ||
                  filterType === ALL_MEDIA) &&
                  item.audioUri && (
                    <AudioCard
                      className={b('image')}
                      contentClass={b('image-card-item')}
                      buttonsClass={b('image-card-actions')}
                      imageClass={b('image-card-image')}
                      iconClass={b('image-buttons')}
                      fileData={item}
                      showPopup={this.showPopup}
                      withMenu
                      selected={selected}
                      addToSelected={this.addToSelected}
                      removeFromSelected={this.removeFromSelected}
                      keyValue={`audio-card-${item.id}`}
                      isLockDigitizeFiles={isLockDigitizeFiles}
                      orderId={orderId}
                    />
                  )}
              </>
            )
            ))}
            {isLockDigitizeTabs && (
              <div className={b('text-popup-block')}>
                <div className={b('text-popup-content')}>
                  <span className={b('popup-text')}>{RENEW_POPUP_TEXT}</span>
                </div>
              </div>
            )}
          </div>
        )}
        {viewType === VIEW_TYPES.slider && (
          <ImageSlider
            addToSelected={this.addToSelected}
            removeFromSelected={this.removeFromSelected}
            selected={selected}
            cards={filesForSlider}
            orderPrints={this.orderPrints}
            enhancePhoto={this.enhancePhoto}
            selectTailView={() => {
              this.changeViewType(VIEW_TYPES.tile);
            }}
            afterChange={this.afterChangeSlider}
            orderId={orderId}
            cloud
          />
        )}
        {this.checkShowPopup('imageUri') &&
          ReactDOM.createPortal(
            <div
              className={b('veil')}
              ref={this.veil}
              onClick={this.closeModal}
              role="button"
              tabIndex="0"
            >
              <div className={b('popup-block')}>
                <div
                  role="button"
                  tabIndex="0"
                  onClick={(e) => this.closeModal(e, true)}
                  className={b('popup-content')}
                >
                  <img
                    className={b('preview-image')}
                    src={fileData.imageUri}
                    alt={fileData.imageUri}
                    layout="fill"
                  />
                </div>
                <button
                  className={b('control-btn')}
                  type="button"
                  onClick={() => this.showPopup(null)}
                >
                  <CloseIcon2 />
                </button>
              </div>
            </div>,
            document.getElementById('__next') || null,
          )}
        {this.checkShowPopup('videoUri') &&
          ReactDOM.createPortal(
            <div
              className={b('veil')}
              ref={this.veil}
              onClick={this.closeModal}
              role="button"
              tabIndex="0"
            >
              <div className={b('popup-block', { video: true })}>
                <div className={b('popup-content', { audio: true })}>
                  <video className={b('preview-video')} width="100%" controls ref={this.mediaRef}>
                    <source src={fileData.videoUri} />
                    <track kind="captions" />
                  </video>
                  <div className={b('popup-control-block', { video: true })}>
                    <button
                      className={b('control-btn', { video: true })}
                      type="button"
                      onClick={() => this.showPopup(null)}
                    >
                      <CloseIcon2 />
                    </button>
                  </div>
                </div>
              </div>
            </div>,
            document.getElementById('__next') || null,
          )}
        {this.checkShowPopup('audioUri') &&
          ReactDOM.createPortal(
            <div
              className={b('veil')}
              ref={this.veil}
              onClick={this.closeModal}
              role="button"
              tabIndex="0"
            >
              <div className={b('popup-block')}>
                <div className={b('popup-content', { audio: true })}>
                  <div className={b('audio-block')}>
                    <div className={b('image-wrap')}>
                      <Image
                        className={b('note')}
                        src={note}
                        alt="note"
                        width="100%"
                        height="100%"
                      />
                    </div>
                    <audio className={b('audio')} width="100%" controls ref={this.mediaRef}>
                      <source src={fileData.audioUri} />
                      <track kind="captions" />
                    </audio>
                  </div>
                  <div className={b('popup-control-block', { audio: true })}>
                    <button
                      className={b('control-btn')}
                      type="button"
                      onClick={() => this.showPopup(null)}
                    >
                      <CloseIcon2 />
                    </button>
                  </div>
                </div>
              </div>
            </div>,
            document.getElementById('__next') || null,
          )}
        {showSharedPopup && <SharePopup data={selectedData} close={this.closeSharedPopup} />}
        {isShowDownloadPopup && (
          <DownloadProgressPopup close={this.closeDownloadPopup} isLoaded={isLoaded} />
        )}
        {Boolean(isShowSlideshow && filesToSlideshow && filesToSlideshow.length) && (
          <SlideshowBlock
            cards={filesToSlideshow}
            close={this.closeSlideshow}
            firstSlideIndex={startIndex}
          />
        )}
        {isShowSubscribePopup &&
          ReactDOM.createPortal(
            <MemoyaCloudRenewPopup close={this.closeSubscribePopup} />,
            document.getElementById('__next') || null,
          )}
        {isShowHintPopup && (
          <CloudHintPopup
            close={this.closeHintPopup}
            buttonText={HINT_POPUP_BUTTON_TEXT}
            title={HINT_POPUP_TITLE}
            list={HINT_LIST}
            onRenew={this.onRenew}
          />
        )}
      </section>
    );
  }
}

MyCloudFiles.propTypes = propTypes;
MyCloudFiles.defaultProps = defaultProps;

const stateProps = (state) => ({
  orderImages: approvedOrderImagesSelector(state),
  orderVideos: orderVideosSelect(state),
  orderAudios: orderAudiosSelect(state),
  orderFolder: myCloudFolder(state),
  orderDetails: orderDetailsSelect(state),
  rootFolder: getRootFilesSelector(state),
  foldersParent: getFileSelector(state),
  token: getToken(state),
  cloudImages: myCloudImages(state),
  cloudAudio: myCloudAudio(state),
  cloudVideo: myCloudVideo(state),
  isDownloading: isDownloadingSelector(state),
  cloudCount: cloudStatus(state),
  isSixtyDaysHavePassed: isSixtyDaysHavePassedSelector(state),
  photoArtProducts: photoArtProductsSelector(state),
  frameMaterials: frameMaterialsSelector(state),
});

const actions = {
  downloadArchive: getArchiveActions,
  setImage: setApprovedImageUrl,
  addImageList: addImageListAction,
  addOrder: addToCart,
  addCloudFile: addFileToCloud,
  deleteCloudFile: deleteFileFromCloud,
  addAllFiles: addAllFilesToCloud,
  deleteAllFiles: deleteAllFilesFromCloud,
  changePlayerFlag: changePlayerFlagAction,
  sortFolder: sortFolderAction,
  clearFolders: cleanFolder,
  clearAllRootFolder: clearRootFolder,
  getAllFolders: getFolderRoot,
  openFolder: getFolderById,
};

export default connect(stateProps, actions)(MyCloudFiles);
