import dynamic from "next/dynamic";

import hintImage from  'public/img/shared/successImage.png';

const EnhanceIcon = dynamic(() => import('src/components/svg/EnhanceIcon'));
const ScanIcon = dynamic(() => import('src/components/svg/ScanIcon'));
const VideoIcon = dynamic(() => import('src/components/svg/VideoIcon'));
const AudioIcon = dynamic(() => import('src/components/svg/AudioIcon'));

const ORDER_IMAGES = 'orderImages';
const ORDER_VIDEOS = 'orderVideos';
const ORDER_AUDIOS = 'orderAudios';
const ORDER_SCAN = 'orderScan';
const ALL_FILES = 'allFiles';
const ALL_MEDIA = 'allMedia';

const IMAGE = 'image';
const APPLICATION = 'application';
const VIDEO = 'video';
const AUDIO = 'audio';

const FILTER_BUTONS = [
  {
    type: ALL_MEDIA,
    Component: ScanIcon,
    isShow: true,
    text: 'All media',
    name: 'allMedia',
  },
  {
    type: ORDER_SCAN,
    Component: ScanIcon,
    isShow: true,
    text: 'Viewing photos',
    name: 'Photos',
  },
  {
    type: ORDER_VIDEOS,
    Component: VideoIcon,
    isShow: true,
    text: 'Viewing videos',
    name: 'Videos',
  },
  {
    type: ORDER_AUDIOS,
    Component: AudioIcon,
    isShow: true,
    text: 'Viewing audios',
    name: 'Audio',
  },
  {
    type: ORDER_IMAGES,
    Component: EnhanceIcon,
    isShow: true,
    text: 'Viewing enhanced',
    name: 'Enhanced',
  },
];

const VIEW_RESTORATIONS = 'View Restorations';
const VIEW_MULTIPLE = 'View Multiple';
const ORDER_PRINTS = 'Order prints';
const DOWNLOAD = 'Download';
const SHARE = 'Share';
const SELECT = 'Select';
const ALL = 'all';
const NONE = 'none';
const SELECTED = 'Selected';
const FOLDER = 'folder';

const VIEW_TYPES = {
  tile: 'tile',
  slider: 'slider',
};

const INFO =
  'Please select your desired file(s), then you can \ndownload, share, or create photo art.';

const CLOUD_IMAGES = 'cloudImages';
const CLOUD_AUDIO = 'cloudAudio';
const CLOUD_VIDEO = 'cloudVideo';

const BACK_TO_CREATE = 'Back to create';

const ERROR_TEXT = 'Please select 25 photos or less to create art.';

const MAX_IMAGE_COUNT = 25;

const DIGITIZE_FILE_TYPES = ['orderVideos', 'orderAudios', 'orderScan'];

const RENEW_POPUP_TEXT = 'Your files are archived and cannot be accessed without a subscription.';
const HINT_POPUP_BUTTON_TEXT = 'Renew your membership';
const HINT_POPUP_TITLE = 'Your memories are \nalways a click away';
const HINT_LIST = [
  {
    id: 1,
    image: hintImage,
    title: 'Access your photos \nfrom any device',
  },
  {
    id: 2,
    image: hintImage,
    title: 'Share with family and \nfriends',
  },
  {
    id: 3,
    image: hintImage,
    title: 'Create photo art for \nany occasion',
  },
  {
    id: 4,
    image: hintImage,
    title: 'Securely stored on \nAmazon servers',
  },
];

export {
  FILTER_BUTONS,
  VIEW_RESTORATIONS,
  VIEW_TYPES,
  VIEW_MULTIPLE,
  ORDER_PRINTS,
  DOWNLOAD,
  SHARE,
  SELECT,
  ALL,
  NONE,
  SELECTED,
  INFO,
  ORDER_IMAGES,
  ORDER_VIDEOS,
  ORDER_AUDIOS,
  ORDER_SCAN,
  ALL_FILES,
  ALL_MEDIA,
  CLOUD_AUDIO,
  CLOUD_IMAGES,
  CLOUD_VIDEO,
  BACK_TO_CREATE,
  ERROR_TEXT,
  MAX_IMAGE_COUNT,
  DIGITIZE_FILE_TYPES,
  RENEW_POPUP_TEXT,
  HINT_POPUP_BUTTON_TEXT,
  HINT_POPUP_TITLE,
  HINT_LIST,
  FOLDER,
  APPLICATION,
  IMAGE,
  AUDIO,
  VIDEO,
};
