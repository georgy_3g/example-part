import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Slider from 'react-slick';
import bem from 'src/utils/bem';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import colors from 'src/styles/colors.json';
import dynamic from "next/dynamic";
import styles from './index.module.scss';

const ArrowInSquareIcon = dynamic(() => import('src/components/svg/ArrowInSquareIcon'));
const ColorButton = dynamic(() => import('src/components/buttons/ColorButton'));
const CompareSlideCard = dynamic(() => import('src/components/cards/CompareSlideCard'));

const orangeColor = colors['$burnt-sienna-color'];
const b = bem('image-slider-with-compare', styles);

const propTypes = {
  slideList: PropTypes.arrayOf(PropTypes.shape()),
  dots: PropTypes.bool,
  arrows: PropTypes.bool,
  slidesToShow: PropTypes.number,
  slidesToScroll: PropTypes.number,
  smallShow: PropTypes.number,
  miniShow: PropTypes.number,
  smallScroll: PropTypes.number,
  className: PropTypes.string,
  imageClass: PropTypes.string,
  beforeChange: PropTypes.func,
  lazyLoad: PropTypes.bool,
  afterChange: PropTypes.func,
  autoplay: PropTypes.bool,
  pauseOnHover: PropTypes.bool,
  autoplaySpeed: PropTypes.number,
  withOutTitle: PropTypes.bool,
  compareImageClass: PropTypes.string,
  infinite: PropTypes.bool,
  withScrollButton: PropTypes.bool,
  scroll: PropTypes.func,
  scrollButtonText: PropTypes.string,
  customClassBtn: PropTypes.string,
};

const defaultProps = {
  slideList: [],
  dots: false,
  arrows: false,
  slidesToShow: 3,
  slidesToScroll: 1,
  smallShow: 2,
  miniShow: 1,
  smallScroll: 1,
  className: '',
  imageClass: '',
  beforeChange: () => {},
  afterChange: () => {},
  lazyLoad: true,
  autoplay: false,
  pauseOnHover: false,
  autoplaySpeed: 5000,
  withOutTitle: false,
  compareImageClass: '',
  infinite: true,
  withScrollButton: false,
  scroll: () => {},
  scrollButtonText: '',
  customClassBtn: '',
};

class ImageSliderWithCompare extends Component {
  constructor(props) {
    super(props);
    this.slider = null;
    this.state = {
      index: 0,
    };
  }

  componentWillUnmount() {
    this.slider = null;
  }

  updateCount = (valueOne, valueTwo) => {
    const { beforeChange } = this.props;
    beforeChange(valueTwo);
    this.setState({ index: valueTwo });
  };

  settings = () => {
    const {
      arrows,
      slidesToShow,
      slidesToScroll,
      smallShow,
      smallScroll,
      afterChange,
      lazyLoad,
      autoplay,
      pauseOnHover,
      autoplaySpeed,
      miniShow,
      infinite,
    } = this.props;
    return {
      arrows,
      infinite,
      focusOnSelect: false,
      lazyLoad,
      speed: 500,
      slidesToShow,
      slidesToScroll,
      beforeChange: this.updateCount,
      afterChange,
      responsive: [
        {
          breakpoint: 1366,
          settings: {
            slidesToShow,
            slidesToScroll: smallScroll,
          },
        },
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: smallShow,
            slidesToScroll: smallScroll,
          },
        },
        {
          breakpoint: 768,
          settings: {
            slidesToShow: smallShow,
            slidesToScroll: smallScroll,
          },
        },
        {
          breakpoint: 750,
          settings: {
            slidesToShow: miniShow,
            slidesToScroll: smallScroll,
          },
        },
      ],
      dotsClass: b('dots'),
      autoplay,
      pauseOnHover,
      autoplaySpeed,
      swipe: false,
    };
  };

  generateDots = () => {
    const { index } = this.state;
    const { slideList } = this.props;
    return slideList.map((item, i) => {
      const { url } = item;
      return {
        isSelect: index === i,
        key: `${i}-${url}`,
        i,
      };
    });
  };

  clickToDot = (value) => {
    this.slider.slickGoTo(value);
  };

  clickNext = () => {
    this.slider.slickNext();
  };

  clickPrev = () => {
    this.slider.slickPrev();
  };

  render() {
    const {
      slideList,
      className,
      imageClass,
      compareImageClass,
      withScrollButton,
      scrollButtonText,
      scroll,
      customClassBtn,
    } = this.props;
    return (
      <div className={b({ mix: className })}>
        <Slider
          ref={(c) => {
            this.slider = c;
          }}
          {...this.settings()}
        >
          {slideList.map((item) => (
            <div className={b('slide-image-wrapper')} key={`${item.id}_${item.url}`}>
              <CompareSlideCard
                item={item}
                imageClass={imageClass}
                compareImageClass={compareImageClass}
              />
              <div className={b('slide-image-title')}>{item.title}</div>
            </div>
          ))}
        </Slider>
        <div className={b('controls')}>
          <button className={b('arrow-btn', { prev: true })} type="button" onClick={this.clickPrev}>
            <ArrowInSquareIcon className={b('arrow-icon', { prev: true })} />
          </button>
          <div className={b('dot-block')}>
            {this.generateDots().map(({ isSelect, key, i }) => (
              <button
                className={b('dot-wrap', { select: isSelect })}
                key={key}
                type="button"
                onClick={() => {
                  this.clickToDot(i);
                }}
              >
                <div className={b('dot', { select: isSelect })} />
              </button>
            ))}
          </div>
          <button className={b('arrow-btn', { next: true })} type="button" onClick={this.clickNext}>
            <ArrowInSquareIcon className={b('arrow-icon', { next: true })} />
          </button>
        </div>
        {withScrollButton && scrollButtonText && (
          <ColorButton
            className={b('scroll-button', { mix: customClassBtn })}
            onClick={scroll}
            text={scrollButtonText}
            backGroundColor={orangeColor}
          />
        )}
      </div>
    );
  }
}

ImageSliderWithCompare.propTypes = propTypes;
ImageSliderWithCompare.defaultProps = defaultProps;

export default ImageSliderWithCompare;
