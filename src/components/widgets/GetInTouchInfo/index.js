import React from 'react';
import bem from 'src/utils/bem';
import PropTypes from 'prop-types';

import dynamic from 'next/dynamic';
import { PLACEHOLDERS, REASON_OPTIONS } from '../../shared/GetInTouch/constants';
import {
  contactsItems,
  contactsTitle,
  description,
  openingItems,
  openingTitle,
  title,
} from './constants';
import styles from './index.module.scss';

const AskQuestionForm = dynamic(() => import('src/components/forms/AskQuestionForm'));

const placeholders = PLACEHOLDERS;
const reasonOptions = REASON_OPTIONS;

const b = bem('get-in-touch-info', styles);

const propTypes = {
  className: PropTypes.string,
  isConnectPage: PropTypes.bool,
};

const defaultProps = {
  className: '',
  isConnectPage: false,
};

function GetInTouchInfo({ className, isConnectPage }) {
  return (
    <section className={b({ mix: className })}>
      {
      isConnectPage
        ? <h1 className={b('title')}>{title}</h1>
        : <span className={b('title')}>{title}</span>
    }
      <h2 className={b('description')}>{description}</h2>
      <div className={b('main-wrapper')}>
        <div className={b('info-wrapper')}>
          <div className={b('info-block-wrapper')}>
            <div className={b('info-block')}>
              <span className={b('info-title')}>{contactsTitle}</span>
              {
              contactsItems.map(({ name, value }) => (
                <div className={b('info-item')} key={value}>
                  <span className={b('info-name')}>{`${name}: `}</span>
                  <span className={b('info-value')}>{value}</span>
                </div>
              ))
            }
            </div>
            <div className={b('info-block')}>
              <span className={b('info-title')}>{openingTitle}</span>
              {
              openingItems.map(({ name, value }) => (
                <div className={b('info-item')} key={value}>
                  <span className={b('info-name')}>{`${name}: `}</span>
                  <span className={b('info-value')}>{value}</span>
                </div>
              ))
            }
            </div>
          </div>
          <AskQuestionForm
            className={b('form-block')}
            placeholders={placeholders}
            reasonOptions={reasonOptions}
          />
        </div>
      </div>
    </section>
  );
}

GetInTouchInfo.propTypes = propTypes;
GetInTouchInfo.defaultProps = defaultProps;

export default GetInTouchInfo;
