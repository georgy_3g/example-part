const title = 'Need a little help from\nyour ******* friends?';
const description = 'Call, chat, message us or come visit our South Florida headquarters.';
const openingTitle = 'Hours of Operation';
const openingItems = [
  { name: 'Monday-Friday', value: '9 am - 6 pm' },
  { name: 'Saturday', value: '11 am - 4 pm' },
];
const contactsTitle = 'Contact Info';
const contactsItems = [
  { name: 'Email', value: 'hello@*******.com' },
  { name: 'Phone', value: '561-235-7808' },
];

export { title, description, openingTitle, openingItems, contactsTitle, contactsItems };
