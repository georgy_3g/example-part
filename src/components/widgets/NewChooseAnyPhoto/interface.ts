export interface imagesOptionInteraface {
  imageId: number;
  quantity: number;
}

export interface enhanceOptionInterface {
  optionId?: number;
  name?: string;
  relationImagesOption?: imagesOptionInteraface[];
  quantity?: number;
}

export interface imageInterface {
  id: number;
  imageUri: string;
  index?: number;
  isSelected?: boolean;
  notes?: string;
  name?: string;
}

export interface noteInterface {
  orderImageId: number;
  notes: string;
  isNew: boolean;
  deleteNote: boolean;
  notesId?: number;
}

export interface Props {
  isColorization?: number[];
  className?: string;
  nameOperation?: string;
  orderImagesEnhance: imageInterface[];
  getOptionEnhanceImage: () => void;
  colorizationImages: imageInterface[];
  isRetouching?: boolean;
  appendOptionEnhance?: enhanceOptionInterface[];
  selectImage: (img: Partial<imageInterface>) => void;
  deleteImageFromSlider: (data: Partial<imageInterface>, isSelect: boolean) => void;
  selectColorizationImage: (data: Partial<imageInterface>, isColor: boolean) => void;
  isShipKit?: boolean;
  setImageNote: (note: noteInterface) => void;
  changeAppendOptions?: (array: enhanceOptionInterface[]) => void;
  step?: number;
}

export interface ObjectWithRefs {
  [key: number]: HTMLDivElement | null;
}
