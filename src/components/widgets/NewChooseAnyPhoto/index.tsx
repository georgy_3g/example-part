import React, { FunctionComponent, useEffect, useRef, useState } from 'react';
import bem from 'src/utils/bem';
import { connect } from 'react-redux';
import dynamic from 'next/dynamic';
import { getOptionEnhance } from 'src/redux/enhance/actions';
import { LABELS_INCLUDE, PHOTO_COLORIZATION } from './constants';
import { Props } from './interface';
import styles from './index.module.scss';
import EnhanceSlider from '../EnhanceSlider';

const ButtonEnhanceOptions = dynamic(() => import('src/components/elements/ButtonEnhanceOptions'));
const EnhanceImageCard = dynamic(() => import('src/components/cards/EnhanceImageCard'));

const b = bem('restoration-new-choose-any-photo', styles);

const ChooseAnyPhoto: FunctionComponent<Props> = (props) => {
  const {
    isColorization,
    getOptionEnhanceImage,
    className,
    orderImagesEnhance,
    nameOperation,
    selectImage,
    deleteImageFromSlider,
    colorizationImages,
    selectColorizationImage,
    appendOptionEnhance,
    changeAppendOptions,
    isRetouching,
    isShipKit,
    step,
  } = props;
  const [show, setShow] = useState<{
    open: boolean;
    optionShow: boolean;
    id?: number;
  }>({ open: false, optionShow: false });
  const [refsList] = useState(new Map());
  const [infoScroll, setScroll] = useState({
    scrollTop: 0,
    scrollLeft: 0,
  });

  const ref = useRef<HTMLDivElement>(null);
  const scroll = useRef<HTMLDivElement>(null);

  const onScroll = () => {
    setScroll({
      scrollTop: scroll.current?.scrollTop || 0,
      scrollLeft: scroll.current?.scrollLeft || 0,
    });
  };

  useEffect(() => {
    if (scroll.current) {
      getOptionEnhanceImage();
      scroll.current.addEventListener('scroll', onScroll);
    }
  }, []);

  const saveImageRef = (imgId: number, e: HTMLDivElement) => {
    const includesRef = refsList.has(imgId);
    if (!includesRef) {
      refsList.set(imgId, e);
    }
  };

  const managementOpenWindow = (id: number) => {
    if (!show.open && !show.optionShow) {
      setShow({ ...show, open: true, id });
    }
    if (show.open && show.optionShow) {
      setShow({ ...show, optionShow: false, id });
    }
    if (show.open && !show.optionShow) {
      setShow({ ...show, open: false, id });
    }
  };

  return (
    <div className={b({ mix: className })}>
      <div className={b('description')}>
        <EnhanceSlider />
      </div>
      <div className={b('choose-photo-main')} ref={ref}>
        <div className={b('choose-photo-container')} ref={scroll}>
          {orderImagesEnhance &&
            orderImagesEnhance.map((item, index) => {
              const { id, isSelected } = item;
              return isSelected ? (
                <div
                  className={b('container-item-wrap')}
                  key={id}
                  ref={(e) => {
                    if (e && id) {
                      saveImageRef(id, e);
                    }
                  }}
                >
                  <EnhanceImageCard
                    className={b('image-card')}
                    data={item}
                    selectedArr={orderImagesEnhance}
                    select={selectImage}
                    deleteImageFromSlider={deleteImageFromSlider}
                    isSelected={item.isSelected}
                    colorizationImages={colorizationImages}
                    selectColorizationImage={selectColorizationImage}
                    isRetouching={isRetouching}
                    withOption={id === show.id}
                    withChildren
                    withoutMenu
                    index={index}
                    isShipKit={isShipKit}
                    withoutDelete
                  >
                    <ButtonEnhanceOptions
                      className="btn-options"
                      nameOperation={
                        isColorization && isColorization.includes(id)
                          ? PHOTO_COLORIZATION
                          : nameOperation
                      }
                      idPhoto={id}
                      appendOptionEnhance={appendOptionEnhance}
                      show={show}
                      setShow={setShow}
                      changeAppendOptions={changeAppendOptions}
                      managementOpenWindow={managementOpenWindow}
                      refsRoot={ref.current}
                      positionData={refsList.get(id) || {}}
                      scroll={infoScroll.scrollTop}
                      scrollX={infoScroll.scrollLeft}
                      isCheckout={false}
                      withoutZindex
                    />
                  </EnhanceImageCard>
                </div>
              ) : null;
            })}
        </div>
      </div>
      {isShipKit && step !== 1 ? <div className={b('labels-include')}>{LABELS_INCLUDE}</div> : null}
    </div>
  );
};

const defaultProps = {
  selectImage: () => {
    return false;
  },
  deleteImageFromSlider: () => {
    return false;
  },
  colorizationImages: [],
  selectColorizationImage: () => {
    return false;
  },
  isRetouching: false,
  isShipKit: false,
  isColorization: [],
  className: '',
  hidden: false,
  nameOperation: 'photo_restoration',
  orderImagesEnhance: [],
  getOptionEnhanceImage: () => {
    return false;
  },
  appendOptionEnhance: [],
  changeAppendOptions: () => {
    return false;
  },
};

ChooseAnyPhoto.defaultProps = defaultProps;

const actions = {
  getOptionEnhanceImage: getOptionEnhance,
};

export default connect(null, actions)(ChooseAnyPhoto);
