const PHOTO_COLORIZATION = 'photo_colorization';
const LABELS_INCLUDE = 'Your kit will include labels to number your photos.';

export { PHOTO_COLORIZATION, LABELS_INCLUDE };
