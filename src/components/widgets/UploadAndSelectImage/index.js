import React, { Component } from 'react';
import PropTypes from 'prop-types';
import bem from 'src/utils/bem';
import { connect } from 'react-redux';
import ImageUploader from 'src/components/widgets/ImageUploader';
import dynamic from 'next/dynamic';
import { numberOfUploadImagesSelector } from 'src/redux/shared/selectors';
import { selectedImagesSelector } from 'src/redux/photoArt/selectors';
import { orderImagesSelector } from 'src/redux/orderImages/selectors';
import {
  approvedImageSelect,
  imageListSelector,
} from 'src/redux/approvedOrderImage/selectors';
import {
  clearApprovedImageUrl,
  clearImageListAction,
  setApprovedImageUrl,
} from 'src/redux/approvedOrderImage/actions';
import { clearOldImages, deleteImages } from 'src/redux/orderImages/actions';
import { addImages, deleteImage } from 'src/redux/photoArt/actions';
import styles from './index.module.scss';

const NewThumbnailCard = dynamic(() => import('src/components/cards/NewThumbnailCard'));

const b = bem('upload-and-select-image', styles);

const defaultProps = {
  className: '',
  photo: {},
  imageFromLink: {},
  promoSection: {},
  selectedPhotos: [],
  selected: null,
  deleteImageFromSlider: () => {},
  deleteDamageImage: () => {},
  selectImage: () => {},
};

const propTypes = {
  className: PropTypes.string,
  photo: PropTypes.shape({ id: PropTypes.number }),
  imageFromLink: PropTypes.shape({ id: PropTypes.number }),
  promoSection: PropTypes.shape({ id: PropTypes.number }),
  selectedPhotos: PropTypes.arrayOf(PropTypes.shape),
  selected: PropTypes.oneOfType([PropTypes.shape({}), PropTypes.number]),
  deleteImageFromSlider: PropTypes.func,
  deleteDamageImage: PropTypes.func,
  selectImage: PropTypes.func,
};

class UploadAndSelectImage extends Component {
  generateTemplateImages = (number) => {
    const newTemplate = [];
    for (let i = 0; i < number; i += 1) {
      newTemplate.push({
        isTemplate: true,
        id: `template_${i}`,
      });
    }
    return number ? newTemplate : [];
  };

  render() {
    const {
      className,
      selected,
      selectedPhotos,
      deleteImageFromSlider,
      deleteDamageImage,
      selectImage,
    } = this.props;

    return (
      <section className={b({ mix: className })}>
        <ImageUploader
          className={b('uploader', { 'with-images': Boolean(selectedPhotos.length) })}
          withoutText
          withoutBottomText={Boolean(selectedPhotos.length)}
          withoutError
          buttonClass={b('uploader-button')}
          forPhotoArtConstructor
        />
        {Boolean(selectedPhotos.length) && (
          <div className={b('list-wrap')}>
            <div className={b('image-list')}>
              {selectedPhotos.map((item, index) => (
                <NewThumbnailCard
                  className={b('image')}
                  data={item}
                  withCrop
                  index={index}
                  deleteImageFromSlider={deleteImageFromSlider}
                  onImageError={deleteDamageImage}
                  select={selectImage}
                  selected={selected}
                  isSmallCard
                  withOutZoom
                  withoutMenu
                />
              ))}
            </div>
          </div>
        )}
      </section>
    );
  }
}

UploadAndSelectImage.propTypes = propTypes;
UploadAndSelectImage.defaultProps = defaultProps;

const stateProps = (state) => ({
  numberOfUploadImages: numberOfUploadImagesSelector(state),
  selectedImages: selectedImagesSelector(state),
  orderImages: orderImagesSelector(state),
  imageList: imageListSelector(state),
  photo: approvedImageSelect(state),
});

const actions = {
  setApprovedImage: setApprovedImageUrl,
  clearOldImage: clearOldImages,
  clearImageList: clearImageListAction,
  addImageList: addImages,
  deleteOrderImg: deleteImage,
  deleteImg: deleteImages,
  clearApprovedImage: clearApprovedImageUrl,
};

export default connect(stateProps, actions)(UploadAndSelectImage);
