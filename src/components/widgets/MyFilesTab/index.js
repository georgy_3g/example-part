import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import bem from 'src/utils/bem';
import dynamic from 'next/dynamic';
import { getPromoSection } from 'src/redux/promo/actions';
import styles from './index.module.scss';

const MyCloudFiles = dynamic(() => import('src/components/widgets/MyCloudFiles'));
const MyCloudPhotoArt = dynamic(() => import('src/components/widgets/MyCloudPhotoArt'));

const b = bem('my-files-tab', styles);

const defaultProps = {
  query: {},
  getPromo: () => {},
};

const propTypes = {
  query: PropTypes.shape({}),
  getPromo: PropTypes.func,
};

class MyFilesTab extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isShowCloud: false,
    };
  }

  componentDidMount() {
    const { getPromo } = this.props;
    getPromo({ key: 'account?main=myFiles' });
  }

  isShowCloudChange = (value) => {
    const { getPromo } = this.props;
    getPromo({ key: value ? 'account?main=myFiles' : 'account?main=create' });
    this.setState({ isShowCloud: value });
  };

  render() {
    const { isShowCloud } = this.state;
    const { query } = this.props;
    return (
      <div className={b()}>
        <div className={b('background', { white: isShowCloud })} />
        <MyCloudFiles
          isComponentShow={!isShowCloud}
          isShowCloudChange={this.isShowCloudChange}
          query={query}
        />
        <MyCloudPhotoArt isComponentShow={isShowCloud} isShowCloudChange={this.isShowCloudChange} />
      </div>
    );
  }
}

MyFilesTab.propTypes = propTypes;
MyFilesTab.defaultProps = defaultProps;

const actions = {
  getPromo: getPromoSection,
};

export default connect(null, actions)(MyFilesTab);
