import dynamic from "next/dynamic";

const EnhanceIcon = dynamic(() => import('src/components/svg/EnhanceIcon'));
const ScanIcon = dynamic(() => import('src/components/svg/ScanIcon'));
const VideoIcon = dynamic(() => import('src/components/svg/VideoIcon'));
const AudioIcon = dynamic(() => import('src/components/svg/AudioIcon'));
const ORDER_IMAGES = 'orderImages';
const ORDER_VIDEOS = 'orderVideos';
const ORDER_AUDIOS = 'orderAudios';
const ORDER_SCAN = 'orderScan';

const FILTER_BUTONS = [
  {
    type: ORDER_IMAGES,
    Component: EnhanceIcon,
    isShow: true,
    text: 'Viewing enhanced',
    name: 'Enhanced',
  },
  {
    type: ORDER_SCAN,
    Component: ScanIcon,
    isShow: true,
    text: 'Viewing photos',
    name: 'Photos',
  },
  {
    type: ORDER_VIDEOS,
    Component: VideoIcon,
    isShow: true,
    text: 'Viewing videos',
    name: 'Videos',
  },
  {
    type: ORDER_AUDIOS,
    Component: AudioIcon,
    isShow: true,
    text: 'Viewing audios',
    name: 'Audio',
  },
];

const VIEW_RESTORATIONS = 'View Restorations';
const VIEW_MULTIPLE = 'View Multiple';
const ORDER_PRINTS = 'Order prints';
const DOWNLOAD = 'Download';
const SHARE = 'Share';
const SELECT = 'Select';
const ALL = 'all';
const NONE = 'none';
const SELECTED = 'Selected';

const VIEW_TYPES = {
  tile: 'tile',
  slider: 'slider',
};

const INFO =
  'Please select your desired file(s), then you can download, share, or create photo art.';

export {
  FILTER_BUTONS,
  VIEW_RESTORATIONS,
  VIEW_TYPES,
  VIEW_MULTIPLE,
  ORDER_PRINTS,
  DOWNLOAD,
  SHARE,
  SELECT,
  ALL,
  NONE,
  SELECTED,
  INFO,
  ORDER_IMAGES,
  ORDER_VIDEOS,
  ORDER_AUDIOS,
};
