import React, { useEffect, useState } from 'react';
import bem from 'src/utils/bem';
import PropTypes from 'prop-types';
import LazyLoad from 'react-lazyload';
import DICTIONARY from 'src/constants/dictionary';
import ROUTES from 'src/constants/routes';
import { connect } from 'react-redux';
import Image from 'next/image';
import colors from 'src/styles/colors.json';
import FrameSlider from 'src/components/widgets/FrameSlider';
import dynamic from 'next/dynamic';
import { photoArtProductsSelector } from 'src/redux/photoArt/selectors';
import { SLIDER_LIST, WATCH_PHOTO } from './constants';
import styles from './index.module.scss';

const PolaroidImageSlider = dynamic(() => import('src/components/widgets/PolaroidImageSlider'));
const ColorButton = dynamic(() => import('src/components/buttons/ColorButton'));

const { photoArt } = ROUTES;
const pickledBluewoodColor = colors['$pickled-bluewood-color'];

const b = bem('what-included', styles);

const { SEE_PHOTO_ART } = DICTIONARY;

const defaultProps = {
  products: {},
  className: '',
  sliderList: SLIDER_LIST,
  buttonText: SEE_PHOTO_ART,
  buttonLink: photoArt,
  withoutButton: false,
  title: '',
  text: '',
  typeProduct: '',
  backgoundColorBtn: '',
  customClassBtn: '',
  infinite: true,
  isShowRightImage: true,
  withoutControls: false,
  withIcons: false,
  bannerIcons: [],
  isMargin: false,
  isEnhance: false,
};

const propTypes = {
  products: PropTypes.shape({}),
  className: PropTypes.string,
  sliderList: PropTypes.arrayOf(PropTypes.shape({})),
  buttonText: PropTypes.string,
  buttonLink: PropTypes.string,
  withoutButton: PropTypes.bool,
  title: PropTypes.string,
  text: PropTypes.string,
  typeProduct: PropTypes.string,
  infinite: PropTypes.bool,
  isShowRightImage: PropTypes.bool,
  withoutControls: PropTypes.bool,
  backgoundColorBtn: PropTypes.string,
  customClassBtn: PropTypes.string,
  withIcons: PropTypes.bool,
  bannerIcons: PropTypes.array,
  isMargin: PropTypes.bool,
  isEnhance: PropTypes.bool,
};

const WhatIncluded = ({
  products,
  className,
  sliderList,
  buttonText,
  buttonLink,
  withoutButton,
  title,
  text,
  typeProduct,
  infinite,
  isShowRightImage,
  withoutControls,
  backgoundColorBtn,
  customClassBtn,
  withIcons,
  bannerIcons,
  isMargin,
  isEnhance,
}) => {
  const [randomProduct, setRandomProducts] = useState({
    firstArray: [],
    lastArray: [],
  });

  const getArrayOfProducts = () => Object.values(products).map(({ frames }) => frames);

  const getRandom = (array) =>
    array.reduce((acc, item = []) => {
      const randomFrameIndex = Math.floor(Math.random() * (item.length - 1));
      return [...acc, item[randomFrameIndex]];
    }, []);

  useEffect(() => {
    const framesArray = getArrayOfProducts();
    const [firstFrames, secondFrames, thirdFrames, fourthFrame, fiveFrames] = framesArray;
    const firstArray = [firstFrames, secondFrames, thirdFrames, fourthFrame, fiveFrames];
    setRandomProducts({
      firstArray: getRandom(firstArray),
    });
  }, []);

  const { firstArray } = randomProduct;

  const isPhotoArt = typeProduct === 'photo_art';

  return (
    <article className={b({ mix: className, art: isPhotoArt, enhance: isEnhance })}>
      <div className={b('container', { art: isPhotoArt, start: isMargin })}>
        <div className={b('stripe-three', { art: isPhotoArt })} />
        <div className={b('slider', { main: true, 'art-main': isPhotoArt })}>
          <LazyLoad offset={100}>
            {isPhotoArt ? (
              <FrameSlider
                className={b('compare-element')}
                imageClass={b('compare-image')}
                slideList={firstArray}
                lazyLoad
                autoplaySpeed={4000}
              />
            ) : (
              <PolaroidImageSlider
                slideList={sliderList}
                lazyLoad
                autoplaySpeed={4000}
                pauseOnHover
                withoutControls={withoutControls}
              />
            )}
          </LazyLoad>
          <div className={b('stripe-one')} />
          <div className={b('stripe-two')} />
        </div>
        <div className={b('text-block', { art: isPhotoArt, enhance: isEnhance })}>
          <h2 className={b('title', { art: isPhotoArt })}>{title}</h2>
          {withIcons && (
            <div className={b('icons-block', { mobile: true })}>
              {bannerIcons.map(({ text: textIcon, img, color, description }) => (
                <div className={b('icon-block', { enhance: isEnhance })} key={img}>
                  <div className={b('icon-wrap')} style={{ backgroundColor: color }}>
                    <img
                      className={b('icon')}
                      src={img.src || img}
                      alt={textIcon || description}
                    />
                  </div>
                  <span className={b('icon-block-text')}>{textIcon}</span>
                </div>
              ))}
            </div>
          )}
          <div className={b('slider', { mobile: true, 'art-mobile': isPhotoArt })}>
            <LazyLoad offset={100}>
              {isPhotoArt ? (
                <FrameSlider
                  className={b('compare-element')}
                  imageClass={b('compare-image')}
                  slideList={firstArray}
                  lazyLoad
                  autoplaySpeed={4000}
                />
              ) : (
                <PolaroidImageSlider
                  slideList={sliderList}
                  lazyLoad
                  autoplaySpeed={4000}
                  pauseOnHover
                  inifinite={infinite}
                  withoutControls={withoutControls}
                />
              )}
            </LazyLoad>
            <div className={b('stripe-one')} />
            <div className={b('stripe-two')} />
          </div>
          <p className={b('text', { art: isPhotoArt })}>{text}</p>
          {withIcons && (
            <div className={b('icons-block', { desktop: true })}>
              {bannerIcons.map(({ text: textIcon, img, description, color }) => (
                <div className={b('icon-block')} key={img}>
                  <div className={b('icon-wrap')} style={{ backgroundColor: color }}>
                    <img
                      className={b('icon')}
                      src={img.src || img}
                      alt={textIcon || description}
                    />
                  </div>
                  <span className={b('icon-block-text')}>{textIcon}</span>
                </div>
              ))}
            </div>
          )}
          {!withoutButton && (
            <div className={b('btn-block')}>
              <a className={b('link')} href={buttonLink}>
                <ColorButton
                  text={buttonText}
                  className={b('btn', { mix: customClassBtn })}
                  backGroundColor={backgoundColorBtn || pickledBluewoodColor}
                />
              </a>
            </div>
          )}
        </div>
        {isShowRightImage && (
          <div className={b('watch')}>
            <Image src={WATCH_PHOTO} alt="watch" loading="lazy" layout="fill" />
          </div>
        )}
      </div>
    </article>
  );
}

WhatIncluded.propTypes = propTypes;
WhatIncluded.defaultProps = defaultProps;

const stateProps = (state) => ({
  products: photoArtProductsSelector(state),
});

export default connect(stateProps)(WhatIncluded);
