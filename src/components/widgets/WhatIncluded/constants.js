import WATCH_PHOTO from  'public/img/photo-restoration/WHAT_INCLUDED/watch.png';
import slideOne from  'public/img/enhance/whatincluded.png';
import slideTwo from  'public/img/enhance/img2.png';
import slideThree from  'public/img/enhance/img3.png';

const TITLE = 'Create photo art.';
const TEXT = (typeProduct) =>
  `Now for the fun part! Select and personalize your favorite art piece = our exclusive gallery of handcrafted photo art to create something truly extraordinary = your newly ${
    typeProduct ? 'restored' : 'retouched'
  } photo.`;

const SLIDER_LIST = [
  {
    id: 1,
    url: slideOne,
  },
  {
    id: 2,
    url: slideTwo,
  },
  {
    id: 3,
    url: slideThree,
  },
];

export { TITLE, TEXT, SLIDER_LIST, WATCH_PHOTO };
