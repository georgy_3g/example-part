import React from 'react';
import PropTypes from 'prop-types';
import bem from 'src/utils/bem';
import { SIZES, SIZE_FRAME } from './constants';
import styles from './index.module.scss';

const b = bem('size-selector', styles);

const defaultProps = {
  className: '',
  selectedFrame: {},
  orientationType: 'horizontal',
  changeSize: () => {},
  selectedSize: {},
  isMobile: false,
};

const propTypes = {
  className: PropTypes.string,
  selectedFrame: PropTypes.shape({}),
  orientationType: PropTypes.string,
  changeSize: PropTypes.func,
  selectedSize: PropTypes.shape({}),
  isMobile: PropTypes.bool,
};

const sortSize = (sizes) =>
  [...sizes].sort((firstElement, secondElement) => {
    if (firstElement.size - secondElement.size === 0) {
      return firstElement.secondSize - secondElement.secondSize;
    }
    return firstElement.size - secondElement.size;
  });

function SizeSelector(props) {
  const { className, selectedFrame, orientationType, changeSize, selectedSize, isMobile } = props;

  const { options = [] } = selectedFrame || {};

  const sizes = options.reduce(
    (acc, item) => {
      const { frameOrientation, frameSize, price, additionalDiscount } = item;
      if (!acc[frameOrientation].find(({ id }) => id === frameSize.id)) {
        const { id, width, height, name } = frameSize;
        return {
          ...acc,
          [frameOrientation]: [
            ...acc[frameOrientation],
            {
              id,
              size: width,
              secondSize: height,
              value: item.frameSize,
              label: name,
              price,
              additionalDiscount,
            },
          ],
        };
      }
      return acc;
    },
    { horizontal: [], vertical: [] },
  );

  const sortSizes = { horizontal: sortSize(sizes.horizontal), vertical: sortSize(sizes.vertical) };

  const { id: selectedSizeId = 0 } = selectedSize || {};

  return (
    <section className={b({ mix: className })}>
      <span className={b('title')}>{isMobile ? SIZE_FRAME : SIZES}</span>
      <div className={b('size-list')}>
        {sortSizes[orientationType].map((item) => {
          return (
            <button
              className={b('size-btn', { select: selectedSizeId === item.id })}
              key={item.id}
              type="button"
              onClick={() => changeSize(item)}
            >
              <div className={b('bullet', { select: selectedSizeId === item.id })} />
              {item.label}
              <div className={b('size-price')}>
                <span className={b('price-value', { origin: Boolean(item.additionalDiscount) })}>{item.price ? `$${item.price}` : ''}</span>
                {item.additionalDiscount ? (
                  <span className={b('price-value')}>
                    {`$${(item.price - item.additionalDiscount)}`}
                  </span>
                ) : null}
              </div>
            </button>
          )
        })}
      </div>
    </section>
  );
}

SizeSelector.propTypes = propTypes;
SizeSelector.defaultProps = defaultProps;

export default SizeSelector;
