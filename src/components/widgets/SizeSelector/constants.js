const SIZES = 'Sizes';
const SIZE_FRAME = 'Sizes';
const PORTRAIT = 'Portrait';
const LANDSCAPE = 'Landscape';
const ORIENTATION_TYPES = {
  horizontal: 'horizontal',
  vertical: 'vertical',
};
const CURRENCY = '$';

export {
  SIZES,
  SIZE_FRAME,
  PORTRAIT,
  LANDSCAPE,
  ORIENTATION_TYPES,
  CURRENCY,
};
