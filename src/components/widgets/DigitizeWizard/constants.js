import filmOne from  'public/img/film-transfer/3.png';
import filmTwo from  'public/img/film-transfer/4.png';
import filmThree from  'public/img/film-transfer/5.png';
import filmFour from  'public/img/film-transfer/7.png';

const BACK = 'Back';
const NEXT = 'Next';
const TITLE = 'Let’s get started \ntransferring your photos';
const DESCRIPTION = (price) => `All photo transfers start at just $${price} per tape. \n******* happiness guarantee included`;
const FIRST_STEP_TITLE = 'Enter your estimated number of \nphotos to scan';
const SUBTOTAL = 'Subtotal:';
const FREE = 'Free';

const MIN_QT = 0;
const MAX_QT = 99999;

const ADD_TO_CART = 'Add to cart';

const ENHANCE_MY_TRANSFER = 'enhance_my_transfer';
const ENHANCE_MY_TRANSFER_TITLE = 'Are you photos in books or slide carousels?';
const ENHANCE_MY_TRANSFER_TITLE_MOB = 'How many books or carousels?';
const MEMOYA_CLOUD = 'cloud_download';
const WARNING = 'Please select at least one option';
const PERSONALIZE = 'personalize';
const USB_DRIVE = '*******_safe';
const DVD_SET = 'archival_dvd_set';
const PERSONALIZE_BTN_TEXT = 'Personalize your USB';
const PERSONALIZE_TEXT = 'Personalize';
const FILM_TRANSFER = 'film_transfer';
const THREE_REELS_QUANTITY = 50;
const FOUR_REELS_QUANTITY = 100;
const FIVE_REELS_QUANTITY = 200;
const SEVEN_REELS_QUANTITY = 400;
const FILM_TRANSFER_CARDS = [
  {
    id: 1,
    imageUri: filmOne,
    title: 'How many \n3” reels?',
    subTitle: 'feet per reel',
    price: 50,
    name: 'threeReels',
  },
  {
    id: 2,
    imageUri: filmTwo,
    title: 'How many \n4” reels?',
    subTitle: 'feet per reel',
    price: 100,
    name: 'fourReels',
  },
  {
    id: 3,
    imageUri: filmThree,
    title: 'How many \n5” reels?',
    subTitle: 'feet per reel',
    price: 200,
    name: 'fiveReels',
  },
  {
    id: 4,
    imageUri: filmFour,
    title: 'How many \n7” reels?',
    subTitle: 'feet per reel',
    price: 400,
    name: 'sevenReels',
  },
];
const TOTAL_FOOTAGE = 'Total footage:';
const TOGGLER_PRODUCT=['tape_transfer', 'audio_transfer'];

const HAPPINESS = 'happiness guarantee';
const INCLUDED = ' included.';

export {
  BACK,
  NEXT,
  FIRST_STEP_TITLE,
  SUBTOTAL,
  MIN_QT,
  MAX_QT,
  ADD_TO_CART,
  DESCRIPTION,
  TITLE,
  ENHANCE_MY_TRANSFER,
  ENHANCE_MY_TRANSFER_TITLE,
  ENHANCE_MY_TRANSFER_TITLE_MOB,
  MEMOYA_CLOUD,
  WARNING,
  PERSONALIZE,
  USB_DRIVE,
  PERSONALIZE_BTN_TEXT,
  PERSONALIZE_TEXT,
  FILM_TRANSFER,
  FILM_TRANSFER_CARDS,
  TOTAL_FOOTAGE,
  HAPPINESS,
  INCLUDED,
  THREE_REELS_QUANTITY,
  FOUR_REELS_QUANTITY,
  FIVE_REELS_QUANTITY,
  SEVEN_REELS_QUANTITY,
  FREE,
  DVD_SET,
  TOGGLER_PRODUCT,
};
