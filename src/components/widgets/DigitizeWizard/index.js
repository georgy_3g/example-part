import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import bem from 'src/utils/bem';
import convertToPrice from 'src/utils/convertToPrice';
import ROUTES from 'src/constants/routes';
import colors from 'src/styles/colors.json';
import Dom from 'react-dom';
import LazyLoad from 'react-lazyload';
import Image from 'next/image';
import bookBackgroundImage from 'public/img/digitize/bookBackgroundImage.jpg';
import filmCardMobileImage from 'public/img/film-transfer/allReel.png';
import dynamic from 'next/dynamic';
import HelpCenter from 'src/components/widgets/HelpCenter';
import { orderPriceSelect } from 'src/redux/prices/selectors';
import { wizardCardsSelector } from 'src/redux/wizardsCards/selectors';
import { storeUtmContentSelector } from 'src/redux/location/selectors';
import { temporaryUtmContentSelector } from 'src/redux/shared/selectors';
import { addToCart } from 'src/redux/orders/actions';
import { getWizardCardsAction } from 'src/redux/wizardsCards/actions';
import { getPriceList } from 'src/redux/prices/actions';
import styles from './index.module.scss';
import {
  ADD_TO_CART,
  BACK,
  DESCRIPTION,
  DVD_SET,
  ENHANCE_MY_TRANSFER,
  ENHANCE_MY_TRANSFER_TITLE,
  ENHANCE_MY_TRANSFER_TITLE_MOB,
  FILM_TRANSFER,
  FILM_TRANSFER_CARDS,
  FIRST_STEP_TITLE,
  FIVE_REELS_QUANTITY,
  FOUR_REELS_QUANTITY,
  FREE,
  MAX_QT,
  MEMOYA_CLOUD,
  MIN_QT,
  NEXT,
  PERSONALIZE,
  PERSONALIZE_BTN_TEXT,
  PERSONALIZE_TEXT,
  SEVEN_REELS_QUANTITY,
  SUBTOTAL,
  THREE_REELS_QUANTITY,
  TITLE,
  TOGGLER_PRODUCT,
  TOTAL_FOOTAGE,
  USB_DRIVE,
  WARNING,
} from './constants';

const BigCountInput = dynamic(() => import('src/components/inputs/BigCountInput'));
const ColorButton = dynamic(() => import('src/components/buttons/ColorButton'));
const DigitizePersonalizeCard = dynamic(() => import('src/components/cards/DigitizePersonalizeCard'));
const DollarIcon = dynamic(() => import('src/components/svg/DollarIcon'));
const NewTogglerInput = dynamic(() => import('src/components/inputs/NewTogglerInput'));
const PersonalizeIcon = dynamic(() => import('src/components/svg/PersonalizeIcon'));


const b = bem('digitize-wizard', styles);
const burntSiennaRedColor = colors['$burnt-sienna-red-color'];
const orangeColor = colors['$sea-pink-red-color'];
const whiteColor = colors['$white-color'];
const blueColor = colors['$pickled-bluewood-color'];

const defaultProps = {
  getWizard: () => {},
  typeProduct: '',
  product: {},
  steps: [],
  addOrder: () => {},
  getPrices: () => {},
  scrollRef: () => {},
  transferTitle: '',
  wizardTitle: TITLE,
  getDescription: DESCRIPTION,
  firstStepTitle: FIRST_STEP_TITLE,
  secondStepTitle: '',
  storeUtmContent: '',
  isSvg: false,
  bannerIcons: [],
  temporaryUtmContent: '',
  windowWidth: 1920,
};

const propTypes = {
  getWizard: PropTypes.func,
  typeProduct: PropTypes.string,
  product: PropTypes.shape({
    options: PropTypes.arrayOf(PropTypes.shape({})),
    price: PropTypes.number,
    additionalDiscount: PropTypes.number,
    reduce: PropTypes.func,
    displayName: PropTypes.string,
    id: PropTypes.number,
    name: PropTypes.string,
  }),
  steps: PropTypes.arrayOf(PropTypes.shape({})),
  addOrder: PropTypes.func,
  getPrices: PropTypes.func,
  scrollRef: PropTypes.func,
  transferTitle: PropTypes.string,
  wizardTitle: PropTypes.string,
  getDescription: PropTypes.func,
  firstStepTitle: PropTypes.string,
  secondStepTitle: PropTypes.string,
  storeUtmContent: PropTypes.string,
  temporaryUtmContent: PropTypes.string,
  isSvg: PropTypes.bool,
  bannerIcons: PropTypes.arrayOf(),
  windowWidth: PropTypes.number,
};

class DigitizeWizard extends Component {
  constructor(props) {
    super(props);
    const {
      product: { name },
    } = this.props;
    this.state = {
      step: 0,
      quantity: name === FILM_TRANSFER ? 0 : 1,
      *******_safe: 0,
      archival_dvd_set: 0,
      enhance_my_transfer: 0,
      cloud_download: false,
      personalize: false,
      text: '',
      textSecond: '',
      isGift: false,
      isShowPersonalizePopup: false,
      isBook: false,
      threeReels: 0,
      fourReels: 0,
      fiveReels: 0,
      sevenReels: 0,
    };
    this.isNameValue = false;
    this.isUseWizard = false;
  }

  componentDidMount() {
    const { getWizard, typeProduct } = this.props;
    const isShippingTargetPopup = Boolean(window.sessionStorage.getItem('isShippingTargetPopup'));
    getWizard(typeProduct);
    if (!isShippingTargetPopup) {
      this.startTimer();
    }
  }

  componentWillUnmount() {
    this.isNameValue = null;
    this.isUseWizard = null;
  }

  startTimer = () => {
    setTimeout(() => {
      this.openShippingTargetPopup();
    }, 1000);
  };

  openShippingTargetPopup = () => {
    const { storeUtmContent, temporaryUtmContent } = this.props;
    const button = document.getElementById('shipping-target-open-button');
    if (
      (storeUtmContent || temporaryUtmContent || '').toLowerCase() === 'l' &&
      button &&
      !this.isUseWizard
    ) {
      button.click();
      window.sessionStorage.setItem('isShippingTargetPopup', true);
    }
  };

  changeUseStatus = () => {
    this.isUseWizard = true;
  };

  getPrices = () => {
    const {
      quantity,
      *******_safe: safe,
      archival_dvd_set: archival,
      enhance_my_transfer: enhance,
      cloud_download: download,
      personalize,
      isBook,
    } = this.state;
    const { product } = this.props;
    const { options = [], name, price, additionalDiscount } = product;
    const productCustomPrice = additionalDiscount ? price - additionalDiscount : price;
    const values = options.reduce(
      (acc, option) => {
        const {
          name: optionName,
          price: optionPrice,
          additionalDiscount: optionAdditionalDiscount,
        } = option;
          return {
        ...acc,
        [optionName]: {
          ...option,
          customPrice: optionAdditionalDiscount
            ? optionPrice - optionAdditionalDiscount : optionPrice,
        },
      }}, {},
    );
    const {
      enhance_my_transfer: enhanceValue = {},
      archival_dvd_set: archivalValue = {},
      cloud_download: cloudValue = {},
      *******_safe: *******SafeValue = {},
      personalize: personalizeValue = {},
    } = values;
    const quantitySum = (isFullSum) => {
      if (isFullSum) {
        return  name === FILM_TRANSFER
        ? this.calculateFilmFeet() * price
        : (quantity || 0) * price;
      }
      return name === FILM_TRANSFER
        ? this.calculateFilmFeet() * productCustomPrice
        : (quantity || 0) * productCustomPrice;
    };
    const enhanceSum = (isFullSum) => {
      if (isFullSum) {
        return isBook ? (enhance || 0) * enhanceValue.price : 0;
      }
      return isBook ? (enhance || 0) * enhanceValue.customPrice : 0;
    };
    const cloudDownloadSum = (isFullSum) => {
      if (isFullSum) {
        return download ? cloudValue.price : 0;
      }
      return download ? cloudValue.customPrice : 0;
    };
    const *******Sum = (isFullSum) => {
      if (isFullSum) {
        return safe ? safe * *******SafeValue.price : 0;
      }
      return safe ? safe * *******SafeValue.customPrice : 0;
    };
    const personalizeSum = (isFullSum) => {
      if (isFullSum) {
        return personalize ? (safe || 0) * personalizeValue.price : 0;
      }
      return personalize ? (safe || 0) * personalizeValue.customPrice : 0;
    };
    const archivalSetSum = (isFullSum) => {
      if (isFullSum) {
        return (archival || 0) * archivalValue.price;
      }
      return (archival || 0) * archivalValue.customPrice;
    };
    const allSum = (
      quantitySum (false)
      + enhanceSum (false)
      + *******Sum (false)
      + personalizeSum (false)
      + archivalSetSum (false)
      + cloudDownloadSum (false)
    );
    const fullSum = (
      quantitySum(true)
      + enhanceSum(true)
      + *******Sum(true)
      + personalizeSum(true)
      + archivalSetSum(true)
      + cloudDownloadSum(true)
    );

    return {
      allSum,
      fullSum,
    };
  };

  onChange = (name, value) => {
    const valid = this.validateInputEnterCount(value);
    this.setState({ [name]: valid });
    this.changeUseStatus();
  };

  validateInputChangeCount = (qt, inc) => {
    if ((qt === MIN_QT && inc < 0) || (qt >= MAX_QT && inc > 0)) {
      return qt;
    }

    return qt + inc;
  };

  validateInputEnterCount = (value) => {
    if (value >= MIN_QT && value <= MAX_QT) {
      return value;
    }
    return value > MAX_QT ? MAX_QT : MIN_QT;
  };

  validateInputChangeQuantity = (qt, inc) => {
    if (qt >= MAX_QT && inc > 0) {
      return qt;
    }
    if (qt <= 1 && inc < 1) {
      return MIN_QT;
    }
    return qt + inc;
  };

  changeCount = (countName) => (isAdd) => () => {
    const { [countName]: qt } = this.state;
    const inc = isAdd ? 1 : -1;
    const valid = this.validateInputChangeCount(qt, inc);
    this.setState({ [countName]: valid });
    this.changeUseStatus();
  };

  enterCount = (countName) => (value) => {
    const valid = this.validateInputEnterCount(value);
    this.setState({ [countName]: valid });
    this.changeUseStatus();
  };

  changeQuantity = (isAdd, name) => () => {
    const { [name]: qt } = this.state;
    const inc = isAdd ? 1 : -1;
    const valid = this.validateInputChangeQuantity(qt, inc);
    this.setState({ [name]: valid });
    this.changeUseStatus();
  };

  enterQuantity = (value) => {
    const valid = this.validateInputEnterQuantity(value);
    this.setState({ quantity: valid });
    this.changeUseStatus();
  };

  openPopup = () => {
    this.setState({ isShowPersonalizePopup: true });
  };

  closePopup = () => {
    this.setState({ isShowPersonalizePopup: false });
  };

  changePersonalize = (text, textSecond, personalize) => {
    this.setState({
      text: personalize ? text : '',
      textSecond: personalize ? textSecond : '',
      personalize,
      isShowPersonalizePopup: false,
    });
  };

  getOrderSection = (section, index, orderSections) => {
    const { productId, title, subTitle, images, order, description } = section;
    const { step, isBook, quantity, isShowPersonalizePopup, text, textSecond, personalize } =
      this.state;
    const { product, windowWidth } = this.props;
    const { options = [], name: nameProduct } = product;
    const currentProduct = product.id === productId
    ? product
    : options.find(({ id }) => id === productId);
    const name = currentProduct ? currentProduct.name : '';
    const isBookOrCarouselCard = name === ENHANCE_MY_TRANSFER;
    const isMemoyaCloud = name === MEMOYA_CLOUD;
    const isDVD = name === DVD_SET;
    const isPersonalize = name === PERSONALIZE;
    const isUsbDrive = name === USB_DRIVE;
    const isLast = index === orderSections.length - 1;
    const isSecondStepLast = index === orderSections.length - 2;
    const { [name]: nameValue } = this.state;
    const productPrice = currentProduct && currentProduct.price ? currentProduct.price : 0;
    const customPrice = currentProduct
      && currentProduct.price
      && currentProduct.additionalDiscount
      ? currentProduct.price - currentProduct.additionalDiscount : 0;

    const newNameValue =
      productPrice === 0 && name === 'cloud_download' && !this.isNameValue ? true : nameValue;
    if (productPrice === 0 && name === 'cloud_download' && !this.isNameValue) {
      this.isNameValue = true;
      this.onChange(name, true);
    }

    if (step === 0) {
      if (isBookOrCarouselCard) {
        if(windowWidth < 580) {
          return (
            <>
              {this.mobileCard({
                title,
                img: bookBackgroundImage,
                isCarousel: true,
                valueInput: isBook,
                nameInput: 'isBook',
              })}
              {isBook &&
                this.mobileCard({
                  title: ENHANCE_MY_TRANSFER_TITLE_MOB,
                  img: images[images.length - 1],
                  price: productPrice,
                  text: subTitle,
                  additionalPrice: customPrice,
                  valueInput: nameValue,
                  nameInput: name,
                  isScan: true,
                })}
            </>
          );
        }
        return (
          <div className={b('two-cards', { last: isLast })} style={{ order }}>
            <div className={b('card-wrapper', { last: !isBook, carousel: isBook })}>
              <div className={b('card-number')}>{index + 1}</div>
              <div className={b('image-block')}>
                <div className={b('slide-wrap')}>
                  <Image
                    className={b('slide')}
                    src={bookBackgroundImage}
                    alt={bookBackgroundImage}
                    layout="fill"
                  />
                </div>
              </div>
              <div className={b('text-block')}>
                <h3 className={b('title')}>{ENHANCE_MY_TRANSFER_TITLE}</h3>
                {description && (
                  <div className={b('card-description', { 'first-step': true })}>{description}</div>
                )}
                <div className={b('toggler')}>
                  <NewTogglerInput
                    checked={isBook}
                    onChange={(value) => this.onChange('isBook', value)}
                  />
                </div>
              </div>
            </div>
            {isBook && (
              <div className={b('card-wrapper', { last: isLast, book: isBookOrCarouselCard })}>
                <div className={b('image-block', { book: isBookOrCarouselCard })}>
                  <div className={b('slide-wrap')}>
                    <Image
                      className={b('slide')}
                      src={images[images.length - 1]}
                      alt={images[images.length - 1]}
                      layout="fill"
                    />
                  </div>
                </div>
                <div className={b('text-block', { book: isBookOrCarouselCard })}>
                  <div className={b('title')}>{title}</div>
                  <BigCountInput
                    className={b('quantity')}
                    name={name}
                    count={nameValue}
                    bemCount={b}
                    withInput
                    min={0}
                    onChange={(value) => this.onChange(name, value)}
                    changeCount={(value) => this.changeQuantity(value, name)}
                  />
                  <div className={b('price-text-wrapper')}>
                    <span
                      className={b('price-text', { price: true, origin: Boolean(customPrice) })}
                    >
                      {productPrice < 1 ? `${productPrice * 100}¢` : `$${productPrice}`}
                    </span>
                    {Boolean(customPrice) && (
                      <span className={b('price-text', { price: true, custom: true })}>
                        {`${
                        customPrice < 1 ? `${customPrice * 100}¢` : `$${customPrice}`
                      }`}
                      </span>
                    )}
                    <span className={b('price-text')}>{subTitle}</span>
                  </div>
                </div>
              </div>
            )}
          </div>
        );
      }

      if(windowWidth < 580) {
        return this.mobileCard({
          title,
          img: images[images.length - 1],
          price: productPrice,
          text: subTitle,
          additionalPrice: customPrice,
          valueInput:quantity,
          isScan: name === 'photo_scan',
          isAudio: name === 'audio_transfer',
          nameInput: 'quantity',
        })
      }
      return (
        <div className={b('card-wrapper', { last: isLast })} style={{ order }}>
          <div className={b('card-number')}>{index + 1}</div>
          <div className={b('image-block')}>
            <div className={b('slide-wrap')}>
              <Image
                className={b('slide')}
                src={images[images.length - 1]}
                alt={images[images.length - 1]}
                layout="fill"
              />
            </div>
          </div>
          <div className={b('text-block')}>
            <h3 className={b('title')}>{title}</h3>
            {description && (
              <div className={b('card-description', { 'first-step': true })}>{description}</div>
            )}
            <BigCountInput
              className={b('quantity')}
              name="quantity"
              count={quantity}
              bemCount={b}
              withInput
              min={0}
              onChange={(value) => this.onChange('quantity', value)}
              changeCount={(value) => this.changeQuantity(value, 'quantity')}
            />
            <div className={b('price-text-wrapper')}>
              <span className={b('price-text', { price: true, origin: Boolean(customPrice) })}>{productPrice < 1 ? `${productPrice * 100}¢` : `$${productPrice}`}</span>
              {Boolean(customPrice) && (
                <span className={b('price-text', { price: true, custom: true })}>{`${customPrice < 1 ? `${customPrice * 100}¢` : `$${customPrice}`}`}</span>
              )}
              <span className={b('price-text')}>{subTitle}</span>
            </div>
          </div>
        </div>
      );
    }


    if (step === 1 && !isPersonalize) {
      if(windowWidth < 580) {
        return this.mobileCard({
          title,
          img: images[images.length - 1],
          price: productPrice,
          text: subTitle,
          additionalPrice: customPrice,
          valueInput:nameValue,
          isMemoyaCloud,
          isUsbDrive,
          isDVD,
          nameInput: name,
          isOption: true,
        })
      }
      return (
        <div
          className={b('card-wrapper', { last: isSecondStepLast, 'second-step': true })}
          style={{ order }}
        >
          <div className={b('image-block', { 'second-step': true })}>
            <div className={b('slide-wrap')}>
              <Image
                className={b('slide', { 'second-step': true })}
                src={images[images.length - 1]}
                alt={images[images.length - 1]}
                layout="fill"
              />
              {isUsbDrive && (
                <button
                  className={b('personalize-btn', { mobile: true, blue: true })}
                  type="button"
                  onClick={this.openPopup}
                >
                  <PersonalizeIcon
                    plusColor={personalize ? whiteColor : blueColor}
                    shadowColor={personalize ? whiteColor : 'none'}
                  />
                  <span className={b('personalize-btn-label')}>{PERSONALIZE_TEXT}</span>
                </button>
              )}
            </div>
          </div>
          <div className={b('text-block', { 'second-step': true })}>
            <div className={b('title', { 'second-step': true })}>{title}</div>
            <div className={b('card-description', { 'second-step': true })}>{description}</div>
            {isMemoyaCloud ||  (TOGGLER_PRODUCT.includes(nameProduct) && isDVD) ? (
              <div className={b('toggler', { 'second-step': true })}>
                <NewTogglerInput
                  checked={newNameValue}
                  onChange={(value) => {
                    if (isMemoyaCloud) {
                      this.onChange(name, value);
                    } else {
                      const newQuantity = value && Boolean(value) ? quantity : 0
                      this.onChange(name, newQuantity, true)
                    }
                  }}
                />
              </div>
            ) : (
              <BigCountInput
                className={b('quantity', { 'second-step': true })}
                name={name}
                count={nameValue}
                bemCount={b}
                withInput
                min={0}
                onChange={(value) => this.onChange(name, value)}
                changeCount={(value) => this.changeQuantity(value, name)}
              />
            )}
            {productPrice === 0 && name === 'cloud_download' ? (
              <span
                className={b('price-text', { 'second-step': true })}
              >
                {`${FREE} ${subTitle}`}
              </span>
            ) : (
              <div className={b('price-text-wrapper')}>
                <span className={b('price-text', { 'second-step': true, price: true, origin: Boolean(customPrice) })}>{productPrice < 1 ? `${productPrice * 100}¢` : `$${productPrice}`}</span>
                {Boolean(customPrice) && (
                  <span className={b('price-text', { 'second-step': true, price: true, custom: true })}>{`${customPrice < 1 ? `${customPrice * 100}¢` : `$${customPrice}`}`}</span>
                )}
                <span className={b('price-text', { 'second-step': true })}>{subTitle}</span>
              </div>
            )}
            {isUsbDrive && (
              <button
                className={b('personalize-btn', { desktop: true })}
                type="button"
                onClick={this.openPopup}
              >
                <PersonalizeIcon />
                <div className={b('personalize-btn-text')}>{PERSONALIZE_BTN_TEXT}</div>
              </button>
            )}
          </div>
        </div>
      );
    }
    if (step === 1 && isPersonalize) {
      if (isShowPersonalizePopup) {
        return Dom.createPortal(
          <DigitizePersonalizeCard
            item={section}
            initialText={{ text, textSecond }}
            price={currentProduct.price}
            onSubmit={this.changePersonalize}
            closePopUp={this.closePopup}
          />,
          document.getElementById('__next') || null,
        );
      }
    }
    return null;
  };

  nextStep = () => {
    const { step } = this.state;
    this.setState({ step: step + 1 });
    if (window.innerWidth < 1025) {
      this.scrollToWizardTop();
    }
    this.changeUseStatus();
  };

  backStep = () => {
    const { step } = this.state;
    this.setState({ step: step - 1 });
    if (window.innerWidth < 1025) {
      this.scrollToWizardTop();
    }
    this.changeUseStatus();
  };

  onSubmit = (e) => {
    e.preventDefault();
    const { addOrder, transferTitle, getPrices, typeProduct, product } = this.props;
    const {
      quantity,
      *******_safe: safe,
      archival_dvd_set: archival,
      enhance_my_transfer: enhance,
      cloud_download: download,
      personalize,
      text,
      textSecond,
      isGift,
    } = this.state;
    const { options, displayName } = product;
    const values = options.reduce(
      (acc, item) => ({
        ...acc,
        [item.name]: { ...item },
      }),
      {},
    );
    const {
      enhance_my_transfer: enhanceValue,
      archival_dvd_set: archivalValue,
      cloud_download: cloudValue,
      *******_safe: *******SafeValue,
      personalize: personalizeValue,
    } = values;
    const order = {
      productId: product.id,
      quantity: typeProduct === FILM_TRANSFER ? this.calculateFilmFeet() : quantity,
      text,
      textSecond,
      title: transferTitle,
      productName: displayName,
      typeProduct,
      isGift,
      options: [
        {
          optionId: enhanceValue.id,
          quantity: enhance,
          price: enhanceValue.price,
          displayName: enhanceValue.displayName,
          name: enhanceValue.name,
        },
        {
          optionId: archivalValue.id,
          quantity: archival || 0,
          price: archivalValue.price,
          displayName: archivalValue.displayName,
          name: archivalValue.name,
        },
        {
          optionId: cloudValue.id,
          quantity: download,
          price: cloudValue.price,
          displayName: cloudValue.displayName,
          name: cloudValue.name,
        },
        {
          optionId: *******SafeValue.id,
          quantity: safe || 0,
          price: *******SafeValue.price,
          displayName: *******SafeValue.displayName,
          name: *******SafeValue.name,
        },
        {
          optionId: personalizeValue.id,
          quantity: personalize,
          price: personalizeValue.price,
          displayName: personalizeValue.displayName,
          name: personalizeValue.name,
        },
      ],
    };
    addOrder(order);
    window.location.href = ROUTES.checkout;
    const typeTransfer = 'digitize';
    getPrices(typeTransfer);
  };

  onChangeReels = (name, value) => {
    const valid = this.validateInputEnterCount(value);
    this.setState({ [name]: valid }, () => {
      this.changeFilmQuantity();
    });
    this.changeUseStatus();
  };

  changeQuantityReels = (isAdd, name) => () => {
    const { [name]: qt } = this.state;
    const inc = isAdd ? 1 : -1;
    const valid = this.validateInputChangeQuantity(qt, inc);
    this.setState({ [name]: valid }, () => {
      this.changeFilmQuantity();
    });
    this.changeUseStatus();
  };

  changeFilmQuantity = () => {
    const { threeReels, fourReels, fiveReels, sevenReels } = this.state;
    const quantity = threeReels + fourReels + fiveReels + sevenReels;
    this.setState({ quantity });
  };

  calculateFilmFeet = () => {
    const { threeReels, fourReels, fiveReels, sevenReels } = this.state;
    return (
      threeReels * THREE_REELS_QUANTITY +
      fourReels * FOUR_REELS_QUANTITY +
      fiveReels * FIVE_REELS_QUANTITY +
      sevenReels * SEVEN_REELS_QUANTITY
    );
  };

  scrollToWizardTop = () => {
    const targetElement = document.getElementById('digitize-wizard');
    const promoBanner = document.getElementById('promo-banner');
    const bannerBottom = promoBanner ? promoBanner.getBoundingClientRect().bottom : null;
    if (targetElement) {
      const { top } = targetElement.getBoundingClientRect();
      window.scrollTo({
        top: document.documentElement.scrollTop + top - (bannerBottom || 100),
        behavior: 'smooth',
      });
    }
  };

  mobileCard = ({
    title,
    img,
    price,
    text,
    additionalPrice,
    valueInput,
    nameInput,
    isMemoyaCloud,
    isUsbDrive,
    isScan,
    isAudio,
    isDVD,
    isOption,
    isCarousel,
  }) => {
    const { quantity } = this.state;
    const { product } = this.props;
    const { name: nameProduct } = product;
    return (
      <div className={b('card-wrapper')}>
        <h3 className={b('mobile-card-title')}>{title}</h3>
        <div className={b('mobile-card-row')}>
          <div className={b('mobile-image-block')}>
            <div className={b('mobile-image-wrap')}>
              <img
                className={b('mobile-image', {
                  option: isOption,
                  usb: isUsbDrive,
                  scan: isScan || isCarousel,
                  audio: isAudio,
                })}
                src={img.src || img}
                alt={img}
              />
            </div>
          </div>
          <div className={b('mobile-card-col', { 'one-input': isCarousel })}>
            {
              isMemoyaCloud ||  (TOGGLER_PRODUCT.includes(nameProduct) && isDVD) || isCarousel
                ? (
                  <NewTogglerInput
                    className={b('toggler', { mobile: true })}
                    inputClassName={b('toggler-input', { checked: valueInput})}
                    textClass={b('toggler-text',{ mobile: true } )}
                    checked={valueInput}
                    onChange={(value) => {
                      if (isMemoyaCloud) {
                        this.onChange(nameInput, value);
                      } else {
                        const newQuantity = value && Boolean(value) ? quantity : 0
                        this.onChange(nameInput, newQuantity, true)
                      }
                    }}
                  />
                )
                : (
                  <BigCountInput
                    className={b('mobile-counter', { 'small-margin': isUsbDrive && Boolean(additionalPrice)})}
                    inputClass={b('mobile-counter-input')}
                    btnClassName={b('mobile-counter-btn')}
                    name={nameInput}
                    count={valueInput}
                    bemCount={b}
                    withInput
                    min={0}
                    onChange={(value) => this.onChange(nameInput, value)}
                    changeCount={(value) => this.changeQuantity(value, nameInput)}
                  />
                )
            }
            {!isCarousel && (
              <div className={b('mobile-card-info', { 'small-margin': isUsbDrive && Boolean(additionalPrice)})}>
                <span className={b('mobile-card-text', {
              underline: Boolean(additionalPrice),
              first: true,
            })}
                >
                  {price === 0 && nameInput === 'cloud_download'
                ? {FREE}
                : convertToPrice(price)}
                </span>
                {Boolean(additionalPrice) && (<span className={b('mobile-card-text', { red: true })}>{convertToPrice(additionalPrice)}</span>)}
                {text && <span className={b('mobile-card-text')}>{text}</span>}
              </div>
            )}
            {isUsbDrive && (
              <button
                className={b('personalize-btn', {
                  small: true,
                  blue: true,
                  'no-margin': Boolean(additionalPrice)
                })}
                type="button"
                onClick={this.openPopup}
              >
                <PersonalizeIcon />
                <div className={b('personalize-btn-text', { small: true })}>{PERSONALIZE_TEXT}</div>
              </button>
            )}
          </div>
        </div>
      </div>
    );
  }

  getDescriptionPrice = ({ price, additionalDiscount, strings }) => (
    <div className={b('price-wrapper')}>
      <div className={b('price-values')}>
        <span className={b('description-text')}>{strings.first}</span>
        <span className={b('description-price', { origin: true })}>{convertToPrice(price)}</span>
        <span className={b('description-price', { custom: true })}>{convertToPrice(price - additionalDiscount)}</span>
        <span className={b('description-text')}>{strings.second}</span>
      </div>
      <span className={b('description-text')}>{strings.third}</span>
    </div>
  )

  render() {
    const {
      step,
      *******_safe: safe,
      archival_dvd_set: archival,
      cloud_download: download,
      quantity,
    } = this.state;
    const {
      product,
      steps,
      typeProduct,
      wizardTitle,
      getDescription,
      firstStepTitle,
      secondStepTitle,
      isSvg,
      bannerIcons,
      windowWidth,
      scrollRef,
    } = this.props;
    const { allSum, fullSum } = this.getPrices();
    const { price, additionalDiscount } = product;
    const priceInCent = convertToPrice(price);
    const currentStep = steps.length && steps.length >= step + 1 ? steps[step] : {};
    const { helpCenters = [], orderSections = [] } = currentStep;
    const isDisableAddToCart = step === 0 ? !quantity : !safe && !archival && !download;
    const filmFeet = this.calculateFilmFeet();
    return (
      <div className={b()}>
        <div className={b('title-wrapper')}>
          <h2 className={b('header-title')}>{wizardTitle}</h2>
          <div className={b('description')}>
            {getDescription(price, additionalDiscount, this.getDescriptionPrice)}
          </div>
          <LazyLoad className={b('lazy-load')} offset={100}>
            {bannerIcons && bannerIcons.length > 0 && (
            <div
              className={b('icons-block', { [bannerIcons.length]: Boolean(bannerIcons.length) })}
            >
                {bannerIcons.map(({ text, img, svg: Svg, color }) => (
                  <div
                    className={b('icon-block')}
                    key={text}
                    style={{
                    ...(color ? { backgroundColor: color } : {}),
                  }}
                  >
                    {isSvg ? (
                      <Svg className={b('icon')} width="80" height="80" />
                  ) : (
                    <img
                      className={b('icon')}
                      src={img.src}
                      alt={text}
                    />
                  )}
                    <span className={b('icon-block-text')}>{text}</span>
                  </div>
              ))}
            </div>
          )}
          </LazyLoad>
        </div>
        <div className={b('steps-wrapper')} id="digitize-wizard" ref={scrollRef}>
          <LazyLoad offset={100}>
            <div className={b('steps-header')}>
              <div className={b('help-center')}>
                <HelpCenter list={helpCenters} step={step} changeUseStatus={this.changeUseStatus} />
              </div>
              <div className={b('steps-header-text')}>
                {step === 0 ? firstStepTitle : secondStepTitle}
              </div>
              <div className={b('step-header-total', { desktop: true })}>
                <DollarIcon />
                <div className={b('summary-title')}>{SUBTOTAL}</div>
                <div className={b('summary-text-wrapper')}>
                  <div className={b('summary-text', { origin: allSum !== fullSum })}>
                    {`$${fullSum.toFixed(2)}`}
                  </div>
                  {allSum !== fullSum && (
                    <div className={b('summary-text')}>{`$${allSum.toFixed(2)}`}</div>
                )}
                </div>
              </div>
            </div>
            <div className={b('steps', { 'second-step': step === 1 })}>
              {typeProduct === FILM_TRANSFER && step === 0 ? (
                <>
                  <div className={b('film-transfer-cards', { desktop: true })}>
                    {FILM_TRANSFER_CARDS.map((card, idx) => {
                    const { [card.name]: reelsValue } = this.state;
                    return (
                      <div
                        className={b('card-wrapper', {
                          last: idx === FILM_TRANSFER_CARDS.length - 1,
                          'film-card': true,
                        })}
                        key={card.id}
                      >
                        <div
                          className={b('image-block', { 'second-step': true, 'film-card': true })}
                        >
                          <div className={b('slide-wrap', { 'film-card': true })}>
                            <Image
                              className={b('slide', { 'film-card': true })}
                              src={card.imageUri.src}
                              alt="film card"
                              layout="fill"
                            />
                          </div>
                        </div>
                        <div className={b('text-block', { 'film-card': true })}>
                          <h3 className={b('title', { 'film-card': true })}>{card.title}</h3>
                          <BigCountInput
                            className={b('quantity', { 'film-card': true })}
                            name={card.name}
                            count={reelsValue}
                            bemCount={b}
                            withInput
                            min={0}
                            onChange={(value) => this.onChangeReels(card.name, value)}
                            changeCount={(value) => this.changeQuantityReels(value, card.name)}
                          />
                          <span className={b('price-text', { 'film-card': true })}>
                            {`${card.price} ${card.subTitle}`}
                          </span>
                        </div>
                      </div>
                    );
                  })}
                  </div>
                  <div className={b('film-transfer-cards', { mobile: true })}>
                    <div
                      className={b('card-wrapper', {
                      'film-card': true,
                    })}
                    >
                      <div className={b('image-block', { 'second-step': true, 'film-card': true })}>
                        <div className={b('slide-wrap', { 'film-card': true })}>
                          <Image
                            className={b('slide', { 'film-card': true })}
                            src={filmCardMobileImage}
                            alt="film card"
                            layout="fill"
                          />
                        </div>
                      </div>
                      {FILM_TRANSFER_CARDS.map((card) => {
                      const { [card.name]: reelsValue } = this.state;
                      return (
                        <div className={b('text-block', { 'film-card': true })} key={card.id}>
                          <h3 className={b('title', { 'film-card': true })}>{card.title}</h3>
                          <BigCountInput
                            className={b('quantity', {
                              'film-card': true,
                              ...(windowWidth < 780 ? { mix: 'mobile-counter' } : {}),
                            })}
                            inputClass={windowWidth < 780 ? b('mobile-counter-input') : ''}
                            btnClassName={windowWidth < 780 ? b('mobile-counter-btn') : ''}
                            name={card.name}
                            count={reelsValue}
                            bemCount={b}
                            withInput
                            min={0}
                            onChange={(value) => this.onChangeReels(card.name, value)}
                            changeCount={(value) => this.changeQuantityReels(value, card.name)}
                          />
                          <span className={b('price-text', { 'film-card': true })}>
                            {`${card.price} ${card.subTitle}`}
                          </span>
                        </div>
                      );
                    })}
                    </div>
                  </div>
                </>
            ) : (
              orderSections.map((section, index) =>
                this.getOrderSection(section, index, orderSections),
              )
            )}
            </div>
            {typeProduct === FILM_TRANSFER && step === 0 && (
              <div className={b('film-footage', { mobile: true })}>
                <div className={b('film-quantity-wrapper')}>
                  <div className={b('film-quantity-title')}>
                    {`${TOTAL_FOOTAGE}`}
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    {`${filmFeet} feet`}
                  </div>
                </div>
                <div className={b('film-price')}>
                  {additionalDiscount ? (
                    <div className={b('wizard-price-wrapper')}>
                      <span className={b('description-price', { wizard: true })}>
                        {convertToPrice(price)}
                      </span>
                      <span className={b('description-price', { custom: true })}>
                        {convertToPrice(price - additionalDiscount)}
                      </span>
                      <span>per foot</span>
                    </div>
                ) : (
                  `${priceInCent} per foot`
                )}
                </div>
              </div>
          )}
            <div className={b('step-header-total', { mobile: true })}>
              <DollarIcon />
              <div className={b('summary-title')}>{SUBTOTAL}</div>
              <div className={b('summary-text-wrapper')}>
                <div className={b('summary-text', { origin: allSum !== fullSum })}>
                  {`$${fullSum.toFixed(2)}`}
                </div>
                {allSum !== fullSum && (
                  <div className={b('summary-text')}>{`$${allSum.toFixed(2)}`}</div>
              )}
              </div>
            </div>
            {isDisableAddToCart && windowWidth < 580 && <div className={b('prompt')}>{WARNING}</div>}
            <div className={b('buttons', { 'is-one-btn': step === 0 })}>
              {step !== 0 && (
                <div className={b('back-btn')}>
                  <ColorButton text={BACK} onClick={this.backStep} withReversIcon />
                </div>
            )}
              {step === 0 && typeProduct === FILM_TRANSFER && (
                <div className={b('film-footage', { desktop: true })}>
                  <div className={b('film-quantity-wrapper')}>
                    <div className={b('film-quantity-title')}>
                      {`${TOTAL_FOOTAGE}`}
                      &nbsp;&nbsp;&nbsp;&nbsp;
                      {`${filmFeet} feet`}
                    </div>
                  </div>
                  {additionalDiscount ? (
                    <div className={b('film-price')}>
                      <span className={b('description-price', { wizard: true })}>
                        {convertToPrice(price)}
                      </span>
                      <span className={b('description-price', { custom: true })}>
                        {convertToPrice(price - additionalDiscount)}
                      </span>
                      <span>per foot</span>
                    </div>
                ) : (
                  `${priceInCent} per foot`
                )}
                </div>
            )}
              <div className={b('next-btn')}>
                {isDisableAddToCart && windowWidth > 580 && (
                  <div className={b('warning', { mobile: windowWidth < 580 })}>{WARNING}</div>
              )}
                <ColorButton
                  className={b('next-btn')}
                  text={step === 0 ? NEXT : ADD_TO_CART}
                  onClick={step === 0 ? this.nextStep : this.onSubmit}
                  backGroundColor={isDisableAddToCart ? orangeColor : burntSiennaRedColor}
                  withArrow
                  disabled={isDisableAddToCart}
                />
              </div>
            </div>
            <div className={b('bottom-content')}>
              <div className={b('help-center')}>
                <HelpCenter list={helpCenters} step={step} changeUseStatus={this.changeUseStatus} />
              </div>
            </div>
          </LazyLoad>
        </div>
      </div>
    );
  }
}

DigitizeWizard.propTypes = propTypes;
DigitizeWizard.defaultProps = defaultProps;

const stateProps = (state) => ({
  allPrices: orderPriceSelect(state),
  steps: wizardCardsSelector(state),
  storeUtmContent: storeUtmContentSelector(state),
  temporaryUtmContent: temporaryUtmContentSelector(state),
});

const actions = {
  addOrder: addToCart,
  getWizard: getWizardCardsAction,
  getPrices: getPriceList,
};

export default connect(stateProps, actions)(DigitizeWizard);
