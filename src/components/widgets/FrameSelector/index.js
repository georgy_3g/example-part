import React, { useState } from 'react';
import PropTypes from 'prop-types';
import bem from 'src/utils/bem';
import { connect } from 'react-redux';
import Colors from 'src/styles/colors.json';
import FrameSelectorCard from 'src/components/cards/FrameSelectorCard';

import dynamic from 'next/dynamic';
import { isImageUploadSelector } from 'src/redux/shared/selectors';
import { enhanceErrorsSelector } from 'src/redux/orderImages/selectors';
import { ART_COLLECTIONS, FRAMES } from './constants';
import styles from './index.module.scss';

const ArrowInSquareIcon = dynamic(() => import('src/components/svg/ArrowInSquareIcon'));

const gray = Colors['$slate-gray-color'];
const white = Colors['$white-color'];

const b = bem('frame-selector', styles);

const defaultProps = {
  className: '',
  photoArtProducts: {},
  selectedCollection: null,
  selectedFrame: {},
  collections: [],
  selectFrame: () => {},
  selectCollection: () => {},
  listClass: '',
  collectionListClass: '',
  imageData: null,
};

const propTypes = {
  className: PropTypes.string,
  photoArtProducts: PropTypes.shape({}),
  selectedCollection: PropTypes.number,
  selectedFrame: PropTypes.shape({}),
  collections: PropTypes.arrayOf(PropTypes.shape({})),
  selectFrame: PropTypes.func,
  selectCollection: PropTypes.func,
  listClass: PropTypes.string,
  collectionListClass: PropTypes.string,
  imageData: PropTypes.shape({}),
};

function FrameSelector(props) {
  const {
    className,
    photoArtProducts,
    selectedCollection,
    selectedFrame,
    collections,
    selectFrame,
    selectCollection,
    listClass,
    collectionListClass,
    imageData,
  } = props;

  const [isShowFrame, showFrame] = useState(Boolean(selectedCollection));

  const onClickCollection = (id) => {
    selectCollection(id);
    showFrame(true);
  };

  const { frames, name } = selectedCollection
    ? photoArtProducts[selectedCollection]
    : { frames: [] };

  return (
    <section className={b({ mix: className })}>
      {isShowFrame ? (
        <div className={b('frame-selector', { mix: listClass })}>
          <button className={b('back-btn')} type="button" onClick={() => showFrame(false)}>
            <ArrowInSquareIcon className={b('back-icon')} stroke={white} fill={gray} />
          </button>
          <div className={b('title-wrap')}>
            <span className={b('frame-text')}>{name || FRAMES}</span>
          </div>
          <div className={b('frame-list-wrap')}>
            <div className={b('frame-list')}>
              {frames.map((item) => (
                <div className={b('frame-wrap')} key={item.id}>
                  <FrameSelectorCard
                    className={b('frame')}
                    data={item}
                    selected={selectedFrame}
                    select={selectFrame}
                    imageData={imageData}
                  />
                </div>
              ))}
            </div>
          </div>
        </div>
      ) : (
        <div className={b('collection-selector', { mix: collectionListClass })}>
          <span className={b('collections-text')}>{ART_COLLECTIONS}</span>
          {collections.map(({ text, id }) => (
            <button
              className={b('collection-button')}
              key={`${text}-${id}`}
              type="button"
              onClick={() => onClickCollection(id)}
            >
              {text}
            </button>
          ))}
        </div>
      )}
    </section>
  );
}

FrameSelector.propTypes = propTypes;
FrameSelector.defaultProps = defaultProps;

const stateProps = (state) => ({
  isImageUpload: isImageUploadSelector(state),
  enhanceErrors: enhanceErrorsSelector(state),
});

export default connect(stateProps, null)(FrameSelector);
