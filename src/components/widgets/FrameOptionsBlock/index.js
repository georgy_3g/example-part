import React from 'react';
import bem from 'src/utils/bem';
import useMobileStyle from 'src/utils/useMobileStyle';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import PhotoArtText from 'src/components/inputs/PhotoArtText';

import dynamic from 'next/dynamic';
import { currencyPriceSelect, priceSelect } from 'src/redux/prices/selectors';
import { frameDisplaysSelector, photoArtHintsSelector } from 'src/redux/photoArt/selectors';
import { isGoodPhotoSelector } from 'src/redux/shared/selectors';
import { isMobileDeviceSelector } from 'src/redux/auth/selectors';
import { addToCart } from 'src/redux/orders/actions';
import { getPriceList } from 'src/redux/prices/actions';
import { getPhotoArtHintsAction, updateOptions } from 'src/redux/photoArt/actions';
import styles from './index.module.scss';
import {
  COLOR_TITLE,
  DISPLAY_TITLE,
  MAT_TITLE,
  PERSONALIZE_TITLE,
  PLACEHOLDER_PERSONALIZE,
  SELECTORS_IDS,
  TITLE,
} from './constants';

const PhotoArtSelector = dynamic(() => import('src/components/inputs/PhotoArtSelector'));

const b = bem('frame-options-block', styles);

const defaultProps = {
  className: '',
  selectedImage: {},
  disabledForm: false,
  disableFormElements: () => {},
  colors: [],
  mattingColors: [],
  displays: [],
  onChangeColor: () => {},
  frame: {},
  colorValue: null,
  onChangeMat: () => {},
  changeText: () => {},
  changeToggler: () => {},
  onChangeDisplay: () => {},
  selectedMatting: null,
  personalizeText: '',
  displayValue: null,
  isPersonalizeOn: false,
  selectedOption: {},
  isShowColors: false,
  isShowMatting: false,
  isShowDisplay: false,
  isMobileDevice: true,
};

const propTypes = {
  className: PropTypes.string,
  selectedImage: PropTypes.shape({}),
  disabledForm: PropTypes.bool,
  disableFormElements: PropTypes.func,
  colors: PropTypes.arrayOf(PropTypes.shape({})),
  mattingColors: PropTypes.arrayOf(PropTypes.shape({})),
  displays: PropTypes.arrayOf(PropTypes.shape({})),
  onChangeColor: PropTypes.func,
  frame: PropTypes.shape({
    framePhotoPosition: PropTypes.shape({}),
  }),
  colorValue: PropTypes.shape({}),
  onChangeMat: PropTypes.func,
  changeText: PropTypes.func,
  changeToggler: PropTypes.func,
  onChangeDisplay: PropTypes.func,
  selectedMatting: PropTypes.shape({}),
  personalizeText: PropTypes.string,
  displayValue: PropTypes.shape({}),
  isPersonalizeOn: PropTypes.bool,
  selectedOption: PropTypes.shape({}),
  isShowColors: PropTypes.bool,
  isShowMatting: PropTypes.bool,
  isShowDisplay: PropTypes.bool,
  isMobileDevice: PropTypes.bool,
};

function FrameOptionsBlock(props) {
  const {
    className,
    frame: { framePhotoPosition } = {},
    disabledForm,
    disableFormElements,
    colors = [],
    mattingColors = [],
    displays = [],
    colorValue,
    onChangeColor,
    onChangeMat,
    changeText,
    changeToggler,
    onChangeDisplay,
    selectedMatting,
    personalizeText,
    displayValue,
    isPersonalizeOn,
    selectedOption,
    isShowColors,
    isShowMatting,
    isShowDisplay,
    isMobileDevice,
  } = props;

  const isMobileDisplay = useMobileStyle(480, isMobileDevice);

  const { isPersonalize = false } = selectedOption || {};

  return (
    <div className={b({ mix: className })}>
      <span className={b('title')}>{TITLE}</span>
      {(isShowColors || isShowMatting || (isShowDisplay && isMobileDisplay)) && (
        <div className={b('mobile-col')}>
          {isShowColors && (
            <PhotoArtSelector
              title={COLOR_TITLE}
              options={colors}
              className={b('selector')}
              titleClass={b('select-title')}
              containerClass={b('select-cont')}
              onChange={onChangeColor}
              value={colorValue}
              instanceId={SELECTORS_IDS.color}
              inputClass={b('select-input')}
              isVersionDisabled
              disabled={disabledForm}
            />
          )}
          {isShowMatting && (
            <PhotoArtSelector
              title={MAT_TITLE}
              options={mattingColors}
              className={b('selector')}
              titleClass={b('select-title')}
              containerClass={b('select-cont')}
              onChange={onChangeMat}
              value={selectedMatting}
              instanceId={SELECTORS_IDS.mat}
              inputClass={b('select-input')}
              isVersionDisabled
              disabled={disabledForm}
            />
          )}
          {isShowDisplay && isMobileDisplay && (
            <PhotoArtSelector
              title={DISPLAY_TITLE}
              options={displays}
              className={b('selector')}
              titleClass={b('select-title')}
              containerClass={b('select-cont')}
              onChange={onChangeDisplay}
              value={displayValue}
              instanceId={SELECTORS_IDS.display}
              inputClass={b('select-input')}
              isVersionDisabled
              disabled={disabledForm}
            />
          )}
        </div>
      )}
      {((isShowDisplay && !isMobileDisplay) || isPersonalize) && (
        <div className={b('mobile-col', { last: true })}>
          {isShowDisplay && !isMobileDisplay && (
            <PhotoArtSelector
              title={DISPLAY_TITLE}
              options={displays}
              className={b('selector')}
              titleClass={b('select-title')}
              containerClass={b('select-cont')}
              onChange={onChangeDisplay}
              value={displayValue}
              instanceId={SELECTORS_IDS.display}
              inputClass={b('select-input')}
              isVersionDisabled
              disabled={disabledForm}
            />
          )}
          {isPersonalize && (
            <PhotoArtText
              className={b('personalize')}
              titleClass={b('personalize-title')}
              priceClass={b('personalize-price')}
              title={PERSONALIZE_TITLE}
              placeholder={PLACEHOLDER_PERSONALIZE}
              handleChange={changeText}
              value={personalizeText}
              changeToggler={changeToggler}
              isPersonalizeOn={isPersonalizeOn}
              position={framePhotoPosition}
              disabled={disabledForm}
              disableFormElements={disableFormElements}
              selectedOption={selectedOption}
              displayValue={displayValue}
            />
          )}
        </div>
      )}
    </div>
  );
}

FrameOptionsBlock.propTypes = propTypes;
FrameOptionsBlock.defaultProps = defaultProps;

const stateProps = (state) => ({
  price: priceSelect(state),
  currency: currencyPriceSelect(state),
  frameDisplays: frameDisplaysSelector(state),
  hints: photoArtHintsSelector(state),
  isGoodPhoto: isGoodPhotoSelector(state),
  isMobileDevice: isMobileDeviceSelector(state),
});

const actions = {
  addOrder: addToCart,
  getPrices: getPriceList,
  updateStateOptions: updateOptions,
  getPhotoArtHints: getPhotoArtHintsAction,
};

export default connect(stateProps, actions)(FrameOptionsBlock);
