const SELECTORS_IDS = {
  size: 'size',
  display: 'display',
  color: 'color',
  mat: 'mat',
  quantity: 'quantity',
};

const COLOR_TITLE = 'Frame color';
const MAT_TITLE = 'Mat color';
const DISPLAY_TITLE = 'Display option';
const PERSONALIZE_TITLE = 'Personalization';
const PLACEHOLDER_PERSONALIZE = 'Personalize text here';

const TITLE = 'Options';

export {
  SELECTORS_IDS,
  COLOR_TITLE,
  MAT_TITLE,
  DISPLAY_TITLE,
  PLACEHOLDER_PERSONALIZE,
  PERSONALIZE_TITLE,
  TITLE,
};
