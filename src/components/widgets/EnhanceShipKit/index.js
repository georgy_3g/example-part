import React from 'react';
import PropTypes from 'prop-types';
import bem from 'src/utils/bem';
import useMobileStyle from 'src/utils/useMobileStyle';
import Colors from 'src/styles/colors.json';
import Image from 'next/image';
import productCardImage from  'public/img/photo-colorization/productCardImage.png';
import optionsCardImage from  'public/img/photo-colorization/optionsCardImage.png';
import sizeCardImage from  'public/img/photo-colorization/sizeCardImage.png';
import dynamic from "next/dynamic";
import styles from './index.module.scss';
import {
  SCAN_QUANTITY_TITLE,
  SCAN_MATERIAL_TITLE,
  SCAN_SIZE_TITLE,
  CONTROLS_TYPE,
  ICONS,
} from './constants';

const grayColor = Colors['$darker-cadet-blue-color'];
const whiteColor = Colors['$white-color'];
const blueColor = Colors['$powder-blue-color'];
const CustomSelect = dynamic(() => import('src/components/inputs/CustomSelect'));
const BigCountInput = dynamic(() => import('src/components/inputs/BigCountInput'));

const b = bem('enhance-scan-kit-block', styles);

const defaultProps = {
  className: '',
  shipKitCount: 0,
  onEnterCount: () => {},
  changeCount: () => {},
  priceToInput: '',
  materialsToForm: [],
  changeMaterial: () => {},
  materialValue: {},
  sizes: [],
  sizeValue: null,
  changeSize: () => {},
  scanQuantityTitle: SCAN_QUANTITY_TITLE,
  scanMaterialTitle: SCAN_MATERIAL_TITLE,
  scanSizeTitle: SCAN_SIZE_TITLE,
};

const propTypes = {
  className: PropTypes.string,
  shipKitCount: PropTypes.number,
  onEnterCount: PropTypes.func,
  changeCount: PropTypes.func,
  priceToInput: PropTypes.string,
  materialsToForm: PropTypes.arrayOf(PropTypes.shape({})),
  changeMaterial: PropTypes.func,
  materialValue: PropTypes.shape({ id: PropTypes.number }),
  sizes: PropTypes.shape({}),
  sizeValue: PropTypes.shape({}),
  changeSize: PropTypes.func,
  scanQuantityTitle: PropTypes.string,
  scanMaterialTitle: PropTypes.string,
  scanSizeTitle: PropTypes.string,
};

function EnhanceShipKit(props) {
  const {
    className,
    shipKitCount,
    onEnterCount,
    changeCount,
    priceToInput,
    materialsToForm,
    changeMaterial,
    materialValue,
    sizes,
    sizeValue,
    changeSize,
    scanQuantityTitle,
    scanMaterialTitle,
    scanSizeTitle,
  } = props;

  const isBig = useMobileStyle(1920);
  const isSmall = useMobileStyle(1025);
  const isMob = useMobileStyle(768);

  const isMinMax = isBig && !isSmall;

  const fontSize =  '1.25rem';
  const height = isMob ? '48px' : '52px';

  const customStyles = {
    valueContainer: (provided) => ({
      ...provided,
      justifyContent: 'center',
    }),
    singleValue: (provided) => ({
      ...provided,
      color: `${Colors['$pickled-bluewood-color']}`,
      fontWeight: 700,
      fontSize: isMinMax ? '1.04vw' : fontSize,
      lineHeight: isMinMax ? '1.4vw' : '27px',
      letterSpacing: '1.11111px',
      opacity: '0.8',
    }),
    control: (provided) => ({
      ...provided,
      border: 'none',
      backgroundColor: 'transparent',
      height: isMinMax ? '2.7vw' : height,
      width: isMinMax ? '9.37vw' : '180px',
      ...(isMob ? { minHeight: 'auto' } : {}),
    }),
  };
  return (
    <div className={b({ mix: className })}>
      <div className={b('form-block')}>
        <div className={b('form-quantity')}>
          <div className={b('image-wrapper')}>
            <Image
              className={b('image')}
              src={productCardImage}
              alt="step two card"
              layout="fill"
              width='100%'
              height='100%'
            />
          </div>
          <span className={b('form-title')}>{scanQuantityTitle}</span>
          <BigCountInput
            className={b('quantity')}
            name="quantity"
            count={shipKitCount}
            bemCount={b}
            mainCounter
            min={0}
            onChange={onEnterCount}
            changeCount={changeCount}
          />
          <div className={b('price')}>{priceToInput}</div>
        </div>
        <div className={b('form-material')}>
          <div className={b('image-wrapper')}>
            <Image
              className={b('image')}
              src={optionsCardImage}
              alt="step two card"
              layout="fill"
            />
          </div>
          <span className={b('form-title')}>{scanMaterialTitle}</span>
          {Object.keys(materialsToForm[0]).length ? (
            <div className={b('materials-buttons-block')}>
              {materialsToForm.map(({ name, id, img }) => {
                const Icon = ICONS[name.toLowerCase()];
                return (
                  <button
                    className={b('material-btn')}
                    key={`material_${id}_${name}`}
                    type="button"
                    onClick={() => changeMaterial({ name, id, img })}
                  >
                    <div
                      className={b('material-icon-block', { selected: materialValue.id === id })}
                    >
                      <Icon
                        className={b('material-svg')}
                        stroke={materialValue.id === id ? whiteColor : grayColor}
                        fill={materialValue.id === id ? blueColor : grayColor}
                      />
                    </div>
                    <span className={b('material-name')}>{name}</span>
                  </button>
                );
              })}
            </div>
          ) : null}
        </div>
        <div className={b('form-sizes')}>
          <div className={b('image-wrapper')}>
            <Image
              className={b('image')}
              src={sizeCardImage}
              alt="step two card"
              layout="fill"
            />
          </div>
          <span className={b('form-title')}>{scanSizeTitle}</span>
          <div className={b('selector-wrap')}>
            {materialValue && sizeValue && sizes && Object.keys(sizes).length ? (
              <CustomSelect
                className={b('selector-block')}
                selectClass={b('selector')}
                instanceId={CONTROLS_TYPE.sizeValue}
                options={sizes[materialValue.id]}
                value={sizeValue}
                onChange={changeSize}
                customStyles={customStyles}
              />
            ) : null}
          </div>
        </div>
      </div>
    </div>
  );
}

EnhanceShipKit.propTypes = propTypes;
EnhanceShipKit.defaultProps = defaultProps;

export default EnhanceShipKit;
