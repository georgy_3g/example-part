import PhotoPrintsIcon from 'src/components/svg/PhotoPrintsIcon';
import RolledPhotoIcon from 'src/components/svg/RolledPhotoIcon';

const CONTROLS_TYPE = {
  quantity: 'quantity',
  img: 'public/img',
  scanShipKit: 'scanKit',
  shipKit: 'shipping',
  text: 'text',
  radio: 'control-group',
  materialValue: 'materialValue',
  sizeValue: 'sizeValue',
};

const SCAN_TITLE = 'Enter your quantity and select what you will be sending in your scan ship kit.';
const SCAN_MOBILE_TITLE = 'Select what you will be sending in your scan ship kit.';
const SCAN_TEXT =
  'Enter the number of photos you have to restored as well as the size of kit you need.';
const SCAN_QUANTITY_TITLE = `How many photos?`;
const SCAN_MATERIAL_TITLE = 'What type of photos?';
const SCAN_SIZE_TITLE = 'What is the largest photo?';

const ICONS = {
  'photo prints': PhotoPrintsIcon,
  'rolled photo': RolledPhotoIcon,
  'photo on glass': PhotoPrintsIcon,
};

export {
  CONTROLS_TYPE,
  SCAN_TITLE,
  SCAN_TEXT,
  SCAN_QUANTITY_TITLE,
  SCAN_MATERIAL_TITLE,
  SCAN_SIZE_TITLE,
  SCAN_MOBILE_TITLE,
  ICONS,
};
