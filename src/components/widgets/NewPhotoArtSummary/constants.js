const PRICE_TITLE = 'Subtotal:';
const ADD_TO_CART = 'Add to cart';
const PRODUCT_NAME_PHOTO_ART = 'photo_art';
const SAVE_PROJECT = 'Save project';
const UNTITLED_PROJECT = 'Untitled project';
const INFO_TEXT = 'All changes automatically saved';
const EDIT_PROJECT = 'Edit Project';
const RENAME = 'Rename';
const CREATE_NEW_PROJECT = 'Create new project';
const LOGIN_INFO_TEXT = 'Please login or create account to save the project.';

export {
  PRICE_TITLE,
  ADD_TO_CART,
  PRODUCT_NAME_PHOTO_ART,
  SAVE_PROJECT,
  UNTITLED_PROJECT,
  INFO_TEXT,
  EDIT_PROJECT,
  RENAME,
  CREATE_NEW_PROJECT,
  LOGIN_INFO_TEXT,
};
