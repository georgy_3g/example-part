import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import bem from 'src/utils/bem';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import HelpCenter from 'src/components/widgets/HelpCenter';

import ROUTES from 'src/constants/routes';
import dynamic from 'next/dynamic';
import ordersTypeSelect from 'src/redux/orders/selectors';
import { cropedImageSelector } from 'src/redux/photoArt/selectors';
import { orderCropImageSelector } from 'src/redux/orderImages/selectors';
import { getToken } from 'src/redux/auth/selectors';
import { wizardCardsSelector } from 'src/redux/wizardsCards/selectors';
import { addToCart } from 'src/redux/orders/actions';
import { addCroppedImagesToCart, loadImages } from 'src/redux/orderImages/actions';
import { openCartAction } from 'src/redux/shared/actions';
import { getWizardCardsAction } from 'src/redux/wizardsCards/actions';
import styles from './index.module.scss';
import {
  ADD_TO_CART,
  CREATE_NEW_PROJECT,
  EDIT_PROJECT,
  INFO_TEXT,
  LOGIN_INFO_TEXT,
  PRICE_TITLE,
  PRODUCT_NAME_PHOTO_ART,
  RENAME,
  SAVE_PROJECT,
  UNTITLED_PROJECT,
} from './constants';

const CloseIcon2 = dynamic(() => import('src/components/svg/CloseIcon2'));
const DollarIcon = dynamic(() => import('src/components/svg/DollarIcon'));
const RedButton = dynamic(() => import('src/components/buttons/RedButton'));
const EditIcon = dynamic(() => import('src/components/svg/EditIcon'));
const CheckIcon = dynamic(() => import('src/components/svg/CheckIcon'));

const b = bem('new-photo-art-summary', styles);

const defaultProps = {
  className: '',
  options: {},
  material: '',
  product: {},
  addOrder: () => {  },
  typeOrders: {},
  croppedImage: {},
  uploadImages: () => {  },
  token: '',
  orderImages: [],
  addImages: () => {  },
  selectedImage: {},
  saveProject: () => {  },
  isProject: false,
  projectName: '',
  updateProject: () => {  },
  onlyProjectCreateInMob: false,
  withoutProject: false,
  isProjectOnMobile: false,
  steps: [],
  getWizard: () => {  },
};

const propTypes = {
  className: PropTypes.string,
  options: PropTypes.shape({
    quantity: PropTypes.shape({}),
    sizeValue: PropTypes.shape({
      value: PropTypes.shape({
        name: PropTypes.string,
      }),
    }),
    colorValue: PropTypes.shape({
      label: PropTypes.string,
    }),
    selectedOption: PropTypes.shape({
      frameDisplays: PropTypes.arrayOf(),
      price: PropTypes.number,
      parentId: PropTypes.number,
      id: PropTypes.number,
    }),
    isPersonalizeOn: PropTypes.bool,
    personalizeText: PropTypes.string,
    selectedMatting: PropTypes.shape({}),
    displayValue: PropTypes.shape({}),
  }),
  material: PropTypes.string,
  product: PropTypes.shape({
    framePhotoArtMatting: PropTypes.shape({
      price: PropTypes.string,
    }),
    displayName: PropTypes.string,
  }),
  addOrder: PropTypes.func,
  typeOrders: PropTypes.shape({}),
  croppedImage: PropTypes.shape({}),
  uploadImages: PropTypes.func,
  token: PropTypes.string,
  orderImages: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number,
  })),
  addImages: PropTypes.func,
  selectedImage: PropTypes.shape({
    id: PropTypes.number,
  }),
  saveProject: PropTypes.func,
  isProject: PropTypes.bool,
  projectName: PropTypes.string,
  updateProject: PropTypes.func,
  onlyProjectCreateInMob: PropTypes.bool,
  withoutProject: PropTypes.bool,
  steps: PropTypes.arrayOf(PropTypes.shape({})),
  getWizard: PropTypes.func,
  isProjectOnMobile: PropTypes.bool,
};

class NewPhotoArtSummary extends Component {
  constructor(props) {
    super(props);
    this.state = {
      newProjectName: '',
      isShowEditPopup: false,
      isClick: false,
    };
  }

  componentDidMount() {
    const { getWizard } = this.props;
    getWizard('photo_art');
  }

  componentDidUpdate(prevProps) {
    const {
      material,
      product,
      addOrder,
      orderImages,
      options,
      addImages,
      selectedImage,
    } = this.props;


    const { isClick } = this.state;

    if (orderImages !== prevProps.orderImages && isClick) {
      const {
        quantity: { value: quantity = 0 } = { value: 0 },
        sizeValue,
        colorValue,
        selectedOption,
        isPersonalizeOn,
        personalizeText,
        selectedMatting,
        displayValue: { id: displayId, name: nameDisplay } = {},
      } = options;
      const { id } = selectedImage || {};

      const { framePhotoArtMatting } = product;

      const { frameDisplays } = selectedOption;
      const personalizeActive = frameDisplays.find(display => {
        if (display.frameDisplaysPersonalize) {
          return display.frameDisplaysPersonalize.some(personalizeDisplay => personalizeDisplay.isPersonalizeActive);
        }
        return false;
      }) || {};
      const name = PRODUCT_NAME_PHOTO_ART;
      const newImages = orderImages ? [...orderImages.map(item => item.id), id] : [];
      const {
        id: colorId,
        value,
        framePhotoMattingId,
        label,
      } = selectedMatting || {};
      const order = {
        title: product.displayName,
        typeProduct: name,
        price: selectedOption.price,
        quantity,
        isGift: false,
        productId: selectedOption.parentId,
        material,
        size: sizeValue.value.name,
        color: colorValue.label,
        images: [...newImages],
        options: [
          {
            optionId: selectedOption.id,
            quantity,
            name,
          },
        ],
        customText: isPersonalizeOn ? personalizeText : '',
        isPersonalizeOn: isPersonalizeOn ? personalizeText : '',
        isMatting: Boolean(value),
        framePhotoMattingId,
        framePhotoMattingColorId: colorId,
        mattingPrice: (framePhotoArtMatting && framePhotoArtMatting.price)
          ? framePhotoArtMatting.price : 0,
        mattingName: label,
        photoArtOptions: {
          size: sizeValue.value.name,
          color: colorValue.label,
        },
        display: frameDisplays.length > 1
          ? {
            id: displayId,
            name: nameDisplay,
          }
          : {},
        isPersonalizeActive: Boolean(personalizeActive.id),
        personalize: personalizeActive,
      };
      addImages(orderImages);
      addOrder(order);
      window.location.href = ROUTES.checkout;
      this.clearClickStatus();
    }
  }

  clearClickStatus = () => {
    this.setState({ isClick: false });
  }

  addProduct = () => {
    const {
      croppedImage,
      uploadImages,
      token,
    } = this.props;
    const isCrop = true;
    const formData = new FormData();
    formData.append('files[]', croppedImage, 'cropImage.jpeg');
    formData.append('isCropped', isCrop);
    uploadImages({ formData, token, isCrop });
    this.setState({ isClick: true });
  };

  onChangeName = (e) => {
    this.setState({ newProjectName: e.target.value });
  }

  onSubmit = () => {
    const { newProjectName } = this.state;
    const { saveProject } = this.props;
    if (newProjectName) {
      saveProject(newProjectName);
      this.setState({ newProjectName: '', isShowEditPopup: false });
    }
  }

  editName = () => {
    const { newProjectName } = this.state;
    const { updateProject, token } = this.props;
    if (newProjectName && token) {
      updateProject(newProjectName);
      this.setState({ newProjectName: '', isShowEditPopup: false });
    }
  }

  openEditPopup = () => {
    const { projectName } = this.props;
    this.setState({ isShowEditPopup: true, newProjectName: projectName });
  }

  closeEditPopup = () => {
    this.setState({ isShowEditPopup: false, newProjectName: '' });
  }

  render() {
    const {
      className,
      options,
      selectedImage,
      isProject,
      projectName,
      onlyProjectCreateInMob,
      withoutProject,
      steps,
      croppedImage,
      token,
      isProjectOnMobile,
    } = this.props;

    const currentStep = steps.length ? steps[0] : {};

    const { helpCenters = [] } = currentStep;

    const {
      newProjectName,
      isShowEditPopup,
    } = this.state;

    const {
      quantity: { value: quantity = 0 } = { value: 0 },
      selectedOption,
      isPersonalizeOn,
      selectedMatting,
      personalizeText,
    } = options;

    const {
      value,
      price,
    } = selectedMatting || {};

    const { price: optionPrice, additionalDiscount } = selectedOption || {};

    const oneProductPrice = optionPrice + (value ? price : 0)
      + ((isPersonalizeOn && personalizeText) ? 10 : 0);

    return (
      <div className={b({ mix: className, center: onlyProjectCreateInMob })}>
        <div className={b('help-wrap', { 'hide-in-mobile': withoutProject })}>
          <HelpCenter list={helpCenters} />
        </div>
        <div className={b('price-container', { 'hide-in-mobile': onlyProjectCreateInMob, mobile: isProjectOnMobile })}>
          {!isProjectOnMobile && <DollarIcon className={b('price-icon')} />}
          <span className={b('price-title')}>{PRICE_TITLE}</span>
          <div className={b('price-value-wrapper', { mobile: isProjectOnMobile })}>
            <span className={b('price-value', { mobile: isProjectOnMobile, origin: Boolean(additionalDiscount) })}>{selectedOption ? `$${(oneProductPrice * (quantity || 0)).toFixed(2)}` : ''}</span>
            {additionalDiscount ? (
              <span className={b('price-value', { mobile: isProjectOnMobile })}>
                {selectedOption ? `$${((oneProductPrice - additionalDiscount) * (quantity || 0)).toFixed(2)}` : ''}
              </span>
            ) : null}
          </div>
        </div>
        <div className={b('project-wrap', { 'hide-in-mobile': withoutProject })}>
          {isProject
            ? (
              <div className={b('project-name-block')}>
                <div className={b('first-string')}>
                  <span className={b('project-name')}>{projectName}</span>
                  <button className={b('edit-name-btn')} type="button" onClick={this.openEditPopup}>
                    <EditIcon className={b('edit-icon')} />
                  </button>
                </div>
                <div className={b('second-string')}>
                  <CheckIcon className={b('check-icon')} />
                  <span className={b('info-text')}>{INFO_TEXT}</span>
                </div>
              </div>
            )
            : (
              <div className={b('project-create')}>
                {!token && (
                <span className={b('login-info')}>{LOGIN_INFO_TEXT}</span>
                  )}
                <input
                  className={b('project-create-input')}
                  type="text"
                  placeholder={UNTITLED_PROJECT}
                  onChange={this.onChangeName}
                  value={newProjectName}
                />
                <button
                  className={b('project-create-btn')}
                  type="button"
                  onClick={this.onSubmit}
                >
                  {SAVE_PROJECT}
                </button>
              </div>
            )}
        </div>
        <div className={b('button-wrap', { 'hide-in-mobile': onlyProjectCreateInMob })}>
          <RedButton
            className={b('btn')}
            onClick={this.addProduct}
            disabled={quantity < 1 || !selectedImage || !croppedImage}
            text={ADD_TO_CART}
          />
        </div>
        {isShowEditPopup
          && ReactDOM.createPortal(
            (
              <div className={b('popup-wrap')}>
                <div className={b('popup-block')}>
                  <button className={b('close-btn')} type="button" onClick={this.closeEditPopup}>
                    <CloseIcon2 />
                  </button>
                  <span className={b('popup-title')}>{EDIT_PROJECT}</span>
                  <div className={b('project-edit')}>
                    <input
                      className={b('project-create-input')}
                      type="text"
                      placeholder={UNTITLED_PROJECT}
                      onChange={this.onChangeName}
                      value={newProjectName}
                    />
                    <button
                      className={b('project-edit-btn')}
                      type="button"
                      onClick={this.editName}
                    >
                      {RENAME}
                    </button>
                  </div>
                  <button className={b('create-new-project-btn')} type="button" onClick={this.onSubmit}>
                    {CREATE_NEW_PROJECT}
                  </button>
                </div>
              </div>
            ),
            document.getElementById('__next') || null,
          )}
      </div>
    );
  }
}

const stateProps = state => ({
  typeOrders: ordersTypeSelect(state),
  croppedImage: cropedImageSelector(state),
  orderImages: orderCropImageSelector(state),
  token: getToken(state),
  steps: wizardCardsSelector(state),
});

const actions = {
  addOrder: addToCart,
  uploadImages: loadImages,
  openCart: openCartAction,
  addImages: addCroppedImagesToCart,
  getWizard: getWizardCardsAction,
};

NewPhotoArtSummary.propTypes = propTypes;
NewPhotoArtSummary.defaultProps = defaultProps;

export default connect(stateProps, actions)(NewPhotoArtSummary);
