const TITLE = {
  DATE: 'Date',
  STATUS: 'Status',
  VIEW_ALL: 'View all tracking path',
  TRACK: 'Track',
};

export default TITLE;
