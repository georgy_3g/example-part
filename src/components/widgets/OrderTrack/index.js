import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import bem from 'src/utils/bem';
import format from 'date-fns/format';
import { orderTrackSelect } from 'src/redux/ordersList/selectors';
import { getToken } from 'src/redux/auth/selectors';
import ordersTypeSelect from 'src/redux/orders/selectors';
import styles from './index.module.scss';
import TITLE from './constants';

const { DATE, STATUS, VIEW_ALL, TRACK } = TITLE;

const b = bem('order-track', styles);

const defaultProps = {
  orderTrack: [],
  className: '',
  isWidget: false,
  onClickViewAll: () => {},
};

const propTypes = {
  orderTrack: PropTypes.arrayOf(PropTypes.shape({})),
  className: PropTypes.string,
  isWidget: PropTypes.bool,
  onClickViewAll: PropTypes.func,
};

class OrderTrack extends PureComponent {
  getCarrierUrl = ({ trackingNumber, carrier }) => {
    const carrierUrl = {
      usps: `https://tools.usps.com/go/TrackConfirmAction?tRef=fullpage&tLc=2&text28777=&tLabels=${trackingNumber}`,
      ups: `https://www.ups.com/track?loc=en_RU&tracknum=${trackingNumber}&requester=WT/trackdetails`,
      fedex: `https://www.fedex.com/fedextrack/?trknbr=${trackingNumber}`,
    };
    return carrierUrl[carrier];
  };

  render() {
    const { orderTrack, className, isWidget, onClickViewAll } = this.props;

    if (!orderTrack.length) {
      return null;
    }

    const [first, second, third] = orderTrack;

    const smallArr = [
      ...(first ? [first] : []),
      ...(second ? [second] : []),
      ...(third ? [third] : []),
    ];

    const trackArr = isWidget ? smallArr : orderTrack;

    return (
      <main className={b({ mix: className })}>
        {isWidget && <span className={b('main-title')}>{TRACK}</span>}
        <div className={b('title')}>
          <div className={b('title-date', { small: isWidget })}>{DATE}</div>
          <div className={b('title-status', { small: isWidget })}>{STATUS}</div>
        </div>
        {trackArr.map(({ date, status, extra }, index) => (
          <div key={status} className={b('item')}>
            <div
              className={b('item-date', {
                last: index + 1 === trackArr.length && isWidget,
                small: isWidget,
              })}
            >
              {format(new Date(date), 'mm/dd/yyyy')}
            </div>
            {extra ? (
              <div className={b('item-status')}>
                <a
                  className={b('item-status-link', {
                    last: index + 1 === trackArr.length && isWidget,
                    small: isWidget,
                  })}
                  href={this.getCarrierUrl(extra)}
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  {status}
                </a>
              </div>
            ) : (
              <div
                className={b('item-status', {
                  last: index + 1 === trackArr.length && isWidget,
                  small: isWidget,
                })}
              >
                {status}
              </div>
            )}
          </div>
        ))}
        {isWidget && (
          <button className={b('view-all-btn')} type="button" onClick={onClickViewAll}>
            {VIEW_ALL}
          </button>
        )}
      </main>
    );
  }
}

OrderTrack.propTypes = propTypes;
OrderTrack.defaultProps = defaultProps;

const stateProps = (state) => ({
  orderTrack: orderTrackSelect(state),
  token: getToken(state),
  typeOrders: ordersTypeSelect(state),
});

export default connect(stateProps, null)(OrderTrack);
