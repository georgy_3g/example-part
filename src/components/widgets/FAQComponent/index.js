import React, { useState } from 'react';
import bem from 'src/utils/bem';
import PropTypes from 'prop-types';
import dynamic from 'next/dynamic';
import LazyLoad from 'react-lazyload';
import styles from './index.module.scss';

const SHOW_ALL = 'Show all';
const HIDE = 'Hide';
const ColorButton = dynamic(() => import('src/components/buttons/ColorButton'));
const KnowAbout = dynamic(() => import('src/components/elements/KnowAbout'));

const b = bem('faq-component', styles);

const defaultProps = {
  className: '',
  list: [],
  title: '',
  withoutButton: false,
  description: '',
  isDigitize: false,
  classNameTitle: '',
};

const propTypes = {
  className: PropTypes.string,
  list: PropTypes.arrayOf(PropTypes.shape({})),
  title: PropTypes.string,
  withoutButton: PropTypes.bool,
  description: PropTypes.string,
  isDigitize: PropTypes.bool,
  classNameTitle: PropTypes.string,
};

const FAQComponent = (props) => {
  const {
    className,
    list,
    title,
    withoutButton,
    description,
    isDigitize,
    classNameTitle,
  } = props;

  const [isShowAll, showAll] = useState(false);

  return (
    <div className={b({ mix: className })}>
      <div className={b('about-container')}>
        <h2 className={b('about-title', { mix: classNameTitle })}>{title}</h2>
        {description ? <p className={b('about-description', { digitize: isDigitize })}>{description}</p> : null}
        {list.map(({ desc, text }, index) =>
          index < 5 || isShowAll ? (
            <KnowAbout title={desc} key={desc}>
              <LazyLoad offset={100}>
                <p className={b('about-text')}>{text}</p>
              </LazyLoad>
            </KnowAbout>
          ) : null,
        )}
        {!withoutButton && (
          <ColorButton
            className={b('btn')}
            onClick={() => {
              showAll(!isShowAll);
            }}
            text={isShowAll ? HIDE : SHOW_ALL}
          />
        )}
      </div>
    </div>
  );
}

FAQComponent.propTypes = propTypes;
FAQComponent.defaultProps = defaultProps;

export default FAQComponent;
