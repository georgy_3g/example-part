import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import debounce from 'lodash/debounce';
import bem from 'src/utils/bem';
import formattedImage from 'src/utils/formattedImage';
import ROUTES from 'src/constants/routes';
import Colors from 'src/styles/colors.json';
import comingSoonIcon from 'public/img/shared/version-coming-soon.png';
import dynamic from 'next/dynamic';
import PhotoArtCollections from 'src/components/widgets/PhotoArtCollections';
import { getToken, isMobileDeviceSelector } from 'src/redux/auth/selectors';
import { isDownloadingSelector } from 'src/redux/ordersList/selectors';
import {
  approvedImageErrorSelect,
  isLoadApproveSelector,
} from 'src/redux/approvedOrderImage/selectors';
import {
  frameMaterialsSelector,
  photoArtProductsSelector,
} from 'src/redux/photoArt/selectors';
import {
  addImageListAction,
  createApprovedImage,
  setApprovedImageUrl,
} from 'src/redux/approvedOrderImage/actions';
import { downloadOrderImages, loadOrderImages } from 'src/redux/ordersList/actions';
import { changeImageToCloud } from 'src/redux/myCloud/actions';
import styles from './index.module.scss';
import TERMS from './constants';

const CloseIcon2 = dynamic(() => import('src/components/svg/CloseIcon2'));
const CheckFull = dynamic(() => import('src/components/svg/CheckFull'));
const RequestChangesForm = dynamic(() => import('src/components/forms/RequestChangesForm'));
const RedButton = dynamic(() => import('src/components/buttons/RedButton'));
const ArrowIcon = dynamic(() => import('src/components/svg/ArrowIcon'));
const CompareIcon = dynamic(() => import('src/components/svg/CompareIcon'));
const CreateArtIcon = dynamic(() => import('src/components/svg/CreateArtIcon'));
const ImageComponent = dynamic(() => import('src/components/elements/Image'));
const MinDownloadIcon = dynamic(() => import('src/components/svg/MinDownloadIcon'));
const MinShareIcon = dynamic(() => import('src/components/svg/MinShareIcon'));
const NewMagnifyingPlusIcon = dynamic(() => import('src/components/svg/NewMagnifyingPlusIcon'));
const OrderPrintsIcon = dynamic(() => import('src/components/svg/OrderPrintsIcon'));
const SharePopup = dynamic(() => import('src/components/popups/SharePopup'));


const b = bem('proofs', styles);
const grayColor = Colors['$light-slate-gray-color'];
const greenColor = Colors['$summer-green-color'];
const whiteColor = Colors['$white-color'];
const gray = Colors['$light-slate-gray-color'];
const catskillWhiteColor = Colors['$catskill-white-color'];

const { photoArtEditor } = ROUTES;

const {
  APPROVE_TEXT,
  APPROVE_BTN,
  ID,
  APPROVE,
  MESSAGE_HISTORY,
  IMAGE_FILTER_TYPES,
  IMAGE_TEXTS,
  ORIGINAL,
  COMING_SOON,
  YOUR_VERSIONS,
  APPROVE_VERSION_TITLE,
  REVISION,
  GO_BACK,
  ORIGINAL_TITLE,
  ORIGINAL_SUBTITLE,
  ORIGINAL_TEXT,
  WITHOUT_IMAGE_TITLE,
  WITHOUT_IMAGE_SUBTITLE,
  WITHOUT_IMAGE_TEXT,
  FIRST_APPROVE_BTN,
  MASSAGE_HISTORY_TITLE,
  APPROVED,
  ORDER_ART,
  DOWNLOAD,
  SHARE,
  COMPARE_TEXT,
  CLOSE,
  IMPROVED,
  AWAITING,
  PROCESS,
  ALL,
  APPROVED_TYPE,
  ORDER_PRINTS,
} = TERMS;

const defaultProps = {
  images: [],
  token: '',
  approveImage: () => {  },
  orderId: null,
  getOrderImages: () => {  },
  query: { versionImageId: false },
  setNewUrl: () => {  },
  isComponentShow: true,
  isShowCloudChange: () => {  },
  addCloudFile: () => {  },
  goBack: () => {  },
  updateProjectList: () => {  },
  removeRoom: () => {  },
  isLoadApprove: false,
  approveError: '',
  addImageList: () => {  },
  photoArtProducts: {},
  frameMaterials: [],
};

const propTypes = {
  images: PropTypes.arrayOf(PropTypes.shape({})),
  token: PropTypes.string,
  approveImage: PropTypes.func,
  orderId: PropTypes.number,
  getOrderImages: PropTypes.func,
  query: PropTypes.shape({
    versionImageId: PropTypes.string,
  }),
  setNewUrl: PropTypes.func,
  isComponentShow: PropTypes.bool,
  isShowCloudChange: PropTypes.func,
  addCloudFile: PropTypes.func,
  goBack: PropTypes.func,
  updateProjectList: PropTypes.func,
  removeRoom: PropTypes.func,
  isLoadApprove: PropTypes.bool,
  approveError: PropTypes.string,
  addImageList: PropTypes.func,
  photoArtProducts: PropTypes.shape({}),
  frameMaterials: PropTypes.arrayOf(PropTypes.shape({})),
};


class Proofs extends Component {
  resizeWindow = debounce(() => {
    this.isMobile();
  }, 300);

  constructor(props) {
    super(props);
    const { images } = props;
    const imagesByType = this.getImagesByType(images) || [];

    const selectedImageId = this.getInitImageId(imagesByType);

    const selectedImage = imagesByType.find(item => item.id === selectedImageId);

    const {
      progressType,
      versions = [],
      isApproved,
      versionsWithoutPhoto = [],
    } = selectedImage || {};

    const selectedImageVersionId = this.getInitImageVersionId(progressType, versions);

    const selectedVersionId = this.getInitVersionId(versions, selectedImageVersionId);

    const allVersions = [...(versions || []), ...(versionsWithoutPhoto || [])];

    this.imageToApprove = null;

    this.state = {
      selectedImageId,
      selectedImageVersionId,
      selectedVersionId,
      isRequest: false,
      isShowPopup: false,
      isApproveDisabled: false,
      isHistory: false,
      isShowCollectionPopup: false,
      imageFilterValue: IMAGE_FILTER_TYPES[0],
      isShowOriginal: progressType === PROCESS || isApproved,
      isShowSharePopup: false,
      isShowCompare: false,
      isShowImagePopup: false,
      isScaleMode: false,
      isFullWindow: false,
      versionStep: allVersions.length <= 10
        ? 0
        : Number(((allVersions.length - 10) / 12).toFixed(0)) + 1,
    };
  }

  componentDidMount() {
    window.addEventListener('resize', this.resizeWindow);
    const {
      setNewUrl,
      orderId,
    } = this.props;
    setNewUrl({ order: orderId });
    window.scrollTo({ left: 0, top: 0, behavior: 'smooth' });
    this.isMobile();
  }

  componentDidUpdate(prevProps) {
    const {
      images,
      isLoadApprove: nextIsLoadApprove,
      approveError,
    } = this.props;
    const { selectedImageVersionId: oldVersionId } = this.state;
    const { isLoadApprove, updateProjectList } = prevProps;

    if (!nextIsLoadApprove && isLoadApprove && !approveError) {
      const imagesByType = this.getImagesByType(images);
      const selectedImageId = this.getInitImageId(imagesByType);
      const selectedImage = imagesByType.find(item => item.id === selectedImageId) || {};
      const { progressType, versions } = selectedImage || {};
      const selectedImageVersionId = this.getInitImageVersionId(progressType, versions);
      updateProjectList();
      this.approveIsGood(oldVersionId, selectedImageVersionId);
    } else if (!nextIsLoadApprove && isLoadApprove) {
      this.closeShowPopup();
    }
  }

  componentWillUnmount() {
    const { removeRoom, updateProjectList, setNewUrl } = this.props;
    this.imageToApprove = null;
    setNewUrl({ order: false });
    updateProjectList();
    removeRoom();
    window.removeEventListener('resize', this.resizeWindow);
  }

  approveIsGood = (oldVersionId, selectedImageVersionId) => {
    this.setState({
      selectedImageId: oldVersionId || null,
      selectedImageVersionId,
      isShowOriginal: true,
    });
  }

  isMobile = () => {
    const { isScaleMode } = this.state;

    const isScaleModeNew = window.innerWidth >= 1025 && window.innerWidth <= 1920 ;
    if (isScaleMode !== isScaleModeNew) {
      this.setState({ isScaleMode: isScaleModeNew });
    }
  };

  closeShowPopup = () => {
    this.setState({
      isShowPopup: false,
    });
  }

  getInitImageVersionId = (progressType, versions) => {
    const { query } = this.props;
    const { versionImageId = '' } = query || {};
    if (!versionImageId) {
      return progressType === AWAITING ? versions[versions.length - 1].id : null;
    }
    return Number(versionImageId);
  }

  getInitVersionId = (versions, versionImageId) => {
    const { versionId } = versions.find(({ id }) => id === versionImageId) || {};
    return versionId;
  }

  getInitImageId = (images) => {
    const { query } = this.props;
    const { versionImageId, isEmail } = query || {};
    if (!versionImageId) {
      const [defaultImage] = images;
      const { id: defaultImageId = null } = defaultImage || {};
      const filterImages = isEmail
        ? images.filter(({ isApproved }) => isApproved) || []
        : images.filter(({ isApproved }) => !isApproved) || [];
      return filterImages[0] ? filterImages[0].id : defaultImageId;
    }
    const filterImages = images
      .filter(({ versions = [], isApproved, id }) => (
        isApproved ? id === Number(versionImageId) : versions
          .some(version => version.id === Number(versionImageId))));
    return filterImages[0] ? filterImages[0].id : null;
  }

  selectFrame = (frameData, image) => {
    const { isShowCloudChange, addCloudFile } = this.props;
    const { frameMaterialId } = frameData;
    if (image) {
      addCloudFile(image);
      isShowCloudChange(true, frameData, frameMaterialId || null);
    }
  }

  closeCollectionPopup = () => {
    this.setState({ isShowCollectionPopup: false });
  }

  selectImage = (value) => {
    const imageId = value.id || 0;
    const imageVersionId = value.versions && value.versions.length ? value.versions[0].id : null;
    const versionId = value.versions && value.versions.length ? value.versions[0].versionId : null;

    this.setState({
      selectedImageId: imageId,
      selectedImageVersionId: imageVersionId,
      selectedVersionId: versionId,
      isShowCollectionPopup: false,
      isShowOriginal: value.progressType === PROCESS || value.isApproved,
      versionStep: 0,
      isShowPopup: false,
    });
    this.closeRequest();
    this.closeHistory();
  }

  selectImageVersion = (value) => {
    this.setState({
      selectedImageVersionId: value.id,
      selectedVersionId: value.versionId,
      isShowCollectionPopup: false,
      isShowOriginal: false,
    });
  }

  selectOriginal = () => {
    this.setState({
      isShowCollectionPopup: false,
      isShowOriginal: true,
    });
  }

  togglePopup = () => {
    const { isShowPopup } = this.state;
    this.setState({
      isShowPopup: !isShowPopup,
    });
  }

  openRequest = () => {
    this.setState({ isRequest: true });
  }

  closeRequest = (openChat) => {
    const newState = openChat
      ? { isRequest: false, isHistory: true }
      : { isRequest: false };
    this.setState(newState);
  }

  openHistory = () => {
    this.setState({ isHistory: true });
  }

  closeHistory = () => {
    this.setState({
      isHistory: false,
      isFullWindow: false,
    });
  }

  showMassageHistory = id => () => {
    this.setState({ isRequest: false });
    this.openHistory(id);
  }

  imageApprove = (id) => {
    const {
      approveImage,
      token,
      orderId,
      getOrderImages,
    } = this.props;
    const params = {
      view: 'versioned',
    };
    this.imageToApprove = id;
    approveImage({
      id,
      token,
      orderId,
      params,
      getOrderImages,
    });
  }

  setFullWindow = () => {
    const { isFullWindow } = this.state;
    this.setState({ isFullWindow: !isFullWindow });
  }

  checkImageVersions = () => {
    const {
      selectedImageId,
      selectedImageVersionId,
    } = this.state;
    const { images } = this.props;

    const imagesByType = this.getImagesByType(images);

    const selectedImage = imagesByType.find(item => item.id === selectedImageId);

    const photoNumber = selectedImage
      ? images.findIndex(({ id, versions }) => (id === selectedImage.id
      || versions.some(({ id: idVersion }) => idVersion === selectedImage.id)
      )) : 0;

    const index = selectedImage && (!selectedImage.isApproved
      ? selectedImage.versions.findIndex(item => item.id === selectedImageVersionId)
      : Number(selectedImage.name[selectedImage.name.length - 1]) - 1);

    const selectedImageVersion = (index !== null && index >= 0 && !selectedImage.isApproved)
      ? selectedImage.versions[index]
      : null;

    const parentId = selectedImage ? selectedImage.id : ID;
    const imageUri = selectedImageVersion ? selectedImageVersion.imageUri : '';
    const watermarkedImageUri = selectedImageVersion ? selectedImageVersion.watermarkedImageUri : '';
    const id = selectedImageVersion ? selectedImageVersion.id : '';
    const approvedImage = (selectedImage && selectedImage.versions)
      && selectedImage.versions.find(item => item.isApproved === true);

    const imageVersionTitle = `Photo ${photoNumber + 1} - Version #${index + 1 || ''}`;
    const approveText = `${APPROVE}${imageVersionTitle}`;
    return {
      selectedImageVersion,
      approvedImage,
      imageVersionTitle,
      approveText,
      selectedImage,
      imageUri,
      id,
      parentId,
      watermarkedImageUri,
    };
  }

  getFilterImages = (images) => {
    const { imageFilterValue } = this.state;
    if (imageFilterValue.value === ALL) {
      return images;
    }

    return images.filter(item => item.progressType === imageFilterValue.value);
  }

  getImagesByType = images => (
    images.reduce((acc, item) => {
      if (!item.versions || !item.versions.length) {
        return [...acc, { ...item, progressType: PROCESS }];
      }

      if (item.versions.length && !item.versions.some(itm => itm.isApproved)) {
        return [...acc, { ...item, progressType: AWAITING }];
      }

      if (item.versions.some(itm => itm.isApproved)) {
        const approvedPhoto = item.versions.find(itm => itm.isApproved);
        return [...acc, { ...approvedPhoto, progressType: APPROVED_TYPE }];
      }

      return acc;
    }, [])
  )

  openSharePopup = () => {
    this.setState({ isShowSharePopup: true });
  }

  closeSharePopup = () => {
    this.setState({ isShowSharePopup: false });
  }

  openComparePopup = () => {
    this.setState({ isShowCompare: true });
  }

  closeComparePopup = () => {
    this.setState({ isShowCompare: false });
  }

  openImagePopup = () => {
    this.setState({ isShowImagePopup: true });
  }

  closeImagePopup = () => {
    this.setState({ isShowImagePopup: false });
  }

  getVersionImage = ({
    isApproved,
    imageUri,
    imageKey,
    watermarkedImageUri,
    watermarkedImageKey,

  }) => (
    isApproved
      ? (formattedImage({imagePath: imageKey, needResize: true, imageSize: '900x'}) || imageUri)
      : (formattedImage({imagePath: watermarkedImageKey, needResize :true, imageSize: '900x'}) || watermarkedImageUri)
  );

  getVersionsButtonData = (versions) => {
    const { versionsData: versionsBySteps } = versions
      .reduce(({ versionsData, versionsStep }, item, index) => {
        const stepData = versionsData[versionsStep] || [];
        const isNewStep = index + 1 > 10 + 12 * versionsStep;
        return {
          versionsData: {
            ...versionsData,
            [versionsStep]: [...stepData, ...(isNewStep ? [] : [item])],
            ...(isNewStep ? { [versionsStep + 1]: [item] } : {}),
          },
          versionsStep: isNewStep ? versionsStep + 1 : versionsStep,
        };
      }, { versionsData: {}, versionsStep: 0 });

    return versionsBySteps;
  }

  changeStep = (step) => {
    this.setState({ versionStep: step });
  }

  generateVersionsButtons = (data, isShowOriginal, length) => {
    const { versionStep, selectedImageVersionId } = this.state;
    const { [versionStep]: stepData } = data;
    const lastStep = length <= 10
      ? 0
      : Number(((length - 10) / 12).toFixed(0)) + 1;

    return (
      <div className={b('versions-block-buttons')}>
        {length > 10 && (
          <>
            <button
              className={b(
                'select-step-btn',
                {
                  back: true,
                  hight: versionStep === 0,
                },
              )}
              type="button"
              onClick={() => { this.changeStep(versionStep - 1); }}
            />
            <button
              className={b(
                'select-step-btn',
                { hight: lastStep === versionStep },
              )}
              type="button"
              onClick={() => { this.changeStep(versionStep + 1); }}
            />
          </>
        )}
        {versionStep === 0 && (
          <button
            className={b('original-button', { select: isShowOriginal })}
            type="button"
            onClick={this.selectOriginal}
          >
            {ORIGINAL}
          </button>
        )}
        {Boolean(stepData && stepData.length) && stepData.map((item, index) => (
          <button
            className={b(
              'version-button',
              {
                select: !isShowOriginal && (
                  item.id === selectedImageVersionId
                  || item.versionId === selectedImageVersionId
                ),
              },
            )}
            key={`version_${item.id || item.versionId}`}
            type="button"
            onClick={() => { this.selectImageVersion(item); }}
          >
            {`v${index + 1 + (versionStep > 0 ? 12 * versionStep - 2 : 0)}`}
          </button>
        ))}
      </div>
    );
  }

  goToArtProofs = (images) => {
    const { addImageList, photoArtProducts } = this.props;
    const { selectedImageId } = this.state;

    const selectImage = images.find(({ id }) => selectedImageId === id);
    const noSelectedImage = images.filter(({ id }) => selectedImageId !== id);

    const newImages = [selectImage, ...noSelectedImage];

    const date = new Date();

    const newImagesDate = newImages.map(item => ({
      ...item,
      addedAt: date.toISOString(),
    }))

    const { frames, id: collectionId } = Object.values(photoArtProducts).find(
      ({ name }) => name.toLowerCase() === 'classic',
    ) || Object.values(photoArtProducts)[0];

    const { id: frameId } = frames.find(({ isDefault }) => isDefault)
      || frames[0];

    addImageList(newImagesDate);
    window.location.href = `${photoArtEditor}?collection=${collectionId}&frame=${frameId}&tab=size`;
  }

  orderPrints = (images) => {
    const { addImageList, photoArtProducts, frameMaterials } = this.props;
    const { selectedImageId } = this.state;

    const { id: idCollection } = frameMaterials.find(({ isDefault }) => isDefault)
      || Object.values(frameMaterials)[0];

    const { id: frameId } = photoArtProducts[idCollection].frames.find(({ isDefault }) => isDefault)
      || photoArtProducts[idCollection].frames[0];

    const selectImage = images.find(({ id }) => selectedImageId === id);
    const noSelectedImage = images.filter(({ id }) => selectedImageId !== id);

    const date = new Date();

    const newImages = [selectImage, ...noSelectedImage];
    const newImagesDate = newImages.map(item => ({
      ...item,
      addedAt: date.toISOString(),
    }))

    addImageList(newImagesDate);
    window.location.href = `${photoArtEditor}?collection=${idCollection}&frame=${frameId}&tab=size`;
  }

  render() {
    const {
      images,
      isComponentShow,
      goBack,
      orderId,
      updateProjectList,
    } = this.props;
    const {
      selectedImageId,
      isShowCollectionPopup,
      isShowOriginal,
      selectedImageVersionId,
      isApproveDisabled,
      isShowPopup,
      isRequest,
      isHistory,
      isShowSharePopup,
      isShowCompare,
      isShowImagePopup,
      isFullWindow,
      selectedVersionId,
    } = this.state;

    const {
      selectedImage = {},
      selectedImageVersion,
      imageVersionTitle,
      id,
      parentId,
      approveText,
    } = this.checkImageVersions();
    const { versions = [], versionsWithoutPhoto = [], isApproved } = selectedImage || {};

    const imagesByType = this.getImagesByType(images);
    const imagesByFilter = this.getFilterImages(imagesByType);
    const isAllImagesNull = imagesByFilter.some(({ imageUri }) => imageUri);

    const isVersionWithoutImage = Boolean(selectedImageVersionId
      && (
        !selectedImageVersion
        || (!selectedImageVersion.imageUri && !selectedImageVersion.watermarkedImageUri)
      ));

    const isVersion = !isShowOriginal
      && selectedImageVersionId
      && selectedImageVersion
      && selectedImageVersion.watermarkedImageUri;

    const allVersions = [...versions, ...versionsWithoutPhoto];

    const versionsData = this.getVersionsButtonData(allVersions);

    const imagesByApproved = imagesByFilter.filter(({
      isApproved: isApprovedImage,
    }) => isApprovedImage);
    return (
      <main className={b({ isComponentShow })}>
        {isShowSharePopup && (
          <SharePopup
            data={{ images: selectedImage && selectedImage.fileId ? [selectedImage.fileId] : [] }}
            close={this.closeSharePopup}
          />
        )}
        <div className={b('top-block')}>
          <button className={b('back-btn')} type="button" onClick={goBack}>
            <span className={b('back-icon-wrap')}>
              <ArrowIcon
                className={b('back-icon')}
                width="9"
                height="20"
                strokeWidth="3"
                stroke={gray}
              />
            </span>
            <span className={b('back-text')}>{GO_BACK}</span>
          </button>
        </div>
        <div className={b('content-block', { 'content-block-center': !isAllImagesNull })}>
          {isShowCollectionPopup && (
            <PhotoArtCollections
              className={b('collection-popup')}
              selectedPhotos={[selectedImage]}
              isPopup
              withoutCollectionSelector
              withSlider
              close={this.closeCollectionPopup}
              selectFrame={(value) => {
                this.selectFrame(value, { ...selectedImage });
              }}
              withSelectFunc
            />
          )}
          {isAllImagesNull && (
            <div className={b('images-block')}>
              <div className={b('image-list')}>
                {imagesByFilter.map(item => (
                  <div
                    key={item.id}
                    className={b('image-block')}
                    role="button"
                    tabIndex="0"
                    onClick={() => {
                      this.selectImage(item);
                    }}
                  >
                    <div
                      className={b('image', {
                        selected: item.id === selectedImageId,
                      })}
                      style={{
                        backgroundImage: `url(${
                          formattedImage({
                            imagePath: item.imageKey,
                            needResize: true,
                            imageSize: '150x',
                          }) || item.imageUri
                        })`,
                      }}
                    >
                      <span className={b('image-text', { [item.progressType]: true })}>
                        {IMAGE_TEXTS[item.progressType]}
                      </span>
                    </div>
                    <span className={b('image-name')}>{item.name}</span>
                  </div>
                ))}
              </div>
              {!isApproved && (
                <div className={b('versions-block', { mobile: true })}>
                  <span className={b('versions-block-title')}>{YOUR_VERSIONS}</span>
                  <div className={b('versions-block-buttons')}>
                    <button
                      className={b('original-button', { select: isShowOriginal })}
                      type="button"
                      onClick={this.selectOriginal}
                    >
                      {ORIGINAL}
                    </button>
                    {allVersions.map((item, index) => (
                      <button
                        className={b('version-button', {
                          select: !isShowOriginal && item.id === selectedImageVersionId,
                        })}
                        key={`version_${item.id}`}
                        type="button"
                        onClick={() => {
                          this.selectImageVersion(item);
                        }}
                      >
                        {`v${index + 1}`}
                      </button>
                    ))}
                  </div>
                </div>
              )}
            </div>
          )}
          {!isFullWindow && (
          <div className={b('photo-version-block')}>
            {isShowCollectionPopup && (
              <PhotoArtCollections
                className={b('mobile-collection-popup')}
                selectedPhotos={[selectedImage]}
                isPopup
                withoutCollectionSelector
                withSlider
                close={this.closeCollectionPopup}
                selectFrame={(value) => {
                  this.selectFrame(value, { ...selectedImage });
                }}
                withSelectFunc
              />
            )}
            {(
              (isShowOriginal && selectedImageId && selectedImage && selectedImage.imageUri)
              || (
                selectedImageVersionId
                && selectedImageVersion
                && (selectedImageVersion.imageUri || selectedImageVersion.watermarkedImageUri)
              )
            )
              ? (
                <>
                  <div className={b('block-image')}>
                    <ImageComponent
                      className={b('selected-image')}
                      imageUri={isShowOriginal
                        ? (formattedImage({imagePath: selectedImage.imageKey, needResize: true, imageSize: '900x'}) || selectedImage.imageUri)
                        : this.getVersionImage(selectedImageVersion)}
                      alt={isShowOriginal ? selectedImage.name : selectedImageVersion.name}
                      backgroundColor={catskillWhiteColor}
                    />
                    <div className={b('selected-image-veil')} />
                    <button
                      className={b('zoom-button')}
                      type="button"
                      onClick={this.openImagePopup}
                    >
                      <NewMagnifyingPlusIcon className={b('zoom-icon')} stroke={grayColor} />
                    </button>
                  </div>
                  {isShowImagePopup && ReactDOM.createPortal(
                    (
                      <div className={b('compare-popup')}>
                        <button
                          className={b('close-compare-btn')}
                          type="button"
                          onClick={this.closeImagePopup}
                        >
                          {CLOSE}
                          <CloseIcon2 />
                        </button>
                        <div className={b('image-popup-content')}>
                          <img
                            className={b('selected-popup-image')}
                            src={isShowOriginal
                              ? selectedImage.imageUri
                              : (
                                selectedImageVersion.watermarkedImageUri
                                || selectedImageVersion.imageUri
                              )}
                            alt={isShowOriginal ? selectedImage.name : selectedImageVersion.name}
                            layout="fill"
                          />
                          <div className={b('selected-image-veil')} />
                        </div>
                      </div>
                    ),
                      document.getElementById('__next') || null,
                    )}
                </>
              ) : (
                <div className={b('coming-soon-block')}>
                  <img
                    className={b('coming-soon-image')}
                    src={comingSoonIcon.src}
                    alt="coming soon icon"
                  />
                  <span className={b('coming-soon-text')}>{COMING_SOON}</span>
                </div>
              )}
          </div>
          )}
          {isAllImagesNull && (
            <div
              className={b('control-block', {
                'hide-in-mobile': isShowCollectionPopup,
                'full-width': isFullWindow,
              })}
            >
              {!isApproved && (
                <div className={b('versions-block')}>
                  <span className={b('versions-block-title')}>{YOUR_VERSIONS}</span>
                  {this.generateVersionsButtons(
                    versionsData || {},
                    isShowOriginal,
                    allVersions.length,
                  )}
                </div>
              )}
              {isApproved && (
                <div
                  className={b('approve-info-block', { 'hide-in-mobile': isShowCollectionPopup })}
                >
                  <div className={b('approve-info-block-title-block')}>
                    <div className={b('approve-info-block-icon-wrap')}>
                      <CheckFull
                        className={b('approve-info-block-title-icon')}
                        strokeWidth="3"
                        stroke={greenColor}
                        fill={greenColor}
                        stroke2={whiteColor}
                      />
                    </div>
                    <span className={b('approve-info-block-title')}>{APPROVED}</span>
                  </div>
                  <span className={b('approve-info-block-subtitle')}>{imageVersionTitle}</span>
                  <div className={b('approve-info-button-block')}>
                    <div className={b('approve-info-button-block-top')}>
                      <a
                        className={b('approve-block-button')}
                        href={selectedImage.imageUri}
                        download
                        target="_blank"
                        rel="noopener noreferrer"
                      >
                        <div className={b('button-icon-wrap')}>
                          <MinDownloadIcon className={b('approve-block-icon')} />
                        </div>
                        {DOWNLOAD}
                      </a>
                      <button
                        className={b('approve-block-button')}
                        type="button"
                        onClick={this.openSharePopup}
                      >
                        <div className={b('button-icon-wrap')}>
                          <MinShareIcon className={b('approve-block-icon')} />
                        </div>
                        {SHARE}
                      </button>
                    </div>
                    <div className={b('approve-info-button-block-bottom')}>
                      <button
                        className={b('approve-block-button')}
                        type="button"
                        onClick={() => {
                          this.orderPrints(imagesByApproved);
                        }}
                      >
                        <div className={b('button-icon-wrap')}>
                          <OrderPrintsIcon className={b('approve-block-icon')} />
                        </div>
                        {ORDER_PRINTS}
                      </button>
                      <button
                        className={b('approve-block-button')}
                        type="button"
                        onClick={() => {
                          this.goToArtProofs(imagesByApproved);
                        }}
                      >
                        <div className={b('button-icon-wrap')}>
                          <CreateArtIcon className={b('approve-block-icon')} />
                        </div>
                        {ORDER_ART}
                      </button>
                    </div>
                  </div>
                </div>
              )}
              {isVersion && (
                <>
                  <div className={b('card-content')}>
                    <span className={b('card-name')}>{APPROVE_VERSION_TITLE}</span>
                    <RedButton
                      className={b('image-approve-btn')}
                      onClick={this.togglePopup}
                      disabled={
                        !(
                          selectedImage &&
                          selectedImage.versions &&
                          selectedImage.versions.length
                        ) || isApproveDisabled
                      }
                      text={FIRST_APPROVE_BTN}
                    />
                    <RedButton
                      className={b('image-request-btn')}
                      type="button"
                      onClick={() => this.openRequest(parentId)}
                      disabled={
                        !(
                          selectedImage &&
                          selectedImage.versions &&
                          selectedImage.versions.length
                        ) || isApproveDisabled
                      }
                      text={REVISION}
                    />
                    <button
                      className={b('message-history-button')}
                      type="button"
                      onClick={() => this.openHistory(parentId)}
                      disabled={
                        !(
                          selectedImage &&
                          selectedImage.versions &&
                          selectedImage.versions.length
                        ) || isApproveDisabled
                      }
                    >
                      {MESSAGE_HISTORY}
                    </button>
                    {isShowPopup && (
                      <div className={b('popup-block')}>
                        <div className={b('popup-content')}>
                          <button
                            className={b('close-btn')}
                            type="button"
                            onClick={this.togglePopup}
                          >
                            <span className={b('popup-back-icon')}>
                              <ArrowIcon
                                className={b('back-icon')}
                                width="5"
                                height="11"
                                strokeWidth="3"
                                stroke="#778997"
                              />
                            </span>
                            <span className={b('popup-back-text')}>{GO_BACK}</span>
                          </button>
                          <div className={b('border-block')}>
                            <span className={b('image-approve-title')}>{`${APPROVE}:`}</span>
                            <span className={b('image-approve-subtitle')}>{approveText}</span>
                            <p className={b('popup-text')}>{APPROVE_TEXT}</p>
                            <RedButton
                              className={b('image-approve-btn')}
                              type="button"
                              onClick={() => this.imageApprove(id)}
                              disabled={
                                !(
                                  selectedImage &&
                                  selectedImage.versions &&
                                  selectedImage.versions.length
                                )
                              }
                              text={APPROVE_BTN}
                            />
                          </div>
                        </div>
                      </div>
                    )}
                    {isRequest && (
                      <div className={b('popup-block')}>
                        <div className={b('popup-content')}>
                          <div className={b('request-wrap')}>
                            <RequestChangesForm
                              className={b('request-form')}
                              orderId={orderId}
                              close={this.closeRequest}
                              imageId={id}
                              parentId={parentId}
                              withOutChat={isRequest}
                              subtitle={imageVersionTitle}
                              showMassageHistory={this.showMassageHistory(parentId)}
                              versions={versions}
                              selectedImageVersionId={selectedImageVersionId}
                              selectedVersionId={selectedVersionId}
                              updateProjectList={updateProjectList}
                              isShowPresets={false}
                              isNewVersion
                            />
                          </div>
                        </div>
                      </div>
                    )}
                    {isHistory && (
                      <div className={b('popup-block')}>
                        <div className={b('popup-content')}>
                          <div className={b('request-wrap')}>
                            <RequestChangesForm
                              className={b('request-form')}
                              orderId={orderId}
                              close={this.closeHistory}
                              imageId={id}
                              parentId={parentId}
                              withOutForm={isHistory}
                              showMassageHistory={this.showMassageHistory(parentId)}
                              title={MASSAGE_HISTORY_TITLE}
                              subtitle={imageVersionTitle}
                              versions={versions}
                              openIcon={isFullWindow}
                              changeIcon={this.setFullWindow}
                              selectedImageVersionId={selectedImageVersionId}
                              selectedVersionId={selectedVersionId}
                              updateProjectList={updateProjectList}
                            />
                          </div>
                        </div>
                      </div>
                    )}
                  </div>
                  <button
                    className={b('compare-button')}
                    type="button"
                    onClick={this.openComparePopup}
                  >
                    <CompareIcon className={b('compare-icon')} />
                    {COMPARE_TEXT}
                  </button>
                  {isShowCompare &&
                    ReactDOM.createPortal(
                      <div className={b('compare-popup')}>
                        <button
                          className={b('close-compare-btn')}
                          type="button"
                          onClick={this.closeComparePopup}
                        >
                          <CloseIcon2 />
                        </button>
                        <div className={b('compare-content')}>
                          <div className={b('compare-image-block')}>
                            <img
                              className={b('compare-image')}
                              alt="original"
                              src={selectedImage.imageUri}
                              layout="fill"
                            />
                            <div className={b('selected-image-veil')} />
                            <span className={b('compare-image-name')}>{ORIGINAL}</span>
                          </div>
                          <div className={b('compare-image-block', { version: true })}>
                            <img
                              className={b('compare-image', { version: true })}
                              alt={imageVersionTitle}
                              src={
                                selectedImageVersion.watermarkedImageUri ||
                                selectedImageVersion.imageUri
                              }
                              layout="fill"
                            />
                            <div className={b('selected-image-veil')} />
                            <span className={b('compare-image-name')}>{IMPROVED}</span>
                            <span className={b('compare-image-subtitle')}>{imageVersionTitle}</span>
                          </div>
                        </div>
                      </div>,
                      document.getElementById('__next') || null,
                    )}
                </>
              )}
              {isShowOriginal && !isApproved && (
                <div className={b('original-info-block')}>
                  <span className={b('original-info-title')}>{ORIGINAL_TITLE}</span>
                  <span className={b('original-info-subtitle')}>{ORIGINAL_SUBTITLE}</span>
                  <span className={b('original-info-text')}>{ORIGINAL_TEXT}</span>
                </div>
              )}
              {isVersionWithoutImage && !isShowOriginal && (
                <div className={b('without-image-info-block')}>
                  <span className={b('without-image-info-title')}>{WITHOUT_IMAGE_TITLE}</span>
                  <span className={b('without-image-info-subtitle')}>{WITHOUT_IMAGE_SUBTITLE}</span>
                  <span className={b('without-image-info-text')}>{WITHOUT_IMAGE_TEXT}</span>
                </div>
              )}
            </div>
          )}
        </div>
      </main>
    );
  }
}

Proofs.propTypes = propTypes;
Proofs.defaultProps = defaultProps;

const stateProps = state => ({
  token: getToken(state),
  isDownloading: isDownloadingSelector(state),
  isLoadApprove: isLoadApproveSelector(state),
  approveError: approvedImageErrorSelect(state),
  photoArtProducts: photoArtProductsSelector(state),
  frameMaterials: frameMaterialsSelector(state),
  isMobileDevice: isMobileDeviceSelector(state),
});

const actions = {
  approveImage: createApprovedImage,
  setImage: setApprovedImageUrl,
  getOrderImages: loadOrderImages,
  downloadImages: downloadOrderImages,
  addCloudFile: changeImageToCloud,
  addImageList: addImageListAction,
};

export default connect(stateProps, actions)(Proofs);
