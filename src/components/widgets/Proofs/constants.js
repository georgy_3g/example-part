const TERMS = {
  MESSAGE:
    'Your restored images are ready for review! Have a look at your images and let us know if you approve or request changes.',
  RESTORATION_TEXT: 'Your restoration images',
  APPROVE_TEXT:
    'By approving this photo enhancement,\nyou agree that we have completed the\nwork to your satisfaction and will no\nlonger require any additional changes.',
  APPROVE_BTN: 'I approve',
  FIRST_APPROVE_BTN: 'Approve this version',
  REQUEST_CHANGES: 'Request changes',
  IMG: 'public/img #',
  APPROVE: 'Approve',
  ID: 'id',
  COMPARE: 'Compare with the original',
  APPROVED_ON: 'Approved on ',
  DOWNLOAD_PHOTOS: 'Download Photos',
  DOWNLOAD_PHOTO: 'Download Photo',
  ORDER_PHOTO_ART: 'Order photo art',
  MESSAGE_HISTORY: 'View message history',
  ORIGINAL: 'Original',
  WITHOUT_VERSION_TITLE: 'Your proof is coming soon.',
  WITHOUT_VERSION_TEXT:
    'Our artists are super busy working on your photo and\nwe’ll email you the moment it is ready to preview.',
  STEPS: [
    {
      name: 'In process',
      id: 1,
    },
    {
      name: 'Awaiting approval',
      mobileName: 'To approve',
      id: 2,
    },
    {
      name: 'Approved',
      id: 3,
    },
  ],
  IN_PROCESS_TITLE: 'All good things take time.',
  APPROVE_INFO_TEXT: 'After you approve your photo, you can no longer request further changes.',
  CONFIRM_TEXT: 'Please confirm your approval.',
  LAST_STEP_TITLE: 'You’re almost there!',
  LAST_STEP_TEXT: 'First you must approve your enhancement.',
  LAST_STEP_TEXT2:
    'Then you can download your photo and create\nsomething truly extraordinary from one of our\nbeautiful photo art collections.',
  CREATE_PHOTO_ART: 'Create photo art',
  IMAGE_FILTER_TYPES: [
    {
      value: 'all',
      label: 'All photos',
    },
    {
      value: 'process',
      label: 'In process',
    },
    {
      value: 'awaiting',
      label: 'Awaiting',
    },
    {
      value: 'approved',
      label: 'Approved',
    },
  ],
  IMAGE_TEXTS: {
    process: 'In process',
    awaiting: 'Awaiting',
    approved: 'Approved',
  },
  COMING_SOON: 'Coming soon',
  YOUR_VERSIONS: 'Your versions',
  APPROVE_VERSION_TITLE: 'Approve your photo or message us for revisions',
  MESSAGE_US: 'Message us',
  GO_BACK: 'Go back',
  ORIGINAL_TITLE: 'This is your',
  ORIGINAL_SUBTITLE: 'original photo',
  ORIGINAL_TEXT:
    'This is your original photo, to see\nimproved versions of this photo\nselect from the box above.',
  WITHOUT_IMAGE_TITLE: 'All good things\ntake time.',
  WITHOUT_IMAGE_SUBTITLE: 'Your proof is coming soon.',
  WITHOUT_IMAGE_TEXT:
    'Our artists are super busy working\non your photo and we’ll email you\nthe moment it is ready to preview.',
  MASSAGE_HISTORY_TITLE: 'Your message history',
  APPROVED: 'Approved!',
  ORDER_ART: 'Create art',
  DOWNLOAD: 'Download',
  SHARE: 'Share',
  COMPARE_TEXT: 'Compare to original',
  CLOSE: 'Close',
  IMPROVED: 'Improved',
  AWAITING: 'awaiting',
  PROCESS: 'process',
  ALL: 'all',
  APPROVED_TYPE: 'approved',
  ORDER_PRINTS: 'Order prints',
  REVISION: 'Request a revision',
};

export default TERMS;
