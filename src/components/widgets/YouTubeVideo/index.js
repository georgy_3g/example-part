import React, { Component } from 'react';
import PropTypes from 'prop-types';
import YouTube from 'react-youtube';
import bem from 'src/utils/bem';
import styles from './index.module.scss';

const b = bem('youtube-video', styles);

const opts = {
  width: '100%',
  height: '100%',
  borderRadius: '10px',
};

const propTypes = {
  contentIframe: PropTypes.string,
  clickPause: PropTypes.func,
  className: PropTypes.string,
  autoPlay: PropTypes.bool,
};
const defaultProps = {
  contentIframe: '/ ?',
  clickPause: () => {},
  className: '',
  autoPlay: false,
};
class YouTubeVideo extends Component {
  constructor(props) {
    super(props);
    const { contentIframe } = this.props;
    const regexFindId = /\/[A-Za-z0-9_-]+[?|"]/;
    const [id] = contentIframe.match(regexFindId);
    const idNoSymbols = id.slice(1, id.length - 1);
    this.id = idNoSymbols;
  }

  componentWillUnmount() {
    this.id = null;
  }

  onReady = (e) => {
    const { autoPlay } = this.props;
    if (autoPlay) {
      e.target.playVideo();
    }
  };

  render() {
    const { clickPause, className } = this.props;

    return (
      <YouTube
        videoId={this.id}
        onReady={this.onReady}
        onPlay={clickPause}
        containerClassName={b('iframe-block')}
        opts={opts}
        className={className}
      />
    );
  }
}

YouTubeVideo.propTypes = propTypes;
YouTubeVideo.defaultProps = defaultProps;

export default YouTubeVideo;
