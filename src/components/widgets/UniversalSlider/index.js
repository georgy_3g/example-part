import React from 'react';
import PropTypes from 'prop-types';
import Slider from 'react-slick';
import bem from 'src/utils/bem';
import Colors from 'src/styles/colors.json';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import FrameCard from 'src/components/cards/FrameCard';
import PopupFrameCard from 'src/components/cards/PopupFrameCard';
import dynamic from "next/dynamic";
import styles from './index.module.scss';

const NewArrowSlider = dynamic(() => import('src/components/elements/NewArrowSlider'));
const Slide = dynamic(() => import('src/components/elements/Slide'));
const MiniPhotoCard = dynamic(() => import('src/components/cards/MiniPhotoCard'));
const SliderPhotoCard = dynamic(() => import('src/components/cards/SliderPhotoCard'));
const PresentationCard = dynamic(() => import('src/components/cards/PresentationCard'));
const ImageCard = dynamic(() => import('src/components/cards/ImageCard'));
const ArrowSlider = dynamic(() => import('src/components/elements/ArrowSlider'));
const MenuCard = dynamic(() => import('src/components/cards/MenuCard'));
const PhotoImageCard = dynamic(() => import('src/components/cards/PhotoImageCard'));

const white = Colors['$white-color'];

const b = bem('universal-slider', styles);
function ArrowNext(props) {
  return <ArrowSlider prefix="next" block={b} {...props} />;
}
function ArrowPrev(props) {
  return <ArrowSlider prefix="prev" block={b} {...props} />;
}

function NewArrowNext(props) {
  return <NewArrowSlider prefix="next" block={b} stroke={white} {...props} />;
}
function NewArrowPrev(props) {
  return <NewArrowSlider prefix="prev" block={b} stroke={white} {...props} />;
}

const propTypes = {
  cardType: PropTypes.string,
  selected: PropTypes.oneOfType([PropTypes.number, PropTypes.shape({})]),
  select: PropTypes.func,
  cards: PropTypes.arrayOf(PropTypes.shape({})),
  fullSHow: PropTypes.number,
  tabletShow: PropTypes.number,
  middleShow: PropTypes.number,
  mediumShow: PropTypes.number,
  smallShow: PropTypes.number,
  miniSHow: PropTypes.number,
  withoutText: PropTypes.bool,
  deleteImageFromSlider: PropTypes.func,
  className: PropTypes.string,
  infinite: PropTypes.bool,
  refFunc: PropTypes.func,
  withoutArrows: PropTypes.bool,
  autoplay: PropTypes.bool,
  slidesToScroll: PropTypes.number,
  pauseOnHover: PropTypes.bool,
  wrapClass: PropTypes.string,
  onImageError: PropTypes.func,
  variableWidth: PropTypes.bool,
  withAutoWidth: PropTypes.bool,
  imageData: PropTypes.shape({}),
  withOutHints: PropTypes.bool,
  focusOnSelect: PropTypes.bool,
  withDeleteBtn: PropTypes.bool,
  withCrop: PropTypes.bool,
  isEnhanceProduct: PropTypes.bool,
  withOutSelect: PropTypes.bool,
  selectedArr: PropTypes.arrayOf(PropTypes.shape({})),
  multiSelect: PropTypes.bool,
  withDownload: PropTypes.bool,
  withOutStorePhoto: PropTypes.bool,
  isNewArrow: PropTypes.bool,
  imageClass: PropTypes.string,
  isResponsiveIcon: PropTypes.bool,
  isLazyLoad: PropTypes.bool,
};
const defaultProps = {
  cardType: 'slide',
  selected: null,
  select: () => {},
  cards: [],
  fullSHow: 6,
  middleShow: 5,
  tabletShow: 4,
  mediumShow: 3,
  smallShow: 2,
  miniSHow: 1,
  withoutText: false,
  deleteImageFromSlider: () => {},
  className: '',
  infinite: false,
  refFunc: () => {},
  withoutArrows: false,
  autoplay: false,
  slidesToScroll: 1,
  pauseOnHover: false,
  wrapClass: '',
  onImageError: () => {},
  variableWidth: false,
  withAutoWidth: false,
  imageData: null,
  withOutHints: false,
  focusOnSelect: true,
  withDeleteBtn: false,
  withCrop: false,
  isEnhanceProduct: false,
  withOutSelect: false,
  selectedArr: [],
  multiSelect: false,
  withDownload: false,
  withOutStorePhoto: false,
  isNewArrow: false,
  imageClass: '',
  isResponsiveIcon: false,
  isLazyLoad: true,
};
function UniversalSlider(props) {
  const {
    cardType,
    select,
    selected,
    cards,
    fullSHow,
    middleShow,
    mediumShow,
    smallShow,
    tabletShow,
    miniSHow,
    withoutText,
    deleteImageFromSlider,
    className,
    infinite,
    refFunc,
    withoutArrows,
    autoplay,
    slidesToScroll,
    pauseOnHover,
    wrapClass,
    onImageError,
    variableWidth,
    imageData,
    withOutHints,
    focusOnSelect,
    withDeleteBtn,
    withCrop,
    isEnhanceProduct,
    withOutSelect,
    selectedArr,
    multiSelect,
    withDownload,
    withOutStorePhoto,
    isNewArrow,
    imageClass,
    isResponsiveIcon,
    isLazyLoad,
  } = props;
  const components = {
    slide: Slide,
    miniPhoto: MiniPhotoCard,
    presentationCard: PresentationCard,
    frame: FrameCard,
    sliderPhoto: SliderPhotoCard,
    menu: MenuCard,
    photo: PhotoImageCard,
    image: ImageCard,
    'popup-frame': PopupFrameCard,
  };
  const SlideComponent = components[cardType];
  const settings = {
    dots: false,
    arrows: !withoutArrows,
    autoplay,
    infinite,
    focusOnSelect,
    lazyLoad: isLazyLoad,
    speed: 500,
    slidesToShow: fullSHow,
    slidesToScroll,
    responsive: [
      {
        breakpoint: 1600,
        settings: {
          slidesToShow: middleShow,
          slidesToScroll: 1,
          variableWidth: false,
        },
      },
      {
        breakpoint: 1400,
        settings: {
          slidesToShow: tabletShow,
          slidesToScroll: 1,
          variableWidth: false,
        },
      },
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: mediumShow,
          slidesToScroll: 1,
          variableWidth: false,
        },
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: smallShow,
          slidesToScroll: 1,
          variableWidth: false,
        },
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: miniSHow,
          slidesToScroll: 1,
          variableWidth: false,
        },
      },
    ],
    nextArrow: isNewArrow ? <NewArrowNext /> : <ArrowNext />,
    prevArrow: isNewArrow ? <NewArrowPrev /> : <ArrowPrev />,
    pauseOnHover,
    variableWidth,
  };

  return (
    <div className={b({ mix: className, 'new-arrows': isNewArrow })} ref={refFunc}>
      <Slider {...settings}>
        {cards.map((item, index) => (
          <div className={b('slide-wrapper', { mix: wrapClass })} key={`${cardType}_${item.id}`}>
            <SlideComponent
              data={item}
              select={select}
              selected={selected}
              isSelected={item.isSelected || false}
              className={b('slide', { mix: imageClass })}
              index={index}
              withoutText={withoutText}
              deleteImageFromSlider={deleteImageFromSlider}
              onImageError={onImageError}
              imageData={imageData}
              withOutHints={withOutHints}
              withDeleteBtn={withDeleteBtn}
              withCrop={withCrop}
              isEnhanceProduct={isEnhanceProduct}
              withOutSelect={withOutSelect}
              selectedArr={selectedArr}
              multiSelect={multiSelect}
              withDownload={withDownload}
              withOutStorePhoto={withOutStorePhoto}
              isResponsiveIcon={isResponsiveIcon}
            />
          </div>
        ))}
      </Slider>
    </div>
  );
}
UniversalSlider.propTypes = propTypes;
UniversalSlider.defaultProps = defaultProps;
export default UniversalSlider;
