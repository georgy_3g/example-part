const AUDIO = 'audio';
const VIDEO = 'video';
const CLOSE_TEXT = 'Close';
const PLAY = 'Play';
const PAUSE = 'Pause';

export { AUDIO, VIDEO, CLOSE_TEXT, PLAY, PAUSE };
