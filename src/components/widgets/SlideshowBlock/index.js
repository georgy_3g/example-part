import React, { Component } from 'react';
import PropTypes from 'prop-types';
import bem from 'src/utils/bem';
import AwesomeSlider from 'react-awesome-slider';
import Image from 'next/image';
import audioImage from  'public/img/account/audio.png';
import playIcon from  'public/img/shared/PLAY.png';
import pauseIcon from  'public/img/shared/PAUSE.png';
import dynamic from "next/dynamic";
import AwesomeSliderStyles from './fold-out-animation.module.scss';
import customStyles from './customStyles.module.scss';
import CoreStyles from './coreStyles.module.scss';
import './awesome-slyder.module.css';
import styles from './index.module.scss';
import {
  AUDIO,
  VIDEO,
  CLOSE_TEXT,
  PLAY,
  PAUSE,
} from './constants';

const CloseIcon2 = dynamic(() => import('src/components/svg/CloseIcon2'));
const ArrowSlideShowIcon = dynamic(() => import('src/components/svg/ArrowSlideShowIcon'));

const b = bem('slideshow-block', styles);
const propTypes = {
  cards: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number,
      imageUri: PropTypes.string,
      videoUri: PropTypes.string,
      audioUrl: PropTypes.string,
    }),
  ),
  firstSlideIndex: PropTypes.number,
  interval: PropTypes.number,
  close: PropTypes.func,
};

const defaultProps = {
  cards: [],
  firstSlideIndex: 0,
  interval: 1500,
  close: () => {},
};

class SlideshowBlock extends Component {
  constructor(props) {
    super(props);

    const { firstSlideIndex, cards } = props;

    this.players = {};
    this.currentIndex = firstSlideIndex;

    this.state = {
      isPlay: cards && cards.length > 1,
      selectedSlide: firstSlideIndex,
    };
  }

  componentDidMount() {
    const { isPlay, selectedSlide } = this.state;
    const { cards } = this.props;
    const { imageUri } = cards[selectedSlide];
    if (isPlay && imageUri) {
      this.setTimer(selectedSlide);
    }

    document.addEventListener('keydown', this.clickFunc);
  }

  componentWillUnmount() {
    document.removeEventListener('keydown', this.clickFunc);
  }

  clickFunc = (e) => {
    const { code, key } = e;

    if (key === ' ' || code === 'Space') {
      this.clickSpace();
    }

    if (key === 'ArrowRight' || code === 'ArrowRight') {
      this.clickNext(this.currentIndex);
    }

    if (key === 'ArrowLeft' || code === 'ArrowLeft') {
      this.clickPrev();
    }
  };

  clickSpace = () => {
    const { isPlay } = this.state;
    const { cards } = this.props;
    const { imageUri } = cards[this.currentIndex];
    this.setState({ isPlay: !isPlay }, () => {
      if (!isPlay && imageUri) {
        this.setTimer(this.currentIndex);
      }
    });
  };

  refFunc = () => {
    const [nextButton = null] = document.getElementsByClassName('customStyles_awssld__next__Hc_HO');
    const [prevButton = null] = document.getElementsByClassName('customStyles_awssld__prev__oZ3iN');
    this.nextButton = nextButton;
    this.prevButton = prevButton;

  };

  clickNext = (index, auto) => {
    const { isPlay } = this.state;
    if (this.nextButton && this.currentIndex === index && (!auto || (auto && isPlay))) {
      this.nextButton.click();
    }
  };

  clickPrev = () => {
    if (this.prevButton) {
      this.prevButton.click();
    }
  };

  setTimer = (index) => {
    const { interval } = this.props;
    setTimeout(() => {
      this.clickNext(index, true);
    }, interval);
  };

  setRef = (e, id, type, index) => {
    this.players[`${type}_${id}`] = e;
    if (index === 0 && e) {
      e.play();
    }
  };

  onTransitionStart = (e) => {
    const { currentIndex } = e;
    const { cards } = this.props;
    const currentItem = cards[currentIndex];
    const { audioUri, videoUri, id } = currentItem || {};
    if (audioUri && this.players[`${AUDIO}_${id}`]) {
      this.players[`${AUDIO}_${id}`].pause();
    }
    if (videoUri && this.players[`${VIDEO}_${id}`]) {
      this.players[`${VIDEO}_${id}`].pause();
    }
  };

  onTransitionEnd = (e) => {
    const { currentIndex } = e;
    const { cards } = this.props;
    const { isPlay } = this.state;
    const currentItem = cards[currentIndex];
    const { audioUri, videoUri, imageUri, id } = currentItem || {};
    if (audioUri && this.players[`${AUDIO}_${id}`]) {
      this.players[`${AUDIO}_${id}`].play();
    }

    if (videoUri && this.players[`${VIDEO}_${id}`]) {
      this.players[`${VIDEO}_${id}`].play();
    }

    if (isPlay && imageUri) {
      this.setTimer(currentIndex);
    }
    this.currentIndex = currentIndex;
  };

  render() {
    const {
      cards,
      close,
    } = this.props;
    const { isPlay, selectedSlide } = this.state;

    return (
      <div className={b()} ref={this.refFunc}>
        <div className={b('close-wrap')}>
          <div className={b('close-text')}>{CLOSE_TEXT}</div>
          <button className={b('close-btn')} type="button" onClick={close}>
            <CloseIcon2 />
          </button>
        </div>
        <div className={b('play-btn-wrap')}>
          <button className={b('play-btn')} type="button" onClick={this.clickSpace}>
            <Image
              className={b('play-icon')}
              src={!isPlay ? playIcon : pauseIcon}
              alt="play/pause"
              width='100%'
              height='100%'
            />
          </button>
          <div className={b('play-text')}>{!isPlay ? PLAY : PAUSE}</div>
        </div>
        <AwesomeSlider
          play={isPlay}
          selected={selectedSlide}
          cancelOnInteraction={false}
          cssModule={[customStyles, CoreStyles, AwesomeSliderStyles]}
          onTransitionStart={this.onTransitionStart}
          onTransitionEnd={this.onTransitionEnd}
          bullets={false}
          buttonContentRight={<ArrowSlideShowIcon className={b('right')} />}
          buttonContentLeft={<ArrowSlideShowIcon className={b('left')} />}
          organicArrows={false}
        >
          {Boolean(cards && cards.length) &&
            cards.map(({ id, imageUri, videoUri, audioUri }, index) => {
              if (audioUri) {
                return (
                  <div
                    className={b('audio-wrapper')}
                    data-src={audioImage}
                    key={`slideshow${id}`}
                  >
                    <audio
                      className={b('audio')}
                      width="100%"
                      controls
                      ref={(e) => {
                        this.setRef(e, id, AUDIO, index);
                      }}
                      onEnded={() => {
                        this.clickNext(index);
                      }}
                    >
                      <source src={audioUri} />
                      <track kind="captions" />
                    </audio>
                  </div>
                );
              }
              if (videoUri) {
                return (
                  <div className={b('video-wrapper')} key={`slideshow${id}`}>
                    <video
                      className={b('video')}
                      width="100%"
                      controls
                      ref={(e) => {
                        this.setRef(e, id, VIDEO, index);
                      }}
                      onEnded={() => {
                        this.clickNext(index);
                      }}
                    >
                      <source src={videoUri} />
                      <track kind="captions" />
                    </video>
                  </div>
                );
              }
              return (
                <div
                  className={b('image-wrapper')}
                  data-src={imageUri || videoUri}
                  key={`slideshow${id}`}
                />
              );
            })}
        </AwesomeSlider>
      </div>
    );
  }
}

SlideshowBlock.propTypes = propTypes;
SlideshowBlock.defaultProps = defaultProps;

export default SlideshowBlock;
