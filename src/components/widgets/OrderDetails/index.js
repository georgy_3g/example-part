import React, { Component, createRef } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Formik } from 'formik';
import * as Yup from 'yup';
import bem from 'src/utils/bem';
import formattedOrderDetails from 'src/utils/formattedOrderDetails';
import formattedToCardNumber from 'src/utils/formattedToCardNumber';
import toCardExpiry from 'src/utils/toCardExpiry';
import cleanCardNumber from 'src/utils/cleanCardNumber';

import { AE_CARD_MASK, CARD_EXPIRATION_DATE, CARD_MASK } from 'src/constants';
import Color from 'src/styles/colors.json';
import PaypalButton from 'src/components/elements/PaypalButton';

import inputCardIcon from 'public/img/shared/inputCardIcon.png';
import dynamic from 'next/dynamic';
import { orderDetailsSelect, requestChangesStatusSelector } from 'src/redux/ordersList/selectors';
import { chatTokenSelector, getToken, userIdSelector } from 'src/redux/auth/selectors';
import ordersTypeSelect from 'src/redux/orders/selectors';
import {
  paymentErrorSelector,
  paymentFailSelector,
  spinnerFlagSelector,
} from 'src/redux/orderCreate/selectors';
import { userPaymentInfoSelector } from 'src/redux/shared/selectors';
import { orderPriceSelect } from 'src/redux/prices/selectors';
import { createOrderMessage, getOrderInvoice } from 'src/redux/ordersList/actions';
import { addToCart } from 'src/redux/orders/actions';
import { orderClear, orderPayment, orderPaymentFailed } from 'src/redux/orderCreate/actions';
import { addPhotoArtReorderImages } from 'src/redux/orderImages/actions';
import { getPromoSection } from 'src/redux/promo/actions';
import { base } from 'src/config';
import SocketIo from 'socket.io-client';
import styles from './index.module.scss';
import { TERMS } from './constants';

const NewCustomInput = dynamic(() => import('src/components/inputs/NewCustomInput'));
const PayPalIcon = dynamic(() => import('src/components/svg/PayPalIcon'));
const OrderDetailsCard = dynamic(() => import('src/components/cards/OderDetailsCard'));
const ModalContainer = dynamic(() => import('src/components/modals/ModalContainer'));
const ColorButton = dynamic(() => import('src/components/buttons/ColorButton'));
const DocumentIcon = dynamic(() => import('src/components/svg/DocumentIcon'));
const OrderTrack = dynamic(() => import('src/components/widgets/OrderTrack'));
const CardPayIcon = dynamic(() => import('src/components/svg/CardPayIcon'));
const RedButton = dynamic(() => import('src/components/buttons/RedButton'));
const SpinnerBlock = dynamic(() => import('src/components/elements/SpinnerBlock'));
const BlueButton = dynamic(() => import('src/components/buttons/BlueButton'));
const MailIcon = dynamic(() => import('src/components/svg/MailIcon'));
const ChatRoom = dynamic(() => import('src/components/popups/ChatRoom'));
const CloseIcon2 = dynamic(() => import('src/components/svg/CloseIcon2'));


const b = bem('order-details', styles);
const port = base || 'http://34.207.168.203:5050';
const cardValidator = require('card-validator');

const orangeColor = Color['$burnt-sienna-color'];
const blue = Color['$pickled-bluewood-color'];

const {
  CURRENCY,
  RUSH_SUB_TOTAL,
  GIFT_SUB_TOTAL,
  FLORIDA_SALE_TAX,
  TOTAL,
  BALANCE_DUE,
  SHIPPING_SUB_TOTAL,
  MESSAGE_US,
  PAY,
  PRINT_INVOICE,
  BANK_CARD,
  BANK_CARD_TEXT,
  CARD_CVC,
  CARD_EXPIRY,
  CARD_INFO,
  CARD_NUMBER,
  PAY_NOW,
  PAYPAL,
  PAYPAL_TEXT,
  NO_ORDERS,
  AMOUNT_PAID,
  PAYMENT_METHOD,
  SAVED_BANK_CARD,
  DISCOUNT,
  SUB_TOTAL,
  AE_CARD_TYPE,
} = TERMS;

const defaultProps = {
  query: {},
  orderDetails: {
    id: '',
    items: [],
  },
  downloadOrderInvoice: () => {  },
  token: '',
  typeOrders: {},
  registrationPayment: () => {  },
  spinnerFlag: false,
  creditCard: {},
  paymentFail: false,
  paymentError: '',
  clearOrderError: () => {  },
  changeScene: () => {  },
  setPaymentFailed: () => {  },
  setNewUrl: () => {  },
  chatToken: '',
  currency: '$',
  selectPrice: {},
  getPromo: () => {  },
};

const propTypes = {
  query: PropTypes.shape({}),
  currency: PropTypes.string,
  selectPrice: PropTypes.shape({}),
  orderDetails: PropTypes.shape({
    id: PropTypes.number,
    items: PropTypes.arrayOf(PropTypes.shape({})),
    orderPayments: PropTypes.arrayOf(PropTypes.shape({})),
    isQuickly: PropTypes.bool,
    orderShippingAddress: PropTypes.shape({}),
    discountAmount: PropTypes.number,
    coupon: PropTypes.shape({}),
    promoCode: PropTypes.objectOf(PropTypes.shape({
      entitledProductIds: PropTypes.arrayOf(PropTypes.shape({})),
      valueType: PropTypes.string,
      targetType: PropTypes.string,
      value: PropTypes.number,
      isGiftCard: PropTypes.bool,
      sOrderIncludeAllProducts: PropTypes.bool,
    })),
    taxAmount: PropTypes.number,
    calculated: PropTypes.shape({}),
  }),
  downloadOrderInvoice: PropTypes.func,
  token: PropTypes.string,
  chatToken: PropTypes.string,
  typeOrders: PropTypes.shape({}),
  registrationPayment: PropTypes.func,
  spinnerFlag: PropTypes.bool,
  creditCard: PropTypes.shape({
    cardNumber: PropTypes.string,
    cardType: PropTypes.string,
    id: PropTypes.number,
  }),
  paymentFail: PropTypes.bool,
  paymentError: PropTypes.string,
  clearOrderError: PropTypes.func,
  setPaymentFailed: PropTypes.func,
  changeScene: PropTypes.func,
  setNewUrl: PropTypes.func,
  getPromo: PropTypes.func,
};

class OrderDetails extends Component {
  constructor(props) {
    super(props);

    const { creditCard, chatToken } = props;
    const { cardNumber } = creditCard || {};
    this.messageList = createRef();
    this.socket = SocketIo(port, { path: '/socket.io', query: { auth: chatToken }});
    this.state = {
      isPayment: false,
      generalRoom: null,
      rooms: [],
      paymentMethod: cardNumber ? PAYMENT_METHOD.savedCard : PAYMENT_METHOD.card,
      validationSchema: Yup.object().shape({ }),
      cvvMaxLength: 3,
      typeCard: '',
    };

  }

  componentDidMount() {
    const {
      clearOrderError,
      query,
      getPromo,
      orderDetails,
    } = this.props;
    const { open } = query || {};
    const { id } = orderDetails || {};
    clearOrderError();
    getPromo({ key: 'account?main=orderDetails' });
    this.socket.on('room', ({ data }) => {
      this.setState({ rooms: data }, () => {
        this.saveRoom();
      });
    })
    if (id) {
      this.getRooms();
    }
    if (open) {
      this.choosePayment();
    }
  }

  componentDidUpdate(prevProps) {
    const { orderDetails } = this.props;
    const { orderDetails: prevOrderDetails } = prevProps;
    if (prevOrderDetails?.id !== orderDetails?.id) {
      this.getRooms();
    }
  }

  componentWillUnmount() {
    this.socket.emit('disconnect');
    this.socket = null;
    this.messageList = null;
  }

  requestOpen = (bool) => {
    const { generalRoom, rooms } = this.state;

    if(!rooms.length || !generalRoom){
      this.getRooms();
    }

    this.setState({ messageOpen: bool });
  }

  getRooms = () => {
    const { orderDetails } = this.props;
    const { id } = orderDetails || {};
    this.socket.emit('getRooms', { orderId: id })
  }

  scrollMessageListToBottom = () => {
    if (this.messageList.current) {
      this.messageList.current.scrollTop = this.messageList.current.scrollHeight - this.messageList.current.clientHeight;
    }
  }

  saveRoom = () => {
    const generalRoom = this.getGeneralRoom();
    this.setState({ generalRoom });
  }

  getGeneralRoom = () => {
    const { orderDetails } = this.props;
    const { rooms } = this.state;
    const { id } = orderDetails || {};
    return rooms.find(({ type, orderId }) => type === 'general' && orderId === Number(id)) || {};
  }

  onDownload = id => () => {
    const { downloadOrderInvoice, token } = this.props;
    downloadOrderInvoice({ id, token });
  }

  choosePayment = () => {
    const { setNewUrl } = this.props;
    const { isPayment } = this.state;
    this.setState({ isPayment: !isPayment });
    setNewUrl({ open: false });
  }

  handlePayment = (paymentMethod, values) => () => {
    this.cardOnChange({ values, paymentMethod });
    this.setState({ paymentMethod });
  }

  onSubmitPayment = (values) => {
    const { paymentMethod } = this.state;
    const {
      registrationPayment,
      token,
      orderDetails: { id },
      creditCard: { id: cardId } = {},
    } = this.props;
    const {
      cardExpirationDate,
      cardNumber,
      cardCode,
    } = values;
    registrationPayment({
      token,
      id,
      type: paymentMethod,
      cardId,
      cardInfo: {
        cardExpirationDate: toCardExpiry(cardExpirationDate),
        cardNumber: cleanCardNumber(cardNumber),
        cardCode,
      },
    });
  }

  closePopup = () => {
    this.setState({ isPayment: false });
  }

  getOrderId = () => {
    const { orderDetails } = this.props;
    return orderDetails || {};
  }

  cardOnChange = ({
    setFieldValue,
    type,
    value,
    values,
    paymentMethod,
  }) => {
    const {
      cardNumber,
    } = values;
    if (type && (value || value === '')) {
      setFieldValue(type, value);
    }
    const number = cleanCardNumber(type === 'cardNumber' ? value : cardNumber);
    const cardInfo = cardValidator.number(number);
    const cvvMaxLength = cardInfo.card ? cardInfo.card.code.size : 3;

    const cardSchema = paymentMethod === PAYMENT_METHOD.card ? {
      cardNumber: Yup.string()
        .typeError(TERMS.SHOULD_BE_NUMBER)
        .test('required', TERMS.WRONG_CARD_NUMBER, (val) => {
          if (val) {
            const isCardValidInfo = cardValidator.number(cleanCardNumber(val));
            return (
              val && Boolean(cleanCardNumber(val).length) && isCardValidInfo.isValid
            )
          }
            return false;

        }),
      cardExpirationDate: Yup.string()
        .test('required', TERMS.SHOULD_BE_FILLED, val => (
          val && toCardExpiry(val).length
        ))
        .test('count', TERMS.MAX_NUMBER_4, val => (
            val && toCardExpiry(val).length === 4
          )
        )
        .test('count', TERMS.WRONG_CARD_DATE, val => {
          const cardDateInfo = cardValidator.expirationDate(val);
          return (
            val && toCardExpiry(val).length && cardDateInfo.isValid
          )
        }),
      cardCode: Yup.number()
        .test('count', TERMS[`MAX_NUMBER_${cvvMaxLength}`], (_, schemaValue) => {
          const { from } = schemaValue;
          const { value: { cardNumber: formCardNumber = '', cardCode } } = from[0];
          if (formCardNumber && cardCode) {
            const newNumber = cleanCardNumber(formCardNumber);
            const newCardInfo = cardValidator.number(newNumber);
            const newCvvMaxLength = newCardInfo.card ? newCardInfo.card.code.size : 3;
            return (cardCode && String(cardCode).length === newCvvMaxLength && newCardInfo.isValid)
          }
            return false;

        })
        .integer()
        .typeError(TERMS.SHOULD_BE_NUMBER)
        .required(TERMS.SHOULD_BE_FILLED),
    } : {};

    this.setState({
      validationSchema: Yup.object().shape({
        ...cardSchema,
      }),
      cvvMaxLength,
      typeCard: cardInfo.card ? cardInfo.card.type : '',
    });
  }

  onClickViewAll = () => {
    const { changeScene } = this.props;
    changeScene('trackOrder');
  }

  render() {
    const {
      isPayment,
      paymentMethod,
      validationSchema,
      cvvMaxLength,
      typeCard,
      messageOpen,
      generalRoom,
    } = this.state;

    const {
      orderDetails,
      token,
      registrationPayment,
      spinnerFlag,
      creditCard,
      paymentError,
      paymentFail,
      currency,
      selectPrice,
      setPaymentFailed,
    } = this.props;
    const { cardNumber } = creditCard;
    const {
      items,
      orderPayments,
      id,
      isQuickly,
      orderShippingAddress,
      discountAmount,
      taxAmount,
      promoCode,
      calculated,
    } = orderDetails;

    const {
      entitledProductIds = [],
      valueType,
      targetType,
      value: promoValue,
      isGiftCard,
      isOrderIncludeAllProducts,
    } = promoCode || {};


    const couponValue = promoValue || 0;
    const products = items || [];
    const {
      giftSubTotal,
    } = formattedOrderDetails({
      products,
      isQuickly,
      orderShippingAddress,
      discountAmount,
      taxAmount,
      couponValue,
      entitledProductIds,
      currency,
      selectPrice,
      valueType,
      targetType,
      isGiftCard,
      isOrderIncludeAllProducts,
    });
    const orderId = orderDetails ? id : 0;
    const amountPaid = orderPayments && orderPayments.length
      ? orderPayments.reduce((acc, { amount }) => acc + amount, 0)
      : 0;

    const {
      sum,
      quickly,
      delivery,
      toPay,
      giftDiscount,
      discount,
      tax,
    } = calculated || {};

    const subTotal = (sum && quickly && delivery)
      ? Number(sum) - Number(quickly) - Number(delivery) - Number(giftSubTotal)
      : 0;
    const shippingSubTotal = delivery ? Number(delivery) : 0;
    const rushSubTotal = quickly ? Number(quickly) : 0;
    const taxData = tax ? Number(tax) : 0;
    const discountData = (discount || giftDiscount) ? Number(discount) || Number(giftDiscount) : 0;
    const total = toPay ? Number(toPay) : 0;

    const balanceSum = total - amountPaid ? total - amountPaid : 0;

    const balanceDue = orderPayments
      ? (balanceSum).toFixed(2)
      : total;


    const shippingOrder = products.find(({ product }) => product.productType === 'shipping');
    const { product: shippingOrderProduct } = shippingOrder || {};
    const { displayName = '' } = shippingOrderProduct || '';

    return Object.keys(orderDetails).length ? (
      <main className={b()}>
        <div className={b('main-content')}>
          <div className={b('order-wrapper')}>
            <OrderDetailsCard />
          </div>
          <div className={b('order-total-info-wrapper')}>
            <div className={b('order-info-wrapper')}>
              <div className={b('order-info', { column: true })}>
                <div className={b('order-info-row')}>
                  <span className={b('order-info-column-title', { 'sub-total': true })}>
                    {SUB_TOTAL}
                  </span>
                  <span className={b('order-info-column-value')}>
                    {CURRENCY}
                    {subTotal.toFixed(2)}
                  </span>
                </div>
                {Boolean(rushSubTotal) && (
                  <div className={b('order-info-row')}>
                    <span className={b('order-info-column-title')}>{RUSH_SUB_TOTAL}</span>
                    <span className={b('order-info-column-value')}>
                      {CURRENCY}
                      {rushSubTotal.toFixed(2)}
                    </span>
                  </div>
                )}
                {Boolean(giftSubTotal) && (
                  <div className={b('order-info-row')}>
                    <span className={b('order-info-column-title')}>{GIFT_SUB_TOTAL}</span>
                    <span className={b('order-info-column-value')}>
                      {CURRENCY}
                      {giftSubTotal.toFixed(2)}
                    </span>
                  </div>
                )}
                {Boolean(shippingSubTotal) && (
                  <div className={b('order-info-row')}>
                    <span className={b('order-info-column-title')}>
                      {displayName ? `${displayName}:` : SHIPPING_SUB_TOTAL}
                    </span>
                    <span className={b('order-info-column-value')}>
                      {CURRENCY}
                      {shippingSubTotal.toFixed(2)}
                    </span>
                  </div>
                )}
                {Boolean(discountData) && (
                  <div className={b('order-info-row')}>
                    <span className={b('order-info-column-title')}>{DISCOUNT}</span>
                    <span className={b('order-info-column-value', { discount: true })}>
                      {`-${CURRENCY}${discountData.toFixed(2)}`}
                    </span>
                  </div>
                )}
                {Boolean(taxData) && (
                  <div className={b('order-info-row')}>
                    <span className={b('order-info-column-title')}>{FLORIDA_SALE_TAX}</span>
                    <span className={b('order-info-column-value')}>
                      {CURRENCY}
                      {taxData.toFixed(2)}
                    </span>
                  </div>
                )}
              </div>
              <div className={b('order-info-row', { total: true })}>
                <span className={b('order-info-column-title', { total: true })}>{TOTAL}</span>
                <span className={b('order-info-column-value', { total: true })}>
                  {CURRENCY}
                  {total.toFixed(2)}
                </span>
              </div>
              <div className={b('order-info')}>
                <div className={b('order-info-row')}>
                  <span className={b('order-info-column-title')}>{AMOUNT_PAID}</span>
                  <span className={b('order-info-column-value')}>
                    {CURRENCY}
                    {amountPaid.toFixed(2)}
                  </span>
                </div>
                <div className={b('order-info-row')}>
                  <span className={b('order-info-column-title')}>{BALANCE_DUE}</span>
                  <span
                    className={b(
                      'order-info-column-value',
                      Number(balanceDue) !== 0 && { red: true },
                    )}
                  >
                    {Number(balanceDue) >= 0
                      ? `${CURRENCY}${balanceDue === '-0.00' ? '0.00' : balanceDue}`
                      : `-${CURRENCY}${(-balanceDue).toFixed(2)}`}
                  </span>
                </div>
              </div>
            </div>
            <div className={b('buttons-wrapper')}>
              <div className={b('first-button-row')}>
                <BlueButton className={b('btn')} onClick={() => this.requestOpen(true)}>
                  <MailIcon className={b('icon')} />
                  <p className={b('btn-text')}>{MESSAGE_US}</p>
                </BlueButton>
                <RedButton
                  className={b('btn')}
                  type="button"
                  onClick={this.choosePayment}
                  disabled={balanceDue <= 0}
                  text={PAY}
                />
              </div>
              <button className={b('invoice-btn')} type="button" onClick={this.onDownload(orderId)}>
                <DocumentIcon className={b('invoice-icon')} />
                {PRINT_INVOICE}
              </button>
            </div>
            <OrderTrack
              className={b('track-block')}
              isWidget
              onClickViewAll={this.onClickViewAll}
            />
          </div>
        </div>
        {isPayment && (
          <ModalContainer
            close={this.closePopup}
            modalCenter
            className={b('payment-form')}
            closeBtn
          >
            <Formik
              initialValues={{
                cardExpirationDate: '',
                cardNumber: '',
                cardCode: '',
              }}
              validationSchema={validationSchema}
              onSubmit={(values, actions) => {
                this.onSubmitPayment(values);
                actions.resetForm();
              }}
              render={({ errors, handleSubmit, touched, setFieldValue, setErrors, values }) => (
                <form onSubmit={handleSubmit} className={b('payment')}>
                  <div className={b('payment-method')}>
                    <div className={b('method-text-wrapper-container')}>
                      {Boolean(cardNumber) && (
                        <div className={b('method-text-wrapper')}>
                          <CardPayIcon fill={blue} />
                          <div className={b('method-text')}>
                            <p className={b('method-description')}>{SAVED_BANK_CARD}</p>
                            <p className={b('method-description', { tiny: true })}>
                              {formattedToCardNumber(cardNumber)}
                            </p>
                          </div>
                          <label htmlFor className={b('method-checkbox')}>
                            <input
                              className={b('input')}
                              type="checkbox"
                              onClick={this.handlePayment(
                                PAYMENT_METHOD.savedCard,
                                values,
                                setErrors,
                                errors,
                              )}
                              checked={paymentMethod === PAYMENT_METHOD.savedCard}
                            />
                            <span className={b('custom')} />
                          </label>
                        </div>
                      )}
                      <div className={b('method-text-wrapper')}>
                        <CardPayIcon fill={blue} />
                        <div className={b('method-text')}>
                          <p className={b('method-description')}>{BANK_CARD}</p>
                          <p className={b('method-description', { tiny: true })}>
                            {BANK_CARD_TEXT}
                          </p>
                        </div>
                        <label htmlFor className={b('method-checkbox')}>
                          <input
                            className={b('input')}
                            type="checkbox"
                            onClick={this.handlePayment(
                              PAYMENT_METHOD.card,
                              values,
                              setErrors,
                              errors,
                            )}
                            checked={paymentMethod === PAYMENT_METHOD.card}
                          />
                          <span className={b('custom')} />
                        </label>
                      </div>
                      <div className={b('method-text-wrapper')}>
                        <PayPalIcon />
                        <div className={b('method-text')}>
                          <p className={b('method-description')}>{PAYPAL}</p>
                          <p className={b('method-description', { tiny: true })}>{PAYPAL_TEXT}</p>
                        </div>
                        <label htmlFor className={b('method-checkbox')}>
                          <input
                            className={b('input')}
                            type="checkbox"
                            onClick={this.handlePayment(
                              PAYMENT_METHOD.paypal,
                              values,
                              setErrors,
                              errors,
                            )}
                            checked={paymentMethod === PAYMENT_METHOD.paypal}
                          />
                          <span className={b('custom')} />
                        </label>
                      </div>
                    </div>
                    <div className={b('card-info')}>
                      {paymentMethod === PAYMENT_METHOD.card && (
                        <>
                          <div className={b('info-form-row')}>
                            <span className={b('card-info-title')}>{CARD_INFO}</span>
                          </div>
                          <div className={b('info-form-row')}>
                            <div className={b('column-input', { 'full-width': true })}>
                              <NewCustomInput
                                className={b('column-input-field')}
                                onChange={({ target: { value } }) =>
                                  this.cardOnChange({
                                    setFieldValue,
                                    type: 'cardNumber',
                                    value,
                                    values,
                                    paymentMethod,
                                  })}
                                name="cardNumber"
                                placeholder={CARD_NUMBER}
                                value={values.cardNumber}
                                isTouched={touched.cardNumber}
                                error={errors.cardNumber}
                                isValid={!errors.cardNumber}
                                withMask
                                mask={typeCard === AE_CARD_TYPE ? AE_CARD_MASK : CARD_MASK}
                                withIcon
                                backGroundImage={inputCardIcon}
                              />
                            </div>
                          </div>
                          <div className={b('info-form-row', { two_column: true })}>
                            <div className={b('column-input')}>
                              <NewCustomInput
                                className={b('column-input-field')}
                                onChange={({ target: { value } }) =>
                                  this.cardOnChange({
                                    setFieldValue,
                                    type: 'cardExpirationDate',
                                    value,
                                    values,
                                    paymentMethod,
                                  })}
                                name="cardExpirationDate"
                                placeholder={CARD_EXPIRY}
                                value={values.cardExpirationDate}
                                isTouched={touched.cardExpirationDate}
                                error={errors.cardExpirationDate}
                                isValid={!errors.cardExpirationDate}
                                withMask
                                mask={CARD_EXPIRATION_DATE}
                                id="cardExpirationDate"
                              />
                            </div>
                            <div className={b('column-input')}>
                              <NewCustomInput
                                className={b('column-input-field')}
                                onChange={({ target: { value } }) =>
                                  this.cardOnChange({
                                    setFieldValue,
                                    type: 'cardCode',
                                    value,
                                    values,
                                    paymentMethod,
                                  })}
                                name="cardCode"
                                placeholder={CARD_CVC}
                                value={values.cardCode}
                                isTouched={touched.cardCode}
                                error={errors.cardCode}
                                isValid={!errors.cardCode}
                                maxLength={cvvMaxLength}
                                id="cardCode"
                              />
                            </div>
                          </div>
                        </>
                      )}
                      {paymentError && paymentFail && (
                        <span className={b('error')}>{paymentError}</span>
                      )}
                      {paymentMethod !== PAYMENT_METHOD.paypal ? (
                        <ColorButton
                          type="submit"
                          text={PAY_NOW}
                          backGroundColor={orangeColor}
                          className={b('payment-btn')}
                        />
                      ) : (
                        <PaypalButton
                          token={token}
                          registrationPayment={registrationPayment}
                          orderPaymentFailed={setPaymentFailed}
                          type="paypal"
                          orderSum={balanceDue}
                          getOrderId={this.getOrderId}
                          additionalPayment
                        />
                      )}
                    </div>
                  </div>
                </form>
              )}
            />
          </ModalContainer>
        )}
        {messageOpen && (
          <div className={b('message-popup')}>
            <div className={b('message-popup-content')}>
              <ChatRoom roomId={generalRoom?.id} socket={this.socket} />
              <div
                className={b('message-popup-close')}
                tabIndex={0}
                role="button"
                onClick={() => this.requestOpen(false)}
              >
                <CloseIcon2 width={30} height={30} />
              </div>
            </div>
          </div>
        )}
        {spinnerFlag && <SpinnerBlock />}
      </main>
    ) : (
      <span className={b('without-order-text')}>{NO_ORDERS}</span>
    );
  }
}

OrderDetails.propTypes = propTypes;
OrderDetails.defaultProps = defaultProps;

const stateProps = state => ({
  orderDetails: orderDetailsSelect(state),
  token: getToken(state),
  userId: userIdSelector(state),
  typeOrders: ordersTypeSelect(state),
  requestStatus: requestChangesStatusSelector(state),
  spinnerFlag: spinnerFlagSelector(state),
  creditCard: userPaymentInfoSelector(state),
  selectPrice: orderPriceSelect(state),
  paymentError: paymentErrorSelector(state),
  paymentFail: paymentFailSelector(state),
  chatToken: chatTokenSelector(state),
});

const actions = {
  downloadOrderInvoice: getOrderInvoice,
  addOrder: addToCart,
  orderRequestChanges: createOrderMessage,
  registrationPayment: orderPayment,
  clearOrderError: orderClear,
  addImagesforCard: addPhotoArtReorderImages,
  setPaymentFailed: orderPaymentFailed,
  getPromo: getPromoSection,
};

export default connect(stateProps, actions)(OrderDetails);
