import * as Yup from 'yup';
import toCardExpiry from 'src/utils/toCardExpiry';
import cleanCardNumber from 'src/utils/cleanCardNumber';

const TERMS = {
  ORDER: 'Order',
  PRICE: 'Price',
  PRODUCTS: 'Products',
  SUB_TOTAL: 'Subtotal:',
  QUANTITY: 'Quantity',
  RUSH_SUB_TOTAL: 'Order rush: ',
  GIFT_SUB_TOTAL: 'Gift options: ',
  SHIPPING_SUB_TOTAL: 'Shipping: ',
  FLORIDA_SALE_TAX: 'Tax: ',
  CURRENCY: '$',
  USD: '(USD)',
  TOTAL: 'Total: ',
  BALANCE_DUE: 'Balance:',
  ADD_MESSAGE: 'Type text',
  REQUEST_CHANGES_TITLE: 'Let us know what changes you would like to see.',
  ORDER_ID: 'order id',
  PAY: 'Pay balance',
  PRINT_INVOICE: 'Print invoice',
  REORDER: 'Reorder',
  REQUEST_CHANGES: 'Request changes',
  MESSAGE_US: 'Messages',
  SUBMIT: 'Submit',
  CANCEL: 'Cancel',
  PAYPAL: 'Paypal',
  PAYPAL_TEXT: 'Simple and fast',
  BANK_CARD: 'Credit card',
  BANK_CARD_TEXT: 'Visa, Master Card, American Express or Discover',
  CARD_INFO: 'Credit card details',
  CARD_NAME: 'Name on card',
  CARD_NUMBER: 'Card number',
  CARD_EXPIRY: 'Expiry date',
  CARD_CVC: 'CVC',
  CHOOSE_PAYMENT: 'Choose your payment method',
  PAY_NOW: 'Pay now',
  PAYPAL_PAYMENT: 'paypal',
  CARD_PAYMENT: 'card',
  SHOULD_BE_NUMBER: 'should be number',
  SHOULD_BE_POSITIVE: 'should be positive',
  TYPE_NUMBER: 'number',
  NO_ORDERS: 'You have no orders yet ',
  AMOUNT_PAID: 'Amount paid:',
  REORDER_ITEMS: ['photo_art', 'photo_restoration', 'photo_colorization', 'photo_retouching'],
  PHOTO_ART: 'photo_art',
  REORDER_INFO: 'You can reorder only Photo Art product or Enhance product with Photo Art',
  COLON: ':',
  PAYMENT_METHOD: {
    savedCard: 'savedCard',
    card: 'card',
    paypal: 'paypal',
  },
  SAVED_BANK_CARD: 'Saved credit card',
  DISCOUNT: 'Discount:',
  MAX_NUMBER_4: 'Should be up to 4 symbols',
  MAX_NUMBER_3: 'Should be up to 3 symbols',
  SHOULD_BE_FILLED: 'Should be filled',
  WRONG_CARD_NUMBER: 'Wrong card number',
  WRONG_CARD_DATE: 'Wrong card date',
  AGREEMENT_TEXT: 'I have read and agree to ******* terms.',
  AE_CARD_TYPE: 'american-express',
  DATE_FORMAT: 'YYYY/MM/DD HH:MM a',
  YOU: 'You',
  ADMIN: 'Admin',
  SETTING_GET_MESSAGES: {
    pagination: {
      limit: 99999,
      offset: 0,
    },
    sorting: [
      {
        column: 'date',
        order: 'desc',
      },
    ],
  },
};
const CARD_SCHEMA = {
  cardExpirationDate: Yup.string()
    .test('required', TERMS.SHOULD_BE_FILLED, (value) => value && toCardExpiry(value).length)
    .test('count', TERMS.MAX_NUMBER_4, (value) => value && toCardExpiry(value).length === 4),
  cardNumber: Yup.string()
    .typeError(TERMS.SHOULD_BE_NUMBER)
    .test('required', TERMS.SHOULD_BE_FILLED, (value) => value && cleanCardNumber(value).length),
  cardCode: Yup.number()
    .test('count', TERMS.MAX_NUMBER_3, (value) => value && String(value).length === 3)
    .integer()
    .typeError(TERMS.SHOULD_BE_NUMBER)
    .required(TERMS.SHOULD_BE_FILLED),
};

export { TERMS, CARD_SCHEMA };
