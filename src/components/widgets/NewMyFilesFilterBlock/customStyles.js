import Colors from 'src/styles/colors.json';

const pickledBluewoodeColor = Colors['$pickled-bluewood-color'];
const grayColor = Colors['$gull-gray-color'];

const customStylesWithoutLine = {
  control: (styles) => ({
    ...styles,
    border: 'none',
    backgroundColor: 'transparent',
    boxShadow: 'none',
  }),
  singleValue: (styles) => ({
    ...styles,
    fontSize: '16px',
    fontWeight: '500',
    lineHeight: '22px',
    color: `${pickledBluewoodeColor}`,
    position: 'initial',
    transform: 'none',
    maxWidth: '100%',
    margin: '0 12px 0 0',
  }),
  option: (styles, { isSelected }) => ({
    ...styles,
    backgroundColor: 'transparent',
    color: `${isSelected ? pickledBluewoodeColor : grayColor}`,
    fontSize: '16px',
    fontWeight: '500',
    lineHeight: '22px',
  }),
  menu: (styles) => ({
    ...styles,
    border: 'none',
    backgroundColor: 'transparent',
    boxShadow: 'none',
    position: 'relative',
  }),
  indicatorSeparator: (styles) => ({
    ...styles,
    display: 'none',
  }),
  indicatorsContainer: (styles) => ({
    ...styles,
    padding: '4px',
  }),
};

export default customStylesWithoutLine;
