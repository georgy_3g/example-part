import { components } from 'react-select';
import React from 'react';
import PropTypes from 'prop-types';
import bem from 'src/utils/bem';
import Colors from 'src/styles/colors.json';
import styles from './index.module.scss';

const orange = Colors['$burnt-sienna-color'];

const b = bem('dropdown-indicator', styles);

const propTypes = {
  stroke: PropTypes.string,
  className: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
  selectProps: PropTypes.shape({ menuIsOpen: PropTypes.bool }),
};

const defaultProps = {
  stroke: orange,
  className: '',
  width: '16',
  height: '9',
  selectProps: {},
};

function DropdownIndicator(props) {
  const {
    selectProps: { menuIsOpen },
    stroke,
    width,
    height,
    className,
  } = props;

  return (
    components.DropdownIndicator && (
      <components.DropdownIndicator {...props}>
        <svg
          className={`${className} ${b()} ${menuIsOpen ? b('caret-up') : b('caret-down')}`}
          width={width}
          height={height}
          viewBox="0 0 16 9"
          fill="none"
        >
          <path
            stroke={stroke}
            strokeLinecap="round"
            strokeWidth="2.5"
            d="M14 2L9.485 6.515a2.1 2.1 0 01-2.97 0L2 2"
          />
        </svg>
      </components.DropdownIndicator>
    )
  );
}

DropdownIndicator.propTypes = propTypes;
DropdownIndicator.defaultProps = defaultProps;

export default DropdownIndicator;
