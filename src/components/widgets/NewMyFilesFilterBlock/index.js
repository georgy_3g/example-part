import React, { useState } from 'react';
import bem from 'src/utils/bem';
import PropTypes from 'prop-types';
import Select from 'react-select';
import customStylesWithoutLine from './customStyles';
import DropdownIndicator from './DropdownIndicator';
import styles from './index.module.scss';

const b = bem('new-my-files-filter-block', styles);

const propTypes = {
  className: PropTypes.string,
  buttons: PropTypes.arrayOf(PropTypes.shape({})),
  changeFilterType: PropTypes.func,
  filterType: PropTypes.string,
};

const defaultProps = {
  className: '',
  buttons: [],
  changeFilterType: () => {},
  filterType: '',
};

const components = {
  IndicatorSeparator: null,
  DropdownIndicator,
};

function NewMyFilesFilterBlock(props) {
  const { buttons, changeFilterType, filterType, className } = props;

  const selectedValue = buttons.find(({ type }) => type === filterType);

  const [value, selectValue] = useState(selectedValue);

  const onClick = (type) => {
    changeFilterType(type);
    const newType = buttons.find((item) => item.type === type);
    selectValue(newType);
  };

  const selectFilterType = (e) => {
    selectValue(e);
    changeFilterType(e.type);
  };

  return (
    <div className={b({ mix: className })}>
      <div className={b('buttons-block')}>
        {buttons.map(({ type, isShow, name }) =>
          isShow ? (
            <button
              className={b('filter-button', { active: type === filterType })}
              type="button"
              key={type}
              onClick={() => onClick(type)}
            >
              <span className={b('name')}>{name}</span>
            </button>
          ) : null,
        )}
      </div>
      <div className={b('select-view', { 'without-line': true })}>
        <Select
          className={b('select', { 'without-line': true })}
          defaultValue={value}
          options={buttons}
          styles={customStylesWithoutLine}
          onChange={selectFilterType}
          isSearchable={false}
          components={components}
          getOptionLabel={(option) => option.name}
          getOptionValue={(option) => option.type}
        />
      </div>
    </div>
  );
}

NewMyFilesFilterBlock.propTypes = propTypes;
NewMyFilesFilterBlock.defaultProps = defaultProps;

export default NewMyFilesFilterBlock;
