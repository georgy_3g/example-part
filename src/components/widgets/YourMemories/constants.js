const TITLE = 'Your memories,\nour passion.';
const SUB_TITLE =
  'We are a dedicated team of archivists, senior photo editors & creators passionate about helping you restore, print\n& display your treasured old pictures. Our team cares deeply about the quality of each and every photo you trust\nus with. Our entire team is comprised of masters in their field. We work together to provide you the best photo\nrestoration delivered fast in high resolution images, photo prints and picture frames. ';
export { TITLE, SUB_TITLE };
