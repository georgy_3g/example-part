import React from 'react';
import bem from 'src/utils/bem';
import PropTypes from 'prop-types';
import LazyLoad from 'react-lazyload';
import dynamic from 'next/dynamic';
import { SUB_TITLE, TITLE } from './constants';
import styles from './index.module.scss';

const BlueButton = dynamic(() => import('src/components/buttons/BlueButton'));
const SliderWithSubTitle = dynamic(() => import('src/components/widgets/SliderWithSubTitle'));

const b = bem('your-memories', styles);

const defaultProps = {
  className: '',
  title: TITLE,
  description: SUB_TITLE,
  autoplay: true,
  infinite: true,
  withScrollButton: false,
  scroll: () => {},
  scrollButtonText: '',
  isMobile: true,
  isRetouching: false,
};

const propTypes = {
  className: PropTypes.string,
  sliderList: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  title: PropTypes.string,
  description: PropTypes.string,
  autoplay: PropTypes.bool,
  infinite: PropTypes.bool,
  withScrollButton: PropTypes.bool,
  scroll: PropTypes.func,
  scrollButtonText: PropTypes.string,
  isMobile: PropTypes.bool,
  isRetouching: PropTypes.bool,
};

function YourMemories(props) {
  const {
    className,
    sliderList,
    title,
    description,
    autoplay,
    infinite,
    withScrollButton,
    scrollButtonText,
    scroll,
    isMobile,
    isRetouching,
  } = props;
  return (
    <div className={b({ mix: className })}>
      <div className={b('text-wrapper')}>
        <h2 className={b('title')}>{title}</h2>
        <div className={b('sub-title')}>{description}</div>
      </div>
      <div className={b('slider-wrapper')}>
        <LazyLoad offset={100}>
          <SliderWithSubTitle
            slideList={sliderList}
            imageClass={b('image')}
            compareImageClass={b('compare-image')}
            slidesToShow={4}
            autoplay={autoplay}
            lazyLoad
            infinite={infinite}
            isMobile={isMobile}
          />
        </LazyLoad>
        <div className={b('left-square')} />
      </div>
      {withScrollButton && scrollButtonText && (
        <BlueButton className={b('scroll-button', { retouching: isRetouching } )} onClick={scroll}>
          {scrollButtonText}
        </BlueButton>
      )}
    </div>
  );
}

YourMemories.propTypes = propTypes;
YourMemories.defaultProps = defaultProps;

export default YourMemories;
