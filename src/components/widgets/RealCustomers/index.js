import React from 'react';
import bem from 'src/utils/bem';
import PropTypes from 'prop-types';
import LazyLoad from 'react-lazyload';
import dynamic from 'next/dynamic';
import { DESCRIPTION, TITLE } from './constants';
import styles from './index.module.scss';

const ImageSliderWithVideo = dynamic(() => import('src/components/widgets/ImageSliderWithVideo'));

const b = bem('real-customers', styles);

const defaultProps = {
  className: '',
  title: TITLE,
  description: DESCRIPTION,
  autoPlayVideo: false,
  withScrollButton: false,
  scroll: () => {},
  scrollButtonText: '',
  customClassBtn: '',
  showVideo: false,
};

const propTypes = {
  className: PropTypes.string,
  sliderList: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  title: PropTypes.string,
  autoPlayVideo: PropTypes.bool,
  description: PropTypes.string,
  withScrollButton: PropTypes.bool,
  scroll: PropTypes.func,
  scrollButtonText: PropTypes.string,
  customClassBtn: PropTypes.string,
  showVideo: PropTypes.bool,
};

function RealCustomers(props) {
  const {
    className,
    sliderList,
    title,
    autoPlayVideo,
    description,
    withScrollButton,
    scrollButtonText,
    scroll,
    customClassBtn,
    showVideo,
  } = props;
  return (
    <div className={b({ mix: className })}>
      <div className={b('text-wrapper')}>
        <h2 className={b('title')}>{title}</h2>
        <div className={b('description')}>{description}</div>
      </div>
      <div className={b('slider-wrapper')}>
        <LazyLoad offset={100}>
          <ImageSliderWithVideo
            slideList={sliderList}
            imageClass={b('image')}
            compareImageClass={b('compare-image')}
            slidesToShow={3}
            autoplay={false}
            lazyLoad
            infinite={false}
            autoPlayVideo={autoPlayVideo}
            withScrollButton={withScrollButton}
            scrollButtonText={scrollButtonText}
            scroll={scroll}
            customClassBtn={customClassBtn}
            showVideo={showVideo}
          />
        </LazyLoad>
        <div className={b('left-square')} />
        <div className={b('right-stripe')} />
      </div>
    </div>
  );
}

RealCustomers.propTypes = propTypes;
RealCustomers.defaultProps = defaultProps;

export default RealCustomers;
