const TITLE = 'Every photo has a story, what’s yours?';
const DESCRIPTION =
  'We believe every photo is as priceless as the memories lived. Do you remember your fondest childhoood memory, falling in love & getting married, holding your child for the first time, watching your children grow and celebrating life’s most special ocassions? We want you to smile and relive your treasured memory everytime you look at your newly restored old photo. We can’t wait to hear the special story behind your vintage photo!';
const LIST = ['Rips', 'Fading', 'Water damage', 'Scratches', 'Lorem ipsum'];

const BOTTOM_TEXT = {
  orderOnline: 'Just place your order online and select "',
  homeKit: 'Home Ship Kit',
  duringCheckout: '" during checkout.',
};

export { TITLE, LIST, BOTTOM_TEXT, DESCRIPTION };
