import firstStepOne from  'public/img/navigation/restore-retouch-photo-service-drop-down-menu-old-photo-restoration-200x150.jpg';

import materialOne from  'public/img/enhance/materialIcons/PAPER-PRINTS.png';
import materialTwo from  'public/img/enhance/materialIcons/ROLLED-PRINT.png';
import materialThree from  'public/img/enhance/materialIcons/PRINT-ON-GLASS.png';
import dynamic from "next/dynamic";

const PhotoPrintsIcon = dynamic(() => import('src/components/svg/PhotoPrintsIcon'));
const RolledPhotoIcon = dynamic(() => import('src/components/svg/RolledPhotoIcon'));

const STEPS = [
  {
    name: 'Your photos',
    id: 1,
  },
  {
    name: 'Quantity',
    id: 2,
  },
  {
    name: 'Colorization',
    id: 3,
  },
  {
    name: 'Notes',
    id: 4,
  },
  {
    name: 'Review',
    id: 5,
  },
];

const BACK = 'Back';
const ADD_MORE_PHOTOS = 'Add more photos';
const FIRST_STEP_TITLE = 'Select how you would like to send\nus your photos to restore.';
const FIRST_STEP_TITLE_RETOUCHING = 'Select how you would like to send\nus your photos to retouch.';
const SHIP_KIT_4_STEP = 'Add paper prints or an 8x10 tribute frame.\nDigital download included.';
const SHIP_KIT_4_STEP_MOBILE = 'Add paper prints or an\n8x10 tribute frame.\nDigital download included.';

const FIRST_STEP_ITEMS = [
  {
    title: 'Upload your photos',
    description: 'Upload your scanned photos from computer or phone to restore.',
    id: 1,
    imageUri: firstStepOne,
    hint: '',
    hintImages: [],
    name: 'upload',
  },
  {
    title: 'Order scan kit',
    description: 'Upload your scanned photos from computer or phone to restore.',
    id: 2,
    imageUri: firstStepOne,
    hint: '',
    hintImages: [],
    name: 'shipKit',
  },
];

const SCANNING_TITLE = 'Tips to ensure the best scan.';
const SCANNING_DESCRIPTION =
  'Before you upload your photos to *******, it’s important to get the most accurate scan of your original photos. Please follow our tips below or find a local professional to scan your photos for you.';
const SCANNING_INFO_LIST = [
  {
    id: 'scan1',
    text: 'Clean the scanner glass',
  },
  {
    id: 'scan2',
    text: 'Scan to jpeg, tiff or png format',
  },
  {
    id: 'scan3',
    text: 'Scan at 300dpi or higher',
  },
  {
    id: 'scan4',
    text: 'Scan just the image and not empty space',
  },
  {
    id: 'scan5',
    text: 'Do not add optional settings',
  },
];
const SUBTOTAL = 'Subtotal:';

const MIN_QT = 0;
const MAX_QT = 99999;

const PER = 'per';
const REEL = 'photo';

const MATERIALS_ORDER = {
  'photo prints': {
    order: 0,
    img: materialOne,
    icon: PhotoPrintsIcon,
  },
  'rolled photo': {
    order: 1,
    img: materialTwo,
    icon: RolledPhotoIcon,
  },
  'photo on glass': {
    order: 2,
    img: materialThree,
    icon: PhotoPrintsIcon,
  },
};

const ADD_TO_CART = 'Add to cart';
const LABELS_INCLUDE = 'Your kit will include labels to number your photos.';

const WHAT_SIZE = 'What size home scan ship\nkit should we send you?';
const SHIP_KIT_3_STEP = 'Click the icons to add notes or\ncolorize your Black and White photos.';
const WIZARD_INFORMATION = 'Ordering information.';
const WIZARD_TITLE = 'Order your photo\nrestoration now.';
const WIZARD_TITLE_RETOUCHING = 'Start your photo retouching now.';
const WIZARD_TITLE_DESCRIPTION = ' With our premium photo restoration service, our senior photo editors can restore any old damaged photo, repair\nrips, tears & fading, colorize black and white photos and enlarge pictures to high resolution images, photo prints &\nframes. With unlimited free revisions, our professional image experts will revise your photo until you are happy,\nguaranteed. Get started now by uploading your photo or order our secure 3-way home ship kit. Your first proof\ncan be ready to view in as little as 24 hours.';
const WIZARD_TITLE_DESCRIPTION_RETOUCHING = ' With our premium photo retouching service, our senior photoshop editors will perfectly color correct your photo,\nremove any blemishes on your face and body, sharpen your photo, airbrush your photo and change or remove the\nbackground of your image. With unlimited free revisions, our expert photo editors will revise your photo until you\nare happy, guaranteed. Get started now by uploading your digital picture or order our easy 3-way scan ship kit.\nYour first proof can be ready to view in as little as 24 hours.';
const STEP_TITLE = 'Upload your photos from your \ncomputer, tablet or phone.';
const STEP_TWO_SELECT_TITLE = `Click the icons to add notes or\ncolorize your Black and White photos.`;
const SCAN_TITLE = 'Select the details for your easy \n scan ship kit.';
const HAPPINESS = 'We know you’re going to love your photo, we guarantee it.';
const PER_PHOTO = 'per photo';
const INCLUDED = ' included.';
const RESTORE_QUANTITY_TEXT = (typeProduct) => `${typeProduct ? 'Restoration' : 'Retouching'} quantity`;
const SHIP_KIT_NOTE_TITLE = 'Digital download included. You can choose to purchase photo prints and frames after you approve your retouched photo.';
const NOTE_TITLE = 'Do you have specific \n instructions?';
const NOTE_DESCRIPTION = 'Let us know if you have any notes for \nour artists regarding this photo.';
const NOTE_PLACEHOLDER = 'Please enter your notes here...';
const NOTE_BUTTON_TEXT = 'Submit Note';
const COLOR_BUTTON_CANCEL = 'No, thanks';
const RESTORATION_PRODUCT = 'photo_restoration';
const CHOOSE_ANY_PHOTO_RESTORATION = 'Add paper prints or an 8x10 tribute frame.\nDigital download included.';
const CHOOSE_ANY_PHOTO_RESTORATION_MOBILE = 'Add paper prints or an\n8x10 tribute frame.\nDigital download included.';
const CHOOSE_ANY_PHOTO_RETOUCHING = 'Add prints or a tribute frame.\nDigital download included.';
const NEXT = 'Next';
const UPLOAD_BUTTON_TEXT = 'Add more photos';

export {
  STEPS,
  BACK,
  FIRST_STEP_TITLE,
  FIRST_STEP_TITLE_RETOUCHING,
  FIRST_STEP_ITEMS,
  SCANNING_TITLE,
  SCANNING_DESCRIPTION,
  SCANNING_INFO_LIST,
  SUBTOTAL,
  MIN_QT,
  MAX_QT,
  PER,
  REEL,
  MATERIALS_ORDER,
  ADD_TO_CART,
  WIZARD_TITLE,
  WIZARD_TITLE_DESCRIPTION,
  WIZARD_TITLE_DESCRIPTION_RETOUCHING,
  STEP_TITLE,
  STEP_TWO_SELECT_TITLE,
  PER_PHOTO,
  SCAN_TITLE,
  RESTORE_QUANTITY_TEXT,
  SHIP_KIT_NOTE_TITLE,
  NOTE_BUTTON_TEXT,
  NOTE_DESCRIPTION,
  NOTE_PLACEHOLDER,
  NOTE_TITLE,
  COLOR_BUTTON_CANCEL,
  WIZARD_TITLE_RETOUCHING,
  RESTORATION_PRODUCT,
  HAPPINESS,
  INCLUDED,
  CHOOSE_ANY_PHOTO_RESTORATION,
  CHOOSE_ANY_PHOTO_RETOUCHING,
  NEXT,
  ADD_MORE_PHOTOS,
  WHAT_SIZE,
  SHIP_KIT_3_STEP,
  WIZARD_INFORMATION,
  SHIP_KIT_4_STEP,
  SHIP_KIT_4_STEP_MOBILE,
  CHOOSE_ANY_PHOTO_RESTORATION_MOBILE,
  UPLOAD_BUTTON_TEXT,
  LABELS_INCLUDE,
};
