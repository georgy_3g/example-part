import React, { Component } from 'react';
import PropTypes from 'prop-types';
import debounce from 'lodash/debounce';
import isEqual from 'lodash/isEqual';
import { connect } from 'react-redux';
import bem from 'src/utils/bem';
import convertToPrice from 'src/utils/convertToPrice';
import axios from 'axios';
import LazyLoad from 'react-lazyload';
import ROUTES from 'src/constants/routes';
import colors from 'src/styles/colors.json';

import ProductCard from 'src/components/cards/ProductCard';
import EnhanceShipKit from 'src/components/widgets/EnhanceShipKit';
import ProductNotesPopup from 'src/components/popups/ProductNotesPopup';
import ImageSelect from 'src/components/widgets/ImageSelect';
import ImageUploader from 'src/components/widgets/ImageUploader';
import HelpCenter from 'src/components/widgets/HelpCenter';

import ShipKitImg from 'public/img/enhance/scanShipKit.jpeg';
import dynamic from 'next/dynamic';
import {
  enhanceErrorsSelector,
  isLoadingEnhanceImagesSelector,
  orderImagesEnhanceSelector,
} from 'src/redux/orderImages/selectors';
import { orderPriceSelect } from 'src/redux/prices/selectors';
import {
  wizardCardsSelector,
  wizardHintSelector,
  wizardRestorationCardsSelector,
} from 'src/redux/wizardsCards/selectors';
import { numberOfUploadEnhanceImagesSelector } from 'src/redux/shared/selectors';
import { getToken } from 'src/redux/auth/selectors';
import {
  addEnhanceImage,
  clearEnhanceErrors,
  clearEnhanceImages,
  deleteImageEnhance,
  getOrderImageIdByFile,
  loadImagesOrderEnhance,
  setEnhanceImages,
  toggleImageEnhance,
} from 'src/redux/orderImages/actions';
import { addToCart } from 'src/redux/orders/actions';
import { deleteImage } from 'src/redux/photoArt/actions';
import { getWizardCardsAction } from 'src/redux/wizardsCards/actions';
import { getOptionEnhance } from 'src/redux/enhance/actions';
import styles from './index.module.scss';
import {
  ADD_MORE_PHOTOS,
  ADD_TO_CART,
  BACK,
  CHOOSE_ANY_PHOTO_RESTORATION,
  CHOOSE_ANY_PHOTO_RESTORATION_MOBILE,
  CHOOSE_ANY_PHOTO_RETOUCHING,
  COLOR_BUTTON_CANCEL,
  FIRST_STEP_ITEMS,
  FIRST_STEP_TITLE,
  FIRST_STEP_TITLE_RETOUCHING,
  LABELS_INCLUDE,
  MATERIALS_ORDER,
  MAX_QT,
  MIN_QT,
  NEXT,
  NOTE_BUTTON_TEXT,
  NOTE_DESCRIPTION,
  NOTE_PLACEHOLDER,
  NOTE_TITLE,
  PER,
  PER_PHOTO,
  REEL,
  RESTORATION_PRODUCT,
  RESTORE_QUANTITY_TEXT,
  SCANNING_DESCRIPTION,
  SCANNING_INFO_LIST,
  SCANNING_TITLE,
  SHIP_KIT_3_STEP,
  SHIP_KIT_4_STEP,
  SHIP_KIT_4_STEP_MOBILE,
  STEP_TITLE,
  STEP_TWO_SELECT_TITLE,
  STEPS,
  SUBTOTAL,
  UPLOAD_BUTTON_TEXT,
  WHAT_SIZE,
  WIZARD_TITLE,
  WIZARD_TITLE_DESCRIPTION,
  WIZARD_TITLE_DESCRIPTION_RETOUCHING,
  WIZARD_TITLE_RETOUCHING,
} from './constants';

const BeforeAfterPrice = dynamic(() => import('src/components/elements/BeforeAfterPrice'));

const CloseIcon2 = dynamic(() => import('src/components/svg/CloseIcon2'));
const NewChooseAnyPhoto = dynamic(() => import('src/components/widgets/NewChooseAnyPhoto'));
const DollarIcon = dynamic(() => import('src/components/svg/DollarIcon'));
const ColorButton = dynamic(() => import('src/components/buttons/ColorButton'));
const BlueButton = dynamic(() => import('src/components/buttons/BlueButton'));


const b = bem('restoration-wizard', styles);
const slateGrayColor = colors['$slate-gray-color'];
const summerGreenColor = colors['$summer-green-color'];
const burntSiennaRedColor = colors['$burnt-sienna-red-color'];
const pickledBluewoodColor = colors['$pickled-bluewood-color'];
const apricotColor = colors['$apricot-color'];
const greyColor = colors['$slate-gray-color'];
const orangeColor = colors['$burnt-sienna-red-color'];

const defaultProps = {
  className: '',
  token: '',
  toggleImage: () => {  },
  deleteImageEn: () => {  },
  orderImagesEnhance: [],
  product: {},
  clearEnhance: () => {  },
  clearImages: () => {  },
  isLoadingImages: false,
  allPrices: {},
  restorationCards: {},
  addOrder: () => {  },
  numberOfUploadImages: 0,
  setImages: () => {  },
  enhanceErrors: null,
  deleteImagesFromSelector: () => {  },
  hint: PropTypes.shape({
    title: '',
    description: '',
    hintImages: [],
  }),
  refFunc: () => {  },
  steps: [],
  getWizard: () => {  },
  typeProduct: '',
  getOptions: () => {  },
  imageIdByFile: () => {  },
  addImage: () => {  },
  bannerIcons: [],
  isSvg: false,
  anchorWizard: '',
  scrollRef: () => {  },
  uploadImages: () => {  },
  isRetouching: false,
};

const propTypes = {
  className: PropTypes.string,
  toggleImage: PropTypes.func,
  deleteImageEn: PropTypes.func,
  orderImagesEnhance: PropTypes.arrayOf(PropTypes.shape({})),
  product: PropTypes.shape({
    options: PropTypes.arrayOf(PropTypes.shape({})),
    price: PropTypes.number,
    reduce: PropTypes.func,
    displayName: PropTypes.string,
    name: PropTypes.string,
    additionalDiscount: PropTypes.number,
  }),
  clearEnhance: PropTypes.func,
  clearImages: PropTypes.func,
  isLoadingImages: PropTypes.bool,
  allPrices: PropTypes.shape({
    photo_colorization: PropTypes.shape({}),
    easy_scan_ship_kit: PropTypes.shape({}),
    photo_restoration: PropTypes.shape({}),
    photo_retouching: PropTypes.shape({}),
  }),
  restorationCards: PropTypes.shape({}),
  addOrder: PropTypes.func,
  numberOfUploadImages: PropTypes.number,
  setImages: PropTypes.func,
  enhanceErrors: PropTypes.arrayOf(PropTypes.string),
  deleteImagesFromSelector: PropTypes.func,
  hint: PropTypes.shape({
    title: PropTypes.string,
    description: PropTypes.string,
    hintImages: PropTypes.arrayOf(PropTypes.shape({})),
  }),
  refFunc: PropTypes.func,
  steps: PropTypes.arrayOf(PropTypes.shape({
    orderSections: PropTypes.arrayOf(PropTypes.shape({
      title: PropTypes.string,
      description: PropTypes.string,
      images: PropTypes.arrayOf(PropTypes.string),
    })),
  })),
  getWizard: PropTypes.func,
  typeProduct: PropTypes.string,
  getOptions: PropTypes.func,
  imageIdByFile: PropTypes.func,
  token: PropTypes.string,
  addImage: PropTypes.func,
  anchorWizard: PropTypes.string,
  scrollRef: PropTypes.func,
  isSvg: PropTypes.bool,
  bannerIcons: PropTypes.arrayOf(PropTypes.shape),
  uploadImages: PropTypes.func,
  isRetouching: PropTypes.bool,
};

class RestorationWizard extends Component {
  resizeWindow = debounce((value) => {
    this.windowWidth = value;
    this.setState({});
   }, 300);

  constructor(props) {
    super(props);

    this.block = null;
    this.wizardsBlock = null;
    this.uploaderBlock = null;
    this.promoBanner = null;
    this.windowWidth = null;
    const { isRetouching } = props;

    this.state = {
      selectedStep: isRetouching ? 2 : 1,
      selectedLoadType: isRetouching ? 'upload' : null,
      maxStep: isRetouching ? 3 : 2,
      materialValue: null,
      sizeValue: null,
      sizes: {},
      isShowScanningPopup: false,
      isShowAddPhoto: true,
      shipKitCount: 1,
      shipKitColorCount: 0,
      materialsToForm: [{}, {}, {}],
      colorizationImages: [],
      notes: '',
      isAccept: false,
      isHintOpen: false,
      selectedHintId: null,
      isShowProductNote: false,
      appendOptionEnhance: [],
      isStepOneClicked: false,
    };
  }

  componentDidMount() {
    const {
      clearEnhance,
      clearImages,
      getWizard,
      typeProduct,
      getOptions,
    } = this.props;
    getOptions();
    clearEnhance();
    clearImages(true);
    this.checkStorage();
    this.getImageByCloud();
    getWizard(typeProduct);
    this.windowWidth = window.innerWidth;
    window.addEventListener('resize', () => this.resizeWindow(window.innerWidth));
  }

  componentDidUpdate(prevProps, prevState) {
    const {
      isLoadingImages,
      numberOfUploadImages,
      orderImagesEnhance,
      enhanceErrors,
    } = this.props;

    const { isLoadingImages: oldFlag } = prevProps;

    const { isShowAddPhoto } = this.state;
    const data = JSON.parse(sessionStorage.getItem('wizardData') || '{}');
    const isNeedUpdate = this.checkState(data);

    if (isNeedUpdate) {
      this.updateStorage();
      this.setColorizationImages();
    }

    if (isLoadingImages && numberOfUploadImages && isShowAddPhoto) {
      this.toggleAddPhotos();
    }

    const isLoaded = oldFlag && !isLoadingImages;

    if (
      isLoaded
      && (!orderImagesEnhance || !orderImagesEnhance.length)
      && enhanceErrors
      && enhanceErrors.length
      && !isShowAddPhoto
    ) {
      this.toggleAddPhotos();
    }
    const { selectedStep } = this.state;
    if (selectedStep !== prevState.selectedStep
        && selectedStep === 2
        && prevState.selectedStep === 3
        && window.innerWidth < 1025
    ) {
      this.scrollToWizardTop(true);
    }
      this.setMaterialsForm();
  }

  componentWillUnmount() {
    this.block = null;
    this.wizardsBlock = null;
    this.uploaderBlock = null;
    this.promoBanner = null;
    this.windowWidth = null;

    window.removeEventListener('resize', () => this.resizeWindow(window.innerWidth));
  }

  setMaterialsForm= () => {
    const {
      sizeValue,
      materialValue,
      sizes,
      materialsToForm,
    } = this.state;
    const { product: { options = [] } } = this.props;

    if (!Object.keys(materialsToForm[0]).length && (!sizeValue || Object.keys(sizeValue).length)&& (!materialValue || Object.keys(materialValue).length) && !Object.keys(sizes).length && options.length) {
    const materials = {};
    const shipKitOptions = options.filter(item => item.name === 'easy_scan_ship_kit');
    shipKitOptions.forEach(({ shipKitMaterial, shipKitSize }) => {
      if (!sizes[shipKitMaterial.id]) {
        sizes[shipKitMaterial.id] = [{ ...shipKitSize, label: shipKitSize.name }];
      } else if (
        sizes[shipKitMaterial.id]
        || !sizes[shipKitMaterial.id].find(({ id }) => id === shipKitSize.id)
      ) {
        sizes[shipKitMaterial.id].push({ ...shipKitSize, label: shipKitSize.name });
      }
      if (!materialsToForm[MATERIALS_ORDER[shipKitMaterial.name.toLowerCase()].order].id) {
        materialsToForm[MATERIALS_ORDER[shipKitMaterial.name.toLowerCase()].order] = {
          name: shipKitMaterial.name,
          id: shipKitMaterial.id,
          img: MATERIALS_ORDER[shipKitMaterial.name.toLowerCase()].img,
        };
      }
      if (!materials[shipKitSize.id]) {
        materials[shipKitSize.id] = [{ ...shipKitMaterial, label: shipKitMaterial.name }];
      }
      if (
        materials[shipKitSize.id]
        && !materials[shipKitSize.id].find(({ id }) => id === shipKitMaterial.id)
      ) {
        materials[shipKitSize.id].push({ ...shipKitMaterial, label: shipKitMaterial.name });
      }
    });

    const shipKitSizes = Object.keys(sizes).reduce((acc, key) => {
      const newSizes = sizes[key].map((item) => {
        const { name } = item;
        const [width, height] = name.split('x');
        return { ...item, width, height };
      }).sort(({ width: firstWidth }, { width: secondWidth }) => firstWidth - secondWidth);
      return { ...acc, [key]: newSizes };
    }, {});

    const clearMaterials = materialsToForm.filter(item => item && item.id);

    const selectMaterial = clearMaterials.length && clearMaterials[0].id ? clearMaterials[0] : null;
    const reult = {
      materialValue: selectMaterial,
      sizeValue: selectMaterial ? shipKitSizes[selectMaterial.id][0] : null,
      sizes: shipKitSizes,
      materialsToForm: clearMaterials,
    }
    this.setState({
      ...reult
    })
    }
  }

  getImageByCloud = () => {
    const { imageIdByFile, token, addImage } = this.props;
    const image = JSON.parse(sessionStorage.getItem('cloudImageData') || '{}');
    sessionStorage.removeItem('cloudImageData');
    const { id, fileId } = image || {};
    if (image && Object.keys(image).length && !id) {
      imageIdByFile({
        value: [fileId],
        token,
      });
      this.selectLoadType('upload', true, true);
    } else if (id) {
      addImage(image);
      this.selectLoadType('upload', true, true);
    }
  }

  checkStorage = () => {
    const data = JSON.parse(sessionStorage.getItem('wizardData') || '{}');
    const { pathname } = data || {};
    const { isRetouching } = this.props;

    const { pathname: path } = window.location;

    if (!data || path !== pathname) {
      const newData = this.getCurrentWizardState();
      sessionStorage.setItem('wizardData', JSON.stringify(newData));
    } else if (path === pathname && !isRetouching) {
      this.updateWizardState({
        ...data,
        selectedStep: data.selectedStep === 0 ? 1 : data.selectedStep,
        maxStep: data.maxStep <= 1 ? 2 : data.maxStep,
      });
    }
    return 0;
  }

  updateWizardState = (data) => {
    const { setImages } = this.props;
    const { orderImagesEnhance, ...rest } = data;
    this.setState({ ...rest });
    setImages(orderImagesEnhance);
  }

  updateStorage = () => {
    const newData = this.getCurrentWizardState();
    sessionStorage.setItem('wizardData', JSON.stringify(newData));
  }

  checkState = (data) => {
    const newData = this.getCurrentWizardState();

    return !isEqual(data, newData);
  }

  getCurrentWizardState = () => {
    const { pathname: path } = window.location;

    const {
      selectedStep,
      selectedLoadType,
      maxStep,
      materialValue,
      sizeValue,
      isShowAddPhoto,
      shipKitCount,
      shipKitColorCount,
      notes,
      colorizationImages,
      isAccept,
      appendOptionEnhance,
    } = this.state;

    const { orderImagesEnhance } = this.props;

    const newData = {
      selectedStep,
      selectedLoadType,
      maxStep,
      materialValue,
      sizeValue,
      isShowAddPhoto: (orderImagesEnhance && orderImagesEnhance.length)
        ? isShowAddPhoto
        : true,
      shipKitCount,
      shipKitColorCount,
      colorizationImages,
      notes,
      isAccept,
      orderImagesEnhance,
      pathname: path,
      appendOptionEnhance,
    };

    return newData;
  }

  onSubmit = () => {
    const {
      addOrder,
      orderImagesEnhance,
      clearImages,
      allPrices: {
        photo_colorization: photoColorization,
        photo_restoration: photoRestoration,
        photo_retouching: photoRetouching,
      } = {},
      typeProduct,
    } = this.props;

    const {
      selectedLoadType,
      sizeValue,
      materialValue,
      notes,
      shipKitColorCount,
      shipKitCount,
      colorizationImages,
      appendOptionEnhance,
    } = this.state;

    const getLocalStorage = (JSON.parse(localStorage.getItem('enhance/Option') || '[]') || []);

    localStorage.setItem('enhance/Option', JSON.stringify([...getLocalStorage, ...orderImagesEnhance]));

    const selectedOption = this.getSelectedOption(sizeValue.id, materialValue.id);
    const {
      id: optionId,
      price,
      additionalDiscount,
      displayName,
      name,
    } = selectedOption || {};

    const isEnhanceProduct = selectedLoadType === 'upload' || selectedLoadType === 'memoyaCloud';

    const colorImages = colorizationImages && colorizationImages
    .filter(({ isSelected }) => isSelected)
    .map(item => item.id);

    const images = orderImagesEnhance && orderImagesEnhance
    .filter(({ isSelected }) => isSelected)
    .map(item => item.id)
    .filter(item => !colorImages.includes(item));

    const restorationOption = appendOptionEnhance.filter((item) => {
      const { options = [] } = typeProduct === RESTORATION_PRODUCT
      ? photoRestoration : photoRetouching || {};
      return options.some(option => option.id === item.optionId);
    });

    const colorizationOption = appendOptionEnhance.filter((item) => {
      const { options = [] } = photoColorization || {};
      return options.some(option => option.id === item.optionId);
    });

    if (
      (images && images.length && isEnhanceProduct)
      || (shipKitCount - shipKitColorCount > 0 && !isEnhanceProduct)
    ) {
      const order = {
        quantity: isEnhanceProduct ? images.length : (shipKitCount - shipKitColorCount || 0),
        typeProduct: typeProduct === RESTORATION_PRODUCT
          ? photoRestoration.name
          : photoRetouching.name,
        productId: typeProduct === RESTORATION_PRODUCT
          ? photoRestoration.id
          : photoRetouching.id,
        isGift: false,
        images,
        options: isEnhanceProduct ? restorationOption : [{
          optionId,
          quantity: 1,
          price,
          additionalDiscount,
          displayName,
          name,
          size: sizeValue.name,
          material: materialValue.name,
        }, ...restorationOption],
        notes,
      };
      addOrder(order);
    }

    if ((isEnhanceProduct && colorImages.length) || (!isEnhanceProduct && shipKitColorCount)) {
      const selectedColorOption = this.getSelectedOption(sizeValue.id, materialValue.id);
      const {
        id: colorOptionId,
        price: colorPrice,
        additionalDiscount: colorAdditionalDiscount,
        colorDisplayName,
        name: colorName,
      } = selectedColorOption;

      const colorProduct = {
        quantity: isEnhanceProduct ? colorImages.length : (shipKitColorCount || 0),
        typeProduct: photoColorization.name,
        productId: photoColorization.id,
        isGift: false,
        images: colorImages,
        options: isEnhanceProduct ? colorizationOption : [{
          optionId: colorOptionId,
          quantity: 1,
          price: colorPrice,
          additionalDiscount: colorAdditionalDiscount,
          displayName: colorDisplayName,
          name: colorName,
          size: sizeValue.name,
          material: materialValue.name,
        }, ...colorizationOption],
        notes,
      };
      addOrder(colorProduct);
    }

    clearImages();
    window.location.href = ROUTES.checkout;
    this.setState({
      selectedStep: 1,
      maxStep: 3,
      colorizationImages: [],
      notes: '',
      shipKitCount: 1,
      shipKitColorCount: 0,
      isAccept: false,
      isShowScanningPopup: false,
      isShowAddPhoto: true,
      appendOptionEnhance: [],
    });
  }

  toggleAddPhotos = () => {
    const { isShowAddPhoto } = this.state;
    this.setState({
      isShowAddPhoto: !isShowAddPhoto,
      colorizationImages: this.getColorizationImages(),
    });
    if (isShowAddPhoto) {
      this.scrollToWizardTop(window.innerWidth > 1024);
    }
  }

  scrollToButton = () => {
    const element = document.getElementById('live-guarantee');
    if (element && element.scrollIntoView) {
      element.scrollIntoView({ block: 'start', behavior: 'smooth' });
    }
  }

  getColorizationImages = () => {
    const { colorizationImages } = this.state;
    const { orderImagesEnhance } = this.props;

    return orderImagesEnhance.reduce((acc, item) => {
      const { isSelected, id } = item;
      if (isSelected) {
        const oldImage = colorizationImages.find(img => img.id === id);
        const newItem = (oldImage && oldImage.id) ? oldImage : { ...item, isSelected: false };
        return [...acc, newItem];
      }
      return acc;
    }, []);
  }

  clickUploadCheck = (uploader) => {
    const { isStepOneClicked } = this.state;
    if (window.innerWidth < 1025 && !isStepOneClicked && uploader) {
      this.setState({ isStepOneClicked: false }, () => { uploader.click(); });
    }
  }

  selectStep = (stepId) => {
    const { maxStep, selectedLoadType } = this.state;
    if (stepId === 1) {
      this.setState({ isStepOneClicked: true });
      this.scrollToWizardTop(true);
    }
    if (maxStep >= stepId && !this.checkDisabledStep(stepId) && selectedLoadType) {
      if (stepId === 2) {
        this.setState({
          selectedStep: stepId,
          maxStep: stepId === maxStep ? stepId + 1 : maxStep,
          colorizationImages: this.getColorizationImages(),
        });
        this.scrollToWizardTop(
          window.innerWidth > 1024 || selectedLoadType === 'shipKit',
        );
      } else {
        this.setState({
          selectedStep: stepId,
          maxStep: stepId === maxStep ? stepId + 1 : maxStep,
        });
      }
    }

    if (stepId === 3 && selectedLoadType === 'shipKit') {
      const {orderImagesEnhance, setImages } = this.props;
      const { shipKitCount } = this.state;
      if (orderImagesEnhance && orderImagesEnhance.length > shipKitCount) {
        setImages(orderImagesEnhance.slice(0,shipKitCount))
      } else {
        this.uploadShipKit();
      }

    }

    if (stepId >= 3) {
      this.scrollToWizardTop(true);
    }
  }

  selectLoadType = (name, isUploadClick, isEnhance = null) => {
    this.setState(
      { selectedLoadType: name },
      () => { this.selectStep(2, isUploadClick, isEnhance); },
    );
  }

  toggleScanningPopup = () => {
    const { isShowScanningPopup } = this.state;
    this.setState({ isShowScanningPopup: !isShowScanningPopup });
  }

  selectImage = (image) => {
    const { toggleImage } = this.props;
    const { colorizationImages } = this.state;
    const { isSelected, id } = image;
    this.setState({
      colorizationImages: isSelected
        ? colorizationImages.filter(item => id !== item.id)
        : [...colorizationImages, { ...image, isSelected: false }],
    },
    () => { toggleImage(id); });
  }

  selectColorizationImage = ({ id }, isSelected) => {
    const { setImages, orderImagesEnhance } = this.props;
    const { colorizationImages, selectedLoadType } = this.state;
    const newImages = colorizationImages.map((item) => {
      const { id: itemId } = item;
      if (itemId === id) {
        return { ...item, isSelected, isColorization: isSelected };
      }
      return item;
    });
    const colorizes = newImages.filter((item) => item.isColorization).length
    this.setState({
      colorizationImages: newImages,
      ...(selectedLoadType === 'shipKit' ? {
        shipKitColorCount: colorizes
      } : {})
    });

    const newOrderImagesEnhance = orderImagesEnhance.map((item) => {
      const { id: itemId } = item;
      if (itemId === id) {
        return { ...item, isColorization: isSelected };
      }
      return item;
    });
    setImages(newOrderImagesEnhance);
  }

  deleteImageFromSlider = (image) => {
    const { deleteImageEn, deleteImagesFromSelector } = this.props;
    deleteImageEn(image.id);
    deleteImagesFromSelector(image);
    this.setState({
      isShowAddPhoto: true,
    });
  }

  uploadShipKit = () => {
    const { uploadImages, token, orderImagesEnhance } = this.props;
    const countImages = orderImagesEnhance ? orderImagesEnhance.length : 0
    const { shipKitCount } = this.state;
    axios
      .get(ShipKitImg.src || ShipKitImg, {
        responseType: 'blob',
        headers: {
          Accept: 'image/jpeg',
        },
      })
      .then((response) => {
        for (let i = 0; i < shipKitCount - countImages; i += 1) {
          const formData = new FormData();
          formData.append('files[]', response.data, 'image.jpeg');
          formData.append('isCropped', false);
          uploadImages({ formData, token, countOfImages: shipKitCount });
        }
      });
  }

  count = () => {
    const { orderImagesEnhance } = this.props;
    return orderImagesEnhance.filter(({ isSelected }) => isSelected).length || 0;
  }

  getPriceInfo = () => {
    const {
      product,
      allPrices: {
        photo_colorization: photoColorization,
        easy_scan_ship_kit: easyScanShipKit,
        photo_restoration: photoRestoration,
        photo_retouching: photoRetouching,
      },
    } = this.props;
    const {
      price: colorPrice,
      displayName: colorDisplayName,
      additionalDiscount: colorAdditionalDiscount,
    } = photoColorization || { price: 0 };
    const { price: easyKitPrice } = easyScanShipKit || {};
    const { displayName, price, additionalDiscount } = product;
    const productCustomPrice = additionalDiscount ? price - additionalDiscount : price;
    const {
      shipKitCount,
      shipKitColorCount,
      colorizationImages,
      selectedLoadType,
      appendOptionEnhance,
    } = this.state;

    const count = selectedLoadType === 'shipKit' ? shipKitCount : this.count();
    const colorCount = selectedLoadType === 'shipKit' ? shipKitColorCount : colorizationImages.filter(({ isSelected }) => isSelected).length || 0;

    const colorCustomPrice = () => {
      if (colorAdditionalDiscount) {
        return colorPrice - colorAdditionalDiscount;
      }
      if (count === colorCount && additionalDiscount) {
        return colorAdditionalDiscount ? (colorPrice - colorAdditionalDiscount - additionalDiscount) : (colorPrice - additionalDiscount);
      }
      return  colorPrice;
    }

    const enhance = [photoRestoration, photoRetouching, photoColorization];

    const colorSum = `Quantity: ${colorCount} = ${convertToPrice(colorCount * colorCustomPrice())}`;
    const productSum = `Quantity: ${count - colorCount} = ${convertToPrice((count - colorCount) * price)}`;

    const getOptionSum = (isFullSum) => appendOptionEnhance.reduce((acc, { quantity, optionId }) => {
      const { options }  = enhance.find(item => {
        return item ? item.options.some(
          ({ id, optionId: optId }) => id === optionId || optId === optionId,
        ) : false
      }) || {};
      const { price: getPrice = 0, additionalDiscount: optionAdditionalDiscount = 0 } = options ? options.find(({ id }) => id === optionId) || {} : {};
      const optionCustomPrice = !isFullSum && optionAdditionalDiscount ? getPrice - optionAdditionalDiscount : getPrice;
      return acc + optionCustomPrice * quantity;
    }, 0);

    const allSum = `$${((count - colorCount) * productCustomPrice + colorCount * colorCustomPrice() + getOptionSum(false)).toFixed(2)}`;
    const fullSum = `$${((count - colorCount) * price + colorCount * colorPrice + getOptionSum(true)).toFixed(2)}`;
    const photoPrice = 'per photo';
    const kitPrice = `per photo + $${easyKitPrice} for the kit`;
    const basePrice = `$${price}`;
    const customPrice = `$${price - additionalDiscount}`;
    return {
      colorSum,
      productSum,
      allSum,
      fullSum,
      displayName,
      colorDisplayName,
      colorCount,
      photoPrice,
      kitPrice,
      basePrice,
      customPrice,
      easyScanShipKit,
    };
  };

  changeCount = isAdd => () => {
    const { shipKitCount: qt } = this.state;
    const inc = isAdd ? 1 : -1;
    const valid = this.validateInputChangeCount(qt, inc);
    this.setState({ shipKitCount: valid });
  }

  onEnterCount = (value) => {
    const valid = this.validateOnEnterCount(value);
    this.setState({ shipKitCount: value === null ? value : valid });
  }

  validateInputChangeCount = (qt, inc, isColorCount) => {
    const { shipKitCount } = this.state;
    const maxQt = isColorCount ? shipKitCount : MAX_QT;
    if ((qt === MIN_QT && inc < 0) || (qt >= maxQt && inc > 0)) {
      return qt;
    }
    return qt + inc;
  }

  validateOnEnterCount = (value, isColorCount) => {
    const { shipKitCount } = this.state;
    const maxQt = isColorCount ? shipKitCount : MAX_QT;

    if (value >= MIN_QT && value <= maxQt) {
      return value;
    }
    return value > maxQt ? maxQt : MIN_QT;
  }

  changeSize = (value) => {
    this.setState({
      sizeValue: value,
    });
  }

  changeMaterial = (value) => {
    const { sizes } = this.state;
    this.setState({
      materialValue: value,
      sizeValue: sizes[value.id][0],
    });
  }

  getSelectedOption = (sizeId, materialId, isColorProduct) => {
    const {
      product: { options = [] },
      allPrices: { photo_colorization: { optionsColor = [] } } = {},
    } = this.props;

    const productOptions = isColorProduct ? optionsColor : options;
    const easyScanOptions = productOptions.filter(item => item.name === 'easy_scan_ship_kit');
    return easyScanOptions.find(({ shipKitMaterial, shipKitSize }) => (
      shipKitMaterial.id === materialId && shipKitSize.id === sizeId
    ));
  }

  changeNotes = ({ value, isValue }) => {
    this.setState({ notes: isValue ? value : '' });
  }

  changeAcceptValue = (value) => {
    this.setState({ isAccept: value });
  }

  renderShipKitControls = () => {
    const {
      materialValue,
      sizeValue,
      sizes,
      shipKitCount,
      materialsToForm,
    } = this.state;
    const { product: { price, additionalDiscount }, typeProduct } = this.props;
    const priceToInput = additionalDiscount && Number(additionalDiscount) ? (
      <BeforeAfterPrice
        price={price}
        additionalDiscount={additionalDiscount}
        textAfter={`${PER} ${REEL}`}
        isRow
      />
    ) : `${convertToPrice(price)} ${PER} ${REEL}`;

    return (
      <EnhanceShipKit
        className={b('ship-kit-block')}
        shipKitCount={shipKitCount}
        onEnterCount={this.onEnterCount}
        changeCount={this.changeCount}
        priceToInput={priceToInput}
        materialsToForm={materialsToForm}
        changeMaterial={this.changeMaterial}
        materialValue={materialValue}
        sizes={sizes}
        sizeValue={sizeValue}
        changeSize={this.changeSize}
        typeProduct={typeProduct}
      />
    );
  }

  renderShipKitAdditionControls = () => {
    const {
      appendOptionEnhance,
      colorizationImages,
      selectedStep,
      selectedLoadType,
      } = this.state;
    const { orderImagesEnhance, typeProduct } = this.props;

    const changeAppendOptions = (value) => {
      this.setState({ appendOptionEnhance: value });
    };

    const isColorization = colorizationImages.reduce((acc, item) => {
      if (item.isColorization) {
        return [...acc, item.id];
      }
      return acc;
    }, []);

    return (
      <NewChooseAnyPhoto
        className={b('ship-kit-block')}
        isColorization={isColorization}
        nameOperation={typeProduct}
        orderImagesEnhance={orderImagesEnhance}
        appendOptionEnhance={appendOptionEnhance}
        changeAppendOptions={changeAppendOptions}
        step={selectedStep}
        isShipKit={selectedLoadType === 'shipKit'}
      />
    );
  }

  generateTemplateImages = (number) => {
    const newTemplate = [];
    for (let i = 0; i < number; i += 1) {
      newTemplate.push({
        isTemplate: true,
        id: `template_${i}`,
      });
    }
    return number ? newTemplate : [];
  }

  getSecondStep = () => {
    const {
      selectedLoadType,
      isShowAddPhoto,
      colorizationImages,
      selectedStep,
    } = this.state;
    const {
      orderImagesEnhance,
      product: { price },
      numberOfUploadImages,
      isRetouching,
    } = this.props;
    switch (selectedLoadType) {
      case 'upload': {
        if (selectedStep === 3) return this.renderShipKitAdditionControls();
        const photos = [
          ...orderImagesEnhance,
        ];
        return (
          <div className={b('second-step-wrap')}>
            <ImageUploader
              className={b('upload-block', { 'with-images': Boolean(photos && photos.length) })}
              scanHelpClass={b('scan-help-button')}
              titleClass={b('upload-block-title')}
              isOrderEnhance
              toggleScanningPopup={this.toggleScanningPopup}
              withoutText={isShowAddPhoto && photos.length}
              uploadTitle={STEP_TITLE}
              onlyTitle
              buttonBackgroundColor={burntSiennaRedColor}
              buttonRefFunc={this.clickUploadCheck}
              buttonText={UPLOAD_BUTTON_TEXT}
            />
            <ImageSelect
              className={b('selector-block')}
              orderImagesEnhance={photos}
              deleteImageFromSlider={this.deleteImageFromSlider}
              price={price}
                // selectImage={this.selectImage}
              openSelectBlock={this.toggleAddPhotos}
              withoutCount={isShowAddPhoto && photos.length}
              onlyTitle
              selectTitle={STEP_TWO_SELECT_TITLE}
              withDeleteBtn
              countText={PER_PHOTO}
              quantityText={RESTORE_QUANTITY_TEXT}
              colorizationImages={colorizationImages}
              selectColorizationImage={this.selectColorizationImage}
              isRetouching={isRetouching}
            />
          </div>
        );
      }

      case 'shipKit': {
        const photos = [
          ...orderImagesEnhance,
          ...this.generateTemplateImages(numberOfUploadImages),
        ];
        if (selectedStep === 3) {
          return (
            <div className={b('second-step-wrap')}>
              <div
                className={b('ship-kit-upload-block')}
                tabIndex={0}
                role="button"
                onClick={() => {
                  this.selectStep(selectedStep - 1);
                }}
              >
                <BlueButton className={b('ship-kit-upload-btn')}>
                  {ADD_MORE_PHOTOS}
                </BlueButton>
              </div>
              <ImageSelect
                className={b('selector-block')}
                orderImagesEnhance={orderImagesEnhance}
                deleteImageFromSlider={this.deleteImageFromSlider}
                price={price}
                  // selectImage={this.selectImage}
                openSelectBlock={this.toggleAddPhotos}
                withoutCount={isShowAddPhoto && photos.length}
                onlyTitle
                selectTitle={STEP_TWO_SELECT_TITLE}
                withDeleteBtn
                countText={PER_PHOTO}
                quantityText={RESTORE_QUANTITY_TEXT}
                colorizationImages={colorizationImages}
                selectColorizationImage={this.selectColorizationImage}
                isRetouching={isRetouching}
                isShipKit={selectedLoadType === 'shipKit'}
              />
              {
                  selectedLoadType === 'shipKit' && selectedStep !== 1 ? (
                    <div className={b('labels-include')}>{LABELS_INCLUDE}</div>
                  ) : null
                }
            </div>
          );
        }
        if (selectedStep === 4) {
          return this.renderShipKitAdditionControls();
        }
        return this.renderShipKitControls();
      }

      default: {
        return null;
      }
    }
  }

  checkDisabledStep = (stepId) => {
    const { orderImagesEnhance } = this.props;
    const { selectedLoadType, shipKitCount } = this.state;

    return Boolean(stepId >= STEPS[2].id
     && ((selectedLoadType !== 'shipKit' && (!orderImagesEnhance || !orderImagesEnhance.length))
    || (selectedLoadType === 'shipKit' && !shipKitCount && (!orderImagesEnhance || !orderImagesEnhance.length))));
  }

  toggleHint = (value, id) => {
    const { isHintOpen } = this.state;
    if (value) {
      this.setState({ isHintOpen: !isHintOpen });
    } else {
      this.setState({
        isHintOpen: true,
        selectedHintId: id,
      });
    }
  }

  getButtonText = (name) => {
    switch (name) {
      case 'upload':
        return 'Upload now';
      case 'shipKit':
        return 'Order kit';
      default:
        return '';
    }
  }

  saveRef = (e) => {
    const { refFunc } = this.props;
    this.block = e;
    refFunc(e);
  }

  getTitle = () => {
    const {
      selectedStep,
      isShowAddPhoto,
      selectedLoadType,
    } = this.state;
    const {
      orderImagesEnhance,
      numberOfUploadImages,
      typeProduct,
    } = this.props;
    const photos = [
      ...orderImagesEnhance,
      ...this.generateTemplateImages(numberOfUploadImages),
    ];

    const firstStepTitle = typeProduct === RESTORATION_PRODUCT
      ? FIRST_STEP_TITLE
      : FIRST_STEP_TITLE_RETOUCHING;

    const isRestoration = typeProduct === RESTORATION_PRODUCT;
    const chooseRestorationTitle = this.windowWidth > 1024 ? CHOOSE_ANY_PHOTO_RESTORATION : CHOOSE_ANY_PHOTO_RESTORATION_MOBILE;
    switch (selectedLoadType) {
      case 'upload':
      case 'memoyaCloud':
        if (selectedStep === 2 && !photos.length && isShowAddPhoto) return STEP_TITLE;
        if (selectedStep === 2 && photos.length) return STEP_TWO_SELECT_TITLE;
        if (selectedStep === 3) {
          return isRestoration ? chooseRestorationTitle : CHOOSE_ANY_PHOTO_RETOUCHING;
        }
        return firstStepTitle;
      case 'shipKit':
        if (selectedStep === 2) return WHAT_SIZE;
        if (selectedStep === 3) return SHIP_KIT_3_STEP;
        if (selectedStep === 4) return this.windowWidth > 1024 ? SHIP_KIT_4_STEP : SHIP_KIT_4_STEP_MOBILE;
        return firstStepTitle;
      default:
        return firstStepTitle;
    }
  }

  getButtonColor = (name) => {
    switch (name) {
      case 'upload':
        return this.windowWidth > 580 ? slateGrayColor: pickledBluewoodColor;
      case 'shipKit':
        return this.windowWidth > 580 ? summerGreenColor : apricotColor;
      default:
        return '';
    }
  }

  getCount = () => {
    const { orderImagesEnhance } = this.props;
    return (
      orderImagesEnhance.filter(({ isSelected }) => isSelected).length || 0
    );
  };

  setColorizationImages = () => {
    this.setState({
      colorizationImages: this.getColorizationImages(),
    });
  }

  toggleNotePopup = (value) => {
    this.setState({ isShowProductNote: value });
  }

  getFirstStepCards = () => {
    const { steps } = this.props;
    const firstStepCards = FIRST_STEP_ITEMS.map((item, index) => {
      const {
        id,
        name,
        title,
        description,
        imageUri,
      } = item;
      const stepCard =
        steps &&
        steps.length &&
        steps[0] &&
        steps[0].orderSections.length &&
        steps[0].orderSections[index]
          ? {
              title: steps[0].orderSections[index].title,
              description: steps[0].orderSections[index].description,
              imageUri:
                steps[0].orderSections[index].images[
                  steps[0].orderSections[index].images.length - 1
                ],
            }
          : {
              title,
              description,
              imageUri,
            };

      return {
        id,
        name,
        ...stepCard,
      };
    });
    return firstStepCards;
  }

  getHelpCenters = () => {
    const { steps } = this.props;
    const { selectedStep } = this.state;
    const { helpCenters } = steps.length && steps[selectedStep - 1]
      ? steps[selectedStep - 1]
      : { helpCenters: [] };
    return helpCenters;
  }

  scrollToWizardTop = (isScrollToWizard) => {
    const promoBanner = document.getElementById('promo-banner');
    const naviBlock = document.getElementById('navigation');
    const bannerBottom = promoBanner ? promoBanner.getBoundingClientRect().height : 0;
    const naviBottom = naviBlock ? naviBlock.getBoundingClientRect().height : 0;
    // const targetBlock = isScrollToWizard ? this.wizardsBlock : this.uploaderBlock;
    const targetBlock = isScrollToWizard ? this.wizardsBlock : this.wizardsBlock;
    const correction = this.windowWidth < 1024 ? 0 : 30;
    if (this.wizardsBlock) {
      const { top } = targetBlock.getBoundingClientRect();
      window.scrollTo({
        top: document.documentElement.scrollTop + top - naviBottom - bannerBottom + correction,
        behavior: 'smooth',
      });
    }
  }

  render() {
    const {
      className,
      orderImagesEnhance,
      product: { price, additionalDiscount },
      typeProduct,
      isSvg,
      bannerIcons,
      anchorWizard,
      scrollRef,
      isRetouching,
    } = this.props;
    const {
      selectedStep,
      selectedLoadType,
      isShowScanningPopup,
      shipKitCount,
      maxStep,
      isHintOpen,
      selectedHintId,
      isShowProductNote,
      notes,
    } = this.state;

    const {
      allSum,
      fullSum,
      photoPrice,
      kitPrice,
      basePrice,
      customPrice,
      easyScanShipKit,
    } = this.getPriceInfo();

    const firstStepCards = this.getFirstStepCards();
    const helpCenters = this.getHelpCenters();
    const isShowNextBtn = selectedStep !== STEPS.length && selectedStep === 2
    && ((orderImagesEnhance.length && selectedLoadType !== 'shipKit')
     || (selectedLoadType === 'shipKit' && selectedStep !== 1))
     || (selectedStep === 3 || selectedStep === "3") && selectedLoadType === 'shipKit' ;

    const priceToInput = additionalDiscount && Number(additionalDiscount) ? (
      <BeforeAfterPrice
        price={price}
        additionalDiscount={additionalDiscount}
        textAfter={`${PER} ${REEL}`}
        isRow
      />
    ) : `${convertToPrice(price)} ${PER} ${REEL}`;

    const wizardTitle = typeProduct === RESTORATION_PRODUCT
      ? WIZARD_TITLE
      : WIZARD_TITLE_RETOUCHING;

    const wizardTitleDescription = typeProduct === RESTORATION_PRODUCT
      ? WIZARD_TITLE_DESCRIPTION
      : WIZARD_TITLE_DESCRIPTION_RETOUCHING;

    const isRestoration = typeProduct === 'photo_restoration';

    return (
      <section className={b({ mix: className, contrast: true })} ref={this.saveRef}>
        <div className={b('title-wrapper')}>
          <h2 className={b('title')}>{wizardTitle}</h2>
          <div className={b('title-description')}>
            {wizardTitleDescription}
          </div>
          <LazyLoad offset={100}>
            <div className={b('icons-block')}>
              {bannerIcons.map(({ text, img, svg: Svg, color }) => (
                <div
                  className={b('icon-block', { retouching: isRetouching })}
                  key={text}
                  style={{
                    ...(color ? { backgroundColor: color } : {}),
                  }}
                >
                  {isSvg ? (
                    <Svg className={b('icon')} width="80" height="80" />
                  ) : (
                    <img className={b('icon')} src={img.src || img} alt={text} />
                  )}
                  <span className={b('icon-block-text')}>{text}</span>
                </div>
              ))}
            </div>
          </LazyLoad>
        </div>
        <div
          className={b('content-wrap')}
          ref={(e) => {
            this.wizardsBlock = e;
            scrollRef(e);
          }}
          id="enhance-wizard"
          name={anchorWizard}
        >
          <LazyLoad offset={100}>
            <div className={b('top-block')}>
              <div className={b('help-center-container')}>
                <HelpCenter
                  className={b('help-center-wrap')}
                  list={helpCenters}
                  name={selectedLoadType}
                  step={selectedStep}
                />
              </div>
              <span className={b('step-title')}>{this.getTitle()}</span>
              {(Boolean(this.count() && selectedStep !== 1) ||
                (Boolean(shipKitCount) &&
                  maxStep > 2 &&
                  selectedLoadType === 'shipKit' &&
                  selectedStep !== 1)) && (
                  <div className={b('summery-block', { desktop: true })}>
                    <div className={b('summary-container')}>
                      <DollarIcon />
                      <div className={b('summary-title')}>{SUBTOTAL}</div>
                      <div className={b('summary-text-wrapper')}>
                        <div
                          className={b('summary-text', {
                          origin: allSum !== fullSum || additionalDiscount,
                        })}
                        >
                          {fullSum}
                        </div>
                        {(allSum !== fullSum || Boolean(additionalDiscount)) && (
                        <div className={b('summary-text')}>{allSum}</div>
                      )}
                      </div>
                    </div>
                  </div>
              )}
            </div>
            <div className={b('main-block')}>
              <div
                className={b('main-content')}
                ref={(e) => {
                  this.uploaderBlock = e;
                }}
              >
                {selectedStep === 1 && (
                  <div className={b('first-step')}>
                    <div className={b('step-cards')}>
                      {firstStepCards.map(({ title, description, id, imageUri, name }, index) => (
                        <div className={b('card-wrap')} key={`product_${id}`}>
                          <ProductCard
                            title={title}
                            text={description}
                            id={id}
                            img={imageUri}
                            name={name}
                            selectedCard={selectedLoadType}
                            select={(e) => {
                              this.selectLoadType(e, name === 'upload');
                            }}
                            isHintOpen={isHintOpen}
                            toggleHint={this.toggleHint}
                            selectedHintId={selectedHintId}
                            selectButtonText={this.getButtonText(name)}
                            imageClassName={b('card-image')}
                            priceText={name === 'shipKit' ? kitPrice : photoPrice}
                            easyScanShipKit={easyScanShipKit}
                            basePrice={basePrice}
                            customPrice={customPrice}
                            backGroundColor={this.getButtonColor(name)}
                            windowWidth={this.windowWidth}
                            firstOrigin={index === 0}
                          />
                        </div>
                      ))}
                    </div>
                  </div>
                )}
                {selectedStep !== 1 && this.getSecondStep()}
              </div>
            </div>
            <div className={b('bottom-block')}>
              {selectedStep !== 1 &&
                ((orderImagesEnhance.length && selectedLoadType !== 'shipKit') ||
                  (selectedLoadType === 'shipKit' && selectedStep !== 2)) && (
                  <div className={b('count-block', { mobile: true })}>
                    <div className={b('count-wrap')}>
                      <span className={b('quantity-wrapper')}>
                        <span className={b('quantity-title')}>
                          {`${RESTORE_QUANTITY_TEXT(
                          isRestoration,
                        )}:`}
                        </span>
                        <span className={b('quantity')}>{this.getCount()}</span>
                      </span>
                      <div className={b('quantity-price')}>{priceToInput}</div>
                    </div>
                  </div>
                )}
              {selectedStep !== 1 && (
                <div className={b('summery-block', { mobile: true })}>
                  <div className={b('summary-container')}>
                    <DollarIcon />
                    <div className={b('summary-title')}>{SUBTOTAL}</div>
                    <div className={b('summary-text-wrapper')}>
                      <div className={b('summary-text', { origin: allSum !== fullSum })}>
                        {fullSum}
                      </div>
                      {allSum !== fullSum && <div className={b('summary-text')}>{allSum}</div>}
                    </div>
                  </div>
                </div>
              )}
              <div className={b('bottom-content')}>
                {selectedStep !== 1 ? (
                  <div className={b('back-btn')}>
                    <ColorButton
                      text={BACK}
                      withArrow
                      withReversIcon
                      onClick={() => {
                        this.selectStep(selectedStep - 1);
                      }}
                    />
                  </div>
                ) : null}
                {selectedStep !== 1 &&
                  ((orderImagesEnhance.length && selectedLoadType !== 'shipKit') ||
                    selectedLoadType === 'shipKit') && (
                    <>
                      {isShowNextBtn ? (
                        <ColorButton
                          text={NEXT}
                          withArrow
                          withIcon
                          onClick={() => {
                            this.selectStep(selectedStep + 1);
                          }}
                          backGroundColor={burntSiennaRedColor}
                        />
                      ) : (
                        <ColorButton
                          text={ADD_TO_CART}
                          onClick={this.onSubmit}
                          backGroundColor={burntSiennaRedColor}
                        />
                      )}
                    </>
                  )}
              </div>
              <div className={b('help-center-container')}>
                <HelpCenter
                  className={b('help-center-wrap')}
                  list={helpCenters}
                  name={selectedLoadType}
                  step={selectedStep}
                />
              </div>
            </div>
            {isShowScanningPopup && (
              <div className={b('scanning-popup')}>
                <div className={b('scan-block')}>
                  <button
                    className={b('close-btn')}
                    type="button"
                    onClick={this.toggleScanningPopup}
                  >
                    <CloseIcon2 />
                  </button>
                  <div className={b('col')}>
                    <h2 className={b('scan-title', { mix: 'title' })}>{SCANNING_TITLE}</h2>
                    <span className={b('scan-text', { scan: true })}>{SCANNING_DESCRIPTION}</span>
                    <ul className={b('scan-list')}>
                      {SCANNING_INFO_LIST.map(({ id, text }) => (
                        <li className={b('scan-list-item')} key={id}>
                          {text}
                        </li>
                      ))}
                    </ul>
                  </div>
                </div>
              </div>
            )}
          </LazyLoad>
        </div>
        {isShowProductNote && (
          <ProductNotesPopup
            portalId="enhance-wizard"
            close={() => this.toggleNotePopup(false)}
            title={NOTE_TITLE}
            description={NOTE_DESCRIPTION}
            placeholder={NOTE_PLACEHOLDER}
            buttonText={NOTE_BUTTON_TEXT}
            submit={this.changeNotes}
            notes={notes}
            buttonBackgroundColor={orangeColor}
            iconColor={orangeColor}
            cancelText={COLOR_BUTTON_CANCEL}
            cancelBackgroundColor={greyColor}
          />
        )}
      </section>
    );
  }
}

RestorationWizard.propTypes = propTypes;
RestorationWizard.defaultProps = defaultProps;

const stateProps = state => ({
  orderImagesEnhance: orderImagesEnhanceSelector(state),
  isLoadingImages: isLoadingEnhanceImagesSelector(state),
  allPrices: orderPriceSelect(state),
  restorationCards: wizardRestorationCardsSelector(state),
  numberOfUploadImages: numberOfUploadEnhanceImagesSelector(state),
  enhanceErrors: enhanceErrorsSelector(state),
  hint: wizardHintSelector(state),
  steps: wizardCardsSelector(state),
  token: getToken(state),
});

const actions = {
  toggleImage: toggleImageEnhance,
  deleteImageEn: deleteImageEnhance,
  clearEnhance: clearEnhanceErrors,
  clearImages: clearEnhanceImages,
  addOrder: addToCart,
  setImages: setEnhanceImages,
  deleteImagesFromSelector: deleteImage,
  getWizard: getWizardCardsAction,
  getOptions: getOptionEnhance,
  imageIdByFile: getOrderImageIdByFile,
  addImage: addEnhanceImage,
  uploadImages: loadImagesOrderEnhance,

};

export default connect(stateProps, actions)(RestorationWizard);
