import React from 'react';
import bem from 'src/utils/bem';
import PropTypes from 'prop-types';
import ROUTES from 'src/constants/routes';

import dynamic from 'next/dynamic';
import {
  DESCRIPTION,
  FIRST_BTN_TEXT,
  FIRST_DESCRIPTION,
  FIRST_IMAGE,
  FIRST_TITLE,
  SECOND_BTN_TEXT,
  SECOND_DESCRIPTION,
  SECOND_IMAGE,
  SECOND_TITLE,
  TITLE,
} from './constants';
import styles from './index.module.scss';

const ColorButton = dynamic(() => import('src/components/buttons/ColorButton'));

const { contact, digitize } = ROUTES;

const b = bem('visit-block', styles);

const defaultProps = {
  className: '',
  title: TITLE,
  description: DESCRIPTION,
};

const propTypes = {
  className: PropTypes.string,
  title: PropTypes.string,
  description: PropTypes.string,
};

const VisitBlock = ({
  className,
  description,
  title,
}) => {

  return (
    <div className={b({ mix: className })}>
      <h2 className={b('title')}>{title}</h2>
      {description && <div className={b('description')}>{description}</div>}
      <div className={b('container')}>
        <div className={b('square')} />
        <div className={b('element')}>
          <div className={b('first-element-square')} />
          <div className={b('first-element-stripe')} />
          <img className={b('image')} src={FIRST_IMAGE.src} alt="Visit our Boca Raton headquarters" />
          <div className={b('element-title')}>{FIRST_TITLE}</div>
          <div className={b('element-description')}>{FIRST_DESCRIPTION}</div>
          <a className={b('element-button')} href={contact}>
            <ColorButton text={FIRST_BTN_TEXT} />
          </a>
        </div>
        <div className={b('element')}>
          <div className={b('second-element-square')} />
          <div className={b('second-element-stripe')} />
          <img className={b('image')} src={SECOND_IMAGE.src} alt="Local home pickup & dropoff service" />
          <div className={b('element-title')}>{SECOND_TITLE}</div>
          <div className={b('element-description')}>{SECOND_DESCRIPTION}</div>
          <a className={b('element-button')} href={digitize}>
            <ColorButton text={SECOND_BTN_TEXT} />
          </a>
        </div>
      </div>
    </div>
  );
}

VisitBlock.propTypes = propTypes;
VisitBlock.defaultProps = defaultProps;
export default VisitBlock;
