import FIRST_IMAGE from  'public/img/digitize/LOCAL-STORE-SCANS.jpeg';
import SECOND_IMAGE from  'public/img/digitize/LOCAL-PICK-UP.jpeg';

const TITLE = 'Visit our Boca Raton transfer facility or schedule a local\npickup service in South Florida.';

const DESCRIPTION = 'No matter where you are in South Florida, we make it easy & convenient to locally digitize your home movie tapes,\nphotos and family memories. We would love for you to visit our Boca Raton headquarters to meet with your\npersonal concierge, tour our transfer facility & see our expert technicians at work with our white glove media\ntransfer service. For your convenience, one of our licensed and insured team members can come to your home to\npersonally pick up your home movies & photo albums curbside to digitize & preserve.'

const FIRST_BTN_TEXT = 'Get directions';
const FIRST_TITLE = 'Visit our Boca Raton\nheadquarters';
const FIRST_DESCRIPTION ='Come meet with your personal concierge\nabout your transfer project & tour our\nstate-of-the-art transfer facility.';

const SECOND_BTN_TEXT = 'Order online';
const SECOND_TITLE = 'Local home pickup\n& dropoff service';
const SECOND_DESCRIPTION ='One of our licensed & insured *******\nStudios team members will come directly to\nyour home or office to pick up your memories.';


export {
  TITLE,
  DESCRIPTION,
  FIRST_BTN_TEXT,
  FIRST_TITLE,
  FIRST_DESCRIPTION,
  FIRST_IMAGE,
  SECOND_BTN_TEXT,
  SECOND_TITLE,
  SECOND_DESCRIPTION,
  SECOND_IMAGE,
};
