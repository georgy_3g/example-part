import React from 'react';
import PropTypes from 'prop-types';
import Slider from 'react-slick';
import bem from 'src/utils/bem';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import dynamic from "next/dynamic";
import styles from './index.module.scss';

const BigPhotoCard = dynamic(() => import('src/components/cards/BigPhotoCard'));
const BigVideoCard = dynamic(() => import('src/components/cards/BigVideoCard'));
const BigAudioCard = dynamic(() => import('src/components/cards/BigAudioCard'));
const ArrowSlider = dynamic(() => import('src/components/elements/ArrowSlider'));

const b = bem('image-slider', styles);
function ArrowNext(props) {
  return <ArrowSlider prefix="next" block={b} {...props} />;
}
function ArrowPrev(props) {
  return <ArrowSlider prefix="prev" block={b} {...props} />;
}

const propTypes = {
  selected: PropTypes.arrayOf(PropTypes.shape({})),
  addToSelected: PropTypes.func,
  removeFromSelected: PropTypes.func,
  cards: PropTypes.arrayOf(PropTypes.shape({})),
  forSharedPage: PropTypes.bool,
  enhancePhoto: PropTypes.func,
  selectTailView: PropTypes.func,
  afterChange: PropTypes.func,
  orderPrints: PropTypes.func,
  orderId: PropTypes.number,
  cloud: PropTypes.bool,
};

const defaultProps = {
  selected: null,
  addToSelected: () => {},
  removeFromSelected: () => {},
  cards: [],
  forSharedPage: false,
  enhancePhoto: () => {},
  selectTailView: () => {},
  afterChange: () => {},
  orderPrints: () => {},
  orderId: 0,
  cloud: false,
};

function ImageSlider(props) {
  const {
    addToSelected,
    removeFromSelected,
    selected,
    cards,
    forSharedPage,
    enhancePhoto,
    selectTailView,
    afterChange,
    orderPrints,
    orderId,
    cloud,
  } = props;

  const settings = {
    dots: false,
    infinite: false,
    focusOnSelect: true,
    lazyLoad: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 1380,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: 900,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
    nextArrow: <ArrowNext />,
    prevArrow: <ArrowPrev />,
    afterChange,
  };
  return (
    <div className={b()}>
      <Slider {...settings}>
        {cards.map((item) => (
          <div className={b('slide-wrapper')} key={item.id}>
            {item.imageUri && (
              <BigPhotoCard
                cloud={cloud}
                data={item}
                addToSelected={addToSelected}
                removeFromSelected={removeFromSelected}
                orderPrints={orderPrints}
                selectedArr={selected}
                className={b('slide')}
                forSharedPage={forSharedPage}
                enhancePhoto={enhancePhoto}
                selectTailView={selectTailView}
                fromMyFiles
                orderId={orderId}
                withoutText
              />
            )}
            {item.videoUri && (
              <BigVideoCard
                fileData={item}
                addToSelected={addToSelected}
                removeFromSelected={removeFromSelected}
                selected={selected}
                className={b('slide')}
                forSharedPage={forSharedPage}
                selectTailView={selectTailView}
              />
            )}
            {item.audioUri && (
              <BigAudioCard
                fileData={item}
                addToSelected={addToSelected}
                removeFromSelected={removeFromSelected}
                selected={selected}
                className={b('slide')}
                forSharedPage={forSharedPage}
                selectTailView={selectTailView}
              />
            )}
          </div>
        ))}
      </Slider>
    </div>
  );
}
ImageSlider.propTypes = propTypes;
ImageSlider.defaultProps = defaultProps;
export default ImageSlider;
