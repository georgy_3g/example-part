import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Slider from 'react-slick';
import bem from 'src/utils/bem';
import debounce from 'lodash/debounce';
import Image from 'next/image';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import dynamic from 'next/dynamic';
import styles from './index.module.scss';

const ArrowInSquareIcon = dynamic(() => import('src/components/svg/ArrowInSquareIcon'));
const BlueButton = dynamic(() => import('src/components/buttons/BlueButton'));
const YouTubeVideo = dynamic(() => import('src/components/widgets/YouTubeVideo'));

const b = bem('image-slider-with-video', styles);

const propTypes = {
  slideList: PropTypes.arrayOf(PropTypes.shape()),
  dots: PropTypes.bool,
  arrows: PropTypes.bool,
  slidesToShow: PropTypes.number,
  slidesToScroll: PropTypes.number,
  smallShow: PropTypes.number,
  miniShow: PropTypes.number,
  smallScroll: PropTypes.number,
  className: PropTypes.string,
  imageClass: PropTypes.string,
  beforeChange: PropTypes.func,
  lazyLoad: PropTypes.bool,
  afterChange: PropTypes.func,
  autoplay: PropTypes.bool,
  pauseOnHover: PropTypes.bool,
  autoplaySpeed: PropTypes.number,
  withOutTitle: PropTypes.bool,
  compareImageClass: PropTypes.string,
  infinite: PropTypes.bool,
  autoPlayVideo: PropTypes.bool,
  withScrollButton: PropTypes.bool,
  scrollButtonText: PropTypes.string,
  scroll: PropTypes.func,
  customClassBtn: PropTypes.string,
  showVideo: PropTypes.bool,
};
const defaultProps = {
  slideList: [],
  dots: false,
  arrows: false,
  slidesToShow: 3,
  slidesToScroll: 1,
  smallShow: 2,
  miniShow: 1,
  smallScroll: 1,
  className: '',
  imageClass: '',
  beforeChange: () => {},
  afterChange: () => {},
  lazyLoad: true,
  autoplay: false,
  pauseOnHover: false,
  autoplaySpeed: 5000,
  withOutTitle: false,
  compareImageClass: '',
  infinite: true,
  autoPlayVideo: false,
  withScrollButton: false,
  scrollButtonText: '',
  scroll: () => {},
  customClassBtn: '',
  showVideo: false,
};

class ImageSliderWithVideo extends Component {
  constructor(props) {
    super(props);
    this.slider = null;
    this.state = {
      index: 0,
    };
  }

  componentWillUnmount() {
    this.slider = null;
    this.isOnclick = null;
  }

  updateCount = (valueOne, valueTwo) => {
    const { beforeChange } = this.props;
    beforeChange(valueTwo);
    this.setState({ index: valueTwo });
  };

  settings = () => {
    const {
      arrows,
      slidesToShow,
      slidesToScroll,
      smallShow,
      smallScroll,
      afterChange,
      lazyLoad,
      autoplay,
      pauseOnHover,
      autoplaySpeed,
      miniShow,
      infinite,
    } = this.props;
    return {
      arrows,
      infinite,
      focusOnSelect: false,
      lazyLoad,
      speed: 500,
      slidesToShow,
      slidesToScroll,
      beforeChange: this.updateCount,
      afterChange,
      responsive: [
        {
          breakpoint: 1366,
          settings: {
            slidesToShow,
            slidesToScroll: smallScroll,
          },
        },
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: smallShow,
            slidesToScroll: smallScroll,
          },
        },
        {
          breakpoint: 768,
          settings: {
            slidesToShow: smallShow,
            slidesToScroll: smallScroll,
          },
        },
        {
          breakpoint: 750,
          settings: {
            slidesToShow: miniShow,
            slidesToScroll: smallScroll,
          },
        },
      ],
      dotsClass: b('dots'),
      autoplay,
      pauseOnHover,
      autoplaySpeed,
      swipe: false,
    };
  };

  generateDots = () => {
    const { slideList } = this.props;
    const { index } = this.state;
    return slideList.map((item, i) => {
      const { url } = item;
      return {
        isSelect: index === i,
        key: `${i}-${url}`,
        i,
      };
    });
  };

  clickToDot = (value) => {
    this.isOnclick = false;
    this.slider.slickGoTo(value);
    this.waitPlay();
  };

  clickNext = () => {
    this.isOnclick = false;
    this.slider.slickNext();
    this.waitPlay();
  };

  clickPrev = () => {
    this.isOnclick = false;
    this.slider.slickPrev();
    this.waitPlay();
  };

  clickPause = () => {
    this.slider.slickPause();
  };

  clickYouTube = () => {
    this.slider.slickPause();
    this.isOnclick = true;
  };

  clickPlay = () => {
    if (this.slider !== null) {
      this.slider.slickPlay();
    }
  };

  waitPlay = () => {
    const { autoplay } = this.props;
    if (autoplay) {
      this.clickPause();
      const debounceFunc = debounce(() => {
        if (!this.isOnclick) {
          this.clickPlay();
        }
      }, 4000);
      debounceFunc();
    }
  };

  render() {
    const {
      slideList,
      className,
      autoPlayVideo,
      withScrollButton,
      scrollButtonText,
      scroll,
      customClassBtn,
      showVideo,
    } = this.props;

    return (
      <div className={b({ mix: className })}>
        <Slider
          ref={(c) => {
            this.slider = c;
          }}
          {...this.settings()}
        >
          {slideList.map((item) => (
            <div className={b('slide-image-wrapper')} key={`${item.id}_${item.url}`}>
              <div className={b('block-video')}>
                {item.type === 'video' && showVideo ? (
                  <YouTubeVideo
                    className={b('video')}
                    contentIframe={item.url}
                    clickPause={this.clickPause}
                    autoPlay={autoPlayVideo}
                  />
                ) : (
                  <div
                    className={b('image')}
                    role="button"
                    tabIndex={-1}
                    onClick={() => this.selectFile(item.id, item.type)}
                  >
                    <Image
                      src={item.videoAlt ? item.videoAlt : item.url}
                      alt={item.alt}
                      className={b('image')}
                      loading="lazy"
                      layout="responsive"
                      width={1}
                      height={1}
                    />
                  </div>
                )}
              </div>
              <p className={b('slide-image-text')}>{item.text}</p>
              <div className={b('slide-image-title')}>{item.title}</div>
            </div>
          ))}
        </Slider>
        <div className={b('controls')}>
          <button className={b('arrow-btn', { prev: true })} type="button" onClick={this.clickPrev}>
            <ArrowInSquareIcon className={b('arrow-icon', { prev: true })} />
          </button>
          <div className={b('dot-block')}>
            {this.generateDots().map(({ isSelect, key, i }) => (
              <button
                className={b('dot-wrap', { select: isSelect })}
                key={key}
                type="button"
                onClick={() => {
                  this.clickToDot(i);
                }}
              >
                <div className={b('dot', { select: isSelect })} />
              </button>
            ))}
          </div>
          <button className={b('arrow-btn', { next: true })} type="button" onClick={this.clickNext}>
            <ArrowInSquareIcon className={b('arrow-icon', { next: true })} />
          </button>
        </div>
        {withScrollButton && scrollButtonText && (
          <BlueButton className={b('scroll-button', { mix: customClassBtn })} onClick={scroll}>
            {scrollButtonText}
          </BlueButton>
        )}
      </div>
    );
  }
}

ImageSliderWithVideo.propTypes = propTypes;
ImageSliderWithVideo.defaultProps = defaultProps;

export default ImageSliderWithVideo;
