const RENEW_NOW = 'Renew now';
const UPGRADE_NOW = 'Upgrade now';
const DAYS_REMAINING = 'Days Remaining';
const DAY_REMAINING = 'day remaining';

export { RENEW_NOW, UPGRADE_NOW, DAYS_REMAINING, DAY_REMAINING };
