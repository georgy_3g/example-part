import React from 'react';
import bem from 'src/utils/bem';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import icon from 'public/img/shared/memoya-symbol-lock.jpg';
import dynamic from 'next/dynamic';
import { cloudStatus, subscriptionSelector } from 'src/redux/myCloud/selectors';
import { ordersListSelect } from 'src/redux/ordersList/selectors';
import { DAY_REMAINING, DAYS_REMAINING, RENEW_NOW, UPGRADE_NOW } from './constants';
import styles from './index.module.scss';

const InfoIcon = dynamic(() => import('src/components/svg/InfoIcon'));


const b = bem('cloud-day-counter', styles);
const propTypes = {
  className: PropTypes.string,
  subscription: PropTypes.string,
  value: PropTypes.number,
  openSubscribePopup: PropTypes.func,
  ordersList: PropTypes.arrayOf(PropTypes.shape({})),
  openHint: PropTypes.func,
};

const defaultProps = {
  className: '',
  subscription: '',
  value: 0,
  openSubscribePopup: () => {},
  ordersList: [],
  openHint: () => {},
};

const CloudDayCounter = (props) => {
  const { className, value, openSubscribePopup, ordersList, openHint, subscription } = props;

  const progressWidth = (value / 30) * 100;

  return (
    <div className={b({ mix: className })}>
      <div className={b('content-block')}>
        <div className={b('count-bar-wrapper')}>
          <div className={b('count-bar')}>
            <div className={b('progress')} style={{ width: `${progressWidth}%` }} />
            <span className={b('count')}>
              {`${value} ${value === 1 ? DAY_REMAINING : DAYS_REMAINING}`}
            </span>
          </div>
          <div className={b('hint-wrap')} role="button" tabIndex={-1} onClick={openHint}>
            <InfoIcon className={b('hint')} />
          </div>
        </div>
        {value <= 31 && subscription !== 'memoya_cloud_1_year' && (
          <button
            className={b('bottom-text')}
            type="button"
            onClick={openSubscribePopup}
            disabled={
              !(ordersList && ordersList.length) ||
              (subscription === 'memoya_cloud_1_year' && value > 0)
            }
          >
            {value > 0 && value <= 30 && subscription === 'memoya_cloud_1_month'
              ? UPGRADE_NOW
              : RENEW_NOW}
          </button>
        )}
      </div>
    </div>
  );
}

CloudDayCounter.propTypes = propTypes;
CloudDayCounter.defaultProps = defaultProps;

const stateProps = (state) => ({
  value: cloudStatus(state),
  ordersList: ordersListSelect(state),
  subscription: subscriptionSelector(state),
});

export default connect(stateProps, null)(CloudDayCounter);
