const TITLE = 'Free photo scanning trial offer.';
const TEXT_ONE =
  "We'll send you our easy 3-way scan ship kit right to your door. Fill it with up to 25 loose photos (8x12 or smaller), 35mm slides or negatives & ship it back to us with the included shipping label.";
const TEXT_TWO =
  'We will digitize & enhance your photos to your personal ******* & provide a 30 day trial so you can view, download, organize & share your photos. You can also order photo restoration & retouching or create extraordinary photo art.';
const TEXT_THREE = 'Just pay $10 for a 3-way ship kit.';
const TEXT_BUTTON = 'Add to cart';

export { TITLE, TEXT_ONE, TEXT_TWO, TEXT_THREE, TEXT_BUTTON };
