import React, { Component } from 'react';
import { connect } from 'react-redux';
import bem from 'src/utils/bem';
import PropTypes from 'prop-types';
import ROUTES from 'src/constants/routes';
import dynamic from 'next/dynamic';
import { orderPriceSelect } from 'src/redux/prices/selectors';
import { getToken, userIdSelector } from 'src/redux/auth/selectors';
import { couponError, couponNameSelect } from 'src/redux/coupon/selectors';
import ordersTypeSelect from 'src/redux/orders/selectors';
import { addToCart } from 'src/redux/orders/actions';
import { checkCoupon } from 'src/redux/coupon/actions';
import styles from './index.module.scss';
import { TEXT_BUTTON, TEXT_ONE, TEXT_THREE, TEXT_TWO, TITLE } from './constants';

const FreePhotoScanContent = dynamic(() => import('src/components/widgets/FreePhotoScanContent'));

const b = bem('free-photo-scan', styles);

const defaultProps = {
  showPopup: () => {},
  addOrder: () => {},
  typeProduct: '',
  product: {},
  orders: {},
  transferTitle: '',
  selectPrice: {},
  error: '',
};

const propTypes = {
  showPopup: PropTypes.func,
  addOrder: PropTypes.func,
  product: PropTypes.shape({
    options: PropTypes.arrayOf(PropTypes.shape({})),
    price: PropTypes.number,
    reduce: PropTypes.func,
    displayName: PropTypes.string,
    id: PropTypes.number,
    name: PropTypes.string,
  }),
  orders: PropTypes.shape({
    photo_scan: PropTypes.arrayOf(PropTypes.shape({})),
  }),
  typeProduct: PropTypes.string,
  transferTitle: PropTypes.string,
  selectPrice: PropTypes.shape({}),
  error: PropTypes.string,
};

class FreePhotoScan extends Component {
  constructor(props) {
    super(props);
    this.veil = React.createRef();

    this.state = {
      isClick: false,
    };
  }

  componentDidUpdate(prevProps) {
    const { isClick } = this.state;
    const { error: oldError } = prevProps;
    const { error, orders } = this.props;

    if (isClick) {
      const { photo_scan: photoScanOrders } = orders;
      const isIncludeTrial = photoScanOrders.find((order) => order.isTrial);
      if (!isIncludeTrial) this.addProducts();
    }

    if (isClick && !oldError && error) {
      this.clearStatus();
    }
  }

  closeModalButton = () => {
    const { showPopup } = this.props;
    showPopup();
  };

  closeModal = (e) => {
    const { showPopup } = this.props;
    if (e.target === this.veil.current) {
      showPopup();
    }
  };

  closeModalButton = () => {
    const { showPopup } = this.props;
    showPopup();
  };

  generateOrder = (typeProduct) => {
    const { selectPrice } = this.props;
    return {
      quantity: 1,
      typeProduct,
      productId: selectPrice[typeProduct].id,
      price: selectPrice[typeProduct].price,
      isGift: false,
      id: 1,
      isTrial: true,
    };
  };

  addProducts = () => {
    const { addOrder, transferTitle, typeProduct, product } = this.props;
    const { options, displayName } = product;
    const values = options.reduce(
      (acc, item) => ({
        ...acc,
        [item.name]: { ...item },
      }),
      {},
    );
    const {
      enhance_my_transfer: enhanceValue,
      archival_dvd_set: archivalValue,
      cloud_download: cloudValue,
      *******_safe: *******SafeValue,
      personalize: personalizeValue,
    } = values;
    const order = {
      productId: product.id,
      quantity: 25,
      text: '',
      textSecond: '',
      title: transferTitle,
      productName: displayName,
      typeProduct,
      isGift: false,
      isTrial: true,
      minValue: 25,
      options: [
        {
          optionId: enhanceValue.id,
          quantity: 0,
          price: enhanceValue.price,
          displayName: enhanceValue.displayName,
          name: enhanceValue.name,
          hide: true,
        },
        {
          optionId: archivalValue.id,
          quantity: 0,
          price: archivalValue.price,
          displayName: archivalValue.displayName,
          name: archivalValue.name,
        },
        {
          optionId: cloudValue.id,
          quantity: true,
          price: cloudValue.price,
          displayName: cloudValue.displayName,
          name: cloudValue.name,
          isNotEdit: true,
        },
        {
          optionId: *******SafeValue.id,
          quantity: 0,
          price: *******SafeValue.price,
          displayName: *******SafeValue.displayName,
          name: *******SafeValue.name,
        },
        {
          optionId: personalizeValue.id,
          quantity: false,
          price: personalizeValue.price,
          displayName: personalizeValue.displayName,
          name: personalizeValue.name,
        },
      ],
    };
    addOrder(order);
    window.location.href = ROUTES.checkout;
    this.closeModalButton();
  };

  onClick = () => {
    this.setState({ isClick: true });
  };

  clearStatus = () => {
    this.setState({ isClick: false });
  };

  render() {
    return (
      <div className={b()} ref={this.veil} onClick={this.closeModal} role="button" tabIndex="0">
        <div className={b('wrapper')}>
          <FreePhotoScanContent
            title={TITLE}
            textOne={TEXT_ONE}
            textTwo={TEXT_TWO}
            textThree={TEXT_THREE}
            textButton={TEXT_BUTTON}
            onClick={this.onClick}
            closeModal={this.closeModalButton}
            withCloseButton
            b={b}
          />
        </div>
      </div>
    );
  }
}

const stateProps = (state) => ({
  selectPrice: orderPriceSelect(state),
  token: getToken(state),
  userId: userIdSelector(state),
  couponName: couponNameSelect(state),
  orders: ordersTypeSelect(state),
  error: couponError(state),
});

const actions = {
  addOrder: addToCart,
  checkingCoupon: checkCoupon,
};

FreePhotoScan.propTypes = propTypes;
FreePhotoScan.defaultProps = defaultProps;

export default connect(stateProps, actions)(FreePhotoScan);
