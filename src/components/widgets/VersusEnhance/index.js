import React from 'react';
import bem from 'src/utils/bem';
import PropTypes from 'prop-types';
import ROUTES from 'src/constants/routes';
import dynamic from 'next/dynamic';
import {
  DESCRIPTION,
  RESTORATION_BTN_TEXT,
  RESTORATION_DESCRIPTION,
  RESTORATION_IMAGES,
  RESTORATION_TITLE,
  RETOUCHING_BTN_TEXT,
  RETOUCHING_DESCRIPTION,
  RETOUCHING_IMAGES,
  RETOUCHING_TITLE,
  TITLE,
} from './constants';
import styles from './index.module.scss';

const PolaroidImageSliderWithCompare = dynamic(() =>
  import('src/components/widgets/PolaroidImageSliderWithCompare'),
);
const ColorButton = dynamic(() => import('src/components/buttons/ColorButton'));

const { photoRestoration, photoRetouching } = ROUTES;

const b = bem('versus-enhance', styles);

const defaultProps = {
  className: '',
  customClassBtn: '',
  restorationSliderList: RESTORATION_IMAGES,
  retouchingSliderList: RETOUCHING_IMAGES,
};

const propTypes = {
  className: PropTypes.string,
  customClassBtn: PropTypes.string,
  restorationSliderList: PropTypes.arrayOf(PropTypes.shape({})),
  retouchingSliderList: PropTypes.arrayOf(PropTypes.shape({})),
};

function VersusEnhance({
  className,
  customClassBtn,
  restorationSliderList,
  retouchingSliderList,
}) {
  return (
    <div className={b({ mix: className })}>
      <h2 className={b('title')}>{TITLE}</h2>
      <div className={b('description')}>{DESCRIPTION}</div>
      <div className={b('container')}>
        <div className={b('square')} />
        <div className={b('element')}>
          <div className={b('first-element-square')} />
          <div className={b('first-element-stripe')} />
          <PolaroidImageSliderWithCompare
            className={b('compare-element')}
            imageClass={b('compare-image')}
            slideList={restorationSliderList}
            lazyLoad
            autoplay
            autoplaySpeed={4000}
          />
          <div className={b('element-text-container')}>
            <div className={b('element-text')}>
              <div className={b('element-title')}>{RESTORATION_TITLE}</div>
              <div className={b('element-description')}>{RESTORATION_DESCRIPTION}</div>
            </div>
            <a className={b('element-button')} href={photoRestoration}>
              <ColorButton
                className={b({ mix: customClassBtn })}
                text={RESTORATION_BTN_TEXT}
              />
            </a>

          </div>
        </div>
        <div className={b('element')}>
          <div className={b('second-element-square')} />
          <div className={b('second-element-stripe')} />
          <PolaroidImageSliderWithCompare
            className={b('compare-element')}
            imageClass={b('compare-image')}
            slideList={retouchingSliderList}
            lazyLoad
            autoplay
            autoplaySpeed={4000}
          />
          <div className={b('element-text-container')}>
            <div className={b('element-text')}>
              <div className={b('element-title')}>{RETOUCHING_TITLE}</div>
              <div className={b('element-description')}>{RETOUCHING_DESCRIPTION}</div>
            </div>
            <a className={b('element-button')} href={photoRetouching}>
              <ColorButton
                className={b({ mix: customClassBtn })}
                text={RETOUCHING_BTN_TEXT}
              />
            </a>
          </div>
        </div>
      </div>
    </div>
  );
}


VersusEnhance.propTypes = propTypes;
VersusEnhance.defaultProps = defaultProps;

export default VersusEnhance;
