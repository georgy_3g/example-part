

import restorationImageOne from  'public/img/photo-retouching/COMPARE/DESKTOP/RESTORE/photo-restoration-before-after-example-1.1.jpg';
import compareRestorationImageOne from  'public/img/photo-retouching/COMPARE/DESKTOP/RESTORE/photo-restoration-before-after-example-1.2.jpg';
import restorationImageTwo from  'public/img/photo-retouching/COMPARE/DESKTOP/RESTORE/photo-restoration-before-after-example-2.1.jpg';
import compareRestorationImageTwo from  'public/img/photo-retouching/COMPARE/DESKTOP/RESTORE/photo-restoration-before-after-example-2.2.jpg';
import restorationImageThree from  'public/img/photo-retouching/COMPARE/DESKTOP/RESTORE/photo-restoration-before-after-example-3.1.jpg';
import compareRestorationImageThree from  'public/img/photo-retouching/COMPARE/DESKTOP/RESTORE/photo-restoration-before-after-example-3.2.jpg';
import restorationImageFour from  'public/img/photo-retouching/COMPARE/DESKTOP/RESTORE/photo-restoration-before-after-example-4.1.jpg';
import compareRestorationImageFour from  'public/img/photo-retouching/COMPARE/DESKTOP/RESTORE/photo-restoration-before-after-example-4.2.jpg';
import restorationImageFive from  'public/img/photo-retouching/COMPARE/DESKTOP/RESTORE/photo-restoration-before-after-example-5.1.jpg';
import compareRestorationImageFive from  'public/img/photo-retouching/COMPARE/DESKTOP/RESTORE/photo-restoration-before-after-example-5.2.jpg';

import restorationImageOneMobile from  'public/img/photo-retouching/COMPARE/MOBILE/RESTORE/photo-restoration-before-after-example-1.1.jpg';
import compareRestorationImageOneMobile from  'public/img/photo-retouching/COMPARE/MOBILE/RESTORE/photo-restoration-before-after-example-1.2.jpg';
import restorationImageTwoMobile from  'public/img/photo-retouching/COMPARE/MOBILE/RESTORE/photo-restoration-before-after-example-2.1.jpg';
import compareRestorationImageTwoMobile from  'public/img/photo-retouching/COMPARE/MOBILE/RESTORE/photo-restoration-before-after-example-2.2.jpg';
import restorationImageThreeMobile from  'public/img/photo-retouching/COMPARE/MOBILE/RESTORE/photo-restoration-before-after-example-3.1.jpg';
import compareRestorationImageThreeMobile from  'public/img/photo-retouching/COMPARE/MOBILE/RESTORE/photo-restoration-before-after-example-3.2.jpg';
import restorationImageFourMobile from  'public/img/photo-retouching/COMPARE/MOBILE/RESTORE/photo-restoration-before-after-example-4.1.jpg';
import compareRestorationImageFourMobile from  'public/img/photo-retouching/COMPARE/MOBILE/RESTORE/photo-restoration-before-after-example-4.2.jpg';
import restorationImageFiveMobile from  'public/img/photo-retouching/COMPARE/MOBILE/RESTORE/photo-restoration-before-after-example-5.1.jpg';
import compareRestorationImageFiveMobile from  'public/img/photo-retouching/COMPARE/MOBILE/RESTORE/photo-restoration-before-after-example-5.2.jpg';

import retouchingImageOne from  'public/img/photo-retouching/COMPARE/DESKTOP/RETOUCH/photo-retouching-compare-before-after-example-1.1.jpg';
import compareRetouchingImageOne from  'public/img/photo-retouching/COMPARE/DESKTOP/RETOUCH/photo-retouching-compare-before-after-example-1.2.jpg';
import retouchingImageTwo from  'public/img/photo-retouching/COMPARE/DESKTOP/RETOUCH/photo-retouching-compare-before-after-example-2.1.jpg';
import compareRetouchingImageTwo from  'public/img/photo-retouching/COMPARE/DESKTOP/RETOUCH/photo-retouching-compare-before-after-example-2.2.jpg';
import retouchingImageThree from  'public/img/photo-retouching/COMPARE/DESKTOP/RETOUCH/photo-retouching-compare-before-after-example-3.1.jpg';
import compareRetouchingImageThree from  'public/img/photo-retouching/COMPARE/DESKTOP/RETOUCH/photo-retouching-compare-before-after-example-3.2.jpg';
import retouchingImageFour from  'public/img/photo-retouching/COMPARE/DESKTOP/RETOUCH/photo-retouching-compare-before-after-example-4.1.jpg';
import compareRetouchingImageFour from  'public/img/photo-retouching/COMPARE/DESKTOP/RETOUCH/photo-retouching-compare-before-after-example-4.2.jpg';
import retouchingImageFive from  'public/img/photo-retouching/COMPARE/DESKTOP/RETOUCH/photo-retouching-compare-before-after-example-5.1.jpg';
import compareRetouchingImageFive from  'public/img/photo-retouching/COMPARE/DESKTOP/RETOUCH/photo-retouching-compare-before-after-example-5.2.jpg';

import retouchingImageOneMobile from  'public/img/photo-retouching/COMPARE/MOBILE/RETOUCH/photo-retouching-compare-before-after-example-1.1.jpg';
import compareRetouchingImageOneMobile from  'public/img/photo-retouching/COMPARE/MOBILE/RETOUCH/photo-retouching-compare-before-after-example-1.2.jpg';
import retouchingImageTwoMobile from  'public/img/photo-retouching/COMPARE/MOBILE/RETOUCH/photo-retouching-compare-before-after-example-2.1.jpg';
import compareRetouchingImageTwoMobile from  'public/img/photo-retouching/COMPARE/MOBILE/RETOUCH/photo-retouching-compare-before-after-example-2.2.jpg';
import retouchingImageThreeMobile from  'public/img/photo-retouching/COMPARE/MOBILE/RETOUCH/photo-retouching-compare-before-after-example-3.1.jpg';
import compareRetouchingImageThreeMobile from  'public/img/photo-retouching/COMPARE/MOBILE/RETOUCH/photo-retouching-compare-before-after-example-3.2.jpg';
import retouchingImageFourMobile from  'public/img/photo-retouching/COMPARE/MOBILE/RETOUCH/photo-retouching-compare-before-after-example-4.1.jpg';
import compareRetouchingImageFourMobile from  'public/img/photo-retouching/COMPARE/MOBILE/RETOUCH/photo-retouching-compare-before-after-example-4.2.jpg';
import retouchingImageFiveMobile from  'public/img/photo-retouching/COMPARE/MOBILE/RETOUCH/photo-retouching-compare-before-after-example-5.1.jpg';
import compareRetouchingImageFiveMobile from  'public/img/photo-retouching/COMPARE/MOBILE/RETOUCH/photo-retouching-compare-before-after-example-5.2.jpg';

const TITLE = 'Which should you choose? \nPhoto restoration or Photo retouching?';
const DESCRIPTION = '******* expert photoshop artists can retouch or restore any photo. Which professional photo editing\nservice is right for you? If your photo is a modern digital image captured from a phone or digital camera and you\nwant us to airbrush your photo, color correct and sharpen your photo or even change or remove the background\nfrom your image, then our photo retouching service is the right choice. If you have an old damaged vintage photo\nwith cracks, rips, tears and fading that needs to be repaired, then select our old photo restoration service.';
const RESTORATION_BTN_TEXT = 'Start restoration';
const RESTORATION_TITLE = 'Photo restoration';
const RESTORATION_DESCRIPTION = 'For photos that need repairs such as rips,\ntears, cracks, water damage, fading,\ndiscoloring or other age damage.';

const RETOUCHING_BTN_TEXT = 'Start retouching';
const RETOUCHING_TITLE = 'Photo retouching';
const RETOUCHING_DESCRIPTION = 'For portraits that need perfecting. Includes\nskin retouching, color correction and other\nimage enhancements to beautify your photo.';
const RESTORATION_IMAGES = [
  {
    id: 1,
    url: restorationImageOne,
    secondUrl: compareRestorationImageOne,
    alt: 'banner image',
  },
  {
    id: 2,
    url: restorationImageTwo,
    secondUrl: compareRestorationImageTwo,
    alt: 'banner image',
  },
  {
    id: 3,
    url: restorationImageThree,
    secondUrl: compareRestorationImageThree,
    alt: 'banner image',
  },
  {
    id: 4,
    url: restorationImageFour,
    secondUrl: compareRestorationImageFour,
    alt: 'banner image',
  },
  {
    id: 5,
    url: restorationImageFive,
    secondUrl: compareRestorationImageFive,
    alt: 'banner image',
  },
];

const RESTORATION_IMAGES_MOBILE = [
  {
    id: 1,
    url: restorationImageOneMobile,
    secondUrl: compareRestorationImageOneMobile,
    alt: 'banner image',
  },
  {
    id: 2,
    url: restorationImageTwoMobile,
    secondUrl: compareRestorationImageTwoMobile,
    alt: 'banner image',
  },
  {
    id: 3,
    url: restorationImageThreeMobile,
    secondUrl: compareRestorationImageThreeMobile,
    alt: 'banner image',
  },
  {
    id: 4,
    url: restorationImageFourMobile,
    secondUrl: compareRestorationImageFourMobile,
    alt: 'banner image',
  },
  {
    id: 5,
    url: restorationImageFiveMobile,
    secondUrl: compareRestorationImageFiveMobile,
    alt: 'banner image',
  },
];

const RETOUCHING_IMAGES = [
  {
    id: 1,
    url: retouchingImageOne,
    secondUrl: compareRetouchingImageOne,
    alt: 'banner image',
  },
  {
    id: 2,
    url: retouchingImageTwo,
    secondUrl: compareRetouchingImageTwo,
    alt: 'banner image',
  },
  {
    id: 3,
    url: retouchingImageThree,
    secondUrl: compareRetouchingImageThree,
    alt: 'banner image',
  },
  {
    id: 4,
    url: retouchingImageFour,
    secondUrl: compareRetouchingImageFour,
    alt: 'banner image',
  },
  {
    id: 5,
    url: retouchingImageFive,
    secondUrl: compareRetouchingImageFive,
    alt: 'banner image',
  },
];

const RETOUCHING_IMAGES_MOBILE = [
  {
    id: 1,
    url: retouchingImageOneMobile,
    secondUrl: compareRetouchingImageOneMobile,
    alt: 'banner image',
  },
  {
    id: 2,
    url: retouchingImageTwoMobile,
    secondUrl: compareRetouchingImageTwoMobile,
    alt: 'banner image',
  },
  {
    id: 3,
    url: retouchingImageThreeMobile,
    secondUrl: compareRetouchingImageThreeMobile,
    alt: 'banner image',
  },
  {
    id: 4,
    url: retouchingImageFourMobile,
    secondUrl: compareRetouchingImageFourMobile,
    alt: 'banner image',
  },
  {
    id: 5,
    url: retouchingImageFiveMobile,
    secondUrl: compareRetouchingImageFiveMobile,
    alt: 'banner image',
  },
];

export {
  TITLE,
  DESCRIPTION,
  RESTORATION_IMAGES,
  RETOUCHING_IMAGES,
  RESTORATION_BTN_TEXT,
  RETOUCHING_BTN_TEXT,
  RESTORATION_DESCRIPTION,
  RESTORATION_TITLE,
  RETOUCHING_DESCRIPTION,
  RETOUCHING_TITLE,
  RESTORATION_IMAGES_MOBILE,
  RETOUCHING_IMAGES_MOBILE,
};
