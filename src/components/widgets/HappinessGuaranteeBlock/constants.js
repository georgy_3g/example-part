const TITLE = '******* \nHappiness Guarantee.';
const TEXT =
  'If you are not satisfied after 3 photo revisions, please \nlet us know within 60 days and we will do everything \nwe can to make your order right.';

export { TITLE, TEXT };
