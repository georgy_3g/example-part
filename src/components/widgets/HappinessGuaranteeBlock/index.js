import React from 'react';
import bem from 'src/utils/bem';
import PropTypes from 'prop-types';
import image from 'public/img/how-it-works/heppiness-logo.jpg';
import bigLogo from 'public/img/how-it-works/*******-logo.png';
import { TEXT, TITLE } from './constants';
import styles from './index.module.scss';

const b = bem('happiness-guarantee-block', styles);
const defaultProps = {
  title: TITLE,
  text: TEXT,
};

const propTypes = {
  title: PropTypes.string,
  text: PropTypes.string,
};

const HappinessGuaranteeBlock = (props) => {
  const { title, text } = props;

  return (
    <section className={b()} id="happiness-guarantee-block">
      <div className={b('mobile-title-block')}>
        <h2 className={b('mobile-title')}>{title}</h2>
      </div>
      <div className={b('image-block')}>
        <div className={b('stripe-one')} />
        <div className={b('stripe-two')} />
        <div className={b('stripe-three')} />
        <img
          className={b('image')}
          src={image.src}
          alt="logo"
          loading="lazy"
        />
      </div>
      <div className={b('text-block')}>
        <div className={b('background-logo')}>
          <img src={bigLogo.src} alt="background logo" loading="lazy" />
        </div>
        <span className={b('title')}>{title}</span>
        <span className={b('text')}>{text}</span>
      </div>
      <div className={b('stripe-four')} />
      <div className={b('stripe-five')} />
    </section>
  );
}

HappinessGuaranteeBlock.propTypes = propTypes;
HappinessGuaranteeBlock.defaultProps = defaultProps;

export default HappinessGuaranteeBlock;
