const TERMS = {
  ENTER_PROMO: 'Enter promo code:',
  AT_CHECKOUT: 'at checkout',
  TRIAL_KIT_PROMO: 'TRIALKIT',
};

export default TERMS;
