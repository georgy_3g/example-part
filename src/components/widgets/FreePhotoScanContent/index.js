import React from 'react';
import PropTypes from 'prop-types';
import dynamic from "next/dynamic";
import TERMS from './constants';

const CloseIcon2 = dynamic(() => import('src/components/svg/CloseIcon2'));

const { TRIAL_KIT_PROMO, ENTER_PROMO, AT_CHECKOUT } = TERMS;

const defaultProps = {
  title: '',
  textOne: '',
  textTwo: '',
  textThree: '',
  textButton: '',
  withCloseButton: false,
  b: () => {},
  closeModal: () => {},
  onClick: () => {},
};

const propTypes = {
  title: PropTypes.string,
  textOne: PropTypes.string,
  textTwo: PropTypes.string,
  textThree: PropTypes.string,
  textButton: PropTypes.string,
  withCloseButton: PropTypes.bool,
  b: PropTypes.func,
  onClick: PropTypes.func,
  closeModal: PropTypes.func,
};

const FreePhotoScanContent = (props) => {
  const {
    title,
    textOne,
    textTwo,
    textThree,
    textButton,
    withCloseButton,
    b,
    onClick,
    closeModal,
  } = props;

  return (
    <div className={b('popup-wrapper')}>
      <div className={b('title')}>{title}</div>
      <div className={b('text-one')}>{textOne}</div>
      <div className={b('text-two')}>{textTwo}</div>
      <div className={b('text-three')}>{textThree}</div>
      <div className={b('block-button')}>
        <button className={b('button')} type="button" onClick={onClick}>
          {textButton}
        </button>
      </div>
      <div className={b('promo-code-text')}>
        {ENTER_PROMO}
        <div className={b('promo-code')}>{TRIAL_KIT_PROMO}</div>
        {AT_CHECKOUT}
      </div>
      {withCloseButton && (
        <button className={b('close-btn')} type="button" onClick={closeModal}>
          <CloseIcon2 />
        </button>
      )}
    </div>
  );
}

FreePhotoScanContent.defaultProps = defaultProps;
FreePhotoScanContent.propTypes = propTypes;

export default FreePhotoScanContent;
