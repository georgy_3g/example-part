import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Slider from 'react-slick';
import bem from 'src/utils/bem';
import debounce from 'lodash/debounce';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import Colors from 'src/styles/colors.json';
import dynamic from "next/dynamic";
import styles from './index.module.scss';

const ArrowInSquareIcon = dynamic(() => import('src/components/svg/ArrowInSquareIcon'));
const YouTubeVideo = dynamic(() => import('src/components/widgets/YouTubeVideo'));
const VimeoVideo = dynamic(() => import('src/components/widgets/VimeoVideo'));
const b = bem('polaroid-help-center-slider', styles);

const sharkGrayColor = Colors['shark-gray-color'];

const regexIsYouTube = /:\/\/www.youtube.com\//;

const propTypes = {
  slideList: PropTypes.arrayOf(PropTypes.shape()),
  dots: PropTypes.bool,
  arrows: PropTypes.bool,
  slidesToShow: PropTypes.number,
  slidesToScroll: PropTypes.number,
  smallShow: PropTypes.number,
  smallScroll: PropTypes.number,
  className: PropTypes.string,
  imageClass: PropTypes.string,
  beforeChange: PropTypes.func,
  lazyLoad: PropTypes.bool,
  afterChange: PropTypes.func,
  autoplay: PropTypes.bool,
  pauseOnHover: PropTypes.bool,
  autoplaySpeed: PropTypes.number,
  withOutTitle: PropTypes.bool,
};
const defaultProps = {
  slideList: [],
  dots: false,
  arrows: false,
  slidesToShow: 1,
  slidesToScroll: 1,
  smallShow: 1,
  smallScroll: 1,
  className: '',
  imageClass: '',
  beforeChange: () => {},
  afterChange: () => {},
  lazyLoad: true,
  autoplay: false,
  pauseOnHover: false,
  autoplaySpeed: 5000,
  withOutTitle: false,
};

class PolaroidHelpCenterSlider extends Component {
  constructor(props) {
    super(props);
    this.isOnclick = false;
    this.slider = null;
    this.state = {
      index: 0,
    };
  }

  componentWillUnmount() {
    this.slider = null;
    this.isOnclick = null;
  }

  updateCount = (valueOne, valueTwo) => {
    const { beforeChange } = this.props;
    beforeChange(valueTwo);
    this.setState({ index: valueTwo });
  };

  generateDots = () => {
    const { slideList } = this.props;
    const { index } = this.state;
    return slideList.map((item, i) => {
      const { url } = item;
      return {
        isSelect: index === i,
        key: `${i}-${url}`,
        i,
      };
    });
  };

  clickToDot = (value) => {
    this.isOnclick = false;
    this.slider.slickGoTo(value);
    this.waitPlay();
  };

  clickNext = () => {
    this.isOnclick = false;
    this.slider.slickNext();
    this.waitPlay();
  };

  clickPrev = () => {
    this.isOnclick = false;
    this.slider.slickPrev();
    this.waitPlay();
  };

  clickPause = () => {
    this.slider.slickPause();
  };

  clickYouTube = () => {
    this.slider.slickPause();
    this.isOnclick = true;
  };

  clickPlay = () => {
    if (this.slider !== null) {
      this.slider.slickPlay();
    }
  };

  waitPlay = () => {
    this.clickPause();
    const debounceFunc = debounce(() => {
      if (!this.isOnclick) {
        this.clickPlay();
      }
    }, 4000);
    debounceFunc();
  };

  render() {
    const {
      slideList,
      arrows,
      slidesToShow,
      slidesToScroll,
      className,
      imageClass,
      smallShow,
      smallScroll,
      afterChange,
      lazyLoad,
      autoplay: autoplayProps,
      pauseOnHover,
      autoplaySpeed,
    } = this.props;

    const autoplay = slideList.length > 1 ? autoplayProps : false;

    const settings = {
      arrows,
      infinite: true,
      focusOnSelect: false,
      draggable: false,
      lazyLoad,
      speed: 500,
      slidesToShow,
      slidesToScroll,
      beforeChange: this.updateCount,
      afterChange,
      responsive: [
        {
          breakpoint: 900,
          settings: {
            slidesToShow: smallShow,
            slidesToScroll: smallScroll,
          },
        },
      ],
      dotsClass: b('dots'),
      autoplay,
      pauseOnHover,
      autoplaySpeed,
      className,
    };

    return (
      <div className={b({ mix: className })}>
        {slideList.length > 1 && (
          <button className={b('arrow-btn', { prev: true })} type="button" onClick={this.clickPrev}>
            <ArrowInSquareIcon
              className={b('arrow-icon', { prev: true })}
              stroke={sharkGrayColor}
              strokeWidth={3}
            />
          </button>
        )}
        <Slider
          ref={(e) => {
            this.slider = e;
          }}
          {...settings}
          className={b('slick-slider-width')}
        >
          {slideList.map(({ url, id, alt }) => {
            const isYouTube = url.match(regexIsYouTube);
            return (
              <div className={b('block-content')}>
                {url[0] === '<' ? (
                  <div className={b('block-iframe')}>
                    {isYouTube ? (
                      <YouTubeVideo
                        className={b('video')}
                        contentIframe={url}
                        clickPause={this.clickYouTube}
                      />
                    ) : (
                      <VimeoVideo contentIframe={url} clickPause={this.clickYouTube} />
                    )}
                  </div>
                ) : (
                  <img
                    className={b('inner-slide', { mix: imageClass })}
                    src={url}
                    alt={alt || url}
                    key={id}
                    layout="fill"
                  />
                )}
              </div>
            );
          })}
        </Slider>
        {slideList.length > 1 && (
          <button className={b('arrow-btn', { next: true })} type="button" onClick={this.clickNext}>
            <ArrowInSquareIcon
              className={b('arrow-icon', { next: true })}
              stroke={sharkGrayColor}
              strokeWidth={3}
            />
          </button>
        )}
      </div>
    );
  }
}

PolaroidHelpCenterSlider.propTypes = propTypes;
PolaroidHelpCenterSlider.defaultProps = defaultProps;

export default PolaroidHelpCenterSlider;
