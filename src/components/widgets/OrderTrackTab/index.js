import React, { Component } from 'react';
import format from 'date-fns/format';
import { connect } from 'react-redux';
import ROUTES from 'src/constants/routes';
import PropTypes from 'prop-types';
import bem from 'src/utils/bem';
import Colors from 'src/styles/colors.json';
import dynamic from 'next/dynamic';
import { projectListSelector } from 'src/redux/projects/selectors';
import { orderTrackSelect } from 'src/redux/ordersList/selectors';
import { getToken } from 'src/redux/auth/selectors';
import ordersTypeSelect from 'src/redux/orders/selectors';
import styles from './index.module.scss';
import TITLE from './constants';

const ListIcon = dynamic(() => import('src/components/svg/ListIcon'));
const ArrowInCircle = dynamic(() => import('src/components/svg/ArrowInCircle'));
const DiamondIcon = dynamic(() => import('src/components/svg/DiamondIcon'));
const ProcessingIcon = dynamic(() => import('src/components/svg/ProcessingIcon'));
const ShippingIcon = dynamic(() => import('src/components/svg/ShippingIcon'));
const ApprovingIcon = dynamic(() => import('src/components/svg/ApprovingIcon'));
const FunnySmileIcon = dynamic(() => import('src/components/svg/FunnySmileIcon'));
const CompleteIcon = dynamic(() => import('src/components/svg/ComleteIcon'));
const OrderDollarIcon = dynamic(() => import('src/components/svg/OrderDollarIcon'));
const DownloadDownArrow = dynamic(() => import('src/components/svg/DownloadDownArrow'));
const TrackIcon = dynamic(() => import('src/components/svg/TrackIcon'));
const ImageNoteIcon = dynamic(() => import('src/components/svg/ImageNoteIcon'));
const DocumentIcon = dynamic(() => import('src/components/svg/DocumentIcon'));

const {
  DATE,
  STATUS,
  TRACK,
  VIEW_ORDER,
  AWAITING_APPROVAL,
  DOWNLOAD,
  VIEW_MESSAGE,
  VIEW_LOCATION,
  VIEW_PHOTOS,
  MAKE_PAYMENT,
} = TITLE;

const { contact } = ROUTES;

const b = bem('order-track-tab', styles);
const red = Colors['$sea-pink-red-color'];
const green = Colors['$summer-green-color'];
const blue = Colors['$pickled-bluewood-color'];
const siennaColor = Colors['$burnt-sienna-color'];

const defaultProps = {
  projectList: [],
  redirect: () => {},
  orderTrack: [],
  className: '',
};

const propTypes = {
  projectList: PropTypes.arrayOf(PropTypes.shape({})),
  redirect: PropTypes.func,
  orderTrack: PropTypes.arrayOf(PropTypes.shape({})),
  className: PropTypes.string,
};

class OrderTrackTab extends Component {
  getVersionImage = (extra, order) => {
    const { message } = extra || {};
    const { projectList = [] } = this.props;
    const { imagesOrig = [], imagesVersion = [] } =
      projectList.find(({ orderId }) => orderId === order) || {};
    const searchOriginal = imagesOrig.findIndex(({ id }) => id === message.orderImageId);
    const filterVersion = imagesVersion.filter(
      ({ parentOrderImageId }) => parentOrderImageId === message.orderImageId,
    );
    const searchVersion = filterVersion.findIndex(
      ({ versionId }) => versionId === message.versionId,
    );
    return `We received your revision request for photo #${
      searchOriginal + 1
    } version ${searchVersion}.`;
  };

  openTracking = (url, carrier) => {
    const link = document.createElement('a');
    const carrierUrl = {
      usps: `https://tools.usps.com/go/TrackConfirmAction?tRef=fullpage&tLc=2&text28777=&tLabels=${url}`,
      ups: `https://www.ups.com/track?loc=en_RU&tracknum=${url}&requester=WT/trackdetails`,
      fedex: `https://www.fedex.com/fedextrack/?trknbr=${url}`,
    };
    link.href = carrierUrl[carrier];
    link.rel = 'noopener noreferrer';
    link.setAttribute('target', '_blank');
    document.body.appendChild(link);
    link.click();
    link.remove();
  };

  redirectUrl = (type, order) => {
    const { redirect } = this.props;
    redirect(type, { order });
  };

  changeTab = (open) => {
    const { redirect } = this.props;
    redirect('orderDetails', open);
  };

  enhanceButton = (type, order) => {
    const { redirect } = this.props;
    redirect(type, order);
  };

  switchIcon = (type) => {
    switch (type) {
      case 'photo_approval':
        return <ApprovingIcon className={b('icon')} approved />;
      case 'awaiting_approval':
        return <ApprovingIcon className={b('icon')} />;
      case 'ship_back':
        return <ShippingIcon type="back" className={b('icon')} />;
      case 'ship_success':
        return <ShippingIcon type="success" className={b('icon')} />;
      case 'ship_out':
        return <ShippingIcon type="sent" className={b('icon')} />;
      case 'process':
        return <ProcessingIcon className={b('icon')} />;
      case 'execute':
        return <FunnySmileIcon className={b('icon')} />;
      case 'download':
        return <DownloadDownArrow className={b('icon')} />;
      case 'quality':
        return <DiamondIcon className={b('icon')} />;
      case 'complete':
        return <CompleteIcon className={b('icon')} />;
      case 'payment':
        return <OrderDollarIcon className={b('icon')} />;
      case 'place':
        return <DocumentIcon className={b('icon')} />;
      case 'enhance_request':
        return <ImageNoteIcon fill={blue} className={b('icon')} />;
      default:
        return <FunnySmileIcon className={b('icon')} />;
    }
  };

  switchButton = (icon, extra, orderId) => {
    const { all = '', isApprove = '', message } = extra || {};
    const { roomId } = message || {};
    if (all && isApprove) {
      return (
        <>
          <div className={b('item-description')}>
            {`You have successfully approved ${isApprove} out of ${all} photos!`}
          </div>
          <button
            className={b('btn', { place: true })}
            type="button"
            onClick={() => this.redirectUrl('proofs', orderId)}
          >
            <ArrowInCircle stroke={siennaColor} className={b('icon', { proofs: true })} />
            {VIEW_PHOTOS}
          </button>
        </>
      );
    }
    if (icon === 'enhance_request') {
      return (
        <>
          <div className={b('item-description')}>{this.getVersionImage(extra, orderId)}</div>
          <button
            className={b('btn', { download: true })}
            type="button"
            onClick={() => {
              this.enhanceButton('my-message', { order: orderId, version: roomId });
            }}
          >
            <ArrowInCircle stroke={green} className={b('icon', { proofs: true })} />
            {VIEW_MESSAGE}
          </button>
        </>
      );
    }
    switch (icon) {
      case 'payment':
        return (
          <button
            className={b('btn', { payment: true })}
            type="button"
            onClick={() => this.changeTab({})}
          >
            <OrderDollarIcon stroke={red} className={b('icon', { dollar: true })} />
            {VIEW_ORDER}
          </button>
        );
      case 'order_update_and_balance_due':
      case 'refund_success':
      case 'place':
        return (
          <button
            className={b('btn', { place: true })}
            type="button"
            onClick={() => this.changeTab({})}
          >
            <ListIcon className={b('icon', { place: true })} />
            {VIEW_ORDER}
          </button>
        );
      case 'fail_pay':
      case 'order_update_and_balance_due_more':
        return (
          <button
            className={b('btn', { place: true })}
            type="button"
            onClick={() => this.changeTab({ open: true })}
          >
            <ListIcon className={b('icon', { place: true })} />
            {icon === 'order_update_and_balance_due_more' ? MAKE_PAYMENT : VIEW_ORDER}
          </button>
        );
      case 'awaiting_approval':
        return (
          <button
            className={b('btn', { place: true })}
            type="button"
            onClick={() => this.redirectUrl('proofs', orderId)}
          >
            <ArrowInCircle stroke={siennaColor} className={b('icon', { proofs: true })} />
            {AWAITING_APPROVAL}
          </button>
        );
      case 'download':
        return (
          <button
            className={b('btn', { download: true })}
            type="button"
            onClick={() => this.redirectUrl('myFiles', orderId)}
          >
            {DOWNLOAD}
          </button>
        );
      case 'send_ship_kit':
      case 'ship_out': {
        const { trackingNumber = '', carrier = '' } = extra || {};
        if (trackingNumber) {
          return (
            <button
              className={b('btn', { download: true })}
              type="button"
              onClick={() => this.openTracking(trackingNumber, carrier)}
            >
              <TrackIcon className={b('icon', { track: true })} />
              {TRACK}
            </button>
          );
        }
        return null;
      }
      case 'awaiting_store_drop_off':
      case 'ready_for_pick_up':
        return (
          <button
            className={b('btn', { download: true })}
            type="button"
            onClick={() => {
              window.location.href = `${contact}?map`;
            }}
          >
            {VIEW_LOCATION}
          </button>
        );
      default:
        return null;
    }
  };

  render() {
    const { orderTrack, className } = this.props;

    if (!orderTrack.length) {
      return null;
    }

    return (
      <main className={b({ mix: className })}>
        <div className={b('title')}>
          <div className={b('title-date')}>{DATE}</div>
          <div className={b('title-status')}>{STATUS}</div>
        </div>
        {orderTrack.map(({ orderId, date, status, extra, description, icon }, index) => (
          <div key={status} className={b('item')}>
            <div className={b('item-date-block', { last: index === orderTrack.length - 1 })}>
              <div className={b('item-date')}>{format(new Date(date), 'MM.dd.yy')}</div>
              <div className={b('item-time')}>{format(new Date(date),'hh:mm').toLowerCase()}</div>
            </div>
            <div className={b('status-icon')}>{this.switchIcon(icon)}</div>
            <div className={b('item-container')}>
              <div className={b('item-status')}>{status}</div>
              <div className={b('item-description')}>{description}</div>
              {this.switchButton(icon, extra, orderId)}
            </div>
          </div>
        ))}
      </main>
    );
  }
}

OrderTrackTab.propTypes = propTypes;
OrderTrackTab.defaultProps = defaultProps;

const stateProps = (state) => ({
  projectList: projectListSelector(state),
  orderTrack: orderTrackSelect(state),
  token: getToken(state),
  typeOrders: ordersTypeSelect(state),
});

export default connect(stateProps, null)(OrderTrackTab);
