const TITLE = {
  DATE: 'Date',
  STATUS: 'Status',
  VIEW_ALL: 'View all tracking path',
  TRACK: 'Track kit',
  GO_PAYMENT: 'Go payment',
  PlACE: 'place',
  AWAITING_APPROVAL: 'Approve photo',
  DOWNLOAD: 'View files',
  VIEW_ORDER: 'View order',
  VIEW_MESSAGE: 'View message',
  VIEW_LOCATION: 'View location',
  VIEW_PHOTOS: 'View photos',
  MAKE_PAYMENT: 'Make payment',
};

export default TITLE;
