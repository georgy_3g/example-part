const TITLE = 'Imagine how the world’s best\n artists can repair your old photo.';
const LIST = ['Rips', 'Fading', 'Water damage', 'Scratches', 'Lorem ipsum'];

const BOTTOM_TEXT = {
  orderOnline: 'Just place your order online and select "',
  homeKit: 'Home Ship Kit',
  duringCheckout: '" during checkout.',
};

export { TITLE, LIST, BOTTOM_TEXT };
