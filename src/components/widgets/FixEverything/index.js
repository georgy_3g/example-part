import React from 'react';
import bem from 'src/utils/bem';
import PropTypes from 'prop-types';
import LazyLoad from 'react-lazyload';
import dynamic from 'next/dynamic';
import { TITLE } from './constants';
import styles from './index.module.scss';

const ImageSliderWithCompare = dynamic(() =>
  import('src/components/widgets/ImageSliderWithCompare'),
);

const b = bem('fix-everything', styles);

const defaultProps = {
  className: '',
  description: '',
  title: TITLE,
  withScrollButton: false,
  scroll: () => {},
  scrollButtonText: '',
  customClassBtn: '',
};

const propTypes = {
  className: PropTypes.string,
  sliderList: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  title: PropTypes.string,
  description: PropTypes.string,
  withScrollButton: PropTypes.bool,
  scroll: PropTypes.func,
  scrollButtonText: PropTypes.string,
  customClassBtn: PropTypes.string,
};

function FixEverything(props) {
  const {
    className,
    sliderList,
    title,
    description,
    withScrollButton,
    scrollButtonText,
    customClassBtn,
    scroll,
  } = props;
  return (
    <div className={b({ mix: className })}>
      <div className={b('text-wrapper')}>
        <h2 className={b('title')}>{title}</h2>
        {description && <div className={b('description')}>{description}</div>}
      </div>
      <div className={b('slider-wrapper')}>
        <LazyLoad offset={100}>
          <ImageSliderWithCompare
            slideList={sliderList}
            slidesToShow={3}
            autoplay={false}
            infinite={false}
            lazyLoad
            withScrollButton={withScrollButton}
            scrollButtonText={scrollButtonText}
            customClassBtn={customClassBtn}
            scroll={scroll}
          />
        </LazyLoad>
        <div className={b('left-square')} />
        <div className={b('right-stripe')} />
      </div>
    </div>
  );
}

FixEverything.propTypes = propTypes;
FixEverything.defaultProps = defaultProps;

export default FixEverything;
