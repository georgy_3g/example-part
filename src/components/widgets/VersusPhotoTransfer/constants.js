import restorationImageOne from  'public/img/photo-retouching/COMPARE/DESKTOP/RESTORE/photo-restoration-before-after-example-1.1.jpg';
import compareRestorationImageOne from  'public/img/photo-retouching/COMPARE/DESKTOP/RESTORE/photo-restoration-before-after-example-1.2.jpg';
import restorationImageTwo from  'public/img/photo-retouching/COMPARE/DESKTOP/RESTORE/photo-restoration-before-after-example-2.1.jpg';
import compareRestorationImageTwo from  'public/img/photo-retouching/COMPARE/DESKTOP/RESTORE/photo-restoration-before-after-example-2.2.jpg';
import restorationImageThree from  'public/img/photo-retouching/COMPARE/DESKTOP/RESTORE/photo-restoration-before-after-example-3.1.jpg';
import compareRestorationImageThree from  'public/img/photo-retouching/COMPARE/DESKTOP/RESTORE/photo-restoration-before-after-example-3.2.jpg';
import restorationImageFour from  'public/img/photo-retouching/COMPARE/DESKTOP/RESTORE/photo-restoration-before-after-example-4.1.jpg';
import compareRestorationImageFour from  'public/img/photo-retouching/COMPARE/DESKTOP/RESTORE/photo-restoration-before-after-example-4.2.jpg';
import restorationImageFive from  'public/img/photo-retouching/COMPARE/DESKTOP/RESTORE/photo-restoration-before-after-example-5.1.jpg';
import compareRestorationImageFive from  'public/img/photo-retouching/COMPARE/DESKTOP/RESTORE/photo-restoration-before-after-example-5.2.jpg';

const TITLE = 'Turn your digitized photos into\nsomething truly extraordinary.';

const RESTORATION_BTN_TEXT = 'Start enhancing';
const RESTORATION_TITLE = 'Photo enhancement';
const RESTORATION_DESCRIPTION = 'Does your digitized photo need a little love?\n******* is the #1 photo restoration service\nin the USA. We’ll make your photo look perfect.';

const CREATE_BTN_TEXT = 'Start creating';
const CREATE_TITLE = 'Create photo art';
const CREATE_DESCRIPTION ='Design wall art from your favorite scanned photo.\nOur photo art collections are handcrafted with\npremium materials, right here in the USA.';

const UNIQUE_BTN_TEXT = 'Start gifting';
const UNIQUE_TITLE = 'Unique gifts';
const UNIQUE_DESCRIPTION ='Create unique photo art or photo gifts after\nscanning your photos to digital for yourself or\nthat special someone in your life.';
const RESTORATION_IMAGES = [
  {
    id: 1,
    url: restorationImageOne,
    secondUrl: compareRestorationImageOne,
    alt: 'banner image',
  },
  {
    id: 2,
    url: restorationImageTwo,
    secondUrl: compareRestorationImageTwo,
    alt: 'banner image',
  },
  {
    id: 3,
    url: restorationImageThree,
    secondUrl: compareRestorationImageThree,
    alt: 'banner image',
  },
  {
    id: 4,
    url: restorationImageFour,
    secondUrl: compareRestorationImageFour,
    alt: 'banner image',
  },
  {
    id: 5,
    url: restorationImageFive,
    secondUrl: compareRestorationImageFive,
    alt: 'banner image',
  },
];

export {
  TITLE,
  RESTORATION_IMAGES,
  RESTORATION_BTN_TEXT,
  RESTORATION_DESCRIPTION,
  RESTORATION_TITLE,
  CREATE_BTN_TEXT,
  CREATE_TITLE,
  CREATE_DESCRIPTION,
  UNIQUE_BTN_TEXT,
  UNIQUE_TITLE,
  UNIQUE_DESCRIPTION,
};
