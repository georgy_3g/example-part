import React, { useEffect, useState } from 'react';
import bem from 'src/utils/bem';
import PropTypes from 'prop-types';
import ROUTES from 'src/constants/routes';
import { connect } from 'react-redux';
import dynamic from 'next/dynamic';
import { photoArtProductsSelector } from 'src/redux/photoArt/selectors';
import {
  CREATE_BTN_TEXT,
  CREATE_DESCRIPTION,
  CREATE_TITLE,
  RESTORATION_BTN_TEXT,
  RESTORATION_DESCRIPTION,
  RESTORATION_IMAGES,
  RESTORATION_TITLE,
  TITLE,
  UNIQUE_BTN_TEXT,
  UNIQUE_DESCRIPTION,
  UNIQUE_TITLE,
} from './constants';
import styles from './index.module.scss';

const PolaroidImageSliderWithCompare = dynamic(() =>
  import('src/components/widgets/PolaroidImageSliderWithCompare'),
);
const ColorButton = dynamic(() => import('src/components/buttons/ColorButton'));
const FrameSlider = dynamic(() => import('src/components/widgets/FrameSlider'));

const { photoRestoration, photoArt, giftGuide } = ROUTES;

const b = bem('versus-photo-transfer', styles);

const defaultProps = {
  className: '',
  title: TITLE,
  description: '',
  restorationSliderList: RESTORATION_IMAGES,
  products: [],
};

const propTypes = {
  className: PropTypes.string,
  restorationSliderList: PropTypes.arrayOf(PropTypes.shape({})),
  products: PropTypes.arrayOf(PropTypes.shape({})),
  title: PropTypes.string,
  description: PropTypes.string,
};

const VersusPhotoTransfer = ({
  className,
  restorationSliderList,
  products,
  description,
  title,
}) => {
  const [randomProduct, setRandomProducts] = useState({
    firstArray: [],
    lastArray: [],
  });

  const getArrayOfProducts = () =>
    Object.values(products).reduce((acc, value) => {
      const { frames } = value;
      return [...acc, frames];
    }, []);

  const getRandom = (array) =>
    array.reduce((acc, item = []) => {
      const randomFrameIndex = Math.floor(Math.random() * (item.length - 1));
      return [...acc, item[randomFrameIndex]];
    }, []);

  useEffect(() => {
    const framesArray = getArrayOfProducts();
    const [firstFrames, secondFrames, thirdFrames, fourthFrame, fiveFrames, sixFrames] =
      framesArray;
    const firstArray = framesArray.length ? [firstFrames, secondFrames, thirdFrames, fourthFrame, sixFrames] : [];
    const secondArray =framesArray.length ? [fourthFrame, fiveFrames, sixFrames, firstFrames, secondFrames] : [];
    setRandomProducts({
      firstArray: getRandom(firstArray),
      secondArray: getRandom(secondArray),
    });
  }, [products]);

  const { firstArray, secondArray } = randomProduct;
  return (
    <div className={b({ mix: className })}>
      <h2 className={b('title')}>{title}</h2>
      {description && <div className={b('description')}>{description}</div>}
      <div className={b('container')}>
        <div className={b('square')} />
        <div className={b('element')}>
          <div className={b('first-element-square')} />
          <div className={b('first-element-stripe')} />
          <FrameSlider
            className={b('compare-element')}
            imageClass={b('compare-image')}
            slideList={firstArray}
            lazyLoad
            autoplay
            autoplaySpeed={4000}
          />
          <h3 className={b('element-title')}>{CREATE_TITLE}</h3>
          <div className={b('element-description')}>{CREATE_DESCRIPTION}</div>
          <a className={b('element-button')} href={photoArt}>
            <ColorButton text={CREATE_BTN_TEXT} />
          </a>
        </div>
        <div className={b('element')}>
          <div className={b('second-element-square')} />
          <div className={b('second-element-stripe')} />
          <PolaroidImageSliderWithCompare
            className={b('compare-element')}
            imageClass={b('compare-image')}
            slideList={restorationSliderList}
            lazyLoad
            autoplay
            autoplaySpeed={4000}
          />
          <h3 className={b('element-title')}>{RESTORATION_TITLE}</h3>
          <div className={b('element-description')}>{RESTORATION_DESCRIPTION}</div>
          <a className={b('element-button')} href={photoRestoration}>
            <ColorButton text={RESTORATION_BTN_TEXT} />
          </a>
        </div>
        <div className={b('element')}>
          <div className={b('second-element-square')} />
          <div className={b('second-element-stripe')} />
          <FrameSlider
            className={b('compare-element')}
            imageClass={b('compare-image')}
            slideList={secondArray}
            lazyLoad
            autoplay
            autoplaySpeed={4000}
          />
          <h3 className={b('element-title')}>{UNIQUE_TITLE}</h3>
          <div className={b('element-description')}>{UNIQUE_DESCRIPTION}</div>
          <a className={b('element-button')} href={giftGuide}>
            <ColorButton text={UNIQUE_BTN_TEXT} />
          </a>
        </div>
      </div>
    </div>
  );
}

const stateProps = (state) => ({
  products: photoArtProductsSelector(state),
});
VersusPhotoTransfer.propTypes = propTypes;
VersusPhotoTransfer.defaultProps = defaultProps;
export default connect(stateProps)(VersusPhotoTransfer);
