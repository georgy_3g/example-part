import React from 'react';
import bem from 'src/utils/bem';
import PropTypes from 'prop-types';
import LazyLoad from 'react-lazyload';
import dynamic from 'next/dynamic';
import BlueButton from 'src/components/buttons/BlueButton';
import { BUTTON_TEXT, TEXT, TITLE, WATCH_PHOTO } from './constants';
import styles from './index.module.scss';

const ColorButton = dynamic(() => import('src/components/buttons/ColorButton'));

const b = bem('love-guarantee', styles);

const defaultProps = {
  className: '',
  withScrollButton: false,
  scroll: () => {},
  image: {},
  buttonText: BUTTON_TEXT,
  title: TITLE,
  description: TEXT,
  withoutRightBlocks: false,
  isSecond: false,
  isRedirect: false,
  redirectLink: '',
  customClassBtn: '',
  isRetouching: false,
};

const propTypes = {
  className: PropTypes.string,
  withScrollButton: PropTypes.bool,
  scroll: PropTypes.func,
  image: PropTypes.shape({
    src: PropTypes.string,
  }),
  buttonText: PropTypes.string,
  title: PropTypes.string,
  description: PropTypes.string,
  withoutRightBlocks: PropTypes.bool,
  isSecond: PropTypes.bool,
  isRedirect: PropTypes.bool,
  redirectLink: PropTypes.string,
  customClassBtn: PropTypes.string,
  isRetouching: PropTypes.bool,
};

const LoveGuarantee = ({
  className,
  withScrollButton,
  scroll,
  image,
  buttonText,
  title,
  description,
  withoutRightBlocks,
  isSecond,
  isRedirect,
  redirectLink,
  customClassBtn,
  isRetouching,
}) => {
  return (
    <article id="live-guarantee" className={b({ mix: className })}>
      <div className={b('container')}>
        <h2 className={b('title-two', { 'large-margin': isSecond })}>{title}</h2>
        {withoutRightBlocks ? null : (
          <>
            <div className={b('stripe-three')} />
            <div className={b('stripe-four')} />
          </>
        )}
        <div className={b('image-wrapper', { second: isSecond })}>
          <div className={b('image', { second: isSecond })}>
            <LazyLoad offset={100}>
              <img
                className={b('image-logo', { second: isSecond })}
                src={image.src || WATCH_PHOTO.src}
                alt="logo"
              />
            </LazyLoad>
          </div>
          <div className={b('stripe-one', { second: isSecond })} />
          <div className={b('stripe-two', { second: isSecond })} />
        </div>
        <div className={b('text-block')}>
          <h2 className={b('title')}>{title}</h2>
          <p className={b('text', { wrap: isSecond })}>{description}</p>
          {withScrollButton && (
            <ColorButton
              className={b('button', { mix: customClassBtn })}
              onClick={scroll}
              text={buttonText}
            />
          )}
          {isRedirect && (
            <a className={b('link')} href={redirectLink}>
              <BlueButton
                className={b('button', { mix: customClassBtn, retouching: isRetouching } )}
              >
                {buttonText}
              </BlueButton>
            </a>
          )}
        </div>
      </div>
    </article>
  );
}

LoveGuarantee.propTypes = propTypes;
LoveGuarantee.defaultProps = defaultProps;

export default LoveGuarantee;
