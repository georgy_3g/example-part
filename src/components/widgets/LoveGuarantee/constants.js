import MAIN_PHOTO from  'public/img/photo-restoration/LOVE GUARANTEE/main.png';
import WATCH_PHOTO from  'public/img/photo-restoration/LOVE GUARANTEE/watch.png';

const BUTTON_TEXT = 'Restore my photo';
const TITLE = 'Your happiness\nis guaranteed.';
const TEXT = 'We know you are going to love your digitally restored photo.\nWork directly with our certified photo editors on unlimited\nfree revisions to ensure your photo restoration is perfect. If\nyou are not 100% satisfied with your restoration, we will\ngive you a free photo restoration on a different image or a\nstore credit for the full amount of the photo restoration.';

export {
  TITLE,
  TEXT,
  MAIN_PHOTO,
  WATCH_PHOTO,
  BUTTON_TEXT,
};
