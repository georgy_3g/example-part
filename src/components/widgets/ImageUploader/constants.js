const TITLE = 'Upload your photos to restore';
const TEXT = 'Select photos from our computer, tablet, or phone.';
const SCANNING_HELP = 'Scanning help';

export { TITLE, TEXT, SCANNING_HELP };
