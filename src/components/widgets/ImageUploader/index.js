import React from 'react';
import PropTypes from 'prop-types';
import bem from 'src/utils/bem';
import { connect } from 'react-redux';
import DragAndDropForm from 'src/components/forms/DragAndDropForm';
import spinner from 'public/img/shared/spinner*******.gif';
import errorIcon from 'public/img/shared/error.png';
import { isImageUploadSelector } from 'src/redux/shared/selectors';
import { enhanceErrorsSelector } from 'src/redux/orderImages/selectors';
import { TEXT, TITLE } from './constants';
import styles from './index.module.scss';

const b = bem('image-uploader', styles);
const defaultProps = {
  className: '',
  titleClass: '',
  isImageUpload: false,
  isOrderEnhance: false,
  enhanceErrors: [],
  withoutText: false,
  uploadTitle: TITLE,
  uploadText: TEXT,
  onlyTitle: false,
  buttonBackgroundColor: '',
  withoutBottomText: false,
  withoutError: false,
  buttonClass: '',
  buttonRefFunc: () => {},
  forPhotoArtConstructor: false,
  buttonText: '',
  isContactImages: false,
};

const propTypes = {
  className: PropTypes.string,
  titleClass: PropTypes.string,
  isImageUpload: PropTypes.bool,
  isOrderEnhance: PropTypes.bool,
  enhanceErrors: PropTypes.arrayOf(PropTypes.string),
  withoutText: PropTypes.bool,
  uploadTitle: PropTypes.string,
  uploadText: PropTypes.string,
  onlyTitle: PropTypes.bool,
  buttonBackgroundColor: PropTypes.string,
  withoutBottomText: PropTypes.bool,
  withoutError: PropTypes.bool,
  buttonClass: PropTypes.string,
  buttonRefFunc: PropTypes.func,
  forPhotoArtConstructor: PropTypes.bool,
  buttonText: PropTypes.string,
  isContactImages: PropTypes.bool,
};

function ImageUploader(props) {
  const {
    className,
    titleClass,
    isImageUpload,
    isOrderEnhance,
    enhanceErrors,
    withoutText,
    uploadTitle,
    uploadText,
    onlyTitle,
    buttonBackgroundColor,
    withoutBottomText,
    withoutError,
    buttonClass,
    buttonRefFunc,
    forPhotoArtConstructor,
    buttonText,
    isContactImages,
  } = props;

  const isShowError = Boolean(enhanceErrors && enhanceErrors.length);

  return (
    <section className={b({ mix: className })}>
      {!withoutText && (
        <div className={b('upload-wrapper', { mix: titleClass })}>
          <h2 className={b('upload-title', { mix: 'title' })}>{uploadTitle}</h2>
          {!onlyTitle && <span className={b('upload-text')}>{uploadText}</span>}
        </div>
      )}
      {isImageUpload && (
        <div className={b('spinner-block')}>
          <img
            className={b('spinner', { invisible: !isImageUpload })}
            src={spinner.src}
            alt="spinner"
          />
        </div>
      )}
      {isShowError && !withoutError && (
        <span className={b('error')}>
          <img className={b('error-icon')} src={errorIcon} alt="error icon" />
          {enhanceErrors[0]}
        </span>
      )}
      <DragAndDropForm
        bemBlock={b}
        isSelectFiles
        isOrderEnhance={isOrderEnhance}
        isShowError={isShowError}
        buttonBackgroundColor={buttonBackgroundColor}
        withoutBottomText={withoutBottomText}
        buttonClass={buttonClass}
        buttonRefFunc={buttonRefFunc}
        forPhotoArtConstructor={forPhotoArtConstructor}
        buttonText={buttonText}
        className={className}
        isContactImages={isContactImages}
      />
    </section>
  );
}

ImageUploader.propTypes = propTypes;
ImageUploader.defaultProps = defaultProps;

const stateProps = (state) => ({
  isImageUpload: isImageUploadSelector(state),
  enhanceErrors: enhanceErrorsSelector(state),
});

export default connect(stateProps, null)(ImageUploader);
