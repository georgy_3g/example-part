import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Slider from 'react-slick';
import bem from 'src/utils/bem';
import debounce from 'lodash/debounce';
import Image from 'next/image';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import dynamic from 'next/dynamic';
import styles from './index.module.scss';

const ArrowInSquareIcon = dynamic(() => import('src/components/svg/ArrowInSquareIcon')); ;

const b = bem('polaroid-image-slider', styles);

const propTypes = {
  slideList: PropTypes.arrayOf(PropTypes.shape()),
  dots: PropTypes.bool,
  arrows: PropTypes.bool,
  slidesToShow: PropTypes.number,
  slidesToScroll: PropTypes.number,
  smallShow: PropTypes.number,
  smallScroll: PropTypes.number,
  className: PropTypes.string,
  imageClass: PropTypes.string,
  beforeChange: PropTypes.func,
  lazyLoad: PropTypes.bool,
  afterChange: PropTypes.func,
  autoplay: PropTypes.bool,
  pauseOnHover: PropTypes.bool,
  autoplaySpeed: PropTypes.number,
  withOutTitle: PropTypes.bool,
  withStrips: PropTypes.bool,
  isRight: PropTypes.bool,
  cardBackgroundStripeColor: PropTypes.string,
  cardBackgroundSecondStripeColor: PropTypes.string,
  infinite: PropTypes.bool,
  withoutControls: PropTypes.bool,
};
const defaultProps = {
  slideList: [],
  dots: false,
  arrows: false,
  slidesToShow: 1,
  slidesToScroll: 1,
  smallShow: 1,
  smallScroll: 1,
  className: '',
  imageClass: '',
  beforeChange: () => {},
  afterChange: () => {},
  lazyLoad: true,
  autoplay: false,
  pauseOnHover: false,
  autoplaySpeed: 5000,
  withOutTitle: false,
  withStrips: false,
  isRight: false,
  cardBackgroundStripeColor: '',
  cardBackgroundSecondStripeColor: '',
  infinite: true,
  withoutControls: false,
};

class PolaroidImageSlider extends Component {
  // throttleFun = throttle(() => {
  //   this.calculateWidth();
  // }, 300);

  constructor(props) {
    super(props);
    this.slider = null;
    this.state = {
      index: 0,
      // widthSlider: '',
    };
  }

  // componentDidMount() {
  //   this.throttleFun();
  //   window.addEventListener('resize', this.throttleFun);
  // }

  componentWillUnmount() {
    this.slider = null;
  }

  // calculateWidth = () => {
  //   const width = window.innerWidth;
  //   const { widthSlider } = this.state;
  //   const newWidth = Number((width / 100) * 30).toFixed();
  //   if (newWidth !== widthSlider) {
  //     this.setState({
  //       widthSlider: `${newWidth}px`,
  //     });
  //   }
  // };

  updateCount = (valueOne, valueTwo) => {
    const { beforeChange } = this.props;
    beforeChange(valueTwo);
    this.setState({ index: valueTwo });
  };

  generateDots = () => {
    const { slideList } = this.props;
    const { index } = this.state;
    return slideList.map((item, i) => {
      const { url } = item;
      return {
        isSelect: index === i,
        key: `${i}-${url}`,
        i,
      };
    });
  };

  clickToDot = (value) => {
    this.slider.slickGoTo(value);
    this.waitPlay();
  };

  clickNext = () => {
    this.slider.slickNext();
    this.waitPlay();
  };

  clickPrev = () => {
    this.slider.slickPrev();
    this.waitPlay();
  };

  clickPause = () => {
    this.slider.slickPause();
  };

  clickPlay = () => {
    if (this.slider !== null) {
      this.slider.slickPlay();
    }
  };

  waitPlay = () => {
    const { autoplay } = this.props;
    if (autoplay) {
      this.clickPause();
      const debounceFunc = debounce(() => {
        this.clickPlay();
      }, 4000);
      debounceFunc();
    }
  };

  render() {
    const {
      slideList,
      arrows,
      slidesToShow,
      slidesToScroll,
      className,
      imageClass,
      smallShow,
      smallScroll,
      afterChange,
      lazyLoad,
      autoplay,
      pauseOnHover,
      autoplaySpeed,
      withStrips,
      isRight,
      cardBackgroundStripeColor,
      cardBackgroundSecondStripeColor,
      infinite,
      withoutControls,
    } = this.props;

    const settings = {
      arrows,
      infinite,
      focusOnSelect: false,
      draggable: false,
      lazyLoad,
      speed: 500,
      slidesToShow,
      slidesToScroll,
      beforeChange: this.updateCount,
      afterChange,
      responsive: [
        {
          breakpoint: 900,
          settings: {
            slidesToShow: smallShow,
            slidesToScroll: smallScroll,
          },
        },
      ],
      dotsClass: b('dots'),
      autoplay,
      pauseOnHover,
      autoplaySpeed,
    };

    const dots = this.generateDots();

    return (
      <div
        className={b({ mix: className, withoutControls })}
      >
        <Slider
          ref={(e) => {
            this.slider = e;
          }}
          {...settings}
        >
          {slideList.map(({ url, id, alt, description }) => (
            <div className={b('slider-content')} key={`${id}__${alt}`}>
              <div className={b('inner-slide', { mix: imageClass })}>
                <Image
                  src={url}
                  alt={alt || url}
                  key={id}
                  layout="fill"
                />
              </div>
              <span className={b('slider-description')}>{description}</span>
            </div>
          ))}
        </Slider>
        {!withoutControls && (
          <div className={b('controls')}>
            <button
              className={b('arrow-btn', { prev: true })}
              type="button"
              onClick={this.clickPrev}
            >
              <ArrowInSquareIcon className={b('arrow-icon', { prev: true })} />
            </button>
            <div className={b('dot-block')}>
              {dots.map(({ isSelect, key, i }) => (
                <button
                  className={b('dot-wrap', { select: isSelect })}
                  key={key}
                  type="button"
                  onClick={() => {
                    this.clickToDot(i);
                  }}
                >
                  <div className={b('dot', { select: isSelect })} />
                </button>
              ))}
            </div>
            <button
              className={b('arrow-btn', { next: true })}
              type="button"
              onClick={this.clickNext}
            >
              <ArrowInSquareIcon className={b('arrow-icon', { next: true })} />
            </button>
          </div>
        )}
        {withStrips && (
          <div className={b('stripes-wrapper')}>
            <div
              className={b('image-stripe-one', { 'is-right': isRight })}
              style={{ backgroundColor: cardBackgroundStripeColor }}
            />
            <div
              className={b('image-stripe-two', { 'is-right': isRight })}
              style={{ backgroundColor: cardBackgroundSecondStripeColor }}
            />
          </div>
        )}
      </div>
    );
  }
}

PolaroidImageSlider.propTypes = propTypes;
PolaroidImageSlider.defaultProps = defaultProps;

export default PolaroidImageSlider;
