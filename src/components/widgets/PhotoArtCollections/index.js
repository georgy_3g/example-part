import React, { useState } from 'react';
import PropTypes from 'prop-types';
import get from 'lodash/get';
import { connect } from 'react-redux';
import ROUTES from 'src/constants/routes';
import bem from 'src/utils/bem';
import FrameCard from 'src/components/cards/FrameCard';
import dynamic from 'next/dynamic';
import PopupFrameCard from 'src/components/cards/PopupFrameCard';
import {
  frameMaterialsSelector,
  photoArtProductsSelector,
} from 'src/redux/photoArt/selectors';
import { addImageListAction } from 'src/redux/approvedOrderImage/actions';
import styles from './index.module.scss';
import { CLOSE, POPUP_TITLE, SELECT_PREFERRED } from './constants';

const CloseIcon2 = dynamic(() => import('src/components/svg/CloseIcon2'));
const Tabs = dynamic(() => import('src/components/elements/Tabs'));
const UniversalSlider = dynamic(() => import('src/components/widgets/UniversalSlider'));

const b = bem('photo-art-collections', styles);

const { photoArt } = ROUTES;

const propTypes = {
  className: PropTypes.string,
  sectionClassName: '',
  photoArtProducts: PropTypes.shape({}),
  addImageList: PropTypes.func,
  selectedPhotos: PropTypes.arrayOf({}),
  isPopup: PropTypes.bool,
  withoutCollectionSelector: PropTypes.bool,
  withSlider: PropTypes.bool,
  close: PropTypes.func,
  frameMaterials: PropTypes.arrayOf(PropTypes.shape({})),
  selectFrame: PropTypes.func,
  withSelectFunc: PropTypes.bool,
  selectedFrame: PropTypes.shape({}),
  refFunc: PropTypes.func,
  getSelectCollection: PropTypes.func,
};

const defaultProps = {
  className: '',
  sectionClassName: '',
  photoArtProducts: {},
  addImageList: () => {},
  selectedPhotos: () => {},
  isPopup: false,
  withoutCollectionSelector: false,
  withSlider: false,
  close: () => {},
  frameMaterials: [],
  selectFrame: () => {},
  withSelectFunc: false,
  selectedFrame: {},
  refFunc: () => {},
  getSelectCollection: () => {},
};

function PhotoArtCollections(props) {
  const {
    className,
    photoArtProducts,
    addImageList,
    selectedPhotos,
    isPopup,
    withoutCollectionSelector,
    withSlider,
    close,
    frameMaterials,
    selectedFrame,
    selectFrame,
    withSelectFunc,
    refFunc,
    getSelectCollection,
    sectionClassName,
  } = props;

  const tabs = [...frameMaterials]
    .sort((a, c) => a.order - c.order)
    .map(({ id, name, description }) => ({ id, text: name, description }));

  const firstMaterial = get(tabs, '[0].id', 0);

  const randomMaterial = get(
    tabs,
    `[${Math.floor(Math.random() * tabs.length)}].id`,
    firstMaterial,
  );

  const selectedMaterial = withoutCollectionSelector ? randomMaterial : firstMaterial;

  const collection = photoArtProducts[selectedMaterial]
    ? photoArtProducts[selectedMaterial].id
    : null;

  const [selectedCollection, selectCollection] = useState(collection);

  const setCollection = (material) => {
    selectCollection(material);
  };

  getSelectCollection(setCollection);

  const orderPrints = (value) => {
    const { frameMaterialId, id: frameId } = value;
    if (withSelectFunc) {
      selectFrame(value);
    } else {
      if (selectedPhotos && selectedPhotos.length) {
        addImageList(selectedPhotos.map(({ id, imageUri }) => ({ id, imageUri })));
      }
      document.location.href = `${photoArt}?collection=${frameMaterialId}&frame=${frameId}`;
    }
  };

  const currentItem = photoArtProducts[selectedCollection];
  const { name = 'None', frames = [] } = currentItem || {};

  const tabItemData = tabs.find((item) => item.id === Number(selectedCollection));

  const selectedCollectionText =
    tabItemData && tabItemData.description
      ? tabItemData.description
      : `${SELECT_PREFERRED} ${name.toLowerCase()} products`;

  const CardComponent = isPopup ? PopupFrameCard : FrameCard;

  return (
    <div className={b({ mix: className, popup: isPopup })} ref={refFunc}>
      {Boolean(Object.keys(photoArtProducts).length) && (
        <div className={b('content', { popup: isPopup })}>
          {!withSlider && (
            <>
              <section className={b('material-selector', { mix: sectionClassName })}>
                <Tabs
                  tabs={tabs}
                  activeTab={Number(selectedCollection)}
                  clickHandler={selectCollection}
                  tabListClass={b('tabs-list')}
                  withoutSelector
                  isPhotoArt
                />
              </section>
              <section className={b('frame-selector')}>
                <span className={b('frame-selector-info')}>{selectedCollectionText}</span>
                <div className={b('frame-list')}>
                  <div className={b('stripe')} />
                  {frames.map((item) => (
                    <div className={b('slid-wrap')} key={`frame_${item.id}`}>
                      <CardComponent
                        data={item}
                        imageData={
                          selectedPhotos && selectedPhotos.length ? selectedPhotos[0] : null
                        }
                        className={b('frame-card')}
                        select={orderPrints}
                        withOutHints={isPopup}
                        selected={selectedFrame || {}}
                        withOutStorePhoto
                      />
                    </div>
                  ))}
                </div>
              </section>
            </>
          )}
          {withSlider && (
            <>
              {isPopup && (
                <button className={b('close-btn')} type="button" onClick={close}>
                  <span className={b('close-text')}>{CLOSE}</span>
                  <div className={b('close-icon-wrap')}>
                    <CloseIcon2 />
                  </div>
                </button>
              )}
              <span className={b('popup-title')}>{POPUP_TITLE}</span>
              <UniversalSlider
                className={b('frame-slider')}
                cardType={isPopup ? 'popup-frame' : 'frame'}
                cards={frames}
                fullSHow={3}
                middleShow={3}
                tabletShow={3}
                mediumShow={2}
                smallShow={1}
                miniSHow={1}
                select={orderPrints}
                selected={selectedFrame || {}}
                imageData={selectedPhotos && selectedPhotos.length ? selectedPhotos[0] : null}
                withOutHints
                withOutStorePhoto
              />
            </>
          )}
        </div>
      )}
    </div>
  );
}
PhotoArtCollections.propTypes = propTypes;
PhotoArtCollections.defaultProps = defaultProps;

const stateProps = (state) => ({
  photoArtProducts: photoArtProductsSelector(state),
  frameMaterials: frameMaterialsSelector(state),
});

const actions = {
  addImageList: addImageListAction,
};

export default connect(stateProps, actions)(PhotoArtCollections);
