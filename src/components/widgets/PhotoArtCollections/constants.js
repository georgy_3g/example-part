const TITLE = 'Photo art collections.';
const SUBTITLE = 'Select and design your favorite hand crafted product from our collections.';
const SELECT_PREFERRED = 'Description about ';
const POPUP_TITLE =
  'Bravo! Now let’s create timeless art for\nyou to display and treasure *******.';
const DOWNLOAD_PHOTO = 'Download photo';
const CLOSE = 'Close';

export { TITLE, SUBTITLE, SELECT_PREFERRED, POPUP_TITLE, DOWNLOAD_PHOTO, CLOSE };
