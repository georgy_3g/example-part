const TITLE = 'Upload your photos to restore';
const TEXT = 'Select photos from our computer, tablet, or phone.';
const SELECT_IMAGE = 'Add photos';
const PER = 'per';
const REEL = 'photo';
const ADDITIONAL_PER_PHOTO = 'additional per photo';
const QUANTITY = 'Quantity';
const INFORMATION = 'Information:';
const ADD_COLOR = 'Add color';
const ADD_NOTE = 'Add note';
const DELETE = 'Delete';

export {
  TITLE,
  TEXT,
  SELECT_IMAGE,
  PER,
  REEL,
  ADDITIONAL_PER_PHOTO,
  QUANTITY,
  INFORMATION,
  ADD_COLOR,
  ADD_NOTE,
  DELETE,
};
