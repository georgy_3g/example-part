import React from 'react';
import PropTypes from 'prop-types';
import bem from 'src/utils/bem';
import { connect } from 'react-redux';
import dynamic from 'next/dynamic';
import { deleteImageEnhance } from 'src/redux/orderImages/actions';
import styles from './index.module.scss';

const EnhanceImageCard = dynamic(() => import('src/components/cards/EnhanceImageCard'));

const b = bem('image-select', styles);

const defaultProps = {
  className: '',
  orderImagesEnhance: [],
  deleteImageFromSlider: () => {},
  selectImage: () => {},
  colorizationImages: [],
  selectColorizationImage: () => {},
  isRetouching: false,
  isShipKit: false,
};

const propTypes = {
  className: PropTypes.string,
  orderImagesEnhance: PropTypes.arrayOf(PropTypes.shape({})),
  deleteImageFromSlider: PropTypes.func,
  selectImage: PropTypes.func,
  colorizationImages: PropTypes.arrayOf(PropTypes.shape({})),
  selectColorizationImage: PropTypes.func,
  isRetouching: PropTypes.bool,
  isShipKit: PropTypes.bool,
};

function ImageSelect(props) {
  const {
    className,
    deleteImageFromSlider,
    orderImagesEnhance,
    selectImage,
    colorizationImages,
    selectColorizationImage,
    isRetouching,
    isShipKit,
  } = props;
  const isNeedScroll = orderImagesEnhance.length > 6;
  return (
    <section className={b({ mix: className })}>
      <div className={b('images')} style={{ overflowY: isNeedScroll ? 'scroll' : 'none' }}>
        {orderImagesEnhance.map((item, index) => {
          return (
            <EnhanceImageCard
              key={item.id}
              className={b('image-card')}
              data={item}
              selectedArr={orderImagesEnhance}
              select={selectImage}
              deleteImageFromSlider={deleteImageFromSlider}
              isSelected={item.isSelected}
              colorizationImages={colorizationImages}
              selectColorizationImage={selectColorizationImage}
              isRetouching={isRetouching}
              portalId="enhance-wizard"
              isShipKit={isShipKit}
              index={index}
            />
          );
        })}
      </div>
    </section>
  );
}

ImageSelect.propTypes = propTypes;
ImageSelect.defaultProps = defaultProps;

const actions = {
  deleteImageEn: deleteImageEnhance,
};

export default connect(null, actions)(ImageSelect);
