import React, { Component, Fragment } from "react";
import PropTypes from 'prop-types';
import bem from 'src/utils/bem';
import { connect } from 'react-redux';
import Colors from 'src/styles/colors.json';

import icon from 'public/img/shared/cloud-lock.jpg';
import dynamic from 'next/dynamic';
import { orderPriceSelect } from 'src/redux/prices/selectors';
import { daysAfterSubscribeSelector } from 'src/redux/myCloud/selectors';
import {
  ALL,
  COLORIZE_PHOTO,
  DOWNLOAD,
  GO_BACK,
  NONE,
  ORDER_ART,
  ORDER_PRINTS,
  RESTORE_PHOTO,
  RETOUCH_PHOTO,
  SELECT,
  SELECTED,
  SHARE,
  SLIDESHOW,
  VIEW_TYPE_TITLE,
  VIEW_TYPES,
  MESSAGE,
  YOUR_*******_CLOUD,
} from './constants';
import styles from './index.module.scss';

const CloseIcon2 = dynamic(() => import('src/components/svg/CloseIcon2'));
const TileIcon = dynamic(() => import('src/components/svg/TileIcon'));
const SliderIcon = dynamic(() => import('src/components/svg/SliderIcon'));
const DownloadFileIcon = dynamic(() => import('src/components/svg/DownloadFileIcon'));
const ShareFileIcon = dynamic(() => import('src/components/svg/ShareFileIcon'));
const SlideshowIcon = dynamic(() => import('src/components/svg/SlideshowIcon'));
const CloudDayCounter = dynamic(() => import('src/components/widgets/CloudDayCounter'));
const ArrowIcon = dynamic(() => import('src/components/svg/ArrowIcon'));
const NewSelectOrders = dynamic(() => import('src/components/elements/NewSelectOrders'));
const PrintIcon = dynamic(() => import('src/components/svg/PrintIcon/index.tsx'));
const CreateIcon = dynamic(() => import('src/components/svg/CreateIcon'));
const FolderIcon = dynamic(() => import('src/components/svg/FolderIcon'));


const grayColor = Colors['$darker-cadet-blue-color'];
const blueColor = Colors['$pickled-bluewood-color'];
const gray = Colors['$light-slate-gray-color'];
const silverGray = Colors['#silver-gray-color'];

const b = bem('my-files-header', styles);

const defaultProps = {
  className: '',
  viewType: VIEW_TYPES.tile,
  changeFilter: () => {},
  changeView: () => {},
  downloadCloudImages: () => {},
  selectAll: () => {},
  removeAll: () => {},
  orderPrints: () => {},
  numberOfSelected: 0,
  forSharedPage: false,
  clickShared: () => {},
  isButtonDisabled: true,
  isButtonOrderPrintDisabled: true,
  isSlideshowDisabled: true,
  allPrices: {},
  enhancePhoto: () => {},
  query: {},
  setNewUrl: () => {},
  changeScene: () => {},
  openSlideshow: () => {},
  openSubscribePopup: () => {},
  isLockDigitizeFiles: false,
  openHint: () => {},
  goBack: () => {},
  isOpenFolder: {},
  disableSlider: false,
  clickOrderPrints: () => {},
  cloudImages: () => {},
  daysAfter: 0,
};

const propTypes = {
  className: PropTypes.string,
  viewType: PropTypes.string,
  changeFilter: PropTypes.func,
  changeView: PropTypes.func,
  downloadCloudImages: PropTypes.func,
  selectAll: PropTypes.func,
  removeAll: PropTypes.func,
  orderPrints: PropTypes.func,
  numberOfSelected: PropTypes.number,
  forSharedPage: PropTypes.bool,
  clickShared: PropTypes.func,
  isButtonDisabled: PropTypes.bool,
  isButtonOrderPrintDisabled: PropTypes.bool,
  isSlideshowDisabled: PropTypes.bool,
  allPrices: PropTypes.objectOf(PropTypes.shape({
    id: PropTypes.number,
  })),
  enhancePhoto: PropTypes.func,
  query: PropTypes.shape({}),
  setNewUrl: PropTypes.func,
  changeScene: PropTypes.func,
  openSlideshow: PropTypes.func,
  openSubscribePopup: PropTypes.func,
  isLockDigitizeFiles: PropTypes.bool,
  openHint: PropTypes.func,
  goBack: PropTypes.func,
  isOpenFolder: PropTypes.shape({}),
  disableSlider: PropTypes.bool,
  clickOrderPrints: PropTypes.func,
  cloudImages: PropTypes.func,
  daysAfter: PropTypes.number,
};

class MyFilesHeader extends Component {
  constructor(props) {
    super(props);
    this.veil = React.createRef();
    this.state = {
      isShowEnhancePopup: false,
    };
  }

  closeModal = (e) => {
    if (e.target === this.veil.current) {
      this.setState({ isShowEnhancePopup: false });
    }
  };

  togglerPopup = () => {
    const { isShowEnhancePopup } = this.state;
    this.setState({ isShowEnhancePopup: !isShowEnhancePopup });
  };

  changeFilterType = (filterType) => {
    const { changeFilter } = this.props;
    changeFilter(filterType);
  };

  changeViewType = (viewType) => () => {
    const { changeView } = this.props;
    changeView(viewType);
  };

  render() {
    const {
      className,
      viewType,
      downloadCloudImages,
      selectAll,
      removeAll,
      orderPrints,
      numberOfSelected,
      forSharedPage,
      clickShared,
      isButtonDisabled,
      isButtonOrderPrintDisabled,
      allPrices,
      enhancePhoto,
      openSlideshow,
      isSlideshowDisabled,
      openSubscribePopup,
      isLockDigitizeFiles,
      openHint,
      goBack,
      isOpenFolder,
      disableSlider,
      clickOrderPrints,
      cloudImages,
      query,
      setNewUrl,
      changeScene,
      daysAfter,
    } = this.props;

    const { isShowEnhancePopup } = this.state;

    const {
      photo_restoration: photoRestoration,
      photo_colorization: photoColorization,
      photo_retouching: photoRetouching,
    } = allPrices;

    return (
      <div className={b({ mix: className })}>
        {!forSharedPage && (
          <div className={b('first-row')}>
            <div className={b('background')} />
            <NewSelectOrders
              className={b('order-selector')}
              changeScene={changeScene}
              query={query}
              setNewUrl={setNewUrl}
              isSearchable
              isCloud
            />
            <div className={b('logo-block')}>
              <img
                className={b('icon')}
                src={icon.src}
                alt="lock-icon"
              />
              <span className={b('logo-text')}>{YOUR_*******_CLOUD}</span>
            </div>
            <div className={b('cloud-block')}>
              <CloudDayCounter
                className={b('cloud-counter')}
                openSubscribePopup={openSubscribePopup}
                openHint={openHint}
              />
            </div>
          </div>
        )}
        {daysAfter > 90
          ? (<div className={b('message-block')}>{MESSAGE}</div>)
          : (
            <>
              <div className={b('second-row')}>
                <div className={b('empty-block')} />
                <div className={b('middle-block')}>
                  <div className={b('all-select-block')}>
                    <span className={b('select-title')}>{SELECT}</span>
                    <button className={b('all-select-btn')} type="button" onClick={selectAll}>
                      {ALL}
                    </button>
                    <span className={b('vertical-separator')}>|</span>
                    <button className={b('none-select-btn')} type="button" onClick={removeAll}>
                      {NONE}
                    </button>
                  </div>
                  <div className={b('counter-block')}>
                    <span className={b('count')}>{numberOfSelected}</span>
                    <span className={b('counter-title')}>{SELECTED}</span>
                  </div>
                </div>
                <div className={b('function-block')}>
                  <button
                    className={b('btn-funk')}
                    disabled={isButtonOrderPrintDisabled}
                    type="button"
                    onClick={() => clickOrderPrints(cloudImages)}
                  >
                    <div
                      className={b('btn-funk-icon', { prints: true })}
                      style={{ borderColor: isButtonOrderPrintDisabled ? silverGray : gray }}
                    >
                      <PrintIcon
                        className={b('btn-block-icon')}
                        disabled={isButtonOrderPrintDisabled}
                      />
                    </div>
                    {ORDER_PRINTS}
                  </button>
                  <button
                    className={b('btn-funk')}
                    disabled={isButtonOrderPrintDisabled}
                    type="button"
                    onClick={() => {
                      orderPrints(cloudImages);
                    }}
                  >
                    <CreateIcon
                      className={b('btn-funk-icon')}
                      disabled={isButtonOrderPrintDisabled}
                    />
                    {ORDER_ART}
                  </button>
                  {!isLockDigitizeFiles && (
                  <button
                    className={b('btn-funk')}
                    disabled={isButtonDisabled}
                    type="button"
                    onClick={downloadCloudImages}
                  >
                    <DownloadFileIcon className={b('btn-funk-icon')} disabled={isButtonDisabled} />
                    {DOWNLOAD}
                  </button>
                  )}
                  {!forSharedPage && !isLockDigitizeFiles && (
                  <button
                    className={b('btn-funk')}
                    disabled={isButtonDisabled}
                    type="button"
                    onClick={clickShared}
                  >
                    <ShareFileIcon className={b('btn-funk-icon')} disabled={isButtonDisabled} />
                    {SHARE}
                  </button>
                  )}
                  {!isLockDigitizeFiles && (
                  <button
                    className={b('btn-funk')}
                    disabled={isSlideshowDisabled}
                    type="button"
                    onClick={openSlideshow}
                  >
                    <SlideshowIcon className={b('btn-funk-icon')} disabled={isSlideshowDisabled} />
                    {SLIDESHOW}
                  </button>
                  )}
                </div>
                <div className={b('left-block')}>
                  <div className={b('view-type-block')}>
                    {!isLockDigitizeFiles && (
                    <div className={b('view-type-wrapper')}>
                      <div className={b('view-type-title')}>{VIEW_TYPE_TITLE}</div>
                      <button
                        className={b('view-type-button')}
                        type="button"
                        onClick={this.changeViewType(VIEW_TYPES.tile)}
                      >
                        <TileIcon
                          className={b('view-button-icon')}
                          stroke={viewType === VIEW_TYPES.tile ? blueColor : grayColor}
                        />
                      </button>
                      <button
                        className={b('view-type-button')}
                        type="button"
                        onClick={this.changeViewType(VIEW_TYPES.slider)}
                        disabled={disableSlider}
                      >
                        <SliderIcon
                          className={b('view-button-icon')}
                          stroke={viewType === VIEW_TYPES.slider ? blueColor : grayColor}
                        />
                      </button>
                    </div>
                    )}
                  </div>
                </div>
              </div>
              <div className={b('third-row')}>
                {Object.keys(isOpenFolder).length ? (
                  <div className={b('back-btn-container')}>
                    <div
                      className={b('back-btn')}
                      tabIndex={0}
                      role="button"
                      onClick={() => goBack(null)}
                    >
                      <ArrowIcon
                        className={b('back-icon')}
                        width="9"
                        height="20"
                        strokeWidth="3"
                        stroke={gray}
                      />
                      <p className={b('back-text')}>{GO_BACK}</p>
                    </div>
                    {Object.entries(isOpenFolder).map(([, value]) => (
                      <div className={b('folder-container')} key={value}>
                        <FolderIcon className={b('folder-icon')} />
                        <p className={b('folder-text')}>{value}</p>
                      </div>
                    ))}
                  </div>
                ) : (
                  <div className={b('back-btn-container')} />
                )}
                <div className={b('middle-block', { mobile: true })}>
                  <div className={b('all-select-block')}>
                    <span className={b('select-title')}>{SELECT}</span>
                    <button className={b('all-select-btn')} type="button" onClick={selectAll}>
                      {ALL}
                    </button>
                    <span className={b('vertical-separator')}>|</span>
                    <button className={b('none-select-btn')} type="button" onClick={removeAll}>
                      {NONE}
                    </button>
                  </div>
                  <div className={b('counter-block')}>
                    <span className={b('count')}>{numberOfSelected}</span>
                    <span className={b('counter-title')}>{SELECTED}</span>
                  </div>
                </div>
              </div>
            </>
)}
        {isShowEnhancePopup && (
          <div
            className={b('enhance-popup')}
            ref={this.veil}
            onClick={this.closeModal}
            role="button"
            tabIndex="-1"
          >
            <div className={b('content-block')}>
              <button className={b('close-btn')} type="button" onClick={this.togglerPopup}>
                <CloseIcon2 />
              </button>
              <button
                className={b('btn', { 'popup-btn': true })}
                type="button"
                onClick={() => {
                  enhancePhoto('photo_retouching', photoRetouching.id);
                }}
              >
                {RETOUCH_PHOTO}
              </button>
              <button
                className={b('btn', { 'popup-btn': true })}
                type="button"
                onClick={() => {
                  enhancePhoto('photo_restoration', photoRestoration.id);
                }}
              >
                {RESTORE_PHOTO}
              </button>
              <button
                className={b('btn', { 'popup-btn': true })}
                type="button"
                onClick={() => {
                  enhancePhoto('photo_colorization', photoColorization.id);
                }}
              >
                {COLORIZE_PHOTO}
              </button>
            </div>
          </div>
        )}
      </div>
    );
  }
}

MyFilesHeader.propTypes = propTypes;
MyFilesHeader.defaultProps = defaultProps;

const stateProps = (state) => ({
  allPrices: orderPriceSelect(state),
  daysAfter: daysAfterSubscribeSelector(state),
});

export default connect(stateProps, null)(MyFilesHeader);
