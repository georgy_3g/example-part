import dynamic from "next/dynamic";

const EnhanceIcon = dynamic(() => import('src/components/svg/EnhanceIcon'));
const ScanIcon = dynamic(() => import('src/components/svg/ScanIcon'));
const VideoIcon = dynamic(() => import('src/components/svg/VideoIcon'));
const AudioIcon = dynamic(() => import('src/components/svg/AudioIcon'));
const ORDER_IMAGES = 'orderImages';
const ORDER_VIDEOS = 'orderVideos';
const ORDER_AUDIOS = 'orderAudios';
const ORDER_SCAN = 'orderScan';
const ALL_MEDIA = 'allMedia';

const FILTER_BUTTONS = [
  {
    type: ALL_MEDIA,
    Component: ScanIcon,
    isShow: true,
    text: 'All media',
    name: 'All media',
  },
  {
    type: ORDER_SCAN,
    Component: ScanIcon,
    isShow: true,
    text: 'Viewing photos',
    name: 'Photos',
  },
  {
    type: ORDER_VIDEOS,
    Component: VideoIcon,
    isShow: true,
    text: 'Viewing videos',
    name: 'Videos',
  },
  {
    type: ORDER_AUDIOS,
    Component: AudioIcon,
    isShow: true,
    text: 'Viewing audios',
    name: 'Audio',
  },
  {
    type: ORDER_IMAGES,
    Component: EnhanceIcon,
    isShow: true,
    text: 'Viewing enhanced',
    name: 'Enhanced',
  },
];

const VIEW_RESTORATIONS = 'View restorations';
const VIEW_GALLERY = 'View gallery';
const VIEW_MULTIPLE = 'View multiple';
const ORDER_ART = 'Create art';
const ORDER_PRINTS = 'Order prints';
const DOWNLOAD = 'Download';
const SHARE = 'Share';
const SLIDESHOW = 'Slideshow';
const SELECT = 'Select';
const ALL = 'All';
const NONE = 'None';
const SELECTED = 'Selected';
const VIEW_IMAGES = 'View photo';
const VIEW_VIDEOS = 'View videos';
const VIEW_AUDIOS = 'Listen to the audio';
const TITLE = 'tile';
const SLIDER = 'slider';
const ENHANCE = 'Enhance';
const MESSAGE = 'Your files have been archived,\nplease contact us to request restoring your files.'

const FILTER_TEXT = {
  [ORDER_IMAGES]: VIEW_IMAGES,
  [ORDER_VIDEOS]: VIEW_VIDEOS,
  [ORDER_AUDIOS]: VIEW_AUDIOS,
};

const TYPE_TEXT = {
  [TITLE]: VIEW_MULTIPLE,
  [SLIDER]: VIEW_GALLERY,
};

const VIEW_TYPES = {
  tile: TITLE,
  slider: SLIDER,
};

const RETOUCH_PHOTO = 'Retouch Photos';
const RESTORE_PHOTO = 'Restore Photos';
const COLORIZE_PHOTO = 'Colorize Photos';
const INFO =
  'Please select your desired file(s), then you can \ndownload, share, or create photo art.';
const VIEW_TYPE_TITLE = 'View type';
const GO_BACK = 'Go back';
const YOUR_*******_CLOUD = 'Your *******';

export {
  FILTER_BUTTONS,
  VIEW_RESTORATIONS,
  VIEW_TYPES,
  VIEW_MULTIPLE,
  ORDER_PRINTS,
  ORDER_ART,
  DOWNLOAD,
  SHARE,
  SLIDESHOW,
  SELECT,
  ALL,
  NONE,
  SELECTED,
  FILTER_TEXT,
  TYPE_TEXT,
  ENHANCE,
  RETOUCH_PHOTO,
  RESTORE_PHOTO,
  COLORIZE_PHOTO,
  INFO,
  VIEW_TYPE_TITLE,
  ORDER_IMAGES,
  GO_BACK,
  YOUR_*******_CLOUD,
  MESSAGE,
};
