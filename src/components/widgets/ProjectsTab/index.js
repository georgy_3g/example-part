import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import bem from 'src/utils/bem';
import generateRotateStyle from 'src/utils/generateRotateStyle';
import formattedImage from 'src/utils/formattedImage';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import debounce from 'lodash/debounce';
import ROUTES from 'src/constants/routes';
import Colors from 'src/styles/colors.json';
import format from 'date-fns/format';
import CustomSelect from 'src/components/inputs/CustomSelect';

import orderImage from 'public/img/shared/navi-img.png';
import audioImage from 'public/img/shared/product-icons/audio-checkout-icon-225x160.jpg';
import deviceImage from 'public/img/shared/product-icons/device-checkout-icon-225x160.jpg';
import filmImage from 'public/img/shared/product-icons/film-checkout-icon-225x160.jpg';
import scanImage from 'public/img/shared/product-icons/scan-checkout-icon-225x160.jpg';
import tapeImage from 'public/img/shared/product-icons/tape-checkout-icon-225x160.jpg';
import enhanceShipKit from 'public/img/shared/product-icons/ENhANCE-SHIP-KIT.png';
import enhanceImage from 'public/img/shared/product-icons/ENHANCE-UPLOAD-CHECKOUT.jpg';
import dynamic from 'next/dynamic';
import {
  isDeletingProjectSelector,
  isEditProjectNameSelector,
  projectListSelector,
} from 'src/redux/projects/selectors';
import { orderPriceSelect } from 'src/redux/prices/selectors';
import { getToken } from 'src/redux/auth/selectors';
import { photoArtProductsSelector } from 'src/redux/photoArt/selectors';
import { downloadOrderImages } from 'src/redux/ordersList/actions';
import { addAllFilesToCloud } from 'src/redux/myCloud/actions';
import {
  deleteProjectAction,
  editProjectDataAction,
  getProjectListAction,
} from 'src/redux/projects/actions';
import { addToCart } from 'src/redux/orders/actions';
import { addCroppedImagesToCart } from 'src/redux/orderImages/actions';
import { getPromoSection } from 'src/redux/promo/actions';
import { addImageListAction } from 'src/redux/approvedOrderImage/actions';
import {
  APPROVE,
  APPROVED_TYPE,
  AWAITING,
  BACK_BUTTON_TEXT,
  CREATION_DATE,
  DELETE,
  DOWNLOAD,
  EDIT,
  EDIT_PROJECT,
  ENHANCE,
  FILTER,
  FILTER_ARR,
  FILTER_TYPE_ARR,
  LAST_EDIT,
  ORDER,
  ORDER_ART,
  ORDERED,
  PHOTO_ART,
  PHOTOS,
  PROCESS,
  PRODUCT_NAME_ENHANCE,
  PRODUCT_NAME_PHOTO_ART,
  RENAME,
  REORDER,
  SAVED,
  SHARE,
  SORT,
  SORT_ARR,
  TYPE,
  UNTITLED_PROJECT,
  VIEW,
} from './constants';
import styles from './index.module.scss';

const MinDownloadIcon = dynamic(() => import('src/components/svg/MinDownloadIcon'));
const MinPhotoArtIcon = dynamic(() => import('src/components/svg/MinPhotoArtIcon'));
const SharePopup = dynamic(() => import('src/components/popups/SharePopup'));
const CheckFull = dynamic(() => import('src/components/svg/CheckFull'));
const ArrowInCircle = dynamic(() => import('src/components/svg/ArrowInCircle'));
const EditNameIcon = dynamic(() => import('src/components/svg/EditNameIcon'));

const Proofs = dynamic(() => import('src/components/widgets/Proofs'));
const CloseIcon2 = dynamic(() => import('src/components/svg/CloseIcon2'));
const MyCloudPhotoArt = dynamic(() => import('src/components/widgets/MyCloudPhotoArt'));
const CartIcon2 = dynamic(() => import('src/components/svg/CartIcon2'));
const MinShareIcon = dynamic(() => import('src/components/svg/MinShareIcon'));
const TrashIcon2 = dynamic(() => import('src/components/svg/TrashIcon2'));

const b = bem('projects-tab', styles);
const greenColor = Colors['$summer-green-color'];
const redColor = Colors['$sea-pink-red-color'];
const blueColor = Colors['$pickled-bluewood-color'];

const { photoArtEditor } = ROUTES;

const defaultProps = {
  projectList: [],
  productList: {},
  downloadImages: () => {  },
  token: '',
  addFilesToCloud: () => {  },
  getProjectList: () => {  },
  deleteProject: () => {  },
  redirect: () => {  },
  isDeleting: false,
  openCart: () => {  },
  addImages: () => {  },
  addOrder: () => {  },
  editProject: () => {  },
  isLoading: false,
  query: {
    order: null,
    versionImageId: null,
  },
  onRef: () => {  },
  getPromo: () => {  },
  addImageList: () => {  },
  photoArtProducts: {},
};

const propTypes = {
  projectList: PropTypes.arrayOf(PropTypes.shape({})),
  productList: PropTypes.shape({}),
  downloadImages: PropTypes.func,
  token: PropTypes.string,
  redirect: PropTypes.func,
  addFilesToCloud: PropTypes.func,
  getProjectList: PropTypes.func,
  deleteProject: PropTypes.func,
  isDeleting: PropTypes.bool,
  openCart: PropTypes.func,
  addImages: PropTypes.func,
  addOrder: PropTypes.func,
  editProject: PropTypes.func,
  isLoading: PropTypes.bool,
  query: PropTypes.shape({
    order: PropTypes.number,
    versionImageId: PropTypes.string,
  }),
  onRef: PropTypes.func,
  getPromo: PropTypes.func,
  addImageList: PropTypes.func,
  photoArtProducts: PropTypes.shape({}),
};

class ProjectsTab extends Component {
  resizeWindow = debounce(() => {
    this.isMobile();
  }, 300);

  constructor(props) {
    super(props);
    this.oneRender = true;
    this.state = {
      isShowCloud: false,
      startFrame: null,
      startCollection: null,
      showSharedPopup: false,
      selectedData: null,
      isShowProof: false,
      sortValue: SORT_ARR[0],
      filterValue: FILTER_ARR[0],
      selectedProject: null,
      selectedOrderId: null,
      newProjectName: '',
      currentProject: null,
      typeFilterValue: FILTER_TYPE_ARR[0],
      isScaleMode: false,
    };
  }

  componentDidMount() {
    window.addEventListener('resize', this.resizeWindow);
    this.isMobile();
    const {
      query: { order, versionImageId },
      projectList,
      getProjectList,
      token,
      onRef,
    } = this.props;
    const { sortValue, selectedProject } = this.state;
    onRef(this);
    this.updatePromoSection();
    if (!projectList.length) {
      getProjectList({ token, order: sortValue.value });
    }
    if (projectList.length && !selectedProject && (versionImageId || order)) {
      this.oneRender = false;
      const { id } = projectList.find(({ orderId }) => orderId === Number(order)) || {};
      this.setOrderId(id);
    }
  }

  componentDidUpdate(prevProps, prevState) {
    const {
      isDeleting,
      getProjectList,
      token,
      isLoading,
      projectList,
      query: { versionImageId, order },
    } = this.props;
    const { isDeleting: oldFlag, isLoading: oldLoadingFlag } = prevProps;
    const {
      sortValue,
      selectedProject,
      isShowCloud,
    } = this.state;

    const {
      isShowCloud: oldShowFlag,
      selectedProject: oldProject,
    } = prevState;

    if ((oldFlag && !isDeleting) || (oldLoadingFlag && !isLoading)) {
      getProjectList({ token, order: sortValue.value });
    }

    if (projectList.length && this.oneRender && !selectedProject && (versionImageId || order)) {
      this.oneRender = false;
      const { id } = projectList.find(({ orderId }) => orderId === Number(order)) || {};
      this.setOrderId(id);
    }

    if (
      (isShowCloud !== oldShowFlag)
      || (Boolean(selectedProject) !== Boolean(oldProject))
    ) {
      this.updatePromoSection();
    }
  }

  componentWillUnmount() {
    const { onRef } = this.props;
    onRef(undefined);
    this.oneRender = null;
    window.removeEventListener('resize', this.resizeWindow);
  }

  isMobile = () => {
    const { isScaleMode } = this.state;

    const isScaleModeNew = window.innerWidth >= 1025 && window.innerWidth <= 1920 ;
    if (isScaleMode !== isScaleModeNew) {
      this.setState({ isScaleMode: isScaleModeNew });
    }
  };

  updatePromoSection = () => {
    const { getPromo } = this.props;
    const {
      isShowCloud,
      selectedProject,
    } = this.state;
    if (!isShowCloud && !selectedProject) {
      getPromo({ key: 'account?main=proofs' });
    } else if (!isShowCloud && selectedProject) {
      getPromo({ key: 'account?main=proof' });
    } else if (isShowCloud) {
      getPromo({ key: 'account?main=create' });
    }
  }

  closeProject = () => {
    const { isShowProof } = this.state;
    if (isShowProof) {
      this.closeProof();
    }
  };

  setOrderId = (id) => {
    this.setState({ isShowProof: true, selectedProject: id });
  }

  isShowCloudChange = (value, frame, collectionId) => {
    this.setState({
      isShowCloud: value,
      startFrame: frame || null,
      startCollection: collectionId || null,
    });
  }

  goToArt = (images) => {
    const { addImageList, photoArtProducts } = this.props;

    const { frames, id: collectionId } = Object.values(photoArtProducts).find(
      ({ name }) => name.toLowerCase() === 'classic',
    ) || Object.values(photoArtProducts)[0];

    const { id: frameId } = frames.find(({ isDefault }) => isDefault)
      || frames[0];

    const date = new Date();

    const newImages = images.map(item => ({
      ...item,
      addedAt: date.toISOString(),
    }))

    addImageList(newImages);
    window.location.href = `${photoArtEditor}?collection=${collectionId}&frame=${frameId}&tab=size`;
  }

  clearPreselect = () => {
    this.setState({
      startFrame: null,
      startCollection: null,
    });
  }

  getDefaultImage = (typeProduct, isEasyScanKit) => {
    switch (typeProduct) {
      case 'tape_transfer':
        return tapeImage;
      case 'film_transfer':
        return filmImage;
      case 'audio_transfer':
        return audioImage;
      case 'digital_transfer':
        return deviceImage;
      case 'photo_scan':
        return scanImage;
      case 'photo_restoration':
      case 'photo_colorization':
      case 'photo_retouching':
        return isEasyScanKit ? enhanceShipKit : enhanceImage;
      default:
        return orderImage;
    }
  };

  getProductInfo = (productId) => {
    const { productList } = this.props;
    const {
      displayName = '',
      name,
    } = Object.values(productList).find(item => item && item.id === productId) || {};
    return { displayName, productName: name };
  }

  getDataInfo = (data) => {
    const {
      stateData: {
        selectedFrameData: {
          displayName = '',
        } = {},
      } = {},
    } = data || {};
    return {
      frameName: displayName,
      photoArtQuantity: 1,
    };
  }

  downloadItemImages = (images, orderId) => {
    const { downloadImages, token } = this.props;
    const data = { images: images.filter(item => item.isApproved).map(item => item.id) };
    if (data && data.images.length) {
      downloadImages({ data, token, id: orderId });
    }
  }

  getApprovedImages = images => (images ? images.filter(item => item.isApproved) : [])

  openSharedPopup = (selectedData, orderId) => {
    this.setState({ showSharedPopup: true, selectedData, selectedOrderId: orderId });
  }

  closeSharedPopup = () => {
    this.setState({ showSharedPopup: false, selectedData: null, selectedOrderId: null });
  }

  showProof = (id) => {
    this.setState({ isShowProof: true, selectedProject: id });
  }

  closeProof = () => {
    const { redirect } = this.props;
    this.setState({ isShowProof: false, selectedProject: null });
    redirect('proofs', { versionImageId: false, order: false });
  }

  changeSort = (value) => {
    const { token, getProjectList } = this.props;
    const { sortValue } = this.state;
    if (sortValue.value !== value.value) {
      this.setState({ sortValue: value });
      getProjectList({ token, order: value.value });
    }
  }

  changeFilter = (value) => {
    this.setState({ filterValue: value });
  }

  changeTypeFilter = (value) => {
    this.setState({ typeFilterValue: value });
  }

  updateProjectList = () => {
    const { token, getProjectList } = this.props;
    const { sortValue } = this.state;
    getProjectList({ token, order: sortValue.value });
  }

  getImagesToProof = (images, imageVersions) => {
    const parentImages = [...images];
    const versionImages = parentImages.reduce((acc, item) => {
      const { id } = item;
      const { versions, versionsWithoutPhoto } = imageVersions
        .filter(itm => itm.parentOrderImageId === id)
        .reduce((versionsAcc, version) => {
          const { orderImage: image, versionId } = version;
          const {
            versions: accVersions,
            versionsWithoutPhoto: accVersionsWithoutPhoto,
          } = versionsAcc;
          if (image && image.id) {
            return {
              versions: [...accVersions, { ...image, versionId }],
              versionsWithoutPhoto: [...accVersionsWithoutPhoto],
            };
          }
          return {
            versions: [...accVersions],
            versionsWithoutPhoto: [...accVersionsWithoutPhoto, version],
          };
        }, { versions: [], versionsWithoutPhoto: [] });

      return [
        ...acc,
        {
          ...item,
          versions: [...versions].sort((first, second) => first.id - second.id),
          versionsWithoutPhoto: [...versionsWithoutPhoto],
        },
      ];
    }, []);
    return versionImages;
  }

  delete = (id) => {
    const { token, deleteProject } = this.props;
    deleteProject({ id, token });
  }

  addToCart = (projectId) => {
    const {
      projectList,
      addImages,
      addOrder,
    } = this.props;

    const currentProject = projectList.find(item => item.id === projectId) || {};

    const {
      data: {
        stateData: {
          selectedImage,
          selectedMatting,
          selectedOption,
          activeTab,
          sizeValue,
          colorValue,
          isPersonalizeOn,
          personalizeText,
          selectedFrameData,
          displayValue: { id: displayId, name: nameDisplay } = {},
        },
      },
      croppedImageId,
      croppedImage,
    } = currentProject;

    const { id } = selectedImage || {};
    const { framePhotoArtMatting } = selectedFrameData;
    const name = PRODUCT_NAME_PHOTO_ART;
    const newImages = [croppedImageId, id];
    const {
      id: colorId,
      value,
      framePhotoMattingId,
      label,
    } = selectedMatting || {};
    const order = {
      title: selectedFrameData.displayName,
      typeProduct: name,
      price: selectedOption.price,
      quantity: 1,
      isGift: false,
      productId: selectedOption.parentId,
      material: activeTab,
      size: sizeValue.label,
      color: colorValue.label,
      images: [...newImages],
      options: [
        {
          optionId: selectedOption.id,
          quantity: 1,
          name,
        },
      ],
      customText: isPersonalizeOn ? personalizeText : '',
      isMatting: Boolean(value),
      framePhotoMattingId,
      framePhotoMattingColorId: colorId,
      mattingPrice: (framePhotoArtMatting && framePhotoArtMatting.price)
        ? framePhotoArtMatting.price : 0,
      mattingName: label,
      photoArtOptions: {
        size: sizeValue.label,
        color: colorValue.label,
      },
      display: displayId
        ? {
          id: displayId,
          name: nameDisplay,
        }
        : {},
      project: currentProject,
    };
    addImages([croppedImage]);
    addOrder(order);
    window.location.href = ROUTES.checkout
  }

  checkAllApprove = (images) => {
    const versions = images.reduce((acc, item) => {
      const { parentId, id } = item;
      if (!parentId) {
        return { ...acc, [id]: [] };
      }
      return { ...acc, [parentId]: [...(acc[parentId] ? acc[parentId] : []), item] };
    }, {});

    return (
      Object.keys(versions).length &&
      Object.values(versions).every((item) => item.some(({ isApproved }) => isApproved))
    );
  };

  getPhotoArtPhoto = (projectId) => {
    const { projectList } = this.props;

    const currentProject = projectList.find(item => item.id === projectId) || {};

    const {
      data: {
        stateData: {
          selectedMatting,
          selectedOption,
        },
      },
      croppedImage,
    } = currentProject;

    const isMatting = Boolean(Object.keys(currentProject.data).length && selectedMatting && selectedMatting.value);

    const { minDisplay, frameImageOptions } = Object.keys(currentProject.data).length ? selectedOption.frameDisplays[0] : {};

    const { colorHex = '' } = selectedMatting ? selectedMatting.value : {};

    const {
      frameScale,
      frameScaleX,
      frameLeft,
      frameTop,
      frameRotateX,
      frameRotateY,
      frameRotateZ,
      framePerspective,
      framePerspectiveOrigin,
      imageUri,
      backgroundLeft,
      backgroundScale,
      backgroundTop,
    } = minDisplay;

    const {
      paddingVertical = 0,
      paddingHorizontal = 0,
      innerType = null,
    } = selectedMatting ? selectedMatting.value : {};

    const top = isMatting ? paddingVertical : 0;
    const left = isMatting ? paddingHorizontal : 0;

    const { frameImageUrl } = selectedOption;

    const {
      height,
      width,
      x,
      y,
    } = frameImageOptions || {};

    const frameBlockStyle = {
      height: `${frameScale}%`,
      width: `${frameScaleX}%`,
      left: `${frameLeft}%`,
      top: `${frameTop}%`,
      perspective: `${framePerspective}px`,
      perspectiveOrigin: framePerspectiveOrigin,
    };

    const blockStyle = {
      backgroundColor: isMatting ? `#${colorHex}` : 'none',
      height: `${height}%`,
      width: `${width}%`,
      left: `${x}%`,
      top: `${y}%`,
      transform: generateRotateStyle(frameRotateX, frameRotateY, frameRotateZ),
    };

    const imageStyle = {
      height: `${100 - top * 2}%`,
      borderRadius: `${innerType === 'circle' ? 50 : 0}%`,
      width: `${100 - left * 2}%`,
      top: `${top}%`,
      left: `${left}%`,
    };

    return (
      <div className={b('art-photo-block')}>
        <div
          className={b('art-wrap')}
        >
          <img
            className={b('display')}
            src={imageUri}
            alt="display"
            style={
              {
                width: `${backgroundScale}%`,
                left: `-${backgroundLeft}%`,
                top: `-${backgroundTop}%`,
              }
            }
          />
          <div className={b('frame-wrap')} style={frameBlockStyle}>
            <div className={b('image-wrap')} style={blockStyle}>
              <img
                className={b('art-photo')}
                style={imageStyle}
                src={croppedImage.imageUri}
                alt="art"
              />
              <div className={b('shadow-block')} style={imageStyle} />
            </div>
            <img
              className={b('frame-image')}
              src={frameImageUrl}
              alt="frame"
            />
          </div>
        </div>
      </div>
    );
  }

  getEnhancePhoto = (images, name, productName) => {
    const originals = images.filter(({ parentId }) => !parentId);
    const isEasyScanKit = !images.some(({ imageUri }) => imageUri);
    if (!originals.length) {
      return (
        <img
          className={b('product-image')}
          src={this.getDefaultImage(productName, isEasyScanKit).src}
          alt={name}
          layout="fill"
        />
      );
    }

    if (originals.length === 1 || isEasyScanKit) {
      const { imageUri, imageKey } = originals[0];
      return (
        <img
          className={b('product-image')}
          src={formattedImage({imagePath: imageKey, needResize: true, imageSize: '150x'}) || imageUri || this.getDefaultImage(productName, isEasyScanKit).src}
          alt={name}
          layout="fill"
        />
      );
    }

    if (originals.length === 2) {
      return (
        <div className={b('image-group')}>
          <div className={b('veil')}>
            <span className={b('veil-text')}>{VIEW}</span>
            <span className={b('veil-number-of-photos')}>{`${originals.length} ${PHOTOS}`}</span>
          </div>
          {originals.map(({ imageUri, imageKey, name: imageName, id }) => (
            <img
              className={b('product-image', { half: true })}
              src={formattedImage({imagePath: imageKey, needResize: true, imageSize: '100x'}) || imageUri || this.getDefaultImage(productName).src}
              alt={imageName}
              key={id}
              layout="fill"
            />
          ))}
        </div>
      );
    }
    if (originals.length === 3) {
      return (
        <div className={b('image-group')}>
          <div className={b('veil')}>
            <span className={b('veil-text')}>{VIEW}</span>
            <span className={b('veil-number-of-photos')}>{`${originals.length} ${PHOTOS}`}</span>
          </div>
          {originals.map(({ imageUri, imageKey, name: imageName, id }, index) => (
            <img
              className={b('product-image', { quarter: index !== 2, 'bottom-half': index === 2 })}
              src={formattedImage({imagePath: imageKey, needResize: true, imageSize: '100x'}) || imageUri || this.getDefaultImage(productName).src}
              alt={imageName}
              key={id}
              layout="fill"
            />
          ))}
        </div>
      );
    }
    if (originals.length >= 4) {
      return (
        <div className={b('image-group')}>
          <div className={b('veil')}>
            <span className={b('veil-text')}>{VIEW}</span>
            <span className={b('veil-number-of-photos')}>{`${originals.length} ${PHOTOS}`}</span>
          </div>
          {originals.map(({ imageUri, imageKey, name: imageName, id }, index) => (
            index < 4 ? (
              <img
                className={b('product-image', { quarter: true })}
                src={formattedImage({imagePath: imageKey, needResize: true, imageSize: '100x'}) || imageUri || this.getDefaultImage(productName).src}
                alt={imageName}
                key={id}
                layout="fill"
              />
            ) : null
          ))}
        </div>
      );
    }
    return (
      <img
        className={b('product-image')}
        src={this.getDefaultImage(productName)}
        alt={name}
        layout="fill"
      />
    );
  }

  getProofData = () => {
    const { projectList, query: { versionImageId } } = this.props;
    const { selectedProject } = this.state;
    const {
      orderItem = {},
      imagesOrig = [],
      imagesVersion = [],
    } = projectList.find(item => item.id === selectedProject) || {};
    const { parentOrderImageId = null } = imagesVersion
      .filter(({ orderImage: img } = {}) => img && img.id)
      .find(image => Number(versionImageId) === image.orderImage.id) || {};
    return {
      orderId: orderItem.orderId || null,
      images: this.getImagesToProof(imagesOrig, imagesVersion),
      imageId: parentOrderImageId,
    };
  }

  openChangeName = (project) => {
    this.setState({ newProjectName: project.name, currentProject: project });
  }

  onChangeName = (e) => {
    this.setState({ newProjectName: e.target.value });
  }

  closeEditPopup = () => {
    this.setState({ newProjectName: '', currentProject: null });
  }

  editName = () => {
    const { newProjectName, currentProject } = this.state;
    const { editProject, token } = this.props;
    if (newProjectName) {
      const {
        productType,
        data,
        imageId,
        croppedImageId,
        orderItemId,
        orderId,
        id,
      } = currentProject;
      editProject({
        id,
        objectData: {
          productType,
          data,
          imageId,
          croppedImageId,
          orderItemId,
          orderId,
          name: newProjectName,
        },
        token,
      });
      this.setState({ newProjectName: '', currentProject: null });
    }
  }

  getStatus = (imagesOrig, imagesVersion, productType, orderId) => {
    if (productType === PRODUCT_NAME_PHOTO_ART) {
      return orderId ? ORDERED : SAVED;
    }

    const imagesByType = this.getImagesToProof(imagesOrig, imagesVersion);

    if (imagesByType.every(({ versions }) => !versions.length)) {
      return PROCESS;
    }

    if (imagesByType.every(({ versions }) => (
      versions.length && versions.some(({ isApproved }) => isApproved)
    ))) {
      return APPROVED_TYPE;
    }

    if (imagesByType.some(({ versions }) => (
      versions.length && versions.every(({ isApproved }) => !isApproved)
    ))) {
      return AWAITING;
    }

    return null;
  }

  filterProjects = () => {
    const {
      filterValue,
      typeFilterValue,
    } = this.state;
    const { projectList } = this.props;

    const filterByStatus = projectList
      ? projectList.filter((item) => {
        const {
          imagesOrig,
          imagesVersion,
          productType,
          orderId,
        } = item;
        const status = this.getStatus(imagesOrig, imagesVersion, productType, orderId);

        return (filterValue.value === 'all')
        || (filterValue.value === 'process' && (
          status === SAVED
          || status === PROCESS
          || status === AWAITING
        ))
        || (filterValue.value === 'completed' && (status === ORDERED || status === APPROVED_TYPE));
      })
      : [];

    return typeFilterValue.value === 'all' ? filterByStatus : filterByStatus.filter(({ productType }) => productType === typeFilterValue.value);
  }

  getFunctionImageBlock = (id, productType) => {
    if (productType === PRODUCT_NAME_ENHANCE) {
      this.showProof(id);
    } else {
      window.location.href = `${photoArtEditor}?project=${id}`;
    }
  }

  render() {
    const {
      isShowCloud,
      startFrame,
      startCollection,
      showSharedPopup,
      selectedData,
      isShowProof,
      sortValue,
      selectedOrderId,
      filterValue,
      newProjectName,
      currentProject,
      typeFilterValue,
      isScaleMode,
    } = this.state;
    const { token, getProjectList } = this.props;

    const proofData = this.getProofData();

    const sortSelectStyles = {
      control: (provided) => ({
        ...provided,
        height: isScaleMode ? '1.1458vw' : '22px',
        minHeight: isScaleMode ? '1.1458vw' : '22px',
      }),
      dropdownIndicator: (provided) => ({
        ...provided,
        paddingRight: '0',
        paddingLeft:'0',
      }),
      singleValue: (provided) => ({
        ...provided,
        fontWeight: '500',
        fontSize: isScaleMode ? '0.8333vw' : '16px',
        lineHeight: isScaleMode ? '1.1458vw' : '22px',
        color: `${Colors['$summer-green-color']}`,
        padding: '0 !important'
      }),
    }

    const typeFilterStyle = {
      control: (provided) => ({
        ...provided,
        padding: isScaleMode ? '0 0.677vw 0 0.677vw' : '0 13px 0 18px',
        height: isScaleMode ? '1.5625vw' : '30px',
        minHeight: isScaleMode ? '1.5625vw' : '30px',
        background: `${Colors['$catskill-white-color']}`,
        border: `1px solid ${Colors['$athens-gray-color']} !important`,
        boxShadow: '37px 37px 80px rgba(#fff, 0.0395269)',
        borderRadius: isScaleMode ? '0.4166vw !important' : '8px !important',
        minWidth: '100%',
        width: '100%',
      }),
      singleValue: (provided) => ({
        ...provided,
        fontWeight: '700',
        fontSize: isScaleMode ? '0.7291vw' : '0.875rem',
        lineHeight:  isScaleMode ? '0.9895vw' : '19px',
        color: `${Colors['$pickled-bluewood-color']}`,
      }),
      dropdownIndicator: (provided) => ({
        ...provided,
        paddingRight: '0',
        paddingLeft:'0',
        ...(isScaleMode ? {
          paddingTop: '0.4166vw',
          paddingBottom: '0.4166vw',
        } : {} ),
      }),
      menu: (provided) => ({
        ...provided,
        marginTop: '5px',
      }),
    }

    return (
      <div className={b()}>
        {!isShowCloud && (
          <>
            {showSharedPopup && (
              <SharePopup
                data={{ images: selectedData.map(({ id, fileId }) => fileId || id) }}
                close={this.closeSharedPopup}
                orderId={selectedOrderId}
              />
            )}
            {!isShowProof && (
              <div className={b('projects-wrap')}>
                <div className={b('sort-block')}>
                  <div className={b('type-filter-block')}>
                    <span className={b('sort-title')}>{TYPE}</span>
                    <CustomSelect
                      className={b('type-filter-select')}
                      selectClass={b('type-filter-select-selector')}
                      options={FILTER_TYPE_ARR}
                      value={typeFilterValue}
                      onChange={this.changeTypeFilter}
                      customStyles={typeFilterStyle}
                    />
                  </div>
                  <div className={b('sort')}>
                    <span className={b('sort-title')}>{FILTER}</span>
                    <CustomSelect
                      className={b('filter-select')}
                      value={filterValue}
                      options={FILTER_ARR}
                      onChange={this.changeFilter}
                      customStyles={{
                        ...sortSelectStyles,
                        control: (provided) => ({
                          ...provided,
                          width: isScaleMode ? '7.031vw' : '135px',
                          minWidth: isScaleMode ? '6.771vw' : '130px',
                          padding: '0',
                          height: '22px',
                          minHeight: '22px',
                          backgroundColor: 'transparent',
                          border: 'none !important',
                          borderRadius: '0',
                          margin: '0 0 3px 10px',
                          boxShadow: 'none !important',
                          ...(isScaleMode ? {
                            fontSize: '0.8333vw',
                            lineHeight: '1.1458vw',
                            marginLeft: '0.4166vw',
                          } : {} ),
                        }),
                        container: (provided) => ({
                          ...provided,
                          padding: '0',
                        }),
                      }}
                    />
                  </div>
                  <div className={b('sort')}>
                    <span className={b('sort-title')}>{SORT}</span>
                    <CustomSelect
                      className={b('sort-select')}
                      selectClass={b('sort-select-input')}
                      value={sortValue}
                      options={SORT_ARR}
                      onChange={this.changeSort}
                      customStyles={{
                        ...sortSelectStyles,
                        control: (provided) => ({
                          ...provided,
                          border: 'none !important',
                          borderRadius: '0',
                          boxShadow: 'none !important',
                          width: isScaleMode ? '7.031vw' : '160px',
                          minWidth: isScaleMode ? '7.813vw' : '150px',
                          height: '22px',
                          minHeight: '22px',

                        }),
                      }}
                    />
                  </div>
                </div>
                <div className={b('project-list')}>
                  {this.filterProjects().map((item) => {
                    const {
                      addedAt,
                      updatedAt,
                      name,
                      productType,
                      imagesOrig,
                      imagesVersion,
                      orderItem,
                      id,
                      data,
                      orderId,
                    } = item;

                    const images = [
                      ...imagesOrig,
                      ...(imagesVersion || []).reduce((acc, itm) => {
                        const { orderImage: image } = itm;
                        if (image && image.imageUri) {
                          return [...acc, image];
                        }
                        return acc;
                      }, []),
                    ];

                    const {
                      displayName,
                      productName,
                    } = this.getProductInfo(orderItem ? orderItem.productId : 0);
                    const {
                      frameName,
                    } = this.getDataInfo(data);
                    const status = this.getStatus(imagesOrig, imagesVersion, productType, orderId);
                    return (
                      <div className={b('project-item')}>
                        <div
                          className={b('image-block')}
                          onClick={() => this.getFunctionImageBlock(id, productType)}
                          role="button"
                          tabIndex={0}
                        >
                          {productType === PRODUCT_NAME_ENHANCE
                            ? this.getEnhancePhoto(images, name, productName)
                            : this.getPhotoArtPhoto(id)}
                          {status && (
                            <span className={b('image-text')}>
                              {status}
                            </span>
                          )}
                        </div>
                        <div className={b('info-block')}>
                          <span className={b('product-name')}>{productType === PRODUCT_NAME_ENHANCE ? ENHANCE : PHOTO_ART}</span>
                          <div className={b('title-wrap')}>
                            <span className={b('project-name')}>{productType === PRODUCT_NAME_PHOTO_ART ? name : `Order #${orderId}`}</span>
                            {productType === PRODUCT_NAME_PHOTO_ART && (
                              <button className={b('change-name-btn')} type="button" onClick={() => this.openChangeName(item)}>
                                <EditNameIcon className={b('edit-Name-icon')} />
                              </button>
                            )}
                          </div>
                          {productType === PRODUCT_NAME_PHOTO_ART && orderId && (
                            <span className={b('product-quantity')}>
                              {`Order #${orderId}`}
                            </span>
                          )}
                          <span className={b('product-quantity')}>
                            {productType === PRODUCT_NAME_ENHANCE
                              ? `${displayName} x${orderItem ? orderItem.quantity : 0}`
                              : frameName}
                          </span>
                          {productType === PRODUCT_NAME_PHOTO_ART && updatedAt && (
                            <div className={b('product-date-block')}>
                              <span className={b('added-title')}>{LAST_EDIT}</span>
                              <span className={b('added-date')}>{format(new Date(updatedAt),'MM.dd.yyyy')}</span>
                            </div>
                          )}
                          <div
                            className={b(
                              'product-date-block',
                              { 'small-margin': productType === PRODUCT_NAME_PHOTO_ART && updatedAt },
                            )}
                          >
                            <span className={b('added-title')}>{CREATION_DATE}</span>
                            <span className={b('added-date')}>{format(new Date(addedAt),'MM.dd.yyyy')}</span>
                          </div>
                        </div>
                        <div className={b('control-block')}>
                          {productType === PRODUCT_NAME_ENHANCE && (
                            <>
                              <button
                                className={b('control-btn')}
                                type="button"
                                onClick={() => {
                                  this.showProof(id);
                                  getProjectList({ token, order: sortValue.value });
                                }}
                              >
                                { status === PROCESS || status === AWAITING
                                  ? (
                                    <ArrowInCircle
                                      className={b('control-btn-icon')}
                                      stroke={blueColor}
                                    />
                                  ) : (
                                    <CheckFull
                                      className={b('control-btn-icon')}
                                      stroke={redColor}
                                      fill="none"
                                      strokeWidth="2"
                                    />
                                  )}
                                {this.checkAllApprove(images) || status === PROCESS
                                || status === AWAITING ? VIEW : APPROVE}
                              </button>
                              {Boolean(this.getApprovedImages(images).length) && (
                                <>
                                  <button
                                    className={b('control-btn')}
                                    type="button"
                                    onClick={() => {
                                      this.downloadItemImages(
                                        images,
                                        orderItem ? orderItem.orderId : 0,
                                      );
                                    }}
                                  >
                                    <MinDownloadIcon
                                      className={b('control-btn-icon')}
                                      stroke={greenColor}
                                      stroke2={greenColor}
                                      strokeWidth="3"
                                    />
                                    {DOWNLOAD}
                                  </button>
                                  <button
                                    className={b('control-btn')}
                                    type="button"
                                    onClick={() => {
                                      this.goToArt(this.getApprovedImages(images));
                                    }}
                                  >
                                    <MinPhotoArtIcon
                                      className={b('control-btn-icon')}
                                      stroke={greenColor}
                                      stroke2={greenColor}
                                      strokeWidth="3"
                                    />
                                    {ORDER_ART}
                                  </button>
                                  <button
                                    className={b('control-btn')}
                                    type="button"
                                    onClick={
                                      () => {
                                        this.openSharedPopup(
                                          this.getApprovedImages(images),
                                          orderId,
                                        );
                                      }
                                    }
                                  >
                                    <MinShareIcon
                                      className={b('control-btn-icon')}
                                      stroke={greenColor}
                                      stroke2={greenColor}
                                      strokeWidth="3"
                                    />
                                    {SHARE}
                                  </button>
                                </>
                              )}
                            </>
                          )}
                          {productType === PRODUCT_NAME_PHOTO_ART && (
                            <>
                              <a
                                className={b('control-btn')}
                                href={`${photoArtEditor}?project=${id}`}
                              >
                                <ArrowInCircle
                                  className={b('control-btn-icon')}
                                  stroke={blueColor}
                                />
                                {EDIT}
                              </a>
                              <button
                                className={b('control-btn')}
                                type="button"
                                onClick={() => { this.addToCart(id); }}
                              >
                                <CartIcon2
                                  className={b('control-btn-icon')}
                                  stroke={greenColor}
                                  stroke2={greenColor}
                                  strokeWidth="2"
                                />
                                {orderId ? REORDER : ORDER}
                              </button>
                              <button
                                className={b('control-btn')}
                                type="button"
                                onClick={() => { this.delete(id); }}
                              >
                                <TrashIcon2
                                  className={b('control-btn-icon')}
                                  stroke={greenColor}
                                  stroke2={greenColor}
                                  strokeWidth="3"
                                />
                                {DELETE}
                              </button>
                            </>
                          )}
                        </div>
                      </div>
                    );
                  })}
                </div>
              </div>
            )}
            {isShowProof && (
              <Proofs
                {...this.props}
                isComponentShow={isShowProof}
                isShowCloudChange={this.isShowCloudChange}
                orderId={proofData.orderId}
                images={proofData.images}
                goBack={this.closeProof}
                updateProjectList={this.updateProjectList}
                imageId={proofData.imageId}
                goToArt={this.goToArt}
              />
            )}
          </>
        )}
        {currentProject
          && ReactDOM.createPortal(
            (
              <div className={b('popup-wrap')}>
                <div className={b('popup-block')}>
                  <button className={b('close-btn')} type="button" onClick={this.closeEditPopup}>
                    <CloseIcon2 />
                  </button>
                  <span className={b('popup-title')}>{EDIT_PROJECT}</span>
                  <div className={b('project-edit')}>
                    <input
                      className={b('project-create-input')}
                      type="text"
                      placeholder={UNTITLED_PROJECT}
                      onChange={this.onChangeName}
                      value={newProjectName}
                    />
                    <button
                      className={b('project-edit-btn')}
                      type="button"
                      onClick={this.editName}
                    >
                      {RENAME}
                    </button>
                  </div>
                </div>
              </div>
            ),
            document.getElementById('__next') || null,
          )}
        {isShowCloud && (
          <MyCloudPhotoArt
            isComponentShow={isShowCloud}
            isShowCloudChange={this.isShowCloudChange}
            backButtonText={BACK_BUTTON_TEXT}
            preselectFrame={startFrame}
            preselectCollection={startCollection}
            clearPreselect={this.clearPreselect}
            isProjectTab
          />
        )}
      </div>
    );
  }
}

ProjectsTab.propTypes = propTypes;
ProjectsTab.defaultProps = defaultProps;

const stateProps = state => ({
  projectList: projectListSelector(state),
  productList: orderPriceSelect(state),
  token: getToken(state),
  isLoading: isEditProjectNameSelector(state),
  isDeleting: isDeletingProjectSelector(state),
  photoArtProducts: photoArtProductsSelector(state),
});


const actions = {
  downloadImages: downloadOrderImages,
  addFilesToCloud: addAllFilesToCloud,
  getProjectList: getProjectListAction,
  deleteProject: deleteProjectAction,
  addOrder: addToCart,
  addImages: addCroppedImagesToCart,
  editProject: editProjectDataAction,
  getPromo: getPromoSection,
  addImageList: addImageListAction,
};

export default connect(stateProps, actions)(ProjectsTab);
