const BACK_BUTTON_TEXT = 'Back to my projects';
const ENHANCE = 'Enhance';
const PHOTO_ART = 'Photo Art';
const CREATION_DATE = 'Creation date:';
const ORDER = 'Order';
const REORDER = 'Re-order';
const SHARE = 'Share';
const DELETE = 'Delete';
const DOWNLOAD = 'Download';
const ORDER_ART = 'Order art';
const APPROVE = 'Approve';
const SORT = 'Sort:';
const TYPE = 'Type:';
const FILTER = 'Filter:';
const PRODUCT_NAME_PHOTO_ART = 'photo_art';
const PRODUCT_NAME_ENHANCE = 'enhance';
const CONTINUE = 'Continue';
const EDIT = 'Edit';
const LAST_EDIT = 'Last edited:';
const SORT_ARR = [
  {
    label: 'From new to old',
    value: 'desc',
  },
  {
    label: 'From old to new',
    value: 'asc',
  },
];
const VIEW = 'View';
const PHOTOS = 'photos';
const AWAITING = 'Awaiting';
const PROCESS = 'In process';
const ALL = 'All';
const APPROVED_TYPE = 'Approved';
const SAVED = 'Saved';
const ORDERED = 'Ordered';

const FILTER_ARR = [
  {
    label: 'All',
    value: 'all',
  },
  {
    label: 'In process',
    value: 'process',
  },
  {
    label: 'Completed',
    value: 'completed',
  },
];
const EDIT_PROJECT = 'Edit Project';
const UNTITLED_PROJECT = 'Untitled project';
const RENAME = 'Rename';

const FILTER_TYPE_ARR = [
  {
    label: 'All',
    value: 'all',
  },
  {
    label: 'Enhance',
    value: PRODUCT_NAME_ENHANCE,
  },
  {
    label: 'Photo art',
    value: PRODUCT_NAME_PHOTO_ART,
  },
];

export {
  BACK_BUTTON_TEXT,
  ENHANCE,
  PHOTO_ART,
  CREATION_DATE,
  ORDER,
  SHARE,
  DELETE,
  DOWNLOAD,
  ORDER_ART,
  APPROVE,
  SORT,
  TYPE,
  FILTER,
  PRODUCT_NAME_PHOTO_ART,
  PRODUCT_NAME_ENHANCE,
  CONTINUE,
  LAST_EDIT,
  SORT_ARR,
  EDIT,
  VIEW,
  PHOTOS,
  AWAITING,
  PROCESS,
  APPROVED_TYPE,
  ALL,
  SAVED,
  ORDERED,
  REORDER,
  FILTER_ARR,
  EDIT_PROJECT,
  UNTITLED_PROJECT,
  RENAME,
  FILTER_TYPE_ARR,
};
