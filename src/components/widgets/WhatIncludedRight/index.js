import React from 'react';
import bem from 'src/utils/bem';
import useMobileStyle from 'src/utils/useMobileStyle';
import PropTypes from 'prop-types';
import { closingQuote, openingQuote } from 'public';
import { connect } from 'react-redux';
import LazyLoad from 'react-lazyload';
import dynamic from 'next/dynamic';
import { isMobileDeviceSelector } from 'src/redux/auth/selectors';
import TERMS from './constants';
import styles from './index.module.scss';

const NewInfoWidgetCard = dynamic(() => import('src/components/cards/NewInfoWidgetCard'));
const ColorButton = dynamic(() => import('src/components/buttons/ColorButton'));

const b = bem('what-included-right', styles);

const { TITLE, DESCRIPTION } = TERMS;

const propTypes = {
  title: PropTypes.string,
  description: PropTypes.string,
  isRight: PropTypes.bool,
  isButton: PropTypes.bool,
  cardImage: PropTypes.shape({}),
  cardImageMob: PropTypes.shape({}),
  cardBackgroundStripeColor: PropTypes.string,
  cardBackgroundSecondStripeColor: PropTypes.string,
  clickHandler: PropTypes.func,
  buttonText: PropTypes.string,
  buttonLink: PropTypes.string,
  isLeftStripe: PropTypes.bool,
  leftStripeColor: PropTypes.string,
  isRightStripe: PropTypes.bool,
  rightStripeColor: PropTypes.string,
  leftStripeStyle: PropTypes.string,
  withoutLeftSquare: PropTypes.bool,
  rightStripeStyle: PropTypes.string,
  leftBigSquareStyle: PropTypes.shape({}),
  leftSmallSquareStyle: PropTypes.shape({}),
  withQuotes: PropTypes.bool,
  reference: PropTypes.string,
  mission: PropTypes.bool,
  isMobileDevice: PropTypes.bool,
  id: PropTypes.string,
};

const defaultProps = {
  title: TITLE,
  description: DESCRIPTION,
  isRight: false,
  isButton: false,
  cardBackgroundStripeColor: '',
  cardBackgroundSecondStripeColor: '',
  clickHandler: () => {},
  buttonText: '',
  buttonLink: '',
  cardImage: {},
  cardImageMob: {},
  isLeftStripe: false,
  leftStripeColor: '',
  isRightStripe: false,
  rightStripeColor: '',
  leftStripeStyle: '',
  withoutLeftSquare: false,
  rightStripeStyle: '',
  leftBigSquareStyle: {},
  leftSmallSquareStyle: {},
  withQuotes: false,
  reference: '',
  mission: false,
  isMobileDevice: true,
  id: '',
};

const WhatIncludedRight = (props) => {
  const {
    title,
    description,
    cardImage,
    isButton,
    clickHandler,
    buttonText,
    buttonLink,
    isRight,
    cardBackgroundStripeColor,
    cardBackgroundSecondStripeColor,
    isLeftStripe,
    leftStripeColor,
    isRightStripe,
    rightStripeColor,
    leftStripeStyle,
    withoutLeftSquare,
    rightStripeStyle,
    leftBigSquareStyle,
    leftSmallSquareStyle,
    withQuotes,
    reference,
    mission,
    cardImageMob,
    isMobileDevice,
    id,
  } = props;

  const isMobile = useMobileStyle(1024, isMobileDevice);
  return (
    <div className={b({ 'mission-page': mission, 'is-right': isRight })} id={id}>
      {!withoutLeftSquare && <div className={b('left-square', { 'is-right': isRight })} />}
      <div className={b('wrapper', { 'is-right': isRight, 'with-quotes': withQuotes })}>
        {isLeftStripe && (
          <>
            <div
              className={b('left-stripe')}
              style={{
                backgroundColor: leftStripeColor,
                ...leftStripeStyle,
              }}
            />
            <div className={b('left-square-big')} style={leftBigSquareStyle} />
            <div className={b('left-square-small')} style={leftSmallSquareStyle} />
          </>
        )}
        {isRightStripe && (
          <>
            <div
              className={b('right-stripe')}
              style={{
                backgroundColor: rightStripeColor,
                ...rightStripeStyle,
              }}
            />
            <div className={b('right-square-small')} />
            <div className={b('right-square-big')} />
          </>
        )}
        <div className={b('main-text-card')}>
          <LazyLoad offset={100}>
            <NewInfoWidgetCard
              cardBackgroundStripeColor={cardBackgroundStripeColor}
              cardBackgroundSecondStripeColor={cardBackgroundSecondStripeColor}
              cardImage={isMobile ? cardImageMob : cardImage}
              isRight={isRight}
              title={title}
              description={description}
              isButton={isButton}
              buttonText={buttonText}
              clickHandler={clickHandler}
              mission={mission}
            />
          </LazyLoad>
        </div>
        <div
          className={b('text-container', {
            'is-right': isRight,
            'block-with-quotes': withQuotes,
            'right-on-mission-page': mission,
          })}
        >
          {withQuotes && <img className={b('top-quotes')} src={closingQuote.src} alt="Quote" />}
          <h2 className={b('text-title')}>{title}</h2>
          <div
            className={b('text-card', {
              'is-right': isRight,
              'block-with-quotes': withQuotes,
              'right-on-mission-page': mission,
            })}
          >
            <LazyLoad offset={100}>
              <NewInfoWidgetCard
                cardBackgroundStripeColor={cardBackgroundStripeColor}
                cardBackgroundSecondStripeColor={cardBackgroundSecondStripeColor}
                cardImage={cardImage}
                isRight={isRight}
                title={title}
                description={description}
                isButton={isButton}
                buttonText={buttonText}
                clickHandler={clickHandler}
                mission={mission}
              />
            </LazyLoad>
          </div>
          <div className={b('text-description', { 'block-with-quotes': withQuotes })}>
            {description}
          </div>
          {withQuotes && <span className={b('reference')}>{reference}</span>}
          {withQuotes && <img className={b('bottom-quotes')} src={openingQuote.src || openingQuote} alt="Quote" />}
          {isButton && (
            <a className={b('btn')} href={buttonLink}>
              <ColorButton className={b('button')} text={buttonText} onClick={clickHandler} />
            </a>
          )}
        </div>
      </div>
    </div>
  );
}

WhatIncludedRight.propTypes = propTypes;
WhatIncludedRight.defaultProps = defaultProps;

const stateProps = (state) => ({
  isMobileDevice: isMobileDeviceSelector(state),
});

export default connect(stateProps, null)(WhatIncludedRight);
