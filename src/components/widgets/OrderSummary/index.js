import React, { Component } from 'react';
import bem from 'src/utils/bem';
import formattedPrice from 'src/utils/formattedPrice';
import orderSummaryInfo from 'src/utils/orderSummaryInfo';

import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { DIGITIZE_PRODUCTS, ENHANCE_PRODUCTS } from 'src/constants';
import DICTIONARY from 'src/constants/dictionary';
import ROUTES from 'src/constants/routes';
import colors from 'src/styles/colors.json';

import spinner from 'public/img/shared/spinner*******.gif';
import dynamic from 'next/dynamic';
import ordersTypeSelect from 'src/redux/orders/selectors';
import {
  orderPriceSelect,
  priceSelect,
  shippingProductsSelector,
} from 'src/redux/prices/selectors';
import { getToken, userIdSelector } from 'src/redux/auth/selectors';
import { couponError, couponSelect } from 'src/redux/coupon/selectors';
import { orderIsUpdateSelector } from 'src/redux/shared/selectors';
import {
  calculateOrderInfoSelector,
  calculateSpinnerFlagSelector,
} from 'src/redux/orderCreate/selectors';
import { createOrder } from 'src/redux/orderCreate/actions';
import { addProductsToOrder } from 'src/redux/orders/actions';
import { checkCoupon, clearCoupon } from 'src/redux/coupon/actions';
import styles from './index.module.scss';
import ORDER_SUMMARY_TERMS from './constants';
import SubtotalInfo from './subtotalInfo';
import ShippingText from './shippingText';

const CloseIcon2 = dynamic(() => import('src/components/svg/CloseIcon2'));
const NewCustomInput = dynamic(() => import('src/components/inputs/NewCustomInput'));
const ColorButton = dynamic(() => import('src/components/buttons/ColorButton'));
const InfoIcon = dynamic(() => import('src/components/svg/InfoIcon'));
const WarningTriangleIcon = dynamic(() => import('src/components/svg/WarningTriangleIcon'));


const b = bem('order-summary', styles);
const greenBtn = colors['$summer-green-color'];
const orangeColor = colors['$burnt-sienna-color'];

const { payment, loginPage } = ROUTES;
const {
  EXPRESS_DELIVERY_PRICE,
  TAX,
  COUPON_ACTIVE,
  COUPON_FAIL,
  COUPON_CODE,
  SHIPPING_INFO_TEXT,
  UNSUITABLE_PRODUCTS,
  DISCOUNT_CODE,
  CHECKOUT,
  SHIPPING_TITLE,
  TOTAL,
  PLEASE_ENTER_ZIP_CODE,
  ENTER_CODE,
} = ORDER_SUMMARY_TERMS;

const { APPLY } = DICTIONARY;

const defaultProps = {
  orders: {},
  currency: '$',
  selectPrice: {},
  orderRushStatus: false,
  token: '',
  couponCheck: () => {},
  getCoupon: {},
  getCouponError: {},
  isCouponForm: true,
  zipCodeData: {},
  addProducts: () => {},
  orderIsUpdate: false,
  isCheckoutPage: false,
  state: {},
  userId: null,
  clearPromo: () => {},
  isValidOrders: false,
  shippingProducts: [],
  calculateOrderInfo: {},
  getOrderPaymentInfo: () => {},
  calculateSpinnerFlag: false,
  zipCode: '',
  setEditStatus: () => {},
};

const propTypes = {
  orders: PropTypes.shape({
    gift_card: PropTypes.arrayOf(PropTypes.shape({})),
    photo_art: PropTypes.arrayOf(PropTypes.shape({})),
  }),
  currency: PropTypes.string,
  orderRushStatus: PropTypes.bool,
  token: PropTypes.string,
  couponCheck: PropTypes.func,
  getCoupon: PropTypes.shape({
    isActive: PropTypes.bool,
    value: PropTypes.number,
    id: PropTypes.number,
    name: PropTypes.string,
    valueType: PropTypes.string,
    targetType: PropTypes.string,
    entitledProductIds: PropTypes.arrayOf(PropTypes.shape({})),
    isGiftCard: PropTypes.bool,
    isOrderIncludeAllProducts: PropTypes.bool,
  }),
  getCouponError: PropTypes.shape({
    message: PropTypes.string,
    statusCode: PropTypes.number,
  }),
  isCouponForm: PropTypes.bool,
  zipCodeData: PropTypes.shape({ localPickUp: PropTypes.bool }),
  selectPrice: PropTypes.shape({
    local_pickup: PropTypes.shape({}),
    easy_home_ship_kit: PropTypes.shape({}),
    easy_scan_ship_kit: PropTypes.shape({}),
    return_shipping: PropTypes.shape({}),
    free_shipping: PropTypes.shape({}),
    in_store: PropTypes.shape({}),
    store_drop_of: PropTypes.shape({}),
    digital_download: PropTypes.shape({}),
    trial_ship_kit: PropTypes.shape({}),
  }),
  addProducts: PropTypes.func,
  orderIsUpdate: PropTypes.bool,
  isCheckoutPage: PropTypes.bool,
  state: PropTypes.shape({}),
  userId: PropTypes.number,
  clearPromo: PropTypes.func,
  isValidOrders: PropTypes.bool,
  shippingProducts: PropTypes.arrayOf(PropTypes.shape({})),
  calculateOrderInfo: PropTypes.shape({
    toPay: PropTypes.oneOf([PropTypes.string, PropTypes.number]),
    delivery: PropTypes.oneOf([PropTypes.string, PropTypes.number]),
  }),
  getOrderPaymentInfo: PropTypes.func,
  calculateSpinnerFlag: PropTypes.bool,
  zipCode: PropTypes.string,
  setEditStatus: PropTypes.func,
};


class OrderSummary extends Component {
  constructor(props) {
    super(props);

    this.state = {
      couponName: '',
      viewCouponStatus: false,
      isBadPromo: false,
      isAddedSipping: false,
      availableShippings: [],
    };
  }

  componentDidMount() {
    this.checkCopyPromoCode();
  }

  componentDidUpdate(prevProps) {
    const { orderIsUpdate, zipCodeData: { zip } = {}, zipCode, shippingProducts, orders } = this.props;
    const { zipCodeData: { zip: oldZip } = {}, zipCode: oldZipCode } = prevProps;
    const newOrders = Object.values(orders);

    const isDelivery = shippingProducts.some(shipping => {
      return newOrders.some(products => {
        return products.some(product => product.productId === shipping.id)
      })
    });

    if ((orderIsUpdate || zip !== oldZip || zipCode !== oldZipCode)) {
      this.checkShipping();
    }
    else if (zipCode.length < 5 && isDelivery) {
      this.handleShippingMethod('');
    }
  }

  checkCopyPromoCode = () => {
    const { isCheckoutPage } = this.props;
    const promoCode = JSON.parse(sessionStorage.getItem('promoCode') || '{}');
    if (!isCheckoutPage && promoCode && promoCode.name) {
      const { name } = promoCode;
      sessionStorage.removeItem('promoCode');
      this.setState({ couponName: name }, () => { this.checkCoupon(); });
    }
  }

  checkDigitizeShipping = () => {
    const { orders } = this.props;
    const isNeedHomeSipKit = DIGITIZE_PRODUCTS.reduce((acc, item) => {
      if (acc) return acc;
      return Boolean(orders[item].length && orders[item].some(({ isTrial }) => !isTrial));
    }, false);
    return isNeedHomeSipKit;
  }

  checkDigitizeTrialShipping = () => {
    const { orders } = this.props;
    const isNeedTrialSipKit = DIGITIZE_PRODUCTS.reduce((acc, item) => {
      if (acc) return acc;
      return Boolean(orders[item].length && orders[item].some(({ isTrial }) => isTrial));
    }, false);
    return isNeedTrialSipKit;
  }

  checkEnhanceShipping = () => {
    const { orders } = this.props;
    const isNeedScanSipKit = ENHANCE_PRODUCTS.reduce((acc, item) => {
      if (acc) return acc;
      return Boolean(orders[item].length);
    }, false);
    return isNeedScanSipKit;
  }

  checkEnhancePrintShipping = () => {
    const { orders } = this.props;
    const isNeedReturnSipKit = ENHANCE_PRODUCTS.reduce((acc, item) => {
      if (acc) return acc;
      return orders[item].length && orders[item]
        .some((itm) => {
          if (!itm) {
            return false;
          }
          const { options = [] } = itm || {};
          return Boolean(options && options.length) && options.some(({ name: optionsName }) => optionsName === 'enhance_prints');
        }
      );
    }, false);
    return isNeedReturnSipKit;
  }

  checkEnhanceScanShipping = () => {
    const { orders } = this.props;
    const isNeedReturnSipKit = ENHANCE_PRODUCTS.reduce((acc, item) => {
      if (acc) return acc;
      return (
        orders[item].length &&
        orders[item].some(
          (itm) => {
            if (!itm) {
              return false
            }
            const { options = [] } = itm || {};
            return Boolean(options && options.length) && options.some(({ name: optionsName }) => optionsName === 'easy_scan_ship_kit');
          }
        )
      );
    }, false);
    return isNeedReturnSipKit;
  }

  generateOrder = (typeProduct) => {
    const { selectPrice } = this.props;
    return {
      quantity: 1,
      typeProduct,
      productId: selectPrice[typeProduct].id,
      price: selectPrice[typeProduct].price,
      isGift: false,
      id: 1,
    };
  }

  checkShipping = () => {
    const {
      orders,
      addProducts,
      shippingProducts,
      zipCodeData,
      getOrderPaymentInfo,
    } = this.props;
    const {
      gift_card: giftCard,
      photo_art: photoArt,
    } = orders;
    const isZipCode = Boolean(Object.keys(zipCodeData).length);
    const isLocalPickUp = Boolean(isZipCode && zipCodeData.localPickUp);

    const activeShippings = shippingProducts
      .filter(({ ext, name }) => ext.shippingSetting.displayAtCheckout && name !== 'photo_art_ship_kit');

    const isPrintShipping = this.checkEnhancePrintShipping();
    const isScanShipping = this.checkEnhanceScanShipping();

    const isTrialShipping = this.checkDigitizeTrialShipping();
    const isActiveEnhance = this.checkEnhanceShipping();
    const isActiveDigitize = this.checkDigitizeShipping();
    const isActiveGiftCard = Boolean(giftCard && giftCard.length);
    const isActiveCreate = Boolean(photoArt && photoArt.length);

    const availableShippings = activeShippings.reduce((acc, item) => {
      const {
        ext: {
          shippingSetting: {
            activeCreate,
            activeDigitize,
            activeEnhance,
            activeGiftCard,
          },
        } = {},
        name,
      } = item;

      if (isActiveDigitize && name !== 'trial_ship_kit') {
        return (isActiveDigitize && activeDigitize)
        ? [...acc, item] : acc;
      }

      if (isTrialShipping && name === 'trial_ship_kit') {
        return (isTrialShipping && activeDigitize)
        ? [...acc, item] : acc;
      }

      if (isActiveEnhance && name !== 'trial_ship_kit') {
        if (isScanShipping && name === 'easy_scan_ship_kit') {
          return (isActiveEnhance && activeEnhance) ? [...acc, item] : acc;
        }

        if (isPrintShipping && !isScanShipping && name === 'return_shipping') {
          return (isActiveEnhance && activeEnhance) ? [...acc, item] : acc;
        }

        if (!isPrintShipping && !isScanShipping && name === 'digital_download') {
          return (isActiveEnhance && activeEnhance) ? [...acc, item] : acc;
        }
      }

      if (isActiveCreate && name !== 'trial_ship_kit') {
        return (isActiveCreate && activeCreate)
        ? [...acc, item] : acc;
      }

      if (isActiveGiftCard && name !== 'trial_ship_kit') {
        return (isActiveGiftCard && activeGiftCard)
        ? [...acc, item] : acc;
      }
      return acc;
    }, []).filter(({ ext: { shippingSetting: { localPickUp, localZipCodes } = {} } }) => {
      const isLocalZip = !localZipCodes || localZipCodes === isZipCode;
      const isPickUp = !localPickUp || localPickUp === isLocalPickUp;

      return isLocalZip && isPickUp;
    });

    const [{ name: firstShippingName } = {}] = availableShippings;
    addProducts({
      ...(availableShippings.reduce((acc, { name }) => (
        {
          ...acc,
          [name]: [],
        }
      ), {})),
      ...(firstShippingName
        ? { [firstShippingName]: [this.generateOrder(firstShippingName)] }
        : {}
      ),
    });
    getOrderPaymentInfo();
    this.setState({
      isAddedSipping: true,
      availableShippings,
    });
  }

  checkCoupon = () => {
    const { token, couponCheck, userId } = this.props;
    const { couponName } = this.state;
    const coupon = { token, couponName, userId };
    localStorage.setItem('editOrder', JSON.stringify(true));
    couponCheck(coupon);
    this.setState({
      viewCouponStatus: true,
      isBadPromo: false,
    });
  }

  closeCouponStatus = () => {
    const { clearPromo } = this.props;
    clearPromo();
    this.setState({ viewCouponStatus: false, isBadPromo: false, couponName: '' });
  }

  clearCoupon = () => {
    const { clearPromo } = this.props;
    const { isAddedSipping } = this.state;
    if (isAddedSipping) {
      localStorage.setItem('editOrder', JSON.stringify(true));
      clearPromo();
      this.setState({ viewCouponStatus: true, isBadPromo: true });
    }
  }

  handleChange = (e) => {
    const { value } = e.target;
    this.setState({
      couponName: value,
    });
  }

  handleShippingMethod = (shippingType) => {
    const {
      addProducts,
      shippingProducts,
      getOrderPaymentInfo,
      setEditStatus,
    } = this.props;
    const newShippingProducts = shippingProducts.reduce((acc, { name }) => (
      {
        ...acc,
        [name]: shippingType === name ? [this.generateOrder(name)] : [],
      }
    ), {});
    localStorage.setItem('editOrder', JSON.stringify(true));
    addProducts(newShippingProducts);
    setEditStatus()
    getOrderPaymentInfo();
  }

  checkShowShippingBlock = (shippingSumInfo) => {
    const {
      scanShipKitSum,
      homeShipKitSum,
      returnShipKitSum,
      freeShippingSum,
      isShowLocalPickUp,
    } = shippingSumInfo;

    return (
      Boolean(scanShipKitSum)
      || Boolean(returnShipKitSum)
      || Boolean(freeShippingSum)
      || Boolean(homeShipKitSum)
    ) && !(
      !scanShipKitSum
      && !returnShipKitSum
      && !freeShippingSum
      && homeShipKitSum
      && isShowLocalPickUp
    );
  }

  paymentRedirect = () => {
    const { token } = this.props;
    document.location.href = token ? payment : `${loginPage}?next=payment`;
  }

  render() {
    const {
      orders,
      currency,
      selectPrice,
      orderRushStatus,
      getCoupon,
      getCouponError,
      isCouponForm,
      isCheckoutPage,
      state,
      isValidOrders,
      calculateOrderInfo,
      calculateSpinnerFlag,
      zipCode,
    } = this.props;
    const { value: stateValue } = state || {};
    const {
      couponName,
      viewCouponStatus,
      isBadPromo,
      availableShippings,
    } = this.state;

    const {
      value: couponValue,
      name: approveCouponName,
      valueType,
      targetType,
      entitledProductIds,
      isGiftCard,
      isOrderIncludeAllProducts,
    } = getCoupon || {};
    const {
      local_pickup: localPickup,
      easy_home_ship_kit: easyHomeShipKit,
      easy_scan_ship_kit: easyScanShipKit,
      return_shipping: returnShipping,
      free_shipping: freeShipping,
      in_store: inStore,
      store_drop_of: storeDropOf,
      digital_download: digitalDownload,
      trial_ship_kit: trialShipKit,
    } = selectPrice;
    const isFlorida = stateValue === 'FL' || stateValue === 'Florida';
    const {
      totalInfo,
    } = orderSummaryInfo({
      orders,
      formattedPrice,
      currency,
      selectPrice,
      orderRushStatus,
      TAX,
      EXPRESS_DELIVERY_PRICE,
      isActive: Boolean(couponValue),
      couponValue,
      approveCouponName,
      isNeedTax: isFlorida && !isCheckoutPage,
      withOutShipping: isCheckoutPage,
      valueType,
      targetType,
      entitledProductIds,
      isGiftCard,
      isOrderIncludeAllProducts,
      clearCoupon: this.clearCoupon,
    });

    const {
      toPay,
      delivery,
    } = calculateOrderInfo;

    const couponSuccess = (getCoupon && Boolean(couponValue) && !isBadPromo);
    return (
      <main className={b()}>
        {isCheckoutPage && (
          <>
            <span className={b('shipping-method-title')}>{SHIPPING_TITLE}</span>
            <div className={b('info-block')}>
              <div className={b('info-icon')}>
                <InfoIcon />
              </div>
              <span className={b('shipping-info-text')}>{SHIPPING_INFO_TEXT}</span>
            </div>
          </>
        )}
        {Boolean(availableShippings && availableShippings.length && !isCheckoutPage) && (
          <div className={b('shipping-method')}>
            {zipCode && zipCode.length >= 5 ? (
              availableShippings.map(({ name, displayName, hint, price, id: shippingId }) => (
                <ShippingText
                  handleShippingMethod={() => {
                    this.handleShippingMethod(name);
                  }}
                  isShippingMethod={Boolean(orders[name] && orders[name].length)}
                  bem={b}
                  description={displayName}
                  tinyDescription={hint}
                  price={price}
                  key={shippingId}
                />
              ))
            ) : (
              <div className={b('no-zipcode')}>
                <WarningTriangleIcon className={b('no-zipcode-icon')} />
                <div className={b('no-zipcode-text')}>{PLEASE_ENTER_ZIP_CODE}</div>
              </div>
            )}
          </div>
        )}
        <div className={b('sub-total-wrapper')}>
          <SubtotalInfo
            totalInfo={totalInfo}
            bem={b}
            isCheckoutPage={isCheckoutPage}
            localPickup={localPickup}
            easyHomeShipKit={easyHomeShipKit}
            easyScanShipKit={easyScanShipKit}
            returnShipping={returnShipping}
            freeShipping={freeShipping}
            inStore={inStore}
            storeDropOf={storeDropOf}
            digitalDownload={digitalDownload}
            trialShipKit={trialShipKit}
            calculateOrderInfo={calculateOrderInfo}
          />
        </div>
        <div className={b('total-info')}>
          <p className={b('total-title', { mix: 'title' })}>{TOTAL}</p>
          <p id="orderTotalSum" className={b('total-sum')}>
            {`USD $${
            isCheckoutPage ? Number(toPay - delivery).toFixed(2) : toPay || '0.00'
          }`}
          </p>
        </div>
        {isCheckoutPage && (
          <div className={b('checkout-button')}>
            {isValidOrders ? (
              <ColorButton
                className={b('payment-btn')}
                text={CHECKOUT}
                backGroundColor={orangeColor}
                onClick={this.paymentRedirect}
              />
            ) : (
              <ColorButton
                className={b('payment-btn')}
                text={CHECKOUT}
                backGroundColor={orangeColor}
                disabled={!isValidOrders}
              />
            )}
          </div>
        )}
        {isCouponForm && (
          <div className={b('subtotal-coupon')}>
            <div className={b('subtotal-coupon-title')}>
              <div className={b('subtotal-coupon-title-text')}>{DISCOUNT_CODE}</div>
              <div className={b('subtotal-coupon-title-block-code')}>
                <div className={b('subtotal-coupon-title-code')}>{approveCouponName}</div>
                {approveCouponName && (
                  <button
                    className={b('subtotal-coupon-close-btn')}
                    type="button"
                    onClick={this.closeCouponStatus}
                  >
                    <CloseIcon2 />
                  </button>
                )}
              </div>
            </div>
            <div className={b('subtotal-coupon-wrapper')}>
              <NewCustomInput
                className={b('coupon-input', { success: couponSuccess })}
                placeholder={ENTER_CODE}
                value={couponName}
                onChange={this.handleChange}
              />
              <div className={b('btn-wrapper')}>
                <ColorButton
                  className={b('btn-coupon')}
                  text={APPLY}
                  onClick={this.checkCoupon}
                  disabled={!couponName}
                  backGroundColor={greenBtn}
                />
              </div>
            </div>
            {viewCouponStatus && (
              <div className={b('coupon-status-block')}>
                <span className={b('coupon-status', { success: couponSuccess })}>
                  {couponSuccess ? COUPON_ACTIVE : ''}
                  {getCouponError && getCouponError.statusCode === 400 && !isBadPromo
                    ? getCouponError.message
                    : ''}
                  {getCouponError && getCouponError.statusCode !== 400 && !isBadPromo
                    ? COUPON_FAIL
                    : ''}
                  {isBadPromo ? UNSUITABLE_PRODUCTS : ''}
                </span>
                <button
                  className={b('coupon-close-btn')}
                  type="button"
                  onClick={this.closeCouponStatus}
                >
                  <CloseIcon2 />
                </button>
              </div>
            )}
          </div>
        )}
        {calculateSpinnerFlag && (
          <div className={b('spinner-block')}>
            <img
              className={b('spinner', { invisible: !calculateSpinnerFlag })}
              src={spinner.src}
              alt="spinner"
            />
          </div>
        )}
      </main>
    );
  }
}

OrderSummary.propTypes = propTypes;
OrderSummary.defaultProps = defaultProps;

const stateProps = state => ({
  orders: ordersTypeSelect(state),
  price: priceSelect(state),
  selectPrice: orderPriceSelect(state),
  token: getToken(state),
  getCoupon: couponSelect(state),
  getCouponError: couponError(state),
  orderIsUpdate: orderIsUpdateSelector(state),
  userId: userIdSelector(state),
  shippingProducts: shippingProductsSelector(state),
  calculateOrderInfo: calculateOrderInfoSelector(state),
  calculateSpinnerFlag: calculateSpinnerFlagSelector(state),
});

const actions = {
  couponCheck: checkCoupon,
  orderCreate: createOrder,
  addProducts: addProductsToOrder,
  clearPromo: clearCoupon,
};

export default connect(stateProps, actions)(OrderSummary);
