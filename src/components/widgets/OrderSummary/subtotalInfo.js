import React from 'react';
import PropTypes from 'prop-types';
import ORDER_SUMMARY_TERMS from './constants';

const {
  SUBTOTAL,
  PRODUCTS,
  EXPRESS_DELIVERY,
  GIFT_OPTIONS,
  STATE_TAX,
  EASY_SCAN_SHIP_KIT,
  EASY_HOME_SHIP_KIT,
  RETURN_SHIP_KIT,
  FREE_SHIPPING,
  LOCAL_PICKUP,
  IN_STORE,
  STORE_DROP_OF,
  DIGITAL_DOWNLOAD,
  TRIAL_SHIP_KIT,
  FREE,
  COUPON_CODE,
} = ORDER_SUMMARY_TERMS;

const defaultProps = {
  bem: () => {},
  totalInfo: {},
  isCheckoutPage: false,
  localPickup: { displayName: LOCAL_PICKUP },
  easyHomeShipKit: { displayName: EASY_HOME_SHIP_KIT },
  easyScanShipKit: { displayName: EASY_SCAN_SHIP_KIT },
  returnShipping: { displayName: RETURN_SHIP_KIT },
  freeShipping: { displayName: FREE_SHIPPING },
  inStore: { displayName: IN_STORE },
  storeDropOf: { displayName: STORE_DROP_OF },
  digitalDownload: { displayName: DIGITAL_DOWNLOAD },
  trialShipKit: { displayName: TRIAL_SHIP_KIT },
  calculateOrderInfo: {},
};

const propTypes = {
  bem: PropTypes.func,
  totalInfo: PropTypes.shape({
    summAll: PropTypes.string,
    expressOrderSum: PropTypes.string,
    giftSum: PropTypes.string,
    taxSum: PropTypes.string,
    discountSum: PropTypes.string,
    discountName: PropTypes.string,
    scanShipKitSum: PropTypes.string,
    homeShipKitSum: PropTypes.string,
    returnShipKitSum: PropTypes.string,
    freeShippingSum: PropTypes.string,
    localPickupSum: PropTypes.string,
    inStoreSum: PropTypes.string,
    storeDropOfSum: PropTypes.string,
    digitalDownloadSum: PropTypes.string,
    trialShipKitSum: PropTypes.string,
  }),
  isCheckoutPage: PropTypes.bool,
  localPickup: PropTypes.shape({ displayName: PropTypes.string }),
  easyHomeShipKit: PropTypes.shape({ displayName: PropTypes.string }),
  easyScanShipKit: PropTypes.shape({ displayName: PropTypes.string }),
  returnShipping: PropTypes.shape({ displayName: PropTypes.string }),
  freeShipping: PropTypes.shape({ displayName: PropTypes.string }),
  inStore: PropTypes.shape({ displayName: PropTypes.string }),
  storeDropOf: PropTypes.shape({ displayName: PropTypes.string }),
  digitalDownload: PropTypes.shape({ displayName: PropTypes.string }),
  trialShipKit: PropTypes.shape({ displayName: PropTypes.string }),
  calculateOrderInfo: PropTypes.shape({
    sum: PropTypes.oneOf([PropTypes.string, PropTypes.number]),
    discount: PropTypes.oneOf([PropTypes.string, PropTypes.number]),
    tax: PropTypes.oneOf([PropTypes.string, PropTypes.number]),
    giftDiscount: PropTypes.oneOf([PropTypes.string, PropTypes.number]),
    quickly: PropTypes.oneOf([PropTypes.string, PropTypes.number]),
    delivery: PropTypes.oneOf([PropTypes.string, PropTypes.number]),
  }),
};


function SubtotalInfo(props) {
  const {
    bem,
    totalInfo,
    isCheckoutPage,
    localPickup,
    easyHomeShipKit,
    easyScanShipKit,
    returnShipping,
    freeShipping,
    inStore,
    storeDropOf,
    digitalDownload,
    trialShipKit,
    calculateOrderInfo,
  } = props;

  const {
    giftSum,
    scanShipKitSum,
    homeShipKitSum,
    returnShipKitSum,
    freeShippingSum,
    localPickupSum,
    inStoreSum,
    storeDropOfSum,
    digitalDownloadSum,
    trialShipKitSum,
  } = totalInfo;

  const {
    sum,
    discount,
    tax,
    giftDiscount,
    quickly,
    delivery,
  } = calculateOrderInfo;
  const summAll = `${(Number(sum) - Number(delivery) - Number(quickly) - Number(giftSum || 0)).toFixed(2)}`;

  const data = [
    {
      title: PRODUCTS,
      value: sum ? summAll : '0.00',
      prefix: '',
      isShow: true,
    },
    {
      title: easyHomeShipKit.displayName,
      value: homeShipKitSum,
      prefix: '',
      isShow: !isCheckoutPage,
    },
    {
      title: localPickup.displayName,
      value: localPickupSum,
      prefix: '',
      isShow: !isCheckoutPage,
    },
    {
      title: easyScanShipKit.displayName,
      value: scanShipKitSum,
      prefix: '',
      isShow: !isCheckoutPage,
    },
    {
      title: returnShipping.displayName,
      value: returnShipKitSum,
      prefix: '',
      isShow: !isCheckoutPage,
    },
    {
      title: freeShipping.displayName,
      value: freeShippingSum,
      prefix: '',
      isShow: !isCheckoutPage,
    },
    {
      title: inStore.displayName,
      value: inStoreSum,
      prefix: '',
      isShow: !isCheckoutPage,
    },
    {
      title: storeDropOf.displayName,
      value: storeDropOfSum,
      prefix: '',
      isShow: !isCheckoutPage,
    },
    {
      title: digitalDownload.displayName,
      value: digitalDownloadSum,
      prefix: '',
      isShow: !isCheckoutPage,
    },
    {
      title: trialShipKit.displayName,
      value: trialShipKitSum,
      prefix: '',
      isShow: !isCheckoutPage,
    },
    {
      title: EXPRESS_DELIVERY,
      value: Number(quickly) ? quickly : 0,
      prefix: '',
      isShow: true,
    },
    {
      title: GIFT_OPTIONS,
      value: giftSum,
      prefix: '',
      isShow: true,
    },
    {
      title: COUPON_CODE,
      value: (Number(discount) || Number(giftDiscount)) ? discount || giftDiscount : 0,
      prefix: '-',
      isShow: true,
    },
  ];

  const dataWithTax = [
    ...data,
    {
      title: STATE_TAX,
      value: tax,
      prefix: '',
      isShow: true,
    },
  ];

  return (
    <>
      <h3 className={bem('subtotal-title', { mix: 'title' })}>{SUBTOTAL}</h3>
      <div className={bem('subtotal')}>
        {(isCheckoutPage ? data : dataWithTax).map(({
          title,
          value,
          prefix,
          isShow,
        }) => ((title && value && isShow && !(title === 'Sales tax' && Number(value) === 0)) ? (
          <div className={bem('subtotal-info')} key={title}>
            <p className={bem('subtotal-info-name')}>{title}</p>
            <p className={bem('subtotal-info-price', { discount: title === COUPON_CODE })}>{Number(value) ? `${prefix}$${value}` : FREE}</p>
          </div>
        ) : null
        ))}
      </div>
    </>
  );
}

SubtotalInfo.propTypes = propTypes;
SubtotalInfo.defaultProps = defaultProps;


export default SubtotalInfo;
