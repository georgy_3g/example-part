import React from 'react';
import PropTypes from 'prop-types';
import ORDER_SUMMARY_TERMS from './constants';

const defaultProps = {
  handleShippingMethod: () => {},
  isShippingMethod: false,
  bem: () => {},
  description: '',
  tinyDescription: '',
  price: 0,
  withoutButton: false,
};

const propTypes = {
  handleShippingMethod: PropTypes.func,
  isShippingMethod: PropTypes.bool,
  bem: PropTypes.func,
  description: PropTypes.string,
  tinyDescription: PropTypes.string,
  price: PropTypes.number,
  withoutButton: PropTypes.bool,
};

const { FREE } = ORDER_SUMMARY_TERMS;

function ShippingText(props) {
  const {
    handleShippingMethod,
    isShippingMethod,
    bem,
    description,
    tinyDescription,
    price,
    withoutButton,
  } = props;
  return (
    <div className={bem('shipping-text-wrapper')}>
      <div className={bem('shipping-text')}>
        <p className={bem('shipping-description')}>{description}</p>
        <p className={bem('shipping-description', { tiny: true })}>{tinyDescription}</p>
      </div>
      {!withoutButton ? (
        <label htmlFor className={bem('shipping-checkbox')}>
          <p className={bem('shipping-price')}>{Number(price) ? `$${price}` : FREE}</p>
          <input
            className={bem('input')}
            type="checkbox"
            onClick={handleShippingMethod}
            checked={isShippingMethod}
          />
          <span className={bem('custom')} />
        </label>
      ) : (
        <p className={bem('shipping-price', { 'without-button': withoutButton })}>
          {Number(price) ? `$${price}` : FREE}
        </p>
      )}
    </div>
  );
}

ShippingText.propTypes = propTypes;
ShippingText.defaultProps = defaultProps;

export default ShippingText;
