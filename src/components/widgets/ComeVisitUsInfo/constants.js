const title = 'If you are in\nSouth Florida,\ncome visit us.';
const subTitle = 'Address';
const address = '1000 Clint Moore Road \nSuite 208\nBoca Raton, FL. 33487';
const bottomText = 'We are located behind\nClint Moore Road on Holland Drive.';
const buttonText = 'Get directions';
const linkToMaps =
  'https://www.google.com/maps/place/1000+Clint+Moore+Rd+%23+208,+Boca+Raton,+FL+33487';

export { title, subTitle, address, bottomText, buttonText, linkToMaps };
