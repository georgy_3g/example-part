import React from 'react';
import bem from 'src/utils/bem';
import PropTypes from 'prop-types';
import {
  title,
  subTitle,
  address,
  buttonText,
  linkToMaps,
  bottomText,
} from './constants';
import styles from './index.module.scss';

const b = bem('come-visit-us-info', styles);

const propTypes = {
  className: PropTypes.string,
};

const defaultProps = {
  className: '',
};

function GetInTouchInfo({ className }) {
  return (
    <section className={`${className} ${b()}`}>
      <div className={b('info-wrapper')}>
        <span className={b('title')}>{title}</span>
        <span className={b('info-title')}>{subTitle}</span>
        <span className={b('info-value')}>{address}</span>
        <span className={b('bottom-text')}>{bottomText}</span>
        <a className={b('link')} href={linkToMaps}>
          <div className={b('btn')}>{buttonText}</div>
        </a>
      </div>
    </section>
  );
}

GetInTouchInfo.propTypes = propTypes;
GetInTouchInfo.defaultProps = defaultProps;

export default GetInTouchInfo;
