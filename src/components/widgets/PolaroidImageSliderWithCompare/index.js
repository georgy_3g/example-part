import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Slider from 'react-slick';
import debounce from 'lodash/debounce';
import bem from 'src/utils/bem';
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import dynamic from "next/dynamic";
import styles from './index.module.scss';

const ArrowInSquareIcon = dynamic(() =>import('src/components/svg/ArrowInSquareIcon')) ;
const CompareSlideCard = dynamic(() =>import('src/components/cards/CompareSlideCard')) ;

const b = bem('polaroid-image-slider-with-compare', styles);

const propTypes = {
  slideList: PropTypes.arrayOf(PropTypes.shape()),
  dots: PropTypes.bool,
  arrows: PropTypes.bool,
  slidesToShow: PropTypes.number,
  slidesToScroll: PropTypes.number,
  smallShow: PropTypes.number,
  smallScroll: PropTypes.number,
  className: PropTypes.string,
  imageClass: PropTypes.string,
  beforeChange: PropTypes.func,
  lazyLoad: PropTypes.bool,
  afterChange: PropTypes.func,
  autoplay: PropTypes.bool,
  pauseOnHover: PropTypes.bool,
  autoplaySpeed: PropTypes.number,
  withOutTitle: PropTypes.bool,
  infinite: PropTypes.bool,
  isStaticFiles: PropTypes.bool,
};
const defaultProps = {
  slideList: [],
  dots: false,
  arrows: false,
  slidesToShow: 1,
  slidesToScroll: 1,
  smallShow: 1,
  smallScroll: 1,
  className: '',
  imageClass: '',
  beforeChange: () => {  },
  afterChange: () => {  },
  lazyLoad: true,
  autoplay: false,
  pauseOnHover: false,
  autoplaySpeed: 5000,
  withOutTitle: false,
  infinite: true,
  isStaticFiles: false,
};

class PolaroidImageSliderWithCompare extends Component {
  constructor(props) {
    super(props);
    this.slider = null;
    this.state = {
      index: 0,
    };
  }

  componentWillUnmount() {
    this.slider = null;
  }

  updateCount = (valueOne, valueTwo) => {
    const { beforeChange } = this.props;
    beforeChange(valueTwo);
    this.setState({ index: valueTwo });
  };

  settings = () => {
    const {
      arrows,
      slidesToShow,
      slidesToScroll,
      smallShow,
      smallScroll,
      afterChange,
      lazyLoad,
      autoplay,
      pauseOnHover,
      autoplaySpeed,
      infinite,
    } = this.props;
    return {
      arrows,
      infinite,
      focusOnSelect: false,
      draggable: false,
      lazyLoad,
      speed: 500,
      slidesToShow,
      slidesToScroll,
      beforeChange: this.updateCount,
      afterChange,
      responsive: [
        {
          breakpoint: 900,
          settings: {
            slidesToShow: smallShow,
            slidesToScroll: smallScroll,
          },
        },
      ],
      dotsClass: b('dots'),
      autoplay,
      pauseOnHover,
      autoplaySpeed,
      swipe: false,
    };
  };

  generateDots = () => {
    const { index } = this.state;
    const { slideList } = this.props;
    return slideList.map((item, i) => {
      const { url } = item;
      return {
        isSelect: index === i,
        key: `${i}-${url}`,
        i,
      };
    });
  }

  clickToDot = (value) => {
    this.slider.slickGoTo(value);
    this.waitPlay();
  };

  clickNext = () => {
    this.slider.slickNext();
    this.waitPlay();
  };

  clickPrev = () => {
    this.slider.slickPrev();
    this.waitPlay();
  };

  clickPause = () => {
    this.slider.slickPause();
  }

  clickPlay = () => {
    if (this.slider !== null) {
      this.slider.slickPlay();
    }
  }

  waitPlay = () => {
    const { autoplay } = this.props;
    if (autoplay) {
      this.clickPause();
      const debounceFunc = debounce(() => {
        this.clickPlay();
      }, 4000);
      debounceFunc();
    }
  }

  render() {
    const {
      slideList,
      className,
      imageClass,
      isStaticFiles,
    } = this.props;

    return (
      <div className={b({ mix: className })}>
        <div className={b('block-slider')}>
          <Slider ref={(c) => { this.slider = c; }} {...this.settings()}>
            {
              slideList.map(item => (
                <div className={b('slider-content')} key={`${item.id}_${item.url}`}>
                  <CompareSlideCard item={item} imageClass={imageClass} isStaticFiles={isStaticFiles} />
                  <span className={b('slider-description')}>{item.description}</span>
                </div>
              ))
            }
          </Slider>
        </div>
        <div className={b('controls')}>
          <button className={b('arrow-btn', { prev: true })} type="button" onClick={this.clickPrev}>
            <ArrowInSquareIcon className={b('arrow-icon', { prev: true })} />
          </button>
          <div className={b('dot-block')}>
            {this.generateDots().map(({ isSelect, key, i }) => (
              <button
                className={b('dot-wrap', { select: isSelect })}
                key={key}
                type="button"
                onClick={() => { this.clickToDot(i); }}
              >
                <div className={b('dot', { select: isSelect })} />
              </button>
            ))}
          </div>
          <button className={b('arrow-btn', { next: true })} type="button" onClick={this.clickNext}>
            <ArrowInSquareIcon className={b('arrow-icon', { next: true })} />
          </button>
        </div>
      </div>
    );
  }
}

PolaroidImageSliderWithCompare.propTypes = propTypes;
PolaroidImageSliderWithCompare.defaultProps = defaultProps;

export default PolaroidImageSliderWithCompare;
