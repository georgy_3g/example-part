import React, { Component } from 'react';
import PropTypes from 'prop-types';
import bem from 'src/utils/bem';
import getZipCode from 'src/utils/getZipCode';
import { connect } from 'react-redux';
import Image from 'next/image';
import debounce from 'lodash/debounce';
import {
  localPickupIsAvailableSelector,
  storeUtmContentSelector,
  zipCodeCreationDateSelector,
  zipCodeIsCheckedSelector,
  zipCodeSelector,
} from 'src/redux/location/selectors';
import { billingInfoSelect, zipCodesSelector } from 'src/redux/shared/selectors';
import { getToken } from 'src/redux/auth/selectors';
import {
  changeTemporaryUtmContentActions,
  getUserBillingInfo,
} from 'src/redux/shared/actions';
import { changeUtmContentActions, changeZipCode } from 'src/redux/location/actions';
import styles from './index.module.scss';
import {
  AVAILABLE,
  BOTH_OPTIONS_AVAILABLE,
  BOTTOM_TEXT,
  CHECK_BOX_TEXT,
  ENTER_ZIP_CODE,
  GREAT_NEWS,
  HEADQUARTERS_DESC,
  HEADQUARTERS_TITLE,
  HOME_SHIP_KIT_AVAILABLE,
  INPUT_PLACEHOLDER,
  LOCAL_PICKUP,
  LOCAL_PICKUP_DESC,
  NATIONAL_TEXT,
  NATIONAL_TITLE,
  NOT_AVAILABLE,
  ONE_DAY_MILLISECONDS,
  SHIP_KIT_DESC,
  SHIP_KIT_TITLE,
  TEXT,
  TITLE,
  YOUR_ZIP_CODE,
} from './constants';

const b = bem('shipping-target-block', styles);

const propTypes = {
  className: PropTypes.string,
  zipCode: PropTypes.string,
  zipCodeCreationDate: PropTypes.instanceOf(Date),
  changeZip: PropTypes.func,
  zipCodes: PropTypes.arrayOf(PropTypes.shape({})),
  shippingImages: PropTypes.shape({
    headquartersImage: PropTypes.shape({}),
    localHomeImage: PropTypes.shape({}),
    shipKitImage: PropTypes.shape({}),
  }),
  utmContent: PropTypes.string,
  getBillingInfo: PropTypes.func,
  token: PropTypes.string,
  storeUtmContent: PropTypes.string,
  billingInfo: PropTypes.shape({
    zipCode: PropTypes.string,
    state: PropTypes.string,
  }),
  changeUtmContent: PropTypes.func,
  changeTemporaryUtmContent: PropTypes.func,
};

const defaultProps = {
  className: '',
  zipCode: '',
  zipCodeCreationDate: '',
  changeZip: () => {},
  zipCodes: [],
  shippingImages: {
    headquartersImage: {},
    localHomeImage: {},
    shipKitImage: {},
  },
  utmContent: '',
  getBillingInfo: () => {},
  token: '',
  storeUtmContent: '',
  billingInfo: {},
  changeUtmContent: () => {},
  changeTemporaryUtmContent: () => {},
};

class ShippingTargetBlock extends Component {
  resizeWindow = debounce(() => {
    this.windowWidth = window.innerWidth;
  }, 300);

  constructor(props) {
    super(props);
    const { utmContent, zipCode, storeUtmContent } = props;
    this.Geolocation = null;
    this.state = {
      coordinate: null,
      isChecked: false,
      utmContent: storeUtmContent || utmContent,
      zip: zipCode || '',
    };
  }

  componentDidMount() {
    const { zipCode, token, getBillingInfo, storeUtmContent } = this.props;
    const { utmContent } = this.state;
    this.Geolocation = navigator && navigator.geolocation;
    this.windowWidth = window.innerWidth;

    if (token && (!zipCode || !storeUtmContent)) {
      getBillingInfo();
    }
    if (this.Geolocation && Object.keys(this.Geolocation).length && !zipCode && !utmContent) {
      this.Geolocation.getCurrentPosition((e) => {
        this.getCoordinates(e, !token);
      });
    }
    if ((!this.Geolocation || !Object.keys(this.Geolocation).length)  && !zipCode && !utmContent && !token && window.geoip2) {
      try {
        window.geoip2.city(this.getIpZipCode, () => null);
      }
      catch(e) {
        console.log('error geoip2', e);
      }
    }
    window.addEventListener('resize', this.resizeWindow);
  }

  componentDidUpdate(prevProps) {
    const { billingInfo } = this.props;
    const { billingInfo: prevBillingInfo } = prevProps;
    if (
      billingInfo &&
      Object.keys(billingInfo).length &&
      (!prevBillingInfo || !Object.keys(prevBillingInfo).length)
    ) {
      this.checkBillingData();
    }
  }

  componentWillUnmount() {
    this.Geolocation = null;
    window.removeEventListener('resize', this.resizeWindow);
    this.windowWidth = null;
  }

  checkBillingData = () => {
    const {
      billingInfo: { zipCode, state } = {},
      storeUtmContent,
      zipCode: storeZipCode,
      zipCodes,
      changeZip,
      changeUtmContent,
    } = this.props;
    const { zip } = this.state;
    if (!zipCode && !state) {
      if (this.Geolocation && Object.keys(this.Geolocation).length) {
        this.Geolocation.getCurrentPosition((e) => {
          this.getCoordinates(e, true);
        });
      } else if(window.geoip2) {
        try {
          window.geoip2.city(this.getIpZipCode, () => null);
        }
        catch(e) {
          console.log('error geoip2', e);
        }
      }
    } else {
      const zipValue = zipCode || zip || '';
      const isLocal = zipCodes.some(({ zip: itmZip }) => itmZip === zipValue);
      this.setState(
        {
          ...(!storeZipCode ? { zip: zipValue } : {}),
          ...(!storeUtmContent && zipValue
            ? { utmContent: isLocal || state.toLowerCase() === 'fl' ? 'l' : 'n' }
            : {}),
        },
        () => {
          if (!storeZipCode && zipCode) {
            changeZip(zipCode);
          }
          if (!storeUtmContent && (zipCode || state)) {
            changeUtmContent((zipCode && isLocal) || state.toLowerCase() === 'fl' ? 'l' : 'n');
          }
        },
      );
    }
  };

  updateUtmContent = (content) => {
    this.setState({ utmContent: content });
  };

  getNewZipCode = async () => {
    const { coordinate } = this.state;
    const { zipCodes, changeTemporaryUtmContent } = this.props;
    const code = await getZipCode(coordinate);
    if (code) {
      const newValue = zipCodes.some(({ zip }) => zip === code) ? 'l' : 'n';
      this.setState({ utmContent: newValue, zip: code }, () => {
        changeTemporaryUtmContent(newValue);
      });
    } else {
      this.setState({ zip: '' });
    }
  };

  checkDate = (zipCodeDate) =>
    zipCodeDate
      ? new Date(zipCodeDate).valueOf() + ONE_DAY_MILLISECONDS < new Date().valueOf()
      : false;

  getCoordinates = ({ coords }, isAutoZip) => {
    const { latitude: lat, longitude: lng } = coords;
    this.setState({ coordinate: { lat, lng } }, () => {
      if (isAutoZip) {
        this.getNewZipCode();
      }
    });
  };

  getIpZipCode = (data) => {
    const { zipCodes, changeTemporaryUtmContent } = this.props;
    const { postal: { code } = {}, subdivisions: [{ names: { en: enName } = {} }] = [{}] } =
      data || {};
    if (code) {
      const newValue = zipCodes.some(({ zip }) => zip === code) ? 'l' : 'n';
      this.setState(
        {
          utmContent: newValue,
          zip: code,
        },
        () => {
          changeTemporaryUtmContent(newValue);
        },
      );
    }
    if (!code && enName) {
      this.setState(
        {
          utmContent: enName.toLowerCase() === 'florida' ? 'l' : 'n',
        },
        () => {
          changeTemporaryUtmContent(enName.toLowerCase() === 'florida');
        },
      );
    }
  };

  checkZipCode = () => {
    const { zipCodes } = this.props;
    const { zip: zipCode } = this.state;
    return zipCodes.some(({ zip }) => zip === zipCode);
  };

  checkZipCodesState = () => {
    const { zipCodes } = this.props;
    const { zip: zipCode } = this.state;
    return zipCodes.some(({ zip, localPickUp }) => zip === zipCode && localPickUp);
  };

  changeZipCode = (e) => {
    const { value } = e.target;
    const { changeZip } = this.props;
    changeZip(value);
    this.setState({ zip: value });
  };

  getInfoText = (zipCode) => {
    if (zipCode && this.checkZipCode()) {
      return BOTH_OPTIONS_AVAILABLE;
    }
    if (zipCode && !this.checkZipCode()) {
      return HOME_SHIP_KIT_AVAILABLE;
    }
    return ENTER_ZIP_CODE;
  };

  onClickCheckBox = () => {
    const { zipCode, zipCodeCreationDate, changeZip } = this.props;
    const { isChecked, coordinate } = this.state;
    this.setState({ isChecked: !isChecked }, () => {
      if (coordinate && (!zipCode || this.checkDate(zipCodeCreationDate)) && !isChecked) {
        this.getNewZipCode();
      } else if (isChecked) {
        changeZip('');
      }
    });
  };

  getTitle = (isZipCode, isNational) => {
    if (isZipCode) {
      return GREAT_NEWS;
    }
    return isNational ? NATIONAL_TITLE : TITLE;
  };

  getText = (isZipCode, isNational) => {
    if (isZipCode) {
      return YOUR_ZIP_CODE;
    }
    return isNational ? NATIONAL_TEXT : TEXT;
  };

  render() {
    const { className, shippingImages } = this.props;

    const {
      isChecked,
      coordinate,
      utmContent,
      zip,
    } = this.state;

    const isNational = (utmContent || '').toLowerCase() !== 'l';
    const { headquartersImage, localHomeImage, shipKitImage } = shippingImages;

    const isZipCode = zip && zip.length >= 5;
    const isLocalPickup = zip && this.checkZipCode();
    const isZipCodeState = zip && this.checkZipCodesState();

    return (
      <div className={b({ mix: className })}>
        <div className={b('content-block')}>
          <div className={b('stripe-one')} />
          <div className={b('stripe-two')} />
          <div className={b('stripe-three')} />
          <div className={b('input-block', { 'small-margin': isZipCode })}>
            <span className={b('input-title')}>{this.getTitle(isZipCode, isNational)}</span>
            <span className={b('input-text')}>{this.getText(isZipCode, isNational)}</span>
            <input
              className={b('zip-code-input')}
              type="text"
              value={zip}
              onChange={this.changeZipCode}
              placeholder={INPUT_PLACEHOLDER}
            />
            {coordinate && !zip && (
              <div className={b('check-block')}>
                <button
                  className={b('check-box', { checked: isChecked })}
                  type="button"
                  onClick={this.onClickCheckBox}
                />
                <span className={b('check-box-text')}>{CHECK_BOX_TEXT}</span>
              </div>
            )}
            {isZipCode && <span className={b('bottom-text')}>{BOTTOM_TEXT}</span>}
          </div>
          <div className={b('card-list')}>
            <div
              className={b('shipping-card', {
                'not-available': !isLocalPickup && zip && !isZipCodeState,
              })}
              style={{ order: isNational ? 2 : 1 }}
            >
              {isZipCode && (
                <div className={b('status-block', { available: isLocalPickup || isZipCodeState })}>
                  <span
                    className={b('status-test', { available: isLocalPickup || isZipCodeState })}
                  >
                    {isLocalPickup || isZipCodeState ? AVAILABLE : NOT_AVAILABLE}
                  </span>
                </div>
              )}
              <span
                className={b('card-title', { 'not-available': !isLocalPickup && !isZipCodeState })}
              >
                {HEADQUARTERS_TITLE}
              </span>
              <div
                className={b('card-image', { 'not-available': !isLocalPickup && !isZipCodeState })}
              >
                <Image
                  src={headquartersImage}
                  alt="ship kit"
                  layout="fill"
                />
              </div>
              <span
                className={b('card-desc', { 'not-available': !isLocalPickup && !isZipCodeState })}
              >
                {HEADQUARTERS_DESC}
              </span>
            </div>
            <div
              className={b('shipping-card', { 'not-available': !isLocalPickup && zip })}
              style={{ order: isNational ? 3 : 2 }}
            >
              {isZipCode && (
                <div className={b('status-block', { available: isLocalPickup })}>
                  <span className={b('status-test', { available: isLocalPickup })}>
                    {isLocalPickup ? AVAILABLE : NOT_AVAILABLE}
                  </span>
                </div>
              )}
              <span className={b('card-title', { 'not-available': !isLocalPickup })}>
                {LOCAL_PICKUP}
              </span>
              <div className={b('card-image', { 'not-available': !isLocalPickup })}>
                <Image
                  src={localHomeImage}
                  alt="ship kit"
                  layout="fill"
                />
              </div>
              <span className={b('card-desc', { 'not-available': !isLocalPickup })}>
                {LOCAL_PICKUP_DESC}
              </span>
            </div>
            <div className={b('shipping-card')} style={{ order: isNational ? 1 : 3 }}>
              {isZipCode && (
                <div className={b('status-block', { available: isZipCode })}>
                  <span className={b('status-test', { available: isZipCode })}>
                    {isZipCode ? AVAILABLE : NOT_AVAILABLE}
                  </span>
                </div>
              )}
              <span className={b('card-title')}>{SHIP_KIT_TITLE}</span>
              <div className={b('card-image')}>
                <Image src={shipKitImage} alt="ship kit" layout="fill" />
              </div>
              <span className={b('card-desc')}>{SHIP_KIT_DESC}</span>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

ShippingTargetBlock.propTypes = propTypes;
ShippingTargetBlock.defaultProps = defaultProps;

const stateProps = (state) => ({
  zipCode: zipCodeSelector(state),
  zipCodeCreationDate: zipCodeCreationDateSelector(state),
  zipCodeIsChecked: zipCodeIsCheckedSelector(state),
  localPickupIsAvailable: localPickupIsAvailableSelector(state),
  zipCodes: zipCodesSelector(state),
  billingInfo: billingInfoSelect(state),
  token: getToken(state),
  storeUtmContent: storeUtmContentSelector(state),
});

const actions = {
  changeZip: changeZipCode,
  getBillingInfo: getUserBillingInfo,
  changeUtmContent: changeUtmContentActions,
  changeTemporaryUtmContent: changeTemporaryUtmContentActions,
};

export default connect(stateProps, actions)(ShippingTargetBlock);
