const LEARN_MORE = 'Learn more';
const YOUR_ZIP_CODE = 'Your zip code:';
const GREAT_NEWS = 'Great news! \nWe have easy pickup & delivery \n options in your area.';
const HOME_SHIP_KIT_AVAILABLE = 'The home ship kit\noptions is available at\nyour location.';
const BOTH_OPTIONS_AVAILABLE = 'Both options are\navailable at your\nlocation.';
const LOCAL_PICKUP = 'Local home pickup';
const AVAILABLE = 'Available!';
const NOT_AVAILABLE = 'Not yet available.';
const SHIP_KIT_DESC =
  'Our 3-Way Home Ship Kit is the safest\nway to transfer your memories\nwithout leaving your home.';
const LOCAL_PICKUP_DESC =
  'Our licensed & insured team member \nwill come directly to your home to \npickup & drop-off your memories.';
const ENTER_ZIP_CODE =
  'Enter your zip code\nto see which options\nare available at your\nlocation.';
const ONE_DAY_MILLISECONDS = 86400000;
const BUTTON_TITLE = 'EASY SHIP KIT';
const TITLE = 'Are you local to us\nin South Florida?';
const TEXT = 'Check to see if you are in our local area:';
const NATIONAL_TITLE = 'Simple & secure nationwide\ndelivery options.';
const NATIONAL_TEXT = 'Check the delivery options in your area.';
const CHECK_BOX_TEXT = 'Use my current location';
const BOTTOM_TEXT = 'Select your preferred\noption at checkout';
const INPUT_PLACEHOLDER = 'Enter your zip code here';
const HEADQUARTERS_TITLE = 'Visit our headquarters';
const HEADQUARTERS_DESC =
  'Want to hand your memories directly \nto your team of archivists? Come \nvisit us at our Boca Raton headquarters.';
const SHIP_KIT_TITLE = 'Home ship kit';

export {
  LEARN_MORE,
  YOUR_ZIP_CODE,
  GREAT_NEWS,
  HOME_SHIP_KIT_AVAILABLE,
  BOTH_OPTIONS_AVAILABLE,
  LOCAL_PICKUP,
  AVAILABLE,
  NOT_AVAILABLE,
  SHIP_KIT_DESC,
  LOCAL_PICKUP_DESC,
  ENTER_ZIP_CODE,
  ONE_DAY_MILLISECONDS,
  BUTTON_TITLE,
  TITLE,
  TEXT,
  NATIONAL_TITLE,
  NATIONAL_TEXT,
  CHECK_BOX_TEXT,
  BOTTOM_TEXT,
  INPUT_PLACEHOLDER,
  HEADQUARTERS_TITLE,
  HEADQUARTERS_DESC,
  SHIP_KIT_TITLE,
};
