const DELETE_CARD = 'Delete card';
const ERROR = 'Something went wrong, check the card data and try again.';

export { DELETE_CARD, ERROR };
