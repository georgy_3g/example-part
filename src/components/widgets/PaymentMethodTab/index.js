import React from 'react';
import formattedToCardNumber from 'src/utils/formattedToCardNumber';
import bem from 'src/utils/bem';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import dynamic from 'next/dynamic';
import { isCardErrorSelector, userPaymentInfoSelector } from 'src/redux/shared/selectors';
import { deletePaymentInfo } from 'src/redux/shared/actions';
import { DELETE_CARD, ERROR, MEMOYA } from './constants';
import styles from './index.module.scss';

const PaymentInfoForm = dynamic(() => import('src/components/forms/PaymentInfoForm'));

const b = bem('payment-method-tab', styles);
const btn = bem('btn', styles);

const defaultProps = {
  creditCard: {},
  className: '',
  deleteCard: () => {},
  isCardError: false,
};

const propTypes = {
  creditCard: PropTypes.shape({
    cardNumber: PropTypes.string,
    cardType: PropTypes.string,
  }),
  className: PropTypes.string,
  deleteCard: PropTypes.func,
  isCardError: PropTypes.bool,
};

function PaymentMethodTab(props) {
  const { creditCard, className, deleteCard, isCardError } = props;
  const { cardNumber, cardType } = creditCard;

  return (
    <main className={b({ mix: className })}>
      <div className={b('tab-wrap')}>
        {isCardError && <span className={b('error')}>{ERROR}</span>}
        {cardNumber ? (
          <div className={b('card-wrap')}>
            <div className={b('card')}>
              <span className={b('logo')}>{MEMOYA}</span>
              <span className={b('card-number')}>
                {`${cardType}: ${formattedToCardNumber(cardNumber)}`}
              </span>
              <button
                className={b('delete-button', { mix: btn() })}
                type="button"
                onClick={deleteCard}
              >
                {DELETE_CARD}
              </button>
            </div>
          </div>
        ) : (
          <PaymentInfoForm />
        )}
      </div>
    </main>
  );
}

PaymentMethodTab.propTypes = propTypes;
PaymentMethodTab.defaultProps = defaultProps;

const stateProps = (state) => ({
  creditCard: userPaymentInfoSelector(state),
  isCardError: isCardErrorSelector(state),
});

const actions = {
  deleteCard: deletePaymentInfo,
};

export default connect(stateProps, actions)(PaymentMethodTab);
