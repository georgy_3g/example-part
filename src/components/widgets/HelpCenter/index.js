import React, { useState } from 'react';
import ReactDOM from 'react-dom';
import bem from 'src/utils/bem';
import PropTypes from 'prop-types';
import dynamic from "next/dynamic";
import HelpCenterCard from 'src/components/cards/HelpCenterCard';
import styles from './index.module.scss';

const DottedQuestionIcon = dynamic(() => import('src/components/svg/DottedQuestionIcon'));

const b = bem('help-center', styles);
const TITLE = 'Help Center';

const defaultProps = {
  className: '',
  list: [],
  name: '',
  changeUseStatus: () => {},
};

const propTypes = {
  className: PropTypes.string,
  list: PropTypes.arrayOf(PropTypes.shape({})),
  name: PropTypes.string,
  changeUseStatus: PropTypes.func,
};

function HelpCenter(props) {
  const { className, list, name, changeUseStatus } = props;

  const [isShowPopup, showPopup] = useState(false);

  const newList = list.filter(({ type }) => {
    if (!type) {
      return true;
    }
    return type === name;
  });

  return (
    <div className={b({ mix: className })}>
      <div
        className={b('main-btn')}
        role="button"
        tabIndex={-1}
        onClick={() => {
          showPopup(true);
          changeUseStatus();
        }}
      >
        <DottedQuestionIcon />
        <div className={b('title')}>{TITLE}</div>
      </div>
      {isShowPopup &&
        ReactDOM.createPortal(
          <HelpCenterCard helpList={newList} closePopUp={() => showPopup(false)} />,
          document.getElementById('__next') || null,
        )}
    </div>
  );
}

HelpCenter.propTypes = propTypes;
HelpCenter.defaultProps = defaultProps;

export default HelpCenter;
