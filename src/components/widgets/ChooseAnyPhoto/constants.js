const TEXT_TITLE_DOWNLOAD = (typeProduct) =>
  `Each photo ${
    typeProduct ? 'restoration' : 'retouching'
  } includes\n******* digital download.`;
const TEXT_TITLE_PRINTS = (typeProduct) =>
  `Add discounted archival photo prints now\nor purchase photo art after you approve\nyour ${
    typeProduct ? 'restorations' : 'retrouched'
  }.`;
const PHOTO_COLORIZATION = 'photo_colorization';

export { TEXT_TITLE_DOWNLOAD, TEXT_TITLE_PRINTS, PHOTO_COLORIZATION };
