import React, { Component, createRef } from 'react';
import bem from 'src/utils/bem';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import dynamic from 'next/dynamic';
import { getOptionEnhance } from 'src/redux/enhance/actions';
import { PHOTO_COLORIZATION, TEXT_TITLE_DOWNLOAD, TEXT_TITLE_PRINTS } from './constants';
import styles from './index.module.scss';

const AddPrints = dynamic(() => import('src/components/svg/AddPrints'));
const CloudDownload = dynamic(() => import('src/components/svg/CloudDownload'));
const EnhanceImageCard = dynamic(() => import('src/components/cards/EnhanceImageCard'));

const b = bem('restoration-choose-any-photo', styles);

const defaultProps = {
  isColorization: [],
  className: '',
  hidden: false,
  nameOperation: 'photo_restoration',
  orderImagesEnhance: [],
  getOptionEnhanceImage: () => {},
  appendOptionEnhance: [],
  changeAppendOptions: () => {},
  isCheckout: false,
  selectColorizationImage: () => {},
};

const propTypes = {
  isColorization: PropTypes.arrayOf(PropTypes.number),
  className: PropTypes.string,
  hidden: PropTypes.bool,
  nameOperation: PropTypes.string,
  orderImagesEnhance: PropTypes.arrayOf(PropTypes.shape({
    imageUri: PropTypes.string,
    bottomText: PropTypes.string,
  })),
  getOptionEnhanceImage: PropTypes.func,
  appendOptionEnhance: PropTypes.arrayOf({}),
  changeAppendOptions: PropTypes.func,
  isCheckout: PropTypes.bool,
  selectColorizationImage: PropTypes.func,
};

class ChooseAnyPhoto extends Component {
  constructor(props) {
    super(props);
    const { orderImagesEnhance } = props;

    this.ref = createRef();
    this.scroll = createRef();
    this.arrayRefs = {};

    this.state = {
      scrollTop: 0,
      orderImagesEnhance: orderImagesEnhance || [],
    };
  }

  componentDidMount() {
    const { getOptionEnhanceImage } = this.props;
    getOptionEnhanceImage();
    this.scroll.current.addEventListener('scroll', this.onScroll);
  }

  componentDidUpdate(prevProps) {
    const { orderImagesEnhance } = this.props;
    const { orderImagesEnhance: orderImagesEnhancePrev } = prevProps;

    const isChangeBottomText = orderImagesEnhance.some((item, index) => item.bottomText && (item.bottomText !== orderImagesEnhancePrev[index].bottomText) );

    if (isChangeBottomText || orderImagesEnhance.length !== orderImagesEnhancePrev.length) {
      this.setEnhanceImages(orderImagesEnhance);
    }
  }

  componentWillUnmount() {
    this.scroll.current.removeEventListener('scroll', this.onScroll);
    this.arrayRefs = {};
    this.ref = null;
  }

  setEnhanceImages = (orderImagesEnhance) => {
    this.setState({ orderImagesEnhance });
  }


  onScroll = () => {
    const { scrollTop: scrollState } = this.state;
    const { scrollTop = 0 } = this.scroll.current;
    if (scrollTop !== scrollState) {
      this.setState({ scrollTop });
    }
  };

  saveImageRef = (imgId, e) => {
    this.arrayRefs[imgId] = e;
  };

  setCardBottomPopup = (idImage, bottomText) => {
    const { orderImagesEnhance } = this.state;

    const newOrderImagesEnhance = orderImagesEnhance.map(item => {
      if (item.id === idImage) {
        return {
          ...item,
          bottomText,
        }
      }
      return item;
    });

    this.setState({ orderImagesEnhance: newOrderImagesEnhance });
  }

  render() {
    const {
      isColorization,
      className,
      appendOptionEnhance,
      changeAppendOptions,
      nameOperation,
      hidden,
      isCheckout,
      selectColorizationImage,
    } = this.props;
    const { scrollTop, orderImagesEnhance } = this.state;

    const isRestoration = nameOperation === 'photo_restoration';

    return (
      <div className={b({ mix: className, cart: hidden })}>
        {!hidden && (
          <div className={b('description')}>
            <div className={b('description-block')}>
              <CloudDownload className={b('description-icon', { cloud: true })} />
              <p className={b('description-text')}>{TEXT_TITLE_DOWNLOAD(isRestoration)}</p>
            </div>
            <div className={b('description-block')}>
              <AddPrints className={b('description-icon', { 'add-prints': true })} />
              <p className={b('description-text')}>{TEXT_TITLE_PRINTS(isRestoration)}</p>
            </div>
          </div>
        )}
        <div className={b('choose-photo-main', { cart: hidden })}>
          <div className={b('choose-photo-wrapper')} ref={this.ref}>
            <div className={b('choose-photo-container', { cart: hidden })} ref={this.scroll}>
              {orderImagesEnhance.map((image, index) =>
                image.isSelected ? (
                  <div
                    className={b('container-item-wrap', { margin: image.bottomText })}
                    key={image.id}
                    style={{ zIndex: orderImagesEnhance.length + 5 - index }}
                  >
                    <EnhanceImageCard
                      portalId="__next"
                      isFullScreen
                      isCheckout={isCheckout}
                      clickPrint={this.clickPrint}
                      data={image}
                      colorizationImages={image.colorizationImages}
                      selectColorizationImage={selectColorizationImage}
                      selectedArr={orderImagesEnhance}
                      nameOperation={
                        isColorization.includes(image.id) ? PHOTO_COLORIZATION : nameOperation
                      }
                      idPhoto={image.id}
                      appendOptionEnhance={appendOptionEnhance}
                      changeAppendOptions={changeAppendOptions}
                      refsRoot={this.ref || {}}
                      positionData={this.arrayRefs[image.id]}
                      scrollTop={scrollTop}
                      saveImageRef={this.saveImageRef}
                      scrollRef={this.scroll}
                      zIndex={orderImagesEnhance.length + 5 - index - 1}
                      index={index}
                      setCardBottomPopupProps={this.setCardBottomPopup}
                    />
                  </div>
                ) : null,
              )}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const actions = {
  getOptionEnhanceImage: getOptionEnhance,
};

ChooseAnyPhoto.propTypes = propTypes;
ChooseAnyPhoto.defaultProps = defaultProps;

export default connect(null, actions)(ChooseAnyPhoto);
