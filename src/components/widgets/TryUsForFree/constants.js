

import MAIN_IMAGE_DESKTOP from  'public/img/scan/TRY_US_FOR_FREE/free-scan-trial-desktop-min.jpg';
import MAIN_IMAGE_TABLET from  'public/img/scan/TRY_US_FOR_FREE/free-scan-trial-tablet-min.jpg';
import MAIN_IMAGE_MOBILE from  'public/img/scan/TRY_US_FOR_FREE/free-scan-trial-mobile-min.jpg';

const TITLE = 'Try us for free.';
const TEXT_ONE ="We know you're going to love our photo scanning service. We’ll scan 25 photos to digital with a 30 day trial of our ******* for free.";
const TEXT_TWO = 'Just pay $10 for a 3-way ship kit.';
const TEXT_BUTTON = 'Order my kit';

export {
  MAIN_IMAGE_DESKTOP,
  MAIN_IMAGE_TABLET,
  MAIN_IMAGE_MOBILE,
  TITLE,
  TEXT_ONE,
  TEXT_TWO,
  TEXT_BUTTON,
};
