import React, { Component } from 'react';
import bem from 'src/utils/bem';
import PropTypes from 'prop-types';
import Image from 'next/image';
import dynamic from 'next/dynamic';
import {
  MAIN_IMAGE_DESKTOP,
  MAIN_IMAGE_MOBILE,
  MAIN_IMAGE_TABLET,
  TEXT_BUTTON,
  TEXT_ONE,
  TEXT_TWO,
  TITLE,
} from './constants';
import styles from './index.module.scss';

const FreePhotoScan = dynamic(() => import('src/components/widgets/FreePhotoScan'));

const b = bem('try-us-for-free', styles);

const defaultProps = {
  typeProduct: '',
  product: {},
  transferTitle: '',
};

const propTypes = {
  product: PropTypes.shape({
    price: PropTypes.number,
    reduce: PropTypes.func,
    displayName: PropTypes.string,
    id: PropTypes.number,
    name: PropTypes.string,
  }),
  typeProduct: PropTypes.string,
  transferTitle: PropTypes.string,
};

class TryUsForFree extends Component {
  constructor(props) {
    super(props);
    this.state = {
      image: MAIN_IMAGE_DESKTOP,
      isShowPopup: false,
    };
  }

  componentDidMount() {
    this.setImage();
    window.addEventListener('resize', this.setImage);
  }

  setImage = () => {
    const width = window.innerWidth;
    if (width > 1024) {
      this.setState({ image: MAIN_IMAGE_DESKTOP });
    }
    if (width > 680 && width <= 1024) {
      this.setState({ image: MAIN_IMAGE_TABLET });
    }
    if (width <= 680) {
      this.setState({ image: MAIN_IMAGE_MOBILE });
    }
  };

  showPopup = (open) => {
    this.setState({ isShowPopup: open });
  };

  render() {
    const { image, isShowPopup } = this.state;
    const { transferTitle, typeProduct, product } = this.props;
    return (
      <div className={b()}>
        <div className={b('container')}>
          <div className={b('image')}>
            <Image src={image} alt="main" className={b('image')} layout="responsive" />
          </div>
          <div className={b('content')}>
            <h2 className={b('title')}>{TITLE}</h2>
            <div className={b('text-one')}>{TEXT_ONE}</div>
            <div className={b('text-two')}>{TEXT_TWO}</div>
            <button
              type="button"
              className={b('button')}
              onClick={() => {
                this.showPopup(true);
              }}
            >
              {TEXT_BUTTON}
            </button>
          </div>
          <div className={b('right-square')} />
          <div className={b('left-square-one')} />
          <div className={b('left-square-two')} />
        </div>
        {isShowPopup && (
          <FreePhotoScan
            showPopup={this.showPopup}
            transferTitle={transferTitle}
            typeProduct={typeProduct}
            product={product}
          />
        )}
      </div>
    );
  }
}

TryUsForFree.propTypes = propTypes;
TryUsForFree.defaultProps = defaultProps;

export default TryUsForFree;
