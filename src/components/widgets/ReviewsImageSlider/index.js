import React, { useState } from 'react';
import PropTypes from 'prop-types';
import Slider from 'react-slick';
import bem from 'src/utils/bem';
import colors from 'src/styles/colors.json';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import dynamic from "next/dynamic";
import ReviewCard from 'src/components/cards/ReviewCard';
import styles from './index.module.scss';

const ArrowInSquareIcon = dynamic(() => import('src/components/svg/ArrowInSquareIcon'));

const gray = colors['$athens-light-gray-color'];

const b = bem('reviews-image-slider', styles);

const propTypes = {
  slideList: PropTypes.arrayOf(PropTypes.shape()),
  dots: PropTypes.bool,
  arrows: PropTypes.bool,
  slidesToShow: PropTypes.number,
  slidesToScroll: PropTypes.number,
  smallShow: PropTypes.number,
  miniShow: PropTypes.number,
  smallScroll: PropTypes.number,
  className: PropTypes.string,
  imageClass: PropTypes.string,
  beforeChange: PropTypes.func,
  lazyLoad: PropTypes.bool,
  afterChange: PropTypes.func,
  autoplay: PropTypes.bool,
  pauseOnHover: PropTypes.bool,
  autoplaySpeed: PropTypes.number,
  withOutTitle: PropTypes.bool,
  customerSlider: PropTypes.bool,
};
const defaultProps = {
  slideList: [],
  dots: false,
  arrows: false,
  slidesToShow: 3,
  slidesToScroll: 1,
  smallShow: 2,
  miniShow: 1,
  smallScroll: 1,
  className: '',
  imageClass: '',
  beforeChange: () => {},
  afterChange: () => {},
  lazyLoad: true,
  autoplay: false,
  pauseOnHover: false,
  autoplaySpeed: 5000,
  withOutTitle: false,
  customerSlider: false,
};

function ReviewsImageSlider(props) {
  const {
    slideList,
    arrows,
    slidesToShow,
    slidesToScroll,
    className,
    smallShow,
    smallScroll,
    afterChange,
    beforeChange,
    lazyLoad,
    autoplay,
    pauseOnHover,
    autoplaySpeed,
    miniShow,
    customerSlider,
  } = props;

  const [index, setCount] = useState(0);

  let slider = null;

  const updateCount = (valueOne, valueTwo) => {
    beforeChange(valueTwo);
    setCount(valueTwo);
  };

  const settings = {
    arrows,
    infinite: true,
    focusOnSelect: true,
    lazyLoad,
    speed: 500,
    slidesToShow,
    slidesToScroll,
    beforeChange: updateCount,
    afterChange,
    responsive: [
      {
        breakpoint: 1366,
        settings: {
          slidesToShow,
          slidesToScroll: smallScroll,
        },
      },
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: smallShow,
          slidesToScroll: smallScroll,
        },
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: smallShow,
          slidesToScroll: smallScroll,
        },
      },
      {
        breakpoint: 750,
        settings: {
          slidesToShow: miniShow,
          slidesToScroll: smallScroll,
        },
      },
    ],
    dotsClass: b('dots'),
    autoplay,
    pauseOnHover,
    autoplaySpeed,
  };

  const generateDots = () =>
    slideList.map((item, i) => {
      const { url } = item;
      return {
        isSelect: index === i,
        key: `${i}-${url}`,
        i,
      };
    });

  const clickToDot = (value) => {
    slider.slickGoTo(value);
  };

  const clickNext = () => {
    slider.slickNext();
  };

  const clickPrev = () => {
    slider.slickPrev();
  };

  return (
    <div className={b({ mix: className })}>
      <Slider
        ref={(c) => {
          slider = c;
        }}
        {...settings}
      >
        {slideList.map((review) => (
          <ReviewCard
            className={b('review')}
            review={review}
            isReverseContent
            isCustomerCard={customerSlider}
            isScrollWrap
          />
        ))}
      </Slider>
      <div className={b('controls')}>
        <button className={b('arrow-btn', { prev: true })} type="button" onClick={clickPrev}>
          <ArrowInSquareIcon fill={gray} className={b('arrow-icon', { prev: true })} />
        </button>
        <div className={b('dot-block')}>
          {generateDots().map(({ isSelect, key, i }) => (
            <button
              className={b('dot-wrap', { select: isSelect })}
              key={key}
              type="button"
              onClick={() => {
                clickToDot(i);
              }}
            >
              <div className={b('dot', { select: isSelect })} />
            </button>
          ))}
        </div>
        <button className={b('arrow-btn', { next: true })} type="button" onClick={clickNext}>
          <ArrowInSquareIcon fill={gray} className={b('arrow-icon', { next: true })} />
        </button>
      </div>
    </div>
  );
}

ReviewsImageSlider.propTypes = propTypes;
ReviewsImageSlider.defaultProps = defaultProps;

export default ReviewsImageSlider;
