import React, { FunctionComponent } from 'react';
import bem from 'src/utils/bem';
import Slider from 'react-slick';
import Image from 'next/image';
import slide1 from  'public/img/enhance/wizardSlide1.jpeg';
import slide2 from  'public/img/enhance/wizardSlide2.jpeg';
import styles from './index.module.scss';
import { propsInterface } from './interface';


const b = bem('enhance-slider', styles);
const defaultProps = {
  className: '',
  slideList: [],
};

const settings = {
  dots: false,
  infinite: true,
  speed: 500,
  slidesToShow: 1,
  slidesToScroll: 1,
  autoplay: true,
  autoplaySpeed: 5000,
};

const EnhanceSlider: FunctionComponent<propsInterface> = (props) => {
  const { className } = props;

  return (
    <div className={b({ mix: className })}>
      <Slider className={b('slider')} {...settings}>
        {[slide1, slide2].map((item) => (
          <div className={b('slide')}>
            <div className={b('image')}>
              <Image src={item} alt="slide" layout="fill" />
            </div>
          </div>
        ))}
      </Slider>
    </div>
  );
};

EnhanceSlider.defaultProps = defaultProps;

export default EnhanceSlider;
