import React from 'react';
import bem from 'src/utils/bem';
import useMobileStyle from 'src/utils/useMobileStyle';
import PropTypes from 'prop-types';
import { closingQuote, openingQuote } from 'public';
import { connect } from 'react-redux';
import LazyLoad from 'react-lazyload';
import dynamic from 'next/dynamic';
import { isMobileDeviceSelector } from 'src/redux/auth/selectors';
import TERMS from './constants';
import styles from './index.module.scss';

const NewInfoWidgetCard = dynamic(() => import('src/components/cards/NewInfoWidgetCard'));
const ColorButton = dynamic(() => import('src/components/buttons/ColorButton'));
const PolaroidImageSlider = dynamic(() => import('src/components/widgets/PolaroidImageSlider'));

const b = bem('new-info-widget', styles);

const { TITLE, DESCRIPTION } = TERMS;

const propTypes = {
  title: PropTypes.string,
  description: PropTypes.string,
  isRight: PropTypes.bool,
  isButton: PropTypes.bool,
  cardImage: PropTypes.shape({}),
  cardImageMob: PropTypes.shape({ src: PropTypes.string }),
  cardBackgroundStripeColor: PropTypes.string,
  cardBackgroundSecondStripeColor: PropTypes.string,
  clickHandler: PropTypes.func,
  buttonText: PropTypes.string,
  isLeftStripe: PropTypes.bool,
  leftStripeColor: PropTypes.string,
  isRightStripe: PropTypes.bool,
  rightStripeColor: PropTypes.string,
  leftStripeStyle: PropTypes.string,
  withoutLeftSquare: PropTypes.bool,
  rightStripeStyle: PropTypes.string,
  leftBigSquareStyle: PropTypes.shape({}),
  leftSmallSquareStyle: PropTypes.shape({}),
  withQuotes: PropTypes.bool,
  reference: PropTypes.string,
  mission: PropTypes.bool,
  withSlider: PropTypes.bool,
  imageList: PropTypes.arrayOf(PropTypes.shape({})),
  sliderWithBackStripes: PropTypes.bool,
  isMobileDevice: PropTypes.bool,
  id: PropTypes.string,
  withoutLeftBigSquare: PropTypes.bool,
  withoutLeftSmallSquare: PropTypes.bool,
  buttonBackGroundColor: PropTypes.string,
  isContactUs: PropTypes.bool,
  contactUsTitle: PropTypes.string,
  redirectLink: PropTypes.string,
  redirectText: PropTypes.string,
  isBigButton: PropTypes.bool,
  className: PropTypes.string,
};

const defaultProps = {
  title: TITLE,
  description: DESCRIPTION,
  isRight: false,
  isButton: false,
  cardBackgroundStripeColor: '',
  cardBackgroundSecondStripeColor: '',
  clickHandler: () => {},
  buttonText: '',
  cardImage: {},
  cardImageMob: {},
  isLeftStripe: false,
  leftStripeColor: '',
  isRightStripe: false,
  rightStripeColor: '',
  leftStripeStyle: '',
  withoutLeftSquare: false,
  rightStripeStyle: '',
  leftBigSquareStyle: {},
  leftSmallSquareStyle: {},
  withQuotes: false,
  reference: '',
  mission: false,
  withSlider: false,
  imageList: [],
  sliderWithBackStripes: false,
  isMobileDevice: true,
  id: '',
  withoutLeftBigSquare: false,
  withoutLeftSmallSquare: false,
  buttonBackGroundColor: '',
  isContactUs: false,
  contactUsTitle: '',
  redirectLink: '',
  redirectText: '',
  isBigButton: false,
  className: '',
};

const NewInfoWidget = (props) => {
  const {
    title,
    description,
    cardImage,
    isButton,
    clickHandler,
    buttonText,
    isRight,
    cardBackgroundStripeColor,
    cardBackgroundSecondStripeColor,
    isLeftStripe,
    leftStripeColor,
    isRightStripe,
    rightStripeColor,
    leftStripeStyle,
    withoutLeftSquare,
    rightStripeStyle,
    leftBigSquareStyle,
    leftSmallSquareStyle,
    withQuotes,
    reference,
    mission,
    cardImageMob,
    withSlider,
    imageList,
    sliderWithBackStripes,
    isMobileDevice,
    id,
    withoutLeftBigSquare,
    withoutLeftSmallSquare,
    buttonBackGroundColor,
    isContactUs,
    contactUsTitle,
    redirectLink,
    redirectText,
    isBigButton,
    className,
  } = props;


  const  mpbImage = cardImageMob && cardImageMob.src ? cardImageMob : cardImage;

  const isMobile = useMobileStyle(1024, isMobileDevice);
  return (
    <div className={b({ mix: className, 'mission-page': mission, 'is-right': isRight })} id={id}>
      {!withoutLeftSquare && <div className={b('left-square', { 'is-right': isRight, mission })} />}
      <div className={b('wrapper', { 'is-right': isRight, 'with-quotes': withQuotes })}>
        {isLeftStripe && (
          <>
            <div
              className={b('left-stripe')}
              style={{
                backgroundColor: leftStripeColor,
                ...leftStripeStyle,
              }}
            />
            {withoutLeftBigSquare ? null : (
              <div className={b('left-square-big')} style={leftBigSquareStyle} />
            )}
            {withoutLeftSmallSquare ? null : (
              <div className={b('left-square-small')} style={leftSmallSquareStyle} />
            )}
          </>
        )}
        {isRightStripe && (
          <>
            <div
              className={b('right-stripe')}
              style={{
                backgroundColor: rightStripeColor,
                ...rightStripeStyle,
              }}
            />
            <div className={b('right-square-small')} />
            <div className={b('right-square-big')} />
          </>
        )}
        <div className={b('main-text-card')}>
          <LazyLoad offset={100}>
            {withSlider ? (
              <PolaroidImageSlider
                slideList={imageList}
                lazyLoad
                cardBackgroundStripeColor={cardBackgroundStripeColor}
                cardBackgroundSecondStripeColor={cardBackgroundSecondStripeColor}
                isRight={isRight}
                sliderWithBackStripes={sliderWithBackStripes}
                withStrips
              />
            ) : (
              <NewInfoWidgetCard
                cardBackgroundStripeColor={cardBackgroundStripeColor}
                cardBackgroundSecondStripeColor={cardBackgroundSecondStripeColor}
                cardImage={isMobile ? mpbImage : cardImage}
                isRight={isRight}
                title={title}
                description={description}
                isButton={isButton}
                buttonText={buttonText}
                clickHandler={clickHandler}
                mission={mission}
              />
            )}
          </LazyLoad>
        </div>
        <div
          className={b('text-container', {
            'is-right': isRight,
            'block-with-quotes': withQuotes,
            'right-on-mission-page': mission && !isRight,
          })}
        >
          {withQuotes && (
            <img
              className={b('top-quotes', { mission })}
              src={closingQuote.src}
              alt="Quote"
            />
          )}
          <h2 className={b('text-title')}>{title}</h2>
          <div
            className={b('text-card', {
              'is-right': isRight,
              'block-with-quotes': withQuotes,
              'right-on-mission-page': mission,
            })}
          >
            <LazyLoad offset={100}>
              {withSlider ? (
                <PolaroidImageSlider
                  slideList={imageList}
                  lazyLoad
                  cardBackgroundStripeColor={cardBackgroundStripeColor}
                  cardBackgroundSecondStripeColor={cardBackgroundSecondStripeColor}
                  isRight={isRight}
                  sliderWithBackStripes={sliderWithBackStripes}
                  withStrips
                />
              ) : (
                <NewInfoWidgetCard
                  cardBackgroundStripeColor={cardBackgroundStripeColor}
                  cardBackgroundSecondStripeColor={cardBackgroundSecondStripeColor}
                  cardImage={cardImage}
                  isRight={isRight}
                  title={title}
                  description={description}
                  isButton={isButton}
                  buttonText={buttonText}
                  clickHandler={clickHandler}
                  mission={mission}
                />
              )}
            </LazyLoad>
          </div>
          <div className={b('text-description', { 'block-with-quotes': withQuotes })}>
            {description}
          </div>
          {withQuotes && <span className={b('reference')}>{reference}</span>}
          {withQuotes && (
            <img
              className={b('bottom-quotes', { mission })}
              src={openingQuote.src}
              alt="Quote"
            />
          )}
          {isButton && (
            <div className={b('btn', { isBig: isBigButton })}>
              <ColorButton
                className={b('btn', { isBig: isBigButton })}
                text={buttonText}
                onClick={clickHandler}
                backGroundColor={buttonBackGroundColor}
              />
            </div>
          )}
          {isContactUs ? (
            <div className={b('contact-us-block')}>
              <div className={b('contact-us-title')}>{contactUsTitle}</div>
              <a className={b('contact-us-url')} href={redirectLink}>
                {redirectText}
              </a>
            </div>
          ) : null}
        </div>
      </div>
    </div>
  );
}

NewInfoWidget.propTypes = propTypes;
NewInfoWidget.defaultProps = defaultProps;

const stateProps = (state) => ({
  isMobileDevice: isMobileDeviceSelector(state),
});

export default connect(stateProps, null)(NewInfoWidget);
