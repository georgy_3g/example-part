const MAX_SIZE = 600;
const CROP = 'Crop';
const ROTATE = 'Rotate';
const PREVIEW = 'Preview';
const LANDSCAPE = 'Landscape';
const PORTRAIT = 'Portrait';
const RESET = 'Reset';
const HTTPS_REGEXP = /^https:\/\//i;
const HTTP = 'http://';
const INFO_TEXT = 'Click and drag and use slider on the right';
const HUNDRED = 100;
const OK_TOGGLE = 'Editor view';
const OFF_TOGGLE = 'Preview';
const EDITOR = 'Editor';
const DONE = 'Done';
const CANCEL = 'Cancel';
const UPLOAD_PHOTO_TEXT = 'Upload your photo\nto get started';
const ABOUT = 'About';

const personalizeList = [
  {
    framePersonalizeOptions: {
      height: 100,
      width: 100,
      x: 0,
      y: 0,
    },
    framePersonalizeUri: '',
    personalizeDisplayDate: {
      RotateX: 0,
      RotateY: 0,
      RotateZ: 0,
      Scale: 20,
      ScaleX: 20,
      Top: 40,
      Left: 40,
      backgroundLeft: -30,
      backgroundScale: 200,
      backgroundTop: -30,
    },
  },
];

export {
  MAX_SIZE,
  CROP,
  ROTATE,
  PREVIEW,
  LANDSCAPE,
  PORTRAIT,
  RESET,
  HTTPS_REGEXP,
  HTTP,
  INFO_TEXT,
  HUNDRED,
  OK_TOGGLE,
  OFF_TOGGLE,
  EDITOR,
  DONE,
  CANCEL,
  UPLOAD_PHOTO_TEXT,
  ABOUT,
  personalizeList,
};
