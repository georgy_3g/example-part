import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import debounce from 'lodash/debounce';
import bem from 'src/utils/bem';
import rotateImageFile from 'src/utils/rotateImageFile';
import checkQuality from 'src/utils/checkQuality';
import generateRotateStyle from 'src/utils/generateRotateStyle';
import { IMAGE_TYPE_JPEG, ORIENTATION_TYPES } from 'src/constants';
import ReactDOM from 'react-dom';
import Colors from 'src/styles/colors.json';
import PhotoArtPreviewPopup from 'src/components/popups/PhotoArtPreviewPopup';
import HelpCenterFrameCard from 'src/components/cards/HelpCenterFrameCard';
import dynamic from 'next/dynamic';
import {
  isShowPersonalizeSelector,
  optionsSelector,
  selectedPersonalizeSelector,
} from 'src/redux/photoArt/selectors';
import { projectDetailSelector } from 'src/redux/projects/selectors';
import {
  addCropedImageAction,
  togglePersonalizePopupAction,
} from 'src/redux/photoArt/actions';
import { clearApprovedImageUrl } from 'src/redux/approvedOrderImage/actions';
import { isGoodPhotoAction } from 'src/redux/shared/actions';
import {
  ABOUT,
  CANCEL,
  CROP,
  DONE,
  EDITOR,
  HUNDRED,
  INFO_TEXT,
  LANDSCAPE,
  PORTRAIT,
  PREVIEW,
  ROTATE,
  UPLOAD_PHOTO_TEXT,
} from './constants';
import styles from './index.module.scss';

const RotateIcon = dynamic(() => import('src/components/svg/RotateIcon'));
const CustomRangeInput = dynamic(() => import('src/components/inputs/CustomRangeInput'));
const Toggler = dynamic(() => import('src/components/elements/Toggler'));
const OrientationIcon = dynamic(() => import('src/components/svg/OrientationIcon'));
const ArrowIcon = dynamic(() => import('src/components/svg/ArrowIcon'));
const CropIcon2 = dynamic(() => import('src/components/svg/CropIcon2'));
const SelectPhotoIcon = dynamic(() => import('src/components/svg/SelectPhotoIcon'));
const SelectCollectionIcon = dynamic(() => import('src/components/svg/SelectCollectionIcon'));
const HintIcon = dynamic(() => import('src/components/svg/HintIcon'));
const CloseIcon2 = dynamic(() => import('src/components/svg/CloseIcon2'));

const grayColor = Colors['$chateau-gray-color'];
const lightSlateGrayColor = Colors['$light-slate-gray-colorr'];

const b = bem('photo-art-constructor', styles);
const keys = {
  37: 1,
  38: 1,
  39: 1,
  40: 1,
};

const defaultProps = {
  className: '',
  photo: {},
  frame: {},
  options: {},
  stateOptions: {},
  addCropedImage: () => {  },
  clearApprovedImage: () => {  },
  changeOrientationType: () => {  },
  changePhotoStatus: () => {  },
  orientationType: ORIENTATION_TYPES.horizontal,
  updateSpinnerStatus: () => {  },
  useStateOptions: false,
  getConstructorMethods: () => {  },
  projectId: null,
  projectDetail: {},
  updateProject: () => {  },
  constructorWrapClass: '',
  selectedPersonalize: null,
  isShowPersonalize: false,
  personalizeText: '',
  togglePersonalizePopup: () => {  },
};

const propTypes = {
  className: PropTypes.string,
  clearApprovedImage: PropTypes.func,
  photo: PropTypes.shape({
    imageUri: PropTypes.string,
    id: PropTypes.number,
  }),
  frame: PropTypes.shape({
    photoArtImages: PropTypes.arrayOf(PropTypes.shape({})),
    id: PropTypes.number,
    framePhotoPosition: PropTypes.shape({}),
    framePhotoArtMatting: PropTypes.shape({}),
    options: PropTypes.arrayOf(PropTypes.shape({})),
  }),
  options: PropTypes.shape({
    displayValue: PropTypes.shape({}),
    selectedOption: PropTypes.shape({}),
    selectedMatting: PropTypes.shape({}),
    colorValue: PropTypes.shape({}),
    sizeValue: PropTypes.shape({}),
  }),
  stateOptions: PropTypes.shape({
    displayValue: PropTypes.shape({}),
    selectedOption: PropTypes.shape({}),
    selectedMatting: PropTypes.shape({}),
    colorValue: PropTypes.shape({}),
    sizeValue: PropTypes.shape({}),
  }),
  addCropedImage: PropTypes.func,
  changeOrientationType: PropTypes.func,
  changePhotoStatus: PropTypes.func,
  orientationType: PropTypes.string,
  updateSpinnerStatus: PropTypes.func,
  useStateOptions: PropTypes.bool,
  getConstructorMethods: PropTypes.func,
  projectId: PropTypes.number,
  projectDetail: PropTypes.shape({}),
  updateProject: PropTypes.func,
  constructorWrapClass: PropTypes.string,
  selectedPersonalize: PropTypes.shape({}),
  isShowPersonalize: PropTypes.bool,
  personalizeText: PropTypes.string,
  togglePersonalizePopup: PropTypes.func,
};


class PhotoArtConstructor extends Component {
  resizeWindow = debounce(() => {
    this.onResizeConstructor();
  }, 300);

  constructor(props) {
    super(props);

    const {
      useStateOptions,
      projectId,
      projectDetail: {
        data: {
          constructorStateDate: { zoomValue, photoStyles } = {},
        } = {},
      } = {},
    } = props;

    this.frame = null;
    this.wrap = null;
    this.canvas = null;
    this.photoWrap = null;
    this.imageToUpload = null;
    this.idMouseDown = false;
    this.coordinates = null;
    this.withOutAutoRotate = false;
    this.firstLoad = false;
    this.navigation = null;
    this.wheelOpt = null;
    this.wheelEvent = null;
    this.photoIsLoad = false;
    this.optionIsLoad = false;
    this.controlRowRef = null;
    this.lastOrientationData = {};

    this.optionsKey = useStateOptions ? 'stateOptions' : 'options';

    this.state = {
      photoStyles: projectId ? photoStyles : {},
      oldPhotoStyles: projectId ? photoStyles : {},
      startPhotoStyle: {},
      rotatedImage: {},
      isShowPreview: false,
      art: null,
      zoomValue: projectId ? zoomValue : 100,
      oldZoomValue: projectId ? zoomValue : 100,
      isEditMode: false,
      correctionFactor: 1,
      isHintOpen: false,
    };
  }

  componentDidMount() {
    document.addEventListener('mouseup', this.onMouseMove(true));
    document.addEventListener('touchend', this.onMouseMove(true));
    window.addEventListener('resize', this.resizeWindow);
    this.navigation = document.querySelector('#navigation');
    this.windowWidth = window.innerWidth;
    this.body = document.body;
    const {
      photo,
      [this.optionsKey]: { displayValue },
      getConstructorMethods,
      projectId,
    } = this.props;
    this.firstLoad = !projectId;
    getConstructorMethods({ getStateData: this.getStateData });
    if (
      photo
      && Object.keys(photo)
      && displayValue
      && Object.keys(displayValue)
      && !projectId
    ) {
      const flags = {
        fromBack: true,
        isNewPhoto: true,
      };
      this.addPhoto(photo, flags);
    }

    if (projectId) {
      this.setPhotoFromProject(photo);
    }

    let supportsPassive = false;
    try {
      window.addEventListener('test', null, Object.defineProperty({}, 'passive', {
        // eslint-disable-next-line getter-return
        get: () => { supportsPassive = true; },
      }));
    } catch (e) {
      supportsPassive = false;
    }

    this.wheelOpt = supportsPassive ? { passive: false } : false;
    this.wheelEvent = 'onwheel' in document.createElement('div') ? 'wheel' : 'mousewheel';
  }

  componentDidUpdate(prevProps) {
    const {
      photo: prevPhoto,
      [this.optionsKey]: {
        selectedMatting: prevSelectedMatting,
        displayValue: prevDisplayValue,
        colorValue: prevColorValue,
        sizeValue: prevSizeValue,
      },
      frame: { id: prevFrameId },
    } = prevProps;
    const {
      photo,
      [this.optionsKey]: {
        selectedMatting,
        displayValue,
        colorValue,
        sizeValue,
      },
      frame: { id: frameId },
    } = this.props;

    if (
      this.lastOrientationData
      && Object.keys(this.lastOrientationData).length
      && (
        prevColorValue.id !== colorValue.id
        || prevSizeValue.id !== sizeValue.id
        || frameId !== prevFrameId
      )
    ) {
      this.lastOrientationData = {};
    }

    const { value: prevMatValue } = prevSelectedMatting || {};
    const { value: matValue } = selectedMatting || {};

    const isFirstDisplay = displayValue && Object.keys(displayValue)
      && (!prevDisplayValue || !Object.keys(prevDisplayValue).length);

    const isDisplayUpdate = isFirstDisplay
      || (
        displayValue && Object.keys(displayValue).length
          && displayValue.id !== prevDisplayValue.id
      );

    const firstPhoto = (this.firstLoad || !prevPhoto || !Object.keys(prevPhoto).length)
      && photo && Boolean(Object.keys(photo).length);

    const isPhotoUpdate = firstPhoto
      || (photo && Object.keys(photo).length && photo.id !== prevPhoto.id);

    const isMatOn = !prevMatValue && matValue;
    const isMatOff = prevMatValue && !matValue;

    if (
      (isDisplayUpdate && Boolean(photo && Object.keys(photo).length))
      || (isPhotoUpdate && displayValue && Object.keys(displayValue))
      || ((isMatOn || isMatOff) && photo && Object.keys(photo).length && photo.id)
    ) {
      const flags = {
        fromBack: true,
        isNewPhoto: true,
        isDisplayUpdate,
        withOutAutoRotateFlag: this.withOutAutoRotate || frameId === prevFrameId && !isPhotoUpdate,
      };
      this.addPhoto(photo, flags);
    }
  }

  componentWillUnmount() {
    document.removeEventListener('mouseup', this.onMouseMove(true));
    document.removeEventListener('touchend', this.onMouseMove(true));
    window.removeEventListener('resize', this.resizeWindow);
    this.frame = null;
    this.wrap = null;
    this.canvas = null;
    this.photoWrap = null;
    this.imageToUpload = null;
    this.coordinates = null;
    this.firstLoad = false;
    this.navigation = null;
    this.wheelOpt = null;
    this.wheelEvent = null;
    this.controlRowRef = null;
    this.coordinates = null;
    this.windowWidth = null;
    this.body = null;
    this.lastOrientationData = null;
  }

  getStateData = () => {
    const { zoomValue, photoStyles, rotatedImage } = this.state;
    return { zoomValue, photoStyles, rotatedImage };
  }

  preventDefault = (e) => {
    e.preventDefault();
  }

  // eslint-disable-next-line consistent-return
  preventDefaultForScrollKeys = (e) => {
    if (keys[e.keyCode]) {
      this.preventDefault(e);
      return false;
    }
  }

  onResizeConstructor = () => {
    const { photo, [this.optionsKey]: { displayValue } } = this.props;
    if (
      photo
      && Object.keys(photo)
      && displayValue
      && Object.keys(displayValue)
      && this.windowWidth !== window.innerWidth
    ) {
      const flags = {
        fromBack: true,
        isNewPhoto: false,
      };
      this.windowWidth = window.innerWidth;
      this.addPhoto(photo, flags);
    }
  };

  updateOrientationType = (type, withOutAutoRotate) => {
    this.withOutAutoRotate = withOutAutoRotate;
    const { photoStyles } = this.state;
    const {
      changeOrientationType,
      frame: { options: frameOptions = [] },
      [this.optionsKey]: { colorValue, sizeValue, displayValue },
      updateSpinnerStatus,
      orientationType,
    } = this.props;

    const newOrientationType = type ? ORIENTATION_TYPES.vertical : ORIENTATION_TYPES.horizontal;
    if (frameOptions.find(({ frameOrientation, frameSize, frameColor }) => (
      (
        frameOrientation === newOrientationType
        && colorValue.id === frameColor.id
        && sizeValue.value.id === frameSize.id
      )
      || (
        frameOrientation === newOrientationType
        && sizeValue.value.id === frameSize.id
      )
      || (
        frameOrientation === newOrientationType
      )
    ))) {
      this.lastOrientationData[orientationType] = {
        photoStyles,
        displayId: displayValue.id,
      };
      changeOrientationType(newOrientationType);
      this.optionIsLoad = true;
      updateSpinnerStatus(this.photoIsLoad, this.optionIsLoad);
    }
  }

  setPhotoFromProject = (image) => {
    const photoImg = new Image();
    const { photoStyles } = this.state;

    photoImg.onload = () => {
      if (photoImg) {
        this.imageToUpload = photoImg;
        this.getFileToUpload(
          photoStyles,
          false,
          true,
        );
      }
    };
    photoImg.setAttribute('crossOrigin', 'anonymous');
    photoImg.src = `${image.imageUri}?v=0.111`;
  }

  addPhoto = (photo, flags) => {
    const { isNewPhoto, isDisplayUpdate, withOutAutoRotateFlag } = flags;
    const { correctionFactor } = this.state;
    const {
      clearApprovedImage,
      [this.optionsKey]: { selectedMatting },
      frame,
      [this.optionsKey]: options,
      updateSpinnerStatus,
    } = this.props;

    const {
      zoomValue,
      photoStyles: oldStyles,
      startPhotoStyle: oldStartStyles,
    } = this.state;
    const { value: matValue } = selectedMatting || {};
    const { selectedOption: { frameOrientation }, displayValue } = options || {};

    const photoImg = new Image();

    photoImg.onerror = () => {
      clearApprovedImage();
    };

    photoImg.onload = () => {
      if (photoImg && this.photoWrap) {
        const { framePhotoArtMatting } = frame;
        const { clientWidth: wrapWidth, clientHeight: wrapHeight } = this.photoWrap;

        const { paddingVertical, paddingHorizontal } = framePhotoArtMatting || {};

        this.imageToUpload = photoImg;
        const { naturalWidth, naturalHeight } = photoImg;

        const matOffsetLeft = matValue ? wrapWidth * Number(paddingHorizontal) / HUNDRED : 0;
        const matOffsetTop = matValue ? wrapHeight * Number(paddingVertical) / HUNDRED : 0;

        const futureWidth = wrapWidth - matOffsetLeft * 2;
        const futureHeight = wrapHeight - matOffsetTop * 2;

        const deltaWidth = Number(naturalWidth) / (futureWidth);
        const deltaHeight = Number(naturalHeight) / (futureHeight);

        const photoOrientationType = naturalHeight - naturalWidth > 0
          ? ORIENTATION_TYPES.vertical : ORIENTATION_TYPES.horizontal;

        const delta = deltaWidth > deltaHeight ? deltaWidth : deltaHeight;

        let photoWidth = Number(naturalWidth) / delta;
        let photoHeight = Number(naturalHeight) / delta;

        if (photoWidth < futureWidth) {
          const newDelta = naturalWidth / futureWidth;
          photoWidth = futureWidth;
          photoHeight = naturalHeight / newDelta;
        }

        if (photoHeight < futureHeight) {
          const newDelta = naturalHeight / futureHeight;
          photoWidth = naturalWidth / newDelta;
          photoHeight = futureHeight;
        }

        const paddingLeft = (futureWidth - photoWidth) / 2;
        const paddingTop = (futureHeight - photoHeight) / 2;

        const newStyle = {
          top: String((matOffsetTop + paddingTop) / correctionFactor),
          left: String((matOffsetTop + paddingLeft) / correctionFactor),
          width: String(photoWidth / correctionFactor),
          height: String(photoHeight / correctionFactor),
        };

        const photoStyles = this.getValidPosition(newStyle);

        const {
          photoStyles: lastPhotoStyles,
          displayId,
        } = this.lastOrientationData[frameOrientation] || {};

        const newStyles = (
          displayId === displayValue.id
          && lastPhotoStyles
          && Object.keys(lastPhotoStyles).length
        )
          ? lastPhotoStyles
          : this.getNewStyle(photoStyles, oldStyles, oldStartStyles, zoomValue);

        if (photoOrientationType !== frameOrientation && isNewPhoto && !withOutAutoRotateFlag) {
          this.updateOrientationType(photoOrientationType === ORIENTATION_TYPES.vertical);
        } else {
          this.firstLoad = false;
          this.setState(
            {
              photoStyles: newStyles,
              rotatedImage: isNewPhoto ? {} : photo,
              startPhotoStyle: photoStyles,
            },
            () => {
              this.withOutAutoRotate = false;
              this.photoIsLoad = false;
              updateSpinnerStatus(this.photoIsLoad, this.optionIsLoad);
              this.getFileToUpload(
                newStyles,
                false,
              );
            },
          );
        }
      }
    };

    if (photo && photo && Object.keys(photo).length) {
      const { rotateNumber } = photo;

      if (rotateNumber) {
        photoImg.src = photo.imageUri;
      } else {
        photoImg.setAttribute('crossOrigin', 'anonymous');
        photoImg.src = `${photo.imageUri}?v=0.111`;
      }

      if (!this.photoIsLoad && !this.optionIsLoad) {
        this.photoIsLoad = true;
        this.optionIsLoad = isDisplayUpdate;
        updateSpinnerStatus(this.photoIsLoad, this.optionIsLoad);
      }
    }
  }

  getNewStyle = (style, oldStyle, oldStartStyle, zoomValue) => {
    if (
      !oldStyle
      || !Object.keys(oldStyle).length
      || !oldStartStyle
      || !Object.keys(oldStartStyle).length
    ) {
      return style;
    }

    const {
      top,
      left,
      width,
      height,
    } = style || {};

    const {
      top: oldTop,
      left: oldLeft,
      width: oldWidth,
      height: oldHeight,
    } = oldStyle || {};

    const {
      top: oldStartTop,
      left: oldStartLeft,
      width: oldStartWidth,
      height: oldStartHeight,
    } = oldStartStyle || {};

    const sizeScale = Number(width) / Number(oldStartWidth);

    const leftOffset = Number(left) - (Number(oldStartLeft) * sizeScale);
    const topOffset = Number(top) - (Number(oldStartTop) * sizeScale);

    const heightDifference = Number(oldHeight) - Number(oldStartHeight);
    const widthDifference = Number(oldWidth) - Number(oldStartWidth);

    const leftWithoutZoom = (Number(oldLeft) + (widthDifference / 2)) * sizeScale + leftOffset;
    const topWithoutZoom = (Number(oldTop) + (heightDifference / 2)) * sizeScale + topOffset;

    const newWidth = Number(width) * zoomValue / HUNDRED;
    const newHeight = Number(height) * zoomValue / HUNDRED;
    const newTop = Number(topWithoutZoom) - (newHeight / 2) + (Number(height) / 2);
    const newLeft = Number(leftWithoutZoom) - (newWidth / 2) + (Number(width) / 2);

    return this.getValidPosition({
      width: String(newWidth),
      height: String(newHeight),
      top: String(newTop),
      left: String(newLeft),
    });
  }

  onResize = isEnd => (value) => {
    const {
      startPhotoStyle: {
        width,
        height,
      },
      photoStyles: {
        width: oldWidth,
        height: oldHeight,
        left,
        top,
      },
    } = this.state;
    const newWidth = Number(width) * value / HUNDRED;
    const nawHeight = Number(height) * value / HUNDRED;
    const newTop = Number(top) - (nawHeight / 2) + (Number(oldHeight) / 2);
    const newLeft = Number(left) - (newWidth / 2) + (Number(oldWidth) / 2);

    const photoStyles = this.getValidPosition({
      width: String(newWidth),
      height: String(nawHeight),
      top: String(newTop),
      left: String(newLeft),
    });

    if (isEnd) {
      this.setState(
        {
          photoStyles,
          zoomValue: value,
        },
        () => {
          this.getFileToUpload(
            photoStyles,
            false,
          );
        },
      );
    } else {
      this.getFileToUpload(
        photoStyles,
        true,
      );
      this.setState({ zoomValue: value });
    }
  }

  getFileToUpload = (photoStyles, withOutSave, withoutSaveProject) => {
    const { correctionFactor } = this.state;

    const {
      frame,
      [this.optionsKey]: { selectedMatting, selectedOption },
      changePhotoStatus,
      updateProject,
      projectId,
    } = this.props;
    const {
      naturalWidth,
      naturalHeight,
    } = this.imageToUpload;

    const { frameSize: { width: photoFrameWidth, height: photoFrameHeight } } = selectedOption;

    const {
      framePhotoArtMatting,
    } = frame;

    const { clientWidth: wrapWidth, clientHeight: wrapHeight } = this.photoWrap;

    const { paddingVertical, paddingHorizontal } = framePhotoArtMatting || {};
    const { value: matValue } = selectedMatting || {};
    const {
      width,
      height,
      top,
      left,
    } = photoStyles;

    const matOffsetLeft = matValue ? wrapWidth * Number(paddingHorizontal) / HUNDRED : 0;
    const matOffsetTop = matValue ? wrapHeight * Number(paddingVertical) / HUNDRED : 0;
    const x = matOffsetLeft - left * correctionFactor;
    const y = matOffsetTop - top * correctionFactor;
    const scaleX = naturalWidth / (width * correctionFactor);
    const scaleY = naturalHeight / (height * correctionFactor);

    const cropNaturalWidth = (wrapWidth - matOffsetLeft * 2) * scaleX;
    const cropNaturalHeight = (wrapHeight - matOffsetTop * 2) * scaleY;
    this.canvas.width = cropNaturalWidth;
    this.canvas.height = cropNaturalHeight;
    const ctx = this.canvas.getContext('2d');

    const isGoodPhoto = checkQuality(
      cropNaturalHeight,
      cropNaturalWidth,
      photoFrameHeight,
      photoFrameWidth,
    );

    ctx.drawImage(
      this.imageToUpload,
      x * scaleX,
      y * scaleY,
      cropNaturalWidth,
      cropNaturalHeight,
      0,
      0,
      cropNaturalWidth,
      cropNaturalHeight,
    );

    if (!withOutSave) {
      this.photoToPreview = this.canvas.toDataURL(IMAGE_TYPE_JPEG);
      this.dataURLtoFile(this.photoToPreview);
      if (projectId && !withoutSaveProject) {
        updateProject();
      }
    }
    changePhotoStatus(isGoodPhoto);
  };

  dataURLtoFile = (dataURL) => {
    const { addCropedImage } = this.props;
    const blobBin = atob(dataURL.split(',')[1]);
    const array = [];
    for (let i = 0; i < blobBin.length; i += 1) {
      array.push(blobBin.charCodeAt(i));
    }
    const data = new Blob([new Uint8Array(array)], { type: IMAGE_TYPE_JPEG });
    addCropedImage(data);
  };

  toPixels = (data) => {
    const newData = {};
    Object.keys(data).forEach((key) => {
      const item = String(data[key]);
      if (item && item.indexOf('px') === -1) {
        newData[key] = `${item}px`;
      } else {
        newData[key] = item;
      }
    });
    return newData;
  }

  onClickReset = () => {
    const { photo } = this.props;
    const flags = { fromBack: true, isNewPhoto: false };
    this.addPhoto(photo, flags);
  }

  rotateImage = () => {
    const {
      rotatedImage: { imageUri, rotateNumber },
    } = this.state;
    const { photo } = this.props;
    if (rotateNumber >= 3) {
      this.setState(
        { rotatedImage: {} },
        () => { this.onClickReset(); },
      );
    } else {
      const img = new Image();

      img.onload = () => {
        const url = rotateImageFile(img);
        const imageData = {
          imageUri: url,
          rotateNumber: rotateNumber ? rotateNumber + 1 : 1,
          id: photo.id,
        };
        this.addPhoto(imageData, {});
      };

      if (imageUri && rotateNumber) {
        img.src = imageUri;
      } else {
        img.setAttribute('crossOrigin', 'anonymous');
        img.src = `${photo.imageUri}?v=0.111`;
      }
    }
  }

  getCanvasStyle = () => {
    if (!this.photoWrap || !this.wrap) {
      return {};
    }

    const {
      height: wrapStyleHeight,
      width: wrapStyleWidth,
    } = this.getCustomWrapStyle(true);

    const { isEditMode } = this.state;
    const { frame, [this.optionsKey]: { selectedMatting = {} } } = this.props;
    const { value: matValue } = selectedMatting || {};
    const {
      framePhotoArtMatting,
    } = frame;

    const { clientWidth: wrapWidth, clientHeight: wrapHeight } = this.photoWrap;
    const { clientWidth, clientHeight } = this.wrap;

    const { height, width } = this.getCanvasBlockStyle(true);
    const calcWidth = (clientWidth * wrapStyleWidth / HUNDRED) * width / HUNDRED;
    const calcHeight = (clientHeight * wrapStyleHeight / HUNDRED) * height / HUNDRED;

    const elementWidth = isEditMode ? wrapWidth : calcWidth;

    const elementHeight = isEditMode ? wrapHeight : calcHeight;

    const { paddingVertical, paddingHorizontal } = framePhotoArtMatting || {};

    const matOffsetTop = Number(paddingVertical) * elementHeight / HUNDRED;
    const matOffsetLeft = Number(paddingHorizontal) * elementWidth / HUNDRED;
    const newWidth = elementWidth - (matValue ? matOffsetLeft * 2 : 0);
    const newHeight = elementHeight - (matValue ? matOffsetTop * 2 : 0);
    const canvasLeft = matValue ? matOffsetLeft : 0;
    const canvasTop = matValue ? matOffsetTop : 0;

    return {
      left: canvasLeft,
      top: canvasTop,
      width: newWidth,
      height: newHeight,
    };
  }

  getEditModeStyle = () => {
    if (this.wrap) {
      const { clientWidth, clientHeight } = this.wrap;
      const { clientWidth: canvasWidth, clientHeight: canvasHeight } = this.canvas;
      const scale = canvasWidth / canvasHeight;
      const editorHeight = clientHeight - (clientWidth < 728 && window.innerWidth < 1024 ? 0 : 130);
      const editorWidth = editorHeight * scale;

      if (clientWidth * 0.8 < editorWidth) {
        const newWidth = clientWidth * 0.8;
        const newHeight = newWidth * canvasHeight / canvasWidth;

        return {
          width: `${newWidth}px`,
          height: `${newHeight}px`,
          left: '50%',
          top: '50%',
          transform: 'translate(-50%, -50%)',
        };
      }

      return {
        width: `${editorWidth}px`,
        height: `${editorHeight}px`,
        left: '50%',
        top: '50%',
        transform: 'translate(-50%, -50%)',
      };
    }
    return {};
  }

  toggleMode = (isReset) => {
    const {
      isEditMode,
      photoStyles,
      zoomValue,
      oldPhotoStyles,
      oldZoomValue,
    } = this.state;

    const { clientWidth, clientHeight } = this.wrap;
    const { clientWidth: canvasWidth, clientHeight: canvasHeight } = this.canvas;
    const scale = canvasWidth / canvasHeight;
    const editorHeight = clientHeight - (clientWidth < 728 && window.innerWidth < 1024 ? 0 : 130);
    const editorWidth = editorHeight * scale;

    const newWidth = clientWidth * 0.8;

    const newCorrectionFactor = clientWidth * 0.8 < editorWidth
      ? newWidth / canvasWidth
      : editorWidth / canvasWidth;

    this.setState(
      {
        isEditMode: !isEditMode,
        correctionFactor: isEditMode ? 1 : newCorrectionFactor,
        ...(!isEditMode
          ? { oldPhotoStyles: photoStyles, oldZoomValue: zoomValue }
          : {
            oldPhotoStyles: {},
            oldZoomValue: 100,
            photoStyles: isReset ? oldPhotoStyles : photoStyles,
            zoomValue: isReset ? oldZoomValue : zoomValue,
          }
        ),
      },
      () => {
        if (isReset) {
          this.getFileToUpload(oldPhotoStyles, false);
        }
        if (isEditMode && !isReset) {
          this.lastOrientationData = {};
        }
        if (!isEditMode && document.body.clientWidth >= 1024) {
          const element = document.getElementById('total-block');
          const { top } = element.getBoundingClientRect() || {};
          window.scrollTo({
            top: document.documentElement.scrollTop + top - 126,
            behavior: 'smooth',
          });
        }
      },
    );
  }

  showPreview = () => {
    const { isShowPreview } = this.state;
    this.setState({
      isShowPreview: !isShowPreview,
      art: isShowPreview ? null : this.photoToPreview,
    });
  }

  getValidPosition = (photoStyles) => {
    const { correctionFactor } = this.state;
    const {
      width,
      height,
      top,
      left,
    } = photoStyles;
    const {
      left: canvasLeft,
      top: canvasTop,
      width: canvasWidth,
      height: canvasHeight,
    } = this.getCanvasStyle();

    const isLeftValid = Number(canvasLeft) >= Number(left) * correctionFactor;
    const isTopValid = Number(canvasTop) >= Number(top) * correctionFactor;
    const isRightValid = Number(canvasLeft) + Number(canvasWidth)
      <= Number(left) * correctionFactor + Number(width) * correctionFactor;
    const isBottomValid = Number(canvasTop) + Number(canvasHeight)
      <= Number(top) * correctionFactor + Number(height) * correctionFactor;

    const newPhotoStyle = {
      width,
      height,
      top,
      left,
    };

    if (!isLeftValid) {
      newPhotoStyle.left = String(canvasLeft);
    }

    if (!isTopValid) {
      newPhotoStyle.top = String(canvasTop);
    }

    if (!isRightValid) {
      newPhotoStyle.left = String(
        (Number(canvasLeft) + Number(canvasWidth)) / correctionFactor - Number(width),
      );
    }

    if (!isBottomValid) {
      newPhotoStyle.top = String(
        (Number(canvasTop) + Number(canvasHeight)) / correctionFactor - Number(height),
      );
    }

    return newPhotoStyle;
  }

  onMouseDown = (e) => {
    this.idMouseDown = true;
    const { screenX, screenY, touches } = e;
    const {
      screenX: mobScreenX,
      clientY: mobScreenY,
    } = (touches && touches.length) ? touches[0] : { screenX: 0, screenY: 0 };
    this.coordinates = { x: screenX || mobScreenX, y: screenY || mobScreenY };
  }

  onMouseMove = isEnd => (e) => {
    if (this.idMouseDown) {
      const { photoStyles, correctionFactor } = this.state;
      const {
        screenX = 0,
        screenY = 0,
        touches,
        changedTouches,
      } = e;
      const touchesArr = isEnd ? changedTouches : touches;
      const {
        screenX: mobScreenX,
        clientY: mobScreenY,
      } = (touchesArr && touchesArr.length) ? touchesArr[0] : { screenX: 0, screenY: 0 };
      const { x, y } = this.coordinates;
      const { top, left } = photoStyles;
      const newTop = Number(top * correctionFactor)
        - Number(y) + (screenY || Math.round(mobScreenY));
      const newLeft = Number(left * correctionFactor)
        - Number(x) + (screenX || Math.round(mobScreenX));

      const newPhotoStyles = this.getValidPosition({
        ...photoStyles,
        top: String(newTop / correctionFactor),
        left: String(newLeft / correctionFactor),
      });
      if (isEnd) {
        this.idMouseDown = false;
        this.coordinates = null;
        this.setState(
          { photoStyles: newPhotoStyles },
          () => {
            this.getFileToUpload(
              newPhotoStyles,
              false,
            );
          },
        );
      } else {
        this.getFileToUpload(
          newPhotoStyles,
          true,
        );
      }
    }
  }


  disableScroll = () => {
    if (document.body.clientWidth <= 1024) {
      window.addEventListener('DOMMouseScroll', this.preventDefault, false);
      window.addEventListener(this.wheelEvent, this.preventDefault, this.wheelOpt);
      window.addEventListener('touchmove', this.preventDefault, this.wheelOpt);
      window.addEventListener('keydown', this.preventDefaultForScrollKeys, false);
    }
  }

  enableScroll = () => {
    if (document.body.clientWidth <= 1024) {
      window.removeEventListener('DOMMouseScroll', this.preventDefault, false);
      window.removeEventListener(this.wheelEvent, this.preventDefault, this.wheelOpt);
      window.removeEventListener('touchmove', this.preventDefault, this.wheelOpt);
      window.removeEventListener('keydown', this.preventDefaultForScrollKeys, false);
    }
  }

  getCanvasBlockStyle = (isValue) => {
    const {
      [this.optionsKey]: { displayValue },
      selectedPersonalize,
      isShowPersonalize,
    } = this.props;
    const {
      frameDisplayData: {
        frameScale: persFrameScale,
        frameScaleX: persFrameScaleX,
        frameTop: persFrameTop,
        frameLeft: persFrameLeft,
      } = {},
    } = selectedPersonalize || {};
    const { isEditMode } = this.state;
    const { liveDisplay, frameImageOptions, isTable } = displayValue || {};
    const {
      frameLeft,
      frameRotateX,
      frameRotateY,
      frameRotateZ,
      frameScale,
      frameScaleX,
      frameTop,
    } = liveDisplay || {};

    const top = isShowPersonalize ? persFrameTop : frameTop;
    const left = isShowPersonalize ? persFrameLeft : frameLeft;
    const scale = isShowPersonalize ? persFrameScale : frameScale;
    const scaleX = isShowPersonalize ? persFrameScaleX : frameScaleX;

    const {
      height,
      width,
      x,
      y,
    } = frameImageOptions || {};

    if (isValue) {
      return liveDisplay
        ? {
            height: !isEditMode ? height : scale,
            width: !isEditMode ? width : scaleX,
            left: !isEditMode ? x : left,
            top: !isEditMode ? y : top,
          }
      : {};
    }

    return liveDisplay
      ? {
          height: `${!isEditMode ? height : scale}%`,
          width: `${!isEditMode ? width : scaleX}%`,
          left: `${!isEditMode ? x : left}%`,
          top: `${!isEditMode ? y : top}%`,
          ...(!isEditMode && !isTable && !isShowPersonalize
            ? {}
            : { transform: generateRotateStyle(frameRotateX, frameRotateY, frameRotateZ) }),
        }
    : {};
  };

  onLoadDisplay = () => {
    this.optionIsLoad = false;
  }

  calculateHeightFactor = (frameFactor) => {
    const { clientWidth, clientHeight } = this.wrap || {};
    return clientHeight * frameFactor < clientWidth
      ? 95
    : Number(((clientWidth / frameFactor / clientHeight) * 100).toFixed());
  };

  getCustomWrapStyle = (isNumbers) => {
    const {
      [this.optionsKey]: { displayValue },
      selectedPersonalize,
      isShowPersonalize,
    } = this.props;

    const {
      frameDisplayData: {
        frameScale: persFrameScale,
        frameScaleX: persFrameScaleX,
        frameTop: persFrameTop,
        frameLeft: persFrameLeft,
      } = {},
    } = selectedPersonalize || {};

    const { clientWidth, clientHeight } = this.wrap || {};

    const { liveDisplay, isTable } = displayValue || {};

    const {
      frameTop,
      frameLeft,
      frameScale,
      frameScaleX,
    } = liveDisplay || {};

    const top = isShowPersonalize ? persFrameTop : frameTop;
    const left = isShowPersonalize ? persFrameLeft : frameLeft;
    const scale = isShowPersonalize ? persFrameScale : frameScale;
    const scaleX = isShowPersonalize ? persFrameScaleX : frameScaleX;

    const frameFactor = scaleX / scale;

    const calculateHeightFactor = (clientWidth && clientHeight)
      ? this.calculateHeightFactor(frameFactor)
      : 73;

    const newHeightFactor = (clientWidth && clientHeight && window.innerWidth < 768)
      ? calculateHeightFactor : 73;

    const customHeight = (isTable || isShowPersonalize) ? scale : newHeightFactor;
    const customWidth = (isTable || isShowPersonalize) ? scaleX : customHeight * frameFactor;
    const customLeft = (isTable || isShowPersonalize) ? left : 50 - customWidth / 2;
    const customTop = (isTable || isShowPersonalize) ? top : 50 - customHeight / 2;

    if (isNumbers) {
      return {
        height: customHeight,
        width: customWidth,
        left: customLeft,
        top: customTop,
      };
    }

    return {
      height: `${customHeight}%`,
      width: `${customWidth}%`,
      left: `${customLeft}%`,
      top: `${customTop}%`,
    };
  }

  getBackgroundStyle = (data) => {
    const {
      backgroundLeft,
      backgroundScale,
      backgroundTop,
    } = data;

    return {
      width: `${backgroundScale}%`,
      left: `-${backgroundLeft}%`,
      top: `-${backgroundTop}%`,
    };
  }

  toggleHint = (status) => {
    this.setState({ isHintOpen: status });
  }

  clickUploadPhoto = () => {
    const uploadButton = document.getElementById('upload-button');
    if (uploadButton) {
      uploadButton.click();
    }
  }

  clickCropPhoto = () => {
    const uploadButton = document.getElementById('crop-button-id');
    if (uploadButton) {
      uploadButton.click();
    }
  }

  getPersonalizeBlockStyle = () => {
    const {
      selectedPersonalize: {
        personalizeDisplayDate: {
          rotateX,
          rotateY,
          rotateZ,
          scale,
          scaleX,
          top,
          left,
        } = {},
        framePersonalizeUri,
      } = {},
    } = this.props;

    return {
      height: `${scale}%`,
      width: `${scaleX}%`,
      left: `${left}%`,
      top: `${top}%`,
      transform: generateRotateStyle(rotateX, rotateY, rotateZ),
      ...((framePersonalizeUri) ? { backgroundImage: `url(${framePersonalizeUri})` } : {}),
    };
  }

  getPersonalizeTextStyle = () => {
    const {
      selectedPersonalize = {},
      [this.optionsKey]: { displayValue },
      isShowPersonalize,
    } = this.props;

    const { liveDisplay } = displayValue || {};

    const {
      frameScale,
    } = liveDisplay || {};

    const {
      fontColor,
      fontSize,
      fontWeight,
      fontFamily,
      framePersonalizeOptions: {
        height,
        width,
        x,
        y,
      } = {},
      frameDisplayData: {
        frameScale: personalizeScale,
      } = {},
    } = selectedPersonalize || {};

    if (!selectedPersonalize || !Object.keys(selectedPersonalize).length) {
      return {};
    }

    const displayWidth = this.wrap ? document.body.clientWidth : 1920;

    const calcSize = isShowPersonalize ? fontSize : fontSize * (frameScale / personalizeScale);

    let newSize;

    if (displayWidth >= 1920) {
      newSize = `${calcSize}px`;
    } else if (displayWidth < 1920 && displayWidth >= 1024) {
      newSize = `${calcSize / 19.2}vw`;
    } else {
      newSize = `${calcSize / 10.24}vw`;
    }

    const style = {
      ...(fontColor ? { color: fontColor } : {}),
      ...(newSize ? { fontSize: newSize } : {}),
      ...(fontWeight ? { fontWeight } : {}),
      ...(fontFamily ? { fontFamily } : {}),
      height: `${height || 100}%`,
      width: `${width || 100}%`,
      top: `${y || 0}%`,
      left: `${x || 0}%`,
    };
    return style;
  }

  render() {
    const {
      className,
      frame,
      [this.optionsKey]: { displayValue, selectedMatting, selectedOption },
      orientationType,
      photo,
      constructorWrapClass,
      selectedPersonalize,
      isShowPersonalize,
      personalizeText,
      togglePersonalizePopup,
    } = this.props;

    const { liveDisplay, frameImageUri = '', isTable } = displayValue || {};
    const {
      imageUri: displayImageUri,
      framePerspective,
      framePerspectiveOrigin,
      backgroundLeft,
      backgroundScale,
      backgroundTop,
    } = liveDisplay || {};

    const {
      frameDisplayData: {
        backgroundLeft: persBackgroundLeft,
        backgroundScale: persBackgroundScale,
        backgroundTop: persBackgroundTop,
      } = {},
    } = selectedPersonalize || {};

    const isShowPersonalizeBlock = Boolean(selectedPersonalize);

    const backgroundData = {
      backgroundLeft,
      backgroundScale,
      backgroundTop,
    };

    const persBackgroundData = {
      backgroundLeft: persBackgroundLeft,
      backgroundScale: persBackgroundScale,
      backgroundTop: persBackgroundTop,
    };

    const {
      isShowPreview,
      art,
      zoomValue,
      isEditMode,
      isHintOpen,
    } = this.state;

    const {
      hint,
      productHintImages = [],
      hintTitle,
      hintStyles,
      displayName,
    } = frame || {};

    const helpItem = {
      images: productHintImages,
      title: hintTitle || displayName,
      description: hint,
      styles: hintStyles,
    };

    const isPortrait = orientationType === ORIENTATION_TYPES.vertical;

    const { framePhotoArtMatting } = frame || {};

    const { innerType } = framePhotoArtMatting || {};

    const circleMat = innerType === 'circle';

    const { value: matValue } = selectedMatting || {};

    const canvasStyles = (this.wrap && photo && photo.imageUri) ? this.getCanvasStyle() : {};

    const previreBtnDisabled = Boolean(this.imageToUpload);

    const selectInfoText = `Select frame${(!photo || !photo.imageUri) ? ' and upload your photo' : ''}\n to get started`;

    const isDisableCrop = !(frame && frame.id && photo && photo.imageUri);

    return (
      <section className={b({ mix: className })} id="photo-art-constructor">
        <div
          className={b('constructor-wrap', { mix: constructorWrapClass })}
          ref={(e) => { this.wrap = e; }}
          id="photo-art-constructor-wrap"
        >
          {(frame && frame.id)
            ? (
              <>
                {(isTable || isShowPersonalize) && !isEditMode && (
                  <img
                    className={b('table-background')}
                    src={displayImageUri}
                    alt="test"
                    style={
                      this.getBackgroundStyle(
                        isShowPersonalize ? persBackgroundData : backgroundData,
                      )
                    }
                  />
                )}
                <div
                  className={b('constructor-block')}
                  style={{
                    perspective: `${framePerspective + 200}px`,
                    perspectiveOrigin: framePerspectiveOrigin,
                    ...(!isEditMode
                      ? this.getCustomWrapStyle()
                      : {}
                    ),
                  }}
                  ref={(e) => { this.frameWrap = e; }}
                >
                  {isEditMode && <span className={b('edit-mode-title')}>{CROP}</span>}
                  {isEditMode && <spam className={b('drag-info')}>{INFO_TEXT}</spam>}
                  <div
                    className={b('photo-wrap-block')}
                    ref={(e) => { this.photoWrap = e; }}
                    style={isEditMode ? this.getEditModeStyle() : this.getCanvasBlockStyle()}
                  >
                    { (photo && photo.imageUri)
                      ? (
                        <>
                          {(matValue && !isEditMode) && (
                            <div
                              className={b('mat')}
                              style={{ backgroundColor: `#${matValue}` }}
                            />
                          )}
                          <canvas
                            className={b('canvas', { editor: isEditMode })}
                            ref={(e) => { this.canvas = e; }}
                            id="photo-art-canvas"
                            style={isEditMode
                              ? this.getEditModeStyle()
                              : { ...this.toPixels(canvasStyles), borderRadius: (circleMat && matValue && !isEditMode) ? '50%' : '0' }}
                          />
                          {Boolean(matValue && !isEditMode) && (
                            <div
                              className={b('shadow-block')}
                              style={{ ...this.toPixels(canvasStyles), borderRadius: (circleMat && matValue) ? '50%' : '0' }}
                            />
                          )}
                          {isEditMode && (
                            <>
                              <div className={b('first-vertical-line')} />
                              <div className={b('second-vertical-line')} />
                              <div className={b('first-horizontal-line')} />
                              <div className={b('second-horizontal-line')} />
                              <div className={b('border')} />
                            </>
                          )}
                        </>
                      )
                      : (
                        <div className={b('select-photo-info')}>
                          <SelectPhotoIcon className={b('select-photo-info-icon')} />
                          <span className={b('select-photo-info-text')}>{UPLOAD_PHOTO_TEXT}</span>
                        </div>
                      )}
                  </div>
                </div>
                {isEditMode && (
                  <div
                    className={b('move-zone')}
                    style={isEditMode ? this.getEditModeStyle() : this.getCanvasBlockStyle()}
                    onMouseDown={this.onMouseDown}
                    onMouseMove={this.onMouseMove(false)}
                    onTouchStart={this.onMouseDown}
                    onTouchMove={this.onMouseMove(false)}
                    role="button"
                    tabIndex="0"
                  />
                )}
                {!isEditMode && (
                  <>
                    <img
                      className={b('frame')}
                      ref={(e) => { this.frame = e; }}
                      style={this.getCustomWrapStyle()}
                      src={frameImageUri}
                      alt="frame"
                      onLoad={this.onLoadDisplay}
                    />
                    <div
                      className={b('block-upload')}
                      style={{
                        perspective: `${framePerspective}px`,
                        perspectiveOrigin: framePerspectiveOrigin,
                        ...(!isEditMode
                          ? this.getCustomWrapStyle()
                          : {}
                        ),
                      }}
                    >
                      <div
                        className={b('block-upload-onclick')}
                        role="button"
                        tabIndex={0}
                        onClick={photo ? this.clickCropPhoto : this.clickUploadPhoto}
                        style={isEditMode ? this.getEditModeStyle() : this.getCanvasBlockStyle()}
                      />
                    </div>
                    {isShowPersonalizeBlock && (
                      <div
                        className={b('personalize-wrap')}
                        style={{
                          perspective: `${framePerspective}px`,
                          perspectiveOrigin: framePerspectiveOrigin,
                          ...(!isEditMode
                            ? this.getCustomWrapStyle()
                            : {}
                          ),
                        }}
                      >
                        <div
                          className={b('personalize-data-block')}
                          style={this.getPersonalizeBlockStyle() || {}}
                        >
                          <div
                            className={b('personalize-text-data')}
                            style={this.getPersonalizeTextStyle() || {}}
                          >
                            {personalizeText}
                          </div>
                        </div>
                      </div>
                    )}
                  </>
                )}
                {!isShowPersonalize && (
                  <div className={b('control-block')}>
                    {!isEditMode
                      ? (
                        <>
                          <div className={b('toggler-preview-block')}>
                            {!isShowPreview
                              ? (
                                <div className={b('preview-text-block')}>
                                  <span className={b('preview-text')}>{PREVIEW}</span>
                                </div>
                              )
                              : (
                                <div className={b('preview-text-block')}>
                                  <span className={b('preview-back-icon')}>
                                    <ArrowIcon className={b('back-icon')} width="5" height="11" strokeWidth="3" stroke={lightSlateGrayColor} />
                                  </span>
                                  <span className={b('preview-text')}>{EDITOR}</span>
                                </div>
                              )}
                            <Toggler
                              className={b('toggler')}
                              inputClassName={b('toggler-input')}
                              togglerChange={this.showPreview}
                              checked={!isShowPreview}
                              onText=""
                              offText=""
                              disabled={!previreBtnDisabled}
                            />
                          </div>
                          <div className={b('orientation-buttons-block')}>
                            <button
                              className={b('orientation-btn')}
                              type="button"
                              onClick={() => { this.updateOrientationType(!isPortrait, true); }}
                              disabled={!isPortrait}
                            >
                              <div className={b('orientation-icon-wrap')}>
                                <OrientationIcon className={b('orientation-icon', { select: !isPortrait })} />
                              </div>
                              <span className={b('orientation-text', { select: !isPortrait })}>{LANDSCAPE}</span>
                            </button>
                            <button
                              className={b('orientation-btn')}
                              type="button"
                              onClick={() => { this.updateOrientationType(!isPortrait, true); }}
                              disabled={isPortrait}
                            >
                              <div className={b('orientation-icon-wrap', { portrait: true })}>
                                <OrientationIcon className={b('orientation-icon', { portrait: true, select: isPortrait })} />
                              </div>
                              <span className={b('orientation-text', { select: isPortrait })}>{PORTRAIT}</span>
                            </button>
                          </div>
                          <button
                            className={b('crop-button')}
                            type="button"
                            onClick={() => { this.toggleMode(false); }}
                            disabled={isShowPreview || isDisableCrop}
                            id="crop-button-id"
                          >
                            <div className={b('crop-icon-block')}>
                              <CropIcon2 />
                            </div>
                            <span className={b('crop-button-text')}>{CROP}</span>
                          </button>
                          <div
                            className={b('hint-button')}
                            role="button"
                            tabIndex={0}
                            onClick={() => this.toggleHint(true)}
                          >
                            <div className={b('border-hint-icon')}>
                              <HintIcon className={b('hint-icon')} />
                            </div>
                            <p className={b('hint-title')}>{ABOUT}</p>
                          </div>
                        </>
                      )
                      : (
                        <>
                          <button
                            className={b('rotate-button')}
                            type="button"
                            onClick={this.rotateImage}
                            disabled={isShowPreview}
                          >
                            <div className={b('rotate-icon-block')}>
                              <RotateIcon className={b('rotate-button-icon')} />
                            </div>
                            <span className={b('rotate-button-text')}>{ROTATE}</span>
                          </button>
                          <div className={b('range-wrap')}>
                            <CustomRangeInput
                              className={b('range')}
                              value={zoomValue}
                              onChange={this.onResize(false)}
                              onChangeEnd={this.onResize(true)}
                              disabled={isShowPreview}
                              isPhotoArt
                            />
                          </div>
                          <div className={b('save-button-block')}>
                            <button
                              className={b('done-btn')}
                              type="button"
                              onClick={() => { this.toggleMode(false); }}
                            >
                              {DONE}
                            </button>
                            <button
                              className={b('cancel-btn')}
                              type="button"
                              onClick={() => { this.toggleMode(true); }}
                            >
                              {CANCEL}
                            </button>
                          </div>
                        </>
                      )}
                  </div>
                )}
                {isShowPersonalize && (
                  <button className={b('pers-close-btn')} type="button" onClick={() => togglePersonalizePopup(false)}>
                    <CloseIcon2 />
                  </button>
                )}
              </>
            )
            : (
              <div className={b('select-info-block')}>
                <div className={b('select-image-block')}>
                  <SelectCollectionIcon className={b('select-info-icon')} />
                  {(!photo || !photo.imageUri) && <SelectPhotoIcon className={b('select-info-icon')} />}
                </div>
                <span className={b('select-info-text')}>{selectInfoText}</span>
              </div>
            )}
          {isShowPreview && (
            <PhotoArtPreviewPopup
              close={this.closePreview}
              photo={art}
              display={displayValue}
              selectedOption={selectedOption}
              matValue={matValue}
              mat={framePhotoArtMatting}
              portalBlock={this.controlRowRef}
              selectedPersonalize={selectedPersonalize}
              personalizeText={personalizeText}
              isShowPersonalizeBlock={isShowPersonalizeBlock}
            >
              <div className={b('toggler-preview-block')}>
                {!isShowPreview
                  ? (
                    <div className={b('preview-text-block')}>
                      <span className={b('preview-text')}>{PREVIEW}</span>
                    </div>
                  )
                  : (
                    <div className={b('preview-text-block')}>
                      <span className={b('preview-back-icon')}>
                        <ArrowIcon className={b('back-icon')} width="5" height="11" strokeWidth="3" stroke={lightSlateGrayColor} />
                      </span>
                      <span className={b('preview-text')}>{EDITOR}</span>
                    </div>
                  )}
                <Toggler
                  className={b('toggler')}
                  inputClassName={b('toggler-input')}
                  togglerChange={this.showPreview}
                  checked={!isShowPreview}
                  onText=""
                  offText=""
                  disabled={!previreBtnDisabled}
                />
              </div>
            </PhotoArtPreviewPopup>
          )}
          {isEditMode && (
            <button
              className={b('mobile-rotate-button')}
              type="button"
              onClick={this.rotateImage}
              disabled={isShowPreview}
            >
              <RotateIcon className={b('mobile-rotate-icon')} />
            </button>
          )}
        </div>
        <div className={b('mobile-control-block', { edit: isEditMode })}>
          {!isEditMode
            ? (
              <div className={b('not-editor-control')}>
                <div className={b('first-row')} ref={(e) => { this.controlRowRef = e; }}>
                  <div className={b('orientation-mobile-block')}>
                    <div className={b('preview-mobile-block-toggle')}>
                      {!isShowPreview
                        ? (
                          <div className={b('preview-text-block')}>
                            <span className={b('preview-text')}>{PREVIEW}</span>
                          </div>
                        )
                        : (
                          <div className={b('preview-text-block')}>
                            <span className={b('preview-back-icon')}>
                              <ArrowIcon className={b('back-icon')} width="5" height="11" strokeWidth="3" stroke={lightSlateGrayColor} />
                            </span>
                            <span className={b('preview-text')}>{EDITOR}</span>
                          </div>
                        )}
                      <Toggler
                        className={b('toggler')}
                        inputClassName={b('toggler-input')}
                        togglerChange={this.showPreview}
                        checked={!isShowPreview}
                        onText=""
                        offText=""
                        disabled={!previreBtnDisabled}
                      />
                    </div>
                    <button
                      className={b('orientation-mobile-btn')}
                      type="button"
                      onClick={() => { this.updateOrientationType(!isPortrait, true); }}
                      disabled={!isPortrait}
                    >
                      <div className={b('orientation-mobile-icon-wrap')}>
                        <OrientationIcon className={b('orientation-mobile-icon', { select: !isPortrait })} />
                      </div>
                      <span className={b('orientation-mobile-text', { select: !isPortrait })}>{LANDSCAPE}</span>
                    </button>
                    <button
                      className={b('orientation-mobile-btn')}
                      type="button"
                      onClick={() => { this.updateOrientationType(!isPortrait, true); }}
                      disabled={isPortrait}
                    >
                      <div className={b('orientation-mobile-icon-wrap', { portrait: true })}>
                        <OrientationIcon className={b('orientation-mobile-icon', { portrait: true, select: isPortrait })} />
                      </div>
                      <span className={b('orientation-mobile-text', { select: isPortrait })}>{PORTRAIT}</span>
                    </button>
                  </div>
                  <button
                    className={b('mobile-crop-button')}
                    type="button"
                    onClick={() => { this.toggleMode(false); }}
                    disabled={isShowPreview || isDisableCrop}
                  >
                    <CropIcon2 className={b('mobile-crop-icon')} stroke={grayColor} />
                  </button>
                </div>
                <div className={b('toggler-preview-mobile-block')}>
                  {!isShowPreview
                    ? (
                      <div className={b('preview-text-block')}>
                        <span className={b('preview-text')}>{PREVIEW}</span>
                      </div>
                    )
                    : (
                      <div className={b('preview-text-block')}>
                        <span className={b('preview-back-icon')}>
                          <ArrowIcon className={b('back-icon')} width="5" height="11" strokeWidth="3" stroke={lightSlateGrayColor} />
                        </span>
                        <span className={b('preview-text')}>{EDITOR}</span>
                      </div>
                    )}
                  <Toggler
                    className={b('toggler')}
                    inputClassName={b('toggler-input')}
                    togglerChange={this.showPreview}
                    checked={!isShowPreview}
                    onText=""
                    offText=""
                    disabled={!previreBtnDisabled}
                  />
                </div>
              </div>
            )
            : (
              <div className={b('edit-control')}>
                <span className={b('mobile-menu-title')}>{CROP}</span>
                <div className={b('mobile-range-wrap')}>
                  <CustomRangeInput
                    className={b('mobile-range')}
                    value={zoomValue}
                    onChange={this.onResize(false)}
                    onChangeEnd={this.onResize(true)}
                    disabled={isShowPreview}
                    isMobPhotoArt
                  />
                </div>
                <div className={b('mobile-save-button-block')}>
                  <button
                    className={b('mobile-done-btn')}
                    type="button"
                    onClick={() => { this.toggleMode(false); }}
                  >
                    {DONE}
                  </button>
                  <button
                    className={b('mobile-cancel-btn')}
                    type="button"
                    onClick={() => { this.toggleMode(true); }}
                  >
                    {CANCEL}
                  </button>
                </div>
              </div>
            )}
        </div>
        { isHintOpen && (
          ReactDOM.createPortal(
            <HelpCenterFrameCard helpItem={helpItem} closePopUp={() => this.toggleHint(false)} />,
            document.getElementById('__next') || null,
          )
        )}
      </section>
    );
  }
}

PhotoArtConstructor.propTypes = propTypes;
PhotoArtConstructor.defaultProps = defaultProps;

const stateProps = state => ({
  options: optionsSelector(state),
  projectDetail: projectDetailSelector(state),
  selectedPersonalize: selectedPersonalizeSelector(state),
  isShowPersonalize: isShowPersonalizeSelector(state),
});

const actions = {
  addCropedImage: addCropedImageAction,
  clearApprovedImage: clearApprovedImageUrl,
  changePhotoStatus: isGoodPhotoAction,
  togglePersonalizePopup: togglePersonalizePopupAction,
};

export default connect(stateProps, actions)(PhotoArtConstructor);
