import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import bem from 'src/utils/bem';
import DICTIONARY from 'src/constants/dictionary';
import ROUTES from 'src/constants/routes';
import dynamic from 'next/dynamic';
import { reviewsCountSelect, reviewsRateSelect } from 'src/redux/reviewers/selectors';
import styles from './index.module.scss';

const Rate = dynamic(() => import('src/components/elements/Rate'));
const OrderTransferForm = dynamic(() => import('src/components/forms/OrderTransferForm'));

const { TRANSFER_TITLE, REVIEWS } = DICTIONARY;
const b = bem('transfer', styles);
const titleBem = bem('title', styles);

const TRANSFER_DESC = 'Lorem ipsum dolor sit amet consectetur';

const defaultProps = {
  reviewsCount: null,
  sharedRate: null,
  transferType: '',
  transferTitle: '',
  typeProduct: '',
  blockTitle: TRANSFER_TITLE,
  blockDescription: TRANSFER_DESC,
};

const propTypes = {
  reviewsCount: PropTypes.number,
  sharedRate: PropTypes.number,
  transferType: PropTypes.string,
  transferTitle: PropTypes.string,
  typeProduct: PropTypes.string,
  blockTitle: PropTypes.string,
  blockDescription: PropTypes.string,
};

function Transfer(props) {
  const {
    reviewsCount,
    sharedRate,
    transferType,
    transferTitle,
    typeProduct,
    blockTitle,
    blockDescription,
  } = props;
  return (
    <section className={b()}>
      <h3 className={b('title', { mix: titleBem({ main: true }) })}>{blockTitle}</h3>
      <div className={b('container')}>
        <p className={b('text')}>{blockDescription}</p>
        <Rate rate={sharedRate} />
        <a className={b('link', { mix: 'link' })} href={ROUTES.reviews}>
          {`${reviewsCount} ${REVIEWS}`}
        </a>
      </div>
      <OrderTransferForm
        transferType={transferType}
        transferTitle={transferTitle}
        typeProduct={typeProduct}
      />
    </section>
  );
}

Transfer.propTypes = propTypes;
Transfer.defaultProps = defaultProps;

const stateProps = (state) => ({
  reviewsCount: reviewsCountSelect(state),
  sharedRate: reviewsRateSelect(state),
});

export default connect(stateProps)(Transfer);
