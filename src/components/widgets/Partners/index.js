import React from 'react';
import PropTypes from 'prop-types';
import LazyLoad from 'react-lazyload';
import bem from 'src/utils/bem';

import {
  TRUSTED_BY,
  FEATURED_ON,
  DATA_TRUSTED,
  DATA_FEATURED,
} from 'src/constants/partners';
import dynamic from "next/dynamic";
import styles from './index.module.scss';

const UniversalSlider = dynamic(() => import('src/components/widgets/UniversalSlider'));

const b = bem('partners', styles);

const defaultProps = {
  className: '',
};

const propTypes = {
  className: PropTypes.string,
};

function Partners({ className }) {
  return (
    <section className={`${b()} ${className}`}>
      <div className={b('container')}>
        <div className={b('slider')}>
          <h2 className={b('title')}>{TRUSTED_BY}</h2>
          <LazyLoad offset={100}>
            <UniversalSlider
              cards={DATA_TRUSTED}
              cardType="presentationCard"
              fullSHow={2}
              middleShow={2}
              tabletShow={2}
              mediumShow={2}
              smallShow={2}
              miniSHow={1}
              infinite
              withoutArrows
              autoplay
              slidesToScroll={1}
              wrapClass={b('slid')}
              isLazyLoad
            />
          </LazyLoad>
        </div>
        <div className={b('slider')}>
          <h2 className={b('title')}>{FEATURED_ON}</h2>
          <LazyLoad offset={100}>
            <UniversalSlider
              cards={DATA_FEATURED}
              cardType="presentationCard"
              fullSHow={2}
              middleShow={2}
              tabletShow={2}
              mediumShow={2}
              smallShow={2}
              miniSHow={1}
              infinite
              withoutArrows
              autoplay
              slidesToScroll={1}
              wrapClass={b('slid')}
              isLazyLoad
            />
          </LazyLoad>
        </div>
      </div>
    </section>
  );
}

Partners.propTypes = propTypes;
Partners.defaultProps = defaultProps;

export default Partners;
