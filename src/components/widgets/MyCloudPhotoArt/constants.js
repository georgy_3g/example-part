const TERMS = {
  BACK_TO_MY_CLOUD: 'Back to my cloud',
  CLOUD_IMAGES: 'cloudImages',
  SELECT_PREFERRED: 'Description about ',
  CONSTRUCTOR_TEXT: {
    constructorTitle: 'Creating extraordinary \nhas never been so easy.',
    constructorSubtitle: 'Simply upload your photos here to get started designing.',
  },
  SELECT_IMAGE: 'Add photos',
  SLIDER_BOTTOM_TEXT: 'Now choose a photo art from \nthe ones below',
  GO_BACK: 'Go back',
};

export default TERMS;
