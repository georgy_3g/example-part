import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import bem from 'src/utils/bem';
import ROUTES from 'src/constants/routes';
import colors from 'src/styles/colors.json';
import PhotoArtCollections from 'src/components/widgets/PhotoArtCollections';

import dynamic from 'next/dynamic';
import { myCloudAudio, myCloudImages, myCloudVideo } from 'src/redux/myCloud/selectors';
import { getToken } from 'src/redux/auth/selectors';
import { photoArtProductsSelector } from 'src/redux/photoArt/selectors';
import { deleteFileFromCloud } from 'src/redux/myCloud/actions';
import {
  addConvertImageListAction,
  addImageListAction,
} from 'src/redux/approvedOrderImage/actions';
import styles from './index.module.scss';
import TERMS from './constants';

const UniversalSlider = dynamic(() => import('src/components/widgets/UniversalSlider'));
const ArrowIcon = dynamic(() => import('src/components/svg/ArrowIcon'));
const ImageCard = dynamic(() => import('src/components/cards/ImageCard'));

const gray = colors['$light-slate-gray-color'];

const { photoArtEditor } = ROUTES;

const b = bem('my-cloud-photo-art', styles);

const { CLOUD_IMAGES, CONSTRUCTOR_TEXT, GO_BACK } = TERMS;

const { constructorTitle, constructorSubtitle } = CONSTRUCTOR_TEXT;
const isShowSlider = false;

const defaultProps = {
  isShowCloudChange: () => {},
  isComponentShow: false,
  cloudImages: [],
  deleteCloudFile: () => {},
  photoArtProducts: {},
  preselectCollection: null,
  preselectFrame: null,
  clearPreselect: () => {},
  addImageList: () => {},
  addConvertImageList: () => {},
  token: '',
  isProjectTab: false,
};

const propTypes = {
  isShowCloudChange: PropTypes.func,
  isComponentShow: PropTypes.bool,
  cloudImages: PropTypes.arrayOf(PropTypes.shape({})),
  deleteCloudFile: PropTypes.func,
  photoArtProducts: PropTypes.shape({}),
  preselectCollection: PropTypes.number,
  preselectFrame: PropTypes.shape({}),
  clearPreselect: PropTypes.func,
  addImageList: PropTypes.func,
  addConvertImageList: PropTypes.func,
  token: PropTypes.string,
  isProjectTab: PropTypes.bool,
};

class MyCloudPhotoArt extends Component {
  constructor(props) {
    super(props);
    this.ref = null;
    this.block = null;
    this.collectionsBlock = null;
    this.selectCollection = () => {};

    const { cloudImages } = props;

    this.state = {
      selectedImage: cloudImages && cloudImages.length ? cloudImages[0] : null,
      selectedFrame: null,
    };
  }

  componentDidUpdate() {
    const { selectedImage, isComponentShow: oldFlag } = this.state;
    const { cloudImages, preselectFrame, preselectCollection, clearPreselect, isComponentShow } =
      this.props;

    if (!selectedImage && cloudImages && cloudImages.length) {
      this.selectImage(cloudImages[0], true);
    }

    if (selectedImage && !cloudImages && !cloudImages.length) {
      this.selectImage(null, true);
    }

    if (
      selectedImage &&
      cloudImages &&
      cloudImages.length &&
      !cloudImages.find((item) => item.id === selectedImage.id)
    ) {
      this.selectImage(cloudImages[0], true);
    }

    if (!oldFlag && isComponentShow && preselectFrame && preselectCollection) {
      this.selectCollection(preselectCollection);
      this.selectFrame(preselectFrame);
      clearPreselect();
    }
  }

  componentWillUnmount() {
    this.ref = null;
    this.block = null;
    this.collectionsBlock = null;
  }

  scrollToRef = () => {
    if (this.ref) {
      const { offsetHeight = 0, offsetTop = 0 } = this.ref || {};
      const widthWindow = window.innerWidth <= 1024 ? -40 : 50;
      window.scrollTo({
        top: offsetTop + offsetHeight - widthWindow,
        behavior: 'smooth',
      });
    }
  };

  scrollToTop = () => {
    if (this.block) {
      const top = this.block.offsetTop - 90;

      window.scrollTo({
        left: 0,
        top,
        behavior: 'smooth',
      });
    }
  };

  scrollToCollections = () => {
    if (this.collectionsBlock) {
      const top = this.collectionsBlock.offsetTop - 90;

      window.scrollTo({
        left: 0,
        top,
        behavior: 'smooth',
      });
    }
  };

  goToMyCloud = () => {
    const { isShowCloudChange } = this.props;
    isShowCloudChange(false);
  };

  selectImage = (image, withoutScroll) => {
    const { selectedFrame } = this.state;
    if (!selectedFrame && !withoutScroll) {
      this.scrollToCollections();
    }

    this.setState({ selectedImage: image });
  };

  deleteImage = (image) => {
    const { deleteCloudFile } = this.props;
    deleteCloudFile({
      fileType: CLOUD_IMAGES,
      fileId: image.id,
    });
  };

  selectFrame = (value) => {
    this.scrollToTop();
    this.setState({ selectedFrame: value }, this.scrollToTop);
  };

  getSelectCollection = (func) => {
    this.selectCollection = func;
  };

  orderPrints = (value) => {
    const { frameMaterialId, id: frameId } = value;
    const { selectedImage } = this.state;
    const { cloudImages, addImageList, addConvertImageList, token, isProjectTab } = this.props;

    const selectedIndex = cloudImages.findIndex((item) => item.id === selectedImage.id);

    const imageList = [
      cloudImages[selectedIndex],
      ...cloudImages.filter((item, index) => index !== selectedIndex),
    ];
    const newImageList = imageList.map(({ id, name, imageKey, imageUri, orderImageId }) => ({
      id: isProjectTab ? id : orderImageId,
      name,
      imageKey,
      imageUri,
      folderId: isProjectTab ? null : id,
    }));

    const orderImages = newImageList.filter(({ id }) => id);
    const folderImages = newImageList.filter(({ id }) => !id);

    if (
      orderImages &&
      orderImages.length &&
      (!folderImages || !folderImages.length) &&
      selectedImage
    ) {
      addImageList(orderImages);
      document.location.href = `${photoArtEditor}?collection=${frameMaterialId}&frame=${frameId}`;
    } else if (folderImages && folderImages.length && selectedImage) {
      addConvertImageList({
        folderImages,
        orderImages,
        selectedImage,
        token,
        redirectUrl: `${photoArtEditor}?collection=${frameMaterialId}&frame=${frameId}`,
      });
    } else {
      this.scrollToTop();
    }
  };

  saveCollectionRef = (e) => {
    this.collectionsBlock = e;
    this.scrollToRef();
  };

  render() {
    const { selectedImage, selectedFrame } = this.state;
    const { isComponentShow, cloudImages, photoArtProducts } = this.props;

    return (
      <div
        className={b({ isComponentShow })}
        ref={(e) => {
          this.block = e;
        }}
      >
        <button className={b('back-btn')} type="button" onClick={this.goToMyCloud}>
          <span className={b('back-icon-wrap')}>
            <ArrowIcon
              className={b('back-icon')}
              width="9"
              height="20"
              strokeWidth="3"
              stroke={gray}
            />
          </span>
          <span className={b('back-text')}>{GO_BACK}</span>
        </button>
        <div className={b('top-block')}>
          <span className={b('material-selector-title')}>{constructorTitle}</span>
          <div className={b('info-text-block')}>
            <span className={b('start_text')}>{constructorSubtitle}</span>
          </div>
        </div>
        <div
          className={b('slider-wrapper')}
          ref={(e) => {
            this.ref = e;
          }}
        >
          <div className={b('border-wrap')}>
            {isShowSlider && (
              <UniversalSlider
                className={b('photo-slider')}
                imageClass={b('photo-card-image')}
                select={this.selectImage}
                selected={selectedImage ? selectedImage.id : null}
                cards={cloudImages}
                cardType="image"
                fullSHow={5}
                middleSHow={5}
                tabletShow={5}
                mediumShow={3}
                smallShow={3}
                miniSHow={2}
                withoutText
                deleteImageFromSlider={this.deleteImage}
                onImageError={this.deleteImage}
                withDeleteBtn
                withCrop
                isNewArrow
                isResponsiveIcon
              />
            )}
            <div className={b('photo-wrap')}>
              <div className={b('photo-list')}>
                {cloudImages.map((item) => (
                  <ImageCard
                    className={b('photo-card')}
                    data={item}
                    selected={selectedImage ? selectedImage.id : null}
                    withDeleteBtn
                    withCrop
                    withoutText
                    deleteImageFromSlider={this.deleteImage}
                    onImageError={this.deleteDamageImage}
                    select={this.selectImage}
                    isSmallCard
                  />
                ))}
              </div>
            </div>
          </div>
        </div>
        {isComponentShow && Boolean(Object.keys(photoArtProducts).length) && (
          <PhotoArtCollections
            sectionClassName={b('collection-photo-art')}
            selectedPhotos={[selectedImage]}
            selectedFrame={selectedFrame || {}}
            withSelectFunc
            selectFrame={this.orderPrints}
            refFunc={(e) => {
              this.saveCollectionRef(e);
            }}
            getSelectCollection={this.getSelectCollection}
          />
        )}
      </div>
    );
  }
}

MyCloudPhotoArt.propTypes = propTypes;
MyCloudPhotoArt.defaultProps = defaultProps;

const stateProps = (state) => ({
  cloudImages: myCloudImages(state),
  cloudAudio: myCloudAudio(state),
  cloudVideo: myCloudVideo(state),
  photoArtProducts: photoArtProductsSelector(state),
  token: getToken(state),
});

const actions = {
  deleteCloudFile: deleteFileFromCloud,
  addImageList: addImageListAction,
  addConvertImageList: addConvertImageListAction,
};

export default connect(stateProps, actions)(MyCloudPhotoArt);
