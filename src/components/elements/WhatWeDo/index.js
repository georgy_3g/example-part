import React from 'react';
import PropTypes from 'prop-types';
import bem from 'src/utils/bem';
import { HOME_TERMS } from 'src/terms';
import colors from 'src/styles/colors.json';

import dynamic from "next/dynamic";
import styles from './index.module.scss';

const ServiceCard = dynamic(() => import('src/components/cards/ServiceCard'));

const defaultBlue = colors['$iceberg-blue-light-color'];

const b = bem('we-do', styles);

const { WHAT_WE_DO } = HOME_TERMS;

const defaultProps = {
  dataCard: [],
  onClick: () => {},
  dataTitle: WHAT_WE_DO,
  subTitle: '',
  withoutTitleInMobile: false,
  stripeColor: defaultBlue,
  className: '',
  id: '',
  isDigitize: false,
  isEnhance: false,
};

const propTypes = {
  dataCard: PropTypes.arrayOf(PropTypes.shape({})),
  onClick: PropTypes.func,
  dataTitle: PropTypes.string,
  subTitle: PropTypes.string,
  withoutTitleInMobile: PropTypes.bool,
  stripeColor: PropTypes.string,
  className: PropTypes.string,
  id: PropTypes.string,
  isDigitize: PropTypes.bool,
  isEnhance: PropTypes.bool,
};

const WhatWeDo = (props) => {
  const {
    onClick,
    dataCard,
    dataTitle,
    subTitle,
    withoutTitleInMobile,
    stripeColor,
    className,
    id,
    isDigitize,
    isEnhance,
  } = props;

  return (
    <section className={b({ mix: className })} id={id}>
      <div className={b('container')}>
        <h2
          className={b('title', {
            'only-desktop': withoutTitleInMobile,
            'with-subtitle': Boolean(subTitle),
          })}
        >
          {dataTitle}
        </h2>
        {subTitle && <p className={b('sub-title', { mix: 'sub-title', digitize: isDigitize, enhance: isEnhance })}>{subTitle}</p>}
      </div>
      <div className={b('card-container')} id='what-we-do-id'>
        <div className={b('background-stripe')} style={{ backgroundColor: stripeColor }} />
        {dataCard.map((item) => (
          <ServiceCard key={item.title} {...item} onClick={onClick} />
        ))}
      </div>
    </section>
  );
}

WhatWeDo.propTypes = propTypes;
WhatWeDo.defaultProps = defaultProps;

export default WhatWeDo;
