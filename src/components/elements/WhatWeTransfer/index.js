import React from 'react';
import PropTypes from 'prop-types';
import bem from 'src/utils/bem';

import dynamic from 'next/dynamic';
import styles from './index.module.scss';

const WhatTransferCard = dynamic(() => import('src/components/cards/WhatTransferCard'));

const b = bem('what-we-transfer', styles);

const defaultProps = {
  cards: [],
  title: '',
  description: '',
  isDigitize: false,
};

const propTypes = {
  cards: PropTypes.arrayOf(PropTypes.shape({})),
  title: PropTypes.string,
  description: PropTypes.string,
  isDigitize: PropTypes.bool,
};

const WhatWeTransfer = (props) => {
  const {
    cards,
    title,
    description,
    isDigitize,
  } = props;
  return (
    <div className={b({ digitize: isDigitize })}>
      <div className={b('wrapper')}>
        <h2 className={b('title')}>{title}</h2>
        {description && <div className={b('description', { digitize: isDigitize })}>{description}</div>}
        <div className={b('cards')}>
          <div className={b('background-stripe')} />
          {cards.map((item, index) => (
            <WhatTransferCard
              card={item}
              isFirst={index === 0}
              isLast={index === cards.length - 1}
              key={item.cardText}
            />
          ))}
        </div>
      </div>
    </div>
  );
}

WhatWeTransfer.propTypes = propTypes;
WhatWeTransfer.defaultProps = defaultProps;

export default WhatWeTransfer;
