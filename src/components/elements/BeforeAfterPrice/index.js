import React from 'react';
import bem from 'src/utils/bem';
import convertToPrice from 'src/utils/convertToPrice';
import PropTypes from 'prop-types';
import styles from './index.module.scss';

const b = bem('before-after-price', styles);

const propTypes = {
  price: PropTypes.number,
  additionalDiscount: PropTypes.number,
  textBefore: PropTypes.string,
  textAfter: PropTypes.string,
  isRow: PropTypes.bool,
};

const defaultProps = {
  price: 0,
  additionalDiscount: 0,
  textBefore: '',
  textAfter: '',
  isRow: false,
};

const BeforeAfterPrice = (props) => {
  const {
    price,
    additionalDiscount,
    textBefore,
    textAfter,
    isRow,
  } = props;
  return (
    <div className={b('price-wrapper', { column: !isRow})}>
      {textBefore ? <span className={b('price-text')}>{textBefore}</span> : null}
      <div className={b('price-values', { row: isRow })}>
        <span className={b('price', { 'origin-column': !isRow, 'origin-row': isRow, row: isRow })}>
          {convertToPrice(price)}
        </span>
        <span className={b('price', { custom: true, row: isRow  })}>
          {convertToPrice(price - additionalDiscount)}
        </span>
      </div>
      {textAfter ? <span className={b('price-text')}>{textAfter}</span> : null}
    </div>
  )
}

BeforeAfterPrice.propTypes = propTypes;
BeforeAfterPrice.defaultProps = defaultProps;

export default BeforeAfterPrice;
