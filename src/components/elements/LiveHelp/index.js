import React from 'react';
import bem from 'src/utils/bem';
import checkDate from 'src/utils/checkTime';
import ROUTES from 'src/constants/routes';
import PropType from 'prop-types';
import Color from 'src/styles/colors.json';
import Image from 'next/image';

import callUsImg from  'public/img/live-help/callUs.png';
import letsChatImg from  'public/img/live-help/letsChat.png';
import emailUsImg from  'public/img/live-help/emailUs.png';
import seeFAQsImg from  'public/img/live-help/seeFAQs.png';
import dynamic from "next/dynamic";
import styles from './index.module.scss';
import TERMS from './constants';

const ArrowIcon = dynamic(() => import('src/components/svg/ArrowIcon'));

const b = bem('live-help', styles);
const white = Color['$white-color'];
const grey = Color['$slate-gray-color'];

const { faq, contact } = ROUTES;
const {
  BACK,
  TITLE,
  TEXT,
  PHONE,
  PHONE_NUMBER,
  WEEKDAYS,
  WEEKDAYS_TIME,
  HOLIDAY_TIME,
  HOLIDAY,
  CALL_US,
  LETS_CHAT,
  EMAIL_US,
  SEE_FAQS,
  AVAILABLE,
  UNAVAILABLE,
  WEEKDAYS_TIME_START,
  WEEKDAYS_TIME_END,
  WORKING_HOLIDAY,
  HOLIDAY_TIME_START,
  HOLIDAY_TIME_END,
} = TERMS;

const defaultProps = {
  backInMenu: () => {},
};

const propType = {
  backInMenu: PropType.func,
};

const LiveHelp = ({ backInMenu }) => {
  const openChatWidget = () => {
    if (window.fcWidget) {
      window.fcWidget.open();
    }
  };

  const goToConnect = () => {
    document.location.href = contact;
  };

  const isAvailable = checkDate({
    weekDaysTimeStart: WEEKDAYS_TIME_START,
    weekDaysTimeEnd: WEEKDAYS_TIME_END,
    holidaysTimeStart: HOLIDAY_TIME_START,
    holidaysTimeEnd: HOLIDAY_TIME_END,
    workingHoliday: WORKING_HOLIDAY,
  });

  return (
    <div className={b('main-wrapper')}>
      <div tabIndex={0} role="button" onClick={backInMenu} className={b('back-button')}>
        <ArrowIcon fill={white} stroke={grey} className="icon" />
        <p className={b('back-button-text')}>{BACK}</p>
      </div>
      <div className={b('timetable')}>
        <span className={b('title')}>{TITLE}</span>
        <span className={b('text')}>{TEXT}</span>
        <div className={b('phone-date-wrapper')}>
          <div className={b('phone')}>
            <span className={b('phone-date-title')}>{PHONE}</span>
            <span className={b('phone-date')}>{PHONE_NUMBER}</span>
          </div>
          <div className={b('weekdays')}>
            <span className={b('phone-date-title')}>{WEEKDAYS}</span>
            <span className={b('phone-date')}>{WEEKDAYS_TIME}</span>
          </div>
          <div className={b('holiday')}>
            <span className={b('phone-date-title')}>{HOLIDAY}</span>
            <span className={b('phone-date')}>{HOLIDAY_TIME}</span>
          </div>
        </div>
      </div>
      <div className={b('help-types')}>
        <div className={b('help-container')}>
          <div className={b('status-indicator')}>
            <div className={b('indicator', { unavailable: !isAvailable })} />
            <span className={b('status-text', { unavailable: !isAvailable })}>
              {isAvailable ? AVAILABLE : UNAVAILABLE}
            </span>
          </div>
          <button
            className={b('help-image-container', { red: true })}
            disabled={!isAvailable}
            type="button"
            onClick={goToConnect}
          >
            <Image
              className={b('help-image')}
              src={callUsImg}
              alt="Call us!"
              width="100%"
              height="100%"
            />
          </button>
          <button
            className={b('help-btn')}
            type="button"
            disabled={!isAvailable}
            onClick={goToConnect}
          >
            <span className={b('btn-text', { 'call-us': true })}>{CALL_US}</span>
          </button>
        </div>
        <div className={b('help-container')}>
          <div className={b('status-indicator')}>
            <div className={b('indicator', { unavailable: !isAvailable })} />
            <span className={b('status-text', { unavailable: !isAvailable })}>
              {isAvailable ? AVAILABLE : UNAVAILABLE}
            </span>
          </div>
          <button
            className={b('help-image-container', { green: true })}
            onClick={openChatWidget}
            disabled={!isAvailable}
            type="button"
          >
            <Image
              className={b('help-image')}
              src={letsChatImg}
              alt="Let's chat!"
              width="100%"
              height="100%"
            />
          </button>
          <button
            className={b('help-btn')}
            type="button"
            onClick={openChatWidget}
            disabled={!isAvailable}
          >
            <span className={b('btn-text')}>{LETS_CHAT}</span>
          </button>
        </div>
        <div className={b('help-container')}>
          <div className={b('status-indicator')}>
            <div className={b('indicator')} />
            <span className={b('status-text')}>{AVAILABLE}</span>
          </div>
          <a className={b('help-image-container', { yellow: true })} href={contact}>
            <Image
              className={b('help-image')}
              src={emailUsImg}
              alt="Email us"
              width="100%"
              height="100%"
            />
          </a>
          <button className={b('help-btn')} type="button">
            <a className={b('link', { 'email-us': true })} href={contact}>
              {EMAIL_US}
            </a>
          </button>
        </div>
        <div className={b('help-container')}>
          <div className={b('status-indicator')}>
            <div className={b('indicator')} />
            <span className={b('status-text')}>{AVAILABLE}</span>
          </div>
          <a className={b('help-image-container', { blue: true })} href={faq}>
            <Image
              className={b('help-image')}
              src={seeFAQsImg}
              alt="See FAQs"
              width="100%"
              height="100%"
            />
          </a>
          <button className={b('help-btn')} type="button">
            <a className={b('link', { faq: true })} href={faq}>
              {SEE_FAQS}
            </a>
          </button>
        </div>
      </div>
    </div>
  );
}

LiveHelp.defaultProps = defaultProps;
LiveHelp.propTypes = propType;

export default LiveHelp;
