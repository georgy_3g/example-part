import React from 'react';
import useMobileStyle from 'src/utils/useMobileStyle';
import bem from 'src/utils/bem';
import checkDate from 'src/utils/checkTime';
import PropTypes from 'prop-types';
import { isMobileDeviceSelector } from 'src/redux/auth/selectors';
import { promoSectionSelector } from 'src/redux/promo/selectors';
import { connect } from 'react-redux';

import colors from 'src/styles/colors.json';
import ROUTES from 'src/constants/routes';
import successImage from 'public/img/shared/greenSuccess.png';
import dynamic from "next/dynamic";
import constants from '../../../../pagesContent/printShop/contants';
import styles from './index.module.scss';

const PolaroidImageSlider = dynamic(() => import('src/components/widgets/PolaroidImageSlider'));
const RedButton = dynamic(() => import('src/components/buttons/RedButton'));

const defaultColor = colors['$intense-citrine-white-color'];

const {
  LOCAL_FRAME_HOURS,
  LIVE_SATURDAY,
  LIVE_DAYS,
  LIVE_TIME,
  LIVE_TIME_SATURDAY,
  WE_ARE_OPEN,
  ADDRESS,
  GET_DIRECTIONS,
} = constants;

const { contact } = ROUTES;
const b = bem('local-new-banner', styles);

const defaultProps = {
  bannerTitle: '',
  bannerDesc: '',
  bannerImages: [],
  firstStripeColor: defaultColor,
  isMobileDevice: false,
  promoSection: {},
  smallDescriptionMargin: false,
  isPhotoArt: false,
  titleClass: '',
  isAutoPlay: false,
  autoplaySpeed: 4500,
  liveHours: {
    weekDaysTimeStart: 9,
    weekDaysTimeEnd: 18,
    workingHoliday: 6,
    holidaysTimeStart: 10,
    holidaysTimeEnd: 16,
  },
  offlineButtonText: '',
  redirectUrl: '',
  offlineTextTitle: LOCAL_FRAME_HOURS,
};

const propTypes = {
  bannerTitle: PropTypes.string,
  bannerDesc: PropTypes.string,
  bannerImages: PropTypes.arrayOf(PropTypes.shape({})),
  firstStripeColor: PropTypes.string,
  isMobileDevice: PropTypes.bool,
  promoSection: PropTypes.shape({
    id: PropTypes.number,
  }),
  smallDescriptionMargin: PropTypes.bool,
  titleClass: PropTypes.string,
  isPhotoArt: PropTypes.bool,
  isAutoPlay: PropTypes.bool,
  autoplaySpeed: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  liveHours: PropTypes.shape({
    weekDaysTimeStart: PropTypes.number,
    weekDaysTimeEnd: PropTypes.number,
    workingHoliday: PropTypes.number,
    holidaysTimeStart: PropTypes.number,
    holidaysTimeEnd: PropTypes.number,
  }),
  offlineButtonText: PropTypes.string,
  redirectUrl: PropTypes.string,
  offlineTextTitle: PropTypes.string,
};

const LocalNewBanner = (props) => {
  const {
    bannerTitle,
    bannerDesc,
    bannerImages,
    firstStripeColor,
    isMobileDevice,
    promoSection,
    smallDescriptionMargin,
    titleClass,
    isPhotoArt,
    isAutoPlay,
    autoplaySpeed,
    liveHours,
    offlineButtonText,
    redirectUrl,
    offlineTextTitle,
  } = props;

  const isMobileDisplay = useMobileStyle(1024, isMobileDevice);

  const isLiveHours = checkDate(liveHours);

  const redirect = () => {
    document.location.href = redirectUrl;
  }

  return (
    <article
      className={b()}
      style={isMobileDisplay && promoSection.id ? { marginTop: `80px` || 0 } : {}}
    >
      <div className={b('stripe-one')} style={{ backgroundColor: firstStripeColor }} />
      <div className={b('stripe-two')} />
      <div className={b('stripe-three')} />
      <div className={b('stripe-four')} />
      <div className={b('stripe-five')} />
      <div className={b('container')}>
        <div className={b('text-block')}>
          <h1 className={b('title', { mix: titleClass, wrap: isPhotoArt })}>{bannerTitle}</h1>
          <div className={b('slider-block', {
            mobile: true,
            desktop: false,
          })}
          >
            <PolaroidImageSlider
              slideList={bannerImages}
              lazyLoad
              autoplay={isAutoPlay}
              pauseOnHover
              autoplaySpeed={autoplaySpeed || 4500}
            />
          </div>
          <h2 className={b('desc', {
            'small-margin': smallDescriptionMargin,
            'desktop-art': isPhotoArt })}
          >
            {bannerDesc}
          </h2>
          {
            isLiveHours ? (
              <div className={b('online-block')}>
                <div className={b('online-success-block')}>
                  <img className={b('online-success-image')} src={successImage.src} alt="online hours" />
                  <div className={b('online-success-text')}>{WE_ARE_OPEN}</div>
                </div>
                <div className={b('online-address-block')}>
                  <div className={b('online-address')}>{ADDRESS}</div>
                  <div className={b('online-contact')}>
                    <a className={b('contact-url')} href={contact}>{GET_DIRECTIONS}</a>
                  </div>
                </div>
              </div>
            ) : (
              <div className={b('offline-block')}>
                <RedButton
                  onClick={redirect}
                  className={b('scroll-button')}
                  text={offlineButtonText}
                />
                <div className={b('offline-text-block')}>
                  <div className={b('offline-text-title')}>{offlineTextTitle}</div>
                  <div className={b('offline-text-description-block')}>
                    <div className={b('offline-text-description')}>
                      <div className={b('days')}>{LIVE_DAYS}</div>
                      <div className={b('time')}>{LIVE_TIME}</div>
                    </div>
                    <div className={b('offline-text-description')}>
                      <div className={b('days')}>{LIVE_SATURDAY}</div>
                      <div className={b('time')}>{LIVE_TIME_SATURDAY}</div>
                    </div>
                  </div>
                </div>
              </div>
            )
          }
        </div>
        <div className={b('slider-block', {
          mobile: false,
          desktop: isPhotoArt,
        })}
        >
          <PolaroidImageSlider
            slideList={bannerImages}
            lazyLoad
            autoplay={isAutoPlay}
            pauseOnHover
            autoplaySpeed={autoplaySpeed || 4500}
          />
        </div>
      </div>
    </article>
  );
}

LocalNewBanner.propTypes = propTypes;
LocalNewBanner.defaultProps = defaultProps;

const stateProps = (state) => ({
  isMobileDevice: isMobileDeviceSelector(state),
  promoSection: promoSectionSelector(state),
});

export default connect(stateProps, null)(LocalNewBanner);
