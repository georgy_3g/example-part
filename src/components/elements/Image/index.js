import React, { useState, useEffect } from 'react';
import bem from 'src/utils/bem';
import PropTypes from 'prop-types';
import spinner from  'public/img/shared/spinner*******.gif';
import styles from './index.module.scss';


const b = bem('image', styles);
const defaultProps = {
  setIsVertical: () => {},
  className: '',
  imageUri: null,
  addRef: () => {},
  onImageError: () => {},
  onLoadImage: () => {},
  isTemplate: false,
  alt: '',
  backgroundColor: 'none',
};

const propTypes = {
  className: PropTypes.string,
  imageUri: PropTypes.string,
  setIsVertical: PropTypes.func,
  addRef: PropTypes.func,
  onImageError: PropTypes.func,
  onLoadImage: PropTypes.func,
  isTemplate: PropTypes.bool,
  alt: PropTypes.string,
  backgroundColor: PropTypes.string,
};

const ImageComponent = (props) => {
  const {
    className,
    imageUri,
    setIsVertical,
    addRef,
    onImageError,
    onLoadImage,
    isTemplate,
    alt,
    backgroundColor,
  } = props;

  const [isLoaded, setIsLoading] = useState(false);

  useEffect(() => {
    setIsLoading(false);
  }, [imageUri]);

  const onLoad = ({ naturalWidth, naturalHeight }) => {
    setIsLoading(true);
    setIsVertical(naturalWidth < naturalHeight);
    onLoadImage();
  };

  const onError = () => {
    onImageError(imageUri);
  };

  const getRef = (e) => {
    if (!e) {
      return;
    }
    addRef(e);
    e.onload = onLoad;
    e.onerror = onError;
    if (e.complete) {
      onLoad(e);
    }
  };

  return (
    <>
      {!isLoaded && (
        <img
          className={b('spinner', { invisible: isLoaded })}
          src={spinner.src}
          alt="spinner"
          style={{ backgroundColor }}
        />
      )}
      {!isTemplate && (
        <img
          className={b({ mix: className, invisible: !isLoaded })}
          src={imageUri}
          alt={alt}
          ref={getRef}
        />
      )}
    </>
  );
}

ImageComponent.propTypes = propTypes;
ImageComponent.defaultProps = defaultProps;

export default ImageComponent;
