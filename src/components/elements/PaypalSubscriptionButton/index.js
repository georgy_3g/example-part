import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { PAYPAL_CLIENT_ID } from 'src/config';
import getRollbar from 'src/utils/getRollbar';

const rollbar = getRollbar();

const defaultProps = {
  postSubscriptionId: () => {},
  token: '',
  planId: 0,
  paypalPlanId: '0',
};

const propTypes = {
  postSubscriptionId: PropTypes.func,
  token: PropTypes.string,
  planId: PropTypes.number,
  paypalPlanId: PropTypes.string,
};

class PaypalSubscriptionButton extends Component {
  componentDidMount() {
    if (window.paypal) {
      this.renderButton();
    } else {
      this.initButton();
    }
  }

  initButton = () => {
    const { token, postSubscriptionId, planId, paypalPlanId } = this.props;

    const script = document.createElement('script');
    script.src = `https://www.paypal.com/sdk/js?client-id=${PAYPAL_CLIENT_ID}&disable-funding=card,venmo,sepa,eps,bancontact,ideal,giropay,mybank,p24,sofort&vault=true`;
    document.head.appendChild(script);
    script.onload = () => {
      window.paypal
        .Buttons({
          style: {
            color: 'blue',
            shape: 'pill',
            label: 'pay',
          },
          createSubscription: (data, actions) =>
            actions.subscription.create({ plan_id: paypalPlanId }),
          onApprove: (data) => {
            const { subscriptionID } = data;
            const objectData = {
              planId,
              subscriptionId: subscriptionID,
            };
            postSubscriptionId({ token, objectData });
          },
          onError(error) {
            rollbar.error('paypal fail', error);
          },
        })
        .render('#paypal-button-container');
    };
  };

  renderButton = () => {
    const { token, postSubscriptionId, planId, paypalPlanId } = this.props;

    window.paypal
      .Buttons({
        style: {
          color: 'blue',
          shape: 'pill',
          label: 'pay',
        },
        createSubscription: (data, actions) =>
          actions.subscription.create({ plan_id: paypalPlanId }),
        onApprove: (data) => {
          const { subscriptionID } = data;
          const objectData = {
            planId,
            subscriptionId: subscriptionID,
          };
          postSubscriptionId({ token, objectData });
        },
      })
      .render('#paypal-button-container');
  };

  render() {
    return <div id="paypal-button-container" role="button" />;
  }
}

PaypalSubscriptionButton.propTypes = propTypes;
PaypalSubscriptionButton.defaultProps = defaultProps;

export default PaypalSubscriptionButton;
