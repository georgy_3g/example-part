import React, { Component } from 'react';
import PropTypes from 'prop-types';
import PageView from './PageView';
import BreakView from './BreakView';

const propTypes = {
  pageCount: PropTypes.number.isRequired,
  pageRangeDisplayed: PropTypes.number.isRequired,
  marginPagesDisplayed: PropTypes.number.isRequired,
  previousLabel: PropTypes.node,
  previousAriaLabel: PropTypes.string,
  prevRel: PropTypes.string,
  nextLabel: PropTypes.node,
  nextAriaLabel: PropTypes.string,
  nextRel: PropTypes.string,
  breakLabel: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
  hrefBuilder: PropTypes.func,
  onPageChange: PropTypes.func,
  onPageActive: PropTypes.func,
  initialPage: PropTypes.number,
  forcePage: PropTypes.number,
  disableInitialCallback: PropTypes.bool,
  containerClassName: PropTypes.string,
  pageClassName: PropTypes.string,
  pageLinkClassName: PropTypes.string,
  pageLabelBuilder: PropTypes.func,
  activeClassName: PropTypes.string,
  activeLinkClassName: PropTypes.string,
  previousClassName: PropTypes.string,
  nextClassName: PropTypes.string,
  previousLinkClassName: PropTypes.string,
  nextLinkClassName: PropTypes.string,
  disabledClassName: PropTypes.string,
  breakClassName: PropTypes.string,
  breakLinkClassName: PropTypes.string,
  extraAriaContext: PropTypes.string,
  ariaLabelBuilder: PropTypes.string,
  eventListener: PropTypes.string,
};

const defaultProps = {
  activeClassName: 'selected',
  previousLabel: 'Previous',
  previousClassName: 'previous',
  previousAriaLabel: 'Previous page',
  prevRel: 'prev',
  nextLabel: 'Next',
  nextClassName: 'next',
  nextAriaLabel: 'Next page',
  nextRel: 'next',
  breakLabel: '...',
  disabledClassName: 'disabled',
  disableInitialCallback: false,
  pageLabelBuilder: (page) => page,
  eventListener: 'onClick',
  hrefBuilder: null,
  onPageChange: () => {},
  onPageActive: () => {},
  initialPage: 0,
  forcePage: 0,
  containerClassName: '',
  pageClassName: '',
  pageLinkClassName: '',
  activeLinkClassName: '',
  previousLinkClassName: '',
  nextLinkClassName: '',
  breakClassName: '',
  breakLinkClassName: '',
  extraAriaContext: '',
  ariaLabelBuilder: '',
};

class PaginationBoxView extends Component {
  constructor(props) {
    super(props);
    const { initialPage, forcePage } = props;

    this.state = {
      selected: initialPage || forcePage || 0,
    };
  }

  componentDidMount() {
    const { initialPage, disableInitialCallback, extraAriaContext } = this.props;

    if (typeof initialPage !== 'undefined' && !disableInitialCallback) {
      this.callCallback(initialPage);
    }

    if (extraAriaContext) {
      console.warn(
        'DEPRECATED (paginate): The extraAriaContext prop is deprecated. You should now use the ariaLabelBuilder instead.',
      );
    }
  }

  componentDidUpdate(prevProps) {
    const { forcePage } = this.props;
    if (typeof forcePage !== 'undefined' && forcePage !== prevProps.forcePage) {
      this.updateSelected(forcePage);
    }
  }

  getPageElement(index) {
    const { selected } = this.state;
    const {
      pageClassName,
      pageLinkClassName,
      activeClassName,
      activeLinkClassName,
      extraAriaContext,
      pageLabelBuilder,
    } = this.props;

    return (
      <PageView
        key={index}
        pageSelectedHandler={(e) => {
          this.handlePageSelected(index, e);
        }}
        selected={selected === index}
        pageClassName={pageClassName}
        pageLinkClassName={pageLinkClassName}
        activeClassName={activeClassName}
        activeLinkClassName={activeLinkClassName}
        extraAriaContext={extraAriaContext}
        href={this.hrefBuilder(index)}
        ariaLabel={this.ariaLabelBuilder(index)}
        page={index + 1}
        pageLabelBuilder={pageLabelBuilder}
        getEventListener={this.getEventListener}
      />
    );
  }

  getForwardJump() {
    const { selected } = this.state;
    const { pageCount, pageRangeDisplayed } = this.props;

    const forwardJump = selected + pageRangeDisplayed;
    return forwardJump >= pageCount ? pageCount - 1 : forwardJump;
  }

  getBackwardJump() {
    const { selected } = this.state;
    const { pageRangeDisplayed } = this.props;

    const backwardJump = selected - pageRangeDisplayed;
    return backwardJump < 0 ? 0 : backwardJump;
  }

  handlePreviousPage = (evt) => {
    const { selected } = this.state;
    if (evt.preventDefault) {
      evt.preventDefault();
    } else {
      // eslint-disable-next-line no-param-reassign
      evt.returnValue = false;
    }

    if (selected > 0) {
      this.handlePageSelected(selected - 1, evt);
    }
  };

  handleNextPage = (evt) => {
    const { selected } = this.state;
    const { pageCount } = this.props;

    if (evt.preventDefault) {
      evt.preventDefault();
    } else {
      // eslint-disable-next-line no-param-reassign
      evt.returnValue = false;
    }
    if (selected < pageCount - 1) {
      this.handlePageSelected(selected + 1, evt);
    }
  };

  handlePageSelected = (selected, evt) => {
    if (evt.preventDefault) {
      evt.preventDefault();
    } else {
      // eslint-disable-next-line no-param-reassign
      evt.returnValue = false;
    }

    const { selected: oldSelected } = this.state;

    if (oldSelected === selected) {
      this.callActiveCallback(selected);
      return;
    }

    this.setState({ selected });

    this.callCallback(selected);
  };

  getEventListener = (handlerFunction) => {
    const { eventListener } = this.props;
    return {
      [eventListener]: handlerFunction,
    };
  };

  handleBreakClick = (index, evt) => {
    if (evt.preventDefault) {
      evt.preventDefault();
    } else {
      // eslint-disable-next-line no-param-reassign
      evt.returnValue = false;
    }

    const { selected } = this.state;

    this.handlePageSelected(selected < index ? this.getForwardJump() : this.getBackwardJump(), evt);
  };

  pagination = () => {
    const {
      pageRangeDisplayed,
      pageCount,
      marginPagesDisplayed,
      breakLabel,
      breakClassName,
      breakLinkClassName,
    } = this.props;

    const { selected } = this.state;

    if (pageCount <= pageRangeDisplayed) {
      return Array.from(Array(pageCount).keys()).map((item, index) => this.getPageElement(index));
    }

    let leftSide = pageRangeDisplayed / 2;
    let rightSide = pageRangeDisplayed - leftSide;

    if (selected > pageCount - pageRangeDisplayed / 2) {
      rightSide = pageCount - selected;
      leftSide = pageRangeDisplayed - rightSide;
    } else if (selected < pageRangeDisplayed / 2) {
      leftSide = selected;
      rightSide = pageRangeDisplayed - leftSide;
    }

    let breakView;
    const createPageView = (i) => this.getPageElement(i);

    return Array.from(Array(pageCount).keys()).reduce((acc, item, index) => {
      const page = index + 1;
      if (page <= marginPagesDisplayed) {
        return [...acc, createPageView(index)];
      }

      if (page > pageCount - marginPagesDisplayed) {
        return [...acc, createPageView(index)];
      }

      if (index >= selected - leftSide && index <= selected + rightSide) {
        return [...acc, createPageView(index)];
      }

      if (breakLabel && acc[acc.length - 1] !== breakView) {
        breakView = (
          <BreakView
            key={`break-view-${page - 1}`}
            breakLabel={breakLabel}
            breakClassName={breakClassName}
            breakLinkClassName={breakLinkClassName}
            breakHandler={(e) => {
              this.handleBreakClick(index, e);
            }}
            getEventListener={this.getEventListener}
          />
        );
        return [...acc, breakView];
      }

      return acc;
    }, []);
  };

  brakeHandleCreator = (index) => () => {
    this.handleBreakClick(null, index);
  };

  updateSelected = (forcePage) => {
    this.setState({ selected: forcePage });
  };

  callCallback = (selectedItem) => {
    const { onPageChange } = this.props;
    if (typeof onPageChange !== 'undefined' && typeof onPageChange === 'function') {
      onPageChange({ selected: selectedItem });
    }
  };

  callActiveCallback = (selectedItem) => {
    const { onPageActive } = this.props;
    if (typeof onPageActive !== 'undefined' && typeof onPageActive === 'function') {
      onPageActive({ selected: selectedItem });
    }
  };

  hrefBuilder(pageIndex) {
    const { hrefBuilder, pageCount } = this.props;
    const { selected } = this.state;
    if (hrefBuilder && pageIndex !== selected && pageIndex >= 0 && pageIndex < pageCount) {
      return hrefBuilder(pageIndex + 1);
    }
    return null;
  }

  ariaLabelBuilder(pageIndex) {
    const { ariaLabelBuilder, pageCount, extraAriaContext } = this.props;
    const { selected: oldSelected } = this.state;
    const selected = pageIndex === oldSelected;
    if (ariaLabelBuilder && pageIndex >= 0 && pageIndex < pageCount) {
      return `${ariaLabelBuilder(pageIndex + 1, selected)}${
        extraAriaContext && !selected ? ` ${extraAriaContext}` : ''
      }`;
    }
    return null;
  }

  render() {
    const {
      disabledClassName,
      pageCount,
      containerClassName,
      previousLabel,
      previousClassName,
      previousLinkClassName,
      previousAriaLabel,
      prevRel,
      nextLabel,
      nextClassName,
      nextLinkClassName,
      nextAriaLabel,
      nextRel,
    } = this.props;

    const { selected } = this.state;

    const previousClasses = previousClassName + (selected === 0 ? ` ${disabledClassName}` : '');
    const nextClasses = nextClassName + (selected === pageCount - 1 ? ` ${disabledClassName}` : '');

    const previousAriaDisabled = selected === 0 ? 'true' : 'false';
    const nextAriaDisabled = selected === pageCount - 1 ? 'true' : 'false';

    return (
      <ul className={containerClassName}>
        <li className={previousClasses}>
          <a
            className={previousLinkClassName}
            href={this.hrefBuilder(selected - 1)}
            tabIndex="0"
            role="button"
            onKeyPress={this.handlePreviousPage}
            aria-disabled={previousAriaDisabled}
            aria-label={previousAriaLabel}
            rel={prevRel}
            {...this.getEventListener(this.handlePreviousPage)}
          >
            {previousLabel}
          </a>
        </li>

        {this.pagination()}

        <li className={nextClasses}>
          <a
            className={nextLinkClassName}
            href={this.hrefBuilder(selected + 1)}
            tabIndex="0"
            role="button"
            onKeyPress={this.handleNextPage}
            aria-disabled={nextAriaDisabled}
            aria-label={nextAriaLabel}
            rel={nextRel}
            {...this.getEventListener(this.handleNextPage)}
          >
            {nextLabel}
          </a>
        </li>
      </ul>
    );
  }
}

PaginationBoxView.propTypes = propTypes;
PaginationBoxView.defaultProps = defaultProps;

export default PaginationBoxView;
