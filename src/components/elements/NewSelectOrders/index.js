import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import bem from 'src/utils/bem';
import dynamic from 'next/dynamic';

import { removeToken } from 'src/redux/auth/actions';
import {
  loadOrderDetails,
  loadOrderImages,
  loadOrderTrack,
} from 'src/redux/ordersList/actions';
import { deleteAllFilesFromCloud } from 'src/redux/myCloud/actions';
import { getToken } from 'src/redux/auth/selectors';
import { orderEnhanceListSelect, ordersListSelect } from 'src/redux/ordersList/selectors';
import styles from './index.module.scss';
import { ENTER_ORDER_NUMBER, ORDER, SELECT_ORDER } from './constants';

const AccountCustomSelect = dynamic(() => import('src/components/inputs/AccountCustomSelect'));
const CloudCustomSelect = dynamic(() => import('src/components/inputs/CloudCustomSelect'));

const b = bem('new-select-order', styles);

const defaultProps = {
  ordersList: [],
  getOrder: () => {},
  getOrderTrack: () => {},
  getOrderImages: () => {},
  token: '',
  view: 'versioned',
  query: {},
  className: '',
  clearCloudFiles: () => {},
  orderEnhanceList: [],
  onlyEnhance: false,
  backValue: () => {},
  isSearchable: false,
  isCloud: false,
};

const propTypes = {
  ordersList: PropTypes.arrayOf(PropTypes.shape({})),
  getOrder: PropTypes.func,
  getOrderTrack: PropTypes.func,
  getOrderImages: PropTypes.func,
  token: PropTypes.string,
  view: PropTypes.string,
  query: PropTypes.shape({
    order: PropTypes.string,
  }),
  className: PropTypes.string,
  clearCloudFiles: PropTypes.func,
  orderEnhanceList: PropTypes.arrayOf(PropTypes.shape({})),
  onlyEnhance: PropTypes.bool,
  backValue: PropTypes.func,
  isSearchable: PropTypes.bool,
  isCloud: PropTypes.bool,
};

class NewSelectOrders extends Component {
  constructor(props) {
    super(props);

    const { query: { order: preloadOrder } = {}, ordersList } = props;
    const selectedOrder =
      ordersList.length && preloadOrder
        ? ordersList.find((item) => item.id === Number(preloadOrder))
        : null;

    const order = selectedOrder
      ? {
          value: selectedOrder.id,
          label: `${ORDER}${selectedOrder.id}`,
        }
      : null;

    this.state = {
      orderValue: order,
    };
  }

  componentDidMount() {
    const { getOrder, getOrderTrack, token, getOrderImages, view, ordersList } = this.props;

    const { orderValue } = this.state;
    if (ordersList.length) {
      const params = {
        view,
      };
      const lastOrder = ordersList.reduce((acc, item) => (item.id > acc.id ? item : acc), {
        id: 0,
      });

      const selectedOrder =
        orderValue && orderValue.value
          ? ordersList.find((item) => item.id === orderValue.value)
          : {};

      const { id } = selectedOrder && selectedOrder.id ? selectedOrder : lastOrder;
      if (!orderValue) {
        const order = {
          value: id,
          label: `${ORDER}${lastOrder.id}`,
        };

        this.setState({
          orderValue: order,
        });
      }
      getOrder({ id, token });
      getOrderTrack({ id, token });
      getOrderImages({ id, token, params });
    }
  }

  onChangeSelect = (order) => {
    const { backValue, getOrder, getOrderTrack, token, getOrderImages, view, clearCloudFiles } =
      this.props;
    this.setState({ orderValue: order });
    const { value } = order;
    const params = {
      view,
    };
    backValue(value);
    getOrder({ id: value, token });
    getOrderTrack({ id: value, token });
    getOrderImages({ id: value, token, params });
    clearCloudFiles();
  };

  render() {
    const { orderValue } = this.state;
    const {
      ordersList,
      className,
      orderEnhanceList,
      onlyEnhance,
      isSearchable,
      isCloud,
    } = this.props;

    const orders = (onlyEnhance ? orderEnhanceList : ordersList)
      .map((item) => ({
        value: item.id,
        label: `${ORDER}${item.id}`,
      }))
      .sort((prevItem, nextItem) => nextItem.value - prevItem.value);
    return (
      <main className={b({ mix: className })}>
        { isCloud ? (
          <CloudCustomSelect
            className={b('select')}
            options={orders}
            placeholder={ENTER_ORDER_NUMBER}
            value={orderValue}
            onChange={this.onChangeSelect}
            round
            isSearchable={isSearchable}
            withListBtn
            withSearchBtn
          />
        ) :
        (
          <AccountCustomSelect
            className={b('select')}
            options={orders}
            placeholder={SELECT_ORDER}
            value={orderValue}
            onChange={this.onChangeSelect}
            round
            isSearchable={isSearchable}
          />
        )}
      </main>
    );
  }
}

NewSelectOrders.propTypes = propTypes;
NewSelectOrders.defaultProps = defaultProps;

const actions = {
  logout: removeToken,
  getOrder: loadOrderDetails,
  getOrderTrack: loadOrderTrack,
  getOrderImages: loadOrderImages,
  clearCloudFiles: deleteAllFilesFromCloud,
};

const stateProps = (state) => ({
  ordersList: ordersListSelect(state),
  token: getToken(state),
  orderEnhanceList: orderEnhanceListSelect(state),
});

export default connect(stateProps, actions)(NewSelectOrders);
