const ORDER = 'Order #';
const SELECT_ORDER = 'Select order';
const VIEW = 'Viewing';
const ENTER_ORDER_NUMBER = 'Enter order number';

export { ORDER, SELECT_ORDER, VIEW, ENTER_ORDER_NUMBER };
