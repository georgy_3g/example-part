import React from 'react';
import PropTypes from 'prop-types';
import bem from 'src/utils/bem';
import useMobileStyle from 'src/utils/useMobileStyle';
import { connect } from 'react-redux';
import colors from 'src/styles/colors.json';
import dynamic from 'next/dynamic';
// import LazyLoad from 'react-lazyload';
import { isMobileDeviceSelector } from 'src/redux/auth/selectors';
import styles from './index.module.scss';

const ColorButton = dynamic(() => import('src/components/buttons/ColorButton'));
const EasyCard = dynamic(() => import('src/components/cards/EasyCard'));

const slateGrayColor = colors['$slate-gray-color'];
const summerGreenColor = colors['$summer-green-color'];
const yellowColor = colors['$coconut-cream-yellow-color'];

const b = bem('easy-steps', styles);

const defaultProps = {
  stripeColor: yellowColor,
  cards: [],
  title: '',
  textBtn: '',
  colorBtn: '',
  isLargeCards: false,
  isMobileDevice: true,
  withScrollBtn: false,
  scroll: () => {},
  description: '',
  customClass: '',
};

const propTypes = {
  stripeColor: PropTypes.string,
  cards: PropTypes.arrayOf(PropTypes.shape({})),
  title: PropTypes.string,
  isLargeCards: PropTypes.bool,
  isMobileDevice: PropTypes.bool,
  description: PropTypes.string,
  textBtn: PropTypes.string,
  colorBtn: PropTypes.string,
  withScrollBtn: PropTypes.bool,
  scroll: PropTypes.func,
  customClass: PropTypes.string,
};

const EasySteps = (props) => {
  const {
    cards,
    title,
    stripeColor,
    isLargeCards,
    isMobileDevice,
    description,
    textBtn,
    colorBtn,
    withScrollBtn,
    scroll,
    customClass,
  } = props;

  const isMobile = useMobileStyle(480, isMobileDevice);

  const getColor = (index) => {
    const itemNumber = index + 1;
    if (isMobile) {
      return (itemNumber % 4) - 2 === 0 || (itemNumber % 4) - 3 === 0
        ? slateGrayColor
      : summerGreenColor;
    }
    return itemNumber % 2 ? summerGreenColor : slateGrayColor;
  };

  return (
    <div className={b({ mix: customClass })}>
      <div className={b('wrapper')}>
        <h2 className={b('title')}>{title}</h2>
        {description && <div className={b('description', { after: true })}>{description}</div>}
        <div className={b('cards')}>
          <div className={b('background-stripe')} style={{ backgroundColor: stripeColor }} />
          {cards.map((item, index) => (
            <div className={b('card-wrapper')} key={item.cardText}>
              {/* <LazyLoad offset={100}> */}
              <EasyCard
                className={b('card', { isLarge: isLargeCards })}
                card={item}
                isFirst={index === 0}
                isLast={index === cards.length - 1}
                isLargeCards
              />
              {/* </LazyLoad> */}
              <div className={b('card-number')} style={{ color: getColor(index) }}>
                {index + 1}
              </div>
            </div>
          ))}
        </div>
        {description && <div className={b('description', { before: true })}>{description}</div>}
        {withScrollBtn && (
        <div className={b('scrollBtn')}>
          <ColorButton
            onClick={scroll}
            text={textBtn}
            className={b('button-wrap', { margin: true })}
            backGroundColor={colorBtn}
          />
        </div>
)}
      </div>
    </div>
  );
}

EasySteps.propTypes = propTypes;
EasySteps.defaultProps = defaultProps;

const stateProps = (state) => ({
  isMobileDevice: isMobileDeviceSelector(state),
});

export default connect(stateProps, null)(EasySteps);
