import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Select from 'react-select';
import bem from 'src/utils/bem';
import DropdownIndicator from './DropdownIndicator';
import styles from './index.module.scss';
import { customStyles, customStylesWithoutLine } from './customSyles';

const b = bem('tabs', styles);

const defaultProps = {
  activeTab: '',
  tabs: [],
  clickHandler: () => {},
  className: '',
  buttonClass: '',
  tabListClass: '',
  withoutSelector: false,
  columnInMobile: false,
  withoutLine: false,
  isPhotoArt: false,
};

const propTypes = {
  activeTab: PropTypes.string,
  tabs: PropTypes.arrayOf(PropTypes.shape({})),
  clickHandler: PropTypes.func,
  className: PropTypes.string,
  buttonClass: PropTypes.string,
  tabListClass: PropTypes.string,
  withoutSelector: PropTypes.bool,
  columnInMobile: PropTypes.bool,
  withoutLine: PropTypes.bool,
  isPhotoArt: PropTypes.bool,
};

const components = {
  IndicatorSeparator: null,
  DropdownIndicator,
};

class Tabs extends Component {
  constructor(props) {
    super(props);
    const { activeTab } = props;
    this.state = {
      activeTab,
    };
  }

  componentDidUpdate(prevProps) {
    const { activeTab } = this.props;
    if (activeTab !== prevProps.activeTab) {
      this.defaultTab({});
    }
  }

  defaultTab = () => {
    const { activeTab } = this.props;
    this.setState({ activeTab });
  };

  clickTab = ({ id }) => {
    const { clickHandler } = this.props;
    this.setState({
      activeTab: id,
    });
    clickHandler(id);
  };

  render() {
    const {
      tabs,
      className,
      buttonClass,
      tabListClass,
      withoutSelector,
      columnInMobile,
      withoutLine,
      isPhotoArt,
    } = this.props;
    const { activeTab } = this.state;
    const sortedTabs = tabs.map((tab) => ({ ...tab, label: tab.text, value: tab.id }));

    return (
      <div className={b({ mix: className, art: isPhotoArt })}>
        <div
          className={b('tabs-view', {
            mix: tabListClass,
            'always-show': withoutSelector,
            column: columnInMobile,
          })}
        >
          {tabs.map(({ text, id }) => (
            <button
              key={id}
              id={id}
              type="button"
              className={b('btn', {
                mix: buttonClass,
                active: activeTab === id,
                column: columnInMobile,
                art: isPhotoArt,
              })}
              onClick={({ target }) => this.clickTab(target)}
            >
              {text}
            </button>
          ))}
        </div>
        {!withoutSelector && (
          <div className={b('select-view', { 'without-line': withoutLine })}>
            <Select
              className={b('select', { 'without-line': withoutLine })}
              defaultValue={sortedTabs[0]}
              options={sortedTabs}
              styles={withoutLine ? customStylesWithoutLine : customStyles}
              onChange={this.clickTab}
              isSearchable={false}
              components={withoutLine ? components : {}}
            />
          </div>
        )}
      </div>
    );
  }
}

Tabs.propTypes = propTypes;
Tabs.defaultProps = defaultProps;

export default Tabs;
