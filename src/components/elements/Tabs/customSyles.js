import Colors from 'src/styles/colors.json';

const pickledBluewoodeColor = Colors['$pickled-bluewood-color'];
const turquoiseColor = Colors['$turquoise-color'];
const grayColor = Colors['$gull-gray-color'];

const customStyles = {
  control: (styles) => ({
    ...styles,
    border: 'none',
    backgroundColor: 'transparent',
    boxShadow: 'none',
  }),
  singleValue: (styles) => ({
    ...styles,
    borderBottom: `2px solid ${turquoiseColor}`,
    fontSize: '1.5rem',
    fontWeight: '500',
    lineHeight: '2rem',
    color: `${pickledBluewoodeColor}`,
    position: 'initial',
    transform: 'none',
    maxWidth: '100%',
  }),
  option: (styles, { isSelected }) => ({
    ...styles,
    backgroundColor: 'transparent',
    color: `${pickledBluewoodeColor}`,
    borderBottom: isSelected ? `2px solid ${turquoiseColor}` : 'none',
    fontSize: '1.5rem',
    fontWeight: '500',
    lineHeight: '2rem',
  }),
  menu: (styles) => ({
    ...styles,
    border: 'none',
    backgroundColor: 'transparent',
    boxShadow: 'none',
    position: 'relative',
  }),
  indicatorSeparator: (styles) => ({
    ...styles,
    display: 'none',
  }),
};

const customStylesWithoutLine = {
  control: (styles) => ({
    ...styles,
    border: 'none',
    backgroundColor: 'transparent',
    boxShadow: 'none',
  }),
  singleValue: (styles) => ({
    ...styles,
    fontSize: '1.25rem',
    fontWeight: '500',
    lineHeight: '2rem',
    color: `${pickledBluewoodeColor}`,
    position: 'initial',
    transform: 'none',
    maxWidth: '100%',
    margin: '0 12px 0 0',
  }),
  option: (styles, { isSelected }) => ({
    ...styles,
    backgroundColor: 'transparent',
    color: `${isSelected ? pickledBluewoodeColor : grayColor}`,
    fontSize: '1.25rem',
    fontWeight: '500',
    lineHeight: '2rem',
  }),
  menu: (styles) => ({
    ...styles,
    border: 'none',
    backgroundColor: 'transparent',
    boxShadow: 'none',
    position: 'relative',
  }),
  indicatorSeparator: (styles) => ({
    ...styles,
    display: 'none',
  }),
  indicatorsContainer: (styles) => ({
    ...styles,
    padding: '4px',
  }),
};

export { customStyles, customStylesWithoutLine };
