import React, { Component } from 'react';
import bem from 'src/utils/bem';
import PropTypes from 'prop-types';
import Colors from 'src/styles/colors.json';
import dynamic from "next/dynamic";
import styles from './index.module.scss';

const HintIcon = dynamic(() => import('src/components/svg/HintIcon'));

const mainColor = Colors['$mercury-gray-color'];
const bgColor = Colors['$dark-slate-blue-color'];

const b = bem('hint', styles);

const defaultProps = {
  toggleHint: () => {},
  className: '',
  backgroundFill: null,
};

const propTypes = {
  toggleHint: PropTypes.func,
  className: PropTypes.string,
  backgroundFill: PropTypes.string,
};

class Hint extends Component {
  constructor(props) {
    super(props);

    const { backgroundFill } = props;

    this.state = {
      fillColor: backgroundFill || mainColor,
      strokeColor: bgColor,
    };
  }

  hoverIcon = () => {
    this.setState({
      fillColor: bgColor,
      strokeColor: mainColor,
    });
  };

  leaveIcon = () => {
    const { backgroundFill } = this.props;
    this.setState({
      fillColor: backgroundFill || mainColor,
      strokeColor: bgColor,
    });
  };

  render() {
    const { toggleHint, className } = this.props;
    const { fillColor, strokeColor } = this.state;
    return (
      <div
        className={b({ mix: className })}
        onMouseEnter={this.hoverIcon}
        onMouseLeave={this.leaveIcon}
        role="button"
        tabIndex="0"
        onClick={toggleHint}
      >
        <HintIcon fillBg={fillColor} stroke={strokeColor} />
      </div>
    );
  }
}

Hint.propTypes = propTypes;
Hint.defaultProps = defaultProps;

export default Hint;
