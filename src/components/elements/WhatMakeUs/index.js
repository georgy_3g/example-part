import React from 'react';
import PropTypes from 'prop-types';
import bem from 'src/utils/bem';
import dynamic from 'next/dynamic';
import styles from './index.module.scss';

const ColorButton = dynamic(() => import('src/components/buttons/ColorButton'));
const MakeUsCard = dynamic(() => import('src/components/cards/MakeUsCard'));

const b = bem('what-make-us', styles);

const defaultProps = {
  cards: [],
  title: '',
  stripeColor: '',
  description: '',
  isDigitize: false,
  displayType: 'desc',
  withScrollBtn: false,
  scroll: () => {},
  textBtn: '',
  colorBtn: '',
  isEnhance: false,
  isOurMission: false,
};

const propTypes = {
  cards: PropTypes.arrayOf(PropTypes.shape({})),
  title: PropTypes.string,
  stripeColor: PropTypes.string,
  isDigitize: PropTypes.bool,
  displayType: PropTypes.string,
  description: PropTypes.string,
  withScrollBtn: PropTypes.bool,
  scroll: PropTypes.func,
  textBtn: PropTypes.string,
  colorBtn: PropTypes.string,
  isEnhance: PropTypes.bool,
  isOurMission: PropTypes.bool,
};

const WhatMakeUs = (props) => {
  const {
    cards,
    title,
    stripeColor,
    isDigitize,
    displayType,
    description,
    withScrollBtn,
    scroll,
    textBtn,
    colorBtn,
    isEnhance,
    isOurMission,
  } = props;

  return (
    <div className={b({ digitize: isDigitize, enhance: isEnhance, mission: isOurMission })}>
      <div className={b('wrapper', { mission: isOurMission })}>
        <h2 className={b('title')}>{title}</h2>
        {description && <div className={b('description', { enhance: isEnhance })}>{description}</div>}
        <div className={b('cards')}>
          <div className={b('background-stripe')} style={{ backgroundColor: stripeColor }} />
          {cards.map((item, index) => (
            <MakeUsCard
              card={item}
              isFirst={index === 0}
              isLast={index === cards.length - 1}
              displayType={displayType}
              key={item.cardText}
            />
          ))}
        </div>
        {
          withScrollBtn && (
            <div className={b('scroll-btn')}>
              <ColorButton
                onClick={scroll}
                text={textBtn}
                backGroundColor={colorBtn}
              />
            </div>
          )
        }
      </div>
    </div>
  );
}

WhatMakeUs.propTypes = propTypes;
WhatMakeUs.defaultProps = defaultProps;

export default WhatMakeUs;
