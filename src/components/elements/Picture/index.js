import React from 'react';
import PropTypes from 'prop-types';

const defaultProps = {
  bem: () => {},
  imgUrl: {},
  imgAlt: '',
  imgText: '',
};

const propTypes = {
  bem: PropTypes.func,
  imgUrl: PropTypes.shape({
    defaultImg: PropTypes.shape({
      src: PropTypes.string,
    }),
  }),
  imgAlt: PropTypes.string,
  imgText: PropTypes.string,
};

const Picture = (props) => {
  const {
    bem,
    imgUrl: { defaultImg },
    imgAlt,
    imgText,
  } = props;
  return (
    <div className={bem('img-container')}>
      <div className={bem('picture')}>
        <img className={bem('img')} src={defaultImg.src} alt={imgAlt} />
      </div>
      <div className={bem('block-text')}>
        <p className={bem('img-text')}>{imgText}</p>
      </div>
    </div>
  );
}

Picture.propTypes = propTypes;
Picture.defaultProps = defaultProps;

export default Picture;
