import React from 'react';
import PropTypes from 'prop-types';
import bem from 'src/utils/bem';
import { HOME_TERMS } from 'src/terms';
import dynamic from "next/dynamic";
import styles from './index.module.scss';

const TypeProductCard = dynamic(() => import('src/components/cards/TypeProductCard'));

const b = bem('type-products', styles);

const { WHAT_WE_DO } = HOME_TERMS;

const defaultProps = {
  dataCard: [],
  onClick: () => {},
  dataTitle: WHAT_WE_DO,
  subTitle: '',
  withoutTitleInMobile: false,
  className: '',
  id: '',
};

const propTypes = {
  dataCard: PropTypes.arrayOf(PropTypes.shape({})),
  onClick: PropTypes.func,
  dataTitle: PropTypes.string,
  subTitle: PropTypes.string,
  withoutTitleInMobile: PropTypes.bool,
  className: PropTypes.string,
  id: PropTypes.string,
};

const TypeProducts = (props) => {
  const {
    onClick,
    dataCard,
    className,
    id,
  } = props;

  return (
    <section className={b({ mix: className })} id={id}>
      <div className={b('card-container')} id='what-we-do-id'>
        <div className={b('background-stripe')} />
        {dataCard.map((item) => (
          <TypeProductCard key={item.title} {...item} onClick={onClick} />
        ))}
      </div>
    </section>
  );
}

TypeProducts.propTypes = propTypes;
TypeProducts.defaultProps = defaultProps;

export default TypeProducts;
