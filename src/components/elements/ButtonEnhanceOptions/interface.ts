export interface imagesOptionInteraface {
  imageId: number;
  quantity: number;
}

export interface enhanceOptionInterface {
  optionId?: number;
  name?: string;
  relationImagesOption?: imagesOptionInteraface[];
  quantity?: number;
}

interface show {
  open: boolean;
  optionShow: boolean;
  id?: number;
}

export interface propsInterface {
  className?: string;
  enhanceGetOption?: {
    name: string;
    options: {
      name: string;
      index: number;
      id: number;
      isPackage?: boolean;
      displayName: string;
      price: number;
    }[];
  }[];
  show: show;
  nameOperation?: string;
  idPhoto?: number;
  appendOptionEnhance?: enhanceOptionInterface[];
  changeAppendOptions?: (array: enhanceOptionInterface[]) => void;
  managementOpenWindow: (id: number) => void;
  setShow: (value: show) => void;
  refsRoot: HTMLDivElement | HTMLElement | null;
  scroll?: number;
  scrollX?: number;
  positionData: HTMLDivElement | HTMLElement | null;
  isCheckout: boolean;
  zIndex?: number;
  withoutZindex?: boolean;
}
