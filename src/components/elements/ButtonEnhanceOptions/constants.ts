const CLOSE = 'Close';
const ADD_PRINTS = 'Add prints or frame';
const BACK = 'Back';
const PRINT_PACKAGES = 'Add tribute frame';
const CART_PRINTS = 'Add prints';

export { CLOSE, ADD_PRINTS, BACK, PRINT_PACKAGES, CART_PRINTS };
