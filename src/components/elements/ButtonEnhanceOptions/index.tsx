import React, { FunctionComponent, useEffect, useState } from 'react';
import ReactDom from 'react-dom';
import { connect } from 'react-redux';
import bem from 'src/utils/bem';
import BigCountInput from 'src/components/inputs/BigCountInput';
import { enhanceGetOptionSelector } from 'src/redux/enhance/selectors';

import Colors from 'src/styles/colors.json';
import debounce from 'lodash/debounce';
import dynamic from 'next/dynamic';
import { ADD_PRINTS, BACK, CART_PRINTS, CLOSE, PRINT_PACKAGES } from './constants';
import { enhanceOptionInterface, imagesOptionInteraface, propsInterface } from './interface';
import styles from './index.module.scss';

const CheckFull = dynamic(() => import('src/components/svg/CheckFull'));

const b = bem('button-drop-down', styles);
const redColor = Colors['$burnt-sienna-color'];
const whiteColor = Colors['$white-color'];


const MIN_QT = 0;
const MAX_QT = 99999;

const defaultProps = {
  className: '',
  enhanceGetOption: [],
  nameOperation: 'photo_restoration',
  idPhoto: 0,
  appendOptionEnhance: [],
  changeAppendOptions: () => { return false },
  setShow: () => { return false },
  managementOpenWindow: () => { return false },
  refsRoot: null,
  scroll: 0,
  positionData: null,
  show: {
    open: false,
    optionShow: false,
  },
  scrollX: 0,
  zIndex: 0,
  withoutZindex: false,
};

const ButtonEnhanceOptions:FunctionComponent<propsInterface> = ({
  className,
  enhanceGetOption,
  nameOperation,
  idPhoto,
  appendOptionEnhance,
  changeAppendOptions,
  refsRoot,
  managementOpenWindow,
  show,
  setShow,
  scroll,
  positionData,
  scrollX,
  isCheckout,
  zIndex,
  withoutZindex,
}) => {
  const [itPacked, setItPacked] = useState(false);
  const [windowWidth, setWindowWidth] = useState(1920);

  const { options: searchOption = [] } = enhanceGetOption
    ? enhanceGetOption.find(({ name = '' }) => name === nameOperation) || {}
    : {};
  const enhanceOptions = searchOption.filter(({ name }) => name === 'enhance_prints')
    .sort(({ index: indexOne }, { index: indexTwo }) => indexOne - indexTwo) || [];

  const validateQuantity = (qt: number, value: number) => {
    const inc = value ? 1 : -1;
    if (qt >= MAX_QT && inc > 0) { return qt; }
    if (qt <= 1 && inc < 1) { return MIN_QT; }
    return qt + inc;
  };

  const setResize = () => {
    const {body} = document;
    const element= document.documentElement;
    const result = (element.clientHeight)? element: body;
    setWindowWidth(result.offsetWidth)
  }

  const resizeWindow = debounce(() => {
    setResize()
  }, 300);

  useEffect(() => {
    setResize();
    window.addEventListener('resize', resizeWindow);
  }, [])



  const editOptionsEnhance = (value: number, idOption: number, imageId: number) => {
    const newState = appendOptionEnhance
      ? appendOptionEnhance.reduce((acc, element) => {
        const { relationImagesOption = [], quantity, optionId } = element || {};
        if (optionId !== idOption) return [...acc, element];
        const {
          imageId: searchImageId = false,
          quantity: quantityImages = 0,
        } = relationImagesOption.find(item => item.imageId === imageId) || {};
        if (!quantityImages && !value) return [...acc, element];
        const newQuantity = validateQuantity(quantity || 0, value);
        if (newQuantity === 0) return acc;

        const newRelationImagesOption = searchImageId
          ? relationImagesOption.reduce((accum: imagesOptionInteraface[], item) => {
            if (item.imageId === imageId) {
              const getQuantity = validateQuantity(item.quantity, value);
              if (getQuantity === 0) return accum;
              return [...accum, {
                imageId: item.imageId,
                quantity: getQuantity,
              }];
            }
            return [...accum, item];
          }, []) : [...relationImagesOption, { imageId, quantity: validateQuantity(0, value) }];

        return [...acc, {
          optionId,
          name: 'enhance_prints',
          quantity: newQuantity,
          relationImagesOption: newRelationImagesOption,
        }];
      },[] as enhanceOptionInterface[])
      : [];
      if (changeAppendOptions) changeAppendOptions(newState);
  };

  const checkIdOption = (
    value: number,
    idOption: number,
    imageId: number,
  ) => () => {
    const option = appendOptionEnhance ? appendOptionEnhance.some(check => check.optionId === idOption) : false;

    if (option) {
      editOptionsEnhance(value, idOption, imageId);
    } else if (!option && value) {
      const newOption = [...(appendOptionEnhance || []) , {
        optionId: idOption,
        name: 'enhance_prints',
        quantity: validateQuantity(0, value),
        relationImagesOption: [{
          imageId,
          quantity: validateQuantity(0, value),
        }],
      }];
      if(changeAppendOptions) changeAppendOptions(newOption);
    }
  };

  const getQuantity = (id: number) => {
    const { relationImagesOption = [] } = appendOptionEnhance
      ? appendOptionEnhance.find(({ optionId }) => optionId === id) || {}
      : {};
    const { quantity = 0 } = relationImagesOption.find(({ imageId }) => imageId === idPhoto) || {};
    return quantity;
  };

  const getPosition = () => {
    const {
      offsetTop = 0,
      offsetLeft = 0,
      clientHeight = 0,
      clientWidth = 0,
    } = positionData || {};
    const top = clientHeight !== 236 ? clientHeight * 0.2 : 0;
    return {
      left: `calc(${clientWidth}px / 2 + ${offsetLeft}px - ${scrollX}px)`,
      transform: 'translateX(-50%)',
      top: clientHeight - (scroll || 0) + offsetTop - top,
      ...(!withoutZindex ? { zIndex } : {}),
    };
  };

  const getStatusShow = (status: boolean) => appendOptionEnhance ? appendOptionEnhance
    .some(({ optionId, relationImagesOption = [] }) => {
      const { isPackage = null } = enhanceOptions.find(item => optionId === item.id) || {};
      const search = relationImagesOption.find(option => option.imageId === idPhoto) || {};
      return Boolean(Object.keys(search).length && status === isPackage);
    }) : false;


  const blockHiddenOrNot = () => {
    const {
      clientWidth: ParentWidth = 0,
      offsetLeft: ParentLeft = 0,
    } = refsRoot || {};
    const {
      offsetTop = 0,
      clientHeight = 0,
      offsetLeft = 0,
      clientWidth = 0
    } = positionData || {};

    const padding = windowWidth < 480 ? 15: 40;
    return (scroll || 0) > offsetTop + clientHeight
    || !show.open
    || (windowWidth < 1024 && !isCheckout
      &&  (((offsetLeft - (scrollX ||  0) - padding) < ParentLeft)
      || (windowWidth > 480 && ParentWidth < offsetLeft + clientWidth - (scrollX || 0))
    || (ParentWidth < offsetLeft + clientWidth - (scrollX || 0) - padding)));
    };

  return (
    <div className={b('container', { mix: className })}>
      <div
        className={b('container-button')}
        role="button"
        tabIndex={0}
        onClick={() => managementOpenWindow(idPhoto || 0)}
      >
        {(show.open && show.id === idPhoto)
        ? (
          <>
            <div className={b('text-close')}>{show.optionShow ? BACK : CLOSE}</div>
            <div className={b('arrow', {
            revert: !show.optionShow,
            back: show.optionShow,
           })}
            />
          </>
)
        : (
          <>
            <p className={b('text-prints')}>{ADD_PRINTS}</p>
            <div className={b('arrow')} />
          </>
        )}
      </div>
      {show.optionShow
      && enhanceOptions
      && refsRoot
      && show.id === idPhoto
      && Boolean(Object.keys(refsRoot).length)
        ? ReactDom.createPortal(
          <div className={b('drop-down-list', { hidden: blockHiddenOrNot(), padding: isCheckout })} style={getPosition()}>
            {enhanceOptions.map(({
              isPackage,
              displayName,
              price,
              id,
            }) => {
              const quantity = getQuantity(id);
              const check = isPackage === itPacked;
              if (check) {
                return (
                  <div className={b('option-container')} key={id}>
                    <div className={b('price', { package: isPackage })}>
                      <p className={b('display', { name: true })}>{displayName}</p>
                      <p className={b('display', { price: true })}>{`$${price} each`}</p>
                    </div>
                    <BigCountInput
                      count={quantity}
                      min={0}
                      withInput
                      mainCounter
                      minSize
                      changeCount={(value: number) => checkIdOption(value, id, idPhoto || 0)}
                    />
                  </div>
                );
              }
              return null;
            })}
          </div>, refsRoot,
        ) : (
          <>
            {show.open
              && show.id === idPhoto
              && refsRoot
              && Boolean(Object.keys(refsRoot).length)
              && ReactDom.createPortal(
                <div className={b('package-container', { hidden: blockHiddenOrNot(), padding: isCheckout})} style={getPosition()}>
                  <div
                    className={b('is-package')}
                    role="button"
                    tabIndex={0}
                    onClick={() => {
                    setItPacked(false);
                    setShow({ ...show, optionShow: true });
                  }}
                  >
                    {getStatusShow(false) ? (
                      <CheckFull
                        className={b('is-package-icon')}
                        stroke={whiteColor}
                        stroke2={whiteColor}
                        fill={redColor}
                      />
                  ) : <div className={b('default-is-package-icon')} />}
                    <p className={b('is-package-text')}>{CART_PRINTS}</p>
                  </div>
                  <div
                    className={b('is-package')}
                    role="button"
                    tabIndex={0}
                    onClick={() => {
                    setItPacked(true);
                    setShow({ ...show, optionShow: true });
                  }}
                  >
                    {getStatusShow(true) ? (
                      <CheckFull
                        className={b('is-package-icon')}
                        stroke={whiteColor}
                        stroke2={whiteColor}
                        fill={redColor}
                      />
                  ) : <div className={b('default-is-package-icon')} />}
                    <p className={b('is-package-text')}>{PRINT_PACKAGES}</p>
                  </div>
                </div>, refsRoot,
            )}
          </>
        )}
    </div>
  );
};

const stateProps = (state: any) => ({
  enhanceGetOption: enhanceGetOptionSelector(state),
});

ButtonEnhanceOptions.defaultProps = defaultProps;

export default connect(stateProps)(ButtonEnhanceOptions);
