import React from 'react';
import PropTypes from 'prop-types';
import bem from 'src/utils/bem';
import dynamic from "next/dynamic";
import styles from './index.module.scss';

const PlayIcon = dynamic(() => import('src/components/svg/PlayIcon'));
const PauseIcon = dynamic(() => import('src/components/svg/PauseIcon'));

const b = bem('media-button', styles);

const defaultProps = {
  className: '',
  isPlay: false,
  onClick: () => {},
};

const propTypes = {
  className: PropTypes.string,
  isPlay: PropTypes.bool,
  onClick: PropTypes.func,
};

function MediaButton(props) {
  const { className, isPlay, onClick } = props;

  const buttonClassName = isPlay ? 'pause' : 'play';

  const Icon = isPlay ? PauseIcon : PlayIcon;

  return (
    <button className={b(buttonClassName, { mix: className })} type="button" onClick={onClick}>
      <Icon className={b('icon')} />
    </button>
  );
}

MediaButton.propTypes = propTypes;
MediaButton.defaultProps = defaultProps;

export default MediaButton;
