import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { myCloudImages } from 'src/redux/myCloud/selectors';
import bem from 'src/utils/bem';
import colors from 'src/styles/colors.json';
import ROUTES from 'src/constants/routes';
import dynamic from 'next/dynamic';
import { orderPriceSelect } from 'src/redux/prices/selectors';
import { getToken } from 'src/redux/auth/selectors';
import { setApprovedImageUrl } from 'src/redux/approvedOrderImage/actions';
import { getArchiveActions } from 'src/redux/folder/actions';
import { loadImagesOrderEnhanceUrl } from 'src/redux/orderImages/actions';
import styles from './index.module.scss';
import {
  CREATE_ART,
  DOWNLOAD,
  DOWNLOAD_AUDIO,
  DOWNLOAD_FOLDER,
  DOWNLOAD_VIDEO,
  ENHANCE_PHOTO,
  ORDER_PRINTS,
  RESTORE_PHOTO,
  RETOUCH_PHOTO,
  SHARE,
  SHARE_AUDIO,
  SHARE_FOLDER,
  SHARE_VIDEO,
} from './constants';

const SharePopup = dynamic(() => import('src/components/popups/SharePopup'));
const CreateSmallIcon = dynamic(() => import('src/components/svg/CreateSmallIcon'));
const DownloadFileSmallIcon = dynamic(() => import('src/components/svg/DownloadFileSmallIcon'));
const ShareFileSmallIcon = dynamic(() => import('src/components/svg/ShareFileSmallIcon'));
const NewDropDownArrow = dynamic(() => import('src/components/svg/NewDropDownArrow'));
const CreateArtIcon = dynamic(() => import('src/components/svg/CreateArtIcon'));
const OrderPrintsIcon = dynamic(() => import('src/components/svg/OrderPrintsIcon'));
const BrushWithPlusIcon = dynamic(() => import('src/components/svg/BrushWithPlusIcon'));

const b = bem('image-menu', styles);

const { photoRestoration, photoRetouching } = ROUTES;

const greenColor = colors['$summer-green-color'];
const grayColor = colors['$shark-gray-color'];

const propTypes = {
  className: PropTypes.string,
  imageData: PropTypes.shape({
    imageUri: PropTypes.string,
    imageKey: PropTypes.string,
    videoUri: PropTypes.string,
    audioUri: PropTypes.string,
    id: PropTypes.string,
    imageId: PropTypes.number,
    folder: PropTypes.number,
    orderImageId: PropTypes.number,
  }),
  setImage: PropTypes.func,
  forSharedPage: PropTypes.bool,
  withOutOrderPrint: PropTypes.bool,
  portalBlock: PropTypes.node,
  orderPrints: PropTypes.func,
  downloadArchive: PropTypes.func,
  selectToCreate: PropTypes.func,
  cloudImages: PropTypes.arrayOf(PropTypes.shape({})),
  isLockDigitizeFiles: PropTypes.bool,
  isNoDropDown: PropTypes.bool,
  isFolder: PropTypes.bool,
  slider: PropTypes.bool,
  token: PropTypes.string,
  item: PropTypes.shape({}),
  clickOrderPrints: PropTypes.func,
  text: PropTypes.string,
  withText: PropTypes.bool,
  type: PropTypes.string,
};

const defaultProps = {
  className: '',
  imageData: {},
  setImage: () => {},
  forSharedPage: false,
  withOutOrderPrint: false,
  downloadArchive: () => {},
  portalBlock: null,
  orderPrints: () => {},
  selectToCreate: () => {},
  cloudImages: [],
  isLockDigitizeFiles: false,
  isNoDropDown: false,
  isFolder: false,
  slider: false,
  token: '',
  item: {},
  clickOrderPrints: () => {},
  text: '',
  withText: false,
  type: 'photo',
};

class ImageMenu extends React.Component {
  constructor(props) {
    super(props);
    this.block = null;
    this.nextElem = null;
    this.menuBlock = null;
    this.state = {
      isOpen: false,
      showPopup: false,
      isOpenEnhance: false,
    };
  }

  componentDidMount() {
    document.addEventListener('click', this.checkClick);
    this.nextElem = document.getElementById('__next');
  }

  componentWillUnmount() {
    document.removeEventListener('click', this.checkClick);
    this.block = null;
    this.nextElem = null;
    this.menuBlock = null;
  }

  closePopup = () => {
    this.setState({ showPopup: false });
  };

  openPopup = (e) => {
    this.setState({ showPopup: true });
    e.stopPropagation();
  };

  checkClick = (e) => {
    const isBlockClick = this.block.contains(e.target);
    const isMenuClick = this.menuBlock ? this.menuBlock.contains(e.target) : false;
    if (!isBlockClick && !isMenuClick) {
      this.close(e);
    }
  };

  setPhotoArtImage = () => {
    const { setImage, imageData } = this.props;
    setImage(imageData);
  };

  open = (e) => {
    const { isOpen } = this.state;
    this.setState({ isOpen: !isOpen });
    e.stopPropagation();
  };

  close = (e) => {
    this.setState({ isOpen: false });
    e.stopPropagation();
  };

  saveRoot = (node) => {
    this.block = node;
  };

  saveMenuBlock = (e) => {
    this.menuBlock = e;
  };

  getSharedDate = () => {
    const { imageData, item } = this.props;
    const { ids } = item || {};
    const { imageUri, videoUri, audioUri } = imageData;
    if (imageUri) return { images: [imageData] };
    if (videoUri) return { videos: [imageData] };
    if (audioUri) return { audios: [imageData] };
    if (ids.length) return { folder: ids };
    return {};
  };

  toggleEnhance = (e) => {
    const { isOpenEnhance } = this.state;
    this.setState({ isOpenEnhance: !isOpenEnhance });
    e.stopPropagation();
  };

  downloadFolder = (e) => {
    const { token, downloadArchive, item } = this.props;
    const { orderId, ids } = item || {};
    downloadArchive({ token, orderId, ids });
    e.stopPropagation();
  };

  download = (url, e) => {
    const link = document.createElement('a');
    link.href = url;
    link.setAttribute('download', true);
    document.body.appendChild(link);
    link.click();
    link.remove();
    e.stopPropagation();
  };

  addEnhance = (imageUri, orderImageId, type, imageKey, e, fileId) => {
    const item = {
      addedAt: '2021-10-01T15:10:35.465Z',
      approvedDate: null,
      id: orderImageId,
      imageKey,
      imageUri,
      isApproved: false,
      isCropped: false,
      isOriginal: false,
      isReturned: false,
      isSubtaskCompleted: false,
      name: null,
      parentId: null,
      parentPath: 'root',
      updatedAt: null,
      userId: 195,
      watermarkedImageKey: imageKey,
      watermarkedImageUri: imageUri,
      fileId,
    };
    sessionStorage.setItem('cloudImageData', JSON.stringify(item));
    if (type === 'photoRestoration') {
      window.location.href = photoRestoration;
    }
    if (type === 'photoRetouching') {
      window.location.href = photoRetouching;
    }
    e.stopPropagation();
  };

  selectAndOrderPrints = (e) => {
    const { orderPrints, selectToCreate, cloudImages, imageData } = this.props;

    const data = this.getSharedDate();
    const {
      images: {
        0: { id: myCloudImageId },
      },
    } = data;

    const result = cloudImages.some((image) => image.id === myCloudImageId);
    if (!result) {
      selectToCreate(e, true);
      orderPrints([imageData]);
    } else {
      orderPrints([imageData]);
    }
    e.stopPropagation();
  };

  changePositionBlock = () => {
    const { portalBlock } = this.props;

    const { right = 0 } = portalBlock.getBoundingClientRect() || {};
    return window.innerWidth - right < 100;
  };

  getDownloadText = () => {
    const { type } = this.props;
    switch (type) {
      case 'photo':
        return DOWNLOAD;
      case 'video':
        return DOWNLOAD_VIDEO;
      case 'audio':
        return DOWNLOAD_AUDIO;
      default:
        return DOWNLOAD;
    }
  };

  getShareText = () => {
    const { type } = this.props;
    switch (type) {
      case 'photo':
        return SHARE;
      case 'video':
        return SHARE_VIDEO;
      case 'audio':
        return SHARE_AUDIO;
      default:
        return SHARE;
    }
  };

  render() {
    const { isOpen, showPopup, isOpenEnhance } = this.state;
    const {
      className,
      imageData,
      forSharedPage,
      withOutOrderPrint,
      portalBlock,
      isLockDigitizeFiles,
      isNoDropDown,
      slider,
      isFolder,
      clickOrderPrints,
      text,
      withText,
    } = this.props;

    const { imageUri, imageKey, audioUri, videoUri, orderImageId, id: fileId } = imageData;

    const data = this.getSharedDate();

    return (
      <section className={b({ mix: className })} ref={this.saveRoot}>
        {isNoDropDown && (
          <div className={b('flat-list')}>
            {!isLockDigitizeFiles && (
              <button
                className={b('flat-list-btn')}
                type="button"
                onClick={(e) => this.download(imageUri || audioUri || videoUri, e)}
              >
                <DownloadFileSmallIcon className={b('icon')} />
                {this.getDownloadText()}
              </button>
            )}
            {!withOutOrderPrint && (
              <button
                className={b('flat-list-btn')}
                type="button"
                onClick={() => clickOrderPrints([imageData])}
              >
                <OrderPrintsIcon className={b('icon')} />
                {ORDER_PRINTS}
              </button>
            )}
            {!withOutOrderPrint && (
              <button
                className={b('flat-list-btn')}
                type="button"
                onClick={this.selectAndOrderPrints}
              >
                <CreateSmallIcon className={b('icon')} />
                {CREATE_ART}
              </button>
            )}
            {!forSharedPage && (
              <>
                {!isLockDigitizeFiles && (
                  <button className={b('flat-list-btn')} type="button" onClick={this.openPopup}>
                    <ShareFileSmallIcon className={b('icon')} />
                    {this.getShareText()}
                  </button>
                )}
                {!withOutOrderPrint && (
                  <button className={b('flat-list-btn')} type="button" onClick={this.toggleEnhance}>
                    <BrushWithPlusIcon className={b('icon')} />
                    {ENHANCE_PHOTO}
                  </button>
                )}
                {isOpenEnhance && (
                  <div className={b('flat-sub-list')}>
                    <button
                      className={b('flat-list-btn', { 'sub-item': true })}
                      type="button"
                      onClick={(e) => {
                        this.addEnhance(imageUri, orderImageId, 'photoRestoration', imageKey, e);
                      }}
                    >
                      <span className={b('sub-item-text')}>{RESTORE_PHOTO}</span>
                    </button>
                    <button
                      className={b('flat-list-btn', { 'sub-item': true })}
                      type="button"
                      onClick={(e) => {
                        this.addEnhance(imageUri, orderImageId, 'photoRetouching', imageKey, e);
                      }}
                    >
                      <span className={b('sub-item-text')}>{RETOUCH_PHOTO}</span>
                    </button>
                  </div>
                )}
              </>
            )}
          </div>
        )}
        {!isNoDropDown && (
          <>
            {portalBlock && (
              <button
                className={b('menu-btn', { active: isOpen })}
                type="button"
                onClick={this.open}
              >
                {withText && <span className={b('button-text')}>{text}</span>}
                <NewDropDownArrow color={isOpen ? greenColor : grayColor} />
              </button>
            )}
            {isOpen && !portalBlock && (
              <div
                className={b('list', { open: isOpen })}
                ref={this.saveMenuBlock}
                onClick={(e) => {
                  e.stopPropagation();
                }}
                role="button"
                tabIndex={0}
              >
                {!isLockDigitizeFiles && (
                  <a className={b('list-link')} href={imageUri || audioUri || videoUri} download>
                    <button className={b('list-btn')} type="button">
                      <DownloadFileSmallIcon className={b('icon')} />
                      {this.getDownloadText()}
                    </button>
                  </a>
                )}
                {!withOutOrderPrint && (
                  <>
                    <button
                      className={b('list-btn')}
                      type="button"
                      onClick={() => clickOrderPrints([imageData])}
                    >
                      <OrderPrintsIcon className={b('icon')} />
                      {ORDER_PRINTS}
                    </button>
                    <button
                      className={b('list-btn')}
                      type="button"
                      onClick={this.selectAndOrderPrints}
                    >
                      <CreateSmallIcon className={b('icon')} />
                      {CREATE_ART}
                    </button>
                  </>
                )}
                {!forSharedPage && (
                  <>
                    {!isLockDigitizeFiles && (
                      <button className={b('list-btn')} type="button" onClick={this.openPopup}>
                        <ShareFileSmallIcon className={b('icon')} />
                        {this.getShareText()}
                      </button>
                    )}
                    <button className={b('list-btn')} type="button" onClick={this.toggleEnhance}>
                      <BrushWithPlusIcon className={b('icon')} />
                      {ENHANCE_PHOTO}
                    </button>
                    {isOpenEnhance && (
                      <div className={b('sub-list')}>
                        <button
                          className={b('list-btn')}
                          type="button"
                          onClick={(e) => {
                            this.addEnhance(
                              imageUri,
                              orderImageId,
                              'photoRestoration',
                              imageKey,
                              e,
                              fileId,
                            );
                          }}
                        >
                          <span className={b('sub-list-text')}>{RESTORE_PHOTO}</span>
                        </button>
                        <button
                          className={b('list-btn')}
                          type="button"
                          onClick={(e) => {
                            this.addEnhance(
                              imageUri,
                              orderImageId,
                              'photoRetouching',
                              imageKey,
                              e,
                              fileId,
                            );
                          }}
                        >
                          <span className={b('sub-list-text')}>{RETOUCH_PHOTO}</span>
                        </button>
                      </div>
                    )}
                  </>
                )}
              </div>
            )}
            {isOpen &&
              portalBlock &&
              ReactDOM.createPortal(
                <div
                  className={b('list', { open: isOpen, slider, right: this.changePositionBlock() })}
                  ref={this.saveMenuBlock}
                  onClick={(e) => {
                    e.stopPropagation();
                  }}
                  role="button"
                  tabIndex={0}
                >
                  {!isLockDigitizeFiles && !isFolder && (
                    <button
                      className={b('list-btn')}
                      type="button"
                      onClick={(e) => this.download(imageUri || audioUri || videoUri, e)}
                    >
                      <DownloadFileSmallIcon className={b('icon')} />
                      {this.getDownloadText()}
                    </button>
                  )}
                  {isFolder && (
                    <button className={b('list-btn')} type="button" onClick={this.downloadFolder}>
                      <DownloadFileSmallIcon className={b('icon')} />
                      {DOWNLOAD_FOLDER}
                    </button>
                  )}
                  {!withOutOrderPrint && (
                    <>
                      <button
                        className={b('list-btn')}
                        type="button"
                        onClick={() => clickOrderPrints([imageData])}
                      >
                        <OrderPrintsIcon className={b('icon')} />
                        {ORDER_PRINTS}
                      </button>
                      <button
                        className={b('list-btn')}
                        type="button"
                        onClick={this.selectAndOrderPrints}
                      >
                        <CreateArtIcon className={b('icon')} />
                        {CREATE_ART}
                      </button>
                    </>
                  )}
                  {!forSharedPage && (
                    <>
                      {!isLockDigitizeFiles && !isFolder && (
                        <button className={b('list-btn')} type="button" onClick={this.openPopup}>
                          <ShareFileSmallIcon className={b('icon')} />
                          {this.getShareText()}
                        </button>
                      )}
                      {isFolder && (
                        <button className={b('list-btn')} type="button" onClick={this.openPopup}>
                          <ShareFileSmallIcon className={b('icon')} />
                          {SHARE_FOLDER}
                        </button>
                      )}
                      {!withOutOrderPrint && (
                        <button
                          className={b('list-btn', { 'last-child': true })}
                          type="button"
                          onClick={this.toggleEnhance}
                        >
                          <BrushWithPlusIcon className={b('icon')} />
                          {ENHANCE_PHOTO}
                        </button>
                      )}
                      {isOpenEnhance && (
                        <div className={b('sub-list')}>
                          <button
                            className={b('list-btn', { 'sub-item': true })}
                            type="button"
                            onClick={(e) => {
                              this.addEnhance(
                                imageUri,
                                orderImageId,
                                'photoRestoration',
                                imageKey,
                                e,
                                fileId,
                              );
                            }}
                          >
                            <span className={b('sub-item-text')}>{RESTORE_PHOTO}</span>
                          </button>
                          <button
                            className={b('list-btn', { 'sub-item': true })}
                            type="button"
                            onClick={(e) => {
                              this.addEnhance(
                                imageUri,
                                orderImageId,
                                'photoRetouching',
                                imageKey,
                                e,
                                fileId,
                              );
                            }}
                          >
                            <span className={b('sub-item-text')}>{RETOUCH_PHOTO}</span>
                          </button>
                        </div>
                      )}
                    </>
                  )}
                </div>,
                portalBlock || null,
              )}
          </>
        )}
        {showPopup &&
          ReactDOM.createPortal(
            <SharePopup data={data} close={this.closePopup} />,
            this.nextElem || null,
          )}
      </section>
    );
  }
}

ImageMenu.propTypes = propTypes;
ImageMenu.defaultProps = defaultProps;

const stateProps = (state) => ({
  allPrices: orderPriceSelect(state),
  cloudImages: myCloudImages(state),
  token: getToken(state),
});

const actions = {
  setImage: setApprovedImageUrl,
  downloadArchive: getArchiveActions,
  uploadImagesEnhance: loadImagesOrderEnhanceUrl,
};

export default connect(stateProps, actions)(ImageMenu);
