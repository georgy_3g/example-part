const CREATE_ART = 'Create art';
const ORDER_PRINTS = 'Order prints';
const DOWNLOAD = 'Download Photo';
const DOWNLOAD_VIDEO = 'Download Video';
const DOWNLOAD_AUDIO = 'Download Audio';
const SHARE = 'Share photo';
const SHARE_VIDEO = 'Share video';
const SHARE_AUDIO = 'Share audio';
const RETOUCH_PHOTO = 'Retouch Photo';
const RESTORE_PHOTO = 'Restore Photo';
const COLORIZE_PHOTO = 'Colorize Photo';
const ENHANCE_PHOTO = 'Enhance photo';
const SHARE_FOLDER = 'Share folder';
const DOWNLOAD_FOLDER = 'Download folder';

export {
  ORDER_PRINTS,
  DOWNLOAD,
  DOWNLOAD_VIDEO,
  DOWNLOAD_AUDIO,
  SHARE,
  SHARE_VIDEO,
  SHARE_AUDIO,
  RETOUCH_PHOTO,
  RESTORE_PHOTO,
  COLORIZE_PHOTO,
  ENHANCE_PHOTO,
  SHARE_FOLDER,
  DOWNLOAD_FOLDER,
  CREATE_ART,
};
