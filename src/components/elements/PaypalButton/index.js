import React, { Component } from 'react';
import axios from 'axios';
import PropTypes from 'prop-types';
import { PAYPAL_CLIENT_ID } from 'src/config';
import { ONE_DAY } from 'src/constants';
import URL from 'src/redux/urls';
import { getError } from 'src/redux/utils';
import getRollbar from 'src/utils/getRollbar';
import differenceInDays from 'date-fns/differenceInDays';

const rollbar = getRollbar();

const defaultProps = {
  orderCreate: () => {},
  orderSum: 0,
  registrationPayment: () => {},
  type: 'paypal',
  getOrderId: () => {},
  orderPaymentFailed: () => {},
  projects: [],
  paypalStart: () => {},
  paypalSuccess: () => {},
  paypalFail: () => {},
  changeProjectsStatus: () => {},
  additionalPayment: false,
  getValues: () => {},
  baseOrderInfo: () => {},
  checkIsNeedOrderDelete: () => {},
};

const propTypes = {
  orderCreate: PropTypes.func,
  token: PropTypes.string.isRequired,
  orderSum: PropTypes.number,
  registrationPayment: PropTypes.func,
  type: PropTypes.string,
  getOrderId: PropTypes.func,
  orderPaymentFailed: PropTypes.func,
  projects: PropTypes.arrayOf(PropTypes.shape({})),
  paypalStart: PropTypes.func,
  paypalSuccess: PropTypes.func,
  paypalFail: PropTypes.func,
  changeProjectsStatus: PropTypes.func,
  additionalPayment: PropTypes.bool,
  getValues: PropTypes.func,
  baseOrderInfo: PropTypes.func,
  checkIsNeedOrderDelete: PropTypes.func,
};

const getOrderInfo = (props) => {
  const { getValues, baseOrderInfo, checkIsNeedOrderDelete } = props;
  const values = getValues();

  const {
    billingFirstName,
    billingLastName,
    billingEmail,
    billingCompanyName,
    billingStreetAddress,
    billingUnit,
    billingCity,
    billingPostCode,
    billingPhone,
    deliveryFirstName,
    deliveryLastName,
    deliveryCompanyName,
    deliveryStreetAddress,
    deliveryUnit,
    deliveryCity,
    deliveryPostCode,
    deliveryPhone,
    billingState,
    deliveryState,
    textArea,
  } = values || {};
  const sameAddressCheckbox = document.getElementById('sameAddress');
  const sameAddress = sameAddressCheckbox ? sameAddressCheckbox.checked : true;

  const billingAddress = {
    firstName: billingFirstName,
    lastName: billingLastName,
    email: billingEmail,
    companyName: billingCompanyName,
    country: 'USA',
    streetAddress: billingStreetAddress,
    city: billingCity,
    zipCode: billingPostCode,
    apartment: billingUnit,
    state: billingState.value,
    phone: billingPhone.replace(/[_|-]/g, ''),
  };

  const shippingAddress = {
    firstName: sameAddress ? billingFirstName : deliveryFirstName,
    lastName: sameAddress ? billingLastName : deliveryLastName,
    companyName: sameAddress ? billingCompanyName : deliveryCompanyName,
    country: 'USA',
    streetAddress: sameAddress ? billingStreetAddress : deliveryStreetAddress,
    city: sameAddress ? billingCity : deliveryCity,
    zipCode: sameAddress ? billingPostCode : deliveryPostCode,
    apartment: sameAddress ? billingUnit : deliveryUnit,
    state: sameAddress ? billingState.value : deliveryState.value,
    phone: sameAddress ? billingPhone.replace(/[_|-]/g, '') : deliveryPhone.replace(/[_|-]/g, ''),
  };
  const orderDetails = {
    ...baseOrderInfo(),
    billingAddress,
    shippingAddress,
    ...checkIsNeedOrderDelete(),
  };
  if (!billingUnit) delete orderDetails.billingAddress.apartment;
  if (!deliveryUnit) delete orderDetails.shippingAddress.apartment;
  if (textArea) orderDetails.shippingAddress.notes = textArea;
  return orderDetails;
};

class PayPalButton extends Component {
  componentDidMount() {
    if (window.paypal) {
      this.renderButton();
    } else {
      this.initButton();
    }
  }

  initButton = () => {
    const {
      token,
      orderSum,
      registrationPayment,
      type,
      orderPaymentFailed,
      paypalStart,
      paypalSuccess,
      paypalFail,
      getOrderId,
      orderCreate,
      projects,
      changeProjectsStatus,
      additionalPayment,
      getValues,
      baseOrderInfo,
      checkIsNeedOrderDelete,
    } = this.props;

    const script = document.createElement('script');
    script.src = `https://www.paypal.com/sdk/js?client-id=${PAYPAL_CLIENT_ID}&disable-funding=card,venmo,sepa,eps,bancontact,ideal,giropay,mybank,p24,sofort`;
    document.head.appendChild(script);
    script.onload = () => {
      window.paypal
        .Buttons({
          style: {
            color: 'blue',
            shape: 'pill',
            label: 'pay',
          },
          createOrder(data, actions) {
            return actions.order.create({
              purchase_units: [
                {
                  amount: {
                    value: orderSum,
                  },
                },
              ],
            });
          },
          // eslint-disable-next-line consistent-return
          onApprove(data, actions) {
            paypalStart();
            const order = getOrderId();
            const { date } = order || {};
            const isEditOrder = JSON.parse(localStorage.getItem('editOrder') || 'false');
            const isOldOrderId = (!date || differenceInDays(new Date(), new Date(date)) > ONE_DAY) && order;
            if ((order.id && !isEditOrder && !isOldOrderId) || additionalPayment) {
              return actions.order.capture().then((details) => {
                registrationPayment({
                  token,
                  id: order.id,
                  type,
                  paypalId: details.id,
                });
                paypalSuccess({ id: order.id });
              });
            }

            const orderDetails = getOrderInfo({
              getValues,
              baseOrderInfo,
              checkIsNeedOrderDelete,
            });

            axios({
              method: 'POST',
              url: URL.ORDER_CREATE,
              data: orderDetails,
              headers: { Authorization: `Bearer ${token}` },
            })
              .then(({ data: { data: newOrder } }) => {
                const { id: orderId } = newOrder;
                if (projects && projects.length) {
                  changeProjectsStatus({
                    orderId,
                    projects,
                    token,
                  });
                }
                return actions.order.capture().then((details) => {
                  registrationPayment({
                    token,
                    id: orderId,
                    type,
                    paypalId: details.id,
                  });
                  paypalSuccess({ id: orderId });
                });
              })
              .catch(({ response }) => {
                paypalFail(getError(response));
              });
          },
          onCancel() {
            const orderDetails = getOrderInfo({
              getValues,
              baseOrderInfo,
              checkIsNeedOrderDelete,
            });

            const { id = 0 } = getOrderId();
            if (!id) {
              orderCreate({
                token,
                order: orderDetails,
                type,
                projects,
              });
            }
          },
          onError() {
            orderPaymentFailed('Payment failed');
          },
        })
        .render('#paypal-button-container');
    };
  };

  renderButton = () => {
    const {
      token,
      orderSum,
      registrationPayment,
      type,
      orderPaymentFailed,
      paypalStart,
      paypalSuccess,
      paypalFail,
      getOrderId,
      orderCreate,
      projects,
      changeProjectsStatus,
      additionalPayment,
      getValues,
      baseOrderInfo,
      checkIsNeedOrderDelete,
    } = this.props;

    window.paypal
      .Buttons({
        style: {
          color: 'blue',
          shape: 'pill',
          label: 'pay',
        },
        createOrder(data, actions) {
          return actions.order.create({
            purchase_units: [
              {
                amount: {
                  value: orderSum,
                },
              },
            ],
          });
        },
        // eslint-disable-next-line consistent-return
        onApprove(data, actions) {
          paypalStart();
          const order = getOrderId();
          const { date } = order || {};
          const isEditOrder = JSON.parse(localStorage.getItem('editOrder') || 'false');
          const isOldOrderId = (!date || differenceInDays(new Date(), new Date(date)) > ONE_DAY) && order;
          if ((order.id && !isEditOrder && !isOldOrderId) || additionalPayment) {
            return actions.order.capture().then((details) => {
              registrationPayment({
                token,
                id: order.id,
                type,
                paypalId: details.id,
              });
              paypalSuccess({ id: order.id });
            });
          }
          const orderDetails = getOrderInfo({
            getValues,
            baseOrderInfo,
            checkIsNeedOrderDelete,
          });
          axios({
            method: 'POST',
            url: URL.ORDER_CREATE,
            data: orderDetails,
            headers: { Authorization: `Bearer ${token}` },
          })
            .then(({ data: { data: newOrder } }) => {
              const { id: orderId } = newOrder;
              if (projects && projects.length) {
                changeProjectsStatus({
                  orderId,
                  projects,
                  token,
                });
              }
              return actions.order.capture().then((details) => {
                registrationPayment({
                  token,
                  id: orderId,
                  type,
                  paypalId: details.id,
                });
                paypalSuccess({ id: orderId });
              });
            })
            .catch(({ response }) => {
              rollbar.error('paypal fail', response);
              paypalFail(getError(response));
            });
        },
        onCancel() {
          const orderDetails = getOrderInfo({
            getValues,
            baseOrderInfo,
            checkIsNeedOrderDelete,
          });

          const { id = 0 } = getOrderId();
          if (!id) {
            orderCreate({
              token,
              order: orderDetails,
              type,
              projects,
            });
          }
        },
        onError(error) {
          orderPaymentFailed('Payment failed');
          rollbar.error('paypal fail', error);
        },
      })
      .render('#paypal-button-container');
  };

  render() {
    return <div id="paypal-button-container" role="button" />;
  }
}

PayPalButton.propTypes = propTypes;
PayPalButton.defaultProps = defaultProps;

export default PayPalButton;
