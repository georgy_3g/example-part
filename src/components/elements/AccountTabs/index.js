import React, { Component } from 'react';
import PropTypes from 'prop-types';
import bem from 'src/utils/bem';
import styles from './index.module.scss';

const b = bem('account-tabs', styles);

const defaultProps = {
  activeTab: '',
  tabs: [],
  clickHandler: () => {},
  className: '',
  buttonClass: '',
  tabListClass: '',
  withoutSelector: false,
  columnInMobile: false,
  isMain: false,
  isSubTabs: false,
  isMobile: false,
  statusCloud: 0,
};

const propTypes = {
  activeTab: PropTypes.string,
  tabs: PropTypes.arrayOf(PropTypes.shape({})),
  clickHandler: PropTypes.func,
  className: PropTypes.string,
  buttonClass: PropTypes.string,
  tabListClass: PropTypes.string,
  withoutSelector: PropTypes.bool,
  columnInMobile: PropTypes.bool,
  isMain: PropTypes.bool,
  isSubTabs: PropTypes.bool,
  isMobile: PropTypes.bool,
  statusCloud: PropTypes.number,
};

class AccountTabs extends Component {
  constructor(props) {
    super(props);
    const { activeTab } = props;
    this.state = {
      activeTab,
    };
  }

  componentDidUpdate(prevProps) {
    const { activeTab } = this.props;
    if (activeTab !== prevProps.activeTab) {
      this.defaultTab({});
    }
  }

  defaultTab = () => {
    const { activeTab } = this.props;
    this.setState({ activeTab });
  };

  clickTab = ({ id }) => {
    const { clickHandler } = this.props;
    this.setState({
      activeTab: id,
    });
    clickHandler(id);
  };

  render() {
    const {
      tabs,
      className,
      buttonClass,
      tabListClass,
      withoutSelector,
      columnInMobile,
      isMain,
      isSubTabs,
      isMobile,
      statusCloud,
    } = this.props;
    const { activeTab } = this.state;

    return (
      <div
        className={b({
          mix: className,
          main: isMain,
          'sub-tubs': isSubTabs,
          mobile: isMobile,
        })}
      >
        <div
          className={b('tabs-view', {
            mix: tabListClass,
            'always-show': withoutSelector,
            column: columnInMobile,
            main: isMain,
            'sub-tubs': isSubTabs,
          })}
        >
          {tabs.filter(({ id }) => (
            id !== 'myFiles' || (id === 'myFiles' && Boolean(statusCloud))
          )).map(({ text, id }) => (
            <button
              key={id}
              id={id}
              type="button"
              className={b('btn', {
                mix: buttonClass,
                active: activeTab === id,
                column: columnInMobile,
                main: isMain,
                'main-active': isMain && activeTab === id,
              })}
              onClick={({ target }) => this.clickTab(target)}
            >
              {text}
            </button>
          ))}
        </div>
      </div>
    );
  }
}

AccountTabs.propTypes = propTypes;
AccountTabs.defaultProps = defaultProps;

export default AccountTabs;
