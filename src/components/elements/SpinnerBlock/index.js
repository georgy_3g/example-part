import React from 'react';
import bem from 'src/utils/bem';
import Image from 'next/image';
import spinner from  'public/img/shared/spinner*******.gif';
import styles from './index.module.scss';


const b = bem('spinner-block', styles);

function SpinnerBlock() {
  return (
    <div className={b()}>
      <Image
        className={b('spinner')}
        src={spinner}
        alt={spinner}
        width="100%"
        height="100%"
      />
    </div>
  );
}

export default SpinnerBlock;
