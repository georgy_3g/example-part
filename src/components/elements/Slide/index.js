import React from 'react';
import PropTypes from 'prop-types';
import dynamic from "next/dynamic";

const ClientCard = dynamic(() => import('src/components/cards/ClientCard'));

const propTypes = {
  data: PropTypes.shape({
    video: PropTypes.string,
  }),
  bem: PropTypes.func,
  withReadMore: PropTypes.bool,
};

const defaultProps = {
  data: {},
  bem: () => {  },
  withReadMore: false,
};

const Slide = ({ data, bem, withReadMore }) => {
  return (
    <div className={bem('inner-slide')}>
      {
      data && <ClientCard {...data} withReadMore={withReadMore} />
    }
    </div>
  )
}

Slide.propTypes = propTypes;
Slide.defaultProps = defaultProps;

export default Slide;
