import React from 'react';
import PropTypes from 'prop-types';
import bem from 'src/utils/bem';
import { connect } from 'react-redux';
import colors from 'src/styles/colors.json';
import dynamic from 'next/dynamic';
import { isMobileDeviceSelector } from 'src/redux/auth/selectors';
import styles from './index.module.scss';

const NewEasyCard = dynamic(() => import('src/components/cards/NewEasyCard'));

const yellowColor = colors['$coconut-cream-yellow-color'];

const b = bem('new-easy-steps', styles);

const defaultProps = {
  stripeColor: yellowColor,
  cards: [],
  title: '',
  description: '',
  className: '',
};

const propTypes = {
  stripeColor: PropTypes.string,
  cards: PropTypes.arrayOf(PropTypes.shape({})),
  title: PropTypes.string,
  description: PropTypes.string,
  className: PropTypes.string,
};

const NewEasySteps = (props) => {
  const {
    cards,
    title,
    stripeColor,
    description,
    className,
  } = props;

  return (
    <div className={b({ mix: className })}>
      <div className={b('wrapper')}>
        <h2 className={b('title')}>{title}</h2>
        {description && <div className={b('description', { after: true })}>{description}</div>}
        <div className={b('cards')}>
          <div className={b('background-stripe')} style={{ backgroundColor: stripeColor }} />
          {cards.map((item, index) => (
            <div className={b('card-wrapper')} key={item.cardText}>
              <NewEasyCard
                className={b('card' )}
                card={item}
                isFirst={index === 0}
                isLast={index === cards.length - 1}
                isLargeCards
              />
            </div>
          ))}
        </div>
        {description && <div className={b('description', { before: true })}>{description}</div>}
      </div>
    </div>
  );
}

NewEasySteps.propTypes = propTypes;
NewEasySteps.defaultProps = defaultProps;

const stateProps = (state) => ({
  isMobileDevice: isMobileDeviceSelector(state),
});

export default connect(stateProps, null)(NewEasySteps);
