import React, { Component } from 'react';
import bem from 'src/utils/bem';
import PropTypes from 'prop-types';
import styles from './index.module.scss';

const b = bem('toggler-input', styles);

const defaultProps = {
  onText: 'yes',
  offText: 'no',
  checked: false,
  disabled: false,
  togglerChange: () => {},
  className: '',
  inputClassName: '',
};

const propTypes = {
  checked: PropTypes.bool,
  onText: PropTypes.string,
  offText: PropTypes.string,
  disabled: PropTypes.bool,
  togglerChange: PropTypes.func,
  className: PropTypes.string,
  inputClassName: PropTypes.string,
};

class Toggler extends Component {
  handleChange = () => {
    const { togglerChange, checked, disabled } = this.props;
    if (!disabled) {
      togglerChange(!checked);
    }
  };

  render() {
    const { onText, offText, disabled, checked, className, inputClassName } = this.props;
    return (
      <div className={b({ mix: className })}>
        <div
          className={b('wrap', { mix: inputClassName, disabled, checked })}
          data-text-on={onText}
          data-text-off={offText}
          onClick={this.handleChange}
          role="button"
          tabIndex="0"
          disabled={disabled}
        >
          <input
            type="checkbox"
            disabled={disabled}
            checked={checked}
            onChange={this.handleChange}
            data-text-on={onText}
            data-text-off={offText}
          />
        </div>
      </div>
    );
  }
}

Toggler.defaultProps = defaultProps;
Toggler.propTypes = propTypes;

export default Toggler;
