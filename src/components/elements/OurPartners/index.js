import React from 'react';
import bem from 'src/utils/bem';
import { AS_SEEN_ON, TRUSTED_BY_DOBLE_DOT } from 'src/constants/partners';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { isMobileDeviceSelector } from 'src/redux/auth/selectors';
import styles from './index.module.scss';
import TERMS from './constants';

const b = bem('our-customers', styles);

const propTypes = {
  className: PropTypes.string,
};
const defaultProps = {
  className: '',
};
const { RESTORATION_AS_SEEN_ON, RESTORATION_DATA_TRUSTED } = TERMS;

const OurPartners = (props) => {
  const { className } = props;
  return (
    <div className={b({ mix: className })}>
      <div className={b('one-block')}>
        <h2 className={b('text')}>{TRUSTED_BY_DOBLE_DOT}</h2>
        <div className={b('block-image')}>
          {RESTORATION_DATA_TRUSTED.map(({ url }, index) => (
            <div className={b('partners-icon-wrap')}>
              <img
                className={b('partners-icon', {
                  rectangle: index === 1,
                })}
                src={url.src}
                alt="partner"
              />
            </div>
            ))}
        </div>
      </div>
      <div className={b('two-block')}>
        <h2 className={b('text')}>{AS_SEEN_ON}</h2>
        <div className={b('block-image')}>
          {RESTORATION_AS_SEEN_ON.map(({ url }, index) => (
            <div className={b('partners-icon-wrap')}>
              <img
                className={b('partners-icon', {
                  square: index === 2 || index === 3 || index === 4 || index === 5,
                  rectangle: index === 1,
                })}
                src={url.src}
                alt="partner"
              />
            </div>
          ))}
        </div>
      </div>
    </div>
)
}

OurPartners.propTypes = propTypes;
OurPartners.defaultProps = defaultProps;

const stateProps = (state) => ({
  isMobileDevice: isMobileDeviceSelector(state),
});

export default connect(stateProps, null)(OurPartners);
