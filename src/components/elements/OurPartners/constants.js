import image1 from 'public/img/logo-partners/cbs.png';
import image2 from 'public/img/logo-partners/apple.png';
import image3 from 'public/img/logo-partners/ap2.png';
import image4 from 'public/img/logo-partners/fox.png';
import image5 from 'public/img/logo-partners/nbc.png';
import image6 from 'public/img/logo-partners/abc.png';

import image7 from 'public/img/logo-partners/mit.png';
import image8 from 'public/img/logo-partners/marriot.png';
import image9 from 'public/img/logo-partners/disnay.png';
import image10 from 'public/img/logo-partners/fau.png';
import image11 from 'public/img/logo-partners/army.png';


const TRUSTED_BY = 'Trusted by:';
const AS_SEEN_ON = 'As seen on:';

const RESTORATION_AS_SEEN_ON = [
  {
    url: image1,
  },
  {
    url: image2,
  },
  {
    url: image3,
  },
  {
    url: image4,
  },
  {
    url: image5,
  },
  {
    url: image6,
  },
];

const RESTORATION_DATA_TRUSTED = [
  {
    url: image7,
  },
  {
    url: image8,
  },
  {
    url: image9,
  },
  {
    url: image10,
  },
  {
    url: image11,
  },
];

export default {
  TRUSTED_BY,
  AS_SEEN_ON,
  RESTORATION_DATA_TRUSTED,
  RESTORATION_AS_SEEN_ON,
};
