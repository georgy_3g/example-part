import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Formik } from 'formik';
import * as Yup from 'yup';
import { connect } from 'react-redux';
import bem from 'src/utils/bem';
import dynamic from 'next/dynamic';
import { orderChangefromCart } from 'src/redux/orders/actions';
import styles from './index.module.scss';
import TERMS from './constants';

const NewCustomInput = dynamic(() => import('src/components/inputs/NewCustomInput'));
const CustomTextarea = dynamic(() => import('src/components/inputs/CustomTextarea'));
const CloseIcon2 = dynamic(() => import('src/components/svg/CloseIcon2'));
const ColorButton = dynamic(() => import('src/components/buttons/ColorButton'));

const { SENT_TO, ADD_WRAPPING, YES_PLEASE, NO, SAVE, SHOULD_BE_FILLED, CLOSE } = TERMS;

const schema = Yup.object().shape({
  name: Yup.string().required(SHOULD_BE_FILLED),
  address1: Yup.string().required(SHOULD_BE_FILLED),
  city: Yup.string().required(SHOULD_BE_FILLED),
  state: Yup.string().required(SHOULD_BE_FILLED),
  zip: Yup.string().required(SHOULD_BE_FILLED),
  message: Yup.string().required(SHOULD_BE_FILLED),
});

const b = bem('is-gift-order', styles);

const orderParams = {
  gift: 'giftOptions',
};

const defaultValues = {
  name: '',
  address1: '',
  address2: '',
  city: '',
  message: '',
  addWrapping: false,
};

const defaultProps = {
  orderChange: () => {},
  item: {},
  close: () => {},
};

const propTypes = {
  orderChange: PropTypes.func,
  item: PropTypes.shape({
    id: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    typeProduct: PropTypes.string,
    giftOptions: PropTypes.objectOf(PropTypes.shape({})),
  }),
  close: PropTypes.func,
};

class IsGiftOrder extends Component {
  onSubmit = (typeProduct, itemId, counter, values) => {
    const { city, state, zip, ...other } = values;
    const { orderChange } = this.props;
    const order = {
      typeProduct,
      itemId,
      counter,
      values: {
        ...other,
        city: `${city}, ${state}, ${zip}`,
      },
    };
    orderChange(order);
  };

  render() {
    const { item, close } = this.props;
    const { typeProduct, giftOptions } = item;

    const initialValues =
      giftOptions && Object.keys(giftOptions).length
        ? { ...defaultValues, ...giftOptions }
        : { ...defaultValues };

    const { city: cityValue } = initialValues;

    const [initialCity = '', initialState = '', initialZip = ''] = cityValue.split(', ');

    return (
      <article className={b()}>
        <div className={b('title-wrapper')}>
          <div className={b('title')}>{SENT_TO}</div>
          <div className={b('close-btn')} role="button" tabIndex={-1} onClick={close}>
            <div className={b('close-btn-text')}>{CLOSE}</div>
            <div className={b('close-btn-icon')}>
              <CloseIcon2 />
            </div>
          </div>
        </div>
        <Formik
          enableReinitialize
          initialValues={{
            ...initialValues,
            city: initialCity,
            state: initialState,
            zip: initialZip,
          }}
          validationSchema={schema}
          onSubmit={(values, actions) => {
            this.onSubmit(typeProduct, item.id, orderParams.gift, values);
            actions.resetForm();
          }}
          render={({
            errors,
            handleSubmit,
            touched,
            handleChange,
            values: { name, address1, address2, city, state, zip, message, addWrapping },
          }) => (
            <form onSubmit={handleSubmit} className={b('gift-form')}>
              <div className={b('gift-row-wrapper')}>
                <div className={b('gift-form-left-column')}>
                  <div className={b('column-input')}>
                    <NewCustomInput
                      className={b('column-input-field')}
                      onChange={handleChange}
                      name="name"
                      placeholder="Name"
                      value={name}
                      isTouched={touched.name}
                      error={errors.name}
                      isValid={!errors.name}
                    />
                    <NewCustomInput
                      className={b('column-input-field')}
                      onChange={handleChange}
                      name="address1"
                      placeholder="Address"
                      value={address1}
                      isTouched={touched.address1}
                      error={errors.address1}
                      isValid={!errors.address1}
                    />
                    <NewCustomInput
                      className={b('column-input-field')}
                      onChange={handleChange}
                      name="address2"
                      placeholder="Suite / Apt."
                      value={address2}
                      isTouched={touched.address2}
                      error={errors.address2}
                      isValid={!errors.address2}
                    />
                  </div>
                  <div className={b('column-input', { min: true })}>
                    <NewCustomInput
                      className={b('column-input-field')}
                      onChange={handleChange}
                      name="city"
                      placeholder="City"
                      value={city}
                      isTouched={touched.city}
                      error={errors.city}
                      isValid={!errors.city}
                    />
                  </div>
                </div>
                <div className={b('gift-form-right-column')}>
                  <div className={b('column-textarea')}>
                    <CustomTextarea
                      className={b('textarea')}
                      name="message"
                      rows="14"
                      onChange={handleChange}
                      placeholder="Message"
                      value={message}
                      isTouched={touched.message}
                      error={errors.message}
                      isValid={!errors.message}
                    />
                  </div>
                  <div className={b('gift-row-wrapper')}>
                    <div className={b('column-input', { 'left-min': true })}>
                      <NewCustomInput
                        className={b('column-input-field')}
                        onChange={handleChange}
                        name="state"
                        placeholder="State"
                        value={state}
                        isTouched={touched.state}
                        error={errors.state}
                        isValid={!errors.state}
                      />
                    </div>
                    <div className={b('column-input', { 'right-min': true })}>
                      <NewCustomInput
                        className={b('column-input-field')}
                        onChange={handleChange}
                        name="zip"
                        placeholder="Zip"
                        value={zip}
                        isTouched={touched.zip}
                        error={errors.zip}
                        isValid={!errors.zip}
                      />
                    </div>
                  </div>
                </div>
              </div>
              <div className={b('gift-row-wrapper')}>
                <div className={b('gift-row')}>
                  <div className={b('gift-row-text')}>{ADD_WRAPPING}</div>
                  <label htmlFor={`${item.id}_${typeProduct}`} className={b('checkbox')}>
                    <input
                      id={`${item.id}_${typeProduct}`}
                      className={b('input-checkbox')}
                      name="addWrapping"
                      type="checkbox"
                      onChange={handleChange}
                      checked={addWrapping}
                    />
                    <span className={b('custom')} />
                    <span className={b('label')}>{YES_PLEASE}</span>
                  </label>
                  <label
                    htmlFor={`${item.id}_${typeProduct}`}
                    className={b('checkbox', { right: true })}
                  >
                    <input
                      id={`${item.id}_${typeProduct}`}
                      className={b('input-checkbox')}
                      name="addWrapping"
                      type="checkbox"
                      onChange={handleChange}
                      checked={!addWrapping}
                    />
                    <span className={b('custom')} />
                    <span className={b('label')}>{NO}</span>
                  </label>
                </div>
                <div className={b('submit-wrapper')}>
                  <ColorButton className={b('submit-btn')} text={SAVE} type="submit" />
                </div>
              </div>
            </form>
          )}
        />
      </article>
    );
  }
}

IsGiftOrder.propTypes = propTypes;
IsGiftOrder.defaultProps = defaultProps;

const actions = {
  orderChange: orderChangefromCart,
};

export default connect(null, actions)(IsGiftOrder);
