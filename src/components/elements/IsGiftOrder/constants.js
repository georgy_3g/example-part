const TERMS = {
  SENT_TO: 'Who do you want to send this to?',
  ADD_WRAPPING: 'Add gift wrapping?',
  YES_PLEASE: 'Yes, please (+$15)',
  NO: 'No',
  SAVE: 'Save',
  SHOULD_BE_FILLED: 'Should be filled',
  CLOSE: 'Close',
  PLUS: '+',
};

export default TERMS;
