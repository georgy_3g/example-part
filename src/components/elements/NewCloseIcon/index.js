import React from 'react';
import bem from 'src/utils/bem';
import PropTypes from 'prop-types';
import closeIcon from  'public/img/icon-png/close-icon.png';
import styles from './index.module.scss';


const b = bem('new-close-icon', styles);
const defaultProps = {
  className: '',
};
const propTypes = {
  className: PropTypes.string,
}

const NewCloseIcon = (props) => {
  const { className } = props
  return <img className={b({ mix: className })} src={closeIcon} alt="Close" />;
};

NewCloseIcon.defaultProps = defaultProps;
NewCloseIcon.propTypes = propTypes;

NewCloseIcon.propTypes = propTypes;
NewCloseIcon.defaultProps = defaultProps;

export default NewCloseIcon;
