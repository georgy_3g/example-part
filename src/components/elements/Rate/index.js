import React from 'react';
import PropTypes from 'prop-types';
import StarRatings from 'react-star-ratings';
import Colors from 'src/styles/colors.json';

const activeColor = Colors['$burnt-sienna-red-color'];
const emptyColor = Colors['$athens-gray-color'];

const propTypes = {
  starDimension: PropTypes.string,
  starSpacing: PropTypes.string,
};

const defaultProps = {
  starDimension: 'min(1.25vw, 24px)',
  starSpacing: '5px',
};

function Rate(props) {
  const { starDimension, starSpacing } = props;

  const settings = {
    numberOfStars: 5,
    starDimension,
    starSpacing,
  };

  // const [isLoading, changeStatus] = useState(false);

  // useEffect(() => {
  //   if (!isLoading) {
  //     changeStatus(true);
  //   }
  // });

  return (
    <StarRatings
      rating={5}
      starRatedColor={activeColor}
      starEmptyColor={emptyColor}
      {...settings}
    />
  )
}

Rate.propTypes = propTypes;
Rate.defaultProps = defaultProps;

export default Rate;
