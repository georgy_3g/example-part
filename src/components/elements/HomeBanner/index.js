import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import bem from 'src/utils/bem';
import useMobileStyle from 'src/utils/useMobileStyle';
import { connect } from 'react-redux';
import { isMobileDeviceSelector } from 'src/redux/auth/selectors';
import { promoSectionSelector } from 'src/redux/promo/selectors';
import styles from './index.module.scss';

const b = bem('home-banner', styles);

const defaultProps = {
  bannerTitle: '',
  bannerDesc: '',
  bgImg: '',
  mobileBgImg: '',
  isMobileDevice: true,
  promoSection: {},
  withImage: false,
  titleClassname: '',
  classNameDescription: '',
  isOurMission: false,
};

const propTypes = {
  bannerTitle: PropTypes.string,
  bannerDesc: PropTypes.string,
  bgImg: PropTypes.string,
  mobileBgImg: PropTypes.string,
  isMobileDevice: PropTypes.bool,
  promoSection: PropTypes.shape({ id: PropTypes.number }),
  withImage: PropTypes.bool,
  titleClassname: PropTypes.string,
  classNameDescription: PropTypes.string,
  isOurMission: PropTypes.bool,
};

const HomeBanner = (props) => {
  const [promoBannerHeight, setPromoBannerHeight] = useState(0);

  useEffect(() => {
    const promoBanner = document.getElementById('promo-banner');
    const { height } = promoBanner ? promoBanner.getBoundingClientRect() : { height: 0 };
    setPromoBannerHeight(height);
  }, []);

  const {
    bannerTitle,
    bannerDesc,
    bgImg,
    mobileBgImg,
    isMobileDevice,
    promoSection: { id } = {},
    withImage,
    titleClassname,
    classNameDescription,
    isOurMission,
  } = props;

  const isMobileDisplay = useMobileStyle(480, isMobileDevice);

  const isSection = Boolean(id);
  const bannerBackgroundImage = isMobileDisplay && mobileBgImg ? mobileBgImg : bgImg;

  return (
    <article className={b({ 'with-banner': isSection, mission: isOurMission })} style={{ paddingTop: `${promoBannerHeight}px`}}>
      <div className={b('stripe-one', { mission: isOurMission })} />
      <div className={b('stripe-two', { mission: isOurMission })} />
      <div className={b('stripe-three', { mission: isOurMission })} />
      <div className={b('stripe-four', { mission: isOurMission })} />
      <div className={b('stripe-five', { mission: isOurMission })} />
      {withImage &&<div className={b('stripe-six')} />}
      {withImage && <div className={b('stripe-seven')} />}
      <h1 className={b('title', { mission: isOurMission, mix: titleClassname })}>{bannerTitle}</h1>
      <h2 className={b('desc', { mission: isOurMission, mix: classNameDescription })}>{bannerDesc}</h2>
      {withImage && (
        <img
          className={b('block-image')}
          src={bannerBackgroundImage.src}
          alt="banner"
        />
      )}
    </article>
  );
}

HomeBanner.propTypes = propTypes;
HomeBanner.defaultProps = defaultProps;

const stateProps = (state) => ({
  isMobileDevice: isMobileDeviceSelector(state),
  promoSection: promoSectionSelector(state),
});

export default connect(stateProps, null)(HomeBanner);
