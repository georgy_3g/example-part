import React, { Component } from 'react';
import PropTypes from 'prop-types';
import bem from 'src/utils/bem';
import Colors from 'src/styles/colors.json';
import dynamic from "next/dynamic";
import styles from './index.module.scss';

const ArrowInCircle = dynamic(() => import('src/components/svg/ArrowInCircle'));

const gray = Colors['$bombay-gray-color'];
const red = Colors['$burnt-sienna-red-color'];

const b = bem('know-about', styles);

const propTypes = {
  title: PropTypes.string,
  children: PropTypes.node,
  withoutPadding: PropTypes.bool,
  isNestedItem: PropTypes.bool,
};

const defaultProps = {
  title: '',
  children: null,
  withoutPadding: false,
  isNestedItem: false,
};

class KnowAbout extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: false,
    };
  }

  handleClick = () => {
    const { isOpen } = this.state;
    this.setState({ isOpen: !isOpen });
  };

  render() {
    const { isOpen } = this.state;
    const { title, children, withoutPadding, isNestedItem } = this.props;
    return (
      <section className={b()} key={title}>
        <div
          className={b('container', { isOpen, 'nested-item': isNestedItem })}
          onClick={this.handleClick}
          role="button"
          tabIndex="0"
        >
          <p className={b('title', { 'nested-title': isNestedItem })}>{title}</p>
          <ArrowInCircle className={b('icon', { open: isOpen })} stroke={isOpen ? red : gray} />
        </div>
        {isOpen && (
          <div
            className={b('content', {
              'without-padding': withoutPadding,
              'nested-text': isNestedItem,
            })}
          >
            {children}
          </div>
        )}
      </section>
    );
  }
}

KnowAbout.propTypes = propTypes;
KnowAbout.defaultProps = defaultProps;

export default KnowAbout;
