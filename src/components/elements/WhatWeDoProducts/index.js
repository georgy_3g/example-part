import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import bem from 'src/utils/bem';
import { HOME_TERMS } from 'src/terms';
import { connect } from 'react-redux';
import ROUTES from 'src/constants/routes';
import colors from 'src/styles/colors.json';
import FrameCard from 'src/components/cards/FrameCard';
import { photoArtProductsSelector } from 'src/redux/photoArt/selectors';
import styles from './index.module.scss';

const defaultBlue = colors['$iceberg-blue-light-color'];

const b = bem('what-we-do-products', styles);

const { WHAT_WE_DO } = HOME_TERMS;

const defaultProps = {
  dataTitle: WHAT_WE_DO,
  subTitle: '',
  stripeColor: defaultBlue,
  products: {},
  isFlip: false,
  id: '',
};

const propTypes = {
  dataTitle: PropTypes.string,
  subTitle: PropTypes.string,
  stripeColor: PropTypes.string,
  products: PropTypes.shape({}),
  isFlip: PropTypes.bool,
  id: PropTypes.string,
};

const WhatWeDoProducts = (props) => {
  const { dataTitle, subTitle, stripeColor, products, isFlip, id } = props;

  const [randomProduct, setRandomProducts] = useState([]);

  const getArrayOfProducts = () =>
    Object.values(products).reduce((acc, value) => {
      const { frames } = value;
      return [...acc, frames];
    }, []);

  useEffect(() => {
    const framesArray = getArrayOfProducts();
    const [firstFrames, secondFrames, thirdFrames, fourthFrame, fiveFrames, sixFrames] =
      framesArray;
    const firstArray = [firstFrames, secondFrames, thirdFrames];
    const secondArray = [fourthFrame, fiveFrames, sixFrames];
    const useArray = isFlip ? secondArray : firstArray;
    setRandomProducts(
      useArray.reduce((acc, item) => {
        const randomFrameIndex = Math.floor(Math.random() * (item.length - 1));
        return [...acc, item[randomFrameIndex]];
      }, []),
    );
  }, []);

  const selectFrame = (frame) => {
    const { frameMaterial } = frame;
    document.location.href = `${ROUTES.photoArtEditor}?collection=${frameMaterial.id}&frame=${frame.id}`;
  };

  return (
    <section className={b()} id={id}>
      <div className={b('container')}>
        <h2
          className={b('title', {
            'with-subtitle': Boolean(subTitle),
          })}
        >
          {dataTitle}
        </h2>
        <p className={b('sub-title')}>{subTitle}</p>
      </div>
      <div className={b('card-container')}>
        <div
          className={b('background-stripe', { isFlip })}
          style={{ backgroundColor: stripeColor }}
        />
        {randomProduct.map((item) => (
          <FrameCard
            className={b('frame-card')}
            key={item.title}
            data={item}
            select={() => selectFrame(item)}
            withOutHints
          />
        ))}
      </div>
    </section>
  );
}

WhatWeDoProducts.propTypes = propTypes;
WhatWeDoProducts.defaultProps = defaultProps;

const stateProps = (state) => ({
  products: photoArtProductsSelector(state),
});

export default connect(stateProps)(WhatWeDoProducts);
