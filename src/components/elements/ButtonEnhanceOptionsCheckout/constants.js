const CLOSE = 'Close';
const ADD_PRINTS = 'Add prints';
const BACK = 'Back';
const PRINT_PACKAGES = 'Print Packages';
const CART_PRINTS = 'A-la-cart Prints';

export { CLOSE, ADD_PRINTS, BACK, PRINT_PACKAGES, CART_PRINTS };
