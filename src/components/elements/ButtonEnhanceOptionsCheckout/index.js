import React, { useState } from 'react';
import ReactDom from 'react-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import bem from 'src/utils/bem';
import Colors from 'src/styles/colors.json';
import BigCountInput from 'src/components/inputs/BigCountInput';
import dynamic from 'next/dynamic';
import { enhanceGetOptionSelector } from 'src/redux/enhance/selectors';
import { ADD_PRINTS, BACK, CART_PRINTS, CLOSE, PRINT_PACKAGES } from './constants';
import styles from './index.module.scss';

const CheckFull = dynamic(() => import('src/components/svg/CheckFull'));
const SceneryIcon = dynamic(() => import('src/components/svg/SceneryIcon'));

const b = bem('button-drop-down', styles);
const redColor = Colors['$burnt-sienna-color'];
const whiteColor = Colors['$white-color'];

const MIN_QT = 0;
const MAX_QT = 99999;

const defaultProps = {
  className: '',
  enhanceGetOption: [],
  nameOperation: 'photo_restoration',
  idPhoto: 0,
  appendOptionEnhance: [],
  changeAppendOptions: () => {},
  refsRoot: {},
  scroll: 0,
  positionData: {
    offsetTop: 0,
    offsetLeft: 0,
    clientHeight: 0,
  },
  isCheckout: false,
  setStep: () => {},
};

const propTypes = {
  className: PropTypes.string,
  enhanceGetOption: PropTypes.arrayOf(PropTypes.shape({})),
  nameOperation: PropTypes.string,
  idPhoto: PropTypes.number,
  appendOptionEnhance: PropTypes.arrayOf(PropTypes.shape({})),
  changeAppendOptions: PropTypes.func,
  refsRoot: PropTypes.shape({}),
  scroll: PropTypes.number,
  positionData: PropTypes.shape({
    offsetTop: PropTypes.number,
    offsetLeft: PropTypes.number,
    clientHeight: PropTypes.number,
  }),
  isCheckout: PropTypes.bool,
  setStep: PropTypes.func,
};

function ButtonEnhanceOptionsCheckout(props) {
  const {
    className,
    enhanceGetOption,
    nameOperation,
    idPhoto,
    appendOptionEnhance,
    changeAppendOptions,
    positionData,
    refsRoot,
    scroll,
    isCheckout,
    setStep,
  } = props;
  const [show, setShow] = useState({ open: false, optionShow: false });
  const [itPacked, setItPacked] = useState(false);

  const { options: searchOption = [] } =
    enhanceGetOption.find(({ name = '' }) => name === nameOperation) || {};
  const enhanceOptions =
    searchOption
      .filter(({ name }) => name === 'enhance_prints')
      .sort(({ index: indexOne }, { index: indexTwo }) => indexOne - indexTwo) || [];

  const validateQuantity = (qt, value) => {
    const inc = value ? 1 : -1;
    if (qt >= MAX_QT && inc > 0) {
      return qt;
    }
    if (qt <= 1 && inc < 1) {
      return MIN_QT;
    }
    return qt + inc;
  };

  const editOptionsEnhance = (value, idOption, imageId) => {
    const newState = appendOptionEnhance.reduce((acc, element) => {
      const { relationImagesOption = [], quantity, optionId } = element || {};
      if (optionId !== idOption) return [...acc, element];
      const { imageId: searchImageId = false, quantity: quantityImages = 0 } =
        relationImagesOption.find((item) => item.imageId === imageId) || {};
      if (!quantityImages && !value) return [...acc, element];
      const newQuantity = validateQuantity(quantity, value);
      if (newQuantity === 0) return acc;

      const newRelationImagesOption = searchImageId
        ? relationImagesOption.reduce((accum, item) => {
            if (item.imageId === imageId) {
              const getQuantity = validateQuantity(item.quantity, value);
              if (getQuantity === 0) return accum;
              return [
                ...accum,
                {
                  imageId: item.imageId,
                  quantity: getQuantity,
                },
              ];
            }
            return [...accum, item];
          }, [])
        : [...relationImagesOption, { imageId, quantity: validateQuantity(0, value) }];

      return [
        ...acc,
        {
          optionId,
          name: 'enhance_prints',
          quantity: newQuantity,
          relationImagesOption: newRelationImagesOption,
        },
      ];
    }, []);

    changeAppendOptions(newState);
  };

  const checkIdOption = (value, idOption, imageId) => () => {
    const option = appendOptionEnhance.some((check) => check.optionId === idOption);
    if (option) {
      editOptionsEnhance(value, idOption, imageId);
    } else if (!option && value) {
      const newOption = [
        ...appendOptionEnhance,
        {
          optionId: idOption,
          name: 'enhance_prints',
          quantity: validateQuantity(0, value),
          relationImagesOption: [
            {
              imageId,
              quantity: validateQuantity(0, value),
            },
          ],
        },
      ];
      changeAppendOptions(newOption);
    }
  };

  const getQuantity = (id) => {
    const { relationImagesOption = [] } =
      appendOptionEnhance.find(({ optionId }) => optionId === id) || {};
    const { quantity = 0 } = relationImagesOption.find(({ imageId }) => imageId === idPhoto) || {};
    return quantity;
  };

  const managementOpenWindow = () => {
    if (!show.open && !show.optionShow) {
      setShow({ ...show, open: true });
    }
    if (show.open && show.optionShow) {
      setShow({ ...show, optionShow: false });
    }
    if (show.open && !show.optionShow) {
      setShow({ ...show, open: false });
      if (isCheckout) {
        setStep(1);
      }
    }
  };

  const getStatusShow = (status) =>
    appendOptionEnhance.some(({ optionId, relationImagesOption = [] }) => {
      const { isPackage = null } = enhanceOptions.find((item) => optionId === item.id) || {};
      const search = relationImagesOption.find((option) => option.imageId === idPhoto) || {};
      return Boolean(Object.keys(search).length && status === isPackage);
    });

  const checkHasElements = () =>
    appendOptionEnhance.some(({ relationImagesOption }) => {
      const search = relationImagesOption.find((option) => option.imageId === idPhoto) || {};
      return Boolean(Object.keys(search).length);
    });

  const getPosition = () => {
    const { offsetTop = 0, offsetLeft = 0, clientHeight = 0, clientWidth = 0 } = positionData || {};
    return {
      left: `calc(${clientWidth}px / 2 + ${offsetLeft}px)`,
      transform: 'translateX(-50%)',
      top: clientHeight - scroll + offsetTop,
    };
  };

  const blockHiddenOrNot = () => {
    const { clientHeight: ParentHeight = 0 } = refsRoot || {};
    const { offsetTop = 0, clientHeight = 0 } = positionData || {};
    return scroll > offsetTop + clientHeight || ParentHeight + scroll < offsetTop + clientHeight;
  };

  return (
    <div className={b('container', { mix: className })}>
      <div
        className={b('container-button')}
        role="button"
        tabIndex={0}
        onClick={managementOpenWindow}
      >
        {show.open ? (
          <div className={b('text-close')}>{show.optionShow ? BACK : CLOSE}</div>
        ) : (
          <>
            <div className={b('container-icons')}>
              <SceneryIcon className={b('icon')} />
              {checkHasElements() && (
                <CheckFull
                  className={b('icon-check')}
                  stroke={whiteColor}
                  stroke2={whiteColor}
                  fill={redColor}
                />
              )}
            </div>
            <p className={b('text-prints')}>{ADD_PRINTS}</p>
          </>
        )}
      </div>
      {show.optionShow && enhanceOptions && refsRoot && Boolean(Object.keys(refsRoot).length) ? (
        ReactDom.createPortal(
          <div
            className={b('drop-down-list', { hidden: blockHiddenOrNot() })}
            style={getPosition()}
          >
            {enhanceOptions.map(({ isPackage, displayName, price, id }) => {
              const quantity = getQuantity(id);
              const check = isPackage === itPacked;
              if (check) {
                return (
                  <div className={b('option-container')} key={id}>
                    <div className={b('price', { package: isPackage })}>
                      <p className={b('display', { name: true })}>{displayName}</p>
                      <p className={b('display', { price: true })}>{`${price} each`}</p>
                    </div>
                    <BigCountInput
                      count={quantity}
                      btnClassName={b('big-count-enhance')}
                      min={0}
                      withInput
                      mainCounter
                      minSize
                      changeCount={(value) => checkIdOption(value, id, idPhoto)}
                    />
                  </div>
                );
              }
              return null;
            })}
          </div>,
          refsRoot || null,
        )
      ) : (
        <>
          {show.open &&
            refsRoot &&
            Boolean(Object.keys(refsRoot).length) &&
            ReactDom.createPortal(
              <div
                className={b('package-container', { hidden: blockHiddenOrNot() })}
                style={getPosition()}
              >
                <div
                  className={b('is-package')}
                  role="button"
                  tabIndex={0}
                  onClick={() => {
                    setItPacked(false);
                    setShow({ ...show, optionShow: true });
                  }}
                >
                  {getStatusShow(false) ? (
                    <CheckFull
                      className={b('is-package-icon')}
                      stroke={whiteColor}
                      stroke2={whiteColor}
                      fill={redColor}
                    />
                  ) : (
                    <div className={b('default-is-package-icon')} />
                  )}
                  <p className={b('is-package-text')}>{CART_PRINTS}</p>
                </div>
                <div
                  className={b('is-package')}
                  role="button"
                  tabIndex={0}
                  onClick={() => {
                    setItPacked(true);
                    setShow({ ...show, optionShow: true });
                  }}
                >
                  {getStatusShow(true) ? (
                    <CheckFull
                      className={b('is-package-icon')}
                      stroke={whiteColor}
                      stroke2={whiteColor}
                      fill={redColor}
                    />
                  ) : (
                    <div className={b('default-is-package-icon')} />
                  )}
                  <p className={b('is-package-text')}>{PRINT_PACKAGES}</p>
                </div>
              </div>,
              refsRoot || null,
            )}
        </>
      )}
    </div>
  );
}

const stateProps = (state) => ({
  enhanceGetOption: enhanceGetOptionSelector(state),
});

ButtonEnhanceOptionsCheckout.propTypes = propTypes;
ButtonEnhanceOptionsCheckout.defaultProps = defaultProps;

export default connect(stateProps)(ButtonEnhanceOptionsCheckout);
