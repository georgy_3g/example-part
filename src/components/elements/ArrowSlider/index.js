import React from 'react';
import PropTypes from 'prop-types';
import Colors from 'src/styles/colors.json';
import dynamic from "next/dynamic";

const ArrowIcon = dynamic(() => import('src/components/svg/ArrowIcon'));

const blue = Colors['$dark-slate-blue-color'];

const arrowPropTypes = {
  className: PropTypes.string,
  style: PropTypes.shape({}),
  onClick: PropTypes.func,
  prefix: PropTypes.string,
  block: PropTypes.func,
  id: PropTypes.string,
  stroke: PropTypes.string,
  strokeWidth: PropTypes.string,
};

const arrowDefaultProps = {
  className: '',
  style: {},
  onClick: () => {},
  prefix: '',
  block: () => {},
  id: null,
  stroke: blue,
  strokeWidth: '2',
};

function ArrowSlider(props) {
  const { className, style, onClick, prefix, block, id, stroke, strokeWidth } = props;
  return (
    <span
      className={`${className} ${block('arrow')} ${block('arrow', { [prefix]: true })}`}
      style={{ ...style }}
      onClick={onClick}
      role="button"
      tabIndex={0}
      id={id}
    >
      <ArrowIcon
        stroke={stroke}
        strokeWidth={strokeWidth}
        className={`${block('svg', { [prefix]: true })}`}
      />
    </span>
  );
}

ArrowSlider.propTypes = arrowPropTypes;
ArrowSlider.defaultProps = arrowDefaultProps;

export default ArrowSlider;
