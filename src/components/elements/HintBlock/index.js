import React, { useState } from 'react';
import bem from 'src/utils/bem';
import PropTypes from 'prop-types';
import colors from 'src/styles/colors.json';
import Image from 'next/image';
import dynamic from "next/dynamic";
import styles from './index.module.scss';

const MagnifyingPlus = dynamic(() => import('src/components/svg/MagnifyingPlus'));
const MinusRoundIcon = dynamic(() => import('src/components/svg/MinusRoundIcon'));

const whiteColor = colors['$white-color'];

const b = bem('hint-text', styles);
const linkBem = bem('link', styles);

const defaultProps = {
  text: '',
  name: '',
  link: null,
  hintImages: [],
  className: '',
};

const propTypes = {
  text: PropTypes.string,
  name: PropTypes.string,
  hintImages: PropTypes.arrayOf(PropTypes.shape({})),
  link: PropTypes.shape({
    text: PropTypes.string,
    href: PropTypes.string,
  }),
  className: PropTypes.string,
};

function HintBlock(props) {
  const { text, link, hintImages, name, className } = props;

  const [imageData, showPopup] = useState(null);
  const wrapText = text ? text.split('#').join('\n') : '';
  return (
    <div className={b({ mix: className })}>
      <p className={b('text')}>
        {wrapText}
        {link && (
          <a className={b('link', { mix: linkBem() })} href={link.href} target="blank">
            {link.text}
          </a>
        )}
      </p>
      <div className={b('images-block')}>
        {Boolean(hintImages && hintImages[0]) &&
          hintImages.map(({ id, imageUri, image_uri: imgUri }) => (
            <div
              className={b('image-block')}
              key={`${name}_${id}`}
              style={{ backgroundImage: `url(${imageUri || imgUri})` }}
            >
              <button
                className={b('btn', { open: true })}
                type="button"
                onClick={() => showPopup({ imageUri: imageUri || imgUri, id })}
              >
                <MagnifyingPlus className={b('icon')} fill={whiteColor} />
              </button>
            </div>
          ))}
      </div>
      {imageData && imageData.imageUri && (
        <div className={b('popup-block')}>
          <div className={b('popup-content')}>
            <div className={b('container')}>
              <Image
                className={b('preview-image')}
                width='100%'
                height='100%'
                src={imageData.imageUri}
                alt={imageData.imageUri}
              />
              <button
                className={b('btn', { close: true })}
                type="button"
                onClick={() => showPopup(false)}
              >
                <MinusRoundIcon className={b('icon')} />
              </button>
            </div>
          </div>
        </div>
      )}
    </div>
  );
}

HintBlock.propTypes = propTypes;
HintBlock.defaultProps = defaultProps;

export default HintBlock;
