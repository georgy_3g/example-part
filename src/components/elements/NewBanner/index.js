import React from 'react';
import bem from 'src/utils/bem';
import useMobileStyle from 'src/utils/useMobileStyle';
import PropTypes from 'prop-types';
import { isMobileDeviceSelector } from 'src/redux/auth/selectors';
import { promoSectionSelector } from 'src/redux/promo/selectors';
import LazyLoad from 'react-lazyload';
import { connect } from 'react-redux';
import dynamic from 'next/dynamic';
import {
  AS_SEEN_ON,
  RESTORATION_AS_SEEN_ON,
  RESTORATION_DATA_TRUSTED,
  TRUSTED_BY_DOBLE_DOT,
} from 'src/constants/partners';
import Image from 'next/image';
import colors from 'src/styles/colors.json';
import styles from './index.module.scss';

const ColorButton = dynamic(() => import('src/components/buttons/ColorButton'));
const PolaroidImageSliderWithCompare = dynamic(() => import('src/components/widgets/PolaroidImageSliderWithCompare'));
const PolaroidImageSlider = dynamic(() => import('src/components/widgets/PolaroidImageSlider'));

const defaultColor = colors['$intense-citrine-white-color'];
const orangeColor = colors['$burnt-sienna-color'];

const b = bem('new-banner', styles);

const defaultProps = {
  bannerTitle: '',
  bannerDesc: '',
  bannerIcons: [],
  withCompare: false,
  bannerImages: [],
  firstStripeColor: defaultColor,
  isMobileDevice: false,
  promoSection: {},
  isSvg: false,
  withScrollButton: false,
  scroll: () => {},
  scrollButtonText: '',
  smallDescriptionMargin: false,
  withBannerIcons: false,
  withPartners: false,
  isPhotoArt: false,
  isBigBannerIcons: false,
  partnersIcon: {},
  btnClass: '',
  titleClass: '',
  isAutoPlay: false,
  autoplaySpeed: 4500,
  withTopButtonText: false,
  topButtonText: '',
};

const propTypes = {
  bannerTitle: PropTypes.string,
  bannerDesc: PropTypes.string,
  bannerIcons: PropTypes.arrayOf(
    PropTypes.shape({
      text: PropTypes.oneOfType([PropTypes.string, PropTypes.shape({})]),
      img: PropTypes.shape({}),
    }),
  ),
  withCompare: PropTypes.bool,
  bannerImages: PropTypes.arrayOf(PropTypes.shape({})),
  firstStripeColor: PropTypes.string,
  isMobileDevice: PropTypes.bool,
  promoSection: PropTypes.shape({
    id: PropTypes.number,
  }),
  isSvg: PropTypes.bool,
  withScrollButton: PropTypes.bool,
  scroll: PropTypes.func,
  scrollButtonText: PropTypes.string,
  smallDescriptionMargin: PropTypes.bool,
  withBannerIcons: PropTypes.bool,
  withPartners: PropTypes.bool,
  partnersIcon: PropTypes.shape({}),
  isBigBannerIcons: PropTypes.bool,
  btnClass: PropTypes.string,
  titleClass: PropTypes.string,
  isPhotoArt: PropTypes.bool,
  isAutoPlay: PropTypes.bool,
  autoplaySpeed: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  withTopButtonText: PropTypes.bool,
  topButtonText: PropTypes.string,
};

const NewBanner = (props) => {
  const {
    bannerTitle,
    bannerDesc,
    bannerIcons,
    withCompare,
    bannerImages,
    firstStripeColor,
    isMobileDevice,
    promoSection,
    isSvg,
    withScrollButton,
    scroll,
    scrollButtonText,
    smallDescriptionMargin,
    withBannerIcons,
    withPartners,
    isBigBannerIcons,
    btnClass,
    titleClass,
    isPhotoArt,
    isAutoPlay,
    autoplaySpeed,
    withTopButtonText,
    topButtonText,
  } = props;

  const isMobileDisplay = useMobileStyle(1024, isMobileDevice);

  const partners = (modifiers) => (
    <div className={b('partners', modifiers)}>
      <div className={b('partners-row', { mobile: modifiers.mobile })}>
        <div className={b('partners-text', { mobile: modifiers.mobile })}>
          {TRUSTED_BY_DOBLE_DOT}
        </div>
        <div className={b('partners-list-icon')}>
          {RESTORATION_DATA_TRUSTED.map(({ imgUrl: { defaultImg } }) => (
            <div className={b('partners-icon-wrap')} key={defaultImg.src}>
              <Image
                className={b('partners-icon')}
                src={defaultImg}
                alt="partner"
                width="100%"
                height="100%"
              />
            </div>
            ))}
        </div>
      </div>
      <div className={b('partners-row', { mobile: modifiers.mobile })}>
        <div className={b('partners-text', { mobile: modifiers.mobile })}>{AS_SEEN_ON}</div>
        <div className={b('partners-list-icon')}>
          {RESTORATION_AS_SEEN_ON.map(({ imgUrl: { defaultImg } }) => (
            <div className={b('partners-icon-wrap')} key={defaultImg.src}>
              <Image
                className={b('partners-icon')}
                src={defaultImg}
                alt="partner"
                width="100%"
                height="100%"
              />
            </div>
            ))}
        </div>
      </div>
    </div>
  );

  const slider = (settings) => (
    <div className={b('slider-block', settings)}>
      {withCompare ? (
        <PolaroidImageSliderWithCompare
          slideList={bannerImages}
          lazyLoad
          autoplay={isAutoPlay}
          pauseOnHover
          autoplaySpeed={autoplaySpeed || 4500}
          infinite={false}
          isStaticFiles
        />
        ) : (
          <PolaroidImageSlider
            slideList={bannerImages}
            lazyLoad
            autoplay={isAutoPlay}
            pauseOnHover
            autoplaySpeed={autoplaySpeed || 4500}
            isStaticFiles
          />
        )}
    </div>
  );

  return (
    <article
      className={b()}
      style={isMobileDisplay && promoSection.id ? { marginTop: `80px` || 0 } : {}}
    >
      <div className={b('stripe-one')} style={{ backgroundColor: firstStripeColor }} />
      <div className={b('stripe-two')} />
      <div className={b('stripe-three')} />
      <div className={b('stripe-four')} />
      <div className={b('stripe-five')} />
      <div className={b('container')}>
        <div className={b('text-block')}>
          <h1 className={b('title', { mix: titleClass, wrap: isPhotoArt })}>{bannerTitle}</h1>
          {isPhotoArt && (
            <h2
              className={b('desc', {
                'small-margin': smallDescriptionMargin,
                'mobile-art': true,
              })}
            >
              {bannerDesc}
            </h2>
          )}
          {(isBigBannerIcons || isPhotoArt) &&
            slider({
              mobile: true,
              desktop: false,
              art: isPhotoArt,
            })}
          <h2
            className={b('desc', {
              'small-margin': smallDescriptionMargin,
              'desktop-art': isPhotoArt,
            })}
          >
            {bannerDesc}
          </h2>
          {withBannerIcons && (
            <LazyLoad offset={100}>
              <div className={b('icons-block', { desktop: true })}>
                {bannerIcons.map(({ text, img, svg: Svg, description, backgroundColor }) => (
                  <div
                    className={b('icon-block', { padding: backgroundColor })}
                    key={text || description}
                    style={{ backgroundColor }}
                  >
                    {isSvg ? (
                      <Svg className={b('icon')} width="80" height="80" />
                    ) : (
                      <Image
                        className={b('icon', { 'no-border-radius': backgroundColor })}
                        src={img}
                        alt={text || description}
                        width={80}
                        height={80}
                      />
                    )}
                    <span className={b('icon-block-text')}>{text}</span>
                  </div>
                ))}
              </div>
            </LazyLoad>
          )}
          {withTopButtonText && topButtonText && (
            <span className={b('top-button-text')}>{topButtonText}</span>
          )}
          {withScrollButton && scrollButtonText && (
            <ColorButton
              className={b('scroll-button', { mix: btnClass })}
              onClick={scroll}
              text={scrollButtonText}
              backGroundColor={orangeColor}
            />
          )}
          {withPartners &&
            partners({
              mobile: false,
              desktop: true,
            })}
        </div>
        {slider({
          mobile: false,
          desktop: isBigBannerIcons || isPhotoArt,
        })}
      </div>
      {withPartners &&
        partners({
          mobile: true,
          desktop: false,
        })}
      {withBannerIcons && (
      <div className={b('icons-block', { mobile: true })}>
        {bannerIcons.map(({ text, img, svg: Svg, backgroundColor }) => (
          <div
            className={b('block-icon', { padding: backgroundColor})}
            style={{ backgroundColor }}
            key={text}
          >
            <div
              className={b('icon-block', { padding: backgroundColor })}
            >
              {isSvg ? (
                <Svg className={b('icon')} width="80" height="80" />
                ) : (
                  <Image
                    className={b('icon', { 'no-size': backgroundColor })}
                    src={img}
                    alt={text}
                    layout="fill"
                  />
                )}
              <span className={b('icon-block-text')}>{text}</span>
            </div>
          </div>
        ))}
      </div>
      )}
      {isBigBannerIcons && (
      <div className={b('mobile-icons')}>
        <div className={b('icons')}>
          {bannerIcons.map(({ text, img }) => (
            <div className={b('big-icon', { oddQuantity: bannerIcons.length % 2 !== 0 })} key={text}>
              <img
                className={b('mobile-icon')}
                src={img.src || img}
                alt={text}
              />
              <div className={b('mobile-icon-text')}>{text}</div>
            </div>
          ))}
        </div>
        <div className={b('mobile-icons-background')} />
      </div>
      )}
    </article>
  );
}

NewBanner.propTypes = propTypes;
NewBanner.defaultProps = defaultProps;

const stateProps = (state) => ({
  isMobileDevice: isMobileDeviceSelector(state),
  promoSection: promoSectionSelector(state),
});

export default connect(stateProps, null)(NewBanner);
