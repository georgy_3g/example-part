import React from 'react';
import PropTypes from 'prop-types';
import Colors from 'src/styles/colors.json';
import dynamic from "next/dynamic";

const ArrowInSquareIcon = dynamic(() => import('src/components/svg/ArrowInSquareIcon'));

const white = Colors['$white-color'];

const arrowPropTypes = {
  className: PropTypes.string,
  style: PropTypes.shape({}),
  onClick: PropTypes.func,
  prefix: PropTypes.string,
  block: PropTypes.func,
  id: PropTypes.string,
  stroke: PropTypes.string,
  strokeWidth: PropTypes.string,
};

const arrowDefaultProps = {
  className: '',
  style: {},
  onClick: () => {},
  prefix: '',
  block: () => {},
  id: null,
  stroke: white,
  strokeWidth: '2',
};

function ArrowSlider(props) {
  const { className, style, onClick, prefix, block, id, stroke, strokeWidth } = props;
  return (
    <span
      className={`${className} ${block('new-arrow')} ${block('new-arrow', { [prefix]: true })}`}
      style={{ ...style }}
      onClick={onClick}
      role="button"
      tabIndex={0}
      id={id}
    >
      <ArrowInSquareIcon
        stroke={stroke}
        strokeWidth={strokeWidth}
        className={`${block('new-svg', { [prefix]: true })}`}
      />
    </span>
  );
}

ArrowSlider.propTypes = arrowPropTypes;
ArrowSlider.defaultProps = arrowDefaultProps;

export default ArrowSlider;
