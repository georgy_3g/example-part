import React, { Component } from 'react';
import PropTypes from 'prop-types';
import bem from 'src/utils/bem';
import dynamic from "next/dynamic";
import { LOGIN, SIGN_UP, FORGOT_PASS, LIVE_HELP } from './constants';
import styles from './index.module.scss';

const CloseIcon2 = dynamic(() => import('src/components/svg/CloseIcon2'));
const BackIcon = dynamic(() => import('src/components/svg/BackIcon'));

const b = bem('modal', styles);

const defaultProps = {
  close: () => {},
  children: null,
  modalCenter: false,
  className: '',
  isOpenMenu: false,
  popupName: 'login',
  openLoginPopup: () => {},
  isLiveHelp: false,
  closeBtn: false,
};

const propsTypes = {
  close: PropTypes.func,
  children: PropTypes.shape({}),
  modalCenter: PropTypes.bool,
  className: PropTypes.string,
  isOpenMenu: PropTypes.bool,
  popupName: PropTypes.string,
  openLoginPopup: PropTypes.func,
  isLiveHelp: PropTypes.bool,
  closeBtn: PropTypes.bool,
};

class ModalContainer extends Component {
  constructor(props) {
    super(props);
    this.veil = React.createRef();
  }

  closeModal = (e) => {
    const { close } = this.props;
    if (e.target === this.veil.current) {
      close();
    }
  };

  backToLogin = () => {
    const { openLoginPopup, close } = this.props;
    close();
    openLoginPopup();
  };

  render() {
    const { children, close, modalCenter, className, isOpenMenu, popupName, isLiveHelp, closeBtn } =
      this.props;
    return (
      <div className={b()} ref={this.veil} onClick={this.closeModal} role="button" tabIndex="0">
        <div
          className={b('position', {
            mix: className,
            center: modalCenter,
            top: !modalCenter,
            menu: isOpenMenu,
            'live-help': isLiveHelp,
          })}
        >
          {!isOpenMenu && popupName !== LIVE_HELP && (
            <button className={b('btn', { 'close-btn': closeBtn })} type="button" onClick={close}>
              <CloseIcon2 />
            </button>
          )}
          {!isOpenMenu && (popupName === SIGN_UP || popupName === FORGOT_PASS) && (
            <button
              className={b('btn', { 'btn-to-login': true })}
              type="button"
              onClick={this.backToLogin}
            >
              <BackIcon />
              <span className={b('btn-text')}>{LOGIN}</span>
            </button>
          )}
          {children}
        </div>
      </div>
    );
  }
}

ModalContainer.propTypes = propsTypes;
ModalContainer.defaultProps = defaultProps;

export default ModalContainer;
