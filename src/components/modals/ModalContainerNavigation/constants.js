const CLOSE = 'Close';
const LOGIN = 'Login';
const SIGN_UP = 'signUp';
const FORGOT_PASS = 'forgotPass';
const LIVE_HELP = 'liveHelp';

export { CLOSE, LOGIN, SIGN_UP, FORGOT_PASS, LIVE_HELP };
