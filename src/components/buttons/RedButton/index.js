import React from 'react';
import PropTypes from 'prop-types';
import bem from 'src/utils/bem';
import styles from './index.module.scss';

const b = bem('red-button', styles);

const defaultProps = {
  onClick: () => {},
  text: '',
  className: '',
  disabled: false,
  type: 'button',
};

const propTypes = {
  onClick: PropTypes.func,
  text: PropTypes.string,
  className: PropTypes.string,
  disabled: PropTypes.bool,
  type: PropTypes.string,
};

const RedButton = (props) => {
  const { onClick, text, className, disabled, type } = props;

  return (
    <button
      className={b({ mix: className })}
      // eslint-disable-next-line react/button-has-type
      type={type}
      onClick={onClick}
      disabled={disabled}
    >
      {text}
    </button>
  );
};

RedButton.propTypes = propTypes;
RedButton.defaultProps = defaultProps;

export default RedButton;
