import React from 'react';
import PropTypes from 'prop-types';
import bem from 'src/utils/bem';
import colors from 'src/styles/colors.json';
import dynamic from "next/dynamic";
import styles from './index.module.scss';

const ArrowIcon = dynamic(() =>import('src/components/svg/ArrowIcon')) ;

const darkBlueColor = colors['$rhino-blue-color'];
const whiteColor = colors['$white-color'];

const b = bem('color-button', styles);

const defaultProps = {
  onClick: () => {},
  text: '',
  className: '',
  textClass: '',
  backGroundColor: darkBlueColor,
  withArrow: false,
  withReversIcon: false,
  disabled: false,
  type: 'button',
};

const propTypes = {
  onClick: PropTypes.func,
  text: PropTypes.string,
  className: PropTypes.string,
  textClass: PropTypes.string,
  backGroundColor: PropTypes.string,
  withArrow: PropTypes.bool,
  withReversIcon: PropTypes.bool,
  disabled: PropTypes.bool,
  type: PropTypes.string,
};

const ColorButton = (props) => {
  const {
    onClick,
    text,
    className,
    textClass,
    backGroundColor,
    withArrow,
    withReversIcon,
    disabled,
    type = 'button',
  } = props;

  return (
    <button
      className={b({ mix: className, disabled })}
      style={{ backgroundColor: backGroundColor || darkBlueColor }}
      // eslint-disable-next-line react/button-has-type
      type={type}
      onClick={onClick}
      disabled={disabled}
    >
      <div className={b('button', { 'with-reverse-icon': withReversIcon })}>
        <div className={b('button-text', { mix: textClass })}>{text}</div>
        {(withArrow || withReversIcon) && (
          <div className={b('arrow', { 'with-reverse-icon': withReversIcon })}>
            <ArrowIcon
              className={b('back-icon')}
              width="8"
              height="11"
              strokeWidth="3"
              stroke={whiteColor}
            />
          </div>
        )}
      </div>
    </button>
  );
};

ColorButton.propTypes = propTypes;
ColorButton.defaultProps = defaultProps;

export default ColorButton;
