import React from 'react';
import PropTypes from 'prop-types';
import bem from 'src/utils/bem';
import colors from 'src/styles/colors.json';
import styles from './index.module.scss';

const darkBlueColor = colors['$rhino-blue-color'];

const b = bem('blue-button', styles);

const defaultProps = {
  onClick: () => {},
  className: '',
  backGroundColor: darkBlueColor,
  disabled: false,
  type: 'button',
  children: null,
};

const propTypes = {
  onClick: PropTypes.func,
  className: PropTypes.string,
  backGroundColor: PropTypes.string,
  disabled: PropTypes.bool,
  type: PropTypes.string,
  children: PropTypes.node,
};

const BlueButton = (props) => {
  const { onClick, className, backGroundColor, disabled, type = 'button', children } = props;

  return (
    <button
      className={b({ mix: className, disabled })}
      style={{ backgroundColor: backGroundColor || darkBlueColor }}
      // eslint-disable-next-line react/button-has-type
      type={type}
      onClick={onClick}
      disabled={disabled}
    >
      {children}
    </button>
  );
};

BlueButton.propTypes = propTypes;
BlueButton.defaultProps = defaultProps;

export default BlueButton;
