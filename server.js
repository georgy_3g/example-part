const express = require('express');
const next = require('next');
const path = require('path');
const basicAuth = require('express-basic-auth');
const compression = require('compression');
const cookieParser = require('cookie-parser');
const Rollbar = require('rollbar');

function installRollbarErrorHandler(app) {
  const renderErrorToHTML = app.renderErrorToHTML.bind(app);
  const rollbar = new Rollbar({
    accessToken: process.env.NEXT_PUBLIC_ROLLBAR_TOKEN,
    environment: `client ${process.env.NEXT_PUBLIC_NODE_ENV}`,
  });
  const errorHandler = rollbar.errorHandler();
  // eslint-disable-next-line no-param-reassign
  app.renderErrorToHTML = (err, req, res, pathname, query) => {
    if (err && process.env.NEXT_PUBLIC_NODE_ENV !== 'develop' && process.env.NEXT_PUBLIC_ROLLBAR_TOKEN) {
      errorHandler(err, req, res, () => {  });
    }
    return renderErrorToHTML(err, req, res, pathname, query);
  };
  return app;
}

const port = parseInt(process.env.NEXT_PUBLIC_PORT, 10) || 3000;
const dev = process.env.NEXT_PUBLIC_NODE_ENV !== 'production';
const app = next({ dev });
const appWithRollbar = installRollbarErrorHandler(app);
const handle = appWithRollbar.getRequestHandler();

appWithRollbar.prepare().then(() => {
  const server = express();

  server.use(cookieParser());

  if (!dev) {
    server.use(compression());
  }

  if (!dev) {
    // PRODUCTION Only
    server.use((req, res, nextStep) => {
      const isStaticReq = req.path.indexOf('public/') !== -1;
      const isNextReq = req.path.indexOf('/_next/') !== -1;
      if (isStaticReq || isNextReq) {
        return nextStep();
      }
      if (req.headers['x-forwarded-proto'] && req.headers['x-forwarded-proto'] === 'http') {
        return res.redirect(301, `https://${req.headers.host}${req.url}`);
      }
      return nextStep();
    });
  }

  server.get('/check', (req, res) => {
    res.send('ok');
  });

  // Authentication
  if (process.env.AUTH_BASIC_USER && process.env.AUTH_BASIC_PASSWORD) {
    server.use(
      basicAuth({
        users: { [process.env.AUTH_BASIC_USER]: process.env.AUTH_BASIC_PASSWORD },
        challenge: true,
      }),
    );
    console.log(
      `Authentication required! Use "${process.env.AUTH_BASIC_USER}" - "${process.env.AUTH_BASIC_PASSWORD}" as credentials`,
    );
  }

  server.get('/', (req, res) => app.render(req, res, '/home', req.query));

  server.get('/contact', (req, res) => app.render(req, res, '/contact', req.query));

  server.get('/login', (req, res) => app.render(req, res, '/loginPage', req.query));

  server.get('/reviews', (req, res) => app.render(req, res, '/reviews', req.query));

  server.get('/blog', (req, res) => app.render(req, res, '/blogList', req.query));

  server.get('/account', (req, res) => app.render(req, res, '/account', req.query));

  server.get('/cart/checkout', (req, res) => app.render(req, res, '/checkout', req.query));

  server.get('/faq', (req, res) => app.render(req, res, '/faq', req.query));

  server.get('/robots.txt', (req, res) => {
    res.sendFile(path.join(__dirname, '/public', 'robots.txt'));
  });

  server.get('/sitemap.xml', (req, res) => app.render(req, res, '/sitemap', req.query));

  server.get('/favicon.ico', (req, res) => {
    res.sendFile(path.join(__dirname, 'public', '/favicon.ico'));
  });

  server.get('/fonts/stylesheet.css', (req, res) => {
    res.sendFile(path.join(__dirname, '/public', 'fonts/stylesheet.css'));
  });

  server.get('/fonts/stylesheetOthers.css', (req, res) => {
    res.sendFile(path.join(__dirname, '/public', 'fonts/stylesheetOthers.css'));
  });

  server.use((req, res, nextStep) => {
    const isStaticReq = req.path.indexOf('public/') !== -1;
    const isNextReq = req.path.indexOf('/_next/') !== -1;
    if (isStaticReq || isNextReq) {
      return nextStep();
    }

    return nextStep();
  });

  server.get('*', (req, res) => handle(req, res));

  server.listen(port, (err) => {
    if (err) throw err;
    console.log(`> Ready on http://localhost:${port}`);
  });
});
